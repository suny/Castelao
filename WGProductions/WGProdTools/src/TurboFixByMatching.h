/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: TurboFixByMatching.h
#ifndef TURBOFIXBYMATCHING_H
#define TURBOFIXBYMATCHING_H 1

// Include files
// from DaVinci, this is a specialized GaudiAlgorithm
#include "Kernel/DaVinciTupleAlgorithm.h"
#include "DetDesc/Condition.h"

// =================================
// ROOT
// =================================
#include "TH1.h"
#include "TFile.h"
//
// =================================
// TeslaTools
// =================================
#include "TeslaTools/ITeslaMatcher.h"



class TurboFixByMatching : public DaVinciTupleAlgorithm {
  public:
    TurboFixByMatching( const std::string& name, ISvcLocator* pSvcLocator );

    virtual ~TurboFixByMatching( );

    StatusCode initialize() override;
    StatusCode execute   () override;
    StatusCode finalize  () override;

    StatusCode fixDaughters ( LHCb::Particle * );

  protected:

  private:
    Gaudi::Property<std::string> m_matcherToolName { this,
      "MatcherTool" , "TeslaMatcher:PUBLIC", "Name of the matching tool" };

    Gaudi::Property<std::string> m_inputParticles { this,
      "InputLocation", "", "TES location of particles to be fixed" };

    Gaudi::Property<std::string> m_targetTes { this,
      "TargetTES"   , "/Event/Rec/Track/Best", 
      "TES location of the particles to match and replace to daughters"
    };

    ITeslaMatcher*  m_matcher;



};
#endif // TURBOFIXBYMATCHING_H

