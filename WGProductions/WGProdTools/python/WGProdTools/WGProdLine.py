#!/usr/bin/env gaudirun.py
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# -*- coding: utf-8 -*-
# =============================================================================
## @file WGProdLine.py
#  Base class for WGProduction lines
#  @attention DataType and CondDB are not specified!
#  Usage: override and implement selection()
#  
#  @author Lucio Anderlini lucio.anderlini@cern.ch
#  @date   2017-07-10
# =============================================================================
""" 
BaseClass for ProductionLine
"""

from WGProdWriter import WGProdWriter
from Configurables import DaVinci
from Gaudi.Configuration import ConfigurableUser


class WGProdLine (ConfigurableUser):
  "Base class for WG production lines"

  __slots__ = {
     "inputStream"  : 'DIMUON.DST',
     "targetStream" : 'WGProdStream',
     "debug"        : True,
     "configDict"   : {},
  }
  __used_configurables__ = [DaVinci, WGProdWriter]

  def __init__ (self, name="WGProdLine", _enabled=True, **options):
    if name == "WGProdLine": name = self.__class__.__name__

    ConfigurableUser.__init__(self, name, _enabled, **options)

    print "name", self.name()
    self.configDict.update ( self.defaults )

#    writer = WGProdWriter ( 'WG' + self.getProp ("targetStream" ) )

  def configure (self, option_dict=None):
    if option_dict == None: option_dict = {}

    for key in option_dict.keys():
      if key not in self.configDict.keys():
        raise KeyError ( "Option %s not defined in %s" % (key, self.name()) )
      else:
        self.configDict[key] = option_dict[key]



  def __str__ ( self ):
    string = ConfigurableUser.__str__ ( self )

    lines = [line for line in string.split('\n') if 'configDict' not in line]


    newlines = [ "|-configDict:" ]
    newlines += [ "|  -{:<20} = {:<50}".format (
                  k, "'%s'"%self.configDict[k]) for k in self.configDict.keys()] 

    return "\n".join ( lines[:-1] + newlines + [lines[-1]] )


  def getWGProdSelections(self):
    from PhysSelPython.Wrappers import  MultiSelectionSequence, SelectionSequence

    selectionList = self.selection(self.configDict)

    if not isinstance (selectionList, (list,tuple,set)):
      selectionList = [ selectionList ]

    return selectionList


