###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#####
## Author: Lucio Anderlini (Lucio.Anderlini@cern.ch)
##    Istituto Nazionale di Fisica Nucleare
##

from Gaudi.Configuration import ConfigurableUser
from Configurables import DaVinci, SelDSTWriter

class WGProdWriter (ConfigurableUser):
  "This Writer handles an output stream"
  __used_configurables__ = [ DaVinci, (SelDSTWriter, "MyDSTWriter") ]

  __slots__ = {
    'Sequences'          : [],
    'TargetWriter'       : 'MyDSTWriter',
    'Stream'             : 'WGProdStream',
    'TurboMatchLocation' : "",
    'RootInTES'          : "/Event",
  }

  def __init__ ( self, name="WG", _enabled=True, **options ):
    ConfigurableUser.__init__ ( self, name, _enabled, **options ) 
    self.setProp('Stream', self.name()[2:])


  def __apply_configuration__(self):
    from PhysSelPython.Wrappers import  MultiSelectionSequence, SelectionSequence

    if not isinstance (self.Sequences, (list,tuple,set)):
      self.Sequences = [ self.Sequences ]


    from Gaudi.Configuration import allConfigurables
    writer = SelDSTWriter ( self.getProp('TargetWriter') )
    writer.applyConf()

    from Configurables import TurboFixByMatching
    from PhysSelPython.Wrappers import AutomaticData 
    from PhysConf.Selections import RebuildSelection

    ## TurboMatchLocation allows to replace proto particles from Tesla with
    ## those from PersistReco obtaining consistent decay candidates.
    turfixLoc = self.getProp('TurboMatchLocation')
    if turfixLoc != "":
      targetSel = RebuildSelection(AutomaticData(turfixLoc))
      DaVinci().UserAlgorithms.append ( targetSel )

    withRoot = lambda x: (self.getProp('RootInTES')+'/'+x).replace('//','/')

    sequences = []
    for configName in allConfigurables.keys():
      configurable = allConfigurables.get(configName)
      if "getWGProdSelections" in dir(configurable):
        print configurable.name() , configurable.getProp("targetStream") , self.getProp('Stream')
        if configurable.getProp("targetStream") == self.getProp('Stream'):
          for sel in configurable.getWGProdSelections():
            turfixAlg = []
            if turfixLoc != "":
              targetTes = withRoot(targetSel.outputLocation())
              turfixAlg = [
                TurboFixByMatching ( 
                  "TurboFix" + sel.name() ,
                  InputLocation = sel.outputLocation(),
                  TargetTES     = targetTes
                )
              ]

            sequences += [
              SelectionSequence ( sel.name()+'Seq', sel,
                  PostSelectionAlgs = turfixAlg
                ) ]
        
    for sequence in sequences:
      DaVinci().UserAlgorithms.append ( sequence )

    selectionSequence = MultiSelectionSequence ( 
        self.getProp ( 'Stream' ), sequences
      )

    writer.SelectionSequences.append ( selectionSequence )
    DaVinci().UserAlgorithms.insert ( 0, selectionSequence )


  def appendSelections ( self, selections ):
    if not isinstance (selections, (list,tuple,set)):
      selections = [selections]

    for selection in selections: self.Sequences.append ( selection )



