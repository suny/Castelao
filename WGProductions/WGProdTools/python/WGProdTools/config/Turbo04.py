###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#### Configures the DST Writer
from DSTWriters.Configuration import ( SelDSTWriter            ,
                                       stripMicroDSTStreamConf ,
                                       stripMicroDSTElements   )

from DSTWriters.microdstelements import CloneHltDecReports, CloneRawBanks

def config ( inputStreamName, outputStreamName ):
  # Default Configuration of SelDSTWriter
  SelDSTWriterConf     = { 'default' : stripMicroDSTStreamConf ( pack = False ) }
  SelDSTWriterElements = { 'default' : stripMicroDSTElements   ( pack = False , refit = False ) }

  usefulBanks = [
        "L0Calo",  
        "L0DU",  
  ##      "PrsE",  
  ##      "EcalE",   
  ##      "HcalE",   
  #      "PrsTrig",   
  #      "EcalTrig",  
  #      "HcalTrig",  
  ##      "Velo",  
  ##      "Rich",  
  ##      "TT",  
  ##      "IT",  
  ##      "OT",  
  ##      "Muon",  
        "L0PU",  
  ##      "DAQ",   
  ##      "ODIN",  
        "HltDecReports",   
  ##      "VeloFull",  
  ##      "TTFull",  
  ##      "ITFull",  
  ##      "EcalPacked",  
  ##      "HcalPacked",  
  ##      "PrsPacked",   
        "L0Muon",  
  ##      "ITError",   
  ##      "TTError",   
  ##      "ITPedestal",  
  ##      "TTPedestal",  
  ##      "VeloError",   
  ##      "VeloPedestal",  
  ##      "VeloProcFull",  
  ##      "OTRaw",   
  ##      "OTError",   
  ##      "EcalPackedError",   
  ##      "HcalPackedError",   
  ##      "PrsPackedError",  
        "L0CaloFull",  
        "L0CaloError",   
  ##      "L0MuonCtrlAll",   
        "L0MuonProcCand",  
        "L0MuonProcData",  
        "L0MuonRaw",   
        "L0MuonError",   
  ##      "GaudiSerialize",  
  ##      "GaudiHeader",   
  ##      "TTProcFull",  
  ##      "ITProcFull",  
  ##      "TAEHeader",   
  ##      "MuonFull",  
  ##      "MuonError",   
  ##      "TestDet",   
        "L0DUError",   
        "HltRoutingBits",  
        "HltSelReports",   
  #      "HltVertexReports",  
        "HltLumiSummary",  
        "L0PUFull",  
        "L0PUError",   
  ##      "DstBank",   
  ##      "DstData",   
  ##      "DstAddress",  
  ##      "FileID",  
  ##      "VP",  
  ##      "FTCluster",   
  ##      "VL",  
  ##      "UT",  
  ##      "UTFull",  
  ##      "UTError",   
  ##      "UTPedestal",  
  ##      "HC",  
  #      "HltTrackReports",   
  ##      "HCError",   
      ]


  from WGProdTools import WGProdWriter
  writerName = 'WG' + outputStreamName
  WGProdWriter (writerName).TurboMatchLocation = "Phys/StdAllNoPIDsPions/Particles"
  WGProdWriter (writerName).RootInTES = "/Event/%s/Turbo"%inputStreamName

  SelDSTWriterConf['default'].remapTES = [
    ("/Event/%s/%s/Turbo/Track/Best/Long" % (outputStreamName,inputStreamName),
     "/Event/%s/%s/Turbo/Rec/Track/Best" % (outputStreamName,inputStreamName)),
#    ("/Event/%s/%s/Turbo/Rec/Summary" % (outputStreamName,inputStreamName),
#     "/Event/Rec/Summary"),
  ]


  #SelDSTWriterConf['default'].extraItems = [ e for e in SelDSTWriterConf['default'].extraItems if e != "/Event/Turbo#99" ]
  SelDSTWriterConf['default'].extraItems = []
  for item in SelDSTWriterConf['default'].extraItems:
    print "Selecting:", item


  ################################################################################
  ## Overriding default configuration to allow Turbo input                      ##
  ################################################################################

  from DSTWriters.dstwriterutils import dataLocations
  def myDataLocations(selSequence,extension,deepSearch=False) :
      return ["/Event/%s/Turbo/%s"%(inputStreamName,x) for x in dataLocations(selSequence,extension,deepSearch)]

  usefulElements = [
    "ClonePVs",
    "FindDuplicates",
    "CloneParticleTrees",
    "CloneBTaggingInfo",
    "CloneRelatedInfo",
    "ClonePVRelations",
  ]
  newElements = []

  for element in SelDSTWriterElements['default']:
    element.dataLocations = myDataLocations
    print element.__class__.__name__
    if element.__class__.__name__ in usefulElements:
      newElements.append ( element )

  #
  SelDSTWriterElements['default'] = newElements

  from Configurables import DaVinci
  from Configurables import Gaudi__DataCopy

  SelDSTWriterConf['default'].extraItems += [
  ## I try to add the following instead of the whole Turbo
    "/Event/Turbo/Rec/Summary#99",


  ### With those below this line I have seen it working
    "/Event/DAQ#99",
    "/Event/Trigger#99"
  #  "/Event/DAQ#99",
  ]

  from Configurables import RootCnvSvc
  RootCnvSvc().GlobalCompression = "LZMA:4"


  #from Configurables import bankKiller
  #bk = bankKiller ( BankTypes = usefulBanks, DefaultIsKill = True )

  ################################################################################
  ## Instance of the DSTWriter and its sequence                                 ##
  ################################################################################

  uDstWriter = SelDSTWriter(
      "MyDSTWriter"                               ,
      StreamConf         =  SelDSTWriterConf      ,
      MicroDSTElements   =  SelDSTWriterElements  ,
    )

  ################################################################################
  ## Cloners with special configurations to write all outputs in WGProd/Turbo
  ################################################################################
  cloners = []


  #########
  ## ODIN CLONER
  #
  #from Configurables import CopyODIN
  #cloners.append ( 
  #    CopyODIN(
  #      "WGP_ODINCloner_WGProd",
  #      RootInTES = "/Event",
  #      OutputPrefix = "WGProd/Turbo"
  #    )
  #)
  #
  #SelDSTWriterConf['default'].extraItems.append("/Event/WGProd/Turbo/DAQ/ODIN#99")

  #########
  ## HLT DecReports
  from Configurables import CopyHltDecReports
  cloners.append ( 
      CopyHltDecReports(
        "WGP_HltDecReportsNCloner_WGProd",
        RootInTES = "/Event",
        InputLocations = [ inputStreamName + "/Turbo/Hlt1/DecReports", 
                           inputStreamName + "/Turbo/Hlt2/DecReports" ],
        OutputPrefix = outputStreamName
      )
  )

  SelDSTWriterConf['default'].extraItems.append("/Event/%s/Turbo/Hlt1#99"%outputStreamName)
  SelDSTWriterConf['default'].extraItems.append("/Event/%s/Turbo/Hlt2#99"%outputStreamName)

  #########
  ## RecSummary
  from Configurables import MoveDataObject
  triggerRaw = MoveDataObject("MoveTriggerRaw")
  triggerRaw.InputLocations = [inputStreamName+"/Turbo/Rec/Summary"]
  triggerRaw.OutputPrefix = outputStreamName
  triggerRaw.RootInTES = "/Event"
  cloners.append ( triggerRaw )

  SelDSTWriterConf['default'].extraItems.append("/Event/%s/%s/Turbo/Rec/Summary#99"%(outputStreamName,inputStreamName))

  #from Configurables import CopyL0DUReport
  #cloners . append (
  #  CopyL0DUReport("CopyL0DU", OutputPrefix="WGProd/Turbo" )
  #)
  #SelDSTWriterConf['default'].extraItems.append("/Event/WGProd/Turbo/Trig#99")



  ## TES explorer
  #from Configurables import StoreExplorerAlg
  #explorer = StoreExplorerAlg(PrintEvt=100) 
  #explorer2 = StoreExplorerAlg("Expl2", PrintEvt=100) 

  from Configurables import DaVinci
  DaVinci.UserAlgorithms =  cloners + [ uDstWriter.sequence()]



  ################################################################################
  ## Configuration wrappers for Turbo03
  from Configurables import CondDB, TurboConf, DstConf, DaVinci

  #DstConf   ( Turbo       = True )
  #TurboConf ( PersistReco = True )

  ################################################################################
  ## fix for persist reco, not needed if "plain" Turbo is used 
  #          I would suggest to merge it into a single place, e.g. in TurboConf
  from Configurables import DataOnDemandSvc
  dod = DataOnDemandSvc( Dump = True )
  from Configurables import Gaudi__DataLink as Link
  for  name , target , what  in [
  #    ( 'LinkHlt2Tracks'    , '/Event/Turbo/Hlt2/TrackFitted/Long' , '/Event/Hlt2/TrackFitted/Long'     ) , 
  #    ( 'LinkPPs'           , '/Event/Turbo/Rec/ProtoP/Charged'    , '/Event/Turbo/Hlt2/Protos/Charged' ) ,
  #    ( 'LinkDAQ'           , '/Event/Turbo/DAQ'                   , '/Event/DAQ'           ) ,
    ] :
      dod.AlgMap [ target ] = Link ( name , Target = target , What = what , RootInTES = '' ) 
      



  ################################################################################
  ## DaVinci configuration (ROOT in TES, not Simulation, Lumi defined)
  DaVinci (
      InputType    = 'MDST'           ,
      RootInTES    = '/Event/%s/Turbo'  % inputStreamName ,
      Simulation   = False            ,
      PrintFreq    = 5000             ,
      Lumi         = True             ,
      Turbo        = True             ,
    )


