###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
#### Configures the DST Writer
from DSTWriters.Configuration import ( SelDSTWriter            ,
                                       stripMicroDSTStreamConf ,
                                       stripMicroDSTElements   )

from DSTWriters.microdstelements import CloneHltDecReports

def config ( outputStreamName, inputStreamName = '' ):

  # Default Configuration of SelDSTWriter
  SelDSTWriterConf     = { 'default' : stripMicroDSTStreamConf ( pack = False ) }
  SelDSTWriterElements = { 'default' : stripMicroDSTElements   ( pack = False , refit = False ) }

  SelDSTWriterConf['default'].extraItems.append("Rec/Summary#99")

  SelDSTWriterConf['default'].extraItems += [
    'Hlt1/DecReports#99'
    'Hlt2/DecReports#99'
  #  '/Event/Hlt1/SelReports#99'
  #  '/Event/Hlt2/SelReports#99'
  ]

  SelDSTWriterElements ['default'] += [
      CloneHltDecReports(locations=["Hlt1/DecReports", "Hlt2/DecReports"]),
    ]

  from WGProdTools import WGProdWriter
  writerName = 'WG' + outputStreamName
  WGProdWriter (writerName).RootInTES = "/Event"


  ################################################################################
  ## Instance of the DSTWriter and its sequence                                 ##
  ################################################################################

  RIT = "/Event" if inputStreamName is '' else "/Event/"+inputStreamName
  uDstWriter = SelDSTWriter(
      "MyDSTWriter"                               ,
      StreamConf         =  SelDSTWriterConf      ,
      MicroDSTElements   =  SelDSTWriterElements  ,
      RootInTES          =  RIT
    )

  from Configurables import DaVinci
  DaVinci.UserAlgorithms = [ uDstWriter.sequence() ]


  from Configurables import RootCnvSvc
  RootCnvSvc().GlobalCompression = "LZMA:6"


  ################################################################################
  ## DaVinci configuration (ROOT in TES, not Simulation, Lumi defined)
  DaVinci (
      InputType       = 'DST' if inputStreamName is '' else 'MDST',
      Simulation      = False            ,
      PrintFreq       = 5000             ,
      Lumi            = True             ,
      RootInTES       = RIT
    )

