###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from config.Turbo02   import config as configureTurbo02
from config.Turbo03   import config as configureTurbo03
from config.Turbo04   import config as configureTurbo04
from config.Stripping import config as configureStripping


def defineConfiguration (
      configuration_key ,
      **kwargs
    ):

  if configuration_key == "Turbo02":
    configureTurbo02 ( **kwargs )
  elif configuration_key == "Turbo03":
    configureTurbo03 ( **kwargs )
  elif configuration_key == "Turbo04":
    configureTurbo04 ( **kwargs )
  elif configuration_key == "Stripping":
    configureStripping ( **kwargs )
  else:
    raise KeyError ( "Unknown configuration key %s" % configuration_key );

