/*****************************************************************************\
 * (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/


// Include files 

 // from Gaudi
#include "Event/FlavourTag.h"
#include "Event/VertexBase.h"
#include "Kernel/Particle2Vertex.h"
#include <boost/algorithm/string/replace.hpp>

// local
#include "restoreRelations.h"

//-----------------------------------------------------------------------------
// Implementation file for class : restoreRelations
//
// 2016-09-01 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( restoreRelations )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
restoreRelations::restoreRelations( const std::string& name,
                                    ISvcLocator* pSvcLocator)
  : DaVinciAlgorithm ( name , pSvcLocator ){

  declareProperty("Origin",m_origins);
  

}

//=============================================================================
// Main execution
//=============================================================================
std::vector<const LHCb::Particle*> restoreRelations::getTree( const LHCb::Particle* P ){
  std::vector<const LHCb::Particle*> tree;
  if( P->isBasicParticle() ){
    tree.push_back( P );
  }else{
    SmartRefVector<LHCb::Particle> daughters = P->daughters(); // local copy to sort
    for (SmartRefVector<LHCb::Particle>::const_iterator idau = daughters.begin(); idau!= daughters.end();++idau){
      std::vector<const LHCb::Particle*> temp=getTree( *idau );
      for( std::vector<const LHCb::Particle*>::iterator dd=temp.begin();temp.end() !=dd; ++dd){
        tree.push_back( *dd );
      }
    }
  }
  return tree;
}


bool restoreRelations::match(const LHCb::Particle* p1,const LHCb::Particle* p0) {
  std::vector<const LHCb::Particle*> t1=getTree(p1);
  std::vector<const LHCb::Particle*> t0=getTree(p0);
  if( t1.size() != t0.size() )return false;
  for( std::vector<const LHCb::Particle*>::const_iterator i1=t1.begin();t1.end()!=i1;i1++){
    bool ok = false;
    for( std::vector<const LHCb::Particle*>::const_iterator i0=t0.begin();t0.end()!=i0;i0++){
      const LHCb::ProtoParticle* pp1=(*i1)->proto();
      const LHCb::ProtoParticle* pp0=(*i0)->proto();
      if( pp1 == pp0 ){ok=true;break;}
    }
    if( !ok) return false;
  }
  return true;
}

StatusCode restoreRelations::execute() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;

  setFilterPassed(true);  // Mandatory. Set to true if event is accepted.

  // loop over ancestors

  const LHCb::Particle::Range parts = particles();
  for(LHCb::Particle::Range::const_iterator ipart = parts.begin();ipart != parts.end(); ++ipart){
    const LHCb::Particle* part=*ipart;
    std::string newLoc =  part->parent()->registry()->identifier() ;
    boost::replace_all( newLoc, "/Particles","" );
    newLoc += "/FlavourTags";
    //info() << "Checking FlavourTag location at "<<newLoc<<endmsg;
    LHCb::FlavourTags* newTags = nullptr;
    if( exist<LHCb::FlavourTags>(newLoc,IgnoreRootInTES)  ){
      newTags=get<LHCb::FlavourTags>(newLoc,IgnoreRootInTES);
    }else{      
      //info() << "creating new FlavourTag location at "<<newLoc<<endmsg;
      newTags=new LHCb::FlavourTags();
      put(newTags,newLoc,IgnoreRootInTES);
    }
    const LHCb::Particle* ancestor=nullptr;
    bool pass=false;
    std::string loc="";
    
    for( std::vector<std::string>::iterator it=m_origins.begin();it!=m_origins.end();it++){
      const LHCb::Particle::Range ancestors = getIfExists<LHCb::Particle::Range>(*it) ;
      if(   ancestors.empty() )continue;
      for(LHCb::Particle::Range::const_iterator iancestor = (ancestors).begin();iancestor != (ancestors).end(); ++iancestor){
        if( match( part,*iancestor) ){
          if( part == *iancestor)pass=true;
          ancestor = *iancestor;
          loc=*it;
          break;
        } 
      }
      if(nullptr!=ancestor)break;
    }    
    if( nullptr == ancestor )
      counter("Origin not found")+=1;
    else if( pass )
      counter("Original input - Nothing to restore") += 1;
    else{
      debug() << "Ancestor has been found in " << loc << endmsg;
      counter("Origin match")+=1;
      
      // ... check BTagging relations
      std::string tagLoc=loc;
      boost::replace_all( tagLoc, "/Particles", "/FlavourTags" );
      LHCb::FlavourTags* tags = getIfExists< LHCb::FlavourTags > (tagLoc);
      if( !tags )counter("No FlavourTags relations")+=1;
      else{
        std::vector<LHCb::FlavourTag*> list;
        bool check=false;
        for(LHCb::FlavourTags::const_iterator it = tags->begin(); it != tags->end(); ++it) {
          if( ancestor != (**it).taggedB()) continue;
          const LHCb::FlavourTag* theTag = *it;
          LHCb::FlavourTag* newTag= new LHCb::FlavourTag( *theTag );
          newTag->setTaggedB(part);
          newTags->insert(newTag);
          //info()<<"inserting newTag in " << newLoc << endmsg;
          check=true;
        }
        if(check)counter("Restored FlavourTags")+=1;
        else counter("Failed restoring FlavourTags")+=1;
        // ... restore
      }
      
      // ... check BestPV relations
      std::string pvLoc=loc;
      std::string stream=rootInTES();
      boost::replace_all( stream, "/Event", "" );
      boost::replace_all( stream, "/", "" );
      boost::replace_all( pvLoc, "/Particles", "/BestPV_"+stream+"_P2PV" );
      Particle2Vertex::Table* table = getIfExists<Particle2Vertex::Table>(pvLoc);
      if( ! table )counter("No P2PV relations")+=1;
      else{
        const Particle2Vertex::Table::Range pvrange = table->relations(ancestor);
        for(Particle2Vertex::Table::Range::iterator ipv = pvrange.begin();ipv != pvrange.end(); ++ipv){
          const LHCb::VertexBase* pv = (LHCb::VertexBase*)*ipv;
          table->relate(*ipart,pv).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
        }
        counter("Restored P2PVs")+=1;
      } 
    }
  }
  return StatusCode::SUCCESS;
}
