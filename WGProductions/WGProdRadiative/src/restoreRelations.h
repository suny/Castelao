/*****************************************************************************\
 * (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/


#ifndef RESTORERELATIONS_H 
#define RESTORERELATIONS_H 1

// Include files 
// from DaVinci.
#include "Kernel/DaVinciAlgorithm.h"
#include "Event/Particle.h"

/** @class restoreRelations restoreRelations.h
 *  
 *
 *  @author Olivier Deschamps
 *  @date   2016-09-01
 *  
 *  
 *  \brief 
 *  For the Radiative WG ntuple production
 *  Matching a cloned B (SubstitutePID) to the original stripped candidate and the original relations (BTagging, bestPV, relInfo ...) are associated to the clone
 *
 *  Related: https://its.cern.ch/jira/browse/LHCBPS-1652
 *
 *
 * @mod  Pere Gironella Gironell (2019-06-25)
 *  
 */
class restoreRelations : public DaVinciAlgorithm {
public: 
  /// Standard constructor
  restoreRelations( const std::string& name, ISvcLocator* pSvcLocator );

  virtual StatusCode execute   () override;    ///< Algorithm execution

  bool match(const LHCb::Particle* p1,const LHCb::Particle* p0);
  std::vector<const LHCb::Particle*> getTree( const LHCb::Particle* P );
    

protected:

private:
  std::vector<std::string> m_origins;
};
#endif // RESTORERELATIONS_H
