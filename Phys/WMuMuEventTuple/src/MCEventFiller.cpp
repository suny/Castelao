/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

  // Include files
#include <algorithm>
#include <string>

#include "Event/MCParticle.h"
#include "LoKi/ParticleProperties.h"
#include "Kernel/ParticleProperty.h"
#include "Math/LorentzRotation.h"
#include "Math/Boost.h"

#include "GaudiKernel/SystemOfUnits.h"

// from LHCb

#include "LoKi/PhysKinematics.h"
#include "WMuMu/MomentumVector.h"
#include "WMuMu/MCEventInfo.h"

namespace
{
  template<typename Float>
  inline Float deltaPhi( const Float& lhs, const Float& rhs )
  {
    Float dphi = lhs - rhs ;
    if( dphi > M_PI )       dphi -= 2*M_PI ;
    else if( dphi < -M_PI ) dphi += 2*M_PI ;
    return dphi ;
  }
  
  struct JetWithDist
  {
    double dR2 ;
    const LHCb::Particle* jet  ;
    bool operator<(const JetWithDist& rhs) const { return dR2 < rhs.dR2 ; }
    JetWithDist( double _dR2, const LHCb::Particle* _jet) :
      dR2( _dR2), jet(_jet) {}
  } ;
  
  void fillMCVirtualJetInfo( WMuMu::MCVirtualJet& jet,
			     const std::vector<Gaudi::LorentzVector>& p4s )
  {
    // first just compute the total p4
    Gaudi::LorentzVector p4tot =
      std::accumulate( std::begin(p4s), std::end(p4s),Gaudi::LorentzVector()  ) ;
    jet.MomentumVector::operator=(WMuMu::MomentumVector{ p4tot }) ;
    
    // now store some info about the 'size' of this jet:
    // - RMS of R
    // - R to get 90 or 100% of jet energy
    const double jetphi = p4tot.Phi() ;
    const double jeteta = p4tot.Eta() ;
    double sumw(0),sumdeta2(0), sumdphi2(0) ;
    typedef std::pair<double,Gaudi::LorentzVector> R2WithP4 ;
    std::vector<R2WithP4> sortedp4s ;
    sortedp4s.reserve(p4s.size()) ;
    jet.numdaughters = 0 ;
    for( const auto& p4 : p4s ) {
      // don't count anything with an energy less than 100 MeV. maybe we should also introduce an eta cut here.
      if( p4.E() > 100* Gaudi::Units::MeV ) {
	++jet.numdaughters ;
	double deta = p4.Eta() - jeteta ;
	double dphi = deltaPhi(p4.Phi(),jetphi) ;
	double weight = p4.E() ;
	sumw += weight ;
	sumdeta2 += weight*deta*deta ;
	sumdphi2 += weight*dphi*dphi ;
	double dR = std::sqrt(deta*deta+dphi*dphi) ;
	sortedp4s.push_back(std::make_pair(dR,p4) ) ;
      }
    }
    jet.width = std::sqrt( (sumdeta2+sumdphi2)/sumw )  ;

    // sort the p4s in distance to the center
    std::sort(sortedp4s.begin(), sortedp4s.end(),[](const R2WithP4&left, const R2WithP4&right) {
	return left.first < right.first;
      });
    
    jet.maxdR_100 = jet.maxdR_90 = 0 ;
    double sumE(0), sumE5(0) ;
    for( const auto& p4 : sortedp4s ) {
      double dR = p4.first ;
      if( jet.maxdR_100 < dR ) jet.maxdR_100 = dR ;
      if( jet.maxdR_90 < dR && sumE < 0.9*p4tot.E() ) jet.maxdR_90 = dR ;
      sumE += p4.second.E() ;
      if( dR < 0.5 ) sumE5 += p4.second.E() ;
    }
    jet.Efrac05 =  float(sumE5/sumE) ;
  }
  
  double bisectByThrust( const std::vector<Gaudi::LorentzVector>& daup4s,
			 std::vector<Gaudi::LorentzVector>& jet1p4s,
			 std::vector<Gaudi::LorentzVector>& jet2p4s )
  {
    // return value is the thrust
    double thrust(0) ;
    // 1. take all duaghters of p
    // 2. transform fourvectors to cms
    // 3. compute a trust axis
    // 4. use that to divide the daughter fourvectors in two hemispheres
    // 5. add them up in the lab frame to get two jets
    
    // transform to cms
    Gaudi::LorentzVector p4tot =
      std::accumulate( std::begin(daup4s), std::end(daup4s),Gaudi::LorentzVector()  ) ;
    ROOT::Math::Boost boost(  p4tot.BoostToCM( )  ) ;
    ROOT::Math::LorentzRotation lrot( boost ) ;

    // transform the daughter vectors and initialize the thrustvector with the maximum momentum found
    std::vector<Gaudi::XYZVector> daup3scms ;
    Gaudi::XYZVector thrustaxis ;
    double maxmom(0), scalarmomsum(0) ;
    Gaudi::XYZVector totp3cms ;
    for( const auto& daup4 : daup4s ) {
      Gaudi::XYZVector daup3cms = (lrot * daup4).Vect() ;
      daup3scms.push_back( daup3cms ) ;
      double mom =  daup3cms.R() ;
      scalarmomsum += mom ;
      if( mom > maxmom ) {
        thrustaxis = daup3cms  ;
        maxmom = mom ;
      }
      totp3cms += daup3cms ;
    }
    thrustaxis /= thrustaxis.R() ;

    // compute the thrust, update the thrustvector, until converged ...
    int N = daup4s.size() ;
    std::vector<int> signs( N, 1 ) ;
    thrust = 0 ; 
    bool converged = false ;
    for(int iter=0; iter<10 && !converged; ++iter) {
      Gaudi::XYZVector p3sum ;
      double thisthrust=0 ;
      for(int i=0; i<N; ++i) {
        double pinthrust = thrustaxis.Dot( daup3scms[i] )  ;
        signs[i] = ( pinthrust > 0) ? 1 : -1 ;
        p3sum += signs[i]*daup3scms[i] ;
        thisthrust += signs[i] * pinthrust ;
      }
      converged = iter>0 && std::abs(thisthrust/thrust-1)<0.01 ;
      //std::cout << "iter, thrust, thisthrust: " << N << " " << iter << " " << thrust << " " << thisthrust << std::endl ;
      thrust = thisthrust ;
      thrustaxis = p3sum.Unit() ;
    }
    thrust /= scalarmomsum ;
    // okay, we have the signs: divide the daughters into two jets
    jet1p4s.clear() ;
    jet2p4s.clear() ;
    for(int i=0; i<N; ++i) 
      if( signs[i] > 0 ) 
        jet1p4s.push_back(daup4s[i]) ;
      else 
	jet2p4s.push_back(daup4s[i]) ;
    return thrust ;
  }  

  

  
  void collectStableParticles( const LHCb::MCParticle& head,
			       std::vector< const LHCb::MCParticle* >& stableparticles) 
  {
    const std::vector<unsigned int> stablepids = {11,12,13,14,16,22,211,130,321,2212,2122} ;
    if( head.endVertices().empty()  ||
	(std::find( stablepids.begin(), stablepids.end(),std::abs(head.particleID().pid()) ) !=
	 stablepids.end() ) ) {
      //auto pprop = LoKi::Particles::ppFromPID(head.particleID()) ;
      //if( (pprop && pprop->ctau() > 1 * Gaudi::Units::mm ) ||
      //head.endVertices().empty() ) {
      stableparticles.push_back(&head) ;
    } else {
      for( const auto& v : head.endVertices() ) {
	for( const auto& dau : v->products() ) {
	  collectStableParticles(*dau,stableparticles) ;
	}
      }
    }
  }
  
  void collectStableParticles( const LHCb::MCVertex& head,
			       std::vector< const LHCb::MCParticle* >& stableparticles) 
  {
    for( const auto& dau : head.products() ) 
      collectStableParticles(*dau,stableparticles) ;
  }
}

void fillMCVirtualJetInfo(const LHCb::MCParticles& mcparticles,
			  WMuMu::MCEventInfo& mcevent)
{
  // let's fill some MC truth info, if we can
  // locate the neutrino
  //const LHCb::MCParticle* nuR(0) ;

  // okay, complicated. we need this to work both for Elena and for
  // Igor.  it turns out that H(36)->2 vertices -> 2x2
  // b-particles. The righthanded neutrino has only one end vertex,
  // with the last particle being the muon.
  
  std::vector<const LHCb::MCVertex*> selectedvertices ;
  for( const auto& mcp : mcparticles) {
    if( std::abs(mcp->particleID().pid())==9900014 ||
	mcp->particleID().pid()==36 ) {
      for( const auto& v : mcp->endVertices() )
	selectedvertices.push_back( v ) ;
    }
    // if( std::abs(mcp->particleID().pid())==9900014 ) {
    //   std::cout << "neutrino" << std::endl ;
    //   for( const auto& v : mcp->endVertices() ) {
    // 	std::cout << "endvertex" << std::endl ;
    // 	for( const auto& dau : v->products() ) {
    // 	  std::cout << "daughter: " << dau->particleID().pid() << std::endl ;
    // 	}
    //   }
    // }
  }

  for( const auto& mcp : selectedvertices) {
        
    // locate the righthanded neutrino or the vpion
    std::vector<const LHCb::MCParticle*> stabledaughters ;
    collectStableParticles( *mcp, stabledaughters ) ;
    if( std::abs(mcp->mother()->particleID().pid()) == 9900014 ) {
      // so the trouble is that for the neutrino the last daughter is
      // the muon (or electron). so, we need to exclude just the last
      // particle? admittedly, it is a bit error prone ...
      if(!stabledaughters.empty()) {
	//std::cout << "Last daughter: " << stabledaughters.back()->particleID().pid() << std::endl ;
	stabledaughters.pop_back() ;
      }
    }
    
    if( !stabledaughters.empty() ) {
      
      // const auto pp = LoKi::Particles::ppFromPID(mcp->particleID()) ;
      // std::string name = pp ? pp->name() : "unknown" ;
      // std::cout << "found nu-right: " << mcp->particleID() << " "
      // 	  << name << " "
      // 	  << mcp->momentum().M() << " " << mcp->momentum() << std::endl ;
      //collect all the daughters of the dijet system
      Gaudi::LorentzVector dijetp4;
      std::vector<Gaudi::LorentzVector> dijetdaughters ;
      for( const auto& dau: stabledaughters ) {
	dijetdaughters.push_back( dau->momentum() ) ;
	dijetp4 += dau->momentum() ;
      }
      
      WMuMu::MCVirtualDiJet mcdijet ;
      mcdijet.MomentumVector::operator=(dijetp4) ;
      mcdijet.pid  = mcp->mother()->particleID().pid() ;
      double origtime = mcp->mother()->originVertex() ? mcp->mother()->originVertex()->time() : 0 ;
      mcdijet.ctau = mcp->time() - origtime ;
      
      // reconstruct the thrust axis etc.
      std::vector<Gaudi::LorentzVector> jet1p4s, jet2p4s ;
      mcdijet.thrust = bisectByThrust(dijetdaughters,jet1p4s,jet2p4s) ;
      fillMCVirtualJetInfo( mcdijet.jet1, jet1p4s ) ;
      fillMCVirtualJetInfo( mcdijet.jet2, jet2p4s ) ;
      mcevent.virtualdijets.push_back( mcdijet ) ;
    }
  }
}
