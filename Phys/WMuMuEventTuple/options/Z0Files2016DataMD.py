###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import EventSelector

import glob
files = []
files += glob.glob('/data/bfys/wouterh/Z0/2016/StrippingZ02MuMu.MD.*.dst')
input =  [ 'PFN:%s' % i for i in files]

from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles( input, clear = True )
print input

from Configurables import DaVinci
DaVinci().DataType = "2016"
DaVinci().Simulation = False
DaVinci().PrintFreq = 100

#IOHelper('ROOT').inputFiles([
#    'PFN:/data/bfys/wouterh/Z02015/zmumu.md.all.dst',
#    'PFN:/data/bfys/wouterh/Z02015/zmumu.mu.all.dst'
#    ], clear=True )
#from Configurables import IODataManager
#IODataManager().AgeLimit = 2
