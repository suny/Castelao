###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


from LHCbKernel.Configuration import LHCbConfigurableUser, DEBUG, VERBOSE
from Gaudi.Configuration import *
from DecayTreeTuple.Configuration import *
from Configurables import DaVinci, PrintDecayTree, GaudiSequencer, CombineParticles
from PhysSelPython.Wrappers import AutomaticData, SelectionBase, Selection, SelectionSequence
from Configurables import FilterDesktop


# global function to find a selection in the list of known particles
from StandardParticles import selections as StandardSelections


###############################################################################
# Import the necessary packages
###############################################################################

from Configurables import HltParticleFlow, HltJetBuilder
from JetAccessories.ParticleFlow_Config import ParticleFlowConf
from JetAccessories.JetMaker_Config import JetMakerConf#

###############################################################################
# Configure the jet algorithm and validation code.
###############################################################################

# Invoke the particle flow algorithm with the standard configuration
ParticleFlow = ParticleFlowConf('PF2')

DaVinci().UserAlgorithms += ParticleFlow.algorithms

HltPF                     = HltParticleFlow()
HltPF.Inputs       = [
        ['Particle',       'particle', 'Phys/StdLooseResolvedPi0/Particles'],
        ['Particle',       'particle', 'Phys/StdLooseMergedPi0/Particles'],
        ['Particle',       'particle', 'Phys/StdKs2PiPiLL/Particles'],
        ['Particle',       'particle', 'Phys/StdKs2PiPiDD/Particles'],
        ['Particle',       'particle', 'Phys/StdLambda2PPiLL/Particles'],
        ['Particle',       'particle', 'Phys/StdLambda2PPiDD/Particles'],
        ['ProtoParticle',  'best',     'Rec/ProtoP/Charged'],
        ['ProtoParticle',  'gamma',     'Rec/ProtoP/Neutrals'],
]

HltPF.Output       = 'Phys/HltPF/Particles'
HltPF.ProBestNames = ['mu+', 'e+', 'p+', 'K+', 'pi+']
HltPF.ProBestKeys  = [701,   700,  704,  703,  702]
HltPF.ProBestMins  = [0.5,   0.5,  0.5,  0.5,  0.5]
HltPF.SprPid = -22
HltPF.EcalBest = True
HltPF.SprRecover = False
HltPF.TrkLnErrMax = 10
HltPF.TrkUpErrMax = 10
HltPF.TrkDnErrMax = 10

# Reconstruct the the jets using the particle flow objects created by HltPF

HltJet = HltJetBuilder("HLTJets")
HltJet.Inputs = ["Phys/HltPF/Particles"]
HltJet.JetEcPath = ""
HltJet.JetVrt = False

# Add both algorithms to the DaVinci User Algorithms
DaVinci().UserAlgorithms += [HltPF, HltJet]

###################################################


## for some reason, I cannot put this in the constructor of my selection class
#from JetAccessories.JetMaker_Config import JetMakerConf
#JetMaker = JetMakerConf( "StdPFJets", 
#                         JetEnergyCorrection = True, 
#                         JetIDCut = False, PtMin = 5000., R = 0.5,
##                         listOfParticlesToBan = ["Phys/MediumHardMuons/Particles"],
##                         PFTypes = ['ChargedHadron','Muon','Electron','Photon','Pi0','HCALNeutrals','NeutralRecovery','V0','BadPhoton','IsolatedPhoton']
#PFTypes = ['ChargedHadron','Muon','Electron','Photon','Pi0','HCALNeutrals','V0','BadPhoton','IsolatedPhoton']
#    )
#
## add the jet selection
## Use standard particle flow, but redefine input for the jets
#jetseq = GaudiSequencer("JetSeq",Members = JetMaker.algorithms, IgnoreFilterPassed = True )

#####################################################################
#
# Select Jets with at least one long track
#
#####################################################################

#SelectedJetsFilter = FilterDesktop("SelectedJets",
#                                       Preambulo = [ "from DisplVertices.JetFunctions import *" ],
#                                                #Code = "(( 1 <= NINTREE( ISLONG & (PT>1.2*GeV))) & (JMPF#<0.8) & (JCPF>0.1) & (JMPT > 1200))",
#                                                #Code = "(( 1 <= NINTREE( ISLONG & (PT>1.2*GeV))) & (JCPF#>0.1) & (JMPT > 1200) & (PT > 10*GeV) )",
#                                                Code = "( (JCPF>0.1) & (JMPT > 1200) & (PT > 10*GeV) )",
#                                                #Code = "( 1 <= NINTREE( ISLONG & (PT>1.2*GeV)))",
#                                                #Code = "( 1 <= NINTREE( (PT>1.2*GeV) ) )",
#                                       Inputs = ["Phys/StdPFJets/Particles"] )
#jetseq.Members.append(SelectedJetsFilter)
#JetSel = SelectionBase(algorithm=jetseq,
#                           outputLocation ="Phys/SelectedJets/Particles",
#                           requiredSelections=[])
        
def removeAllButNth( String, Character, N):
    newstring = String.replace(Character,' ',N-1)
    pos=newstring.find(Character)
    newstring = newstring[0:pos+1] + newstring[pos+1:].replace(Character,' ')
    return newstring

######################################################################
#
# Define the PreFilters
#
######################################################################

# to be replaced with jet triggers!
from Configurables import LoKi__HDRFilter
line = "WMuLine"
stripfilter = LoKi__HDRFilter( 'StripPassFilter', 
                               Code="( "
#                               "HLT_PASS('Stripping"+line+"Decision')"
                               "HLT_PASS('StrippingWMuLineDecision')"
                               "| HLT_PASS('StrippingWeLineDecision')"
                               "| HLT_PASS('StrippingLowMultDiMuonLineDecision')"
                               "| HLT_PASS('StrippingA1MuMuLineDecision')"
                               "| HLT_PASS('StrippingA1MuMuA1MuMuSameSignLineDecision')"
                               "| HLT_PASS('StrippingA2MuMuLineDecision')"
                               "| HLT_PASS('StrippingA2SameSignLineDecision')"
                               "| HLT_PASS('StrippingDY2MuMuLine2HltDecision')"
                               "| HLT_PASS('StrippingDY2MuMuLine3Decision')"
                               "| HLT_PASS('StrippingDY2MuMuLine4Decision')"
                               "| HLT_PASS('StrippingZ02MuMuLineDecision')"
                               "| HLT_PASS('StrippingDY2eeLine3')"
                               "| HLT_PASS('StrippingDY2eeLine4')"
                               ")"
                               , Location="/Event/Strip/Phys/DecReports" )
#stripfilter.OutputLevel = 1

#####################################################################
#
# Configure DaVinci
#
######################################################################
from Configurables import DaVinci
#DaVinci().EventPreFilters = [ stripfilter ]
#DaVinci().EventPreFilters = [ ]
DaVinci().DataType = "2016"
DaVinci().EvtMax = -1
DaVinci().Simulation = False

#GaudiSequencer("JetSeq").Members.insert(0, stripfilter )
if os.getenv('EVTMAX'):
    DaVinci().EvtMax = int(os.getenv('EVTMAX','-1'))

# add the jet reco to the DaVinci useralgs
    
#DaVinci().UserAlgorithms += [ jetseq ]
    
# add the MC Jet Matching -- CVS: CHANGED FOR LOW MASS JETS
if DaVinci().Simulation:
    from Configurables import LoKi__MCJetMaker
    from GaudiConf.Manipulations import addPrivateToolFromString
    from Configurables import MCJets2JetsAlg
    matchAlg = MCJets2JetsAlg()
    matchAlg.JetsBLocation = "Phys/SelectedJets/Particles"
    matchAlg.JetsBLocation = "Phys/StdPFJets/Particles"
    matchAlg.JetsALocation = "Phys/MCJetMakerJets/Particles"
    matchAlg.OutputTable = "Relations/Phys/MCJets2Jets"
    matchAlg.OutputInverseTable = "Relations/Phys/Jets2MCJets"
    matchTool = addPrivateToolFromString(matchAlg, "LoKi::MCJets2Jets/MCJ2J")
    matchAlg.Jets2Jets = matchTool.getFullName()
    from JetAccessoriesMC.MCJetMaker_Config import MCJetMakerConf
    MCJetMaker = MCJetMakerConf( "MCJetMaker",
                                 PtMin = 5000.,
                                 R = 1.3, # 1.3 to 1.5
                                 MotherCut = "( MCABSID == 'nu_Rmu' )",
                                 SimpleAcceptance = True, 
                                 OutputTableMC = "Relations/Phys/MCJets2MCParticles", 
                                 SaveMotherOnly = True,
                                 algtype = "Cambridge" # Cambridge-Aachen algorithm instead of default anti-kt )
    #                             ToBanCut = "GNONE" )
    GaudiSequencer("JetSeq").Members += MCJetMaker.algorithms + [matchAlg]
    
# let's try this now
from Configurables import WMuMuTupleMaker
wmumutuple = WMuMuTupleMaker( JetLocation = "Phys/HltPF/Particles",
#wmumutuple = WMuMuTupleMaker( JetLocation = "Phys/StdPFJets/Particles",
                                  #JetSel.outputLocation(),
                              OutputFileName = "JetTuple.root",
                              StoreAllEvents = True)

# add this to the end of a filter sequence, to save time
DaVinci().UserAlgorithms += [  wmumutuple ]
# for MC add it to the end of the LumiSeq, such that it runs for every event
if DaVinci().Simulation:
    GaudiSequencer("LumiSeq").Members += [ wmumutuple ]

#from Configurables import DecodeVeloRawBuffer
#DecodeVeloRawBuffer("createBothVeloClusters").OutputLevel = 2

inputfiles = ['/eos/lhcb/user/i/ikostiuk/00059560_00000001_1.bhadroncompleteevent.dst']
from Configurables import EventSelector
EventSelector().Input   = [
"DATAFILE='%s' TYP='POOL_ROOTTREE' OPT='READ'" % f for f in inputfiles]
