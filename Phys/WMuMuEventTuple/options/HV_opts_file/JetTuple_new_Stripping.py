###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from LHCbKernel.Configuration import LHCbConfigurableUser, DEBUG, VERBOSE
from Gaudi.Configuration import *
from DecayTreeTuple.Configuration import *
from Configurables import DaVinci, PrintDecayTree, GaudiSequencer, CombineParticles
from PhysSelPython.Wrappers import AutomaticData, SelectionBase, Selection, SelectionSequence
from Configurables import FilterDesktop
from GaudiKernel.SystemOfUnits import GeV

# global function to find a selection in the list of known particles
from StandardParticles import selections as StandardSelections

###############################################################################
# Import the necessary packages
###############################################################################

from Configurables import HltParticleFlow, HltJetBuilder
from JetAccessories.ParticleFlow_Config import ParticleFlowConf
from JetAccessories.JetMaker_Config import JetMakerConf

###############################################################################
# Configure the jet algorithm and validation code.
###############################################################################

from JetAccessories.HltJetConf import HltParticleFlowConf, HltJetBuilderConf
inputs = ['Photons', 'ResolvedPi0s', 'MergedPi0s', 'Ks', 'Lambda', 'ChargedProtos',
'VeloProtos', 'EcalClusters', 'HcalClusters', 'EcalMatch', 'HcalMatch',
'PrimaryVtxs']
pf = HltParticleFlowConf('pf_hlt', SprRecover = True, _inputs = inputs )
jb = HltJetBuilderConf('jb_hlt', pf.getOutputLocation(), JetEcPath = '', JetPtMin = 5*GeV, JetVrt = False)
DaVinci().UserAlgorithms += [pf.getSeq()]
DaVinci().UserAlgorithms += [jb.getSeq()]

#####################################################################
#
# Select Jets with at least one long track
#
#####################################################################

#SelectedJetsFilter = FilterDesktop("SelectedJets",
#                                       Preambulo = [ "from DisplVertices.JetFunctions import *" ],
#                                                #Code = "(( 1 <= NINTREE( ISLONG & (PT>1.2*GeV))) & (JMPF#<0.8) & (JCPF>0.1) & (JMPT > 1200))",
#                                                #Code = "(( 1 <= NINTREE( ISLONG & (PT>1.2*GeV))) & (JCPF#>0.1) & (JMPT > 1200) & (PT > 10*GeV) )",
#                                                Code = "( (JCPF>0.1) & (JMPT > 1200) & (PT > 10*GeV) )",
#                                                #Code = "( 1 <= NINTREE( ISLONG & (PT>1.2*GeV)))",
#                                                #Code = "( 1 <= NINTREE( (PT>1.2*GeV) ) )",
#                                       Inputs = ["Phys/StdPFJets/Particles"] )
#jetseq.Members.append(SelectedJetsFilter)
#JetSel = SelectionBase(algorithm=jetseq,
#                           outputLocation ="Phys/SelectedJets/Particles",
#                           requiredSelections=[])
        
def removeAllButNth( String, Character, N):
    newstring = String.replace(Character,' ',N-1)
    pos=newstring.find(Character)
    newstring = newstring[0:pos+1] + newstring[pos+1:].replace(Character,' ')
    return newstring

######################################################################
#
# Define the PreFilters
#
######################################################################

from Configurables import LoKi__HDRFilter
lines = ['StrippingHltQEEJetsDiJetLine', 
         'StrippingHltQEEJetsDiJetMuMuLine', 
         'StrippingHltQEEJetsDiJetSVLine', 
         'StrippingHltQEEJetsDiJetSVMuLine']
         #'StrippingHltQEEJetsDiJetSVSVLine']

_code = "( "
for line in lines:
  _code += "HLT_PASS('"+line+"Decision') | "
_code += "HLT_PASS('StrippingHltQEEJetsDiJetSVSVLineDecision') )"
#line = "WMuLine"

stripfilter = LoKi__HDRFilter( 'StripPassFilter', Code = _code, Location = "/Event/Strip/Phys/DecReports" )
#stripfilter = LoKi__HDRFilter( 'StripPassFilter', 
#                               Code="( "
##                               "HLT_PASS('Stripping"+line+"Decision')"
#                               "HLT_PASS('StrippingWMuLineDecision')"
#                               "| HLT_PASS('StrippingWeLineDecision')"
#                               "| HLT_PASS('StrippingLowMultDiMuonLineDecision')"
#                               "| HLT_PASS('StrippingA1MuMuLineDecision')"
#                               "| HLT_PASS('StrippingA1MuMuA1MuMuSameSignLineDecision')"
#                               "| HLT_PASS('StrippingA2MuMuLineDecision')"
#                               "| HLT_PASS('StrippingA2SameSignLineDecision')"
#                               "| HLT_PASS('StrippingDY2MuMuLine2HltDecision')"
#                               "| HLT_PASS('StrippingDY2MuMuLine3Decision')"
#                               "| HLT_PASS('StrippingDY2MuMuLine4Decision')"
#                               "| HLT_PASS('StrippingZ02MuMuLineDecision')"
#                               "| HLT_PASS('StrippingDY2eeLine3')"
#                               "| HLT_PASS('StrippingDY2eeLine4')"
#                               ")"
#                               , Location="/Event/Strip/Phys/DecReports" )
stripfilter.OutputLevel = 2

#####################################################################
#
# Configure DaVinci
#
######################################################################
from Configurables import DaVinci
DaVinci().DataType = "2016"
DaVinci().EvtMax = 1000
DaVinci().Simulation = False # True for MC
MCTruth    = False
PreFilters = False
if PreFilters:
  DaVinci().EventPreFilters = [ stripfilter ]

#GaudiSequencer("JetSeq").Members.insert(0, stripfilter) # Check this.
if os.getenv('EVTMAX'):
    DaVinci().EvtMax = int(os.getenv('EVTMAX','-1'))

# add the jet reco to the DaVinci useralgs
    
#DaVinci().UserAlgorithms += [ jetseq ]
    
# add the MC Jet Matching
if DaVinci().Simulation:
    from Configurables import LoKi__MCJetMaker
    from GaudiConf.Manipulations import addPrivateToolFromString
    from Configurables import MCJets2JetsAlg
    matchAlg = MCJets2JetsAlg()
    #matchAlg.JetsBLocation = "Phys/SelectedJets/Particles"
    #matchAlg.JetsBLocation = "Phys/StdPFJets/Particles"
    matchAlg.JetsBLocation = "Phys/jb_hlt/Particles"
    matchAlg.JetsALocation = "Phys/MCJetMakerJets/Particles"
    matchAlg.OutputTable = "Relations/Phys/MCJets2Jets"
    matchAlg.OutputInverseTable = "Relations/Phys/Jets2MCJets"
    matchTool = addPrivateToolFromString(matchAlg, "LoKi::MCJets2Jets/MCJ2J")
    matchAlg.Jets2Jets = matchTool.getFullName()
    from JetAccessoriesMC.MCJetMaker_Config import MCJetMakerConf
    MCJetMaker = MCJetMakerConf( "MCJetMaker", PtMin = 5000., R = 0.7, MotherCut = "( MCABSID == 'H_30' )", SimpleAcceptance = True, 
                                OutputTableMC = "Relations/Phys/MCJets2MCParticles", 
                                SaveMotherOnly = True
    #                             ToBanCut = "GNONE"
        )
    GaudiSequencer("JetSeq").Members += MCJetMaker.algorithms + [matchAlg]
    
# let's try this now
from Configurables import WMuMuTupleMaker
if DaVinci().Simulation and MCTruth:
  _JetLocation = "Phys/MCJetMakerJets/Particles"
else:
  _JetLocation = "Phys/jb_hlt/Particles"

wmumutuple = WMuMuTupleMaker( JetLocation = _JetLocation,
#				PFLocation = pf.outputLocation(), #check in the cpp file
                              OutputFileName = "JetTuple.root",
                              StoreAllEvents = True)

# add this to the end of a filter sequence, to save time
DaVinci().UserAlgorithms += [  wmumutuple ]
# for MC add it to the end of the LumiSeq, such that it runs for every event
if DaVinci().Simulation:
    GaudiSequencer("LumiSeq").Members += [ wmumutuple ]

#from Configurables import DecodeVeloRawBuffer
#DecodeVeloRawBuffer("createBothVeloClusters").OutputLevel = 2

inputfiles = ['/eos/lhcb/user/i/ikostiuk/00059560_00000001_1.bhadroncompleteevent.dst']
from Configurables import EventSelector
EventSelector().Input   = [
"DATAFILE='%s' TYP='POOL_ROOTTREE' OPT='READ'" % f for f in inputfiles]
