/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef ParticleListEntry_H
#define ParticleListEntry_H

namespace WMuMu
{

  class ParticleListHolder
  {
  public:
    

  } ;

  class ParticleListEntry
  {
  public:
    unsigned char listindex ;
    unsigned char particleindex ;
  } ;
  
}

#endif
