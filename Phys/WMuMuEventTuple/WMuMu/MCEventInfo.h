/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef WMUMU_MCEVENTINFO_H
#define WMUMU_MCEVENTINFO_H

#include "Particle.h"
#include "TLorentzVector.h"
#include <vector>

namespace WMuMu
{
  class MCParticle : public MomentumVector
  {
  public: 
    friend class ::WMuMuTupleMaker ;
    MCParticle() {}
    template<class LHCbMCParticle>
      MCParticle(const LHCbMCParticle& p) :
    MomentumVector(p.momentum()),
	pid(p.particleID().pid()) {}
  public:
    int pid ;
  } ;
  
  class MCVirtualJet : public MomentumVector
  {
  public:
    float maxdR_100 ; // cone size needed to get 100% of energy
    float maxdR_90 ;  // cone size needed to get 90% of energy
    float width ;
    float Efrac05 ;
    unsigned char numdaughters ;
  } ;
  
  class MCVirtualDiJet : public MomentumVector
  {
  public:
    //float dphi() const { return jet1.deltaPhi( jet2 ); }
    //float deta() const { return jet1.Eta() - jet2.Eta() ; }
  public:
    int pid ;
    float ctau ;
    float thrust ;
    MCVirtualJet jet1 ;
    MCVirtualJet jet2 ;
  } ;
  
  class MCEventInfo
  {
  public:
  MCEventInfo() :
    NumPV(0),NNumStrange(0),NNumCharm(0),NNumBottom(0) {}
    // compute angle between muons in rest frame of W
    double costhetaMuMu() const {
      TLorentzVector Np4 = N ;
      TVector3 Nframe = Np4.BoostVector() ;
      TLorentzVector wmuonp4prime = Wmuon ;
      wmuonp4prime.Boost( -Nframe) ;
      TLorentzVector nmuonp4prime = Nmuon ;
      nmuonp4prime.Boost( -Nframe) ;
      TVector3 wmuondir = wmuonp4prime.Vect().Unit() ;
      TVector3 nmuondir = nmuonp4prime.Vect().Unit() ;
      double costheta = wmuondir.Dot(nmuondir) ;
      return costheta ;
    }
    // decide if event is inside acceptance
    bool inAcceptance() const {
      const float wmuoneta = Wmuon.p4().Eta() ;
      const float nmuoneta = Nmuon.p4().Eta() ;
      const float xeta     = (W.p4() - Wmuon.p4()- Nmuon.p4()).Eta() ;
      return 2.0 < wmuoneta && wmuoneta < 5.0 && 2.0 < nmuoneta && nmuoneta < 5.0 && 2.0 < xeta && xeta < 5.0 ;
    }
    double mumumass() const {
      return (Wmuon.p4()+Nmuon.p4()).M() ;
    }

  public:
    unsigned char NumPV ;
    MCParticle W ;
    MCParticle N ;
    MCParticle Wmuon ;
    MCParticle Nmuon ;
    unsigned char NNumStrange ;
    unsigned char NNumCharm ;
    unsigned char NNumBottom ;
    std::vector<MCVirtualDiJet> virtualdijets ;
  } ;
}

#endif
