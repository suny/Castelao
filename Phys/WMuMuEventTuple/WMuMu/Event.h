/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef __TrackTupleMaker_TTMEVENT_H__
#define __TrackTupleMaker_TTMEVENT_H__

#include "JetParticle.h"
#include "TrackParticle.h"
#include "PrimaryVertex.h"
#include "CompositeParticle.h"
#include "MCEventInfo.h"
#include "EventInfo.h"
#include <vector>
#include "Common.h"

class MuMuPiTupleMaker ;
namespace WMuMu
{
  class Event
  {
  public:
    typedef std::vector<TrackParticle> MuonContainer ;
    typedef std::vector<JetParticle> JetContainer ;
    //typedef std::vector<Track> TrackContainer ;
    typedef std::vector<Particle> ParticleContainer ;
    typedef std::vector<TrackParticle> TrackParticleContainer ;
    typedef std::vector<PrimaryVertex> PVContainer ;
    
    Event() ;
    virtual ~Event() ;
    void clear() ;
    friend class ::WMuMuTupleMaker ;
    static Event* gInstance ;
    double cosThetaStarCS( int dau1, int dau2 ) const ;
  public:
    unsigned short    bxtype ;
    unsigned short    bxid ;
    unsigned long int eventnumber ;
    unsigned int      runnumber ;
    JetContainer jets ;
    TrackParticleContainer tracks ;
    PVContainer PVs ;
    //ParticleContainer particles ;
    //std::vector<WToMuMuXCandidate> WToMuNu ;
    MCEventInfo mctruth ;
    unsigned int index ; // index of event in job
    StrippingBits stripping ;
    L0Bits        L0 ;
    Hlt1Bits      hlt1 ;
    Hlt2Bits      hlt2 ;

    // some info from the particle flow
    MomentumVector p4tot ;
    MomentumVector p4track ;
    
    //TO ADD:
    //  - number SPD,OT,Velo hits
    //  - number of L0 SPD hits
    // From RecSummary
    unsigned short nLongTracks  ;
    unsigned short nDownstreamTracks ;
    unsigned short nUpstreamTracks  ;
    unsigned short nVeloTracks  ;
    unsigned short nTTracks ;
    unsigned short nBackTracks ;
    unsigned short nMuonTracks ;
    unsigned short nVeloClusters ;
    unsigned short nITClusters ;
    unsigned short nTTClusters ;
    unsigned short nOTClusters ;
    unsigned short nSPDhits ;
    bool hasRawEvent ;
    signed char bFieldPolarity ;
    // From L0DUReport
    unsigned short L0SumET[5] ;
    float beamlineX ;
    float beamlineY ;
    unsigned short L0nSPDhits{0} ;
    //ClassDef(Event,1) ;
  } ;

}

#endif
