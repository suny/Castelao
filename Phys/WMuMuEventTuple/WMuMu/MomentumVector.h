/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef WMM_LORENTZVECTOR_H
#define WMM_LORENTZVECTOR_H

#include "Common.h"
#include <cmath>
#include "TLorentzVector.h"

namespace WMuMu
{
  class MomentumVectorXYZM
  {
  public:
    friend class ::WMuMuTupleMaker ;
    
    // constructor
    MomentumVectorXYZM() : Px(0),Py(0),Pz(0),M(0) {}
    
    // destructor
    // virtual ~MomentumVector() ;
    
    // another constructor
    template<class LV>
      MomentumVectorXYZM( const LV& p4 ) :
    Px(p4.Px()),Py(p4.Py()),Pz(p4.Pz()),M(p4.M()) {}

    float P() const { return std::sqrt(Px*Px+Py*Py+Pz*Pz) ; }
    float Pt() const { return std::sqrt(Px*Px+Py*Py) ; }
    float Phi() const { return std::atan2( Py, Px ) ; }
    float Eta() const { 
      double p = std::sqrt(double(Px)*Px+double(Py)*Py+double(Pz)*Pz) ;
      return 0.5*std::log( (p+Pz)/(p-Pz) ) ;
    }
    double E() const { 
      return std::sqrt( double(Px*Px+Py*Py+Pz*Pz)+double(M*M) ) ;
    }
    
    operator TLorentzVector() const {
      return p4() ;
    }

    TLorentzVector p4() const {
      return TLorentzVector{Px,Py,Pz,E()} ;
    }

    float deltaPhi( const MomentumVectorXYZM& rhs ) const
    {
      float dphi = Phi() - rhs.Phi() ;
      if( dphi > M_PI )       dphi -= 2*M_PI ;
      else if( dphi < -M_PI ) dphi += 2*M_PI ;
      return dphi ;
    }
    
  public:
    float Px ;
    float Py ;
    float Pz ;
    //float E ;
    float M ;
    //ClassDef( MomentumVector, 1 ) ;
  } ;

 class MomentumVector
  {
  public:
    friend class ::WMuMuTupleMaker ;
    
    // constructor
    MomentumVector() : Pt(0),Phi(0),Pz(0),M(0) {}
    
    // another constructor
    template<class LV>
      MomentumVector( const LV& p4 ) :
    Pt(p4.Pt()),Phi(p4.Phi()),Pz(p4.Pz()),M(p4.M()) {}

    float P() const { return std::sqrt(double(Pt)*Pt+double(Pz)*Pz) ; }
    float Px() const { return Pt*cos(Phi) ; }
    float Py() const { return Pt*sin(Phi) ; }
    float Eta() const { 
      double p = std::sqrt(double(Pt)*Pt+double(Pz)*Pz) ;
      return 0.5*std::log( (p+Pz)/(p-Pz) ) ;
    }
    double E() const { 
      return std::sqrt( double(Pt*Pt+Pz*Pz)+double(M*M) ) ;
    }
    
    operator TLorentzVector() const {
      return p4() ;
    }

    TLorentzVector p4() const {
      return TLorentzVector{Px(),Py(),Pz,E()} ;
    }

    double deltaPhi( const MomentumVector& rhs ) const {
      double dphi = Phi - rhs.Phi ;
      if( dphi > M_PI )       dphi -= 2*M_PI ;
      else if( dphi < -M_PI ) dphi += 2*M_PI ;
      return dphi ;
    }

    double deltaEta( const MomentumVector& rhs ) const {
      return Eta() - rhs.Eta() ;
    }

    double deltaR( const MomentumVector& rhs ) const {
      double dphi = deltaPhi(rhs) ;
      double deta = deltaEta(rhs) ;
      return std::sqrt( dphi*dphi+deta*deta) ;
    }
    
  public:
    float Pt ;
    float Phi ;
    float Pz ;
    float M ;
    //ClassDef( MomentumVector, 1 ) ;
  } ;
 
}

#endif
