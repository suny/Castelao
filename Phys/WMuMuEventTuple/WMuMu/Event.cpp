/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event.h"
#include "WToMuMuXContainer.h"
#include "LinkDef.h"

#include <iostream>

// various things needed for persisted classes

//ClassImp(WMuMu::Event)
//ClassImp(WMuMu::MomentumVector)
//ClassImp(WMuMu::Particle)
//ClassImp(WMuMu::TrackParticle)

namespace WMuMu 
{
  Particle::~Particle() {}
  TrackParticle::~TrackParticle() {}
  //MomentumVector::~MomentumVector() {}
}


namespace WMuMu 
{
  Event* Event::gInstance = 0 ;

  // constructor
  Event::Event() 
    : index(0)
  {
    if(!gInstance) gInstance = this ;
  }
  
  // destructor
  Event::~Event() 
  {
    if(gInstance==this) gInstance = 0 ;
  }

  void Event::clear() 
  {
    runnumber = 0 ;
    eventnumber = 0 ;
    bxid = 0 ;
    bxtype = 0 ;
    jets.clear() ;
    tracks.clear() ;
    PVs.clear() ;
    //particles.clear() ;
    //WToMuNu.clear() ;
    stripping = 0 ;
    L0 = 0 ;
    hlt1 = 0 ;
    hlt2 = 0 ;

    p4tot = MomentumVector() ;
    p4track = MomentumVector() ;

    nLongTracks =0 ;
    nDownstreamTracks =0;
    nUpstreamTracks =0 ;
    nVeloTracks =0 ;
    nTTracks =0;
    nBackTracks =0;
    nMuonTracks =0;
    nVeloClusters =0;
    nITClusters =0;
    nTTClusters =0;
    nOTClusters =0;
    nSPDhits =0;
    hasRawEvent = false ;
    bFieldPolarity = 0 ;
    for(int i=0; i<5; ++i) L0SumET[i]=0 ;
    beamlineX = beamlineY = 0;
    L0nSPDhits = 0 ;
  } 

  double Event::cosThetaStarCS( int dau1, int dau2 ) const {
    TLorentzVector p4p = tracks[dau1].p4() ;
    TLorentzVector p4m = tracks[dau2].p4() ;
    if( tracks[dau1].TRACK_qOverP < 0 ) std::swap(p4p,p4m) ;
    TLorentzVector p4sum = p4p + p4m ;
    auto boostvector = - p4sum.BoostVector()  ;
    TLorentzVector p4mprime = p4m ;
    p4mprime.Boost( boostvector ) ;
    TVector3 p4mprimedir = p4mprime.Vect().Unit() ;

    const double Ebeam = 6.5e6 ;
    TLorentzVector protonA(0.,0.,Ebeam,Ebeam) ;
    protonA.Boost( boostvector ) ;
    TLorentzVector protonB(0.,0.,-Ebeam,Ebeam) ;
    protonB.Boost( boostvector ) ;
    TVector3 zdir = (0.5*(protonA.Vect().Unit() - protonB.Vect().Unit() )).Unit() ;
    return p4mprimedir.Dot( zdir ) ;
  }

  void WToMuMuXContainer::clear()
  {
    candidates.clear() ;
  }
  
  size_t WToMuMuXContainer::numSingleCandidate() const
  {
    size_t rc = 0 ;
    for(const auto& c : candidates)
      if( c.decaytype == WToMuMuXCandidate::MuJetRS ||
	  c.decaytype == WToMuMuXCandidate::MuJetWS ) ++rc ;
    return rc ;
  }

  const WToMuMuXCandidate* WToMuMuXContainer::bestSingleJetCandidate() const
  {
    // return a single jet candidate with the highest pT jet (hoping that there is only 1)
    const WToMuMuXCandidate* rc(0) ;
    for(const auto& c : candidates) {
      if( c.decaytype == WToMuMuXCandidate::MuJetRS ||
	  c.decaytype == WToMuMuXCandidate::MuJetWS ) 
	if(!rc || rc->Njet1 > c.Njet1 ) rc = &c ;
    }
    return rc ;
  }

  const WToMuMuXCandidate* WToMuMuXContainer::bestDiJetCandidate() const
  {
    // return a single jet candidate with the highest pT jet (hoping that there is only 1)
    const WToMuMuXCandidate* rc(0) ;
    for(const auto& c : candidates) {
      if( c.decaytype == WToMuMuXCandidate::MuDiJetRS ||
	  c.decaytype == WToMuMuXCandidate::MuDiJetWS ) 
	if(!rc || std::min(rc->Njet1,rc->Njet2) >  std::min(c.Njet1,c.Njet2) ) rc = &c ;
    }
    return rc ;
  }

  size_t lineIndex( const std::vector<std::string>& lines, const std::string& line )
  {
    auto it = std::find(lines.begin(),lines.end(),line) ;
    return it != lines.end() ? std::distance(lines.begin(),it) : lines.size() ;
  }
  
  ULong64_t createMask( const std::vector<std::string>& lines, const std::string& line )
  {
    auto it = std::find(lines.begin(),lines.end(),line) ;
    ULong64_t rc = it != lines.end() ? ULong64_t(1) << std::distance(lines.begin(),it) : 0 ;
    return rc ;
  }
  
  void printFiredLines( const std::vector<std::string>& lines, ULong64_t bits )
  {
    const unsigned char N = lines.size() ;
    for(unsigned char i=0; i<N; ++i) {
      ULong64_t thisbit = ULong64_t(1)<<i ;
      if( bits & thisbit )
	std::cout << lines[i] << std::endl ;
    }
  }
  
}
