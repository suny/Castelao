/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <GaudiKernel/IIncidentSvc.h>

#include <Event/RecVertex.h>

#include <LoKi/Random.h>

#include "PVMixer.h"

//-----------------------------------------------------------------------------
// Implementation file for class : PVMixer
//
// 2012-09-18 : Roel Aaij
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT(PVMixer)

//=============================================================================
StatusCode PVMixer::initialize()
{
   StatusCode sc = GaudiAlgorithm::initialize();
   if ( sc.isFailure() ) return sc;

   // grab IncidentSvc
   m_incidentSvc = service("IncidentSvc", true).as<IIncidentSvc>();
   if(!m_incidentSvc.isValid()) {
      fatal() << "Failed to get the IncidentSvc." << endmsg;
      return StatusCode::FAILURE;
   }
   m_incidentSvc->addListener(this, IncidentType::BeginInputFile);

   return StatusCode::SUCCESS;
}

//=============================================================================
StatusCode PVMixer::execute()
{
   LHCb::RecVertex::Range PVs = getIfExists<LHCb::RecVertex::Range>(m_pvInputLocation);

   // Clone input PVs
   LHCb::RecVertices* outputPVs = new LHCb::RecVertices;
   put(outputPVs, m_pvOutputLocation);
   for (const LHCb::RecVertex* PV : PVs) {
      outputPVs->insert(PV->clone());
   }

   // No PVs in the event, can't do anything.
   if (PVs.empty()) {
      setFilterPassed(false);  // Mandatory. Set to true if event is accepted.
      return StatusCode::SUCCESS;
   }

   // Select random PV to mix
   if (m_mixedVertices.size() <= m_waitEvents) {
      // Randomly select PV to mix
      LoKi::Random::Uniform rndm(0., double(PVs.size()));
      unsigned int index(rndm(0));
      // This will never happen, but let's protect anyway
      if (index == PVs.size()) --index;
      // Clone PV
      LHCb::RecVertex* clone = PVs.at(index)->clone();
      // Remove all track SmartRefs
      clone->clearTracks();
      // Set extra info to flag this is a mixed PV.
      clone->addInfo(m_infoKey, 1.);
      m_mixedVertices.emplace_back(clone);
   }

   if (m_waited >= m_waitEvents) {
      // Add mixed vertex to current event
      auto mixedPV = std::move(m_mixedVertices.front());
      m_mixedVertices.pop_front();
      outputPVs->insert(mixedPV.release());
   } else {
      ++m_waited;
   }

   setFilterPassed(true);  // Mandatory. Set to true if event is accepted.
   return StatusCode::SUCCESS;
}

//=============================================================================
void PVMixer::handle(const Incident&)
{
   // Delete PV clones
   m_mixedVertices.clear();
   m_waited = 0;
}
