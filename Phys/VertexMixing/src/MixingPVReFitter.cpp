/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// Event
#include <Event/VertexBase.h>

// boost
#include <boost/foreach.hpp>

// local
#include "MixingPVReFitter.h"

//-----------------------------------------------------------------------------
// Implementation file for class : MixingPVReFitter
//
// 2012-09-18 : Roel Aaij
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT(MixingPVReFitter)

//=============================================================================
// Initialize
//=============================================================================
StatusCode MixingPVReFitter::initialize(){
   StatusCode sc = GaudiTool::initialize();
   if (!sc) return sc;

   m_refitter = tool<IPVReFitter>(m_refitterName, parent());
   return sc;
}

//=============================================================================
// refit PV
//=============================================================================
StatusCode MixingPVReFitter::reFit(LHCb::VertexBase* PV) const {

   if (PV->hasInfo(m_infoKey)) {
      return StatusCode::SUCCESS;
   }
   return m_refitter->reFit(PV);
}

//=============================================================================
// remove track used for a LHCb::Particle and refit PV
//=============================================================================
StatusCode MixingPVReFitter::remove(const LHCb::Particle* part,
                                    LHCb::VertexBase* PV) const {
   // Do the refitting
   if (PV->hasInfo(m_infoKey)) {
      return StatusCode::SUCCESS;
   }
   return m_refitter->remove(part, PV);
}
