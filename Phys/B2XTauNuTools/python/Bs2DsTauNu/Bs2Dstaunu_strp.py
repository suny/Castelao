###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
from Configurables import (
    DaVinci,
    EventSelector,
    EventCountHisto,
    CheckPV
)
from Configurables import TrackScaleState as SCALER
from Configurables import TimingAuditor, SequencerTimerTool
from Configurables import EventTuple, TupleToolTrigger

# Global output level
outputLevel = INFO
isMC = SIMULATION 

# Run stripping on the MC
importOptions(
    "$B2XTAUNUTOOLSROOT/python/B2XTauNuTools/SelB2XTauNu_ConfigureStripping.py")

# SEQUENCER - As per the old .opts files.
SeqSelBs2Dstaunu = GaudiSequencer("SeqSelBs2Dstaunu")
SeqSelBs2Dstaunu.OutputLevel = outputLevel

# Events in
SeqSelBs2DstaunuInputCount = EventCountHisto("SeqSelBs2DstaunuInputCount")
SeqSelBs2DstaunuInputCount.OutputLevel = outputLevel
SeqSelBs2Dstaunu.Members.append(SeqSelBs2DstaunuInputCount)


# Events passing Selection
SeqSelBs2DstaunuAfterSelection = EventCountHisto(
    "SeqSelBs2DstaunuAfterSelection")
SeqSelBs2DstaunuAfterSelection.OutputLevel = outputLevel
SeqSelBs2Dstaunu.Members.append(SeqSelBs2DstaunuAfterSelection)

if isMC == True:
    importOptions(
        "$B2XTAUNUTOOLSROOT/python/Bs2DsTauNu/TupleBs2Dstaunu_MC.py")
else:
     importOptions(
         "$B2XTAUNUTOOLSROOT/python/Bs2DsTauNu/TupleBs2Dstaunu.py")

# Events filled to ntuple
SeqSelBs2DstaunuOutputCount = EventCountHisto("SeqSelBs2DstaunuOutputCount")
SeqSelBs2DstaunuOutputCount.OutputLevel = outputLevel
SeqSelBs2Dstaunu.Members.append(SeqSelBs2DstaunuOutputCount)
SeqSelBs2Dstaunu.OutputLevel = outputLevel

# SEQUENCER - As per the old .opts files.
SeqSelBs2DstaunuWS = GaudiSequencer("SeqSelBs2DstaunuWS")
SeqSelBs2DstaunuWS.OutputLevel = outputLevel

# Events in
SeqSelBs2DstaunuWSInputCount = EventCountHisto("SeqSelBs2DstaunuWSInputCount")
SeqSelBs2DstaunuWSInputCount.OutputLevel = outputLevel
SeqSelBs2DstaunuWS.Members.append(SeqSelBs2DstaunuWSInputCount)


# Events passing Selection
SeqSelBs2DstaunuWSAfterSelection = EventCountHisto(
    "SeqSelBs2DstaunuWSAfterSelection")
SeqSelBs2DstaunuWSAfterSelection.OutputLevel = outputLevel
SeqSelBs2DstaunuWS.Members.append(SeqSelBs2DstaunuWSAfterSelection)

if isMC == True:
    importOptions(
        "$B2XTAUNUTOOLSROOT/python/Bs2DsTauNu/TupleBs2DstaunuWS_MC.py")
else:
     importOptions(
         "$B2XTAUNUTOOLSROOT/python/Bs2DsTauNu/TupleBs2DstaunuWS.py")

# Events filled to ntuple
SeqSelBs2DstaunuWSOutputCount = EventCountHisto(
    "SeqSelBs2DstaunuWSOutputCount")
SeqSelBs2DstaunuWSOutputCount.OutputLevel = outputLevel
SeqSelBs2DstaunuWS.Members.append(SeqSelBs2DstaunuWSOutputCount)
SeqSelBs2DstaunuWS.OutputLevel = outputLevel


# SEQUENCER - As per the old .opts files.
SeqSelBs2DstaunuNonPhys = GaudiSequencer("SeqSelBs2DstaunuNonPhys")
SeqSelBs2DstaunuNonPhys.OutputLevel = outputLevel

# Events in
SeqSelBs2DstaunuNonPhysInputCount = EventCountHisto(
    "SeqSelBs2DstaunuNonPhysInputCount")
SeqSelBs2DstaunuNonPhysInputCount.OutputLevel = outputLevel
SeqSelBs2DstaunuNonPhys.Members.append(SeqSelBs2DstaunuNonPhysInputCount)


# Events passing Selection
SeqSelBs2DstaunuNonPhysAfterSelection = EventCountHisto(
    "SeqSelBs2DstaunuNonPhysAfterSelection")
SeqSelBs2DstaunuNonPhysAfterSelection.OutputLevel = outputLevel
SeqSelBs2DstaunuNonPhys.Members.append(SeqSelBs2DstaunuNonPhysAfterSelection)

if isMC == True:
    importOptions(
        "$B2XTAUNUTOOLSROOT/python/Bs2DsTauNu/TupleBs2DstaunuNonPhys_MC.py")
else:
     importOptions(
         "$B2XTAUNUTOOLSROOT/python/Bs2DsTauNu/TupleBs2DstaunuNonPhys.py")

# Events filled to ntuple
SeqSelBs2DstaunuNonPhysOutputCount = EventCountHisto(
    "SeqSelBs2DstaunuNonPhysOutputCount")
SeqSelBs2DstaunuNonPhysOutputCount.OutputLevel = outputLevel
SeqSelBs2DstaunuNonPhys.Members.append(SeqSelBs2DstaunuNonPhysOutputCount)
SeqSelBs2DstaunuNonPhys.OutputLevel = outputLevel


TimingAuditor().addTool(SequencerTimerTool, name="TIMER")
TimingAuditor().TIMER.NameSize = 60
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

# Momentum scale corrections
scaler = SCALER('Scaler')  # default configuration is perfectly fine

# Event tuple to count how many events were processed
etuple = EventTuple()
etuple.ToolList = ["TupleToolEventInfo", "TupleToolPrimaries"]

# DaVinci configuration
DaVinci().Simulation = SIMULATION 
DaVinci().DataType = YEAR 
DaVinci().EvtMax = -1 
if DaVinci().Simulation is True:
    DaVinci().CondDBtag = CONDDBTAG 
    DaVinci().DDDBtag = DDDBTAG 
else:
    DaVinci().Lumi = True
DaVinci().InputType = 'DST'

DaVinci().TupleFile = 'DVNtuple.root'
DaVinci().HistogramFile = 'DVHistos.root'
if DaVinci().Simulation == False:
    DaVinci().UserAlgorithms = [scaler,
                                SeqSelBs2Dstaunu,
                                SeqSelBs2DstaunuWS,
                                SeqSelBs2DstaunuNonPhys,
                                etuple]
else:
    #Scaler seems to be unable to deal with MC files
    DaVinci().UserAlgorithms = [SeqSelBs2Dstaunu,
                                SeqSelBs2DstaunuWS,
                                SeqSelBs2DstaunuNonPhys,
                                etuple]

