###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
A script to help the submission of Dsttaunu and Lctaunu jobs.
It is divided in three parts:
    - the creation of the JobConf class
    - the creation of a setup dictionnary with JobConf items
    - the ganga part with the job creation and submission
'''
import subprocess
import argparse

class JobConf(object) :
    '''
    A class to build easily ganga jobs with all the DaVinci setup in only one object.

    elements :
        name : name of the object (example : Lc3pi)
        path : path to access the LHC dataset
        polarity : Up or Down
        year : 2011, 2012, 2015, 2016
        isSimu : to set DaVinci().Simulation, True or False
        CondDBtag and DDDBtag for MC ntuple
    '''

    def __init__(self, name, path, polarity, year, isSimu, CondDBtag, DDDBtag):
        self.name = name
        self.path = path
        self.polarity = polarity
        self.year = year
        self.isSimu = isSimu
        self.CondDBtag = CondDBtag
        self.DDDBtag = DDDBtag
    
    def __repr__(self):
        return "({0}, {1}, {2}, {3}, {4}, {5},{6})".format(
            self.name,
            self.path,
            self.polarity,
            self.year,
            self.isSimu,
            self.CondDBtag,
            self.DDDBtag,
        )

# Application configuration
dv = GaudiExec() 
dv_version = 'v41r2'
project_root = 'Projects/' + 'DaVinciDev_' + dv_version
dv.directory = project_root

# Create a dictionnary to store all the paths we'll use
conddbtag_up = "sim-20160321-2-vc-mu100"
conddbtag_down = "sim-20160321-2-vc-md100"
dddbtag = "dddb-20150928"

setup = {}
# MC configuration

# Here should be put the configuration of the MC jobs with the path given by the bookeeping.
#setup['Bs2Dstaunu_Up'] = JobConf(
#    'Bs2Dstaunu',
#    'PATH',
#    'Up',
#    '2012',
#    True,
#    conddbtag_up,
#    dddbtag
#)



# Data configuration
setup['Data11_Up'] = JobConf(
    'Data11',
    '/LHCb/Collision11/Beam3500GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21r1/90000000/BHADRONCOMPLETEEVENT.DST',
    'Up',
    '2011',
    False,
    '',
    '',
)

setup['Data11_Down'] = JobConf(
    'Data11',
    '/LHCb/Collision11/Beam3500GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21r1/90000000/BHADRONCOMPLETEEVENT.DST',
    'Down',
    '2011',
    False,
    '',
    '',
)

setup['Data12_Up'] = JobConf(
    'Data12',
    '/LHCb/Collision12/Beam4000GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21/90000000/BHADRONCOMPLETEEVENT.DST',
    'Up',
    '2012',
    False,
    '',
    '',
)

setup['Data12_Down'] = JobConf(
    'Data12',
    '/LHCb/Collision12/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21/90000000/BHADRONCOMPLETEEVENT.DST',
    'Down',
    '2012',
    False,
    '',
    '',
)

setup['Data15_Up'] = JobConf(
    'Data15',
    '/LHCb/Collision15/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco15a/Stripping24/90000000/BHADRONCOMPLETEEVENT.DST',
    'Up',
    '2015',
    False,
    '',
    '',
)

setup['Data15_Down'] = JobConf(
    'Data15',
    '/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24/90000000/BHADRONCOMPLETEEVENT.DST',
    'Down',
    '2015',
    False,
    '',
    '',
)

setup['Data16_Up'] = JobConf(
    'Data16',
    '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco16/Stripping26/90000000/BHADRONCOMPLETEEVENT.DST',
    'Up',
    '2016',
    False,
    '',
    '',
)

setup['Data16_Down'] = JobConf(
    'Data16',
    '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Stripping26/90000000/BHADRONCOMPLETEEVENT.DST',
    'Down',
    '2016',
    False,
    '',
    '',
)

if __name__ == "__main__":
    # A parser to give to the script the names of jobs one wants to submit
    # In shell: lb-run Ganga ganga Bs2Dstaunu_ganga.py --jobs Data12 Data15 Lc3pi LcXc ... will create and submit of the jobs with the matching names
    parser = argparse.ArgumentParser()
    parser.add_argument('--jobs', nargs='+', help="specify the names of the desired jobs")
    parser.add_argument('--polarity', help="specifiy the desired polarity of the job")
    parser.add_argument('--all', help="process all job configurations")
    args = parser.parse_args()

    # If polarity exists, it should be Up or Down
    if args.polarity and args.polarity not in ["Up", "Down"]:
        raise Exception("not a polarity option (Up or Down)")

    # According to the parsing options, create a filtered setup from the default one
    if args.all:
        setup_filtered = setup
    elif args.polarity:
        setup_filtered = {jobname:jobconf for jobname,jobconf in setup.iteritems() if jobconf.polarity == args.polarity
                          and jobconf.name in args.jobs}
    else:
        setup_filtered = {jobname:jobconf for jobname,jobconf in setup.iteritems() if jobconf.name in args.jobs}

    # Loop over the setup configurations to create and submit jobs according to the parsing options
    for jobconf in setup_filtered:
        with open(project_root+'/Phys/B2XTauNuTools/python/Bs2DsTauNu/'+'Bs2Dstaunu_strp.py', 'r') as input_file, open(
            project_root+'/Phys/B2XTauNuTools/python/Bs2DsTauNu/'+'generated_strp_conf/Bs2Dstaunu_{0}_{1}_strp.py'.format(
                    setup[jobconf].name, setup[jobconf].polarity), "w")as output_file:
            options = dict.fromkeys(["YEAR", "SIMULATION", "CONDBTAG", "DDDBTAG"], "")
            options["YEAR"] = "'{0}'".format(setup[jobconf].year)
            options["SIMULATION"] = str(setup[jobconf].isSimu)
            options["CONDDBTAG"] = "'{0}'".format(setup[jobconf].CondDBtag)
            options["DDDBTAG"] = "'{0}'".format(setup[jobconf].DDDBtag)
    
            for line in input_file:
                for el in options.keys():
                    if el in line:
                        line = line.replace(el, options[el])
                output_file.write(line)
    
        dv.options = [project_root+'/Phys/B2XTauNuTools/python/Bs2DsTauNu/'+'generated_strp_conf/Bs2Dstaunu_{0}_{1}_strp.py'.format(
                          setup[jobconf].name, setup[jobconf].polarity)]
    
        # Get dataset
        bk = BKQuery(dqflag=["OK"], path=setup[jobconf].path, type="Path")
        data = bk.getDataset()
    
        # Job configuration
        j = Job(name='{0}_{1}_Bs2Dstaunu'.format(setup[jobconf].name, setup[jobconf].polarity), application=dv)
        j.inputdata = data
        j.backend = Dirac()

        if setup[jobconf].isSimu == True:
            j.splitter = SplitByFiles(filesPerJob=1)
        else:
            j.splitter = SplitByFiles(filesPerJob=5)

        # Output as DiracFile if Data and LocalFile if MC
        if setup[jobconf].isSimu == True:
            j.outputfiles = [
                LocalFile('DVNtuple.root'),
                LocalFile('DVHistos.root')]
        else:
            j.outputfiles = [
                DiracFile('DVNtuple.root'),
                LocalFile('DVHistos.root')]
    
        j.do_auto_resubmit = False 
        j.submit()

