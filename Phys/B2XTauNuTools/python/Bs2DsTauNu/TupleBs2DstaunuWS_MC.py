###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *

import os, sys, inspect
# realpath() with make your script run, even if you symlink it :)
cmd_folder = os.path.realpath(
    os.path.abspath(
        os.path.split(
            inspect.getfile(inspect.currentframe()))[0]))
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

from B2XTauNuTools.TupleHelper import *
outputLevel = INFO

SeqTupleBs2DstaunuWS = GaudiSequencer("SeqTupleBs2DstaunuWS")
TupleBs2DstaunuWS = DecayTreeTuple("Bs2DstaunuWSTuple")
TupleBs2DstaunuWS.OutputLevel = outputLevel
TupleBs2DstaunuWS.Inputs = ["Phys/Bs2DsTauNuWSForB2XTauNu/Particles"]

TupleBs2DstaunuWS.Decay = "[(B_s0 -> ^(D_s- -> ^K- ^K+ ^pi-) ^(tau- -> ^pi- ^pi+ ^pi- ))]CC"
TupleBs2DstaunuWS.Branches = {
    "Bs": "[(B_s0 -> (D_s- -> K- K+ pi-) (tau- -> pi- pi+ pi-))]CC",
    "Ds": "[(B_s0 -> ^(D_s- -> K- K+ pi-) (tau- -> pi- pi+ pi-))]CC",
    "ds_km": "[(B_s0 -> (D_s- -> ^K- K+ pi-) (tau- -> pi- pi+ pi-))]CC",
    "ds_kp": "[(B_s0 -> (D_s- -> K- ^K- pi-) (tau- -> pi- pi+ pi-))]CC",
    "ds_pion": "[(B_s0 -> (D_s- -> K- K+ ^pi-) (tau- -> pi- pi+ pi-))]CC",
    "tau": "[(B_s0 -> (D_s- -> K- K+ pi-) ^(tau- -> pi- pi+ pi-))]CC",
    "tau_pion": "[(B_s0 -> (D_s- -> K- K+ pi-) (tau- -> ^pi- ^pi+ ^pi-))]CC"
}

TupleBs2DstaunuWS.addTool(TupleToolDecay, name='Bs')
TupleBs2DstaunuWS.addTool(TupleToolDecay, name='Ds')
TupleBs2DstaunuWS.addTool(TupleToolDecay, name='ds_km')
TupleBs2DstaunuWS.addTool(TupleToolDecay, name='ds_kp')
TupleBs2DstaunuWS.addTool(TupleToolDecay, name='ds_pion')
TupleBs2DstaunuWS.addTool(TupleToolDecay, name='tau')
TupleBs2DstaunuWS.addTool(TupleToolDecay, name='tau_pion')

FinalStateConfig(TupleBs2DstaunuWS.ds_km)
FinalStateConfig(TupleBs2DstaunuWS.ds_kp)
FinalStateConfig(TupleBs2DstaunuWS.ds_pion)
FinalStateConfig(TupleBs2DstaunuWS.tau_pion)

DalitzConfig(TupleBs2DstaunuWS.tau)
DalitzConfig(TupleBs2DstaunuWS.Ds)

IsoConfig(TupleBs2DstaunuWS.tau) 
IsoConfig(TupleBs2DstaunuWS.Bs) 
#LifetimeConfig(TupleBs2DstaunuWS.tau)

ParentConfig(TupleBs2DstaunuWS.tau)
ParentConfig(TupleBs2DstaunuWS.Ds)
ParentConfig(TupleBs2DstaunuWS.Bs)

CommonConfig(TupleBs2DstaunuWS.Ds)
CommonConfig(TupleBs2DstaunuWS.Bs)
CommonConfig(TupleBs2DstaunuWS.ds_kp)
CommonConfig(TupleBs2DstaunuWS.ds_km)
CommonConfig(TupleBs2DstaunuWS.ds_pion)
CommonConfig(TupleBs2DstaunuWS.tau)
CommonConfig(TupleBs2DstaunuWS.tau_pion)
# Kinematic fit is using TupleToolB2XTauNuFit
# which is not used in the analysis
# which causes bugs with Bs2Dstaunu events
#KinematicFitConfig(TupleBs2DstaunuWS.Bs,isMC)

DTFConfig(TupleBs2DstaunuWS, 'D_s-')

TISTOSToolConfig(TupleBs2DstaunuWS.Bs)
TupleToolConfig(TupleBs2DstaunuWS)

TupleToolIsoGenericConfig(TupleBs2DstaunuWS)

# Specific MC tools
TupleToolMCConfig(TupleBs2DstaunuWS)

GaudiSequencer("SeqTupleBs2DstaunuWS").Members.append(TupleBs2DstaunuWS)
SeqTupleBs2DstaunuWS.IgnoreFilterPassed = True
GaudiSequencer("SeqSelBs2DstaunuWS").Members.append(SeqTupleBs2DstaunuWS)

