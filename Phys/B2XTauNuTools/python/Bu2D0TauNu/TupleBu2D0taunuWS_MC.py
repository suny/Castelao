###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *

import os, sys, inspect
# realpath() with make your script run, even if you symlink it :)
cmd_folder = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile( inspect.currentframe() ))[0]))
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

from B2XTauNuTools.TupleHelper import *
outputLevel = INFO

SeqTupleBu2D0taunuWS = GaudiSequencer("SeqTupleBu2D0taunuWS")
TupleBu2D0taunuWS = DecayTreeTuple("Bu2D0taunuWSTuple")
TupleBu2D0taunuWS.OutputLevel = outputLevel
TupleBu2D0taunuWS.Inputs = ["/Event/Phys/Bu2D0TauNuWSForB2XTauNu/Particles"]
TupleBu2D0taunuWS.Decay = "[B- -> (^D0 -> ^K+ ^pi-) (^tau+ -> ^pi+ ^pi- ^pi+ )]cc"
TupleBu2D0taunuWS.Branches = {
    "B" : "[B-]cc : [B- -> (D0 -> K+ pi-) (tau+ -> pi+ pi- pi+)]cc",
    "tau_pion" : "[B- -> (D0 -> K+ pi-) (tau+ -> ^pi+ ^pi- ^pi+)]cc",
    "d0_kaon" : "[B- -> (D0 -> ^K+ pi-) (tau+ -> pi+ pi- pi+)]cc",
    "d0_pion" : "[B- -> (D0 -> K+ ^pi-) (tau+ -> pi+ pi- pi+)]cc",
    "tau" : "[B- -> (D0 -> K+ pi-) (^tau+ -> pi+ pi- pi+)]cc",
    "D0" : "[B- -> (^D0 -> K+ pi-) (tau+ -> pi+ pi- pi+)]cc"}


TupleBu2D0taunuWS.addTool(TupleToolDecay, name = 'tau_pion')
TupleBu2D0taunuWS.addTool(TupleToolDecay, name = 'd0_pion')
TupleBu2D0taunuWS.addTool(TupleToolDecay, name = 'd0_kaon')
TupleBu2D0taunuWS.addTool(TupleToolDecay, name = 'tau')
TupleBu2D0taunuWS.addTool(TupleToolDecay, name = 'D0')
TupleBu2D0taunuWS.addTool(TupleToolDecay, name = 'B')


FinalStateConfig(TupleBu2D0taunuWS.tau_pion)
FinalStateConfig(TupleBu2D0taunuWS.d0_pion)
FinalStateConfig(TupleBu2D0taunuWS.d0_kaon)
#DalitzConfig(TupleBu2D0taunuWS.tau)
LifetimeConfig(TupleBu2D0taunuWS.tau)
ParentConfig(TupleBu2D0taunuWS.tau)
ParentConfig(TupleBu2D0taunuWS.D0)
ParentConfig(TupleBu2D0taunuWS.B)
CommonConfig(TupleBu2D0taunuWS.tau_pion)
CommonConfig(TupleBu2D0taunuWS.d0_pion)
CommonConfig(TupleBu2D0taunuWS.tau)
CommonConfig(TupleBu2D0taunuWS.D0)
CommonConfig(TupleBu2D0taunuWS.B)
CommonConfig(TupleBu2D0taunuWS.d0_kaon)
TISTOSToolConfig(TupleBu2D0taunuWS.B)
TupleToolConfig(TupleBu2D0taunuWS)
TupleToolMCConfig(TupleBu2D0taunuWS)

GaudiSequencer("SeqTupleBu2D0taunuWS").Members.append(TupleBu2D0taunuWS)

SeqTupleBu2D0taunuWS.IgnoreFilterPassed = True
GaudiSequencer("SeqSelBu2D0taunuWS").Members.append(SeqTupleBu2D0taunuWS)

