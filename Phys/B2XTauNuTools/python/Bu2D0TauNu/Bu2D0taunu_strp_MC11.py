###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
from Configurables import (
		DaVinci,
		EventSelector,
		EventCountHisto,
		CheckPV,
		)



## GLOBAL OUTPUT LEVEL
outputLevel = INFO

#Run stripping on the MC
importOptions( "$B2XTAUNUTOOLSROOT/python/B2XTauNuTools/SelBd2Dsttaunu_ConfigureStripping_MC.py")


#B- -> D0 tau nu

## SEQUENCER - As per the old .opts files.
SeqSelBu2D0taunu = GaudiSequencer("SeqSelBu2D0taunu")
SeqSelBu2D0taunu.OutputLevel = outputLevel

# Events in
SeqSelBu2D0taunuInputCount = EventCountHisto("SeqSelBu2D0taunuInputCount")
SeqSelBu2D0taunuInputCount.OutputLevel = outputLevel
SeqSelBu2D0taunu.Members.append(SeqSelBu2D0taunuInputCount)

#Events passing Selection
SeqSelBu2D0taunuAfterSelection = EventCountHisto("SeqSelBu2D0taunuAfterSelection")
SeqSelBu2D0taunuAfterSelection.OutputLevel = outputLevel
SeqSelBu2D0taunu.Members.append(SeqSelBu2D0taunuAfterSelection)

importOptions( "$B2XTAUNUTOOLSROOT/python/Bu2D0TauNu_MC/TupleBu2D0taunu_MC.py" )

#Events filled to ntuple
SeqSelBu2D0taunuOutputCount = EventCountHisto("SeqSelBu2D0taunuOutputCount")
SeqSelBu2D0taunuOutputCount.OutputLevel = outputLevel
SeqSelBu2D0taunu.Members.append(SeqSelBu2D0taunuOutputCount)
SeqSelBu2D0taunu.OutputLevel = outputLevel

## SEQUENCER - As per the old .opts files.
SeqSelBu2D0taunuWS = GaudiSequencer("SeqSelBu2D0taunuWS")
SeqSelBu2D0taunuWS.OutputLevel = outputLevel

# Events in
SeqSelBu2D0taunuWSInputCount = EventCountHisto("SeqSelBu2D0taunuWSInputCount")
SeqSelBu2D0taunuWSInputCount.OutputLevel = outputLevel
SeqSelBu2D0taunuWS.Members.append(SeqSelBu2D0taunuWSInputCount)

#Events passing Selection
SeqSelBu2D0taunuWSAfterSelection = EventCountHisto("SeqSelBu2D0taunuWSAfterSelection")
SeqSelBu2D0taunuWSAfterSelection.OutputLevel = outputLevel
SeqSelBu2D0taunuWS.Members.append(SeqSelBu2D0taunuWSAfterSelection)
importOptions( "$B2XTAUNUTOOLSROOT/python/Bu2D0TauNu_MC/TupleBu2D0taunuWS_MC.py" )

#Events filled to ntuple
SeqSelBu2D0taunuWSOutputCount = EventCountHisto("SeqSelBu2D0taunuWSOutputCount")
SeqSelBu2D0taunuWSOutputCount.OutputLevel = outputLevel
SeqSelBu2D0taunuWS.Members.append(SeqSelBu2D0taunuWSOutputCount)
SeqSelBu2D0taunuWS.OutputLevel = outputLevel


## SEQUENCER - As per the old .opts files.
SeqSelBu2D0taunuNonPhys = GaudiSequencer("SeqSelBu2D0taunuNonPhys")
SeqSelBu2D0taunuNonPhys.OutputLevel = outputLevel

# Events in
SeqSelBu2D0taunuNonPhysInputCount = EventCountHisto("SeqSelBu2D0taunuNonPhysInputCount")
SeqSelBu2D0taunuNonPhysInputCount.OutputLevel = outputLevel
SeqSelBu2D0taunuNonPhys.Members.append(SeqSelBu2D0taunuNonPhysInputCount)

#Events passing Selection
SeqSelBu2D0taunuNonPhysAfterSelection = EventCountHisto("SeqSelBu2D0taunuNonPhysAfterSelection")
SeqSelBu2D0taunuNonPhysAfterSelection.OutputLevel = outputLevel
SeqSelBu2D0taunuNonPhys.Members.append(SeqSelBu2D0taunuNonPhysAfterSelection)

importOptions( "$B2XTAUNUTOOLSROOT/python/Bu2D0TauNu_MC/TupleBu2D0taunuNonPhys_MC.py" )

#Events filled to ntuple
SeqSelBu2D0taunuNonPhysOutputCount = EventCountHisto("SeqSelBu2D0taunuNonPhysOutputCount")
SeqSelBu2D0taunuNonPhysOutputCount.OutputLevel = outputLevel
SeqSelBu2D0taunuNonPhys.Members.append(SeqSelBu2D0taunuNonPhysOutputCount)
SeqSelBu2D0taunuNonPhys.OutputLevel = outputLevel



from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"


#Event tuple to count how many events were processed
from Configurables import EventTuple, TupleToolTrigger
etuple=EventTuple()
etuple.ToolList = ["TupleToolEventInfo", "TupleToolPrimaries"]

#FOR MC11a:
DaVinci().CondDBtag = "sim-20111111-vc-md100"   #u for up, d for down in MC11
DaVinci().DDDBtag = "MC11-20111102"
DaVinci().EvtMax=-1
DaVinci().DataType='2011'
DaVinci().Simulation = True
DaVinci().Lumi = False
DaVinci().TupleFile = 'DVNtuple.root'
DaVinci().HistogramFile = 'DVHistos.root'
DaVinci().UserAlgorithms = [ SeqSelBu2D0taunu,  SeqSelBu2D0taunuWS, SeqSelBu2D0taunuNonPhys, etuple ]


