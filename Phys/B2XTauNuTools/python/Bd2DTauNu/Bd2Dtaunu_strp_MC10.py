###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
from Configurables import (
		DaVinci,
		EventSelector,
		EventCountHisto,
		CheckPV,
		)



## GLOBAL OUTPUT LEVEL
outputLevel = INFO

#Run stripping on the MC
importOptions( "$B2XTAUNUTOOLSROOT/python/B2XTauNuTools/SelBd2Dsttaunu_ConfigureStripping_MC.py") #This module configures all lines in the B2XTauNu stripping

## SEQUENCER - As per the old .opts files.
SeqSelBd2Dtaunu = GaudiSequencer("SeqSelBd2Dtaunu")
SeqSelBd2Dtaunu.OutputLevel = outputLevel

# Events in
SeqSelBd2DtaunuInputCount = EventCountHisto("SeqSelBd2DtaunuInputCount")
SeqSelBd2DtaunuInputCount.OutputLevel = outputLevel
SeqSelBd2Dtaunu.Members.append(SeqSelBd2DtaunuInputCount)

#Events passing Selection
SeqSelBd2DtaunuAfterSelection = EventCountHisto("SeqSelBd2DtaunuAfterSelection")
SeqSelBd2DtaunuAfterSelection.OutputLevel = outputLevel
SeqSelBd2Dtaunu.Members.append(SeqSelBd2DtaunuAfterSelection)

importOptions( "$B2XTAUNUTOOLSROOT/python/Bd2DTauNu_MC/TupleBd2Dtaunu_MC.py" )

#Events filled to ntuple
SeqSelBd2DtaunuOutputCount = EventCountHisto("SeqSelBd2DtaunuOutputCount")
SeqSelBd2DtaunuOutputCount.OutputLevel = outputLevel
SeqSelBd2Dtaunu.Members.append(SeqSelBd2DtaunuOutputCount)
SeqSelBd2Dtaunu.OutputLevel = outputLevel

## SEQUENCER - As per the old .opts files.
SeqSelBd2DtaunuWS = GaudiSequencer("SeqSelBd2DtaunuWS")
SeqSelBd2DtaunuWS.OutputLevel = outputLevel

# Events in
SeqSelBd2DtaunuWSInputCount = EventCountHisto("SeqSelBd2DtaunuWSInputCount")
SeqSelBd2DtaunuWSInputCount.OutputLevel = outputLevel
SeqSelBd2DtaunuWS.Members.append(SeqSelBd2DtaunuWSInputCount)

#Events passing Selection
SeqSelBd2DtaunuWSAfterSelection = EventCountHisto("SeqSelBd2DtaunuWSAfterSelection")
SeqSelBd2DtaunuWSAfterSelection.OutputLevel = outputLevel
SeqSelBd2DtaunuWS.Members.append(SeqSelBd2DtaunuWSAfterSelection)

importOptions( "$B2XTAUNUTOOLSROOT/python/Bd2DTauNu_MC/TupleBd2DtaunuWS_MC.py" )

#Events filled to ntuple
SeqSelBd2DtaunuWSOutputCount = EventCountHisto("SeqSelBd2DtaunuWSOutputCount")
SeqSelBd2DtaunuWSOutputCount.OutputLevel = outputLevel
SeqSelBd2DtaunuWS.Members.append(SeqSelBd2DtaunuWSOutputCount)
SeqSelBd2DtaunuWS.OutputLevel = outputLevel


## SEQUENCER - As per the old .opts files.
SeqSelBd2DtaunuNonPhys = GaudiSequencer("SeqSelBd2DtaunuNonPhys")
SeqSelBd2DtaunuNonPhys.OutputLevel = outputLevel

# Events in
SeqSelBd2DtaunuNonPhysInputCount = EventCountHisto("SeqSelBd2DtaunuNonPhysInputCount")
SeqSelBd2DtaunuNonPhysInputCount.OutputLevel = outputLevel
SeqSelBd2DtaunuNonPhys.Members.append(SeqSelBd2DtaunuNonPhysInputCount)


#Events passing Selection
SeqSelBd2DtaunuNonPhysAfterSelection = EventCountHisto("SeqSelBd2DtaunuNonPhysAfterSelection")
SeqSelBd2DtaunuNonPhysAfterSelection.OutputLevel = outputLevel
SeqSelBd2DtaunuNonPhys.Members.append(SeqSelBd2DtaunuNonPhysAfterSelection)

importOptions( "$B2XTAUNUTOOLSROOT/python/Bd2DTauNu_MC/TupleBd2DtaunuNonPhys_MC.py" )

#Events filled to ntuple
SeqSelBd2DtaunuNonPhysOutputCount = EventCountHisto("SeqSelBd2DtaunuNonPhysOutputCount")
SeqSelBd2DtaunuNonPhysOutputCount.OutputLevel = outputLevel
SeqSelBd2DtaunuNonPhys.Members.append(SeqSelBd2DtaunuNonPhysOutputCount)
SeqSelBd2DtaunuNonPhys.OutputLevel = outputLevel


from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"


#Event tuple to count how many events were processed
from Configurables import EventTuple, TupleToolTrigger
etuple=EventTuple()
etuple.ToolList = ["TupleToolEventInfo", "TupleToolPrimaries"]


#For MC10
DaVinci().CondDBtag = "sim-20101210-vc-md100"   #u for up, d for down in MC11
DaVinci().DDDBtag = "head-20101206"
DaVinci().EvtMax=-1
DaVinci().DataType='2010'
DaVinci().Simulation = True
DaVinci().TupleFile = 'DVNtuple.root'
DaVinci().HistogramFile = 'DVHistos.root'
DaVinci().UserAlgorithms = [ SeqSelBd2Dtaunu,  SeqSelBd2DtaunuWS, SeqSelBd2DtaunuNonPhys ]

