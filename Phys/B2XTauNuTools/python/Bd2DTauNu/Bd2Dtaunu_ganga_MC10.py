###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
dv = DaVinci()
dv.version = 'v33r2'
dv.platform = 'x86_64-slc5-gcc43-opt'
base=dv.user_release_area+'/DaVinci_'+dv.version+'/Phys/B2XTauNuTools/python/Bd2DTauNu_MC/'
dv.optsfile = [base+'Bd2Dtaunu_strp_MC10.py']

myJobs = [
	 ['Bd2DXMC10_withBd2DTauNuSel','MCMC1011960000Beam3500GeV-Oct2010-MagDown-Nu2,5Sim01Trig0x002e002aFlaggedReco08Stripping12FlaggedSTREAMSDST.py']
	,['Bu2DXMC10_withBd2DTauNuSel','MCMC1012960000Beam3500GeV-Oct2010-MagDown-Nu2,5Sim01Trig0x002e002aFlaggedReco08Stripping12FlaggedSTREAMSDST.py']
	,['Bs2DXMC10_withBd2DTauNuSel','MCMC1013960000Beam3500GeV-Oct2010-MagDown-Nu2,5Sim01Trig0x002e002aFlaggedReco08Stripping12FlaggedSTREAMSDST.py']
	,['Lb2DXMC10_withBd2DTauNuSel','MCMC1015960000Beam3500GeV-Oct2010-MagDown-Nu2,5Sim01Trig0x002e002aFlaggedReco08Stripping12FlaggedSTREAMSDST.py']
]

for i in myJobs:	

	j = Job (name=i[0],application=dv)

	j.backend          = Dirac()
	j.inputdata        = dv.readInputData(base+i[1])
	j.splitter         = SplitByFiles(filesPerJob=10)
	j.outputfiles      = ['DVNtuple.root','DVHistos.root']
	j.postprocessors   = RootMerger(overwrite=True,ignorefailed=True,files=['DVNtuple.root'])
	j.do_auto_resubmit = True
	j.submit()
