###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *

import os, sys, inspect
# realpath() with make your script run, even if you symlink it :)
cmd_folder = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile( inspect.currentframe() ))[0]))
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

from B2XTauNuTools.TupleHelper import *
outputLevel = INFO

SeqTupleBd2Dtaunu = GaudiSequencer("SeqTupleBd2Dtaunu")
TupleBd2Dtaunu = DecayTreeTuple("Bd2DtaunuTuple")
TupleBd2Dtaunu.OutputLevel = outputLevel
TupleBd2Dtaunu.Inputs = ["/Event/Phys/Bd2DTauNuForB2XTauNu/Particles"]
TupleBd2Dtaunu.Decay = "[B0 -> (^D- -> ^K+ ^pi- ^pi-) (^tau+ -> ^pi+ ^pi- ^pi+ )]cc"
TupleBd2Dtaunu.Branches = {
    "B0" : "[B0]cc : [B0 -> (D- -> K+ pi- pi-) (tau+ -> pi+ pi- pi+)]cc",
    "tau_pion" : "[B0 -> (D- -> K+ pi- pi-) (tau+ -> ^pi+ ^pi- ^pi+)]cc",
    "d_kaon" : "[B0 -> (D- -> ^K+ pi- pi-) (tau+ -> pi+ pi- pi+)]cc",
    "d_pion" : "[B0 -> (D- -> K+ ^pi- ^pi-) (tau+ -> pi+ pi- pi+)]cc",
    "tau" : "[B0 -> (D- -> K+ pi- pi-) (^tau+ -> pi+ pi- pi+)]cc",
    "D" : "[B0 -> (^D- -> K+ pi- pi-) (tau+ -> pi+ pi- pi+)]cc"}


TupleBd2Dtaunu.addTool(TupleToolDecay, name = 'tau_pion')
TupleBd2Dtaunu.addTool(TupleToolDecay, name = 'd_pion')
TupleBd2Dtaunu.addTool(TupleToolDecay, name = 'd_kaon')
TupleBd2Dtaunu.addTool(TupleToolDecay, name = 'tau')
TupleBd2Dtaunu.addTool(TupleToolDecay, name = 'D')
TupleBd2Dtaunu.addTool(TupleToolDecay, name = 'B0')


FinalStateConfig(TupleBd2Dtaunu.tau_pion)
FinalStateConfig(TupleBd2Dtaunu.d_pion)
FinalStateConfig(TupleBd2Dtaunu.d_kaon)
#DalitzConfig(TupleBd2Dtaunu.tau)
LifetimeConfig(TupleBd2Dtaunu.tau)
ParentConfig(TupleBd2Dtaunu.tau)
ParentConfig(TupleBd2Dtaunu.D)
ParentConfig(TupleBd2Dtaunu.B0)
CommonConfig(TupleBd2Dtaunu.tau_pion)
CommonConfig(TupleBd2Dtaunu.d_pion)

CommonConfig(TupleBd2Dtaunu.tau)
CommonConfig(TupleBd2Dtaunu.D)
CommonConfig(TupleBd2Dtaunu.B0)
CommonConfig(TupleBd2Dtaunu.d_kaon)
TISTOSToolConfig(TupleBd2Dtaunu.B0)
TupleToolConfig(TupleBd2Dtaunu)
TupleToolMCConfig(TupleBd2Dtaunu)
GaudiSequencer("SeqTupleBd2Dtaunu").Members.append(TupleBd2Dtaunu)
SeqTupleBd2Dtaunu.IgnoreFilterPassed = True
GaudiSequencer("SeqSelBd2Dtaunu").Members.append(SeqTupleBd2Dtaunu)

