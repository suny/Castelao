###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#-- GAUDI jobOptions generated on Tue Mar 12 15:57:01 2013
#-- Contains event types : 
#--   11266006 - 54 files - 519200 events - 214.14 GBytes


#--  Extra information about the data processing phases:


#--  Processing Pass Step-16578 

#--  StepId : 16578 
#--  StepName : Reco12a for MC11 - MagDown 
#--  ApplicationName : Brunel 
#--  ApplicationVersion : v41r1p1 
#--  OptionFiles : $APPCONFIGOPTS/Brunel/DataType-2011.py;$APPCONFIGOPTS/Brunel/MC-WithTruth.py;$APPCONFIGOPTS/Brunel/MC11a_dedxCorrection.py;$APPCONFIGOPTS/Brunel/TrigMuonRawEventFix.py 
#--  DDDB : MC11-20111102 
#--  CONDDB : sim-20111111-vc-md100 
#--  ExtraPackages : AppConfig.v3r122;SQLDDDB.v6r20 
#--  Visible : Y 


#--  Processing Pass Step-16379 

#--  StepId : 16379 
#--  StepName : Trigger - TCK 0x40760037 Flagged - MD - MC11a 
#--  ApplicationName : Moore 
#--  ApplicationVersion : v12r8g1 
#--  OptionFiles : $APPCONFIGOPTS/Moore/MooreSimProduction.py;$APPCONFIGOPTS/Conditions/TCK-0x40760037.py;$APPCONFIGOPTS/Moore/DataType-2011.py 
#--  DDDB : MC11-20111102 
#--  CONDDB : sim-20111111-vc-md100 
#--  ExtraPackages : AppConfig.v3r120;SQLDDDB.v6r20 
#--  Visible : Y 


#--  Processing Pass Step-16498 

#--  StepId : 16498 
#--  StepName : Sim05 with Nu=2.0 (w/o spillover) - MD - MC11a 
#--  ApplicationName : Gauss 
#--  ApplicationVersion : v41r1 
#--  OptionFiles : $APPCONFIGOPTS/Gauss/Beam3500GeV-md100-MC11-nu2.py;$DECFILESROOT/options/@{eventType}.py;$LBPYTHIAROOT/options/Pythia.py;$APPCONFIGOPTS/Gauss/G4PL_LHEP_EmNoCuts.py 
#--  DDDB : MC11-20111102 
#--  CONDDB : sim-20111111-vc-md100 
#--  ExtraPackages : AppConfig.v3r122;DecFiles.v25r1;SQLDDDB.v6r20 
#--  Visible : Y 


#--  Processing Pass Step-15798 

#--  StepId : 15798 
#--  StepName : Digi11 w/o spillover - MD - MC11a 
#--  ApplicationName : Boole 
#--  ApplicationVersion : v23r1 
#--  OptionFiles : $APPCONFIGOPTS/Boole/Default.py;$APPCONFIGOPTS/L0/L0TCK-0x0037.py 
#--  DDDB : MC11-20111102 
#--  CONDDB : sim-20111111-vc-md100 
#--  ExtraPackages : AppConfig.v3r118;SQLDDDB.v6r20 
#--  Visible : N 


#--  Processing Pass Step-15558 

#--  StepId : 15558 
#--  StepName : Stripping17Flagged for MC11 MagDown 
#--  ApplicationName : DaVinci 
#--  ApplicationVersion : v29r1p1 
#--  OptionFiles : $APPCONFIGOPTS/DaVinci/DV-Stripping17-Stripping-MC.py 
#--  DDDB : MC11-20111102 
#--  CONDDB : sim-20111111-vc-md100 
#--  ExtraPackages : AppConfig.v3r116;SQLDDDB.v6r20 
#--  Visible : Y 

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles(['LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00013966/0000/00013966_00000001_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00013966/0000/00013966_00000002_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00013966/0000/00013966_00000003_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00013966/0000/00013966_00000004_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00013966/0000/00013966_00000005_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00013966/0000/00013966_00000006_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00013966/0000/00013966_00000007_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00013966/0000/00013966_00000008_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00013966/0000/00013966_00000009_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00013966/0000/00013966_00000010_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00013966/0000/00013966_00000011_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00013996/0000/00013996_00000001_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00013996/0000/00013996_00000002_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00013996/0000/00013996_00000003_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00013996/0000/00013996_00000004_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00013996/0000/00013996_00000005_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00013996/0000/00013996_00000006_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00013996/0000/00013996_00000007_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00013996/0000/00013996_00000008_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00013996/0000/00013996_00000009_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00013996/0000/00013996_00000010_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00013996/0000/00013996_00000011_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00015257/0000/00015257_00000001_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00015257/0000/00015257_00000002_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00015257/0000/00015257_00000003_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00015257/0000/00015257_00000004_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00015257/0000/00015257_00000005_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00015257/0000/00015257_00000006_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00015257/0000/00015257_00000007_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00015257/0000/00015257_00000008_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00015257/0000/00015257_00000009_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00015257/0000/00015257_00000010_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00015257/0000/00015257_00000011_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00015257/0000/00015257_00000012_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00015257/0000/00015257_00000013_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00015257/0000/00015257_00000014_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00015257/0000/00015257_00000015_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00015257/0000/00015257_00000016_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00015275/0000/00015275_00000001_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00015275/0000/00015275_00000002_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00015275/0000/00015275_00000003_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00015275/0000/00015275_00000004_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00015275/0000/00015275_00000005_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00015275/0000/00015275_00000006_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00015275/0000/00015275_00000007_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00015275/0000/00015275_00000008_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00015275/0000/00015275_00000009_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00015275/0000/00015275_00000010_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00015275/0000/00015275_00000011_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00015275/0000/00015275_00000012_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00015275/0000/00015275_00000013_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00015275/0000/00015275_00000014_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00015275/0000/00015275_00000015_1.allstreams.dst',
'LFN:/lhcb/MC/MC11a/ALLSTREAMS.DST/00015275/0000/00015275_00000016_1.allstreams.dst'
], clear=True)
