###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *

import os, sys, inspect
# realpath() with make your script run, even if you symlink it :)
cmd_folder = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile( inspect.currentframe() ))[0]))
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

from B2XTauNuTools.TupleHelper import *
outputLevel = INFO

SeqTupleBd2DtaunuNonPhys = GaudiSequencer("SeqTupleBd2DtaunuNonPhys")
TupleBd2DtaunuNonPhys = DecayTreeTuple("Bd2DtaunuNonPhysTuple")
TupleBd2DtaunuNonPhys.OutputLevel = outputLevel
TupleBd2DtaunuNonPhys.Inputs = ["/Event/Phys/Bd2DTauNuNonPhysTauForB2XTauNu/Particles"]
TupleBd2DtaunuNonPhys.Decay = "[B0 -> (^D- -> ^K+ ^pi- ^pi-) (^tau+ -> ^pi+ ^pi+ ^pi+ )]cc"
TupleBd2DtaunuNonPhys.Branches = {
    "B0" : "[B0]cc : [B0 -> (D- -> K+ pi- pi-) (tau+ -> pi+ pi+ pi+)]cc",
    "tau_pion" : "[B0 -> (D- -> K+ pi- pi-) (tau+ -> ^pi+ ^pi+ ^pi+)]cc",
    "d_pion" : "[B0 -> (D- -> K+ ^pi- ^pi-) (tau+ -> pi+ pi+ pi+)]cc",
    "d_kaon" : "[B0 -> (D- -> ^K+ pi- pi-) (tau+ -> pi+ pi+ pi+)]cc",
    "tau" : "[B0 -> (D- -> K+ pi- pi-) (^tau+ -> pi+ pi+ pi+)]cc",
    "D" : "[B0 -> (^D- -> K+ pi- pi-) (tau+ -> pi+ pi+ pi+)]cc"}

TupleBd2DtaunuNonPhys.addTool(TupleToolDecay, name = 'tau_pion')
TupleBd2DtaunuNonPhys.addTool(TupleToolDecay, name = 'd_pion')
TupleBd2DtaunuNonPhys.addTool(TupleToolDecay, name = 'd_kaon')
TupleBd2DtaunuNonPhys.addTool(TupleToolDecay, name = 'tau')
TupleBd2DtaunuNonPhys.addTool(TupleToolDecay, name = 'D')
TupleBd2DtaunuNonPhys.addTool(TupleToolDecay, name = 'B0')


FinalStateConfig(TupleBd2DtaunuNonPhys.tau_pion)
FinalStateConfig(TupleBd2DtaunuNonPhys.d_pion)
FinalStateConfig(TupleBd2DtaunuNonPhys.d_kaon)
#DalitzConfig(TupleBd2DtaunuNonPhys.tau)
ParentConfig(TupleBd2DtaunuNonPhys.tau)
ParentConfig(TupleBd2DtaunuNonPhys.D)
ParentConfig(TupleBd2DtaunuNonPhys.B0)
CommonConfig(TupleBd2DtaunuNonPhys.tau_pion)
CommonConfig(TupleBd2DtaunuNonPhys.d_pion)
LifetimeConfig(TupleBd2DtaunuNonPhys.tau)

CommonConfig(TupleBd2DtaunuNonPhys.tau)
CommonConfig(TupleBd2DtaunuNonPhys.D)
CommonConfig(TupleBd2DtaunuNonPhys.B0)
CommonConfig(TupleBd2DtaunuNonPhys.d_kaon)
TISTOSToolConfig(TupleBd2DtaunuNonPhys.B0)

TupleToolConfig(TupleBd2DtaunuNonPhys)
TupleToolMCConfig(TupleBd2DtaunuNonPhys)

GaudiSequencer("SeqTupleBd2DtaunuNonPhys").Members.append(TupleBd2DtaunuNonPhys)

SeqTupleBd2DtaunuNonPhys.IgnoreFilterPassed = True
GaudiSequencer("SeqSelBd2DtaunuNonPhys").Members.append(SeqTupleBd2DtaunuNonPhys)

