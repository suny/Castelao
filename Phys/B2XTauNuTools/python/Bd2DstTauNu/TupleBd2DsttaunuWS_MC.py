###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *

import os
import sys
import inspect
from B2XTauNuTools.TupleHelper import *

# realpath() with make your script run, even if you symlink it :)
cmd_folder = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile( inspect.currentframe() ))[0]))
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

outputLevel = INFO

SeqTupleBd2DsttaunuWS = GaudiSequencer("SeqTupleBd2DsttaunuWS")
TupleBd2DsttaunuWS = DecayTreeTuple("Bd2DsttaunuWSTuple")
TupleBd2DsttaunuWS.OutputLevel = outputLevel
TupleBd2DsttaunuWS.Inputs = ["/Event/Phys/Bd2DstarTauNuWSForB2XTauNu/Particles"]
TupleBd2DsttaunuWS.Decay = "[B0 -> (^D*(2010)+ -> ^pi+ (^D0 -> ^K- ^pi+ )) (^tau+ -> ^pi+ ^pi- ^pi+ )]cc"
TupleBd2DsttaunuWS.Branches = {
    "B0" : "[B0]cc : [B0 -> (D*(2010)+ -> pi+ (D0 -> K- pi+ )) (tau+ -> pi+ pi- pi+)]cc",
    "tau_pion" : "[B0 -> (D*(2010)+ -> pi+ (D0 -> K- pi+ )) (tau+ -> ^pi+ ^pi- ^pi+)]cc",
    "dz_pion" : "[B0 -> (D*(2010)+ -> pi+ (D0 -> K- ^pi+ )) (tau+ -> pi+ pi- pi+)]cc",
    "dst_pion" : "[B0 -> (D*(2010)+ -> ^pi+ (D0 -> K- pi+ )) (tau+ -> pi+ pi- pi+)]cc",
    "dz_kaon" : "[B0 -> (D*(2010)+ -> pi+ (D0 -> ^K- pi+ )) (tau+ -> pi+ pi- pi+)]cc",
    "tau" : "[B0 -> (D*(2010)+ -> pi+ (D0 -> K- pi+ )) (^tau+ -> pi+ pi- pi+)]cc",
    "D0" : "[B0 -> (D*(2010)+ -> pi+ (^D0 -> K- pi+ )) (tau+ -> pi+ pi- pi+)]cc",
    "Dst" : "[B0 -> (^D*(2010)+ -> pi+ (D0 -> K- pi+ )) (tau+ -> pi+ pi- pi+)]cc"}


TupleBd2DsttaunuWS.addTool(TupleToolDecay, name = 'dst_pion')
TupleBd2DsttaunuWS.addTool(TupleToolDecay, name = 'tau_pion')
TupleBd2DsttaunuWS.addTool(TupleToolDecay, name = 'dz_pion')
TupleBd2DsttaunuWS.addTool(TupleToolDecay, name = 'dz_kaon')
TupleBd2DsttaunuWS.addTool(TupleToolDecay, name = 'tau')
TupleBd2DsttaunuWS.addTool(TupleToolDecay, name = 'D0')
TupleBd2DsttaunuWS.addTool(TupleToolDecay, name = 'Dst')
TupleBd2DsttaunuWS.addTool(TupleToolDecay, name = 'B0')

FinalStateConfig(TupleBd2DsttaunuWS.dst_pion)
FinalStateConfig(TupleBd2DsttaunuWS.tau_pion)
FinalStateConfig(TupleBd2DsttaunuWS.dz_pion)
FinalStateConfig(TupleBd2DsttaunuWS.dz_kaon)
DalitzConfig(TupleBd2DsttaunuWS.tau)
IsoConfig(TupleBd2DsttaunuWS.tau)   #,True)
IsoConfig(TupleBd2DsttaunuWS.B0)   #,True)
#LifetimeConfig(TupleBd2DsttaunuWS.tau)
ParentConfig(TupleBd2DsttaunuWS.tau)
ParentConfig(TupleBd2DsttaunuWS.Dst)
ParentConfig(TupleBd2DsttaunuWS.D0)
ParentConfig(TupleBd2DsttaunuWS.B0)
CommonConfig(TupleBd2DsttaunuWS.dst_pion)
CommonConfig(TupleBd2DsttaunuWS.tau_pion)
CommonConfig(TupleBd2DsttaunuWS.dz_pion)
KinematicFitConfig(TupleBd2DsttaunuWS.B0, True)


CommonConfig(TupleBd2DsttaunuWS.tau)
CommonConfig(TupleBd2DsttaunuWS.Dst)
CommonConfig(TupleBd2DsttaunuWS.D0)
CommonConfig(TupleBd2DsttaunuWS.B0)
CommonConfig(TupleBd2DsttaunuWS.dz_kaon)
TISTOSToolConfig(TupleBd2DsttaunuWS.B0)
TupleToolConfig(TupleBd2DsttaunuWS)
TupleToolMCConfig(TupleBd2DsttaunuWS)
GaudiSequencer("SeqTupleBd2DsttaunuWS").Members.append(TupleBd2DsttaunuWS)
SeqTupleBd2DsttaunuWS.IgnoreFilterPassed = True
GaudiSequencer("SeqSelBd2DsttaunuWS").Members.append(SeqTupleBd2DsttaunuWS)
