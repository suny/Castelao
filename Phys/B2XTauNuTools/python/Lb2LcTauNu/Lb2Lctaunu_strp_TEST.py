###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
from Configurables import (
    DaVinci,
    EventSelector,
    EventCountHisto,
    CheckPV
)
from Configurables import TrackScaleState as SCALER
from Configurables import TimingAuditor, SequencerTimerTool
from Configurables import EventTuple, TupleToolTrigger
from GaudiConf import IOHelper

# Global output level
outputLevel = INFO
isMC = False

# Run stripping on the MC
importOptions(
    "$B2XTAUNUTOOLSROOT/python/B2XTauNuTools/SelB2XTauNu_ConfigureStripping.py"
)

# Momentum scale corrections
scaler = SCALER('Scaler')  # default configuration is perfectly fine

# SEQUENCER - As per the old .opts files.
SeqSelLb2Lctaunu = GaudiSequencer("SeqSelLb2Lctaunu")
SeqSelLb2Lctaunu.OutputLevel = outputLevel

# Events in
SeqSelLb2LctaunuInputCount = EventCountHisto("SeqSelLb2LctaunuInputCount")
SeqSelLb2LctaunuInputCount.OutputLevel = outputLevel
SeqSelLb2Lctaunu.Members.append(SeqSelLb2LctaunuInputCount)


# Events passing Selection
SeqSelLb2LctaunuAfterSelection = EventCountHisto(
    "SeqSelLb2LctaunuAfterSelection")
SeqSelLb2LctaunuAfterSelection.OutputLevel = outputLevel
SeqSelLb2Lctaunu.Members.append(SeqSelLb2LctaunuAfterSelection)

if isMC is True:
    importOptions(
        "$B2XTAUNUTOOLSROOT/python/Lb2LcTauNu/TupleLb2Lctaunu_MC.py"
    )
else:
    importOptions(
        "$B2XTAUNUTOOLSROOT/python/Lb2LcTauNu/TupleLb2Lctaunu.py"
    )


# Events filled to ntuple
SeqSelLb2LctaunuOutputCount = EventCountHisto("SeqSelLb2LctaunuOutputCount")
SeqSelLb2LctaunuOutputCount.OutputLevel = outputLevel
SeqSelLb2Lctaunu.Members.append(SeqSelLb2LctaunuOutputCount)
SeqSelLb2Lctaunu.OutputLevel = outputLevel

# SEQUENCER - As per the old .opts files.
SeqSelLb2LctaunuWS = GaudiSequencer("SeqSelLb2LctaunuWS")
SeqSelLb2LctaunuWS.OutputLevel = outputLevel

# Events in
SeqSelLb2LctaunuWSInputCount = EventCountHisto("SeqSelLb2LctaunuWSInputCount")
SeqSelLb2LctaunuWSInputCount.OutputLevel = outputLevel
SeqSelLb2LctaunuWS.Members.append(SeqSelLb2LctaunuWSInputCount)


# Events passing Selection
SeqSelLb2LctaunuWSAfterSelection = EventCountHisto(
    "SeqSelLb2LctaunuWSAfterSelection")
SeqSelLb2LctaunuWSAfterSelection.OutputLevel = outputLevel
SeqSelLb2LctaunuWS.Members.append(SeqSelLb2LctaunuWSAfterSelection)

if isMC is True:
    importOptions(
        "$B2XTAUNUTOOLSROOT/python/Lb2LcTauNu/TupleLb2Lctaunu_MC.py")
else:
    importOptions(
        "$B2XTAUNUTOOLSROOT/python/Lb2LcTauNu/TupleLb2Lctaunu.py")

# Events filled to ntuple
SeqSelLb2LctaunuWSOutputCount = EventCountHisto(
    "SeqSelLb2LctaunuWSOutputCount")
SeqSelLb2LctaunuWSOutputCount.OutputLevel = outputLevel
SeqSelLb2LctaunuWS.Members.append(SeqSelLb2LctaunuWSOutputCount)
SeqSelLb2LctaunuWS.OutputLevel = outputLevel


# SEQUENCER - As per the old .opts files.
SeqSelLb2LctaunuNonPhys = GaudiSequencer("SeqSelLb2LctaunuNonPhys")
SeqSelLb2LctaunuNonPhys.OutputLevel = outputLevel

# Events in
SeqSelLb2LctaunuNonPhysInputCount = EventCountHisto(
    "SeqSelLb2LctaunuNonPhysInputCount")
SeqSelLb2LctaunuNonPhysInputCount.OutputLevel = outputLevel
SeqSelLb2LctaunuNonPhys.Members.append(SeqSelLb2LctaunuNonPhysInputCount)


# Events passing Selection
SeqSelLb2LctaunuNonPhysAfterSelection = EventCountHisto(
    "SeqSelLb2LctaunuNonPhysAfterSelection")
SeqSelLb2LctaunuNonPhysAfterSelection.OutputLevel = outputLevel
SeqSelLb2LctaunuNonPhys.Members.append(SeqSelLb2LctaunuNonPhysAfterSelection)

if isMC is True:
    importOptions(
        "$B2XTAUNUTOOLSROOT/python/Lb2LcTauNu/TupleLb2Lctaunu_MC.py")
else:
    importOptions(
        "$B2XTAUNUTOOLSROOT/python/Lb2LcTauNu/TupleLb2Lctaunu.py")

# Events filled to ntuple
SeqSelLb2LctaunuNonPhysOutputCount = EventCountHisto(
    "SeqSelLb2LctaunuNonPhysOutputCount")
SeqSelLb2LctaunuNonPhysOutputCount.OutputLevel = outputLevel
SeqSelLb2LctaunuNonPhys.Members.append(SeqSelLb2LctaunuNonPhysOutputCount)
SeqSelLb2LctaunuNonPhys.OutputLevel = outputLevel


TimingAuditor().addTool(SequencerTimerTool, name="TIMER")
TimingAuditor().TIMER.NameSize = 60
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

# Event tuple to count how many events were processed
etuple = EventTuple()
etuple.ToolList = ["TupleToolEventInfo", "TupleToolPrimaries"]

# DaVinci configuration
DaVinci().Simulation = False
DaVinci().DataType = '2012'
DaVinci().EvtMax = 300
if DaVinci().Simulation is True:
    DaVinci().CondDBtag = "sim-20160321-2-vc-mu100"   # u for up, d for down in MC11
    DaVinci().DDDBtag = "dddb-20150928"
    DaVinci().Lumi = False
else:
    DaVinci().Lumi = True
DaVinci().InputType = 'DST'

DaVinci().TupleFile = 'DVNtuple.root'
DaVinci().HistogramFile = 'DVHistos.root'
DaVinci().UserAlgorithms = [
                            scaler,
                            SeqSelLb2Lctaunu,
                            SeqSelLb2LctaunuWS,
                            SeqSelLb2LctaunuNonPhys,
                            etuple
                            ]

IOHelper().inputFiles([
    '/afs/cern.ch/work/v/vrenaudi/00041836_00000482_1.bhadroncompleteevent.dst',
    #'/afs/cern.ch/work/v/vrenaudi/00053310_00000018_1.lblcds3pin.striptrig.dst'
], clear=True)
