###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *

import os
import sys
import inspect
from B2XTauNuTools.TupleHelper import *

# realpath() will make your script run, even if you symlink it :)
cmd_folder = os.path.realpath(
    os.path.abspath(
        os.path.split(
            inspect.getfile(inspect.currentframe()))[0]))
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

outputLevel = INFO

SeqTupleLb2Lctaunu = GaudiSequencer("SeqTupleLb2Lctaunu")
TupleLb2Lctaunu = DecayTreeTuple("Lb2LctaunuTuple")
TupleLb2Lctaunu.OutputLevel = outputLevel
TupleLb2Lctaunu.Inputs = ["Phys/Lb2LcTauNuForB2XTauNu/Particles"]

TupleLb2Lctaunu.Decay = "[(Lambda_b0 -> ^(Lambda_c+ -> ^p+ ^K- ^pi+) ^(tau- -> ^pi- ^pi+ ^pi- ))]CC"
TupleLb2Lctaunu.Branches = {
    "Lambda_b0": "[(Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (tau- -> pi- pi+ pi-))]CC",
    "Lambda_c": "[(Lambda_b0 -> ^(Lambda_c+ -> p+ K- pi+) (tau- -> pi- pi+ pi-))]CC",
    "lc_p": "[(Lambda_b0 -> (Lambda_c+ -> ^p+ K- pi+) (tau- -> pi- pi+ pi-))]CC",
    "lc_kaon": "[(Lambda_b0 -> (Lambda_c+ -> p+ ^K- pi+) (tau- -> pi- pi+ pi-))]CC",
    "lc_pion": "[(Lambda_b0 -> (Lambda_c+ -> p+ K- ^pi+) (tau- -> pi- pi+ pi-))]CC",
    "tau": "[(Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) ^(tau- -> pi- pi+ pi-))]CC",
    "tau_pion": "[(Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (tau- -> ^pi- ^pi+ ^pi-))]CC"
}

TupleLb2Lctaunu.addTool(TupleToolDecay, name='Lambda_b0')
TupleLb2Lctaunu.addTool(TupleToolDecay, name='Lambda_c')
TupleLb2Lctaunu.addTool(TupleToolDecay, name='lc_p')
TupleLb2Lctaunu.addTool(TupleToolDecay, name='lc_kaon')
TupleLb2Lctaunu.addTool(TupleToolDecay, name='lc_pion')
TupleLb2Lctaunu.addTool(TupleToolDecay, name='tau')
TupleLb2Lctaunu.addTool(TupleToolDecay, name='tau_pion')

FinalStateConfig(TupleLb2Lctaunu.lc_p)
FinalStateConfig(TupleLb2Lctaunu.tau_pion)
FinalStateConfig(TupleLb2Lctaunu.lc_pion)
FinalStateConfig(TupleLb2Lctaunu.lc_kaon)

DalitzConfig(TupleLb2Lctaunu.tau)
DalitzConfig(TupleLb2Lctaunu.Lambda_c)

IsoConfig(TupleLb2Lctaunu.tau)
IsoConfig(TupleLb2Lctaunu.Lambda_b0)
# LifetimeConfig(TupleLb2Lctaunu.tau)
ParentConfig(TupleLb2Lctaunu.tau)
ParentConfig(TupleLb2Lctaunu.Lambda_c)
ParentConfig(TupleLb2Lctaunu.Lambda_b0)

CommonConfig(TupleLb2Lctaunu.Lambda_b0)
CommonConfig(TupleLb2Lctaunu.Lambda_c)
CommonConfig(TupleLb2Lctaunu.lc_p)
CommonConfig(TupleLb2Lctaunu.lc_kaon)
CommonConfig(TupleLb2Lctaunu.lc_pion)
CommonConfig(TupleLb2Lctaunu.tau)
CommonConfig(TupleLb2Lctaunu.tau_pion)
# Kinematic fit is using TupleToolB2XTauNuFit
# which is not used in the analysis
# which causes bugs with Lb2Lctaunu events
#KinematicFitConfig(TupleLb2LctaunuWS.Lambda_b0,isMC)

DTFConfig(TupleLb2Lctaunu.Lambda_b0, 'Lambda_c+')

TISTOSToolConfig(TupleLb2Lctaunu.Lambda_b0, run="run2")
TISTOSToolConfig(TupleLb2Lctaunu.Lambda_c, run="run2")
TISTOSToolConfig(TupleLb2Lctaunu.tau, run="run2")
TupleToolConfig(TupleLb2Lctaunu)

TupleToolIsoGenericConfig(TupleLb2Lctaunu)

# Specific MC tools
TupleToolMCConfig(TupleLb2Lctaunu)

GaudiSequencer("SeqTupleLb2Lctaunu").Members.append(TupleLb2Lctaunu)
SeqTupleLb2Lctaunu.IgnoreFilterPassed = True
GaudiSequencer("SeqSelLb2Lctaunu").Members.append(SeqTupleLb2Lctaunu)
