###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
A script to help the submission of Dsttaunu and Lctaunu jobs.
It is divided in three parts:
    - the creation of the JobConf class
    - the creation of a setup dictionnary with JobConf items
    - the ganga part with the job creation and submission
"""
import subprocess
import argparse

class JobConf(object) :
    '''
    A class to build easily ganga jobs with all the DaVinci setup in only one object.

    elements :
        name : name of the object (example : Lc3pi)
        path : path to access the LHC dataset
        polarity : Up or Down
        year : 2011, 2012, 2015, 2016
        isSimu : to set DaVinci().Simulation, True or False
        CondDBtag and DDDBtag for MC ntuple
    '''

    def __init__(self, name, path, polarity, year, isSimu, CondDBtag, DDDBtag):
        self.name = name
        self.path = path
        self.polarity = polarity
        self.year = year
        self.isSimu = isSimu
        self.CondDBtag = CondDBtag
        self.DDDBtag = DDDBtag

    def __repr__(self):
        return "({0}, {1}, {2}, {3}, {4}, {5},{6})".format(
            self.name,
            self.path,
            self.polarity,
            self.year,
            self.isSimu,
            self.CondDBtag,
            self.DDDBtag,
        )

# Application configuration
dv = GaudiExec()
dv.platform = "x86_64-slc6-gcc62-opt"
dv_version = 'v44r6'
project_root = '/afs/cern.ch/user/v/vrenaudi/Projects/' + 'DaVinciDev_' + dv_version
dv.directory = project_root

# Create a dictionnary to store all the paths we'll use
setup = {}

# MC configuration
# Sim08 configurations:
setup['Sim08_MC12Lctaunu_tau3pi_Up'] = JobConf(
    'Sim08_MC12Lctaunu_tau3pi',
    '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08g/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/15863040/ALLSTREAMS.DST',
    'Up',
    '2012',
    True,
    'sim-20130522-1-vc-mu100',
    'dddb-20130929-1', )

setup['Sim08_MC12Lctaunu_tau3pi_Down'] = JobConf(
    'Sim08_MC12Lctaunu_tau3pi',
    '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08g/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/15863040/ALLSTREAMS.DST',
    'Down',
    '2012',
    True,
    'sim-20130522-1-vc-md100',
    'dddb-20130929-1', )

setup['Sim08_MC12Lctaunu_tau3pipi0_Up'] = JobConf(
    'Sim08_MC12Lctaunu_tau3pipi0',
    '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08g/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/15863040/ALLSTREAMS.DST',
    'Up',
    '2012',
    True,
    'sim-20130522-1-vc-mu100',
    'dddb-20130929-1', )

setup['Sim08_MC12Lctaunu_tau3pipi0_Down'] = JobConf(
    'Sim08_MC12Lctaunu_tau3pipi0',
    '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08g/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/15863040/ALLSTREAMS.DST',
    'Down',
    '2012',
    True,
    'sim-20130522-1-vc-md100',
    'dddb-20130929-1', )

setup['Sim08_MC12Lctaunu_Lc3pi_Up'] = JobConf(
    'Sim08_MC12Lctaunu_Lc3pi',
    '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08c/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/15266005/ALLSTREAMS.DST',
    'Up',
    '2012',
    True,
    '',
    '', )
setup['Sim08_MC12Lctaunu_Lc3pi_Down'] = JobConf(
    'Sim08_MC12Lctaunu_Lc3pi',
    '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08c/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/15266005/ALLSTREAMS.DST',
    'Down',
    '2012',
    True,
    '',
    '',)

# 2011 configuration ------------------------------------------------------------------------------------------------------------------

# tau3pi ------------------------------------
setup['MC11Lctaunu_tau3pi_Up'] = JobConf(
    'MC11Lctaunu_tau3pi',
    '/MC/2011/Beam3500GeV-2011-MagUp-Nu2-Pythia8/Sim09c/Trig0x40760037/Reco14c/Stripping21r1Filtered/15863030/LB2LCXC.STRIPTRIG.DST',
    'Up',
    '2011',
    True,
    'sim-20160614-1-vc-mu100',
    'dddb-20170721-1', )
setup['MC11Lctaunu_tau3pi_Down'] = JobConf(
    'MC11Lctaunu_tau3pi',
    '/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim09c/Trig0x40760037/Reco14c/Stripping21r1Filtered/15863030/LB2LCXC.STRIPTRIG.DST',
    'Down',
    '2011',
    True,
    'sim-20160614-1-vc-md100',
    'dddb-20170721-1',)

# tau3pipi0 ---------------------------------
setup['MC11Lctaunu_tau3pipi0_Up'] = JobConf(
    'MC11Lctaunu_tau3pipi0',
    '/MC/2011/Beam3500GeV-2011-MagUp-Nu2-Pythia8/Sim09c/Trig0x40760037/Reco14c/Stripping21r1Filtered/15863040/LB2LCXC.STRIPTRIG.DST',
    'Up',
    '2011',
    True,
    'sim-20160614-1-vc-mu100',
    'dddb-20170721-1', )
setup['MC11Lctaunu_tau3pipi0_Down'] = JobConf(
    'MC11Lctaunu_tau3pipi0',
    '/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim09c/Trig0x40760037/Reco14c/Stripping21r1Filtered/15863040/LB2LCXC.STRIPTRIG.DST',
    'Down',
    '2011',
    True,
    'sim-20160614-1-vc-md100',
    'dddb-20170721-1',)

# Lc3pi -------------------------------------
setup['MC11Lctaunu_Lc3pi_Up'] = JobConf(
    'MC11Lctaunu_Lc3pi',
    '/MC/2011/Beam3500GeV-2011-MagUp-Nu2-Pythia8/Sim09c/Trig0x40760037/Reco14c/Stripping21r1Filtered/15464000/LB2LCXC.STRIPTRIG.DST',
    'Up',
    '2011',
    True,
    'sim-20160614-1-vc-mu100',
    'dddb-20170721-1', )
setup['MC11Lctaunu_Lc3pi_Down'] = JobConf(
    'MC11Lctaunu_Lc3pi',
    '/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim09c/Trig0x40760037/Reco14c/Stripping21r1Filtered/15464000/LB2LCXC.STRIPTRIG.DST',
    'Down',
    '2011',
    True,
    'sim-20160614-1-vc-md100',
    'dddb-20170721-1',)

# LcDs --------------------------------------
setup['MC11Lctaunu_LcDs_Up'] = JobConf(
    'MC11Lctaunu_LcDs',
    '/MC/2011/Beam3500GeV-2011-MagUp-Nu2-Pythia8/Sim09c/Trig0x40760037/Reco14c/Stripping21r1Filtered/15296601/LB2LCXC.STRIPTRIG.DST',
    'Up',
    '2011',
    True,
    'sim-20160614-1-vc-mu100',
    'dddb-20170721-1', )
setup['MC11Lctaunu_LcDs_Down'] = JobConf(
    'MC11Lctaunu_LcDs',
    '/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim09c/Trig0x40760037/Reco14c/Stripping21r1Filtered/15296601/LB2LCXC.STRIPTRIG.DST',
    'Down',
    '2011',
    True,
    'sim-20160614-1-vc-md100',
    'dddb-20170721-1',)

# LcD0 --------------------------------------
setup['MC11Lctaunu_LcD0_Up'] = JobConf(
    'MC11Lctaunu_LcD0',
    '/MC/2011/Beam3500GeV-2011-MagUp-Nu2-Pythia8/Sim09c/Trig0x40760037/Reco14c/Stripping21r1Filtered/15894401/LB2LCXC.STRIPTRIG.DST',
    'Up',
    '2011',
    True,
    'sim-20160614-1-vc-mu100',
    'dddb-20170721-1', )
setup['MC11Lctaunu_LcD0_Down'] = JobConf(
    'MC11Lctaunu_LcD0',
    '/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim09c/Trig0x40760037/Reco14c/Stripping21r1Filtered/15894401/LB2LCXC.STRIPTRIG.DST',
    'Down',
    '2011',
    True,
    'sim-20160614-1-vc-md100',
    'dddb-20170721-1',)

# LcD- --------------------------------------
setup['MC11Lctaunu_LcD-_Up'] = JobConf(
    'MC11Lctaunu_LcD-',
    '/MC/2011/Beam3500GeV-2011-MagUp-Nu2-Pythia8/Sim09c/Trig0x40760037/Reco14c/Stripping21r1Filtered/15895401/LB2LCXC.STRIPTRIG.DST',
    'Up',
    '2011',
    True,
    'sim-20160614-1-vc-mu100',
    'dddb-20170721-1', )
setup['MC11Lctaunu_LcD-_Down'] = JobConf(
    'MC11Lctaunu_LcD-',
    '/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim09c/Trig0x40760037/Reco14c/Stripping21r1Filtered/15895401/LB2LCXC.STRIPTRIG.DST',
    'Down',
    '2011',
    True,
    'sim-20160614-1-vc-md100',
    'dddb-20170721-1',)

# 2012 configuration ------------------------------------------------------------------------------------------------------------------

# tau3pi ------------------------------------
setup['MC12Lctaunu_tau3pi_Up'] = JobConf(
    'MC12Lctaunu_tau3pi',
    '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim09c/Trig0x409f0045/Reco14c/Stripping21Filtered/15863030/LB2LCXC.STRIPTRIG.DST',
    'Up',
    '2012',
    True,
    'sim-20160321-2-vc-mu100',
    'dddb-20170721-1',)
setup['MC12Lctaunu_tau3pi_Down'] = JobConf(
    'MC12Lctaunu_tau3pi',
    '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim09c/Trig0x409f0045/Reco14c/Stripping21Filtered/15863030/LB2LCXC.STRIPTRIG.DST',
    'Down',
    '2012',
    True,
    'sim-20160321-2-vc-md100',
    'dddb-20170721-1',)

# tau3pipi0 ---------------------------------
setup['MC12Lctaunu_tau3pipi0_Up'] = JobConf(
    'MC12Lctaunu_tau3pipi0',
    '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim09c/Trig0x409f0045/Reco14c/Stripping21Filtered/15863040/LB2LCXC.STRIPTRIG.DST',
    'Up',
    '2012',
    True,
    'sim-20160321-2-vc-mu100',
    'dddb-20170721-1',)
setup['MC12Lctaunu_tau3pipi0_Down'] = JobConf(
    'MC12Lctaunu_tau3pipi0',
    '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim09c/Trig0x409f0045/Reco14c/Stripping21Filtered/15863040/LB2LCXC.STRIPTRIG.DST',
    'Down',
    '2012',
    True,
    'sim-20160321-2-vc-md100',
    'dddb-20170721-1',)

# Lcst2625taunu ------------------------------------
setup['MC12Lctaunu_Lcst2625taunu_Up'] = JobConf(
    'MC12Lctaunu_Lcst2625taunu',
    '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim09c/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/15865010/ALLSTREAMS.DST',
    'Up',
    '2012',
    True,
    'sim-20160321-2-vc-mu100',
    'dddb-20170721-2',)
setup['MC12Lctaunu_Lcst2625taunu_Down'] = JobConf(
    'MC12Lctaunu_Lcst2625taunu',
    '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim09c/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/15865010/ALLSTREAMS.DST',
    'Down',
    '2012',
    True,
    'sim-20160321-2-vc-md100',
    'dddb-20170721-2',)

# Lcst2625taunu 3pipi0 ------------------------------------
setup['MC12Lctaunu_Lcst2625taunu_3pipi0_Up'] = JobConf(
    'MC12Lctaunu_Lcst2625taunu_3pipi0',
    '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim09c/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/15865015/ALLSTREAMS.DST',
    'Up',
    '2012',
    True,
    'sim-20160321-2-vc-mu100',
    'dddb-20170721-2',)
setup['MC12Lctaunu_Lcst2625taunu_3pipi0_Down'] = JobConf(
    'MC12Lctaunu_Lcst2625taunu_3pipi0',
    '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim09c/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/15865015/ALLSTREAMS.DST',
    'Down',
    '2012',
    True,
    'sim-20160321-2-vc-md100',
    'dddb-20170721-2',)

# Lcst2593taunu ---------------------------------
setup['MC12Lctaunu_Lcst2593taunu_Up'] = JobConf(
    'MC12Lctaunu_Lcst2593taunu',
    '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim09c/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/15865011/ALLSTREAMS.DST',
    'Up',
    '2012',
    True,
    'sim-20160321-2-vc-mu100',
    'dddb-20170721-2',)
setup['MC12Lctaunu_Lcst2593taunu_Down'] = JobConf(
    'MC12Lctaunu_Lcst2593taunu',
    '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim09c/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/15865011/ALLSTREAMS.DST',
    'Down',
    '2012',
    True,
    'sim-20160321-2-vc-md100',
    'dddb-20170721-2',)

# Lcst2593taunu 3pipi0 ---------------------------------
setup['MC12Lctaunu_Lcst2593taunu_3pipi0_Up'] = JobConf(
    'MC12Lctaunu_Lcst2593taunu_3pipi0',
    '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim09c/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/15865016/ALLSTREAMS.DST',
    'Up',
    '2012',
    True,
    'sim-20160321-2-vc-mu100',
    'dddb-20170721-2',)
setup['MC12Lctaunu_Lcst2593taunu_3pipi0_Down'] = JobConf(
    'MC12Lctaunu_Lcst2593taunu_3pipi0',
    '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim09c/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/15865016/ALLSTREAMS.DST',
    'Down',
    '2012',
    True,
    'sim-20160321-2-vc-md100',
    'dddb-20170721-2',)

# Sigmactaunu ---------------------------------
setup['MC12Lctaunu_Sigmactaunu_Up'] = JobConf(
    'MC12Lctaunu_Sigmactaunu',
    '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim09c/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/15865012/ALLSTREAMS.DST',
    'Up',
    '2012',
    True,
    'sim-20160321-2-vc-mu100',
    'dddb-20170721-2',)
setup['MC12Lctaunu_Sigmactaunu_Down'] = JobConf(
    'MC12Lctaunu_Sigmactaunu',
    '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim09c/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/15865012/ALLSTREAMS.DST',
    'Down',
    '2012',
    True,
    'sim-20160321-2-vc-md100',
    'dddb-20170721-2',)

# Sigmactaunu 3pipi0 ---------------------------------
setup['MC12Lctaunu_Sigmactaunu_3pipi0_Up'] = JobConf(
    'MC12Lctaunu_Sigmactaunu_3pipi0',
    '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim09c/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/15865017/ALLSTREAMS.DST',
    'Up',
    '2012',
    True,
    'sim-20160321-2-vc-mu100',
    'dddb-20170721-2',)
setup['MC12Lctaunu_Sigmactaunu_3pipi0_Down'] = JobConf(
    'MC12Lctaunu_Sigmactaunu_3pipi0',
    '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim09c/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/15865017/ALLSTREAMS.DST',
    'Down',
    '2012',
    True,
    'sim-20160321-2-vc-md100',
    'dddb-20170721-2',)

# Sigmacsttaunu ---------------------------------
setup['MC12Lctaunu_Sigmacsttaunu_Up'] = JobConf(
    'MC12Lctaunu_Sigmacsttaunu',
    '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim09c/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/15865013/ALLSTREAMS.DST',
    'Up',
    '2012',
    True,
    'sim-20160321-2-vc-mu100',
    'dddb-20170721-2',)
setup['MC12Lctaunu_Sigmacsttaunu_Down'] = JobConf(
    'MC12Lctaunu_Sigmacsttaunu',
    '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim09c/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/15865013/ALLSTREAMS.DST',
    'Down',
    '2012',
    True,
    'sim-20160321-2-vc-md100',
    'dddb-20170721-2',)

# Sigmacsttaunu 3pipi0 ---------------------------------
setup['MC12Lctaunu_Sigmacsttaunu_3pipi0_Up'] = JobConf(
    'MC12Lctaunu_Sigmacsttaunu_3pipi0',
    '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim09c/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/15865018/ALLSTREAMS.DST',
    'Up',
    '2012',
    True,
    'sim-20160321-2-vc-mu100',
    'dddb-20170721-2',)
setup['MC12Lctaunu_Sigmacsttaunu_3pipi0_Down'] = JobConf(
    'MC12Lctaunu_Sigmacsttaunu_3pipi0',
    '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim09c/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/15865018/ALLSTREAMS.DST',
    'Down',
    '2012',
    True,
    'sim-20160321-2-vc-md100',
    'dddb-20170721-2',)

# Lc3pi -------------------------------------
setup['MC12_Lc3pi_Up'] = JobConf(
    'MC12Lctaunu_Lc3pi',
    '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim09c/Trig0x409f0045/Reco14c/Stripping21Filtered/15464000/LB2LCXC.STRIPTRIG.DST',
    'Up',
    '2012',
    True,
    'sim-20160321-2-vc-mu100',
    'dddb-20170721-2')
setup['MC12Lc3pi_Down'] = JobConf(
    'MC12Lctaunu_Lc3pi',
    '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim09c/Trig0x409f0045/Reco14c/Stripping21Filtered/15464000/LB2LCXC.STRIPTRIG.DST',
    'Down',
    '2012',
    True,
    'sim-20160321-2-vc-md100',
    'dddb-20170721-1',)

# LcDs ----------------------------------
setup['MC12Lctaunu_LcDs_Up'] = JobConf(
    'MC12Lctaunu_LcDs',
    '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim09c/Trig0x409f0045/Reco14c/Stripping21Filtered/15296601/LB2LCXC.STRIPTRIG.DST',
    'Up',
    '2012',
    True,
    'sim-20160321-2-vc-mu100',
    'dddb-20170721-1',)
setup['MC12Lctaunu_LcDs_Down'] = JobConf(
    'MC12Lctaunu_LcDs',
    '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim09c/Trig0x409f0045/Reco14c/Stripping21Filtered/15296601/LB2LCXC.STRIPTRIG.DST',
    'Down',
    '2012',
    True,
    'sim-20160321-2-vc-md100',
    'dddb-20170721-1',)

# LcD0 ----------------------------------
setup['MC12Lctaunu_LcD0_Up'] = JobConf(
    'MC12Lctaunu_LcD0',
    '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim09c/Trig0x409f0045/Reco14c/Stripping21Filtered/15894401/LB2LCXC.STRIPTRIG.DST',
    'Up',
    '2012',
    True,
    'sim-20160321-2-vc-mu100',
    'dddb-20170721-1',)
setup['MC12Lctaunu_LcD0_Down'] = JobConf(
    'MC12Lctaunu_LcD0',
    '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim09c/Trig0x409f0045/Reco14c/Stripping21Filtered/15894401/LB2LCXC.STRIPTRIG.DST',
    'Down',
    '2012',
    True,
    'sim-20160321-2-vc-md100',
    'dddb-20170721-1',)

# LcD- ----------------------------------
setup['MC12Lctaunu_LcD-_Up'] = JobConf(
    'MC12Lctaunu_LcD-',
    '/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim09c/Trig0x409f0045/Reco14c/Stripping21Filtered/15895401/LB2LCXC.STRIPTRIG.DST',
    'Up',
    '2012',
    True,
    'sim-20160321-2-vc-mu100',
    'dddb-20170721-1',)
setup['MC12Lctaunu_LcD-_Down'] = JobConf(
    'MC12Lctaunu_LcD-',
    '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim09c/Trig0x409f0045/Reco14c/Stripping21Filtered/15895401/LB2LCXC.STRIPTRIG.DST',
    'Down',
    '2012',
    True,
    'sim-20160321-2-vc-md100',
    'dddb-20170721-1',)

# old LcDs --------------------------------
setup['MC12Lctaunu_LcXc_Down'] = JobConf(
    'MC12LcXc',
    '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim09a/Trig0x409f0045/Reco14c/Stripping21Filtered/15296600/LBLCDS3PIN.STRIPTRIG.DST',
    'Down',
    '2012',
    True,
    'sim-20160321-2-vc-md100',
    'dddb-20170721-1',)
setup['MC12Lctaunu_LcXc_Up'] = JobConf(
    'MC12LcXc',
    '/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim09a/Trig0x409f0045/Reco14c/Stripping21Filtered/15296600/LBLCDS3PIN.STRIPTRIG.DST',
    'Up',
    '2012',
    True,
    'sim-20160321-2-vc-mu100',
    'dddb-20170721-1',)

# 2016 configuration ------------------------------------------------------------------------------------------------------------------

# tau3pi ------------------------------------
setup['MC16Lctaunu_tau3pi_Up'] = JobConf(
    'MC16Lctaunu_tau3pi',
    '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1Filtered/15863030/LB2LCXC.STRIPTRIG.DST',
    'Up',
    '2016',
    True,
    "sim-20170721-2-vc-mu100",
    "dddb-20170721-3")
setup['MC16Lctaunu_tau3pi_Down'] = JobConf(
    'MC16Lctaunu_tau3pi',
    '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1Filtered/15863030/LB2LCXC.STRIPTRIG.DST',
    'Down',
    '2016',
    True,
    "sim-20170721-2-vc-md100",
    "dddb-20170721-3")

# tau3pipi0 ---------------------------------
setup['MC16Lctaunu_tau3pipi0_Up'] = JobConf(
    'MC16Lctaunu_tau3pipi0',
    '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1Filtered/15863040/LB2LCXC.STRIPTRIG.DST',
    'Up',
    '2016',
    True,
    "sim-20170721-2-vc-mu100",
    "dddb-20170721-3")
setup['MC16Lctaunu_tau3pipi0_Down'] = JobConf(
    'MC16Lctaunu_tau3pipi0',
    '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1Filtered/15863040/LB2LCXC.STRIPTRIG.DST',
    'Down',
    '2016',
    True,
    "sim-20170721-2-vc-md100",
    "dddb-20170721-3")

# Lcst2625taunu ------------------------------------
setup['MC16Lctaunu_Lcst2625taunu_Up'] = JobConf(
    'MC16Lctaunu_Lcst2625taunu',
    '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/15865010/ALLSTREAMS.DST',
    'Up',
    '2016',
    True,
    "sim-20170721-2-vc-mu100",
    "dddb-20170721-3")
setup['MC16Lctaunu_Lcst2625taunu_Down'] = JobConf(
    'MC16Lctaunu_Lcst2625taunu',
    '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/15865010/ALLSTREAMS.DST',
    'Down',
    '2016',
    True,
    "sim-20170721-2-vc-md100",
    "dddb-20170721-3")

# Lcst2593taunu ---------------------------------
setup['MC16Lctaunu_Lcst2593taunu_Up'] = JobConf(
    'MC16Lctaunu_Lcst2593taunu',
    '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/15865011/ALLSTREAMS.DST',
    'Up',
    '2016',
    True,
    "sim-20170721-2-vc-mu100",
    "dddb-20170721-3")
setup['MC16Lctaunu_Lcst2593taunu_Down'] = JobConf(
    'MC16Lctaunu_Lcst2593taunu',
    'MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/15865011/ALLSTREAMS.DST',
    'Down',
    '2016',
    True,
    "sim-20170721-2-vc-md100",
    "dddb-20170721-3")

# Lc3pi ---------------------------------
setup['MC16Lctaunu_Lc3pi_Up'] = JobConf(
    'MC16Lctaunu_Lc3pi',
    '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1Filtered/15464000/LB2LCXC.STRIPTRIG.DST',
    'Up',
    '2016',
    True,
    "sim-20170721-2-vc-mu100",
    "dddb-20170721-3")
setup['MC16Lctaunu_Lc3pi_Down'] = JobConf(
    'MC16Lctaunu_Lc3pi',
    '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1Filtered/15464000/LB2LCXC.STRIPTRIG.DST',
    'Down',
    '2016',
    True,
    "sim-20170721-2-vc-md100",
    "dddb-20170721-3")

# LcDs ----------------------------------
setup['MC16Lctaunu_LcDs_Up'] = JobConf(
    'MC16Lctaunu_LcDs',
    '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1Filtered/15296601/LB2LCXC.STRIPTRIG.DST',
    'Up',
    '2016',
    True,
    "sim-20170721-2-vc-mu100",
    "dddb-20170721-3")
setup['MC16Lctaunu_LcDs_Down'] = JobConf(
    'MC16Lctaunu_LcDs',
    '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1Filtered/15296601/LB2LCXC.STRIPTRIG.DST',
    'Down',
    '2016',
    True,
    "sim-20170721-2-vc-md100",
    "dddb-20170721-3")

# LcD0 ----------------------------------
setup['MC16Lctaunu_LcD0_Up'] = JobConf(
    'MC16Lctaunu_LcD0',
    '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1Filtered/15894401/LB2LCXC.STRIPTRIG.DST',
    'Up',
    '2016',
    True,
    "sim-20170721-2-vc-mu100",
    "dddb-20170721-3")
setup['MC16Lctaunu_LcD0_Down'] = JobConf(
    'MC16Lctaunu_LcD0',
    '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1Filtered/15894401/LB2LCXC.STRIPTRIG.DST',
    'Down',
    '2016',
    True,
    "sim-20170721-2-vc-md100",
    "dddb-20170721-3")

# LcD- ----------------------------------
setup['MC16Lctaunu_LcD-_Up'] = JobConf(
    'MC16Lctaunu_LcD-',
    '/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1Filtered/15895401/LB2LCXC.STRIPTRIG.DST',
    'Up',
    '2016',
    True,
    "sim-20170721-2-vc-mu100",
    "dddb-20170721-3")
setup['MC16Lctaunu_LcD-_Down'] = JobConf(
    'MC16Lctaunu_LcD-',
    '/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1Filtered/15895401/LB2LCXC.STRIPTRIG.DST',
    'Down',
    '2016',
    True,
    "sim-20170721-2-vc-md100",
    "dddb-20170721-3")

# Lcst3pi --------------------------------------
setup['MC16Lctaunu_Lcst2593_3pi_Up'] = JobConf(
    'MC16Lctaunu_Lcst2593_3pi',
    'MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c-ReDecay01/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/15466010/ALLSTREAMS.MDST',
    'Up',
    '2016',
    True,
    'sim-20170721-2-vc-mu100',
    'dddb-20170721-3', )
setup['MC16Lctaunu_Lcst2593_3pi_Down'] = JobConf(
    'MC16Lctaunu_Lcst2593_3pi',
    'MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c-ReDecay01/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/15466010/ALLSTREAMS.MDST',
    'Down',
    '2016',
    True,
    'sim-20170721-2-vc-md100',
    'dddb-20170721-3', )

setup['MC16Lctaunu_Lcst2625_3pi_Up'] = JobConf(
    'MC16Lctaunu_Lcst2625_3pi',
    'MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c-ReDecay01/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/15466011/ALLSTREAMS.MDST',
    'Up',
    '2016',
    True,
    'sim-20170721-2-vc-mu100',
    'dddb-20170721-3', )
setup['MC16Lctaunu_Lcst2625_3pi_Down'] = JobConf(
    'MC16Lctaunu_Lcst2625_3pi',
    'MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c-ReDecay01/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/15466011/ALLSTREAMS.MDST',
    'Down',
    '2016',
    True,
    'sim-20170721-2-vc-md100',
    'dddb-20170721-3', )
# 2015 configuration ------------------------------------------------------------------------------------------------------------------

# tau3pi ------------------------------------
setup['MC15Lctaunu_tau3pi_Up'] = JobConf(
    'MC15Lctaunu_tau3pi',
    '',
    'Up',
    '2015',
    True,
    "",
    "")
setup['MC15Lctaunu_tau3pi_Down'] = JobConf(
    'MC15Lctaunu_tau3pi',
    '',
    'Down',
    '2015',
    True,
    "",
    "")

# tau3pipi0 ---------------------------------
setup['MC15Lctaunu_tau3pipi0_Up'] = JobConf(
    'MC15Lctaunu_tau3pipi0',
    '',
    'Up',
    '2015',
    True,
    "",
    "")
setup['MC15Lctaunu_tau3pipi0_Down'] = JobConf(
    'MC15Lctaunu_tau3pipi0',
    '',
    'Down',
    '2015',
    True,
    "",
    "")

# Lc3pi ---------------------------------
setup['MC15Lctaunu_Lc3pi_Up'] = JobConf(
    'MC15Lctaunu_Lc3pi',
    '',
    'Up',
    '2015',
    True,
    "",
    "")
setup['MC15Lctaunu_Lc3pi_Down'] = JobConf(
    'MC15Lctaunu_Lc3pi',
    '',
    'Down',
    '2015',
    True,
    "",
    "")

# LcDs ----------------------------------
setup['MC15Lctaunu_LcDs_Up'] = JobConf(
    'MC15Lctaunu_LcDs',
    '',
    'Up',
    '2015',
    True,
    "",
    "")
setup['MC15Lctaunu_LcDs_Down'] = JobConf(
    'MC15Lctaunu_LcDs',
    '',
    'Down',
    '2015',
    True,
    "",
    "")

# LcD0 ----------------------------------
setup['MC15Lctaunu_LcD0_Up'] = JobConf(
    'MC15Lctaunu_LcD0',
    '',
    'Up',
    '2015',
    True,
    "",
    "")
setup['MC15Lctaunu_LcD0_Down'] = JobConf(
    'MC15Lctaunu_LcD0',
    '',
    'Down',
    '2015',
    True,
    "",
    "")

# LcD- ----------------------------------
setup['MC15Lctaunu_LcD-_Up'] = JobConf(
    'MC15Lctaunu_LcD-',
    '',
    'Up',
    '2015',
    True,
    "",
    "")
setup['MC15Lctaunu_LcD-_Down'] = JobConf(
    'MC15Lctaunu_LcD-',
    '',
    'Down',
    '2015',
    True,
    "",
    "")

# Data configuration

# Data 11 ------------------------------
setup['Data11_Up'] = JobConf(
    'Data11',
    '/LHCb/Collision11/Beam3500GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21r1/90000000/BHADRONCOMPLETEEVENT.DST',
    'Up',
    '2011',
    False,
    '',
    '',
)
setup['Data11_Down'] = JobConf(
    'Data11',
    '/LHCb/Collision11/Beam3500GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21r1/90000000/BHADRONCOMPLETEEVENT.DST',
    'Down',
    '2011',
    False,
    '',
    '',
)

# Data 12 ------------------------------
setup['Data12_Up'] = JobConf(
    'Data12',
    '/LHCb/Collision12/Beam4000GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21/90000000/BHADRONCOMPLETEEVENT.DST',
    'Up',
    '2012',
    False,
    '',
    '',
)
setup['Data12_Down'] = JobConf(
    'Data12',
    '/LHCb/Collision12/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21/90000000/BHADRONCOMPLETEEVENT.DST',
    'Down',
    '2012',
    False,
    '',
    '',
)

# Data 15 ------------------------------
setup['Data15_Up'] = JobConf(
    'Data15',
    '/LHCb/Collision15/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco15a/Stripping24r1/90000000/BHADRONCOMPLETEEVENT.DST',
    'Up',
    '2015',
    False,
    '',
    '',
)
setup['Data15_Down'] = JobConf(
    'Data15',
    '/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24r1/90000000/BHADRONCOMPLETEEVENT.DST',
    'Down',
    '2015',
    False,
    '',
    '',
)

# Data 16 ------------------------------
setup['Data16_Up'] = JobConf(
    'Data16',
    '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco16/Stripping28r1/90000000/BHADRONCOMPLETEEVENT.DST',
    'Up',
    '2016',
    False,
    '',
    '',
)
setup['Data16_Down'] = JobConf(
    'Data16',
    '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Stripping28r1/90000000/BHADRONCOMPLETEEVENT.DST',
    'Down',
    '2016',
    False,
    '',
    '',
)

if __name__ == "GangaCore.GPI":
    # A parser to give to the script the names of jobs one wants to submit
    # In shell: lb-run Ganga ganga Lb2Lctaunu_ganga.py --jobs Data12 Data15 Lc3pi LcXc ... will create and submit of the jobs with the matching names
    parser = argparse.ArgumentParser()
    parser.add_argument('--jobs', nargs='+', help="specify the names of the desired jobs")
    parser.add_argument('--polarity', help="specifiy the desired polarity of the job")
    parser.add_argument('--all', help="process all job configurations")
    args = parser.parse_args()

    # If polarity exists, it should be Up or Down
    if args.polarity and args.polarity not in ["Up", "Down"]:
        raise Exception("not a polarity option (Up or Down)")

    # According to the parsing options, create a filtered setup from the default one
    if args.all:
        setup_filtered = setup
    elif args.polarity:
        setup_filtered = {jobname:jobconf for jobname,jobconf in setup.iteritems() if jobconf.polarity == args.polarity
                          and jobconf.name in args.jobs}
    else:
        setup_filtered = {jobname:jobconf for jobname,jobconf in setup.iteritems() if jobconf.name in args.jobs}

    # Loop over the setup configurations to create and submit jobs according to the parsing options
    for jobconf in setup_filtered:
        if setup[jobconf].year in ["2015", "2016"]:
            strp_file = "Lb2Lctaunu_strp_run2.py"
            run_option = "run2"
        else:
            strp_file = "Lb2Lctaunu_strp.py"
            run_option = "run1"
        with open(project_root+'/Phys/B2XTauNuTools/python/Lb2LcTauNu/'+strp_file, 'r') as input_file,open(project_root+'/Phys/B2XTauNuTools/python/Lb2LcTauNu/'+'generated_strp_conf/Lb2Lctaunu_{0}_{1}_strp_{2}.py'.format(
                    setup[jobconf].name,
                    setup[jobconf].polarity,
                    run_option), "w") as output_file:
            options = dict.fromkeys(["YEAR", "SIMULATION", "CONDBTAG", "DDDBTAG"], "")
            options["YEAR"] = "'{0}'".format(setup[jobconf].year)
            options["SIMULATION"] = str(setup[jobconf].isSimu)
            options["CONDDBTAG"] = "'{0}'".format(setup[jobconf].CondDBtag)
            options["DDDBTAG"] = "'{0}'".format(setup[jobconf].DDDBtag)

            for line in input_file:
                for el in options.keys():
                    if el in line:
                        line = line.replace(el, options[el])
                output_file.write(line)

        dv.options = [
            project_root+'/Phys/B2XTauNuTools/python/Lb2LcTauNu/'+'generated_strp_conf/Lb2Lctaunu_{0}_{1}_strp_{2}.py'.format(
                          setup[jobconf].name, setup[jobconf].polarity, run_option)]

        # Get dataset
        bk = BKQuery(dqflag=["OK"], path=setup[jobconf].path, type="Path")
        data = bk.getDataset()

        # Job configuration
        j = Job(name='{0}_{1}_Lb2Lctaunu'.format(setup[jobconf].name, setup[jobconf].polarity), application=dv)
        j.inputdata = data
        j.backend = Dirac()

        if setup[jobconf].isSimu == True:
            j.splitter = SplitByFiles(filesPerJob=1)
        else:
            j.splitter = SplitByFiles(filesPerJob=10)

        # Output as DiracFile if Data and LocalFile if MC
        if setup[jobconf].isSimu == True:
            j.outputfiles = [
                LocalFile('DVNtuple.root'),
                LocalFile('DVHistos.root')]
        else:
            j.outputfiles = [
                DiracFile('DVNtuple.root'),
                LocalFile('DVHistos.root')]

        j.do_auto_resubmit = False
        j.submit()
