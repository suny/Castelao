###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *

import os
import sys
import inspect
from B2XTauNuTools.TupleHelper import *

# realpath() with make your script run, even if you symlink it :)
cmd_folder = os.path.realpath(
    os.path.abspath(
        os.path.split(
            inspect.getfile(inspect.currentframe()))[0]))
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

outputLevel = INFO

SeqTupleLb2LctaunuWS = GaudiSequencer("SeqTupleLb2LctaunuWS")
TupleLb2LctaunuWS = DecayTreeTuple("Lb2LctaunuWSTuple")
TupleLb2LctaunuWS.OutputLevel = outputLevel
TupleLb2LctaunuWS.Inputs = ["Phys/Lb2LcTauNuWSForB2XTauNu/Particles"]

TupleLb2LctaunuWS.Decay = "[(Lambda_b0 -> ^(Lambda_c+ -> ^p+ ^K- ^pi+) ^(tau+ -> ^pi+ ^pi- ^pi+ ))]CC"
TupleLb2LctaunuWS.Branches = {
    "Lambda_b0" : "[(Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (tau+ -> pi+ pi- pi+))]CC",
    "tau_pion" : "[(Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (tau+ -> ^pi+ ^pi- ^pi+))]CC",
    "lc_p" : "[(Lambda_b0 -> (Lambda_c+ -> ^p+ K- pi+) (tau+ -> pi+ pi- pi+))]CC",
    "lc_pion" : "[(Lambda_b0 -> (Lambda_c+ -> p+ K- ^pi+) (tau+ -> pi+ pi- pi+))]CC",
    "lc_kaon" : "[(Lambda_b0 -> (Lambda_c+ -> p+ ^K- pi+) (tau+ -> pi+ pi- pi+))]CC",
    "tau" : "[(Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) ^(tau+ -> pi+ pi- pi+))]CC",
    "Lambda_c" : "[(Lambda_b0 -> ^(Lambda_c+ -> p+ K- pi+) (tau+ -> pi+ pi- pi+))]CC"}

TupleLb2LctaunuWS.addTool(TupleToolDecay, name = 'Lambda_b0')
TupleLb2LctaunuWS.addTool(TupleToolDecay, name = 'Lambda_c')
TupleLb2LctaunuWS.addTool(TupleToolDecay, name = 'lc_p')
TupleLb2LctaunuWS.addTool(TupleToolDecay, name = 'lc_kaon')
TupleLb2LctaunuWS.addTool(TupleToolDecay, name = 'lc_pion')
TupleLb2LctaunuWS.addTool(TupleToolDecay, name = 'tau')
TupleLb2LctaunuWS.addTool(TupleToolDecay, name = 'tau_pion')

FinalStateConfig(TupleLb2LctaunuWS.lc_p)
FinalStateConfig(TupleLb2LctaunuWS.tau_pion)
FinalStateConfig(TupleLb2LctaunuWS.lc_pion)
FinalStateConfig(TupleLb2LctaunuWS.lc_kaon)

DalitzConfig(TupleLb2LctaunuWS.tau)
DalitzConfig(TupleLb2LctaunuWS.Lambda_c)

IsoConfig(TupleLb2LctaunuWS.tau)
IsoConfig(TupleLb2LctaunuWS.Lambda_b0)
#LifetimeConfig(TupleLb2LctaunuWS.tau)

ParentConfig(TupleLb2LctaunuWS.tau)
ParentConfig(TupleLb2LctaunuWS.Lambda_c)
ParentConfig(TupleLb2LctaunuWS.Lambda_b0)

CommonConfig(TupleLb2LctaunuWS.Lambda_b0)
CommonConfig(TupleLb2LctaunuWS.Lambda_c)
CommonConfig(TupleLb2LctaunuWS.lc_p)
CommonConfig(TupleLb2LctaunuWS.lc_kaon)
CommonConfig(TupleLb2LctaunuWS.lc_pion)
CommonConfig(TupleLb2LctaunuWS.tau)
CommonConfig(TupleLb2LctaunuWS.tau_pion)
# Kinematic fit is using TupleToolB2XTauNuFit
# which is not used in the analysis
# which causes bugs with Lb2Lctaunu events
#KinematicFitConfig(TupleLb2LctaunuWS.Lambda_b0,isMC)

DTFConfig(TupleLb2LctaunuWS, 'Lambda_c+')

TISTOSToolConfig(TupleLb2LctaunuWS.Lambda_b0, run="run2")
TISTOSToolConfig(TupleLb2LctaunuWS.Lambda_c, run="run2")
TISTOSToolConfig(TupleLb2LctaunuWS.tau, run="run2")
TupleToolConfig(TupleLb2LctaunuWS)

TupleToolIsoGenericConfig(TupleLb2LctaunuWS)

# Specific MC tools
TupleToolMCConfig(TupleLb2LctaunuWS)

GaudiSequencer("SeqTupleLb2LctaunuWS").Members.append(TupleLb2LctaunuWS)
SeqTupleLb2LctaunuWS.IgnoreFilterPassed = True
GaudiSequencer("SeqSelLb2LctaunuWS").Members.append(SeqTupleLb2LctaunuWS)
