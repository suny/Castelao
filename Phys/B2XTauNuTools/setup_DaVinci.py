###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
This is a script aiming to help to configure a DaVinci environment for B2XTauNu analyses
It is using DaVinci v44r6 and can be easily extended to another version by updating the versions
of the projects accordingly
'''
from subprocess import call
import argparse, os

# parsing options
parser = argparse.ArgumentParser()

parser.add_argument("--version", help="version of DaVinci")
parser.add_argument("--name", help="name of the folder")

args = parser.parse_args()

if args.version[0]!='v' :
    raise Exception("not an actual version of DaVinci")

if not args.name:
    folder_name = "DaVinciDev_{0}".format(args.version)
else:
    folder_name = "DaVinciDev_{0}".format(args.name)

# create the DaVinci folder
call(["lb-dev", "--name", format(folder_name), "DaVinci", "{0}".format(args.version)])

oldpath = os.getcwd()
os.chdir(oldpath+"/{0}".format(folder_name))

options = dict(
    DaVinci=["v44r6", "Phys/DaVinci"],
    Analysis=["v20r6", "Phys/DecayTreeTuple", "Phys/DecayTreeTupleMC","Phys/DaVinciTrackScaling"],
    Castelao=["v1r3", "Phys/B2XTauNuTools"],
)

# setup git lb-use with all the projects we need through `git lb-use project`
for project in options.keys():
    call(["git", "lb-use", project])

# setup git lb-checkout will all the packages and the appropriate versions of the projects we need
# Loop over the projects and then loop over the packages before calling `git lb-checkout Project/Version Package`
for project in options:
    for package in options[project][1:]:
        call(["git", "lb-checkout", "{0}/{1}".format(project, options[project][0]), package])

# copy the files in Phys/B2XTauNuTools/forDecayTreeTuple in Phys/DecayTreeTuple/src
# and copy CMakesLists.txt from forDecayTreetuple to DecayTreeTuple/
call('cp Phys/B2XTauNuTools/forDecayTreeTuple/*.{cpp,h,C} Phys/DecayTreeTuple/src', shell=True)
call('mv Phys/B2XTauNuTools/forDecayTreeTuple/forDecayTreeTuple_CMakeLists.txt Phys/DecayTreeTuple/CMakeLists.txt', shell=True)

# make configure && make -j8
call('make configure && make -j8', shell=True)

os.chdir(oldpath)
