/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TUPLETOOLEWTRACKISOLATION_H
#define TUPLETOOLEWTRACKISOLATION_H 1     
     // Include files
     // from DaVinci, this is a specialized GaudiAlgorithm
     #include "Kernel/IParticleTupleTool.h"
     #include "DecayTreeTupleBase/TupleToolBase.h"
     
     class TupleToolEWTrackIsolation : public TupleToolBase, virtual public IParticleTupleTool {
         
    public: 
            TupleToolEWTrackIsolation( const std::string &type, 
                                                           const std::string &name,
                                                           const IInterface *parent );
         
   
      virtual ~TupleToolEWTrackIsolation();
      virtual StatusCode initialize();
           virtual StatusCode fill( const LHCb::Particle*,
                                                             const LHCb::Particle*,
                                                             const std::string&,
                                                             Tuples::Tuple& ); protected:
 private:
    
      double m_minConeRadius;
         
            double m_maxConeRadius;
         
            double m_coneStepSize;
      int m_trackType;
         
            std::string  m_extraParticlesLocation;
          
            std::string  m_extraPhotonsLocation;
   
      bool m_fillComponents;
          
      std::vector<const LHCb::Particle*> m_decayParticles;
        
      void saveDecayParticles( const LHCb::Particle *top );
      StatusCode ChargedCone( const LHCb::Particle *seed,
                                               const LHCb::Particles *parts,
                                               const double rcut,
                                               int &mult,
                                               std::vector<double> &vP,
                                               double &sPt,
                                               int    &maxPt_Q,
                                               double &maxPt_PX,
                                               double &maxPt_PY,
                                               double &maxPt_PZ,
                                               double &maxPt_PE );
   
      StatusCode NeutralCone( const LHCb::Particle *seed,
                                                           const LHCb::Particles *parts,
                                                           const double rcut,
                                                           int &mult,
                                                           std::vector<double> &vP,
                                                           double &sPt,
                                               double &maxPt_PX,
                                               double &maxPt_PY,
                                               double &maxPt_PZ );
      bool isTrackInDecay( const LHCb::Track *track );    
    }
      ;
#endif // TUPLETOOLTRACKISOLATION_H
