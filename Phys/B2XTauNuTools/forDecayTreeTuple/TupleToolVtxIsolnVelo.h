/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MPATEL_TUPLETOOLVTXISOLNPLUS_H
#define MPATEL_TUPLETOOLVTXISOLNPLUS_H 1

// Include files
// from Gaudi
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Kernel/IParticleTupleTool.h"            // Interface
#include "GaudiKernel/ToolHandle.h"

class IDVAlgorithm;

namespace LHCb {
  class Particle;
  class Vertex;
  class Track;
  class State;
}

/** @class TupleToolVtxIsolnVelo TupleToolVtxIsolnVelo.h
 *
 * \brief Fill isolation information for DecayTreeTuple
 *
 * - head_NOPARTWITHINDCHI2WDW : no. of non-signal particles that when added to vertex give delta chi2 < specified window
 * - head_NOPARTWITHINCHI2WDW : no. of non-signal particles that when added to vertex give chi2 < specified window
 * head_SMALLESTCHI2: chi2 of smallest chi2 combination with any of the input Particles
 * head_SMALLESTDELTACHI2: delta chi2 of smallest delta chi2 combination with any of the input Particles
 *
 * \sa DecayTreeTuple
 *
 *  @todo Maybe one should get Tracks instead of Particles?
 *
 *  @author Mitesh Patel, Patrick Koppenburg
 *  @date   2008-04-15
 */
class TupleToolVtxIsolnVelo : public TupleToolBase,
                          virtual public IParticleTupleTool
{

public:

  /// Standard constructor
  TupleToolVtxIsolnVelo( const std::string& type,
                     const std::string& name,
                     const IInterface* parent);

  virtual ~TupleToolVtxIsolnVelo( ){}; ///< Destructor

  virtual StatusCode initialize();

  StatusCode fill( const LHCb::Particle*
                   , const LHCb::Particle*
                   , const std::string&
                   , Tuples::Tuple& );
  double ipchi2( const LHCb::Track&, 
  	       const LHCb::Vertex&) const;
  double ipchi2( const LHCb::State&, 
  		const LHCb::Vertex&) const;
private:
  std::vector<std::string> m_inputTracks;

};

#endif // MPATEL_TUPLETOOLGEOMETRY_H
