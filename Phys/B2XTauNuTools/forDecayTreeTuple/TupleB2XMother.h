/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TUPLEB2XMOTHER_H 
#define TUPLEB2XMOTHER_H 1

// Include files
// from Gaudi
//#include "GaudiAlg/GaudiTool.h"
#include "TupleB2XMother.h"            // Interface
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Kernel/IParticleTupleTool.h"            // Interface                                                                                                                      
#include "Kernel/IParticle2MCAssociator.h"

/** @class TupleB2XMother TupleB2XMother.h
 *  
 *
 *  @author Benedetto Gianluca Siddi
 *  @date   2016-01-07
 */
// class IDVAlgorithm;
// class IDistanceCalculator;
// class IVertexFit;
// class IPVReFitter;
// class IParticleDescendants;
class IParticle2MCAssociator;

namespace LHCb 
{
  class Particle;
}

class TupleB2XMother : public TupleToolBase, virtual public IParticleTupleTool {
public: 
  bool isStable(int);
  bool isMeson(int);
  typedef std::vector<const LHCb::MCParticle*> MCParticleVector;
  typedef std::vector<const LHCb::Particle*> ParticleVector;

  /// Standard constructor
  TupleB2XMother( const std::string& type, 
                  const std::string& name,
                  const IInterface* parent);

  virtual ~TupleB2XMother( ); ///< Destructor

  virtual StatusCode initialize();

  
  virtual StatusCode fill(const LHCb::Particle*
                          ,const LHCb::Particle*
                          ,const std::string&
                          ,Tuples::Tuple& );
  
  const LHCb::MCParticle* ancestor(const LHCb::MCParticle*) const;

  MCParticleVector create_finalstatedaughterarray_for_mcmother(const LHCb::MCParticle*);

  //IParticle2MCWeightedAssociator *m_assoc;
  
  
protected:

private:

  // IDVAlgorithm* m_dva;
  // IDistanceCalculator *m_dist;
  // const IVertexFit* m_pVertexFit;
  // IPVReFitter* m_pvReFitter;
  // std::string m_typeVertexFit;
  // IParticleDescendants* m_particleDescendants;
  std::vector<std::string> m_InputParticles;
  
  // std::string m_TargetParticle;
  // std::string m_GeneratedParticles;
  
  float m_neutralThetaMax;
  float m_neutralThetaMin;
  float m_chargedThetaMax;
  float m_chargedThetaMin;
  
  bool m_isMC;
  
  IParticle2MCAssociator* m_p2mcAssoc;
  std::string m_p2mcAssocType;
  
};
#endif // TUPLEB2XMOTHER_H
