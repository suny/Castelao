###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
LOCOUT=/afs/cern.ch/user/c/cofitzpa/work/public/DstTauNu/bdt_results_s20r1
DIR1=Bd2DsttaunuWSTuple/DecayTree
DIR2=Bd2DsttaunuNonPhysTuple/DecayTree
DIR3=Bd2DsttaunuTuple/DecayTree

for path in "$@"
do
	fname=$(basename $path .root)
	passname=$fname"_comboBDT.root"

	root -b "AddComboKillerBDTToNtuple.C+(\"$path\",\"$LOCOUT/$passname\",\"$DIR1\")"
	root -b "AddComboKillerBDTToNtuple.C+(\"$path\",\"$LOCOUT/$passname\",\"$DIR2\")"
	root -b "AddComboKillerBDTToNtuple.C+(\"$path\",\"$LOCOUT/$passname\",\"$DIR3\")"
done
