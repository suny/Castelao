/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/**********************************************************************************
 * Project   : TMVA - a Root-integrated toolkit for multivariate data analysis    *
 * Package   : TMVA                                                               *
 * Exectuable: ClassApplication                                                   *
 *                                                                                *
 * Test suit for comparison of Reader and standalone class outputs                *
 **********************************************************************************/

#include "TEventList.h"
#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
#include "TProfile.h"
#include "TRandom3.h"
#include "TMatrixF.h"
#include "TVectorF.h"
#include "TMath.h"
#include "TROOT.h"
#include <fstream>
#include "TMVA/Tools.h"
#include <vector>
#include <iostream>
#include <TFile.h>
#include <TROOT.h>
#include <TSystem.h>
#include <TCint.h>
#include <TMath.h>
#include <TTree.h>
#include <TStopwatch.h>
#include <TFormula.h>
#include <TTreeFormula.h>
#include <TTreeFormulaManager.h>

using namespace std;
#include "combokillerweights/TMVAClassification_BDT.class.C"
void AddComboKillerBDTToNtuple(TString infname, TString outfname, TString infdir)
{

	TFile *input = TFile::Open(infname);
	if(!input){	
		        cout << "Error opening the specified ntuple- is the path you specified correct?" << endl;
			exit(1);
	}
	TTree* theTree; 
	input->GetObject(infdir,theTree);
	        if(!theTree){
		        cout << "Error opening the specified ntuple- is the path you specified correct?" << endl;

			        exit(1);
				        }
	TString slash = "/";
	TString name = infdir;
	infdir.Resize(infdir.First(slash)); 
	TFile* sout = new TFile(outfname,"RECREATE");
	if(infdir!=name){
		sout->mkdir(infdir);
		sout->cd(infdir);
	}else{
		sout->cd();
	}   
	TTree *soutTree = theTree->CloneTree(-1,"fast");

	// create a set of variables and declare them to the reader
	// - the variable names must corresponds in name and type to 
	// those given in the weight file(s) that you use

	std::vector<std::string> iVars;
	TObjArray *forms = new TObjArray(1);
	ifstream fileStream;
	fileStream.open("combokillerweights/varscorrs.txt");
	TString line;
	UInt_t num = 0;
	while(!line.ReadLine(fileStream).eof()){
		if(!line.BeginsWith("#")){
			TObjArray* Strings = line.Tokenize(" \t\n;");
			TIter iString(Strings);
			TObjString* os=(TObjString*)iString();
			iVars.push_back(os->String().Data());
			TTreeFormula *form = new TTreeFormula("formula_"+num,os->String(),soutTree);
			forms->Add(form);
			num++;
		}
	}
	fileStream.close();
	std::vector<double> iVals; 
	iVals.resize(iVars.size());

	gROOT->LoadMacro( "combokillerweights/TMVAClassification_BDT.class.C" );
	IClassifierReader* classReader = new ReadBDT  ( iVars );
	Float_t mvaval;
	TBranch *branch = soutTree->Branch("BDT_ALLMCSig_MCNP", &mvaval, "BDT_ALLMCSig_MCNP");
	for (Long64_t ievt=0; ievt<soutTree->GetEntries();ievt++) {
		if (ievt%1000 == 0) cout << "=== Macro        : ... processing event: " << ievt << endl;
		theTree->GetEntry(ievt);
		for(int i=0; i< iVars.size(); i++){
			//	cout << iVars[i];
			Double_t v = ((TTreeFormula*)forms->At(i))->EvalInstance(0);
			//	cout << " : " << v <<endl;
			iVals[i] = v;
		}
		mvaval = classReader->GetMvaValue( iVals );
		//cout << mvaval << endl;
		branch->Fill();
	}
	soutTree->Write();
	sout->Close();
	exit(0);
}
