###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
fname=$(basename $1 .root)

tripiname=$fname"_true3pi.root"
tauname=$fname"_truetau.root"
bkgname=$fname"_bkg.root"


dname=$(dirname $1)


#Are pions really pions? 
truepi="(abs(tau_pion0_TRUEID)==211&&abs(tau_pion1_TRUEID)==211&&abs(tau_pion2_TRUEID)==211)"

#Do pions come from the same parent?
pifromsameparent="(tau_pion0_MC_MOTHER_KEY==tau_pion1_MC_MOTHER_KEY&&tau_pion0_MC_MOTHER_KEY==tau_pion2_MC_MOTHER_KEY)"

#If not, do pions have two with grandparent equal to the third one's mother? 
pifromsamegrapdparent0="(tau_pion0_MC_MOTHER_KEY==tau_pion1_MC_MOTHER_KEY)&&(tau_pion2_MC_MOTHER_KEY==tau_pion0_MC_GD_MOTHER_KEY)"
pifromsamegrapdparent1="(tau_pion1_MC_MOTHER_KEY==tau_pion2_MC_MOTHER_KEY)&&(tau_pion0_MC_MOTHER_KEY==tau_pion1_MC_GD_MOTHER_KEY)"
pifromsamegrapdparent2="(tau_pion2_MC_MOTHER_KEY==tau_pion0_MC_MOTHER_KEY)&&(tau_pion1_MC_MOTHER_KEY==tau_pion2_MC_GD_MOTHER_KEY)"

truedstar="(Dst_BKGCAT==0)"
trueb="(B0_BKGCAT<60)"
truetau="(abs(tau_TRUEID)==15)"

#Dst3pi: with no intermediate tau but maybe an intermediate a1. At least one pion will have MOTHER OR GRANDMOTHER key equal to Dst MOTHER key, at least two with GD MOTHER equal to Dst MOTHER
pimotherdstmother0="($pifromsamegrapdparent0&&(tau_pion0_MC_MOTHER_KEY==Dst_MC_MOTHER_KEY||tau_pion0_MC_GD_MOTHER_KEY==Dst_MC_MOTHER_KEY))"
pimotherdstmother1="($pifromsamegrapdparent1&&(tau_pion1_MC_MOTHER_KEY==Dst_MC_MOTHER_KEY||tau_pion1_MC_GD_MOTHER_KEY==Dst_MC_MOTHER_KEY))"
pimotherdstmother2="($pifromsamegrapdparent2&&(tau_pion2_MC_MOTHER_KEY==Dst_MC_MOTHER_KEY||tau_pion2_MC_GD_MOTHER_KEY==Dst_MC_MOTHER_KEY))"
pimotherdstmother3="($pifromsameparent&&(tau_pion0_MC_MOTHER_KEY==Dst_MC_MOTHER_KEY||tau_pion0_MC_GD_MOTHER_KEY==Dst_MC_MOTHER_KEY))"

#Fully truth-matched Dst3pi:
dst3pitrue="($truedstar&&$trueb&&(!$truetau)&&$truepi&&($pifromsamegrapdparent0||$pifromsamegrapdparent1||$pifromsamegrapdparent2||$pifromsameparent))"

#Dsttau: Check at least one pion has grandmother equal to tau mother, and tau mother same as Dst mother: 
pimothertaumother0="($pifromsamegrapdparent0&&tau_pion0_MC_GD_MOTHER_KEY==tau_MC_MOTHER_KEY&&tau_MC_MOTHER_KEY==Dst_MC_MOTHER_KEY)"
pimothertaumother1="($pifromsamegrapdparent0&&tau_pion1_MC_GD_MOTHER_KEY==tau_MC_MOTHER_KEY&&tau_MC_MOTHER_KEY==Dst_MC_MOTHER_KEY)"
pimothertaumother2="($pifromsamegrapdparent0&&tau_pion2_MC_GD_MOTHER_KEY==tau_MC_MOTHER_KEY&&tau_MC_MOTHER_KEY==Dst_MC_MOTHER_KEY)"
pimothertaumother3="($pifromsameparent&&tau_pion0_MC_GD_MOTHER_KEY==tau_MC_MOTHER_KEY&&tau_MC_MOTHER_KEY==Dst_MC_MOTHER_KEY)"

#Fully truth-matched Dsttau:
dsttautrue="($truetau&&$truedstar&&$trueb&&$truepi&&($pimothertaumother0||$pimothertaumother1||$pimothertaumother2||$pimothertaumother3))"

#Background:
background="((!$dsttautrue)&&(!$dst3pitrue))"

echo $background

cutapplier $1 Bd2DsttaunuTuple/DecayTree $dsttautrue $dname/$tauname | tee $dname/"truth.log"
cutapplier $1 Bd2DsttaunuTuple/DecayTree $dst3pitrue $dname/$tripiname | tee -a $dname/"truth.log"
cutapplier $1 Bd2DsttaunuTuple/DecayTree $background $dname/$bkgname | tee -a $dname/"truth.log"
