/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// local
#include "TupleToolB2XTauNuFit.h"

#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"

#include "Event/Particle.h"

#include <Kernel/IDVAlgorithm.h>
#include <Kernel/GetIDVAlgorithm.h>

#include "TMath.h"
#include "Math/VectorUtil.h"
#include "gsl/gsl_cdf.h"

//-----------------------------------------------------------------------------
// Implementation file for class : TupleToolB2XTauNuFit
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( TupleToolB2XTauNuFit )
//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TupleToolB2XTauNuFit::TupleToolB2XTauNuFit( const std::string& type,
	const std::string& name,
	const IInterface* parent )
: TupleToolBase ( type, name , parent )
, mTau(1776.84)
, mB0(5279.53)
, m_dva(0)
{
	declareInterface<IParticleTupleTool>(this);
	declareProperty("IsMC", m_isMC=true);  
	declareProperty("UseMinuit", m_useMinuit=true); 
}

StatusCode TupleToolB2XTauNuFit::initialize()
{
	const StatusCode sc = TupleToolBase::initialize();
	if ( sc.isFailure() ) return sc;

	m_dva = Gaudi::Utils::getIDVAlgorithm( contextSvc(), this ) ;
	if ( !m_dva ) { return Error( "Couldn't get parent DVAlgorithm", StatusCode::FAILURE ); }
	//m_linkChi2 = new Particle2MCLinker( this, Particle2MCMethod::Chi2,"/Event/Phys/Bd2DstarTauNuForB2XTauNu/Particles"); //CF: Replaced by smartassociator
	//m_mcFinder = tool<IMCDecayFinder>("MCDecayFinder",this); //CF: Replaced by smartassociator
	m_p2mcAssoc = tool<IParticle2MCAssociator>("DaVinciSmartAssociator",this);
	if ( !m_p2mcAssoc ) { return Error( "Couldn't get associator", StatusCode::FAILURE ); }
	m_fit = tool<IBTauFitter>( "BTauFitter", this );
	if ( !m_fit ) { return Error( "Couldn't get fitter", StatusCode::FAILURE ); }

	// B & tau mass squared 
	// CF: moved to initialisation as it's only done once. 
	mB0Sq = mB0*mB0;
	mTauSq = mTau*mTau;

	return sc;
}

//=============================================================================

StatusCode TupleToolB2XTauNuFit::fill( const LHCb::Particle* 
	, const LHCb::Particle* B0
	, const std::string& head
	, Tuples::Tuple& tuple ){

	if( !B0 ) return StatusCode::FAILURE;

	//CF:  Check B has right number of daughters with right PID before doing anything else
	LHCb::Particle::ConstVector products = B0->daughtersVector(); 
	if ( msgLevel(MSG::DEBUG) )debug() << "B has " << products.size() << " products." << endmsg;
	if(products.size()!=2){
		error() << "B0 should have 2 products; one tau, one Dstar" << endmsg;
		return StatusCode::FAILURE;
	}
	LHCb::Particle::ConstVector::const_iterator product1 = products.begin();
	LHCb::Particle::ConstVector::const_iterator product2 = product1+1;
	if ( msgLevel(MSG::DEBUG) )debug() << "product 1: " << (*product1) << ", product 2: " << (*product2) << endmsg;
	if(!(TMath::Abs((*product1)->particleID().pid())==15 || TMath::Abs((*product2)->particleID().pid())==15)){
		error() << "B candidate does not contain a tau candidate!" << endmsg;
		return StatusCode::FAILURE;
	}

	//Now carry on


	double w1(0.25),w2(0.25),w3(0.25),w4(0.25);

	const LHCb::Particle* threePi;
	const LHCb::Particle* Dstar;
	const LHCb::VertexBase* BV;
	const LHCb::VertexBase* DV;

	if(TMath::Abs((*product1)->particleID().pid())==15) {
		threePi = (*product1);
		Dstar = (*product2);
		BV = (*product2)->endVertex();
		DV = threePi->endVertex();

		if ( msgLevel(MSG::DEBUG) ) debug() << "1. pid of tau particle (should be +/-15): " << (threePi)->particleID().pid() << endmsg;
		if ( msgLevel(MSG::DEBUG) ) debug() << "1. tau: " << (threePi) << endmsg;
	}else{
		threePi = (*product2);
		Dstar = (*product1);
		BV = (*product1)->endVertex();
		DV = threePi->endVertex();
		if ( msgLevel(MSG::DEBUG) )debug() << "2. pid of tau particle (should be +/-15): " << (threePi)->particleID().pid() << endmsg;
		if ( msgLevel(MSG::DEBUG) )debug() << "2. tau: " << (threePi) << endmsg;
	}

	//Check the covariance matrices:
	const Gaudi::SymMatrix4x4& TauMomCovm = threePi->momCovMatrix();
  	const Gaudi::SymMatrix4x4& DstMomCovm = Dstar->momCovMatrix();
  	const Gaudi::SymMatrix7x7& TauCovm = threePi->covMatrix();
  	const Gaudi::SymMatrix7x7& DstCovm = Dstar->covMatrix();



  if(msgLevel(MSG::VERBOSE)) verbose() << "3pi momentum covariance matrix: \n" << TauMomCovm << endmsg;
  if(msgLevel(MSG::VERBOSE)) verbose() << "D* momentum covariance matrix: \n" << DstMomCovm << endmsg;
  if(msgLevel(MSG::VERBOSE)) verbose() << "3pi covariance matrix: \n" << TauCovm << endmsg;
  if(msgLevel(MSG::VERBOSE)) verbose() << "D* covariance matrix: \n" << DstCovm << endmsg;


  //check the pos-def nature of the momentum covariance matrices:
  //arbitrary vectors to check the product vT M v
  Vector4 threePi_v(1,-1,34,-8);
  Vector4 Dst_v(1,27,-1890,12);

  double threePi_def = Similarity(TauMomCovm,threePi_v);
  double Dst_def = Similarity(DstMomCovm,Dst_v);

  //For covariance matrices, the detemrinant should be positive as well
  double threePi_det;
  double Dst_det;

  //bool ret_threePi_det = TauMomCovm.Det2(threePi_det);
  //bool ret_Dst_det = DstMomCovm.Det2(Dst_det);

  verbose()<<"3pi def : "<<threePi_def<<endmsg;
  verbose()<<"D* def : "<<Dst_def<<endmsg;
  verbose()<<"3pi determinant : "<<threePi_det<<endmsg;
  verbose()<<"D* determinant : "<<Dst_det<<endmsg;

  //Inverse to see if the momentum covariance matrices are invertible

  int fail_threePi;
  SymMatrix4x4 threePi_Inv = TauMomCovm.Inverse(fail_threePi);
  if(fail_threePi) {
    verbose() << "3pi momentum covariance matrix could not be inversed" << endmsg;
    return StatusCode::FAILURE;
  }

  int fail_Dst;
  SymMatrix4x4 Dst_Inv = DstMomCovm.Inverse(fail_Dst);
  if(fail_Dst) {
    verbose() << "D* momentum covariance matrix could not be inversed" << endmsg;
    return StatusCode::FAILURE;
  }

  verbose()<<"Inverse of threePi matrix : \n"<<threePi_Inv<<endmsg;
  verbose()<<"Inverse of D* matrix : \n"<<Dst_Inv<<endmsg;


  //Inverse Choleski to see if the momentum covariance matrices are invertible

  int fail_threePiChol;
  SymMatrix4x4 threePi_InvChol = TauMomCovm.InverseChol(fail_threePiChol);
  if(fail_threePiChol) {
    verbose() << "3pi momentum covariance matrix could not be inversed" << endmsg;
    return StatusCode::FAILURE;
  }

  int fail_DstChol;
  SymMatrix4x4 Dst_InvChol = DstMomCovm.InverseChol(fail_DstChol);
  if(fail_DstChol) {
    verbose() << "D* momentum covariance matrix could not be inversed" << endmsg;
    return StatusCode::FAILURE;
  }

  verbose()<<"Inverse Choleski of threePi matrix : \n"<<threePi_InvChol<<endmsg;
  verbose()<<"Inverse Choleski of D* matrix : \n"<<Dst_InvChol<<endmsg;

  SymMatrix4x4 threePi_InvDiff = threePi_Inv - threePi_InvChol;
  SymMatrix4x4 Dst_InvDiff = Dst_Inv - Dst_InvChol;

  verbose()<<"Difference in threePi inverse matrices : \n"<<threePi_InvDiff<<endmsg;
  verbose()<<"Difference in D* inverse matrices : \n"<<Dst_InvDiff<<endmsg;




	//CF: Moved up
	const LHCb::VertexBase* PV = m_dva->bestVertex(threePi);
	if(PV==NULL){
		warning() << "PV range is empty" << endmsg;
		return StatusCode::FAILURE;
	}

	//CF: There will only ever be 1 tau, 1 D*... This code isn't needed
	/*
	   int nthTau(0);
	   int nthDstar(0);
	   int nthPion(0);

	   std::vector<int> tau_keys;
	   std::vector<int> Dstar_keys;
	   if(find(tau_keys.begin(),tau_keys.end(),threePi->key())==tau_keys.end()) {
	   tau_keys.push_back(threePi->key());
	   nthTau++;  
	   }

	   if(find(Dstar_keys.begin(),Dstar_keys.end(),Dstar->key())==Dstar_keys.end()) {
	   Dstar_keys.push_back(Dstar->key());
	   nthDstar++;  
	   }
	 */

	   int RightSign = 1;
	   int tau_charge = threePi->charge();
	   int Dst_charge = Dstar->charge();
	if(tau_charge==Dst_charge) RightSign = 0; //CF: This won't work for NP 

	LHCb::RecVertices* PVs = get<LHCb::RecVertices>(LHCb::RecVertexLocation::Primary);
	long nPV = PVs->size();
	if ( msgLevel(MSG::DEBUG) )debug() << "nPV="<<nPV<<endmsg;

	//CF: How do you know the first is always the slow pion? I guess momentum ordering? 
	LHCb::Particle::ConstVector Dstar_daughters = Dstar->daughtersVector();
	const LHCb::Particle *slowPion = Dstar_daughters[0];
	const LHCb::Particle *D0 = Dstar_daughters[1];
	if (UNLIKELY( msgLevel(MSG::VERBOSE))) verbose() << "D0 PID: " << D0->particleID().pid() << " (D0, 421)" << endmsg;

	Gaudi::LorentzVector mom_D0 = D0->momentum();
	//Gaudi::LorentzVector mom_slowPion = slowPion->momentum(); //CF: This is never used

	Gaudi::LorentzVector MCmom;
	Gaudi::LorentzVector MCBnumom;
	Gaudi::LorentzVector MCDstmom;
	Gaudi::LorentzVector MCBmom;
	Gaudi::LorentzVector MCpi1mom;
	Gaudi::LorentzVector MCpi2mom;
	Gaudi::LorentzVector MCpi3mom;
	Gaudi::LorentzVector MCnumom;
	Gaudi::LorentzVector MCWmom;
	Gaudi::LorentzVector MCD0mom;
	//Gaudi::LorentzVector MCslowpimom; //CF: This is never used

	double MCangle_D0_in_Dstframe=0;
	double MCangle_W_in_Bframe=0;
	double MCangle_tau_in_Wframe=0;
	double MCangle_nu_in_tauframe=0;
	double MCangle_Dst_D0_plane_with_W_tau_plane=0;
	double MCangle_W_tau_plane_with_tau_nu_plane=0;

	Gaudi::XYZVector z(0,0,1);
	Gaudi::XYZPoint MCDstarvx;
	Gaudi::XYZPoint MCtauvx;
	Gaudi::XYZPoint MCPV;

	Vector17 MCx0;
	double MClife(0);
	double MCBlife(0);

	//int realDstar=0; //CF: redundant. Use bkgcat tool 
	//int realTau=0;

	bool decay_found = false; //CF: checks that we found the MC decay. If not, write defaults to MC tuple.
	if(m_isMC) {
	  
	  //CF: The old code finds _a_ true D*taunu decay in the event, and uses it whether or not it's associated to the D*taunu we've recoed. 
	  //This isn't ideal as it means you can't compare the true values to the fitted values (there's no guarantee of 1:1 correspondence). 
	  //Instead match the B we found all the way down to the daughters. This way we're consistent with the bkgcat tool.  
	  
	  
	  Assert( m_p2mcAssoc, "The DaVinci smart associator hasn't been initialized!");
	  
	  //CF: Scope the particles outside of the first decay_found check so that we can perform the second check on the tau daughters
	  const LHCb::MCParticle* MCB = NULL;
	  const LHCb::MCParticle* MCDstar = NULL;
	  const LHCb::MCParticle* MCtau = NULL;
	  const LHCb::MCParticle* MCBnu = NULL;
	  const LHCb::MCParticle* MCD0 = NULL;
	  const LHCb::MCParticle* MCslowpi = NULL;
	  const LHCb::MCParticle* MCpi1 = NULL;
	  const LHCb::MCParticle* MCpi2 = NULL;
	  const LHCb::MCParticle* MCpi3 = NULL;
	  const LHCb::MCParticle* MCnu = NULL;
	  
	  MCB = m_p2mcAssoc->relatedMCP(B0); //Make sure the PID of the associated particles is what it should be
	  if(MCB&&abs(MCB->particleID().pid())==511){
	    if ( msgLevel(MSG::DEBUG) ) debug() << "Found the MC B0" << endmsg;
	    MCtau = m_p2mcAssoc->relatedMCP(threePi);
	    if(MCtau&&abs(MCtau->particleID().pid())==15){
	      if ( msgLevel(MSG::DEBUG) ) debug() << "Found the MC tau" << endmsg;
	      MCDstar = m_p2mcAssoc->relatedMCP(Dstar);
	      if(MCDstar&&abs(MCDstar->particleID().pid())==413){
		if ( msgLevel(MSG::DEBUG) ) debug() << "Found the MC D*" << endmsg;
		MCD0 = m_p2mcAssoc->relatedMCP(D0);
		if(MCD0&&abs(MCD0->particleID().pid())==421){
		  if ( msgLevel(MSG::DEBUG) ) debug() << "Found the MC D0" << endmsg;
		  MCslowpi = m_p2mcAssoc->relatedMCP(slowPion);
		  if(MCslowpi&&abs(MCslowpi->particleID().pid())==211){
		    if ( msgLevel(MSG::DEBUG) ) debug() << "Found the MC slowpi" << endmsg;
		    decay_found=true;
		  }
		}
	      }
	    }
	  }
	  
	  /*	const LHCb::MCParticle* MC_check_tau = m_linkChi2->firstMCP(threePi);
	    if(MC_check_tau!=0) {
	    if(TMath::Abs(MC_check_tau->particleID().pid())==15){
	    realTau = 1;
	    }
	    }
	    const LHCb::MCParticle* MC_check_Dstar = m_linkChi2->firstMCP(Dstar);
	    if(MC_check_Dstar!=0) {
	    if(TMath::Abs(MC_check_Dstar->particleID().pid())==413){
	    realDstar = 1;
	    } 
	    }
	    const LHCb::MCParticle::Container* particle_container = get<LHCb::MCParticle::Container>("MC/Particles");
	    if (UNLIKELY( msgLevel(MSG::VERBOSE))) verbose() << "got MCparticle container. It is " << (particle_container->empty()?" empty!":" not empty.") << endmsg;
	    
	    LHCb::MCParticle::ConstVector particles;
	    LHCb::MCParticle::Container::const_iterator part = particle_container->begin();
	    for(;part!=particle_container->end();part++){
	    particles.push_back(*part);
	    }
	    
	    std::string decay = "[B0 -> D*(2010)- tau+ nu_tau]cc";
	    m_mcFinder->setDecay(decay);
	    decay_found = m_mcFinder->findDecay(particles,MCB);
	  */
	  
	  
	  
	  if(!decay_found) { //CF: This printed that the decay _was_ found in the event when it wasnt, and then exited without writing to ntuple, which then segfaults as the tuple doesn't get filled.
	    
	    if ( msgLevel(MSG::DEBUG) ) debug() << "No MC association to this candidate, writing -9999 to MC variables" << endmsg;
	  }else{
	    if ( msgLevel(MSG::DEBUG) ) debug() << "MC Matching found all final state particles" << endmsg;
	    
	    //CF: Check each of the particles and print all PIDs, not just of the D*, tau, Bnu. 
	    //CF: If the tau isn't straight from the B (eg: from Ds, then it won't have a neutrino. 
	    if((MCB->endVertices()[0])->products().size()==3){
	      MCBnu    = ((MCB->endVertices()[0])->products()[2]);
	    }
	    if(!MCBnu||abs(MCBnu->particleID().pid())!=16){
	      decay_found=false;
	      if ( msgLevel(MSG::DEBUG) ) debug() << "Failed to find MC neutrino from the B decay" << endmsg;
	    }else{
	      if ( msgLevel(MSG::DEBUG) ){
		for(unsigned int i=0; i<(MCtau->endVertices()[0])->products().size(); i++ ){
		  debug() << "tau daughter "<< i << " pid: " << ((MCtau->endVertices()[0])->products()[i])->particleID().pid() << endmsg;
		}
	      }
	      if((MCtau->endVertices()[0])->products().size()<4) {
		warning() << "tau particle has " << (MCtau->endVertices()[0])->products().size() << " daughters (expect 4+)" << endmsg;
		
		decay_found = false;
	      }
	    }
	  }
	  
	  if(decay_found){
	    MCpi1 = ((MCtau->endVertices()[0])->products()[0]);
	    MCpi2 = ((MCtau->endVertices()[0])->products()[1]);
	    MCpi3 = ((MCtau->endVertices()[0])->products()[2]);
	    MCnu  = ((MCtau->endVertices()[0])->products()[3]);
	    if(!MCnu||abs(MCnu->particleID().pid())!=16){
	      decay_found=false;
	      if ( msgLevel(MSG::DEBUG) ) debug() << "Failed to find MC tau neutrino!" << endmsg;
	    } //Make sure we find the neutrino
	  }
	  
	  if(decay_found){
	    
	    if (UNLIKELY( msgLevel(MSG::VERBOSE))){
	      verbose() << "MC decay products are:" << endmsg;
	      
	      verbose() << "1: " << MCDstar->particleID().pid() << " (D*, 413)" << endmsg;
	      verbose() << "2: " << MCtau->particleID().pid() << " (tau, 15)" <<endmsg;
	      verbose() << "3: " << MCBnu->particleID().pid() << " (nutau, 16)" << endmsg;
	      verbose() << "4: " << MCD0->particleID().pid() << " (D0, 421)" << endmsg;
	      verbose() << "5: " << MCslowpi->particleID().pid() << " (slowpi, 211)" << endmsg;
	      verbose() << "5: " << MCpi1->particleID().pid() << " (pi1, 211)" << endmsg;
	      verbose() << "6: " << MCpi2->particleID().pid() << " (pi2, 211)" << endmsg;
	      verbose() << "7: " << MCpi3->particleID().pid() << " (pi3, 211)" << endmsg;
	      verbose() << "8: " << MCnu->particleID().pid() << " (nutau, 16)" << endmsg;
			}
	    
	    MCBmom      =  MCB->momentum();
	    MCDstmom    =  MCDstar->momentum();
	    MCmom       =  MCtau->momentum();
	    MCBnumom    =  MCBnu->momentum(); 
	    //MCslowpimom =  MCslowpi->momentum(); //CF: This is never used
	    MCD0mom     =  MCD0->momentum();  
	    MCpi1mom    =  MCpi1->momentum();
	    MCpi2mom    =  MCpi2->momentum();
	    MCpi3mom    =  MCpi3->momentum();
	    MCnumom     =  MCnu->momentum();
	    MCWmom      =  MCmom+MCBnumom;
	    MCtauvx     =((MCtau->endVertices())[0])->position();
	    MCDstarvx   = (MCtau->originVertex())->position();
	    MCPV        = (MCB->originVertex())->position();
	    MClife      = lifetime(MCDstarvx,MCtauvx,MCmom.Vect(),mTau);
	    MCBlife     = lifetime(MCPV,MCDstarvx,MCBmom.Vect(),mB0);
	    
	    double MCL  = (MCtauvx-MCDstarvx).R()/MCmom.P();
	    double MCLB = (MCDstarvx-MCPV).R()/MCBmom.P();
	    if ( msgLevel(MSG::DEBUG) ){
	      debug() << "MCB info: px/py/px/m:  "<<MCBmom.px()  <<"/"<<MCBmom.py()  <<"/"<<MCBmom.pz()  <<"/"<<MCBmom.mass()  <<endmsg;
	      debug() << "MCD* info: px/py/px/m: "<<MCDstmom.px()<<"/"<<MCDstmom.py()<<"/"<<MCDstmom.pz()<<"/"<<MCDstmom.mass()<<endmsg;
	      debug() << "MCD0 info: px/py/px/m: "<<MCD0mom.px()<<"/"<<MCD0mom.py()<<"/"<<MCD0mom.pz()<<"/"<<MCD0mom.mass()<<endmsg;
	      debug() << "MCtau info: px/py/px/m:"<<MCmom.px()   <<"/"<<MCmom.py()   <<"/"<<MCmom.pz()   <<"/"<<MCmom.mass()   <<endmsg;
	      debug() << "MCPV vertex: (" << MCPV.x() << "," << MCPV.y() << ","<< MCPV.z() << ")" << endmsg;
	      debug() << "MCD*vx vertex: (" << MCDstarvx.x() << "," << MCDstarvx.y() << ","<< MCDstarvx.z() << ")" << endmsg;
	      debug() << "MCtauvx vertex: (" << MCtauvx.x() << "," << MCtauvx.y() << ","<< MCtauvx.z() << ")" << endmsg;
	    }
	    
	    double array[] = {MCDstarvx.x(),MCDstarvx.y(),MCDstarvx.z(),
			      MCmom.px()   ,MCmom.py()   ,MCmom.pz(),
			      MCnumom.px() ,MCnumom.py() ,MCnumom.pz(),MCL,
			      MCBmom.px()  ,MCBmom.py()  ,MCBmom.pz(),
			      MCBnumom.px(),MCBnumom.py(),MCBnumom.pz(),MCLB};
	    
	    Vector17 x0(&*array,(unsigned int)17);
	    MCx0 = x0;
	    
	    Gaudi::XYZVector MCperp_D0_Dst = (         MCD0mom.Vect().Unit().Cross( MCDstmom.Vect().Unit() )).Unit();
	    Gaudi::XYZVector MCperp_W_tau  = ((MCmom+MCBnumom).Vect().Unit().Cross(    MCmom.Vect().Unit() )).Unit();
	    Gaudi::XYZVector MCperp_nu_tau = (         MCnumom.Vect().Unit().Cross(    MCmom.Vect().Unit() )).Unit();
	    MCangle_Dst_D0_plane_with_W_tau_plane=MCperp_W_tau.Dot(MCperp_D0_Dst);
	    MCangle_W_tau_plane_with_tau_nu_plane=MCperp_W_tau.Dot(MCperp_nu_tau);  
	    MCangle_D0_in_Dstframe=cosTheta(MCDstmom,MCD0mom);
	    MCangle_nu_in_tauframe=cosTheta(MCmom,MCnumom);
	    MCangle_tau_in_Wframe=cosTheta(MCWmom,MCmom);
	    MCangle_W_in_Bframe=cosTheta(MCBmom,MCWmom);
	  }
	} //END OF is_MC 
	
	
	//CHECK WETHER EXACT CALC IS POSSIBLE
	//	double exactw1(0.25)  ,exactw2(0.25)  ,exactw3(0.25),  exactw4(0.25); //CF: These are never used
	double tauExact_p1(-1),tauExact_p2(-1);
	double BExact1_p1(-1) ,BExact1_p2(-1);
	double BExact2_p1(-1) ,BExact2_p2(-1);
	//	bool tauExact= false; //CF: This is never used
	//	bool BExact1 = false; //CF: This is never used
	//	bool BExact2 = false; //CF: This is never used
	
	//calculate angle between 3pi-3pi
	//Gaudi::XYZVector pipipi_mom = (threePi)->momentum().Vect(); //CF: This is never used
	Gaudi::LorentzVector pipipi_mom4 = (threePi)->momentum();
	
	// angle between the reconstructed inter-vertex spatial vector and the 3pi reconstructed vector
	double thetaold = acos(((threePi)->endVertex()->position()-BV->position()).Dot(pipipi_mom4.Vect())/
			       (sqrt(((threePi)->endVertex()->position()-BV->position()).Mag2())*pipipi_mom4.P()));  
	
	// reconstructed unit spatial vector of the tau in the lab-frame
	Gaudi::XYZVector tauExact_u = (DV->position()-BV->position())/(DV->position()-BV->position()).R();
	// same as thetaold, as far as I can tell... 
	double tauExact_costheta = (pipipi_mom4.Vect().Dot(tauExact_u))/pipipi_mom4.Vect().R();
	
	debug() << "tauExact_u.Mag2(): " << tauExact_u.Mag2() << endmsg;
	debug() << "tauExact_costheta: " << tauExact_costheta << endmsg;
	
	double MCtauExact_costheta = 0;
	//CF: Check not only if we are running on MC, but also that we found a particle
	if(m_isMC && decay_found) { // gen-level unit spatial vector of the tau in the lab-frame 
	  Gaudi::XYZVector MCtauExact_u = (MCtauvx-MCDstarvx)/(MCtauvx-MCDstarvx).R();
	  // angle between gen-level inter-vertex spatial vector and gen-level 3pi vector
	  MCtauExact_costheta = ((MCpi1mom+MCpi2mom+MCpi3mom).Vect().Dot(MCtauExact_u))/(MCpi1mom+MCpi2mom+MCpi3mom).Vect().R();
	  debug() << "MC tauExact_costheta: " << MCtauExact_costheta << endmsg;
	}
	
	// the Keune identity
	double tauExact_a = 4*(pow(pipipi_mom4.E(),2)-pow(pipipi_mom4.P(),2)*pow(tauExact_costheta,2));
	double tauExact_b =-4*(mTauSq+pow(pipipi_mom4.M(),2))*pipipi_mom4.P()*tauExact_costheta;
	double tauExact_c = 4*mTauSq*pow(pipipi_mom4.E(),2)-pow(mTauSq+pow(pipipi_mom4.M(),2),2);
	double tauExact_D = tauExact_b*tauExact_b-4*tauExact_a*tauExact_c;
	debug() << "tauExact_D: " << tauExact_D << endmsg;
	
	if(tauExact_D>0) {
	  //tauExact = true; //CF: This is never used
	  // two solutions for the tau momentum
	  tauExact_p1 = (-tauExact_b + sqrt(tauExact_D))/(2*tauExact_a);
	  tauExact_p2 = (-tauExact_b - sqrt(tauExact_D))/(2*tauExact_a);
	  
	  debug() << "exact_life: " << lifetime(BV,DV,tauExact_p1,mTau) << endmsg;
	  
	  // two solutions for the tau energy
	  double tauExact_e1 = sqrt(mTauSq+pow(tauExact_p1,2));
	  double tauExact_e2 = sqrt(mTauSq+pow(tauExact_p2,2));
	  
	  // solution 1 of the tau momentum components
	  double tauExact_p1x = tauExact_p1*tauExact_u.x();
	  double tauExact_p1y = tauExact_p1*tauExact_u.y();
	  double tauExact_p1z = tauExact_p1*tauExact_u.z();
	  
	  // solution 2 of the tau momentum components
	  double tauExact_p2x = tauExact_p2*tauExact_u.x();
	  double tauExact_p2y = tauExact_p2*tauExact_u.y();
	  double tauExact_p2z = tauExact_p2*tauExact_u.z();
	  
	  //  the two solutions of the analytical calculation
	  Gaudi::LorentzVector tauExact1(tauExact_p1x,tauExact_p1y,tauExact_p1z,tauExact_e1);
	  Gaudi::LorentzVector tauExact2(tauExact_p2x,tauExact_p2y,tauExact_p2z,tauExact_e2);
	  
	  // the two tau+D* analytical solutions
	  Gaudi::LorentzVector DstarTauExact1 = Dstar->momentum()+tauExact1;
	  Gaudi::LorentzVector DstarTauExact2 = Dstar->momentum()+tauExact2;
	  
	  // unit spatial vector from the PV to the reconstructed B decay vertex
	  Gaudi::XYZVector BExact_u = (BV->position()-PV->position())/(BV->position()-PV->position()).R();
	  
	  // angle between the first tau-D* combo and the B flight direction
	  double BExact1_costheta = (DstarTauExact1.Vect().Dot(BExact_u))/DstarTauExact1.Vect().R();
	  // the Keune identity
	  double BExact1_a = 4*(pow(DstarTauExact1.E(),2)-pow(DstarTauExact1.P(),2)*pow(BExact1_costheta,2));
	  double BExact1_b = -4*(pow(mB0,2)+pow(DstarTauExact1.M(),2))*DstarTauExact1.P()*BExact1_costheta;
	  double BExact1_c = 4*pow(mB0,2)*pow(DstarTauExact1.E(),2)-pow(pow(mB0,2)+pow(DstarTauExact1.M(),2),2);  
	  double BExact1_D = BExact1_b*BExact1_b-4*BExact1_a*BExact1_c;
	  if(BExact1_D>0) {
	    //BExact1 = true; CF: This is never used
	    // two B momentum solutions from the first tau solution
	    BExact1_p1 = (-BExact1_b + sqrt(BExact1_D))/(2*BExact1_a);
	    BExact1_p2 = (-BExact1_b - sqrt(BExact1_D))/(2*BExact1_a);
	  }
	  
	  // angle between the second tau-D* combo and the B flight direction
	  double BExact2_costheta = (DstarTauExact2.Vect().Dot(BExact_u))/sqrt(DstarTauExact2.Vect().Mag2());
	  // the Keune identity
	  double BExact2_a = 4*(pow(DstarTauExact2.E(),2)-pow(DstarTauExact2.P(),2)*pow(BExact2_costheta,2));
	  double BExact2_b = -4*(pow(mB0,2)+pow(DstarTauExact2.M(),2))*DstarTauExact2.P()*BExact2_costheta;
	  double BExact2_c = 4*pow(mB0,2)*pow(DstarTauExact2.E(),2)-pow(pow(mB0,2)+pow(DstarTauExact2.M(),2),2);
	  double BExact2_D = BExact2_b*BExact2_b-4*BExact2_a*BExact2_c;
	  if(BExact2_D>0) {
	    //BExact2 = true; //CF: This is never used
	    // two B momentum solutions from the second tau solution
	    BExact2_p1 = (-BExact2_b + sqrt(BExact2_D))/(2*BExact2_a);
	    BExact2_p2 = (-BExact2_b - sqrt(BExact2_D))/(2*BExact2_a);
	  }
	}
	if ( msgLevel(MSG::DEBUG) ){
	  debug() << " Analytical solutions " << endmsg;
	  debug() << "----------------------" << endmsg;
	  if(m_isMC && decay_found) debug()<< "truth: P(tau)= "<<MCmom.P()<<"     P(B)="<<MCBmom.P()<<endmsg;
	  debug() << "soln1: P(tau)= " << tauExact_p1 << " #1: P(B)=" << BExact1_p1 << endmsg;
	  debug() << "               " << tauExact_p1 << " #2: P(B)=" << BExact1_p2 << endmsg;
	  debug() << "soln2: P(tau)= " << tauExact_p2 << " #3: P(B)=" << BExact2_p1 << endmsg;
	  debug() << "               " << tauExact_p2 << " #4: P(B)=" << BExact2_p2 << endmsg;
	}
	
	/*
	//CF: These are never used
	if(!tauExact || (!BExact1&&!BExact2)) { 
	exactw1=0;
	exactw2=0;
	exactw3=0;
	exactw4=0;
	}
	else if(BExact1&&!BExact2) {
	exactw1=0.5;
	exactw2=0.5;
	exactw3=0;
	exactw4=0;
	}
	else if(!BExact1&&BExact2) {
	exactw1=0;
	exactw2=0;
	exactw3=0.5;
	exactw4=0.5;
	}
	//END EXACT SOLUTIONS

	*/ 
	
	
	//STARTING THE FITTING PROCESS
	debug() << "Giving info to fitter program." << endmsg;
	m_fit->setMeasuredData(PV,BV,threePi,Dstar);
	m_fit->setWeights(PV,BV,threePi,Dstar).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
	debug()<< "initial chi2="<<m_fit->chi2()<<endmsg;
	// loop over 3x3=9 starting sets
	int besti(0),bestj(0);
	double bestchi2(99999); 
	for(int i=0; i<3; i++) {
	  for(int j=0; j<3; j++) {
	    m_fit->setInitialGuess(i,j);
	    if(m_useMinuit){ m_fit->performMinuit(); }else{ m_fit->performFit().ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */); }
	    debug() << "trying combination: " << i << " " << j << ", which gives chi2: " << m_fit->chi2()<< endmsg;
	    if(i==0&&j==0) bestchi2 = m_fit->chi2();
	    if(m_fit->chi2()<bestchi2) {
	      bestchi2 = m_fit->chi2();// malcolm added
	      besti = i;
	      bestj = j;
	    } 
	  }
	}
	// identify the best one and redo (so 10 fits in total...)
	if ( msgLevel(MSG::DEBUG) ) debug() << "besti " << besti << ", bestj " << bestj <<", bestchi2 "<<bestchi2<< endmsg;
	m_fit->setInitialGuess(besti,bestj);
	//m_fit->setInitialGuess(MCx0); // Debug: provide the perfect initial guess (the true values)
	if(m_useMinuit){ m_fit->performMinuit(); }else{ m_fit->performFit().ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */); }
	
	// recover the best-fit momentum values - functions called here are declared in BTauFitter.h 
	Gaudi::LorentzVector mom_B      = m_fit->BMomentum();
	Gaudi::LorentzVector mom_Dst    = m_fit->DstMomentum();
	Gaudi::LorentzVector mom_Bnu    = m_fit->nu2Momentum();
	Gaudi::LorentzVector mom_tau    = m_fit->tauMomentum(); 
	Gaudi::LorentzVector mom_nu     = m_fit->nuMomentum();
	Gaudi::LorentzVector mom_pipipi = m_fit->pipipiMomentum();
	// recover the best-fit position values
	Gaudi::XYZPoint ev = m_fit->eVertex();
	Gaudi::XYZPoint bv = m_fit->bVertex();
	Gaudi::XYZPoint pv = m_fit->PVertex();
	
	//Added by Donal 9/4/13 - recover the measured errors on each of the quantities (functions defined in BTauFitter.h)
	Gaudi::XYZPoint evErr = m_fit->eVertexErr();
	Gaudi::XYZPoint bvErr = m_fit->bVertexErr();
	Gaudi::XYZPoint pvErr = m_fit->PVertexErr();
	//Momentum 4-vector filled with px,py,pz,m2 measured uncertainties
	Gaudi::LorentzVector momErr_threePi = m_fit->pipipiMomentumErr();
	Gaudi::LorentzVector momErr_Dst = m_fit->DstMomentumErr();  
	
	//Retrieve components of errors - all 17 components used in the fitter as input observables
	double evErr_x = evErr.x();
	double evErr_y = evErr.y();
	double evErr_z = evErr.z();
	double bvErr_x = bvErr.x();
	double bvErr_y = bvErr.y();
	double bvErr_z = bvErr.z();
	double pvErr_x = pvErr.x();
	double pvErr_y = pvErr.y();
	double pvErr_z = pvErr.z();
	double pipipiErr_px = momErr_threePi.px();
	double pipipiErr_py = momErr_threePi.py();
	double pipipiErr_pz = momErr_threePi.pz();
	double pipipiErr_m2 = momErr_threePi.e(); //fourth component is in terms of m2, not E - taken from Vnew, which is the Jacobian transformed covariance matrix
	double DstErr_px = momErr_Dst.px();
	double DstErr_py = momErr_Dst.py();
	double DstErr_pz = momErr_Dst.pz();
	double DstErr_m2 = momErr_Dst.e();
	
	
	
	//double prob(0); //CF: This is never used
	double chi2 = m_fit->chi2();
	//const double probLimit = 1e-15; //CF: This is never used
	//double chi2Max = gsl_cdf_chisq_Qinv(probLimit,17); //CF: This is never used
	//prob = chi2 < chi2Max ? gsl_cdf_chisq_Q(chi2,17):0; //CF: This is never used
	if ( msgLevel(MSG::DEBUG) ) debug() << "tau momentum: " << mom_tau.P() << "MeV/c  chi2: " << chi2 << endmsg;
	
	// angle between the tau and the 3pi (reconstructed)
	double theta = acos(mom_tau.Vect().Dot(mom_pipipi.Vect())/(mom_tau.P()*mom_pipipi.P()));
	if ( msgLevel(MSG::DEBUG) ) debug() << "theta: " << theta << endmsg;
	theta = acos((ev-bv).Dot(mom_pipipi.Vect())/(sqrt((ev-bv).mag2())*mom_pipipi.P()));
	if ( msgLevel(MSG::DEBUG) ) debug() << "theta: " << theta <<" (should match prev. line)"<< endmsg;
	double thetamax = asin((mTauSq-mom_pipipi.mass2())/(2*mTau*mom_pipipi.P()));
	
	//choosing tau starting values for 2nd fit - equation 3.4 from Annes thesis
	
	double p1 = (((mTauSq+mom_pipipi.mass2())*mom_pipipi.P()*cos(theta)+
		      mom_pipipi.e()*sqrt(pow(mTauSq-mom_pipipi.mass2(),2)-4*mTauSq*pow(mom_pipipi.P(),2)*pow(sin(theta),2)))
		     /(2*(pow(mom_pipipi.e(),2)-pow(mom_pipipi.P(),2)*pow(cos(theta),2))));
	double p2 = (((mTauSq+mom_pipipi.mass2())*mom_pipipi.P()*cos(theta)-
		      mom_pipipi.e()*sqrt(pow(mTauSq-mom_pipipi.mass2(),2)-4*mTauSq*pow(mom_pipipi.P(),2)*pow(sin(theta),2)))
		     /(2*(pow(mom_pipipi.e(),2)-pow(mom_pipipi.P(),2)*pow(cos(theta),2))));
	
	double    p1st = p1;
	double    p2nd = p1;
	if(p1<p2) p2nd = p2;
	else      p1st = p2;
	if ( msgLevel(MSG::DEBUG) ) debug() << "calculated: p1=" << p1 << " and p2=" << p2 << endmsg;
	
	// check which solution was the original!
	int original_solution = 0;
	if(fabs(p1st-mom_tau.P())<fabs(p2nd-mom_tau.P())){ original_solution=1; }else{ original_solution=3; }
	
	// check that the fit has found a physical solution
	double tauroot = (pow(mTauSq-mom_pipipi.mass2(),2)-4*mTauSq*pow(mom_pipipi.P(),2)*pow(sin(theta),2));
	if(tauroot<0) { w1=0; w2=0; w3=0; w4=0; }
	
	Gaudi::LorentzVector mom_tautmp(p1st/sqrt((ev-bv).mag2())*(ev.x()-bv.x()),
					p1st/sqrt((ev-bv).mag2())*(ev.y()-bv.y()),
					p1st/sqrt((ev-bv).mag2())*(ev.z()-bv.z()),
					sqrt(mTauSq+p1st*p1st));
	
	mom_tau = mom_tautmp; //HACK mom_tau stays mom_tau
	
	Gaudi::LorentzVector mom_nutmp(mom_tau.px()-mom_pipipi.px(),
				       mom_tau.py()-mom_pipipi.py(),
				       mom_tau.pz()-mom_pipipi.pz(),
				       mom_tau.e() -mom_pipipi.e());
	
	mom_nu = mom_nutmp; //HACK mom_nu stays mom_nu
	
	Gaudi::LorentzVector mom_tau2(p2nd/sqrt((ev-bv).mag2())*(ev.x()-bv.x()),
				      p2nd/sqrt((ev-bv).mag2())*(ev.y()-bv.y()),
				      p2nd/sqrt((ev-bv).mag2())*(ev.z()-bv.z()),
				      sqrt(mTauSq+p2nd*p2nd));
	
	Gaudi::LorentzVector mom_nu2(mom_tau2.px()-mom_pipipi.px(),
				     mom_tau2.py()-mom_pipipi.py(),
				     mom_tau2.pz()-mom_pipipi.pz(),
				     mom_tau2.e() -mom_pipipi.e());
	
	//LHCb::Particle new_tau1(LHCb::ParticleID(tau->particleID().pid()));
	//new_tau1.setMomentum(mom_tau);
	//double ctau;
	//StatusCode status = LoKi::Fitters::ctau0(BV,new_tau1,DV,ctau);
	
	//find the B solutions for the first tau solution
	Gaudi::LorentzVector mom_DstTau = mom_Dst+mom_tau;
	double Btheta = acos((bv-pv).Dot(mom_DstTau.Vect())/(sqrt((bv-pv).mag2())*mom_DstTau.P()));
	if ( msgLevel(MSG::DEBUG) ) debug() << "Btheta: " << Btheta << endmsg;
	double DstTau_e = mom_DstTau.e();
	double DstTau_p = mom_DstTau.P();
	double DstTau_m2 = mom_DstTau.mass2();
	
	//debug() << Btheta << " " << mB0Sq << " " << DstTau_e << " " << DstTau_p << " " << DstTau_m2 << endmsg;
	//debug() << sqrt(pow(mB0Sq-DstTau_m2,2)-4*mB0Sq*pow(DstTau_p,2)*pow(sin(Btheta),2)) << endmsg;
	
	double Bp1 = (((mB0Sq+DstTau_m2)*DstTau_p*cos(Btheta)+
		       DstTau_e*sqrt(pow(mB0Sq-DstTau_m2,2)-4*mB0Sq*pow(DstTau_p,2)*pow(sin(Btheta),2)))
		      /(2*(pow(DstTau_e,2)-pow(DstTau_p,2)*pow(cos(Btheta),2))));
	
	double Bp2 = (((mB0Sq+DstTau_m2)*DstTau_p*cos(Btheta)-
		       DstTau_e*sqrt(pow(mB0Sq-DstTau_m2,2)-4*mB0Sq*pow(DstTau_p,2)*pow(sin(Btheta),2)))
		      /(2*(pow(DstTau_e,2)-pow(DstTau_p,2)*pow(cos(Btheta),2))));
	
	double      Bp1st = Bp1;
	double      Bp2nd = Bp1;
	if(Bp1<Bp2) Bp2nd = Bp2;
	else        Bp1st = Bp2;
	
	double original_Bp = mom_B.P();
	if(original_solution==1&&fabs(Bp1st-original_Bp)>fabs(Bp2nd-original_Bp)) original_solution=2;
	
	double Broot = (pow(mB0Sq-DstTau_m2,2)-4*mB0Sq*pow(DstTau_p,2)*pow(sin(Btheta),2));
	if(Broot<0||mom_DstTau.mass()>mB0) {
	  w1=0;
	  w2=0;
	  if(w3!=0) w3+=0.25;
	  if(w4!=0) w4+=0.25;
	}
	
	Gaudi::LorentzVector mom_Btmp(Bp1st/sqrt((bv-pv).mag2())*(bv.x()-pv.x()),
				      Bp1st/sqrt((bv-pv).mag2())*(bv.y()-pv.y()),
				      Bp1st/sqrt((bv-pv).mag2())*(bv.z()-pv.z()),
				      sqrt(mB0Sq+Bp1st*Bp1st));
	mom_B = mom_Btmp;
	
	Gaudi::LorentzVector mom_Bnutmp(mom_B.px()-mom_DstTau.px(),
					mom_B.py()-mom_DstTau.py(),
					mom_B.pz()-mom_DstTau.pz(),
					mom_B.e() -mom_DstTau.e());
	mom_Bnu = mom_Bnutmp;
	
	Gaudi::LorentzVector mom_B2(Bp2nd/sqrt((bv-pv).mag2())*(bv.x()-pv.x()),
				    Bp2nd/sqrt((bv-pv).mag2())*(bv.y()-pv.y()),
				    Bp2nd/sqrt((bv-pv).mag2())*(bv.z()-pv.z()),
				    sqrt(mB0Sq+Bp2nd*Bp2nd));
	
	Gaudi::LorentzVector mom_Bnu2(mom_B2.px()-mom_DstTau.px(),
				      mom_B2.py()-mom_DstTau.py(),
				      mom_B2.pz()-mom_DstTau.pz(),
				      mom_B2.e() -mom_DstTau.e());
	
	debug() << "Bp1/Bp2: " << Bp1 << "/" << Bp2 <<"=" << Bp1/Bp2 << " mom_B2.P()" << mom_B2.P() << endmsg;
	
	//changing variable
	mom_DstTau = mom_Dst+mom_tau2;
	//Btheta remains unchanged...
	DstTau_e = mom_DstTau.e();
	DstTau_p = mom_DstTau.P();
	DstTau_m2 = mom_DstTau.mass2();
	
	Btheta = acos((bv-pv).Dot(mom_DstTau.Vect())/(sqrt((bv-pv).mag2())*mom_DstTau.P()));
	debug() << "Btheta: " << Btheta << endmsg;
	
	double Bp3 = (((mB0Sq+DstTau_m2)*DstTau_p*cos(Btheta)+
		       DstTau_e*sqrt(pow(mB0Sq-DstTau_m2,2)-4*mB0Sq*pow(DstTau_p,2)*pow(sin(Btheta),2)))
		      /(2*(pow(DstTau_e,2)-pow(DstTau_p,2)*pow(cos(Btheta),2))));
	
	double Bp4 = (((mB0Sq+DstTau_m2)*DstTau_p*cos(Btheta)-
		       DstTau_e*sqrt(pow(mB0Sq-DstTau_m2,2)-4*mB0Sq*pow(DstTau_p,2)*pow(sin(Btheta),2)))
		      /(2*(pow(DstTau_e,2)-pow(DstTau_p,2)*pow(cos(Btheta),2))));
	
	double Broot2 = (pow(mB0Sq-DstTau_m2,2)-4*mB0Sq*pow(DstTau_p,2)*pow(sin(Btheta),2));
	
	if((Broot2<0||mom_DstTau.mass()>mB0)&&(tauroot>0)) {
	  if(w1!=0) w1+=0.25;
	  if(w2!=0) w2+=0.25;
	  w3=0;
	  w4=0;
	}
	if ( msgLevel(MSG::DEBUG) ) debug() << "Bp1= " << Bp1 << " Bp2= " << Bp2 << " Bp3= " << Bp3 << " Bp4= " << Bp4 << " Broot= " << Broot << " Broot2= " << Broot2 << endmsg;
	
	if(original_solution==3&&fabs(Bp1st-original_Bp)>fabs(Bp2nd-original_Bp)) original_solution=4;
	
	Gaudi::LorentzVector mom_B3(Bp3/sqrt((bv-pv).mag2())*(bv.x()-pv.x()),
				    Bp3/sqrt((bv-pv).mag2())*(bv.y()-pv.y()),
				    Bp3/sqrt((bv-pv).mag2())*(bv.z()-pv.z()),
				    sqrt(mB0Sq+Bp3*Bp3));
	
	Gaudi::LorentzVector mom_Bnu3(mom_B3.px()-mom_DstTau.px(),
				      mom_B3.py()-mom_DstTau.py(),
				      mom_B3.pz()-mom_DstTau.pz(),
				      mom_B3.e()-mom_DstTau.e());
	
	Gaudi::LorentzVector mom_B4(Bp4/sqrt((bv-pv).mag2())*(bv.x()-pv.x()),
				    Bp4/sqrt((bv-pv).mag2())*(bv.y()-pv.y()),
				    Bp4/sqrt((bv-pv).mag2())*(bv.z()-pv.z()),
				    sqrt(mB0Sq+Bp4*Bp4));
	
	Gaudi::LorentzVector mom_Bnu4(mom_B4.px()-mom_DstTau.px(),
				      mom_B4.py()-mom_DstTau.py(),
				      mom_B4.pz()-mom_DstTau.pz(),
				      mom_B4.e()-mom_DstTau.e());
	
	double original_bVertex_x = BV->position().x();
	double original_bVertex_y = BV->position().y();
	double original_bVertex_z = BV->position().z();
	
	double original_eVertex_x = DV->position().x();
	double original_eVertex_y = DV->position().y();
	double original_eVertex_z = DV->position().z();
	
	double original_PVertex_x = PV->position().x();
	double original_PVertex_y = PV->position().y();
	double original_PVertex_z = PV->position().z();
	
	int best_solution(0); 
	int cat_tau(0);
	int cat_B(0);
	int ok1(0);
	int ok2(0);
	
	if(m_isMC && decay_found) {
	  if(fabs(MCmom.P()-mom_tau.P())<fabs(MCmom.P()-mom_tau2.P())) {
	    ok1 = 1;
	    if(fabs(MCBmom.P()-mom_B.P())<fabs(MCBmom.P()-mom_B2.P())) ok2 = 1;
	  } else {
	    if(fabs(MCBmom.P()-mom_B3.P())<fabs(MCBmom.P()-mom_B4.P())) ok2 = 1;
	  }
	  double check1 = (fabs(MCmom.e()-mom_tau.e())/(MCmom.e()+mom_tau.e())+
			   fabs(MCBnumom.e()-mom_Bnu.e())/(MCBnumom.e()+mom_Bnu.e())+
			   fabs(MCnumom.e()-mom_nu.e())/(MCnumom.e()+mom_nu.e())+
			   fabs(MCBmom.e()-mom_B.e())/(MCBmom.e()+mom_B.e()));
	  double check2 = (fabs(MCmom.e()-mom_tau.e())/(MCmom.e()+mom_tau.e())+
			   fabs(MCBnumom.e()-mom_Bnu2.e())/(MCBnumom.e()+mom_Bnu2.e())+
			   fabs(MCnumom.e()-mom_nu.e())/(MCnumom.e()+mom_nu.e())+
			   fabs(MCBmom.e()-mom_B2.e())/(MCBmom.e()+mom_B2.e()));
	  double check3 = (fabs(MCmom.e()-mom_tau2.e())/(MCmom.e()+mom_tau2.e())+
			   fabs(MCBnumom.e()-mom_Bnu3.e())/(MCBnumom.e()+mom_Bnu3.e())+
			   fabs(MCnumom.e()-mom_nu2.e())/(MCnumom.e()+mom_nu2.e())+
			   fabs(MCBmom.e()-mom_B3.e())/(MCBmom.e()+mom_B3.e()));
	  double check4 = (fabs(MCmom.e()-mom_tau2.e())/(MCmom.e()+mom_tau2.e())+
			   fabs(MCBnumom.e()-mom_Bnu4.e())/(MCBnumom.e()+mom_Bnu4.e())+
			   fabs(MCnumom.e()-mom_nu2.e())/(MCnumom.e()+mom_nu2.e())+
			   fabs(MCBmom.e()-mom_B4.e())/(MCBmom.e()+mom_B4.e()));
	  
	  if( check1<check2 && check1<check3 && check1<check4 ) best_solution=1;
	  else if( check2<check1 && check2<check3 && check2<check4 ) best_solution=2;
	  else if( check3<check1 && check3<check2 && check3<check4 ) best_solution=3;
	  else if( check4<check1 && check4<check2 && check4<check3 ) best_solution=4;
	  if ( msgLevel(MSG::DEBUG) )debug()<<"checks "<<check1<<" "<<check2<<" "<<check3<<" "<<check4<<" and best soln is #"<<best_solution<<endmsg;
	}
	cat_tau = 1-ok1;
	cat_B   = 1-ok2 + 1-ok1;
	
	if ( msgLevel(MSG::DEBUG) ) debug()<<" fitted B energies: "<<mom_B.E()<<" "<<mom_B2.E()<<" "<<mom_B3.E()<<" "<<mom_B4.E()<<endmsg;
	
	std::vector<double> weight;
	std::vector<double> originalSolution;
	
	std::vector<double> tau_px;
	std::vector<double> tau_py;
	std::vector<double> tau_pz;
	std::vector<double> tau_p;
	std::vector<double> tau_e;
	std::vector<double> tau_life;
	
	std::vector<double> nu_px;
	std::vector<double> nu_py;
	std::vector<double> nu_pz;
	std::vector<double> nu_p;
	std::vector<double> nu_e;

	std::vector<double> W_px;
	std::vector<double> W_py;
	std::vector<double> W_pz;
	std::vector<double> W_p;
	std::vector<double> W_e;
	std::vector<double> W_m;

	std::vector<double> Bnu_px;
	std::vector<double> Bnu_py;
	std::vector<double> Bnu_pz;
	std::vector<double> Bnu_p;
	std::vector<double> Bnu_e;

	std::vector<double> B0_px;
	std::vector<double> B0_py;
	std::vector<double> B0_pz;
	std::vector<double> B0_p;
	std::vector<double> B0_e;
	std::vector<double> B0_life;

	std::vector<double> angle_W_in_Bframe;
	std::vector<double> angle_tau_in_Wframe;
	std::vector<double> angle_nu_in_tauframe;
	std::vector<double> angle_Dst_D0_plane_with_W_tau_plane;
	std::vector<double> angle_W_tau_plane_with_tau_nu_plane;

	Gaudi::XYZVector perp_D0_Dst = mom_D0.Vect().Unit().Cross( mom_Dst.Vect().Unit() );

	for(int sol=1;sol<=4;sol++){
	  if(sol==original_solution){originalSolution.push_back(1);}else{originalSolution.push_back(0);}
	  
	  Gaudi::LorentzVector B0=mom_B;
	  Gaudi::LorentzVector nu=mom_nu;
	  Gaudi::LorentzVector tau=mom_tau;
	  Gaudi::LorentzVector Bnu=mom_Bnu;
	  if(sol==3 or sol==4){tau=mom_tau2;nu=mom_nu2;}
	  if(sol==1){                          weight.push_back(w1); }
	  if(sol==2){ B0=mom_B2; Bnu=mom_Bnu2; weight.push_back(w2); }
	  if(sol==3){ B0=mom_B3; Bnu=mom_Bnu3; weight.push_back(w3); }
	  if(sol==4){ B0=mom_B4; Bnu=mom_Bnu4; weight.push_back(w4); }
	  
	  Gaudi::LorentzVector W=mom_tau+mom_Bnu;
	  
	  Gaudi::XYZVector perp_W_tau = W.Vect().Unit().Cross( tau.Vect().Unit() );
	  Gaudi::XYZVector perp_nu_tau = tau.Vect().Unit().Cross( nu.Vect().Unit() );
	  
	  angle_W_in_Bframe.push_back(   cosTheta(B0, W));
	  angle_tau_in_Wframe.push_back( cosTheta(tau,W));
	  angle_nu_in_tauframe.push_back(cosTheta(nu,tau));
	  angle_Dst_D0_plane_with_W_tau_plane.push_back( perp_W_tau.Dot(perp_D0_Dst) );
	  angle_W_tau_plane_with_tau_nu_plane.push_back( perp_W_tau.Dot(perp_nu_tau) );
	  
	  tau_px.push_back(tau.Px());
	  tau_py.push_back(tau.Py());
	  tau_pz.push_back(tau.Pz());
	  tau_p.push_back(tau.P());
	  tau_e.push_back(tau.E());
	  tau_life.push_back(lifetime(bv,ev,tau.P(),mTau));
	  
	  nu_px.push_back(nu.Px());
	  nu_py.push_back(nu.Py());
	  nu_pz.push_back(nu.Pz());
	  nu_p.push_back(nu.P());
	  nu_e.push_back(nu.E());
	  
	  W_px.push_back(W.Px());
	  W_py.push_back(W.Py());
	  W_pz.push_back(W.Pz());
	  W_p.push_back(W.P());
	  W_e.push_back(W.E());
	  W_m.push_back(W.M());
	  
	  Bnu_px.push_back(Bnu.Px());
	  Bnu_py.push_back(Bnu.Py());
	  Bnu_pz.push_back(Bnu.Pz());
	  Bnu_p.push_back(Bnu.P());
	  Bnu_e.push_back(Bnu.E());
	  
	  B0_px.push_back(B0.Px());
	  B0_py.push_back(B0.Py());
	  B0_pz.push_back(B0.Pz());
	  B0_p.push_back(B0.P());
	  B0_e.push_back(B0.E());
	  B0_life.push_back(lifetime(pv,bv,B0.P(),mB0));
	}
	
	bool test = true;
	std::string body="_DstTauNu_";
	
	
	if ( msgLevel(MSG::DEBUG) ) debug()<<" filling tuple" << endmsg;
	
	test &= tuple->column(head+body+"tau_charge",tau_charge);
	test &= tuple->column(head+body+"Dst_charge",Dst_charge);
	test &= tuple->column(head+body+"RightSign",RightSign);
	test &= tuple->column(head+body+"Nsolutions",(w1==0.25?4:(w1==0.5||w3==0.5?2:0)));
	
	test &= tuple->column(head+body+"tauroot",tauroot);
	test &= tuple->column(head+body+"Broot",Broot);
	test &= tuple->column(head+body+"Broot2",Broot2);
	
	test &= tuple->column(head+body+"besti",besti);
	test &= tuple->column(head+body+"bestj",bestj);
	
	//	test &= tuple->column(head+body+"nthTau",nthTau); //CF: These do nothing
	//	test &= tuple->column(head+body+"nthDstar",nthDstar);
	//	test &= tuple->column(head+body+"nthPion",nthPion);
	
	test &= tuple->column(head+body+"tau_key",threePi->key());
	test &= tuple->column(head+body+"Dstar_key",Dstar->key());
	
	test &= tuple->column(head+body+"cat_tau",cat_tau);
	test &= tuple->column(head+body+"cat_B",cat_B);
	test &= tuple->column(head+body+"chi2",chi2);
	
	if(m_isMC) {  
	  if(decay_found){
	    
	    if ( msgLevel(MSG::DEBUG) ) debug()<<" filling mctuple" << endmsg;
	    //test &= tuple->column(head+body+ "TRUErealTau", realTau);  //CF: This is redundant due to use of bkgcat tool. 
	    //test &= tuple->column(head+body+ "TRUErealDstar", realDstar);
	    
	    test &= tuple->column(head+body+"TRUEtau_px",MCmom.px());
	    test &= tuple->column(head+body+"TRUEtau_py",MCmom.py());
	    test &= tuple->column(head+body+"TRUEtau_pz",MCmom.pz());
	    test &= tuple->column(head+body+"TRUEtau_p",MCmom.P());
	    test &= tuple->column(head+body+"TRUEtau_e",MCmom.E());
	    test &= tuple->column(head+body+"TRUEtau_m",MCmom.M());
	    
	    test &= tuple->column(head+body+"TRUEBnu_px",MCBnumom.px());
	    test &= tuple->column(head+body+"TRUEBnu_py",MCBnumom.py());
	    test &= tuple->column(head+body+"TRUEBnu_pz",MCBnumom.pz());
	    test &= tuple->column(head+body+"TRUEBnu_p",MCBnumom.P());
	    test &= tuple->column(head+body+"TRUEBnu_e",MCBnumom.E());
	    test &= tuple->column(head+body+"TRUEBnu_m",MCBnumom.M());
	    
	    test &= tuple->column(head+body+"TRUEW_px",(MCBnumom+MCmom).px());
	    test &= tuple->column(head+body+"TRUEW_py",(MCBnumom+MCmom).py());
	    test &= tuple->column(head+body+"TRUEW_pz",(MCBnumom+MCmom).pz());
	    test &= tuple->column(head+body+"TRUEW_p",(MCBnumom+MCmom).P());
	    test &= tuple->column(head+body+"TRUEW_e",(MCBnumom+MCmom).E());
	    test &= tuple->column(head+body+"TRUEW_m",(MCBnumom+MCmom).M());
	    
	    test &= tuple->column(head+body+"TRUEB_px",MCBmom.px());
	    test &= tuple->column(head+body+"TRUEB_py",MCBmom.py());
	    test &= tuple->column(head+body+"TRUEB_pz",MCBmom.pz());
	    test &= tuple->column(head+body+"TRUEB_p",MCBmom.P());
	    test &= tuple->column(head+body+"TRUEB_e",MCBmom.E());
	    test &= tuple->column(head+body+"TRUEB_m",MCBmom.M());
	    
	    test &= tuple->column(head+body+"TRUEnu_px",MCnumom.px());
	    test &= tuple->column(head+body+"TRUEnu_py",MCnumom.py());
	    test &= tuple->column(head+body+"TRUEnu_pz",MCnumom.pz());
	    test &= tuple->column(head+body+"TRUEnu_p",MCnumom.P());
	    test &= tuple->column(head+body+"TRUEnu_e",MCnumom.E());
	    test &= tuple->column(head+body+"TRUEnu_m",MCnumom.M());
	    
	    test &= tuple->column(head+body+"TRUED0_px",MCD0mom.px());
	    test &= tuple->column(head+body+"TRUED0_py",MCD0mom.py());
	    test &= tuple->column(head+body+"TRUED0_pz",MCD0mom.pz());
	    test &= tuple->column(head+body+"TRUED0_p",MCD0mom.P());
	    test &= tuple->column(head+body+"TRUED0_e",MCD0mom.E());
	    test &= tuple->column(head+body+"TRUED0_m",MCD0mom.M());
	    
	    test &= tuple->column(head+body+"TRUEDst_px",MCDstmom.px());
	    test &= tuple->column(head+body+"TRUEDst_py",MCDstmom.py());
	    test &= tuple->column(head+body+"TRUEDst_pz",MCDstmom.pz());
	    test &= tuple->column(head+body+"TRUEDst_p",MCDstmom.P());
	    test &= tuple->column(head+body+"TRUEDst_e",MCDstmom.E());
	    test &= tuple->column(head+body+"TRUEDst_m",MCDstmom.M());
	    
	    test &= tuple->column(head+body+"TRUE_Qsqrd",(MCmom+MCBnumom).M());
	    test &= tuple->column(head+body+"TRUElife",MClife);
	    test &= tuple->column(head+body+"TRUEBlife",MCBlife);
	    
	    test &= tuple->column(head+body+"TRUEBvx_x",MCDstarvx.x());
	    test &= tuple->column(head+body+"TRUEBvx_y",MCDstarvx.y());
	    test &= tuple->column(head+body+"TRUEBvx_z",MCDstarvx.z());
	    
	    test &= tuple->column(head+body+"TRUEPVvx_x",MCPV.x());
	    test &= tuple->column(head+body+"TRUEPVvx_y",MCPV.y());
	    test &= tuple->column(head+body+"TRUEPVvx_z",MCPV.z());
	    
	    test &= tuple->column(head+body+"TRUEtauvx_x",MCtauvx.x());
	    test &= tuple->column(head+body+"TRUEtauvx_y",MCtauvx.y());
	    test &= tuple->column(head+body+"TRUEtauvx_z",MCtauvx.z());
	    
	    test &= tuple->column(head+body+"TRUEbest_solution",best_solution);
	    
	    test &= tuple->column(head+body+"TRUEangle_D0_in_Dstframe" ,MCangle_D0_in_Dstframe);
	    test &= tuple->column(head+body+"TRUEangle_W_in_Bframe"  ,MCangle_W_in_Bframe);
	    test &= tuple->column(head+body+"TRUEangle_tau_in_Wframe"  ,MCangle_tau_in_Wframe);
	    test &= tuple->column(head+body+"TRUEangle_nu_in_tauframe"  ,MCangle_nu_in_tauframe);  
	    test &= tuple->column(head+body+"TRUEangle_Dst_D0_plane_with_W_tau_plane" , MCangle_Dst_D0_plane_with_W_tau_plane);
	    test &= tuple->column(head+body+"TRUEangle_W_tau_plane_with_tau_nu_plane" , MCangle_W_tau_plane_with_tau_nu_plane);
	  }else{
	    
	    //CF: Fill the MC part of the tuple with -9999 if we failed to find the true decay
	    //Without this Davinci segfaults and we miss a whole heap of events beyond the segfault. 
	    if ( msgLevel(MSG::DEBUG) ) debug()<<" filling mctuple with -9999" << endmsg;
	    //test &= tuple->column(head+body+ "TRUErealTau", -9999); //CF: This is redundant due to use of bkg tool.
	    //test &= tuple->column(head+body+ "TRUErealDstar", -9999);
	    
	    test &= tuple->column(head+body+"TRUEtau_px",-9999.);
	    test &= tuple->column(head+body+"TRUEtau_py",-9999.);
	    test &= tuple->column(head+body+"TRUEtau_pz",-9999.);
	    test &= tuple->column(head+body+"TRUEtau_p",-9999.);
	    test &= tuple->column(head+body+"TRUEtau_e",-9999.);
	    test &= tuple->column(head+body+"TRUEtau_m",-9999.);
	    
	    test &= tuple->column(head+body+"TRUEBnu_px",-9999.);
	    test &= tuple->column(head+body+"TRUEBnu_py",-9999.);
	    test &= tuple->column(head+body+"TRUEBnu_pz",-9999.);
	    test &= tuple->column(head+body+"TRUEBnu_p",-9999.);
	    test &= tuple->column(head+body+"TRUEBnu_e",-9999.);
	    test &= tuple->column(head+body+"TRUEBnu_m",-9999.);
	    
	    test &= tuple->column(head+body+"TRUEW_px",-9999.);
	    test &= tuple->column(head+body+"TRUEW_py",-9999.);
	    test &= tuple->column(head+body+"TRUEW_pz",-9999.);
	    test &= tuple->column(head+body+"TRUEW_p",-9999.);
	    test &= tuple->column(head+body+"TRUEW_e",-9999.);
	    test &= tuple->column(head+body+"TRUEW_m",-9999.);
	    
	    test &= tuple->column(head+body+"TRUEB_px",-9999.);
	    test &= tuple->column(head+body+"TRUEB_py",-9999.);
	    test &= tuple->column(head+body+"TRUEB_pz",-9999.);
	    test &= tuple->column(head+body+"TRUEB_p",-9999.);
	    test &= tuple->column(head+body+"TRUEB_e",-9999.);
	    test &= tuple->column(head+body+"TRUEB_m",-9999.);
	    
	    test &= tuple->column(head+body+"TRUEnu_px",-9999.);
	    test &= tuple->column(head+body+"TRUEnu_py",-9999.);
	    test &= tuple->column(head+body+"TRUEnu_pz",-9999.);
	    test &= tuple->column(head+body+"TRUEnu_p",-9999.);
	    test &= tuple->column(head+body+"TRUEnu_e",-9999.);
	    test &= tuple->column(head+body+"TRUEnu_m",-9999.);
	    
	    test &= tuple->column(head+body+"TRUED0_px",-9999.);
	    test &= tuple->column(head+body+"TRUED0_py",-9999.);
	    test &= tuple->column(head+body+"TRUED0_pz",-9999.);
	    test &= tuple->column(head+body+"TRUED0_p",-9999.);
	    test &= tuple->column(head+body+"TRUED0_e",-9999.);
	    test &= tuple->column(head+body+"TRUED0_m",-9999.);
	    
	    test &= tuple->column(head+body+"TRUEDst_px",-9999.);
	    test &= tuple->column(head+body+"TRUEDst_py",-9999.);
	    test &= tuple->column(head+body+"TRUEDst_pz",-9999.);
	    test &= tuple->column(head+body+"TRUEDst_p",-9999.);
	    test &= tuple->column(head+body+"TRUEDst_e",-9999.);
	    test &= tuple->column(head+body+"TRUEDst_m",-9999.);
	    
	    test &= tuple->column(head+body+"TRUE_Qsqrd",-9999.);
	    test &= tuple->column(head+body+"TRUElife",-9999.);
	    test &= tuple->column(head+body+"TRUEBlife",-9999.);
	    
	    test &= tuple->column(head+body+"TRUEBvx_x",-9999.);
	    test &= tuple->column(head+body+"TRUEBvx_y",-9999.);
	    test &= tuple->column(head+body+"TRUEBvx_z",-9999.);
	    
	    test &= tuple->column(head+body+"TRUEPVvx_x",-9999.);
	    test &= tuple->column(head+body+"TRUEPVvx_y",-9999.);
	    test &= tuple->column(head+body+"TRUEPVvx_z",-9999.);
	    
	    test &= tuple->column(head+body+"TRUEtauvx_x",-9999.);
	    test &= tuple->column(head+body+"TRUEtauvx_y",-9999.);
	    test &= tuple->column(head+body+"TRUEtauvx_z",-9999.);
	    
	    test &= tuple->column(head+body+"TRUEbest_solution",-9999);
	    
	    test &= tuple->column(head+body+"TRUEangle_D0_in_Dstframe" ,-9999.);
	    test &= tuple->column(head+body+"TRUEangle_W_in_Bframe"  ,-9999.);
	    test &= tuple->column(head+body+"TRUEangle_tau_in_Wframe"  ,-9999.);
	    test &= tuple->column(head+body+"TRUEangle_nu_in_tauframe"  ,-9999.);  
	    test &= tuple->column(head+body+"TRUEangle_Dst_D0_plane_with_W_tau_plane" , -9999.);
	    test &= tuple->column(head+body+"TRUEangle_W_tau_plane_with_tau_nu_plane" , -9999.);
	    
	  }
	}
	if ( msgLevel(MSG::DEBUG) ) debug()<<" filled mctuple" << endmsg;
	
	test &= tuple->column(head+body+"Bvx_x",bv.x());
	test &= tuple->column(head+body+"Bvx_y",bv.y());
	test &= tuple->column(head+body+"Bvx_z",bv.z());

	test &= tuple->column(head+body+"tauvx_x",ev.x());
	test &= tuple->column(head+body+"tauvx_y",ev.y());
	test &= tuple->column(head+body+"tauvx_z",ev.z());

	test &= tuple->column(head+body+"PVvx_x",pv.x());
	test &= tuple->column(head+body+"PVvx_y",pv.y());
	test &= tuple->column(head+body+"PVvx_z",pv.z());

	test &= tuple->column(head+body+"original_PVvx_x",original_PVertex_x);
	test &= tuple->column(head+body+"original_PVvx_y",original_PVertex_y);
	test &= tuple->column(head+body+"original_PVvx_z",original_PVertex_z);

	test &= tuple->column(head+body+"original_Bvx_x",original_bVertex_x);
	test &= tuple->column(head+body+"original_Bvx_y",original_bVertex_y);
	test &= tuple->column(head+body+"original_Bvx_z",original_bVertex_z);

	test &= tuple->column(head+body+"original_tauvx_x",original_eVertex_x);
	test &= tuple->column(head+body+"original_tauvx_y",original_eVertex_y);
	test &= tuple->column(head+body+"original_tauvx_z",original_eVertex_z);

	test &= tuple->column(head+body+"theta",theta);
	test &= tuple->column(head+body+"thetamax",thetamax);
	test &= tuple->column(head+body+"thetaold",thetaold);

	test &= tuple->column(head+body+"dist",(ev-bv).R());  
	test &= tuple->column(head+body+"angle_D0_in_Dstframe"  , cosTheta(mom_Dst,mom_D0));

	test &= tuple->column(head+body+"threePi_px",pipipi_mom4.px());
	test &= tuple->column(head+body+"threePi_py",pipipi_mom4.py());
	test &= tuple->column(head+body+"threePi_pz",pipipi_mom4.pz());
	test &= tuple->column(head+body+"threePi_p",pipipi_mom4.P());
	test &= tuple->column(head+body+"threePi_e",pipipi_mom4.E());
	test &= tuple->column(head+body+"threePi_m",pipipi_mom4.M());

	test &= tuple->farray(head+body+"weight", weight  ,"N",4);
	test &= tuple->farray(head+body+"originalSolution", originalSolution  ,"N",4);

	test &= tuple->farray(head+body+"tau_px"  ,tau_px  ,"N",4);
	test &= tuple->farray(head+body+"tau_py"  ,tau_py  ,"N",4);
	test &= tuple->farray(head+body+"tau_pz"  ,tau_pz  ,"N",4);
	test &= tuple->farray(head+body+"tau_p"   ,tau_p   ,"N",4);
	test &= tuple->farray(head+body+"tau_e"   ,tau_e   ,"N",4);
	test &= tuple->farray(head+body+"tau_life",tau_life,"N",4);

	test &= tuple->farray(head+body+"nu_px",nu_px,"N",4);
	test &= tuple->farray(head+body+"nu_py",nu_py,"N",4);
	test &= tuple->farray(head+body+"nu_pz",nu_pz,"N",4);
	test &= tuple->farray(head+body+"nu_p" ,nu_p ,"N",4);
	test &= tuple->farray(head+body+"nu_e" ,nu_e ,"N",4);

	test &= tuple->farray(head+body+"W_px",W_px,"N",4);
	test &= tuple->farray(head+body+"W_py",W_py,"N",4);
	test &= tuple->farray(head+body+"W_pz",W_pz,"N",4);
	test &= tuple->farray(head+body+"W_p" ,W_p ,"N",4);
	test &= tuple->farray(head+body+"W_e" ,W_e ,"N",4);
	test &= tuple->farray(head+body+"W_m" ,W_m ,"N",4);

	test &= tuple->farray(head+body+"Bnu_px",Bnu_px,"N",4);
	test &= tuple->farray(head+body+"Bnu_py",Bnu_py,"N",4);
	test &= tuple->farray(head+body+"Bnu_pz",Bnu_pz,"N",4);
	test &= tuple->farray(head+body+"Bnu_p" ,Bnu_p ,"N",4);
	test &= tuple->farray(head+body+"Bnu_e" ,Bnu_e ,"N",4);

	test &= tuple->farray(head+body+"B0_px"  ,B0_px  ,"N",4);
	test &= tuple->farray(head+body+"B0_py"  ,B0_py  ,"N",4);
	test &= tuple->farray(head+body+"B0_pz"  ,B0_pz  ,"N",4);
	test &= tuple->farray(head+body+"B0_p"   ,B0_p   ,"N",4);
	test &= tuple->farray(head+body+"B0_e"   ,B0_e   ,"N",4);
	test &= tuple->farray(head+body+"B0_life",B0_life,"N",4);

	test &= tuple->farray(head+body+"angle_W_in_Bframe"  ,angle_W_in_Bframe  ,"N",4);
	test &= tuple->farray(head+body+"angle_tau_in_Wframe"  ,angle_tau_in_Wframe  ,"N",4);
	test &= tuple->farray(head+body+"angle_nu_in_tauframe"  ,angle_nu_in_tauframe  ,"N",4);  
	test &= tuple->farray(head+body+"angle_Dst_D0_plane_with_W_tau_plane" , angle_Dst_D0_plane_with_W_tau_plane  ,"N",4);
	test &= tuple->farray(head+body+"angle_W_tau_plane_with_tau_nu_plane" , angle_W_tau_plane_with_tau_nu_plane  ,"N",4);


	//Added branches for the 17 fit parameter measured uncertainties
	test &= tuple->column(head+body+"PVvx_xErr"  ,pvErr_x);
	test &= tuple->column(head+body+"PVvx_yErr"  ,pvErr_y);
	test &= tuple->column(head+body+"PVvx_zErr"  ,pvErr_z);
	
	test &= tuple->column(head+body+"Bvx_xErr"  ,bvErr_x);
	test &= tuple->column(head+body+"Bvx_yErr"  ,bvErr_y);
	test &= tuple->column(head+body+"Bvx_zErr"  ,bvErr_z);

	test &= tuple->column(head+body+"threePivx_xErr"  ,evErr_x);
	test &= tuple->column(head+body+"threePivx_yErr"  ,evErr_y);
	test &= tuple->column(head+body+"threePivx_zErr"  ,evErr_z);

	test &= tuple->column(head+body+"threePi_pxErr"  ,pipipiErr_px);
	test &= tuple->column(head+body+"threePi_pyErr"  ,pipipiErr_py);
	test &= tuple->column(head+body+"threePi_pzErr"  ,pipipiErr_pz);
	test &= tuple->column(head+body+"threePi_m2Err"  ,pipipiErr_m2);
	test &= tuple->column(head+body+"Dstar_pxErr"  ,DstErr_px);
	test &= tuple->column(head+body+"Dstar_pyErr"  ,DstErr_py);
	test &= tuple->column(head+body+"Dstar_pzErr"  ,DstErr_pz);
	test &= tuple->column(head+body+"Dstar_m2Err"  ,DstErr_m2);

	
	if ( msgLevel(MSG::DEBUG) ){ 
	  
	  if(test){debug()<<" filled tuple, status is good" << endmsg;}else{ debug()<<" filled tuple, status is bad" << endmsg;}
	}
	
	
	
	return StatusCode(test);
}
