/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
#ifndef BTAUFITTER_H 
#define BTAUFITTER_H 1

// Include files
#include "GaudiAlg/GaudiTool.h"

#include "IBTauFitter.h"
#include "TObject.h"
#include "TMinuit.h"

/** @class BTauFitter BTauFitter.h
 *  
 *
 *  @author Anne Keune
 *  @date   2010-04-30
 */

class BTauFitter : public GaudiTool, virtual public IBTauFitter, public TObject {
public: 
  /// Standard constructor
  BTauFitter( const std::string& type, 
          const std::string& name,
          const IInterface* parent ); 
  
  virtual ~BTauFitter( ); ///< Destructor

  virtual StatusCode initialize() override;
  virtual StatusCode finalize() override;

  //protected:

  virtual StatusCode performFit() override;
  virtual void performMinuit() override;

  virtual Vector17 calculateModel(Vector17 x) override;
  virtual Matrix17x17 calculateDerivative(Vector17 x) override;

  virtual void setMeasuredData(const LHCb::VertexBase* pVertex, const LHCb::VertexBase* bVertex, 
                               const LHCb::Particle* mother, const LHCb::Particle* rest) override;
  virtual StatusCode setWeights(const LHCb::VertexBase* pVertex, const LHCb::VertexBase* bVertex , 
                                const LHCb::Particle* mother, const LHCb::Particle* rest) override;
  virtual void setInitialGuess(int i, int j) override;
  
  virtual inline void setMeasuredData(Vector17 m) override { m_meas = m; }
  virtual inline void setWeights(SymMatrix17x17 W) override { m_Weights = W; }
  virtual inline void setInitialGuess(Vector17 x0) override { m_x0 = x0; }

  virtual inline Gaudi::LorentzVector tauMomentum() override { 
    double E = sqrt(pow(tau_m,2)+pow(m_x(3),2)+pow(m_x(4),2)+pow(m_x(5),2));
    Gaudi::LorentzVector mom(m_x(3),m_x(4),m_x(5),E);
    return mom;}

  virtual inline Gaudi::LorentzVector initial_tauMomentum() override { 
    double E = sqrt(pow(tau_m,2)+pow(m_x0(3),2)+pow(m_x0(4),2)+pow(m_x0(5),2));
    Gaudi::LorentzVector mom(m_x0(3),m_x0(4),m_x0(5),E);
    return mom;}

  virtual inline Gaudi::LorentzVector nuMomentum() override { 
    double E = sqrt(pow(m_x(6),2)+pow(m_x(7),2)+pow(m_x(8),2));
    Gaudi::LorentzVector mom(m_x(6),m_x(7),m_x(8),E);
    return mom;}

  virtual inline Gaudi::LorentzVector BMomentum() override { 
    double E = sqrt(pow(B_m,2)+pow(m_x(10),2)+pow(m_x(11),2)+pow(m_x(12),2));
    Gaudi::LorentzVector mom(m_x(10),m_x(11),m_x(12),E);
    return mom;}

  virtual inline Gaudi::LorentzVector initial_BMomentum() override { 
    double E = sqrt(pow(B_m,2)+pow(m_x0(10),2)+pow(m_x0(11),2)+pow(m_x0(12),2));
    Gaudi::LorentzVector mom(m_x0(10),m_x0(11),m_x0(12),E);
    return mom;}
  
  virtual inline Gaudi::LorentzVector nu2Momentum() override { 
    double E = sqrt(pow(m_x(13),2)+pow(m_x(14),2)+pow(m_x(15),2));
    Gaudi::LorentzVector mom(m_x(13),m_x(14),m_x(15),E);
    return mom;}

  virtual inline Gaudi::LorentzVector pipipiMomentum() override { 
    double E = sqrt(m_hx(9)+pow(m_hx(6),2)+pow(m_hx(7),2)+pow(m_hx(8),2));
    Gaudi::LorentzVector mom(m_hx(6),m_hx(7),m_hx(8),E);
    return mom;}

  virtual inline Gaudi::LorentzVector DstMomentum() override { 
    double E = sqrt(m_hx(16)+pow(m_hx(13),2)+pow(m_hx(14),2)+pow(m_hx(15),2));
    Gaudi::LorentzVector mom(m_hx(13),m_hx(14),m_hx(15),E);
    return mom;}

  virtual inline Gaudi::XYZPoint bVertex() override {
    Gaudi::XYZPoint vertex(m_hx(10),m_hx(11),m_hx(12));
    return vertex;}

  virtual inline Gaudi::XYZPoint eVertex() override {
    Gaudi::XYZPoint vertex(m_hx(3),m_hx(4),m_hx(5));
    return vertex;}

  virtual inline Gaudi::XYZPoint PVertex() override {
    Gaudi::XYZPoint vertex(m_hx(0),m_hx(1),m_hx(2));
    return vertex;}

  //Added by Donal to try and extract the errors for each measured quantity

  //Primary vertex
  virtual inline Gaudi::XYZPoint PVertexErr() override {
    Gaudi::XYZPoint vertex(sqrt(Vdiag(0)),sqrt(Vdiag(1)),sqrt(Vdiag(2)));
    return vertex;}

  //End vertex of the 3pi
  virtual inline Gaudi::XYZPoint eVertexErr() override {
    Gaudi::XYZPoint vertex(sqrt(Vdiag(3)),sqrt(Vdiag(4)),sqrt(Vdiag(5)));
    return vertex;}

  //End vertex of the B
  virtual inline Gaudi::XYZPoint bVertexErr() override {
    Gaudi::XYZPoint vertex(sqrt(Vdiag(10)),sqrt(Vdiag(11)),sqrt(Vdiag(12)));
    return vertex;}

  //4-vector momentum of 3pi in terms of m2 - px , py, pz, m2
  virtual inline Gaudi::LorentzVector pipipiMomentumErr() override { 
    Gaudi::LorentzVector mom(sqrt(Vdiag(6)),sqrt(Vdiag(7)),sqrt(Vdiag(8)),sqrt(Vdiag(9)));
    return mom;}

  //4-vector momentum of 3pi in terms of m2 - px , py, pz, m2
  virtual inline Gaudi::LorentzVector DstMomentumErr() override { 
    Gaudi::LorentzVector mom(sqrt(Vdiag(13)),sqrt(Vdiag(14)),sqrt(Vdiag(15)),sqrt(Vdiag(16)));
    return mom;}

  virtual inline double chi2() override { return m_chi2; }
  virtual inline unsigned int iterations() override { return m_iterations; }

  virtual inline double chi2_at_10() override { return m_chi2_at_10; }
  virtual inline double chi2_at_50() override { return m_chi2_at_50; }
  virtual inline double chi2_at_100() override { return m_chi2_at_100; }
  virtual inline double chi2_at_500() override { return m_chi2_at_500; }
  virtual inline double chi2_at_1000() override { return m_chi2_at_1000; }

  virtual inline Vector17 getMeasuredData() override { return m_meas; }
  virtual inline SymMatrix17x17 getWeights() override { return m_Weights; }
  virtual inline Vector17 getResult() override { return m_x; }

  
private:

  Vector17 m_meas;
  Vector17 m_x0;
    
  Vector17 m_x;
  Vector17 m_hx;
  Vector17 Vdiag;

  SymMatrix17x17 m_Weights;

  unsigned int m_iterations;
  double m_chi2;

  unsigned int m_maxIterations;
  double tau_m;
  double B_m;

  double m_chi2_at_10;
  double m_chi2_at_50;
  double m_chi2_at_100;
  double m_chi2_at_500;
  double m_chi2_at_1000;
  

};
#endif // BTAUFITTER_H
