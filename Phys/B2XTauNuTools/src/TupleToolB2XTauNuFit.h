/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TUPLETOOLB2XTauNuFit_H
#define TUPLETOOLB2XTauNuFit_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/PhysicalConstants.h"
// Interface
#include "Kernel/IParticleTupleTool.h"
//#include "Kernel/Particle2MCLinker.h"
//#include "MCInterfaces/IMCDecayFinder.h"
#include  "Kernel/IParticle2MCAssociator.h"

#include "DecayTreeTupleBase/TupleToolBase.h"

#include "IBTauFitter.h"

#include "TMath.h"
#include "Math/VectorUtil.h"

class IDVAlgorithm;
class IParticle2MCAssociator;


typedef ROOT::Math::SMatrix<double, 10, 10,
                            ROOT::Math::MatRepSym<double,10> > SymMatrix10x10;
typedef ROOT::Math::SMatrix<double,10,10> Matrix10x10;
typedef ROOT::Math::SVector<double,10> Vector10;




class TupleToolB2XTauNuFit : public TupleToolBase, virtual public IParticleTupleTool {
public:
  /// Standard constructor
  TupleToolB2XTauNuFit( const std::string& type,
		    const std::string& name,
		    const IInterface* parent);

  virtual ~TupleToolB2XTauNuFit(){}; ///< Destructor
  virtual StatusCode initialize() override;
  virtual StatusCode fill( const LHCb::Particle*
			   , const LHCb::Particle*
			   , const std::string&
			   , Tuples::Tuple& ) override;
private:


  double mTau, mTauSq;
  double mB0, mB0Sq;
  bool m_isMC;
  bool m_useMinuit;
  
  IDVAlgorithm* m_dva;
  IBTauFitter* m_fit;  
  
  //Particle2MCLinker* m_linkChi2;  ///< Pointer to associator using Chi2
  //IMCDecayFinder* m_mcFinder;
  IParticle2MCAssociator* m_p2mcAssoc;

  
  double lifetime(const LHCb::VertexBase*, const LHCb::VertexBase*, double, double);
  double lifetime(Gaudi::XYZPoint, Gaudi::XYZPoint, Gaudi::XYZVector, double);
  double lifetime(Gaudi::XYZPoint, Gaudi::XYZPoint, double, double);
  double cosTheta(const Gaudi::LorentzVector&, const Gaudi::LorentzVector&);
};

inline double TupleToolB2XTauNuFit::lifetime(const LHCb::VertexBase* birth, const LHCb::VertexBase* death, double p, double m)
{
	return (death->position()-birth->position()).R() * m/(Gaudi::Units::picosecond*Gaudi::Units::c_light*p);
}
inline double TupleToolB2XTauNuFit::lifetime(Gaudi::XYZPoint birth, Gaudi::XYZPoint death, Gaudi::XYZVector pvec, double m)
{
  return (death-birth).Dot(pvec)/pvec.mag2() * m/(Gaudi::Units::picosecond*Gaudi::Units::c_light);
}
inline double TupleToolB2XTauNuFit::lifetime(Gaudi::XYZPoint birth, Gaudi::XYZPoint death, double p, double m)
{
	return (death-birth).R() * m/(Gaudi::Units::picosecond*Gaudi::Units::c_light*p);
}

inline double TupleToolB2XTauNuFit::cosTheta( const Gaudi::LorentzVector& mother, const Gaudi::LorentzVector& mcp)
{
  ROOT::Math::Boost boost( mother.BoostToCM() );
  const Gaudi::XYZVector boostedParticle = (boost( mcp )).Vect().unit();
  const Gaudi::XYZVector boostedMother = mother.Vect().unit();
  double cosT = boostedParticle.Dot(boostedMother) ;
  return cosT;
}


#endif
