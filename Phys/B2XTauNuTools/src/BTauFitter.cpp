/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
// Include files 

// local
#include "BTauFitter.h"
#include "TMinuit.h"

//-----------------------------------------------------------------------------
// Implementation file for class : BTauFitter
//
// 2010-04-30 : Anne Keune
//
// Ideas to implement:
// Use tau mass with error as variable
//
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( BTauFitter )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
BTauFitter::BTauFitter( const std::string& type,
                   const std::string& name,
                   const IInterface* parent )
  : GaudiTool ( type, name, parent )
{
  declareInterface<IBTauFitter>(this);
  declareProperty("MaxIterations", m_maxIterations=1000);
  declareProperty( "BMass", B_m=5279.0, "mass B" );
  declareProperty( "TauMass", tau_m=1777.05, "mass tau" );
}
//=============================================================================
// Destructor
//=============================================================================
BTauFitter::~BTauFitter() {} 
//=============================================================================
StatusCode BTauFitter::initialize() 
{
  StatusCode sc = GaudiTool::initialize();
  if (sc.isFailure()) return GaudiTool::Error("Failed to initialize", sc);
  return StatusCode::SUCCESS;  
}
//=============================================================================
StatusCode BTauFitter::finalize() 
{
  StatusCode sc = GaudiTool::finalize();
  if (sc.isFailure()) return GaudiTool::Error("Failed to finalize", sc);
  return StatusCode::SUCCESS;  
}
//=============================================================================
StatusCode BTauFitter::performFit() 
{
  verbose() << "===> performFit()" << endmsg;

  //check if measurements/weights/guess are set
  if(m_meas==0||m_Weights==0||m_x0==0) {
    warning() << "measurements/weights/guess not set" << endmsg;
    return StatusCode::FAILURE;
  }
  
  int stable(0);
  double lambda(0);
  double v(10);

  m_iterations = 0;
  m_chi2 = 0;

  m_x = m_x0;

  //verbose() << "initial guess: " << m_x.Print(std::cout) << endmsg;
  m_hx = calculateModel(m_x);
  //if(msgLevel(MSG::VERBOSE)) verbose() << "initial model: " << m_hx.Print(std::cout) << endmsg;
  //verbose() << "initial chi2: " << Similarity((m_meas-m_hx),m_Weights) << endmsg;

  m_chi2_at_10 = m_chi2_at_50 =  m_chi2_at_100 =  m_chi2_at_500 =  m_chi2_at_1000 = -1;
  
  for(unsigned int iteration(0); iteration<m_maxIterations; iteration++) {
    

    

    m_hx = calculateModel(m_x);

    //if(msgLevel(MSG::VERBOSE)) verbose() << "model: " << m_hx.Print(std::cout) << endmsg;

    

    //if(msgLevel(MSG::VERBOSE)) verbose() << "VARIABLES: " << m_x.Print(std::cout) << endmsg;    
    //if(msgLevel(MSG::VERBOSE)) verbose() << "MODEL: " << m_hx.Print(std::cout) << endmsg;

    //Determine the chi2.
    double chi2 = Similarity((m_meas-m_hx),m_Weights);
    //if(msgLevel(MSG::VERBOSE)) verbose() << iteration << ". chi2: " << chi2 << endmsg;

    if(iteration==10) m_chi2_at_10 = chi2;
    else if(iteration==50) m_chi2_at_50 = chi2;
    else if(iteration==100) m_chi2_at_100 = chi2;
    else if(iteration==500) m_chi2_at_500 = chi2;
    else if(iteration==1000) m_chi2_at_1000 = chi2;

    Matrix17x17 H = calculateDerivative(m_x);
    Vector17 dchi2dx = -2*Transpose(H)*m_Weights*(m_meas-m_hx); //first derivative of chi2 (vector)
    SymMatrix17x17 d2chi2dx2 = 2*SimilarityT(H,m_Weights);   //second derivative of chi2 (matrix)
      
    //TRY LEVENBURG-MARQUARDT ADDITION
    SymMatrix17x17 diagonal;
    diagonal.SetDiagonal(d2chi2dx2.Diagonal());

    int fail;
    SymMatrix17x17 A = d2chi2dx2+lambda*diagonal;
    SymMatrix17x17 B = A.Inverse(fail);
      
    Vector17 deltax = -1*B*dchi2dx;
      
    //evaluate chi2 with new parameters
    Vector17 x1 = m_x+deltax;
    Vector17 hx1 = calculateModel(x1);
      
    //Determine the chi2.
    double chi2_test = Similarity((m_meas-hx1),m_Weights);
   
    int maxN = 50;
    if(chi2_test<=chi2){
      //verbose() << "check hx1(9): " << hx1(9) << endmsg;
      //verbose() << "check hx1(16): " << hx1(16) << endmsg;
      verbose() << "chi2_test smaller than chi2" << endmsg;
      m_x=x1;
      m_hx = hx1;
      lambda = 0;
    }
    else{
      lambda = 0.000001;
      double it = 0;
      while((chi2_test>chi2) && it<maxN) {
        
        //verbose() << "in loop check hx1(9): " << hx1(9) << endmsg;
        //verbose() << "in loop check hx1(16): " << hx1(16) << endmsg;
        //verbose() << "in loop test chi2: " << chi2_test << endmsg;

        lambda *= v;
        
        //verbose() << "in loop" << endmsg;
        A = d2chi2dx2+lambda*diagonal;
        B = A.Inverse(fail);
        deltax = -1*B*dchi2dx; 
        x1 = m_x+deltax;
        hx1 = calculateModel(x1);
        chi2_test = Similarity((m_meas-hx1),m_Weights);
        it++;
      }
      m_x = x1;
      m_hx = hx1;
    }

    if(chi2_test>=chi2-0.001 && chi2_test<=chi2+0.001) 
      stable++;
    
    if(chi2< 0.001 || stable==5 || iteration==m_maxIterations-1) {
    //if(iteration==m_maxIterations-1) {
      verbose() << "in final state: chi2: " << chi2 << ", stable: " << stable << ", iteration: " << iteration << endmsg;
      //if(chi2<10) {
      //if(msgLevel(MSG::VERBOSE)) verbose() << "final param: " << m_x.Print(std::cout) << endmsg;
      //if(msgLevel(MSG::VERBOSE)) verbose() << "final model: " << m_hx.Print(std::cout) << endmsg;
      //}
      
      //double tau_e = sqrt(pow(tau_m,2)+pow(m_x(3),2)+pow(m_x(4),2)+pow(m_x(5),2));
      //double Dst_e = sqrt((m_hx(16))+pow(m_hx(13),2)+pow(m_hx(14),2)+pow(m_hx(15),2));


      //warning() << "m: " 
      //          << sqrt(pow(Dst_e+tau_e,2)-pow(m_x(3)+m_hx(13),2)-pow(m_x(4)+m_hx(14),2)-pow(m_x(5)+m_hx(15),2)) << endmsg;
      
      /*
      warning() << "tau_pe: " << tau_e << endmsg;
      warning() << "Dst_pe: " << Dst_e << endmsg;
      
      warning() << "tau_px+Dst_px: " << m_x(3)+m_hx(13) << endmsg;
      warning() << "tau_py+Dst_py: " << m_x(4)+m_hx(14) << endmsg;
      warning() << "tau_pz+Dst_pz: " << m_x(5)+m_hx(15) << endmsg;
      warning() << "tau_pe+Dst_pe: " << tau_e+Dst_e << endmsg;
      */
      

      //if(msgLevel(MSG::VERBOSE)) verbose() << "NEW OUTCOME: " << m_hx.Print(std::cout) << endmsg;
      //verbose() << iteration << ". FINAL CHI2: " << chi2 << endmsg;
      m_iterations = iteration;
      m_chi2 = chi2;
      return StatusCode::SUCCESS;
    }   
  }
  return StatusCode::SUCCESS;
}
//=============================================================================
Vector17 BTauFitter::calculateModel(Vector17 x) 
{

  double nu_p2= pow(x(6),2)+pow(x(7),2)+pow(x(8),2);
  double tau_p2 = pow(x(3),2)+pow(x(4),2)+pow(x(5),2);  
  double tau_m2 = pow(tau_m,2);
  double tau_dot_nu = x(3)*x(6)+x(4)*x(7)+x(5)*x(8);

  double tau_dot_nu2 = x(3)*x(13)+x(4)*x(14)+x(5)*x(15);
  double B_dot_nu2 = x(10)*x(13)+x(11)*x(14)+x(12)*x(15);
  double B_dot_tau = x(10)*x(3)+x(11)*x(4)+x(12)*x(5);
  
  double tau_e = sqrt(tau_m2+tau_p2);
  double nu2_p2 = pow(x(13),2)+pow(x(14),2)+pow(x(15),2);
  double nu2_e = sqrt(nu2_p2);
  double B_p2 = pow(x(10),2)+pow(x(11),2)+pow(x(12),2);
  double B_e = sqrt(pow(B_m,2)+B_p2);
  
  double array[] = {x(0)-x(10)*fabs(x(16)),x(1)-x(11)*fabs(x(16)),x(2)-x(12)*fabs(x(16)), //PV
                    x(0)+x(3)*fabs(x(9)),x(1)+x(4)*fabs(x(9)),x(2)+x(5)*fabs(x(9)), //endV
                    x(3)-x(6),x(4)-x(7),x(5)-x(8), //pipipi_p = tau_p-nu_p
                    tau_m2-2*tau_e*sqrt(nu_p2)+2*tau_dot_nu, //pipipi_m2
                    x(0),x(1),x(2), //bV
                    x(10)-x(13)-x(3),x(11)-x(14)-x(4),x(12)-fabs(x(15))-x(5), //Dst_p = B_p-Bnu_p-tau_p //HACK made fabs!!
                    pow(B_m,2)+pow(tau_m,2)-2*(B_e*tau_e+B_e*nu2_e-tau_e*nu2_e)+2*(B_dot_nu2+B_dot_tau-tau_dot_nu2) //Dst_m2
  };
  
  Vector17 hx(&*array,(unsigned int)17);
  
  return hx;
}
//=============================================================================
Matrix17x17 BTauFitter::calculateDerivative(Vector17 x) 
{
  double nu_p2= pow(x(6),2)+pow(x(7),2)+pow(x(8),2);
  double tau_p2 = pow(x(3),2)+pow(x(4),2)+pow(x(5),2);  
  double tau_m2 = pow(tau_m,2);      

  double nu2_e = sqrt(pow(x(13),2)+pow(x(14),2)+pow(x(15),2));
  double B_e = sqrt(pow(B_m,2)+pow(x(10),2)+pow(x(11),2)+pow(x(12),2));
  double tau_e = sqrt(tau_m2+tau_p2);
  
  Matrix17x17 H; //the derivative matrix 
  

  //PV = bV-pB*fabs(LB)
  H(0,0) = H(1,1) = H(2,2) = 1;
  H(0,16) = -m_x(10)*fabs(x(16))/x(16);
  H(1,16) = -m_x(11)*fabs(x(16))/x(16);
  H(2,16) = -m_x(12)*fabs(x(16))/x(16);
  H(0,10) = -fabs(x(16));
  H(1,11) = -fabs(x(16));
  H(2,12) = -fabs(x(16));
  
  //eV = bV+ptau*fabs(L)
  H(3,0) = H(4,1) = H(5,2) = 1;
  H(3,9) = m_x(3)*fabs(x(9))/x(9);
  H(4,9) = m_x(4)*fabs(x(9))/x(9);
  H(5,9) = m_x(5)*fabs(x(9))/x(9);
  H(3,3) = H(4,4) = H(5,5) = fabs(x(9));

  //p_3h = ptau-pnu
  H(6,6) = H(7,7) = H(8,8) = -1;  
  H(6,3) = H(7,4) = H(8,5) = 1;
  
  //m^2 = mtau^2-2Etau*enu+2ptau*pnu
  H(9,3) = -1/tau_e*sqrt(nu_p2)*2*x(3)+2*x(6);
  H(9,4) = -1/tau_e*sqrt(nu_p2)*2*x(4)+2*x(7);
  H(9,5) = -1/tau_e*sqrt(nu_p2)*2*x(5)+2*x(8);
  H(9,6) = -1/sqrt(nu_p2)*tau_e*2*x(6)+2*x(3);
  H(9,7) = -1/sqrt(nu_p2)*tau_e*2*x(7)+2*x(4);
  H(9,8) = -1/sqrt(nu_p2)*tau_e*2*x(8)+2*x(5);

  //bV = bV
  H(10,0) = H(11,1) = H(12,2) = 1;
  
  //pD = pB-ptau-pnu2
  H(13,3) = H(14,4) = H(15,5) = -1;
  H(13,10) = H(14,11) = H(15,12) = 1;
  H(13,13) = H(14,14) = -1;
  H(15,15) = -x(15)/fabs(x(15)); //hack to match calculateModel
  
  //mD^2 = mB^2+mtau^2-2*EB*Etau-2*EB*Enu2-2*Etau*Enu2+2*pB*ptau+2*pB*pnu2+2*ptau*pnu2
  H(16,3) = +1/tau_e*nu2_e*2*x(3)-1/tau_e*B_e*2*x(3)+2*x(10)-2*x(13);
  H(16,4) = +1/tau_e*nu2_e*2*x(4)-1/tau_e*B_e*2*x(4)+2*x(11)-2*x(14);
  H(16,5) = +1/tau_e*nu2_e*2*x(5)-1/tau_e*B_e*2*x(5)+2*x(12)-2*x(15);

  H(16,10) = -1/B_e*nu2_e*2*x(10)-1/B_e*tau_e*2*x(10)+2*x(3)+2*x(13);
  H(16,11) = -1/B_e*nu2_e*2*x(11)-1/B_e*tau_e*2*x(11)+2*x(4)+2*x(14);
  H(16,12) = -1/B_e*nu2_e*2*x(12)-1/B_e*tau_e*2*x(12)+2*x(5)+2*x(15);
  
  H(16,13) = -1/nu2_e*B_e*2*x(13)+1/nu2_e*tau_e*2*x(13)-2*x(3)+2*x(10);
  H(16,14) = -1/nu2_e*B_e*2*x(14)+1/nu2_e*tau_e*2*x(14)-2*x(4)+2*x(11);
  H(16,15) = -1/nu2_e*B_e*2*x(15)+1/nu2_e*tau_e*2*x(15)-2*x(5)+2*x(12);

  return H;
}
//=============================================================================
void BTauFitter::setMeasuredData(const LHCb::VertexBase* pVertex, const LHCb::VertexBase* bVertex, 
                                 const LHCb::Particle* mother, const LHCb::Particle* rest ) 
{
  
  const LHCb::Vertex* eVertex = (mother)->endVertex();
  
  //measured variables
  double array[] = {pVertex->position().x(), //PV_x
                    pVertex->position().y(), //PV_y
                    pVertex->position().z(), //PV_z

                    eVertex->position().x(), //eVertex_x
                    eVertex->position().y(), //eVertex_y
                    eVertex->position().z(), //eVertex_z
                    (mother)->momentum().px(), //pipipi_px
                    (mother)->momentum().py(), //pipipi_py
                    (mother)->momentum().pz(), //pipipi_pz
                    pow((mother)->measuredMass(),2), //pipipi_m2
                    
                    bVertex->position().x(), //bVertex_x -- also endVertex of Dstar -- checked
                    bVertex->position().y(), //bVertex_y
                    bVertex->position().z(), //bVertex_z
                    (rest)->momentum().px(), //Dstar_px
                    (rest)->momentum().py(), //Dstar_py
                    (rest)->momentum().pz(), //Dstar_pz
                    pow((rest)->measuredMass(),2) //Dstar m2
  };  

  Vector17 m(&*array,(unsigned int)17);
  
  m_meas = m;
  
  //if(msgLevel(MSG::VERBOSE)) verbose() << "measured values: " << m_meas.Print(std::cout) << endmsg;
}
//=============================================================================
StatusCode BTauFitter::setWeights(const LHCb::VertexBase* pVertex, const LHCb::VertexBase*, 
                                  const LHCb::Particle* mother, const LHCb::Particle* rest) 
{
  const Gaudi::SymMatrix3x3& pVertex_covm = pVertex->covMatrix();
  const Gaudi::SymMatrix7x7& Pcovm = (mother)->covMatrix();
  const Gaudi::SymMatrix7x7& Restcovm = (rest)->covMatrix();

  
  //We call the covariance matrix V
  SymMatrix17x17 V;
  V.Place_at(pVertex_covm,0,0);
  V.Place_at(Pcovm,3,3);
  V.Place_at(Restcovm,10,10);
  
  //Jacobian matrix to change pipipi_E to pipipi_m2
  Matrix17x17 J;
  J(0,0) = J(1,1) = J(2,2) = J(3,3) = J(4,4) = J(5,5) = J(6,6) = J(7,7) = J(8,8) = 1;
  J(10,10) = J(11,11) = J(12,12) = J(13,13) = J(14,14) = J(15,15) = 1;
  
  J(9,6) = -2*m_meas(6);
  J(9,7) = -2*m_meas(7);
  J(9,8) = -2*m_meas(8);
  J(9,9) = +2*(mother)->momentum().e();

  J(16,13) = -2*m_meas(13);
  J(16,14) = -2*m_meas(14);
  J(16,15) = -2*m_meas(15);
  J(16,16) = +2*(rest)->momentum().e();
  
  SymMatrix17x17 Vnew = Similarity(J,V);
  //if(msgLevel(MSG::VERBOSE)) verbose() << "COV: " << Vnew.Print(std::cout) << endmsg;

  //Vector17 Vdiag = Vnew.Diagonal();
  
  
  //The weight matrix W is the inverse of the covariance matrix: W = (V)^-1
  int fail;
  m_Weights = Vnew.InverseChol(fail);
  //m_Weights = Vnew.Inverse(fail);

  //if(msgLevel(MSG::VERBOSE)) verbose() << "W: " << m_Weights.Print(std::cout) << endmsg;
  if(fail) {
    verbose() << "Vnew could not be inversed" << endmsg;
    return StatusCode::FAILURE;
  }
  return StatusCode::SUCCESS;
}
//=============================================================================
void BTauFitter::setInitialGuess(int i = 0, int j = 0) 
{  
  //first guess for x 'x0', the parameters of the system => initial estimation
  double pipipi_p = sqrt(pow(m_meas(6),2)+pow(m_meas(7),2)+pow(m_meas(8),2));
  double pipipi_m2 = m_meas(9);
  double pipipi_e = sqrt(pipipi_m2+pow(pipipi_p,2));
  
  double costhetamax = cos(asin((pow(tau_m,2)-pipipi_m2)/(2*tau_m*pipipi_p)));
  double pipipi_px = m_meas(6);
  double pipipi_py = m_meas(7);
  double pipipi_pz = m_meas(8);
  

  //solution theta=0 +sqrt(D)
  double mother_p(0);
  if(i==0) mother_p = (((pow(tau_m,2)+pipipi_m2)*pipipi_p+pipipi_e*(pow(tau_m,2)-pipipi_m2))/(2*pipipi_m2));
  else if(i==1) mother_p = ((pow(tau_m,2)+pipipi_m2)*pipipi_p*costhetamax)/(2*(pow(pipipi_e,2)-pow(pipipi_p,2)*pow(costhetamax,2)));
  else mother_p = (((pow(tau_m,2)+pipipi_m2)*pipipi_p-pipipi_e*(pow(tau_m,2)-pipipi_m2))/(2*pipipi_m2));

  //verbose() << "for tau: 1/2*(p-E): " << 1/2.*(pipipi_p-pipipi_e) << endmsg;
  //verbose() << "(m_tau/m_hhh): " << tau_m/sqrt(pipipi_m2) << endmsg;
  //verbose() << "mother_p: " << mother_p << endmsg;
  //verbose() << "pipipi_p: " << pipipi_p << endmsg;
  
  
  double L = (sqrt(pow(m_meas(3)-m_meas(10),2)+pow(m_meas(4)-m_meas(11),2)+pow(m_meas(5)-m_meas(12),2))
              /mother_p); //scale factor
  
  double tau_px = (m_meas(3)-m_meas(10))/L;
  double tau_py = (m_meas(4)-m_meas(11))/L;
  double tau_pz = (m_meas(5)-m_meas(12))/L;
  double tau_e = sqrt(pow(tau_m,2)+pow(mother_p,2));

  
  //Do a check for bad measurements (inverted beginV and endVertex)
  bool inverted_x(false), inverted_y(false), inverted_z(false);
  if((m_meas(3)-m_meas(10))*tau_px<0) inverted_x = true;
  if((m_meas(4)-m_meas(11))*tau_py<0) inverted_y = true;
  if((m_meas(5)-m_meas(12))*tau_pz<0) inverted_z = true;    

  //verbose() << "taup pre inversion: " << tau_px << "/" << tau_py << "/" << tau_pz << endmsg;

  //if(inverted_x) tau_px *= -1;
  //if(inverted_y) tau_py *= -1;
  //if(inverted_z) tau_pz *= -1;

  verbose() << "tau inverted: " << inverted_x << " " << inverted_y << " "<< inverted_z << endmsg;
  
  double nu_p = sqrt(pow(tau_m,2)+pow(mother_p,2))-pipipi_e;
  double nu_px,nu_py,nu_pz;
  //try to make a good guess:
  if(i==0) { 
    nu_px = +nu_p*tau_px/mother_p;
    nu_py = +nu_p*tau_py/mother_p;
    nu_pz = +nu_p*tau_pz/mother_p;
  } else if(i==1) {
    double tauminpi_len = sqrt(pow(tau_px-pipipi_px,2)+pow(tau_py-pipipi_py,2)+pow(tau_pz-pipipi_pz,2));
    nu_px = +nu_p*(tau_px-pipipi_px)/tauminpi_len;
    nu_py = +nu_p*(tau_py-pipipi_py)/tauminpi_len;
    nu_pz = +nu_p*(tau_pz-pipipi_pz)/tauminpi_len;
  } else {
    nu_px = -nu_p*tau_px/mother_p;
    nu_py = -nu_p*tau_py/mother_p;
    nu_pz = -nu_p*tau_pz/mother_p;
  }
  
  
  
  //double nu_px = tau_px-m_meas(6);
  //double nu_py = tau_py-m_meas(7);
  //double nu_pz = tau_pz-m_meas(8);

  //verbose() << "tau_pz: " << tau_pz << ", nu_pz: " << nu_pz << ", pipipi_pz: " << m_meas(8) << endmsg;
 
  //if((m_meas(10)-m_meas(0))*(m_meas(13)+m_meas(6))<0) inverted_x = true;
  //if((m_meas(11)-m_meas(1))*(m_meas(14)+m_meas(7))<0) inverted_y = true;
  //if((m_meas(12)-m_meas(2))*(m_meas(15)+m_meas(8))<0) inverted_z = true;    
    
  //first guess for x 'x0', the parameters of the system => initial estimation
  double Rest_p = sqrt(pow(m_meas(13),2)+pow(m_meas(14),2)+pow(m_meas(15),2));
  double Rest_e = sqrt(m_meas(16)+pow(Rest_p,2));
  double RestTau_px = m_meas(13)+tau_px;
  double RestTau_py = m_meas(14)+tau_py;
  double RestTau_pz = m_meas(15)+tau_pz;

  double RestTau_p = sqrt(pow(RestTau_px,2)+pow(RestTau_py,2)+pow(RestTau_pz,2));
  double RestTau_e = tau_e+Rest_e;
  double RestTau_m2 = pow(RestTau_e,2)-pow(RestTau_p,2);

  if(sqrt(RestTau_m2)>B_m) verbose() << "Mass Dtau too large!!" << endmsg;

  //verbose() << "tau_e: " << tau_e << " and rest_e: " << Rest_e << endmsg;
  
  costhetamax = cos(asin((pow(B_m,2)-RestTau_m2)/(2*B_m*RestTau_p)));
  
  //solution theta=0 +sqrt(D)
  double gran_p(0);
  if(j==0) gran_p = (((pow(B_m,2)+RestTau_m2)*RestTau_p+RestTau_e*(pow(B_m,2)-RestTau_m2))/(2*RestTau_m2));
  else if(j==1) gran_p = (((pow(B_m,2)+RestTau_m2)*RestTau_p*costhetamax)
                          /(2*(pow(RestTau_e,2)-pow(RestTau_p,2)*pow(costhetamax,2))));
  else gran_p = (((pow(B_m,2)+RestTau_m2)*RestTau_p-RestTau_e*(pow(B_m,2)-RestTau_m2))/(2*RestTau_m2));
  //this one seems wrong!
  
  double LB = (sqrt(pow(m_meas(10)-m_meas(0),2)+pow(m_meas(11)-m_meas(1),2)+pow(m_meas(12)-m_meas(2),2))
               /gran_p); //scale factor
  
  double B_px = (m_meas(10)-m_meas(0))/LB;
  double B_py = (m_meas(11)-m_meas(1))/LB;
  double B_pz = (m_meas(12)-m_meas(2))/LB;
  double B_e = sqrt(pow(B_m,2)+pow(gran_p,2));

  inverted_x=false;
  inverted_y=false; 
  inverted_z=false;
  if((m_meas(10)-m_meas(0))*(B_px)<0) inverted_x = true;
  if((m_meas(11)-m_meas(1))*(B_py)<0) inverted_y = true;
  if((m_meas(12)-m_meas(2))*(B_pz)<0) inverted_z = true; 

  //if(inverted_x) B_px *= -1;
  //if(inverted_y) B_py *= -1;
  //if(inverted_z) B_pz *= -1;

  verbose() << "B inverted: " << inverted_x << " " << inverted_y << " "<< inverted_z << endmsg;


  //verbose() << "Bvx x " << (m_meas(10)-m_meas(0))/fabs(m_meas(10)-m_meas(0)) << endmsg;
  //verbose() << "Bvx y " << (m_meas(11)-m_meas(1))/fabs(m_meas(11)-m_meas(1)) << endmsg;
  //verbose() << "Bvx z " << (m_meas(12)-m_meas(2))/fabs(m_meas(12)-m_meas(2)) << endmsg;

  //verbose() << "B px " << B_px/fabs(B_px) << endmsg;
  //verbose() << "B py " << B_py/fabs(B_py) << endmsg;
  //verbose() << "B pz " << B_pz/fabs(B_pz) << endmsg;
  
  //try to make a good guess:
  
  double Bnu_p = B_e-RestTau_e;

  verbose() << "E_nu: " << Bnu_p << endmsg;
  //double Bnu_px = Bnu_p*B_px/gran_p;
  //double Bnu_py = Bnu_p*B_py/gran_p;
  //double Bnu_pz = Bnu_p*B_pz/gran_p;

  double Btau_p = sqrt(pow(B_px-tau_px,2)+pow(B_py-tau_py,2)+pow(B_pz-tau_pz,2));

  //try
  double Bnu_px,Bnu_py,Bnu_pz;
  if(j==0) {
    Bnu_px = Bnu_p/Btau_p*(B_px-tau_px);
    Bnu_py = Bnu_p/Btau_p*(B_py-tau_py);
    Bnu_pz = Bnu_p/Btau_p*(B_pz-tau_pz);
  } 
  else if(j==1){
    double BTauminRest_len = sqrt(pow(B_px-tau_px-m_meas(13),2)+pow(B_py-tau_py-m_meas(14),2)+pow(B_pz-tau_pz-m_meas(15),2));
    Bnu_px = Bnu_p/BTauminRest_len*(B_px-tau_px-m_meas(13));
    Bnu_py = Bnu_p/BTauminRest_len*(B_py-tau_py-m_meas(14));
    Bnu_pz = Bnu_p/BTauminRest_len*(B_pz-tau_pz-m_meas(15));
  }
  else{
    Bnu_px = -Bnu_p/Btau_p*(B_px-tau_px);
    Bnu_py = -Bnu_p/Btau_p*(B_py-tau_py);
    Bnu_pz = -Bnu_p/Btau_p*(B_pz-tau_pz);
  }
  
  

  verbose() << "E_nu: " << sqrt(Bnu_px*Bnu_px+Bnu_py*Bnu_py+Bnu_pz*Bnu_pz) << endmsg;
  

  //verbose() << "test: Bp vs granp: " << sqrt(B_px*B_px+B_py*B_py+B_pz*B_pz) << " vs. " << gran_p << endmsg;
  

  //gives negative mass
  //double Bnu_px = B_px-tau_px-m_meas(13);
  //double Bnu_py = B_py-tau_py-m_meas(14);
  //double Bnu_pz = B_pz-tau_pz-m_meas(15);

  //double Dst_e = sqrt(m_meas(16)+pow(m_meas(15),2)+pow(m_meas(14),2)+pow(m_meas(13),2));

  //verbose() << "mass test: " << (pow(B_e-tau_e-Bnu_p,2)-pow(B_px-tau_px-Bnu_px,2)- 
  //                            pow(B_py-tau_py-Bnu_py,2)- pow(B_pz-tau_pz-Bnu_pz,2)) << endmsg;

  //verbose() << "mass test2: " << pow(Dst_e,2)-pow(B_pz-tau_pz-Bnu_pz,2)-pow(B_py-tau_py-Bnu_py,2)-pow(B_px-tau_px-Bnu_px,2) 
  //       << endmsg;
  

 
  

  //The 'x0' variables we put into the array 'x0'
  double array[] = {m_meas(10),m_meas(11),m_meas(12),
                    tau_px,tau_py,tau_pz,
                    nu_px,nu_py,nu_pz,
                    L,B_px,B_py,B_pz,Bnu_px,Bnu_py,Bnu_pz,LB //HACK
  };

  Vector17 x0(&*array,(unsigned int)17);
  
  m_x0 = x0;


}

//=============================================================================
//=============================================================================
//=============================================================================
//=============================================================================
//TMINUIT
//=============================================================================
TMinuit *minuit0 = new TMinuit(17); //maximum 17 params
void myFCN0(int &, double *gin, double &f, double *par, int flag) 
{

  BTauFitter *object = (BTauFitter*)minuit0->GetObjectFit();
  
  if(flag==2) {

    Vector17 x(par,17);
    Vector17 hx = object->calculateModel(x);
    
    Vector17 meas = object->getMeasuredData();
    SymMatrix17x17 W = object->getWeights();
    
    Matrix17x17 H = object->calculateDerivative(x);  

    Vector17 dchi2dx = -2*Transpose(H)*W*(meas-hx); 
    for (int i = 0; i < 17; i++) {
      gin[i] = dchi2dx[i];
    }
  } else {    
    
    Vector17 x(par,17);
    Vector17 hx = object->calculateModel(x);
    
    Vector17 meas = object->getMeasuredData();
    SymMatrix17x17 W = object->getWeights();
    
    f = Similarity((meas-hx),W);
    
  }
  
}
//=============================================================================
void BTauFitter::performMinuit() 
{
  minuit0->SetFCN(myFCN0);
 
  Vector17 hx = calculateModel(m_x0);
  verbose() << "Initial chi2: " << Similarity((m_meas-hx),m_Weights) << endmsg;

  //steps+lower limits+upper limits for these variables:
  //m_meas(10),m_meas(11),m_meas(12),tau_px,tau_py,tau_pz,nu_px,nu_py,nu_pz,L,B_px,B_py,B_pz,Bnu_px,Bnu_py,Bnu_pz,LB


  

  double initErr[] = {0.0001,0.0001,0.0001,
                      1.,1.,1.,
                      1.,1.,1.,
                      0.000001,
                      1.,1.,1.,
                      1.,1.,1.,
                      0.000001};
  
  double lowerLimit[] = {-200.,-200.,-200., 
                         -300000.,-300000.,-300000.,
                         -300000.,-300000.,-300000.,
                         0.0000001,//10^-7 //check if it doesn't want to go under this
                         -300000.,-300000.,-300000.,
                         -300000.,-300000.,-300000.,
                         0.0000001};//10^-7 //check if it doesn't want to go under this
  
  double upperLimit[] = {200.,200.,200.,
                         300000.,300000.,300000.,
                         300000.,300000.,300000.,
                         0.001,//10^-3
                         300000.,300000.,300000.,
                         300000.,300000.,300000.,
                         0.001};//10^-3
  
  
  minuit0->DefineParameter(0, "PVx",   m_x0[0], initErr[0], lowerLimit[0], upperLimit[0]);
  minuit0->DefineParameter(1, "PVy",   m_x0[1], initErr[1], lowerLimit[1], upperLimit[1]);
  minuit0->DefineParameter(2, "PVz",   m_x0[2], initErr[2], lowerLimit[2], upperLimit[2]);
  minuit0->DefineParameter(3, "tau_px",m_x0[3], initErr[3], lowerLimit[3], upperLimit[3]);
  minuit0->DefineParameter(4, "tau_py",m_x0[4], initErr[4], lowerLimit[4], upperLimit[4]);
  minuit0->DefineParameter(5, "tau_pz",m_x0[5], initErr[5], lowerLimit[5], upperLimit[5]);
  minuit0->DefineParameter(6, "nu_px", m_x0[6], initErr[6], lowerLimit[6], upperLimit[6]);
  minuit0->DefineParameter(7, "nu_py", m_x0[7], initErr[7], lowerLimit[7], upperLimit[7]);
  minuit0->DefineParameter(8, "nu_pz", m_x0[8], initErr[8], lowerLimit[8], upperLimit[8]);
  minuit0->DefineParameter(9, "L",     m_x0[9], initErr[9], lowerLimit[9], upperLimit[9]);
  minuit0->DefineParameter(10,"B_px",  m_x0[10],initErr[10],lowerLimit[10],upperLimit[10]);
  minuit0->DefineParameter(11,"B_py",  m_x0[11],initErr[11],lowerLimit[11],upperLimit[11]);
  minuit0->DefineParameter(12,"B_pz",  m_x0[12],initErr[12],lowerLimit[12],upperLimit[12]);
  minuit0->DefineParameter(13,"nu2_px",m_x0[13],initErr[13],lowerLimit[13],upperLimit[13]);
  minuit0->DefineParameter(14,"nu2_py",m_x0[14],initErr[14],lowerLimit[14],upperLimit[14]);
  minuit0->DefineParameter(15,"nu2_pz",m_x0[15],initErr[15],lowerLimit[15],upperLimit[15]);
  minuit0->DefineParameter(16,"LB",    m_x0[16],initErr[16],lowerLimit[16],upperLimit[16]);

  minuit0->SetObjectFit(this); //Must be TObject
  
  
  minuit0->SetErrorDef(1);
  //minuit0->SetMaxIterations(500);
  minuit0->SetPrintLevel(-1);

  minuit0->Migrad();

  //get results
  double result[17];
  double resultError[17];
  
  minuit0->GetParameter(0, result[0], resultError[0]);
  minuit0->GetParameter(1, result[1], resultError[1]);
  minuit0->GetParameter(2, result[2], resultError[2]);
  minuit0->GetParameter(3, result[3], resultError[3]);
  minuit0->GetParameter(4, result[4], resultError[4]);
  minuit0->GetParameter(5, result[5], resultError[5]);
  minuit0->GetParameter(6, result[6], resultError[6]);
  minuit0->GetParameter(7, result[7], resultError[7]);
  minuit0->GetParameter(8, result[8], resultError[8]);
  minuit0->GetParameter(9, result[9], resultError[9]);
  minuit0->GetParameter(10,result[10],resultError[10]);
  minuit0->GetParameter(11,result[11],resultError[11]);
  minuit0->GetParameter(12,result[12],resultError[12]);
  minuit0->GetParameter(13,result[13],resultError[13]);
  minuit0->GetParameter(14,result[14],resultError[14]);
  minuit0->GetParameter(15,result[15],resultError[15]);
  minuit0->GetParameter(16,result[16],resultError[16]);
 
  Vector17 x(result,17);
  m_x = x;
  m_hx = calculateModel(m_x);
  m_iterations = minuit0->GetMaxIterations(); //FOR NOW
  m_chi2 = Similarity((m_meas-m_hx),m_Weights);
  
}
//=============================================================================
