/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TUPLETOOLPIDCALIB_H
#define TUPLETOOLPIDCALIB_H 1

/// include files
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Kernel/IParticleTupleTool.h"            // Interface
#include "Kernel/IParticleTupleTool.h"
#include "RecInterfaces/IChargedProtoANNPIDTool.h"

/// forward declarations
class IParticleTransporter;
class IDistanceCalculator;
class IDVAlgorithm;
class ITriggerTisTos;
class IBremAdder;
class IRelatedPVFinder;
class IChargedProtoANNPIDTool;
namespace LHCb
{
  class Particle;
  class VertexBase;
}

/** @class EvtTupleToolPIDCalib EvtTupleToolPIDCalib.h CalibDataSel/EvtTupleToolPIDCalib.h
 *
 * \brief Adds a limited number of kinematic and PID variables
 *  used by PIDCalib for DecayTreeTuple, in addition to some variables
 *  specific to PIDCalib.

 * Basic tuple columns:
 * - Magnitude of the momentum 3-vector (p);
 * - Transverse momentum (pt);
 * - Pseudorapidity (eta);
 * - Azimuthal component of momentum (phi);
 * - Reconstructed invariant-mass (m, for composite particles only);

 * If FillPID is true (default), the HEP ID is filled. For non-composite,
 * charged particles, the following are also filled:
 * - Delta-log likehood (DLL) of the kaon, pion, proton, electron and muon
hypotheses with respect to the pion hypothesis;
 * - Neutral network (ProbNN) probalities of the kaon, pion, proton,
electron, muon and ghost hypotheses;
 * - Whether the particle has RICH, muon or CALO ID information
 *   (hasRich, hasMuon and hasCalo respectively)
 * - Whether the particle is within the acceptance of the muon stations
 *   (inAccMuon);
 * - Whether the particle passes the 'isMuon' or 'isMuonLoose' criteria;
 * - The number tracks with shared hits in the muon stations (NShared).
 *
 * If both FillPID and FillRichRadInfo are true, then the following columns
 * are filled (if applicable):
 * - Whether the particle is above the Cherenkov threshold for kaons,
 * pions, protons, electrons, or muons (RICHThreshold);
 * - Whether the Aerogel, RICH1 gas or RICH2 gas were used to ID the particle
 *  ({RICH1Gas,RICH2Gas,Aerogel}Used).
 *
 * If FillMuonTagAndProbe is true, the muons from J/psi -> mu+ mu- are
 * flagged based on whether they pass the tag and/or the probe criteria.
 * The following columns are filled for the muons in this case:
 * - Whether the muon passes the probe criteria (Probe);
 * - Whether the muon passes the tag criteria (Tag).
 * If Verbose is true, the following columns are also filled:
 * - The minimum IP / IP chi^2 of the muon with respect to the PV that
 * corresponds to the smallest IP / IP chi^2 (MIP_PV / MIPCHI2_PV);
 * - The track chi^2 per degrees of freedom (TRCHI2DOF).
 * The following properties can be used to customise the probe cuts:
 * - ProbeMinP - the minimum momentum of the probe muon.
 * - ProbeMinPt - the minimum pt of the probe muon.
 * - ProbeMinIPChi2PV - the "MIPCHI2_PV" of the probe muon.
 * - ProbeMaxTrChi2DOF - the maximum track chi^2/d.o.f. of the probe muon.
 * Similarly, the following properties are used to set the cuts applied
 * to the tagging muon:
 * - TagMinP - the minimum momentum of the tagging muon.
 * - TagMinPt - the minimum pt of the tagging muon.
 * - TagMinIPChi2PV - the "MIPCHI2_PV" of the tagging muon.
 * In addition to these cuts, the tagging muon is required to pass the "isMuon"
 * selection.
 *
 * If FillMuonTisTagged/FillMuonTosTagged is true, then the particle
 * is flagged based on whether it passes the muon "TisTag"/"TosTag" trigger
 * criteria (the former is referred to as "MuonUnBiased" in the PIDCalib
 * packages)
 * The following column is filled in these cases (for charged, non-composite
 * particles):
 * - Whether the particle passed the TIS/TOS trigger requirements (Tis /
 * TosTagged).
 * If Verbose is true, then the following columns are also filled:
 * - Whether the particle passed the TIS/TOS L0 trigger requirements (L0Tis /
 * L0TosTagged);
 * - Whether the particle passed the TIS/TOS HLT1 trigger requirements (
 * Hlt1Tis / Hlt1TosTagged);
 * - Whether the particle passed the TIS/TOS HLT2 trigger requirements (
 * Hlt2Tis / Hlt2TosTagged).
 * The trigger requirements are configured by the following properties:
 * - MuonTis/TosL0Criterion - the regular expression indicating the L0 TIS/TOS
 * triggers to consider.
 * - MuonTis/TosHlt1Criterion - the regular expression indicating the HLT1
 * TIS/TOS triggers to consider.
 * - MuonTis/TosHlt2Criterion - the regular expression indicating the HLT2
 * TIS/TOS triggers to consider.
 */
class TupleToolPIDCalib : public TupleToolBase,
                          virtual public IParticleTupleTool
{
public:
  /** Indicates whether to evaluate the TIS or TOS trigger decisions.
   * Used by the getTisTosDecision and fillMuonTisTosTagged methods.
   */
  enum TisTosType {
    UnknownTisTosType,
    TIS,
    TOS
    };

  /** Indicates whether to evaluate the L0, HLT1 or HLT2 trigger decisons.
   * Used by the getTisTosDecision method.
   */
  enum TriggerType {
    UnknownTriggerType,
    L0,
    Hlt1,
    Hlt2
    };

  /** Indicates which particle to evaluate the L0, HLT1 and HLT2 trigger
   * decisons for. Used by the getTisTosDecision method.
   */
  enum ParticleType {
    UnknownParticleType,
    Electron = 11,
    Muon = 13
    };

  /** Indicates in which region of the ECAL the electron was found
   */
  enum EcalRegion {
    UnknownRegion,
    BeamPipe,
    Inner,
    Middle,
    Outer
    };

  /// Standard constructor
  TupleToolPIDCalib( const std::string& type, const std::string& name,
                     const IInterface* parent);
  virtual ~TupleToolPIDCalib();
  StatusCode fill( const LHCb::Particle* /*mother*/,
                   const LHCb::Particle* P,
                   const std::string& head,
                   Tuples::Tuple& tuple) override;
  StatusCode initialize() override;

private:
  // methods

  /// Find region of the ecal
  EcalRegion EcalRegionFinder(double x, double y, double z);

  /// Fill track kinematics
  StatusCode fillKinematics( const LHCb::Particle* P,
                             const std::string& head,
                             Tuples::Tuple& tuple);
  /// Fill PID information
  StatusCode fillPID( const LHCb::Particle* P,
                      const std::string& head,
                      Tuples::Tuple& tuple);

  /// Fill muon tag-and-probe flags
  StatusCode fillMuonTagProbe( const LHCb::Particle* P,
                               const std::string& head,
                               Tuples::Tuple& tuple);

  /// Fill muon tag-and-probe flags
  StatusCode fillElectronTagProbe( const LHCb::Particle* P,
                                   const std::string& head,
                                   Tuples::Tuple& tuple );

  /// Determine the minimum IP and IP chi^2
  StatusCode getMinIP( const LHCb::Particle* P,
                       float& ip, float& chi2);

  /// Determine the IP and IP chi^2 or related PV
  StatusCode getIP( const LHCb::Particle* P,
                       float& ip, float& chi2);

  /// Determine the track chi^2/d.o.f.
  StatusCode getTrChi2DOF( const LHCb::Particle* P,
                           float& chi2DOF);

  /// Get the TIS or TOS trigger decision(s) for the particle
  StatusCode getTisTosDecision( const LHCb::Particle* P,
                                const TupleToolPIDCalib::TisTosType& tistosType,
                                const TupleToolPIDCalib::TriggerType& triggerType,
                                bool& dec,
                                const TupleToolPIDCalib::ParticleType& particle );

  /// Fill the TisTag or TosTag flags
  StatusCode fillTisTosTagged( const LHCb::Particle* P,
                                   const std::string& head,
                                   Tuples::Tuple& tuple,
                                   const TupleToolPIDCalib::TisTosType& tistosType,
                                   const TupleToolPIDCalib::ParticleType& particleType);

  /// Fill the muon TisTag flag (called by fillMuonTisTosTagged)
  StatusCode fillMuonTisTagged( const LHCb::Particle* P,
                                const std::string& head,
                                Tuples::Tuple& tuple);

  /// Fill the muon TosTag flag (called by fillMuonTisTosTagged)
  StatusCode fillMuonTosTagged( const LHCb::Particle* P,
                                const std::string& head,
                                Tuples::Tuple& tuple);

  /// Fill the muon TisTag flag (called by fillMuonTisTosTagged)
  StatusCode fillElectronTisTagged( const LHCb::Particle* P,
                                    const std::string& head,
                                    Tuples::Tuple& tuple);

  /// Fill the muon TosTag flag (called by fillMuonTisTosTagged)
  StatusCode fillElectronTosTagged( const LHCb::Particle* P,
                                    const std::string& head,
                                    Tuples::Tuple& tuple);

  /// Fill the electron bremsstrahlung info (called by fillBremInfo)
  StatusCode fillBremInfo( const LHCb::Particle* P,
                           const std::string& head,
                           Tuples::Tuple& tuple);

  // data members

  /// fill PID information
  bool m_fillPID;
  /// fill RICH radiator information
  bool m_fillRichRadInfo;

  /// the particle transporter, used to retrieve the particle momentum
  IParticleTransporter* m_transporter;
  /// name of the particle transporter
  std::string m_transporterName;

  /// the DV algorithm, used the retrieve the IP of the particle
  IDVAlgorithm* m_dva;
  const IDistanceCalculator* m_dist;

  /// fill muon tag-and-probe flags
  bool m_fillMuonTagProbe;
  /// fill electron tag-and-probe flags
  bool m_fillElectronTagProbe;

  /// tag minimum momentum
  float m_tagMinMom; // MeV/c
  /// probe minimum momentum
  float m_probMinMom; // MeV/c
  /// tag minimum pt
  float m_tagMinPt; // MeV/c
  /// probe minimum pt
  float m_probMinPt; // MeV/c
  /// tag minimum IP chi^2
  float m_tagMinIPchi2PV;
  /// probe minimum IP chi^2
  float m_probMinIPchi2PV;
  /// probe maximum track chi^2/d.o.f.
  float m_probMaxTrChi2DOF; // MeV/c

  /// tag electron P cut
  float m_electronTagP;
  /// probe electron Pt cut
  float m_electronProbeP;
  /// tag electron Pt cut
  float m_electronTagPt;
  /// probe electron Pt cut
  float m_electronProbePt;
  /// tag electron IP Chi2 cut wrt associated PV
  float m_electronTagIpChi2;
  /// probe electron IP Chi2 cut wrt associated PV
  float m_electronProbeIpChi2;
  /// tag electron DLL(e-pi) cut
  float m_electronTagDLLe;

  /// the L0 trigger TIS/TOS tool
  ITriggerTisTos* m_l0TisTosTool;
  /// the HLT trigger TIS/TOS tool
  ITriggerTisTos* m_hltTisTosTool;
  /// name of the L0 trigger TIS/TOS tool
  std::string m_l0TisTosToolName;
  /// name of the HLT trigger TIS/TOS tool
  std::string m_hltTisTosToolName;

  /// TisTag and TosTag muon L0 trigger criteria
  std::pair<std::string, std::string> m_tistosTagL0Crit; // TIS, TOS
  /// TisTag and TosTag muon HLT1 trigger criteria
  std::pair<std::string, std::string> m_tistosTagHlt1Crit; // TIS, TOS
  /// TisTag and TosTag muon HLT2 trigger criteria
  std::pair<std::string, std::string> m_tistosTagHlt2Crit; // TIS, TOS

  /// TisTag and TosTag electron L0 trigger criteria
  std::pair<std::string, std::string> m_electronTISTOSL0Crit; // TIS, TOS
  /// TisTag and TosTag electron HLT1 trigger criteria
  std::pair<std::string, std::string> m_electronTISTOSHlt1Crit; // TIS, TOS
  /// TisTag and TosTag electron HLT2 trigger criteria
  std::pair<std::string, std::string> m_electronTISTOSHlt2Crit; // TIS, TOS

  /// Fill muon TisTag flag
  bool m_fillMuonTISTagged;
  /// Fill muon TosTag flag
  bool m_fillMuonTOSTagged;

  /// Fill electron TisTag flag
  bool m_fillElectronTISTagged;
  /// Fill electron TosTag flag
  bool m_fillElectronTOSTagged;

  /// Fill infomation about bremsstrahlung
  bool m_fillBremInfo;
  const ANNGlobalPID::IChargedProtoANNPIDTool * m_pidTool;

  /// Tool to gather infomation on bremsstrahlung
  IBremAdder* m_bremAdder;

  /// Tool to find associated PV
  IRelatedPVFinder* m_pvFinder;
};
#endif // TUPLETOOLPIDCALIB_H
