/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "ApplySWeights.h"

// ROOT
#include "TH1D.h"
#include "TH2D.h"
#include "TFile.h"

//-----------------------------------------------------------------------------
// Implementation file for class : ApplySWeights
//
// 2015-06-15 : Lucio Anderlini        
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( ApplySWeights )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
ApplySWeights::ApplySWeights( const std::string& name,
    ISvcLocator* pSvcLocator)
  : DaVinciTupleAlgorithm ( name , pSvcLocator )
  , m_factory             (                    )
  , m_sTableMagUpFile     (                    )
  , m_sTableMagDownFile   (                    )
  , m_sTableDir           (                    )
  , m_sTableName          (                    )
  , m_preambulo           (                    )
  , m_inputTes            (                    )
  , m_condLocation        (                    )
  , m_file                (        NULL        )
  , m_map                 (        NULL        )
  , m_headCut ( LoKi::BasicFunctors<const LHCb::Particle*>::Constant (0)  )
  , m_cut     ( LoKi::BasicFunctors<const LHCb::Particle*>::Constant (0)  )
  , m_vars    (1, LoKi::BasicFunctors<const LHCb::Particle*>::Constant (0))
{
  declareProperty ( "Factory",    
      m_factory="LoKi::Hybrid::Tool/HybridFactory:PUBLIC" );
  declareProperty ( "sTableMagUpFile",   m_sTableMagUpFile );
  declareProperty ( "sTableMagDownFile", m_sTableMagDownFile );
  declareProperty ( "sTableDir",         m_sTableDir  );
  declareProperty ( "sTableName",        m_sTableName );
  declareProperty ( "Preambulo",         m_preambulo );
  declareProperty ( "InputTes",          m_inputTes );
  declareProperty ( "MagnetCondition",  
                    m_condLocation = "/dd/Conditions/Online/LHCb/Magnet/Set");
}
//=============================================================================
// Destructor
//=============================================================================
ApplySWeights::~ApplySWeights() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode ApplySWeights::initialize() {
  StatusCode sc = DaVinciTupleAlgorithm::initialize(); 
  if ( sc.isFailure() ) return sc;

  if (msgLevel(MSG::DEBUG)) debug() << "==> Initialize" << endmsg;

  registerCondition (m_condLocation,
                     m_condition, &ApplySWeights::i_updateCache);


  return StatusCode::SUCCESS;
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode ApplySWeights::execute() 
{
  if (msgLevel(MSG::DEBUG)) debug() << "==> Execute" << endmsg;
  StatusCode sc = StatusCode::SUCCESS ;

  LHCb::Particles *particleArray=getIfExists<LHCb::Particles>(m_inputTes);
  if (particleArray == NULL)
  {
    setFilterPassed(true);  // Mandatory. Set to true if event is accepted.
    return StatusCode::SUCCESS;
  }

  //const LHCb::Particle::Range particleArray = particles();
  LHCb::Particle::Range::iterator im, jm;

  for ( auto im : *particleArray )
  {
    double weight = 0;

    if (m_headCut ( im ) == 0) 
      continue;

    if (m_vars.size() == 1)
      weight = m_map->GetBinContent(m_map->FindBin(m_vars[0] (im)));
    else if (m_vars.size() == 2)
      weight = m_map->GetBinContent( static_cast < TH2D* >
          (m_map)->FindBin ( m_vars[0] ( im ), m_vars[1] ( im ))
        );
        

    if (m_cut ( im ) == 0)
      weight = 0;

    // Recursive lambda function
    std::function<void(LHCb::Particle*)> updateWeight = [&] (LHCb::Particle* part)  
    {
      part -> setWeight ( weight );
      for (auto jm : part->daughters())  updateWeight ( jm );
    }; 
    updateWeight ( im );


    if (msgLevel(MSG::DEBUG)) 
      debug() << "M: " << m_vars[0] ( im )  
              << "; Bin: " << m_map->FindBin(m_vars[0] (im)) 
              << "; Weight: " 
              << m_map->GetBinContent(m_map->FindBin ( m_vars[0] ( im ) ) )
              << endmsg;
  }
 
  setFilterPassed(true);  // Mandatory. Set to true if event is accepted.
  return StatusCode::SUCCESS;
}


//=============================================================================
//  Finalize
//=============================================================================
StatusCode ApplySWeights::finalize() {

  info() << "==> Finalize" << endmsg;
  if (msgLevel(MSG::DEBUG)) debug() << "==> Finalize" << endmsg;

  info() << "Deleting map" << endmsg;
  delete m_map;

  info() << "Closing " << m_file->GetName() << endmsg;
  m_file->Close();
  info() << "Done." << endmsg;

  return DaVinciTupleAlgorithm::finalize(); 
} 

//=============================================================================
// Update Cache
//==============================================================================
StatusCode ApplySWeights::i_updateCache (void)
{
  int polarity = m_condition->param<int> ("Polarity");
//  std::cout << "PROCESSING i_updateCache. Polarity " << polarity 
//            << " (Mag" << (polarity>0 ?"Up":"Down") << ")"
//            << std::endl;

  TNamed* buffer;
  std::string s;

  if (m_file && m_file->IsOpen())
    m_file->Close();

  if (polarity > 0) // MagUp
    m_file = TFile::Open ( m_sTableMagUpFile.c_str() );
  if (polarity < 0) // MagDown
    m_file = TFile::Open ( m_sTableMagDownFile.c_str() );

  if (!m_file) 
    return Error ( "Cannot open input map file.", StatusCode::FAILURE );

  s = m_sTableDir + "/Filter";
  m_file->GetObject ( s.c_str(), buffer ); 
  if (!buffer) return Error ( "Cannot find filter definition. ", 
                              StatusCode::FAILURE );
  std::string cut ( buffer -> GetTitle() );


  s = m_sTableDir + "/HeadSelection";
  m_file->GetObject ( s.c_str(), buffer ); 
  if (!buffer) return Error ( "Cannot find head selection definition. ", 
                              StatusCode::FAILURE );
  std::string headCut ( buffer -> GetTitle() );

  s = m_sTableDir + "/" + m_sTableName;
  m_file->GetObject ( s.c_str(), m_map );
  if (!m_map) return Error ( "Cannot open sTable. ", StatusCode::FAILURE );
 
  // get the factory
  LoKi::IHybridFactory* factory = tool<LoKi::IHybridFactory>(m_factory,this);
  //

  StatusCode sc = factory->get("switch("+cut+",1,0)", m_cut, m_preambulo);
  if (sc.isFailure()) return Error("Check cut: " + cut, sc) ;

  sc = factory->get("switch("+headCut+",1,0)", m_headCut, m_preambulo);
  if (sc.isFailure()) return Error("Check cut: " + headCut, sc) ;

  sc = factory->get(m_map->GetXaxis()->GetTitle(), 
                               m_vars[0], m_preambulo);
  if (sc.isFailure())
    return Error("Check x variable: " + 
            std::string(m_map->GetXaxis()->GetTitle()), sc) ;

  if (std::string(m_map->ClassName()) == std::string("TH2D"))
  {
    m_vars.push_back(LoKi::BasicFunctors<const LHCb::Particle*>::Constant(0));
    sc = factory->get(m_map->GetYaxis()->GetTitle(), 
                                 m_vars[1], m_preambulo);
    if (sc.isFailure())
      return Error("Check y variable: " + 
          std::string(m_map->GetXaxis()->GetTitle()), sc) ;
  }

  //
  release ( factory ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */); // we do not need the factory anymore 
  //

  return StatusCode::SUCCESS;
}

