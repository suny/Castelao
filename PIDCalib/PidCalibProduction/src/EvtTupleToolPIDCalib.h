/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef EVTTUPLETOOLPIDCALIB_H
#define EVTTUPLETOOLPIDCALIB_H 1

/// include files
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Kernel/IEventTupleTool.h"            // Interface

/// forward declarations
class ILHCbMagnetSvc;

/** @class EvtTupleToolPIDCalib EvtTupleToolPIDCalib.h CalibDataSel/EvtTupleToolPIDCalib.h
 *
 * \brief Adds a limited number of event-related variables used by PIDCalib for DecayTreeTuple.

 * Tuple columns:
 * - runNumber;
 * - eventNumber;
 * - magnet polarity;
 * - odin TCK;
 * - number of reconstructed primary vertices;
 * - number of best, long, downstream and muon tracks;
 * - number of RICH1 and RICH2 hits;
 * - number of SPD hits.

 * If Verbose is true, the following columns are also included:
 * - L0 and HLT TCKs;
 * - GPS time;
 * - bunch crossing ID and type;
 * - number of upstream, VELO, TT and backward tracks;
 * - number of VELO, OT, TT and IT clusters;
 * - number of 'Coords' in the muon stations.

 * \sa DecayTreeTuple
 * @author Philip Hunt
 * @date 2013-09-26
 */


class EvtTupleToolPIDCalib : public TupleToolBase,
                             virtual public IEventTupleTool
{
public:

  /// Standard constructor.
  EvtTupleToolPIDCalib( const std::string& type,
                        const std::string& name,
                        const IInterface* parent);

  StatusCode initialize() override;

  virtual ~EvtTupleToolPIDCalib( ); ///< Destructor.

  StatusCode fill( Tuples::Tuple& ) override;

private:
  /** Magnet detector service, used to
      determine the magnet polarity **/
  ILHCbMagnetSvc* m_magSvc ;
};

#endif // EVTTUPLETOOLPIDCALIB_H
