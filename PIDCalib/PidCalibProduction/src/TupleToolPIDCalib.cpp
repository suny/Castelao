/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

/// GaudiAlg
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"

/// Kernel
#include "Kernel/IParticleTransporter.h"
#include "Kernel/IDVAlgorithm.h"
#include "Kernel/ITriggerTisTos.h"
#include "Kernel/GetIDVAlgorithm.h"
#include "Kernel/IDistanceCalculator.h"
#include "Kernel/IBremAdder.h"
#include "Kernel/IRelatedPVFinder.h"
 #include "Kernel/IParticleTupleTool.h"
 #include "RecInterfaces/IChargedProtoANNPIDTool.h"
/// Event
#include "Event/Particle.h"

#include <boost/foreach.hpp>

// local
#include "TupleToolPIDCalib.h"

//-----------------------------------------------------------------------------
// Implementation file for class : TupleToolPIDCalib
//
// 2013-09-26 : Philip Hunt
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
// actually acts as a using namespace TupleTool
DECLARE_COMPONENT( TupleToolPIDCalib )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TupleToolPIDCalib::TupleToolPIDCalib( const std::string& type,
                                      const std::string& name,
                                      const IInterface* parent )
  : TupleToolBase ( type, name , parent ), m_transporter(0),
    m_dva(0), m_dist(0), m_l0TisTosTool(0), m_hltTisTosTool(0)
{
  declareInterface<IParticleTupleTool>(this);
  declareProperty( "Transporter", m_transporterName="ParticleTransporter:PUBLIC",
                   "Set the name of the particle transporter.");

  declareProperty( "FillPID", m_fillPID=true,
                   "Adds the RICH and muon PID variables (if applicable).");

  declareProperty( "L0TisTosToolName", m_l0TisTosToolName="L0TriggerTisTos",
    "Set the name of the TisTos tool used to retrieve the L0 decisions.");

  declareProperty( "HltTisTosToolName", m_hltTisTosToolName="TriggerTisTos",
    "Set the name of the TisTos tool used to retrieve the HLT decisions.");

  declareProperty( "FillElectronTagAndProbe", m_fillElectronTagProbe=false,
                   "For electrons, flag whether the particle " \
                   "satisfies the tag or probe criteria set in the electron " \
                   "calibration stripping lines.");

  declareProperty( "ElectronTagMinP", m_electronTagP=6000.0,
                   "Set the minimum momentum of the tagged electron.");
  declareProperty( "ElectronProbeMinP", m_electronProbeP=3000.0,
                   "Set the minimum momentum of the probe electron.");

  declareProperty( "ElectronTagMinPt", m_electronTagPt=1500.0,
                   "Set the minimum pt of the tagged electron.");
  declareProperty( "ElectronProbeMinPt", m_electronProbePt=500.0,
                   "Set the minimum pt of the probe electron.");

  declareProperty( "ElectronTagMinIPChi2PV", m_electronTagIpChi2=9.0,
                   "Set the minimum IP chi^2 of the tagged electron with respect to associated PV.");
  declareProperty( "ElectronProbeMinIPChi2PV", m_electronProbeIpChi2=9.0,
                   "Set the minimum IP chi^2 of the probe electron with respect to associated PV.");

  declareProperty( "ElectronTagDLLe", m_electronTagDLLe=5.0,
                   "Set the minimum DLL(e-pi) of the tag electron.");


  declareProperty( "FillElectronTisTagged", m_fillElectronTISTagged=false,
                   "Flags whether the electron candidate is " \
                   "TIS with respect to specific hardware and software triggers." );

  declareProperty( "ElectronTisL0Criterion",
    m_electronTISTOSL0Crit.first="L0ElectronDecision",
    "Sets the L0 TIS decision regular expression used to check for 'TISTagged' particles.");
  declareProperty( "ElectronTisHlt1Criterion",
    m_electronTISTOSHlt1Crit.first="Hlt1PhysDecision",
    "Sets the HLT1 TIS decision regular expression used to check for 'TISTagged' particles.");
  declareProperty( "ElectronTisHlt2Criterion",
    m_electronTISTOSHlt2Crit.first="Hlt2PhysDecision",
    "Sets the HLT2 TIS decision regular expression usedto check for 'TISTagged' particles.");

  declareProperty( "FillElectronTosTagged", m_fillElectronTOSTagged=false,
                   "Flag whether the electron candidate " \
                   "is TOS with respect to specific hardware and software triggers." );

  declareProperty( "ElectronTosL0Criterion",
    m_electronTISTOSL0Crit.second="L0ElectronDecision",
    "Sets the L0 TIS decision regular expression to check for 'TOSTagged' particles");
  declareProperty( "ElectronTosHlt1Criterion",
    m_electronTISTOSHlt1Crit.second="Hlt1PhysDecision",
    "Sets the HLT1 TIS decision regular expression to check for 'TOSTagged' particles.");
  declareProperty( "ElectronTosHlt2Criterion",
    m_electronTISTOSHlt2Crit.second="Hlt2PhysDecision",
    "Sets the HLT2 TIS decision regular expression to check for 'TOSTagged' particles.");


  declareProperty( "FillMuonTagAndProbe", m_fillMuonTagProbe=false,
                   "For muons, flag whether the particle " \
                   "satisfies the tag or probe criteria set in the muon " \
                   "calibration stripping lines.");

  declareProperty( "MuonTagMinP", m_tagMinMom=6000.0,
                   "Set the minimum momentum of the tagged muon.");
  declareProperty( "MuonProbeMinP", m_probMinMom=3000.0,
                   "Set the minimum momentum of the probe muon.");

  declareProperty( "MuonTagMinPt", m_tagMinPt=1500.0,
                   "Set the minimum transverse momentum of the tagged muon.");
  declareProperty( "MuonProbeMinPt", m_probMinPt=800.0,
                   "Set the minimum transverse momentum of the probe muon.");

  declareProperty( "MuonTagMinIPChi2PV", m_tagMinIPchi2PV=25.0,
                   "Set the minimum IP chi^2 of the tagged muon with respect to ANY PV.");
  declareProperty( "MuonProbeMinIPChi2PV", m_probMinIPchi2PV=10.0,
                   "Set the minimum IP chi^2 of the probe muon with respect to ANY PV.");

  declareProperty( "MuonProbeMaxTrChi2DOF", m_probMaxTrChi2DOF=3.0,
                   "Set the maximum track chi^2/d.o.f. of the probe muon.");

  declareProperty( "FillMuonTisTagged", m_fillMuonTISTagged=false,
                   "Flags whether the particle is " \
                   "TIS with respect to specific hardware and software triggers." );

  declareProperty( "MuonTisL0Criterion",
		   //   m_tistosTagL0Crit.first="L0(Hadron|Muon|DiMuon)Decision",
		   //    "Sets the L0 TIS decision regular expression used to check for 'TISTagged' particles.");

m_tistosTagL0Crit.first="L0.*Decision",
		       "Sets the L0 TIS decision regular expression used to check for 'TISTagged' particles.");
 


 declareProperty( "MuonTisHlt1Criterion",
                  //		  //   m_tistosTagHlt1Crit.first="Hlt1(TrackAllL0|TrackMuon|SingleMuonHighPT)Decision",
                  m_tistosTagHlt1Crit.first="Hlt1(?!ODIN)(?!L0)(?!Lumi)(?!Tell1)(?!MB)(?!NZS)(?!Velo)(?!BeamGas)(?!Incident).*Decision",
                  "Sets the HLT1 TIS decision regular expression used to check for 'TISTagged' particles.");
  declareProperty( "MuonTisHlt2Criterion",
    m_tistosTagHlt2Crit.first="Hlt2(?!Forward)(?!DebugEvent)(?!Express)(?!Lumi)(?!Transparent)(?!PassThrough).*Decision",
    "Sets the HLT2 TIS decision regular expression usedto check for 'TISTagged' particles.");

  declareProperty( "FillMuonTosTagged", m_fillMuonTOSTagged=false,
                   "Flag whether the muon candidate " \
                   "is TOS with respect to specific hardware and software triggers." );

  declareProperty( "MuonTosL0Criterion",
    m_tistosTagL0Crit.second="L0MuonDecision",
    "Sets the L0 TIS decision regular expression to check for 'TOSTagged' particles");
  declareProperty( "MuonTosHlt1Criterion",
    m_tistosTagHlt1Crit.second="Hlt1(TrackAllL0|TrackMuon|SingleMuonHighPT)Decision",
    "Sets the HLT1 TIS decision regular expression to check for 'TOSTagged' particles.");
  declareProperty( "MuonTosHlt2Criterion",
    m_tistosTagHlt2Crit.second="Hlt2(SingleMuon|SingleMuonHighPT)Decision",
    "Sets the HLT2 TIS decision regular expression to check for 'TOSTagged' particles.");

  declareProperty( "FillRichRadInfo", m_fillRichRadInfo=true,
                   "Adds information on whether the track is above the threshold of " \
                   "various track types, and whether information from a particular " \
                   "radiator was used to determine the RICH particle ID "\
                   "(NB. the radiator variables are only filled if FillPID "\
                   "is also set to true).");

  declareProperty( "FillBremInfo", m_fillBremInfo=false,
                   "Adds information on bremsstrahlung of the particle.");

}

//=============================================================================
// Destructor
//=============================================================================
TupleToolPIDCalib::~TupleToolPIDCalib() {}

StatusCode TupleToolPIDCalib::initialize()
{
  const StatusCode sc = TupleToolBase::initialize();
  if ( sc.isFailure() ) return sc;
  m_transporter = tool<IParticleTransporter>(m_transporterName, this);
  if (!m_transporter)
  {
    return Error("Unable to retrieve IParticleTransporter tool "+m_transporterName);
  }
  info () << "m_fillMuonTagProbe || m_fillElectronTagProbe: "
          << int(m_fillMuonTagProbe || m_fillElectronTagProbe)
          << endmsg;
  if ( m_fillMuonTagProbe || m_fillElectronTagProbe )
  {
    m_dva = Gaudi::Utils::getIDVAlgorithm(contextSvc(), this);
    if (!m_dva)
    {
      return Error("Unable to retrieve parent DVAlgorithm");
    }
    m_dist = m_dva->distanceCalculator();
    if (!m_dist)
    {
      return Error("Unable to retrieve the IDistanceCalculator tool");
    }
  }

  m_l0TisTosTool = tool<ITriggerTisTos>( m_l0TisTosToolName, this);
  if (!m_l0TisTosTool)
  {
    return Error("Unable to retrieve ITriggerTisTos tool "+m_l0TisTosToolName);
  }
  m_hltTisTosTool = tool<ITriggerTisTos>( m_hltTisTosToolName, this );
  if (!m_hltTisTosTool)
  {
    return Error("Unable to retrieve ITriggerTisTos tool "+m_hltTisTosToolName);
  }
  m_bremAdder = tool<IBremAdder>("BremAdder","BremAdder" ,this );
  if (!m_bremAdder)
  {
    return Error("Unable to retrieve IBremAdder tool ");
  }
  // m_pvFinder = tool<IRelatedPVFinder>("GenericParticle2PVRelator__p2PVWithIPChi2_OfflineDistanceCalculatorName_",this);
 m_pvFinder = tool<IRelatedPVFinder>("GenericParticle2PVRelator<_p2PVWithIPChi2, OfflineDistanceCalculatorName>",this);
  if (!m_bremAdder)
  {
    return Error("Unable to retrieve IRelatedPVFinder tool ");
  }
 m_pidTool =tool<ANNGlobalPID::IChargedProtoANNPIDTool>( "ANNGlobalPID::ChargedProtoANNPIDTool","ChargedProtoANNPID" );

  return sc;
}

StatusCode TupleToolPIDCalib::fill( const LHCb::Particle* /*mother*/,
                                    const LHCb::Particle* P,
                                    const std::string& head,
                                    Tuples::Tuple& tuple )
{
  const std::string prefix=fullName(head);
  bool test = true;
  if (P) {
    test &= fillKinematics(P, head, tuple);
    if (m_fillPID) {
      test &= fillPID(P, head, tuple);
    }
    if (m_fillMuonTagProbe) {
      test &= fillMuonTagProbe(P, head, tuple);
    }
    if (m_fillMuonTISTagged) {
      test &= fillMuonTisTagged(P, head, tuple);
    }
    if (m_fillMuonTOSTagged) {
      test &= fillMuonTosTagged(P, head, tuple);
    }
    if (m_fillElectronTagProbe) {
      test &= fillElectronTagProbe(P, head, tuple);
    }
    if (m_fillElectronTISTagged) {
      test &= fillElectronTisTagged(P, head, tuple);
    }
    if (m_fillElectronTOSTagged) {
      test &= fillElectronTosTagged(P, head, tuple);
    }
    if (m_fillBremInfo) {
      test &= fillBremInfo(P, head, tuple);
    }
  }
  else {
    return StatusCode::FAILURE;
  }
  return StatusCode(test);
}

StatusCode TupleToolPIDCalib::fillKinematics(const LHCb::Particle* P,
                                             const std::string& head,
                                             Tuples::Tuple& tuple )
{
  const std::string prefix=fullName(head);
  bool test = true;
  if (P) {
    // float p = P->p();
    //float pt = P->pt();
    int charge = P->charge();
    //    test &= tuple->column( prefix+"_P", p );
    //test &= tuple->column( prefix+"_PT", pt );
    test &= tuple->column( prefix+"_trackcharge", charge );
    //const Gaudi::LorentzVector& p4 = P->momentum();
    //float eta = p4.eta();
    //float phi = p4.phi();
    //test &= tuple->column( prefix+"_ETA", eta );
    //test &= tuple->column( prefix+"_PHI", phi );
    //if ( !P->isBasicParticle() ||  P->particleID().pid()  ==  111 ) {
    //float m = p4.mass();
      //  test &= tuple->column( prefix+"_M", m);
    //}
  }
  else {
    return StatusCode::FAILURE;
  }
  return StatusCode(test);
}

StatusCode TupleToolPIDCalib::fillPID(const LHCb::Particle* P,
                                      const std::string& head,
                                      Tuples::Tuple& tuple )
{
  const std::string prefix=fullName(head);
  bool test = true;

  if (P) {
    // int Assign_PDGID = P->particleID().pid();
    // Rich::ParticleIDType Assign_RICHID = Rich::Unknown;
    // if(abs(Assign_PDGID)==211)
    // {
    //   Assign_RICHID = Rich::Pion;
    // }
    // else if(abs(Assign_PDGID)==321)
    // {
    //   Assign_RICHID = Rich::Kaon;
    // }
    // else if(abs(Assign_PDGID)==11)
    // {
    //   Assign_RICHID = Rich::Electron;
    // }
    // else if(abs(Assign_PDGID)==13)
    // {
    //   Assign_RICHID = Rich::Muon;
    // }
    // else if(abs(Assign_PDGID)==2212)
    // {
    //   Assign_RICHID = Rich::Proton;
    // }
    test &= tuple->column( prefix+"_ID", P->particleID().pid() );
    if ( !P->isBasicParticle() ) return StatusCode(test); // no PID info for composite!
    if ( isPureNeutralCalo(P)  ) return StatusCode(test); // no PID information for calo neutrals
    const LHCb::ProtoParticle* proto = P->proto();
    if ( proto )
    {
      //float dlle = proto->info(LHCb::ProtoParticle::CombDLLe,-1000.);
      //float dllmu = proto->info(LHCb::ProtoParticle::CombDLLmu,-1000.);
      //float dllk = proto->info(LHCb::ProtoParticle::CombDLLk,-1000.);
      //float dllp = proto->info(LHCb::ProtoParticle::CombDLLp,-1000.);

      // Combined DLLs  ----// Already added with Loki
      //      test &= tuple->column( prefix+"_PIDe", dlle );
      //test &= tuple->column( prefix+"_PIDmu", dllmu );
      //test &= tuple->column( prefix+"_PIDK", dllk);
      //test &= tuple->column( prefix+"_PIDp", dllp);

      
      //const ANNGlobalPID::IChargedProtoANNPIDTool * m_pidTool;

std::string m_pidTunesv2 = "MC12TuneV2";
std::string m_pidTunesv3 = "MC12TuneV3";
//typedef ANNGlobalPID::IChargedProtoANNPIDTool::RetType AnnRet;

// AnnRet V2probNNe = m_pidTool->annPID(proto,LHCb::ParticleID(11),m_pidTunesv2);
 //test &= tuple->column(prefix+"_V2ProbNNe",V2probNNe.value);
// AnnRet V3probNNe = m_pidTool->annPID(proto,LHCb::ParticleID(11),m_pidTunesv3);
 //test &= tuple->column(prefix+"_V3ProbNNe",V3probNNe.value);

//AnnRet V2probNNmu = m_pidTool->annPID(proto,LHCb::ParticleID(13),m_pidTunesv2);
//test &= tuple->column(prefix+"_V2ProbNNmu",V2probNNmu.value);
// AnnRet V3probNNmu = m_pidTool->annPID(proto,LHCb::ParticleID(13),m_pidTunesv3);
 //test &= tuple->column(prefix+"_V3ProbNNmu",V3probNNmu.value);

//AnnRet V2probNNpi = m_pidTool->annPID(proto,LHCb::ParticleID(211),m_pidTunesv2);
//test &= tuple->column(prefix+"_V2ProbNNpi",V2probNNpi.value);
// AnnRet V3probNNpi = m_pidTool->annPID(proto,LHCb::ParticleID(211),m_pidTunesv3);
 //test &= tuple->column(prefix+"_V3ProbNNpi",V3probNNpi.value);

//AnnRet V2probNNk = m_pidTool->annPID(proto,LHCb::ParticleID(321),m_pidTunesv2);
//test &= tuple->column(prefix+"_V2ProbNNk",V2probNNk.value);
// AnnRet V3probNNk = m_pidTool->annPID(proto,LHCb::ParticleID(321),m_pidTunesv3);
 //test &= tuple->column(prefix+"_V3ProbNNk",V3probNNk.value);


//AnnRet V2probNNp = m_pidTool->annPID(proto,LHCb::ParticleID(2212),m_pidTunesv2);
//test &= tuple->column(prefix+"_V2ProbNNp",V2probNNp.value);
// AnnRet V3probNNp = m_pidTool->annPID(proto,LHCb::ParticleID(2212),m_pidTunesv3);
 // test &= tuple->column(prefix+"_V3ProbNNp",V3probNNp.value);
//AnnRet V2probNNghost = m_pidTool->annPID(proto,LHCb::ParticleID(0),m_pidTunesv2);
//test &= tuple->column(prefix+"_V2ProbNNghost",V2probNNghost.value);
// AnnRet V3probNNghost = m_pidTool->annPID(proto,LHCb::ParticleID(0),m_pidTunesv3);
 //test &= tuple->column(prefix+"_V3ProbNNghost",V3probNNghost.value);
      // The NeuroBays PID probabilities

  // There is one for each hypothesis - not anymore - testing i get the same
  //    float probNNe = proto->info(LHCb::ProtoParticle::ProbNNe,-1000.);
      //      test &= tuple->column( prefix+"_ProbNNe", probNNe );
//  float probNNk = proto->info(LHCb::ProtoParticle::ProbNNk,-1000.);
      //test &= tuple->column( prefix+"_ProbNNk", probNNk );
//    float probNNp = proto->info(LHCb::ProtoParticle::ProbNNp,-1000.);
      //test &= tuple->column( prefix+"_ProbNNp", probNNp );
//    float probNNpi = proto->info(LHCb::ProtoParticle::ProbNNpi,-1000.);
      //test &= tuple->column( prefix+"_ProbNNpi", probNNpi);
//    float probNNmu = proto->info(LHCb::ProtoParticle::ProbNNmu,-1000.);
      //test &= tuple->column( prefix+"_ProbNNmu", probNNmu);
//    float probNNghost = proto->info(LHCb::ProtoParticle::ProbNNghost,-1000.);
      //test &= tuple->column( prefix+"_ProbNNghost", probNNghost);



      // Muon
      int inAccMuon = proto->info(LHCb::ProtoParticle::InAccMuon, -1000);
      const LHCb::MuonPID* muonPID = proto->muonPID();
      int NShared  = -1;
      bool hasMuon = false;
      bool isMuon = false;
      bool isMuonLoose = false;
      bool isMuonTight = false;
      double chi2Corr = -1.;
      double mva1 = -10.;
      double mva2 = -10.;

      const LHCb::Track *track=proto->track();
      float fitmatchchi2=track->info(LHCb::Track::FitMatchChi2,-1);
      // float muonDist2 = -9999.;
      if (muonPID) {
        hasMuon = true;
        isMuon = muonPID->IsMuon();
        isMuonLoose = muonPID->IsMuonLoose();
        isMuonTight = muonPID->IsMuonTight();
        NShared = muonPID->nShared();
        chi2Corr = muonPID->chi2Corr();
        mva1 = muonPID->muonMVA1();
        mva2 = muonPID->muonMVA2();
        //   const LHCb::Track* track = muonPID->muonTrack();
        //   if (track) {
        //     muonDist2 = track->info(LHCb::Track::MuonDist2, -1000.);
        //   }
      }
      test &= tuple->column( prefix+"_hasMuon", hasMuon );
      test &= tuple->column( prefix+"_isMuon",  isMuon );
      test &= tuple->column( prefix+"_isMuonLoose",  isMuonLoose );
      test &= tuple->column( prefix+"_isMuonTight",  isMuonTight );
      test &= tuple->column( prefix+"_NShared", NShared);
      test &= tuple->column( prefix+"_InMuonAcc", inAccMuon);
      test &= tuple->column( prefix+"_chi2Corr", chi2Corr);
      test &= tuple->column( prefix+"_muonMVA1", mva1);
      test &= tuple->column( prefix+"_muonMVA2", mva2);

      test &= tuple->column( prefix+"_TRACK_MatchCHI2", fitmatchchi2 );


      // test &= tuple->column( prefix+"_MuonDist2",  muonDist2);

      // RICH
      const LHCb::RichPID * richPID = proto->richPID();

      bool hasRich=false;
      // int richThresh=-1;
      int richThresh_el=-1;
      int richThresh_mu=-1;
      int richThresh_pi=-1;
      int richThresh_k=-1;
      int richThresh_pr=-1;
      int richAerogelUsed=-1;
      int rich1GasUsed=-1;
      int rich2GasUsed=-1;

      if (richPID) {
        hasRich=true;
        if (m_fillRichRadInfo)
        {
          // richThresh=richPID->isAboveThreshold(Assign_RICHID);
          richThresh_el=richPID->isAboveThreshold(Rich::Electron);
          richThresh_mu=richPID->isAboveThreshold(Rich::Muon);
          richThresh_pi=richPID->isAboveThreshold(Rich::Pion);
          richThresh_k=richPID->isAboveThreshold(Rich::Kaon);
          richThresh_pr=richPID->isAboveThreshold(Rich::Proton);
          richAerogelUsed=richPID->traversedRadiator(Rich::Aerogel);
          rich1GasUsed=richPID->traversedRadiator(Rich::Rich1Gas);
          rich2GasUsed=richPID->traversedRadiator(Rich::Rich2Gas);
        }
      }

      test &= tuple->column( prefix+"_hasRich", hasRich );

      // CALO
      test &= tuple->column( prefix+"_hasCalo", !proto->calo().empty() );

      if (m_fillRichRadInfo)
      {
        //test&= tuple->column( prefix+"_RICHThreshold", richThresh);
        test &= tuple->column( prefix+"_RICHThresholdPi", richThresh_pi);
        test &= tuple->column( prefix+"_RICHThresholdKa", richThresh_k);
        test &= tuple->column( prefix+"_RICHThresholdPr", richThresh_pr);
        test &= tuple->column( prefix+"_RICHThresholdEl", richThresh_el);
        test &= tuple->column( prefix+"_RICHThresholdMu", richThresh_mu);
        test &= tuple->column( prefix+"_RICHAerogelUsed", richAerogelUsed);
        test &= tuple->column( prefix+"_RICH1GasUsed", rich1GasUsed);
        test &= tuple->column( prefix+"_RICH2GasUsed", rich2GasUsed);
      }
    } // end ProtoParticle!=NULL
  } // end Particle!=NULL
  else
  {
    return StatusCode::FAILURE;
  }
  return StatusCode(test);
}

StatusCode TupleToolPIDCalib::getMinIP(const LHCb::Particle* P,
                                       float& ip, float& chi2)
{
  if (!P) return StatusCode::FAILURE;
  ip=-1.;
  chi2=-1.;
  if(msgLevel(MSG::VERBOSE)) verbose() << "Looking for Min IP"  << endmsg  ;

  const LHCb::RecVertex::Range PV = m_dva->primaryVertices();
  if(msgLevel(MSG::VERBOSE)) verbose() << "PV size: "  << PV.size() << endmsg  ;
  if ( !PV.empty() )
  {
    if(msgLevel(MSG::VERBOSE)) verbose() << P << " PVs:" << PV.size() << endmsg ;
    for ( LHCb::RecVertex::Range::const_iterator pv = PV.begin();
          pv!=PV.end() ; ++pv)
    {
      LHCb::RecVertex newPV(**pv);
      double currentip, currentchi2=-1.;
      LHCb::VertexBase* newPVPtr = (LHCb::VertexBase*)&newPV;
      StatusCode test2 = m_dist->distance ( P, newPVPtr,
                                            currentip, currentchi2 );
      if (test2.isSuccess()) {
        if ( (currentip<(float)ip) || (ip<0.) )
        {
          ip = (float)currentip;
        }
        if ( (currentchi2<(float)chi2) || (chi2<0.) )
        {
          chi2 = (float)currentchi2;
        }
      }
    }
  }
  return StatusCode::SUCCESS;
}

StatusCode TupleToolPIDCalib::getIP(const LHCb::Particle* P,
                                       float& ip, float& chi2)
{
  if (!P) return StatusCode::FAILURE;
  ip=-1.;
  chi2=-1.;
  if(msgLevel(MSG::VERBOSE)) verbose() << "Looking for IP"  << endmsg  ;

  const LHCb::RecVertex::Range pVertices = m_dva->primaryVertices();
  if ( pVertices.empty() ) {
    if(msgLevel(MSG::VERBOSE)) verbose() << "No PVs?!?!"  << endmsg  ;
    return StatusCode::FAILURE;
  }
  const LHCb::VertexBase* bestPV = m_pvFinder->relatedPV( P, pVertices);
  if ( !bestPV ) {
    if(msgLevel(MSG::VERBOSE)) verbose() << "No best PV?"  << endmsg  ;
    return StatusCode::FAILURE;
  }

  double ipd = -1.;
  double chi2d = -1;

  StatusCode sc = m_dist->distance ( P, bestPV, ipd, chi2d );

  ip = ipd; chi2 = chi2d;

  return sc;
}

StatusCode TupleToolPIDCalib::getTrChi2DOF(const LHCb::Particle* P,
                                           float& chi2DOF)
{
  if (!P) return StatusCode::FAILURE;

  chi2DOF=1.0e6;

  //first just return if the particle isn't supposed to have a track
  if ( !P->isBasicParticle() ) return StatusCode::SUCCESS;

  const LHCb::ProtoParticle* protop = P->proto();
  if (!protop) return StatusCode::SUCCESS;

  const LHCb::Track* track = protop->track();
  if (!track) return StatusCode::SUCCESS;

  if (msgLevel(MSG::DEBUG))
    debug() << P << " got track type" << track->type()
            << ", TRACK_CHI2 = " << track->chi2() << endmsg ;
  if (msgLevel(MSG::VERBOSE)) verbose() << *track << endmsg ;

  int nDOF = track->nDoF();
  chi2DOF = track->chi2()/nDOF;

  return StatusCode::SUCCESS;
}

StatusCode TupleToolPIDCalib::fillMuonTagProbe(const LHCb::Particle* P,
                                               const std::string& head,
                                               Tuples::Tuple& tuple )
{
  const std::string prefix=fullName(head);
  bool test = true;

  if (P) {

    // just return if the particle isn't a muon
    if ( abs(P->particleID().pid()) != 13 ) return StatusCode::SUCCESS;

    // get the tag-and-probe cut variables
    float p = P->p();
    float pt = P->pt();

    float minip, minipchi2 = 0.;
    StatusCode sc = getMinIP(P, minip, minipchi2);
    if (sc.isFailure()) return StatusCode::FAILURE;

    float trChi2DOF = 0.;
    sc = getTrChi2DOF(P, trChi2DOF);
    if (sc.isFailure()) return StatusCode::FAILURE;

    //bool hasMuonTagProbe = false;
    bool probe = true;
    //int inAccMuon = -2;
    probe &= (p>m_probMinMom);
    probe &= (pt>m_probMinPt);
    probe &= (trChi2DOF<m_probMaxTrChi2DOF);
    probe &= (minipchi2>m_probMinIPchi2PV);
    //need to add the isLong requirement
    bool tag = false;
    const LHCb::ProtoParticle* proto = P->proto();
    if ( proto )
    {
      const LHCb::MuonPID* muonPID = proto->muonPID();
      //inAccMuon = proto->info(LHCb::ProtoParticle::InAccMuon, -1);
      //if (inAccMuon>=0)
      if (muonPID)
      {
        //hasMuonTagProbe=true;
        //probe = true;
        //probe &= bool(inAccMuon);

        tag = true;
        bool isMuon = muonPID->IsMuon();
        tag &= (probe);
        tag &= (isMuon);
        tag &= (p>m_tagMinMom);
        tag &= (pt>m_tagMinPt);
        tag &= (minipchi2>m_tagMinIPchi2PV);
      }
    }
    //test &= tuple->column(prefix+"_hasMuonTagProbe", hasMuonTagProbe);
    test &= tuple->column(prefix+"_Probe", probe);
    test &= tuple->column(prefix+"_Tag", tag);
    if (isVerbose())
    {
      // no need to include p, pt or muonInAcc, as these are filled
      // by other methods
      test &= tuple->column(prefix+"_MIP_PV", minip);
      test &= tuple->column(prefix+"_MIPCHI2_PV", minipchi2);
      test &= tuple->column(prefix+"_TRCHI2DOF", trChi2DOF);
    }
  }
  else {
    return StatusCode::FAILURE;
  }
  return StatusCode(test);
}

StatusCode TupleToolPIDCalib::getTisTosDecision(const LHCb::Particle* P,
    const TupleToolPIDCalib::TisTosType& tistosType,
    const TupleToolPIDCalib::TriggerType& triggerType,
    bool& dec,
    const TupleToolPIDCalib::ParticleType& particleType = TupleToolPIDCalib::Muon)
{
  if (tistosType==TupleToolPIDCalib::UnknownTisTosType) {
    return Warning("TisTosType was not specified. This should not happen!", StatusCode::FAILURE, 1);
  }
  if (triggerType==TupleToolPIDCalib::UnknownTriggerType) {
    return Warning("TriggerType was not specified. This should not happen!", StatusCode::FAILURE, 1);
  }
  if (particleType==TupleToolPIDCalib::UnknownParticleType) {
    return Warning("TriggerType was not specified. This should not happen!", StatusCode::FAILURE, 1);
  }
  if (!P) return StatusCode::FAILURE;

  ITriggerTisTos* tistosTool = NULL;
  std::pair<std::string, std::string> trigCrit;

  if (particleType == TupleToolPIDCalib::Muon) {
    if (triggerType==TupleToolPIDCalib::L0) {
      tistosTool = m_l0TisTosTool;
      trigCrit = m_tistosTagL0Crit;
    }
    else {
      tistosTool = m_hltTisTosTool;
      trigCrit = (triggerType==TupleToolPIDCalib::Hlt1) ? m_tistosTagHlt1Crit : m_tistosTagHlt2Crit;
    }
  } else if (particleType == TupleToolPIDCalib::Electron){ // its an electron!
    if (triggerType==TupleToolPIDCalib::L0) {
      tistosTool = m_l0TisTosTool;
      trigCrit = m_electronTISTOSL0Crit;
    }
    else {
      tistosTool = m_hltTisTosTool;
      trigCrit = (triggerType==TupleToolPIDCalib::Hlt1) ? m_electronTISTOSHlt1Crit : m_electronTISTOSHlt2Crit;
    }
  }

  tistosTool->setOfflineInput(*P);

  if (tistosType==TupleToolPIDCalib::TIS) {
    tistosTool->setTriggerInput(trigCrit.first);
    dec = tistosTool->tisTrigger();
  }
  else {
    tistosTool->setTriggerInput(trigCrit.second);
    dec = tistosTool->tosTrigger();
  }
  return StatusCode::SUCCESS;
}

StatusCode TupleToolPIDCalib::fillMuonTisTagged(const LHCb::Particle* P,
                                                const std::string& head,
                                                Tuples::Tuple& tuple)
{
  return fillTisTosTagged(P, head, tuple, TupleToolPIDCalib::TIS, TupleToolPIDCalib::Muon);
}

StatusCode TupleToolPIDCalib::fillMuonTosTagged(const LHCb::Particle* P,
                                                const std::string& head,
                                                Tuples::Tuple& tuple)
{
  return fillTisTosTagged(P, head, tuple, TupleToolPIDCalib::TOS, TupleToolPIDCalib::Muon);
}


StatusCode TupleToolPIDCalib::fillElectronTisTagged(const LHCb::Particle* P,
                                                const std::string& head,
                                                Tuples::Tuple& tuple)
{
  return fillTisTosTagged(P, head, tuple, TupleToolPIDCalib::TIS, TupleToolPIDCalib::Electron);
}

StatusCode TupleToolPIDCalib::fillElectronTosTagged(const LHCb::Particle* P,
                                                const std::string& head,
                                                Tuples::Tuple& tuple)
{
  return fillTisTosTagged(P, head, tuple, TupleToolPIDCalib::TOS, TupleToolPIDCalib::Electron);
}

StatusCode TupleToolPIDCalib::fillTisTosTagged(const LHCb::Particle* P,
                                                    const std::string& head,
                                                    Tuples::Tuple& tuple,
                                                    const TupleToolPIDCalib::TisTosType& tisTosType,
                                                    const TupleToolPIDCalib::ParticleType& particleType)
{

  const std::string prefix=fullName(head);
  bool test = true;

  if (P)
  {
    if ( !P->isBasicParticle() ) return StatusCode::SUCCESS; // can't do PID calibration of composite!
    if ( isPureNeutralCalo(P)  ) return StatusCode::SUCCESS; // can't do PID calibration for calo neutrals

    bool l0Tagged = false;
    StatusCode sc = getTisTosDecision(P, tisTosType, TupleToolPIDCalib::L0,
                                      l0Tagged);
    if (sc.isFailure()) return StatusCode::FAILURE;

    bool hlt1Tagged = false;
    sc = getTisTosDecision(P, tisTosType, TupleToolPIDCalib::Hlt1, hlt1Tagged);
    if (sc.isFailure()) return StatusCode::FAILURE;

    bool hlt2Tagged = false;
    sc = getTisTosDecision(P, tisTosType, TupleToolPIDCalib::Hlt2, hlt2Tagged);
    if (sc.isFailure()) return StatusCode::FAILURE;

    std::string sType = "";

    if (particleType == TupleToolPIDCalib::Muon) {
      sType = (tisTosType==TupleToolPIDCalib::TIS) ? "MuonTisTagged" : "MuonTosTagged";
    } else if (particleType == TupleToolPIDCalib::Electron) {
      sType = (tisTosType==TupleToolPIDCalib::TIS) ? "ElectronTisTagged" : "ElectronTosTagged";
    }

    test &= tuple->column(prefix+"_UT2_"+sType,
                          l0Tagged&&hlt1Tagged&&hlt2Tagged);

  test &= tuple->column(prefix+"_UT1_"+sType,
                          l0Tagged&&hlt1Tagged);
  // if (isVerbose())
  // {
      test&= tuple->column(prefix+"_L0"+sType, l0Tagged);
      test&= tuple->column(prefix+"_Hlt1"+sType, hlt1Tagged);
      test&= tuple->column(prefix+"_Hlt2"+sType, hlt2Tagged);
      //}
  }
  else
  {
    return StatusCode::FAILURE;
  }
  return StatusCode(test);
}

TupleToolPIDCalib::EcalRegion TupleToolPIDCalib::EcalRegionFinder(double x, double y, double z) {
  // roughly around the ecal which is at z~12m
  if ( abs(z-12.*Gaudi::Units::m)<2.*Gaudi::Units::m ) {
    if (abs(x) < 325.1*Gaudi::Units::mm and abs(y) < 243.8*Gaudi::Units::mm) {
      return TupleToolPIDCalib::BeamPipe;
    } else if (abs(x) < 975.2*Gaudi::Units::mm and abs(y) < 731.4*Gaudi::Units::mm) {
      return TupleToolPIDCalib::Inner;
    } else if (abs(x) < 1950.4*Gaudi::Units::mm and abs(y) < 1219.0*Gaudi::Units::mm) {
      return TupleToolPIDCalib::Middle;
    } else if (abs(x) < 3900.8*Gaudi::Units::mm and abs(y) < 3169.4*Gaudi::Units::mm) {
      return TupleToolPIDCalib::Outer;
    }
    return TupleToolPIDCalib::UnknownRegion;
  } 
  return TupleToolPIDCalib::UnknownRegion;
}



StatusCode TupleToolPIDCalib::fillBremInfo(const LHCb::Particle* P,
                                                    const std::string& head,
                                                    Tuples::Tuple& tuple)
{
  const std::string prefix=fullName(head);
  bool test = true;
  int bremmulti(-1);
  int calosize(-1);
  int ecalregion(-1);
  int bremadded(-1);
  double calox(-999.);
  double caloy(-999.);
  double caloz(-999.);
  double lh(-999.);
  double hyp(-999.);
  double bremp(-999.);
  //  if(msgLevel(MSG::INFO)) info() << "PV size: Sneha Debug 1" << endmsg;  
  if (P)
  {
    //if(msgLevel(MSG::INFO)) info() << "PV size: Sneha Debug 2" << endmsg;  
    LHCb::CaloMomentum brem =
      m_bremAdder->bremMomentum( P, Gaudi::Utils::toString(P->particleID().pid()) );
    //  if(msgLevel(MSG::INFO)) info() << "PV size: Sneha Debug 2a" << endmsg;  
    bremmulti = brem.multiplicity();
    //  if(msgLevel(MSG::INFO)) info() << "PV size: Sneha Debug 2b" << endmsg;  
    bremp = brem.momentum().P();
    //      if(msgLevel(MSG::INFO)) info() << "PV size: Sneha Debug 2c" << endmsg;  
    bremadded = P->info(LHCb::Particle::HasBremAdded,0.) == 1;
    
    //brem info from the particle (from creation)
    //filltuple &= tuple->column( prefix+"_HasBremAdded"    , P->info(LHCb::Particle::HasBremAdded,0.)==1) ;
    //brem info from rerunning the BremAdder, looking at the photons
    //This is not correct when running on an mDST - you look at a subset of the total photons in the event.
    //filltuple &= tuple->column( prefix+"_BremP"           , brem.momentum());
    //filltuple &= tuple->column( prefix+"_BremOrigin"      , brem.referencePoint());
    //if(msgLevel(MSG::INFO)) info() << "PV size: Sneha Debug 3" << endmsg;  
    const LHCb::ProtoParticle * proto = P->proto();
    const SmartRefVector< LHCb::CaloHypo > calohypos = proto->calo();
   
    calosize = calohypos.size();
    //if(msgLevel(MSG::INFO)) info() << "PV size: Sneha Debug 4" << endmsg;  
    if (calosize>0) {
      BOOST_FOREACH(const SmartRef<LHCb::CaloHypo> calohypo, calohypos) {
        
        if ( calohypo->hypothesis() == LHCb::CaloHypo::Electron or 
             calohypo->hypothesis() == LHCb::CaloHypo::Positron or 
             calohypo->hypothesis() == LHCb::CaloHypo::ElectronSeed or  
             calohypo->hypothesis() == LHCb::CaloHypo::PositronSeed or 
//              calohypo->hypothesis() == LHCb::CaloHypo::BremmstrahlungPhoton or 
             calohypo->hypothesis() == LHCb::CaloHypo::EmCharged ) {
          
          if (msgLevel(MSG::DEBUG))
            debug() << calohypo << " hyp " << calohypo->hypothesis() 
                    << " lh " << calohypo->lh() 
                    << " x " << calohypo->position()->x() 
                    << " y " << calohypo->position()->y()  
                    << " z " << calohypo->position()->z() << endmsg ;
                    
          if (lh < calohypo->lh()) {
                    
            calox = calohypo->position()->x();
            caloy = calohypo->position()->y();
            caloz = calohypo->position()->z();
            lh = calohypo->lh();
            hyp = calohypo->hypothesis();
            ecalregion = EcalRegionFinder(calox,caloy,caloz);
          }
        }
        
      }
      //  if(msgLevel(MSG::INFO)) info() << "PV size: Sneha Debug 5" << endmsg;      
}

  
  }
  else
  {
    // if(msgLevel(MSG::INFO)) info() << "PV size: Sneha Debug 6" << endmsg;  
    return StatusCode::FAILURE;
  }
  
  test &= tuple->column( prefix+"_BremMultiplicity", bremmulti );
  test &= tuple->column( prefix+"_BremP", bremp );
  test &= tuple->column( prefix+"_HasBremAdded", bremadded );
  test &= tuple->column( prefix+"_CaloRegion", ecalregion );
//   test &= tuple->column( prefix+"_CaloSize", calosize );
  test &= tuple->column( prefix+"_CaloHyp", hyp );
  test &= tuple->column( prefix+"_CaloX", calox );
  test &= tuple->column( prefix+"_CaloY", caloy );
  test &= tuple->column( prefix+"_CaloZ", caloz );
  test &= tuple->column( prefix+"_CaloLH", lh );
  //if(msgLevel(MSG::INFO)) info() << "PV size: Sneha Debug 7" << endmsg;  
  return StatusCode(test);
}

StatusCode TupleToolPIDCalib::fillElectronTagProbe( const LHCb::Particle* P,
                                                    const std::string& head,
                                                    Tuples::Tuple& tuple )
{
  const std::string prefix=fullName(head);
  bool test = true;

  if (P) {

    // just return if the particle isn't an electron
    if ( abs(P->particleID().pid()) != 11 ) return StatusCode::SUCCESS;

    // get the tag-and-probe cut variables
    float p = P->p();
    float pt = P->pt();

    float ip, ipchi2 = 0.;
    StatusCode sc = getIP(P, ip, ipchi2);
    if (sc.isFailure()) return StatusCode::FAILURE;

    bool probe = true;
    probe &= (p>m_electronProbeP);
    probe &= (pt>m_electronProbePt);
    probe &= (ipchi2>m_electronProbeIpChi2);

    bool tag = false;
    float dlle(-999.);

    const LHCb::ProtoParticle* proto = P->proto();
    if ( proto )
    {
      dlle = proto->info(LHCb::ProtoParticle::CombDLLe,-1000.);

      tag = true;
      tag &= (p>m_electronTagP);
      tag &= (pt>m_electronTagPt);
      tag &= (ipchi2>m_electronTagIpChi2);
      tag &= (dlle>m_electronTagDLLe);
    }

    if (msgLevel(MSG::DEBUG))
      debug() << P << " p " << p << " pt " << pt << " ipchi2 " << ipchi2  << " dlle " << dlle
              << " tag " << tag << " probe " << probe << endmsg ;

    //test &= tuple->column(prefix+"_hasMuonTagProbe", hasMuonTagProbe);
    test &= tuple->column(prefix+"_Probe", probe);
    test &= tuple->column(prefix+"_Tag", tag);
    if (isVerbose())
    {
      // no need to include p, pt or muonInAcc, as these are filled
      // by other methods
      test &= tuple->column(prefix+"_IP_PV", ip);
      test &= tuple->column(prefix+"_IPCHI2_PV", ipchi2);
    }
  }
  else {
    return StatusCode::FAILURE;
  }
  return StatusCode(test);
}





