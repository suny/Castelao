/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id:$
// ============================================================================
#ifndef COPYANDMATCHCOMBINATION_H
#define COPYANDMATCHCOMBINATION_H 1
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/IAlgContextSvc.h"
// ============================================================================
// DaVinciKernel
// ============================================================================
#include "Kernel/DaVinciAlgorithm.h"
#include "Kernel/IDecodeSimpleDecayString.h"
#include "Kernel/GetDecay.h"
#include "Kernel/GetParticlesForDecay.h"
#include "Kernel/IPlotTool.h"
#include "Kernel/ISetInputParticles.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/PhysTypes.h"
#include "LoKi/ATypes.h"
#include "LoKi/IHybridFactory.h"
// ============================================================================
// TeslaTools
// ============================================================================
#include "TeslaTools/ITeslaMatcher.h"
// ============================================================================

class CopyAndMatchCombination
  : public extends2<DaVinciAlgorithm,ISetInputParticles,IIncidentListener>
{
  // ==========================================================================
  // the friend factory, needed for instantiation
  //friend class AlgFactory<CopyAndMatchCombination> ;
  // ==========================================================================
public:
  CopyAndMatchCombination (
  const std::string& name ,   // the algorithm instance name
  ISvcLocator*       pSvc ) ; // the service locator
  /// virtual and protected destrcutor

  // ==========================================================================
  /// the standard initialization of the algorithm
  StatusCode initialize () override;                 // standard initialization
  /// the standard execution      of the algorithm
  StatusCode execute    () override;                 //      standard execution
  /// the standard finalization of the algorithm
  StatusCode finalize   () override;                 //  standard  finalization
  // ==========================================================================
  //
  void handle(const Incident&) override;

  StatusCode setInput ( const LHCb::Particle::ConstVector& input ) override;

private:
  std::string m_motherName;
  int         m_motherId;
  bool        m_charConj;
  LHCb::Particle::ConstVector m_inputParticles;
  bool m_useInputParticles;
  bool m_downstream; 

  std::string m_teslaMatcherName;
  ITeslaMatcher* m_matcher;

  std::map <std::string, std::string> m_matching_locations;
  std::map <int, std::string>         m_parsed_locations;

  virtual StatusCode initTESlocations();
  virtual StatusCode matchDaughters(LHCb::Particle* p);

};
#endif // COPYANDMATCHCOMBINATION_H
