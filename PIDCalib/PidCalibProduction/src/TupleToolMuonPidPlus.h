/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id:  $
#ifndef TUPLETOOLMUONPIDPLUS_H
#define TUPLETOOLMUONPIDPLUS_H 1
#define MAXMUSTATIONS 5
// Include files
// from Gaudi
#include <map>
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Kernel/IParticleTupleTool.h"            // Interface

class IMuonIDTool;
namespace TMVA {
  class Reader;
}


/** @class TupleToolMuonPidPlus TupleToolMuonPidPlus.h
 *
 *  @author Giacomo Graziani
 *  @date   2015-12-18
 *
 */
class TupleToolMuonPidPlus : public TupleToolBase, virtual public IParticleTupleTool {
public:
  /// Standard constructor
  TupleToolMuonPidPlus( const std::string& type,
                const std::string& name,
                const IInterface* parent);

  virtual ~TupleToolMuonPidPlus(){}; ///< Destructor
  virtual StatusCode initialize() override;
  virtual StatusCode fill( const LHCb::Particle*, const LHCb::Particle*
                           , const std::string&, Tuples::Tuple& ) override;
private:
  IMuonIDTool* m_newtool;

  void dumpInput();
  void initTMVA();
  float CombDLL2BDT(double x);


  // variables for BDT
  TMVA::Reader *m_TMVAreader;
  float m_fP;
  float m_fPt;
  float m_fnTracks;
  float m_fMuMatch[MAXMUSTATIONS];
  float m_fMuSigma[MAXMUSTATIONS];
  float m_fMuIso[MAXMUSTATIONS];
  float m_fMuTime[MAXMUSTATIONS];
  float m_fChi2Cag;
  
  float m_fTrChi2Ndof;
  float m_fGhostProb;
  float m_fTrMatchChi2;
  float m_fTlik;
  float m_fTrFitVeloChi2Ndof;
  float m_fTrFitTChi2Ndof;
  float m_fUsedRich1Gas;
  float m_fUsedRich2Gas;
  float m_fAboveKThr;
  float m_fRichDLLmu;
  float m_fRichDLLk;
  float m_fVeloCharge;
  float m_fEcalE;
  float m_fHcalE; 
  //options
  std::string m_MuonIDPlusToolName;
  bool m_runOnGrid;
  
};
#endif // TUPLETOOLMUONPIDPLUS_H
