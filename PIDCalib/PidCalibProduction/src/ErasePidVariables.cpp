/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "ErasePidVariables.h"

//-----------------------------------------------------------------------------
// Implementation file for class : ErasePidVariables
//
// 2015-06-15 : Lucio Anderlini        
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( ErasePidVariables )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
ErasePidVariables::ErasePidVariables( const std::string& name,
    ISvcLocator* pSvcLocator)
: DaVinciTupleAlgorithm ( name , pSvcLocator )
  , m_protoPath ( "/Event/Rec/ProtoP/Charged" )
  , m_eraseRich ( false )
  , m_eraseMuon ( false )
  , m_eraseComb ( false )
{
  declareProperty ( "ProtoParticleLocation", 
                    m_protoPath = LHCb::ProtoParticleLocation::Charged );
  declareProperty ( "EraseRichInfo", m_eraseRich );
  declareProperty ( "EraseMuonInfo", m_eraseMuon );
  declareProperty ( "EraseCombInfo", m_eraseComb );

}
//=============================================================================
// Destructor
//=============================================================================
ErasePidVariables::~ErasePidVariables() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode ErasePidVariables::initialize() {
  StatusCode sc = DaVinciTupleAlgorithm::initialize(); 
  if ( sc.isFailure() ) return sc;

  if (msgLevel(MSG::DEBUG)) debug() << "==> Initialize" << endmsg;

  return StatusCode::SUCCESS;
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode ErasePidVariables::execute() 
{
  if (msgLevel(MSG::DEBUG)) debug() << "==> Execute" << endmsg;
  StatusCode sc = StatusCode::SUCCESS ;

  LHCb::ProtoParticles *protos=getIfExists<LHCb::ProtoParticles>(m_protoPath);
  if (protos == NULL)
  {
    setFilterPassed ( false );
    return StatusCode::SUCCESS;
  }

  for ( LHCb::ProtoParticle * proto : *protos )
  {
    if (proto == NULL) continue;
    if (m_eraseComb)
    {
      proto->eraseInfo ( LHCb::ProtoParticle::CombDLLe ) ;
      proto->eraseInfo ( LHCb::ProtoParticle::CombDLLmu );
      proto->eraseInfo ( LHCb::ProtoParticle::CombDLLpi );
      proto->eraseInfo ( LHCb::ProtoParticle::CombDLLk ) ;
      proto->eraseInfo ( LHCb::ProtoParticle::CombDLLp ) ;
    }
    if (m_eraseRich)
    {
      proto->eraseInfo ( LHCb::ProtoParticle::RichDLLe ) ;
      proto->eraseInfo ( LHCb::ProtoParticle::RichDLLmu );
      proto->eraseInfo ( LHCb::ProtoParticle::RichDLLpi );
      proto->eraseInfo ( LHCb::ProtoParticle::RichDLLk ) ;
      proto->eraseInfo ( LHCb::ProtoParticle::RichDLLp ) ;
    }
    if (m_eraseMuon)
    {
      proto->eraseInfo ( LHCb::ProtoParticle::MuonMuLL ) ;
      proto->eraseInfo ( LHCb::ProtoParticle::MuonBkgLL );
    }

    proto->eraseInfo ( LHCb::ProtoParticle::VeloCharge ) ;
    proto->addInfo ( LHCb::ProtoParticle::VeloCharge, 0);

    if (proto->track() == NULL)
      error() << "ProtoParticle has no track" << endmsg;

    const_cast<LHCb::Track*>(proto->track())->setLikelihood(0);
  }

  setFilterPassed(true);   // Mandatory. Set to true if event is accepted.
  return StatusCode::SUCCESS;
}


//=============================================================================
//  Finalize
//=============================================================================
StatusCode ErasePidVariables::finalize() {

  if (msgLevel(MSG::DEBUG)) debug() << "==> Finalize" << endmsg;

  return DaVinciTupleAlgorithm::finalize(); 
} 

//=============================================================================
