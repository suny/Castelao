###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import DaVinci

from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *

from Configurables import LoKi__Hybrid__EvtTupleTool as LokiEventTool
from Configurables import LoKi__Hybrid__TupleTool as LokiTool

tuple = DecayTreeTuple("Tuple")
sample = "DSt_KM"
tuple.Inputs = ["Phys/{}/Particles".format(sample)]
tuple.RootInTES = "/Event/PID"
tuple.Decay  =  "[D*(2010)+ -> ^(D0 -> ^K- pi+) pi+]CC"
branches = tuple.addBranches ( { 
        "Dst"     : "^([D*(2010)+ -> (D0 -> K- pi+) pi+]CC)" 
        , "Dz"    :  "[D*(2010)+ -> ^(D0 -> K- pi+) pi+]CC" 
        , "probe" :  "[D*(2010)+ -> (D0 -> ^K- pi+) pi+]CC"
      })

#matchingLocation = {"K+": "/Event/PID/Phys/Match"+sample+"/Particles"}

#b = branches["probe"]
#matcher = b.addTupleTool ( "TupleToolTwoParticleMatching/Matcher" )
#matcher.ToolList = ["TupleToolKinematic"]
#matcher.MatchLocations = matchingLocation
#
wBranch = tuple.addTupleTool ( "LoKi::Hybrid::TupleTool/LokiWprobe" )
wBranch.Variables = {'momentum' : "P"}


#matcher.Prefix = ""; matcher.Suffix = "_Brunel"


#m_tuple = DecayTreeTuple("TupleMatch")
#sample = "DSt_KM"
#m_tuple.Inputs = ["Phys/Match{}/Particles".format(sample)]
#m_tuple.RootInTES = "/Event/PID"
#m_tuple.Decay  =  "[D*(2010)+ -> ^(D0 -> ^K- pi+) pi+]CC"
#branches = m_tuple.addBranches ( { 
#        "Dst"     : "^([D*(2010)+ -> (D0 -> K- pi+) pi+]CC)" 
#        , "Dz"    :  "[D*(2010)+ -> ^(D0 -> K- pi+) pi+]CC" 
#        , "probe" :  "[D*(2010)+ -> (D0 -> ^K- pi+) pi+]CC"
#      })
#
#m_tuple.addBranches ( {
#        "Dst"     : "^([D*(2010)+ -> (D0 -> K- pi+) pi+]CC)" 
#        , "Dz"    :  "[D*(2010)+ -> ^(D0 -> K- pi+) pi+]CC" 
#        , "probe" :  "[D*(2010)+ -> (D0 -> ^K- pi+) pi+]CC"
#      })
#

DaVinci().appendToMainSequence([tuple,# m_tuple,
 ])

DaVinci().Input = ["00000.PID.mdst"]
DaVinci().TupleFile  = 'PID_modesL.root'
DaVinci().DataType   = '2015'
DaVinci().InputType  = 'MDST'
DaVinci().Simulation = False
DaVinci().Lumi       = True
DaVinci().EvtMax     = -1
DaVinci().PrintFreq  = 1
