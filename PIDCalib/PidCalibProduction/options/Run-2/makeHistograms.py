###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from TeslaTools.TeslaMonitor import TeslaH1, TeslaH2, TeslaH3
from Configurables import DaVinci
from PidCalibProduction import StandardOfflineRequirements as StdCut

cut         = StdCut.TrackGhostProb
DstCut      = StdCut.DstCut
PosId       = StdCut.PosId
NegId       = StdCut.NegId
DsPhiVetos  = StdCut.DsPhiVetos

dv          = DaVinci()


                


## Format:  TurboLine: Particle or decaydescriptor: LoKi variable[: other vars]

dv.MoniSequence =  [

    # DSt vs Dz Pos
    TeslaH2 ( "Hlt2PIDD02KPiTagTurboCalib:D*(2010)+:CHILD(M,1):M-CHILD(M,1)",
              ";m(K^{-}#pi^{+}) [MeV/c^{2}]; m(D^{0}#pi^{+}) - m(K^{-}#pi^{+}) [MeV/c^{2}]",
              300, 1825, 1910, 300, 141, 153, DstCut + PosId,
              histname = "DstDz_Pos").getAlgorithm()
    # DSt vs Dz Neg
    , TeslaH2 ( "Hlt2PIDD02KPiTagTurboCalib:D*(2010)+:CHILD(M,1):M-CHILD(M,1)",
              ";m(K^{-}#pi^{+}) [MeV/c^{2}]; m(D^{0}#pi^{+}) - m(K^{-}#pi^{+}) [MeV/c^{2}]",
              300, 1825, 1910, 300, 141, 153, DstCut + NegId, 
              histname = "DstDz_Neg").getAlgorithm()

    # DSt vs Dz (4 body) Pos  -- Contact: Federico.Redi@cern.ch
    , TeslaH2 ( "Hlt2PIDD02KPiPiPiTagTurboCalib:D*(2010)+:CHILD(M,1):M-CHILD(M,1)",
              ";m(K^{-}#pi^{+}#pi^{-}#pi^{+}) [MeV/c^{2}]"+
              "; m(D^{0}#pi^{+}) - m(K^{-}#pi^{+}) [MeV/c^{2}]",
              300, 1800, 1920, 300, 139, 160, cut + PosId,
              histname = "DstDz_K3Pi_Pos").getAlgorithm()
    # DSt vs Dz (4 body) Neg  -- Contact: Federico.Redi@cern.ch
    , TeslaH2 ( "Hlt2PIDD02KPiPiPiTagTurboCalib:D*(2010)+:CHILD(M,1):M-CHILD(M,1)",
              ";m(K^{-}#pi^{+}#pi^{-}#pi^{+}) [MeV/c^{2}]"+
              "; m(D^{0}#pi^{+}) - m(K^{-}#pi^{+}) [MeV/c^{2}]",
              300, 1800, 1920, 300, 139, 160, cut + NegId, 
              histname = "DstDz_K3Pi_Neg").getAlgorithm()
    
    # Jpsi
       # Negative Tag (positive probe)
    , TeslaH1 ( "Hlt2PIDDetJPsiMuMuNegTaggedTurboCalib:J/psi(1S):M" ,
                ";m(#mu#mu) [MeV/c^{2}]; Candidates",
              1000, 3096 - 110, 3096 + 110,cut, histname = "Jpsi_Pos").getAlgorithm()
       # Positive Tag (negative probe)
    , TeslaH1 ( "Hlt2PIDDetJPsiMuMuPosTaggedTurboCalib:J/psi(1S):M" ,
                ";m(#mu#mu) [MeV/c^{2}]; Candidates",
              1000, 3096 - 110, 3096 + 110,cut, histname = "Jpsi_Neg").getAlgorithm()

    # Lambda0
       # Low PT bin
    , TeslaH1 ( "Hlt2PIDLambda2PPiLLTurboCalib:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
              1000, 1115 - 15, 1115 + 15,cut+PosId, histname = "L0_Pos").getAlgorithm()
       # High PT bin
    , TeslaH1 ( "Hlt2PIDLambda2PPiLLhighPTTurboCalib:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
              1000, 1115 - 15, 1115 + 15,cut+PosId, histname = "L0HPT_Pos").getAlgorithm()
       # Highest PT bin
    , TeslaH1 ( "Hlt2PIDLambda2PPiLLveryhighPTTurboCalib:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
              1000, 1115 - 15, 1115 + 15,cut+PosId, histname = "L0VHPT_Pos").getAlgorithm()

    # Lambda~0
       # Low PT bin
    , TeslaH1 ( "Hlt2PIDLambda2PPiLLTurboCalib:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
              1000, 1115 - 15, 1115 + 15,cut+NegId, histname = "L0_Neg").getAlgorithm()
       # High PT bin
    , TeslaH1 ( "Hlt2PIDLambda2PPiLLhighPTTurboCalib:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
              1000, 1115 - 15, 1115 + 15,cut+NegId, histname = "L0HPT_Neg").getAlgorithm()
       # Highest PT bin
    , TeslaH1 ( "Hlt2PIDLambda2PPiLLveryhighPTTurboCalib:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
              1000, 1115 - 15, 1115 + 15,cut+NegId, histname = "L0VHPT_Neg").getAlgorithm()


    # Phi(1020) -- Contact: Ivan.Polyakov@cern.ch
      # positive tag
    , TeslaH1 ( "Hlt2PIDDetPhiKKPosTaggedTurboCalib:phi(1020):M",
                ";m(K^{+}K^{-}) [MeV/c^{2}]; Candidates",
               40*5, 1000, 1040, cut, histname = "Phi_KM").getAlgorithm() 

      # negative tag
    , TeslaH1 ( "Hlt2PIDDetPhiKKNegTaggedTurboCalib:phi(1020):M",
                ";m(K^{+}K^{-}) [MeV/c^{2}]; Candidates",
               40*5, 1000, 1040, cut, histname = "Phi_KP").getAlgorithm() 

    # D_s+ -> phi(1020) pi+ -- Contact: Ivan.Polyakov@cern.ch               
      #unbiased                                                            
    , TeslaH2 ( "Hlt2PIDDs2PiPhiKKUnbiasedTurboCalib:D_s+:CHILD(M,1):M",  
                ";m(K^{+}K^{-}) [MeV/c^{2}];m(#phi#pi^{+}) [MeV/c^{2}]",  
                39*10, 1000, 1039, 140*5, 1898, 2038, DsPhiVetos, 
                histname = "DsPhi_K_NoTag").getAlgorithm()
      #unbiased  DEBUG                                                          
    , TeslaH2 ( "Hlt2PIDDs2PiPhiKKUnbiasedTurboCalib:D_s+:CHILD(M,1):M",  
                ";m(K^{+}K^{-}) [MeV/c^{2}];m(#phi#pi^{+}) [MeV/c^{2}]",  
                39*10, 1000, 1039, 140*5, 1898, 2038, cut,
                histname = "DsPhi_K_NoTag_NOVETOES").getAlgorithm()

      # positive tag                                                    
    , TeslaH2 ( "Hlt2PIDDs2PiPhiKKPosTaggedTurboCalib:D_s+:CHILD(M,1):M",  
                ";m(K^{+}K^{-}) [MeV/c^{2}];m(#phi#pi^{+}) [MeV/c^{2}]",  
                40*10, 1000, 1039, 140*5, 1898, 2038, cut + PosId, 
                histname = "DsPhi_Neg").getAlgorithm()
                                                                                        
      # negative tag                                                      
    , TeslaH2 ( "Hlt2PIDDs2PiPhiKKNegTaggedTurboCalib:D_s+:CHILD(M,1):M",  
                ";m(K^{+}K^{-}) [MeV/c^{2}];m(#phi#pi^{+}) [MeV/c^{2}]",  
                40*10, 1000, 1039, 140*5, 1898, 2038, cut + NegId, 
                histname = "DsPhi_Pos").getAlgorithm()


    # B -> J/psi (ee) K -- Contact: Dianne and Federico Redi
      # negative tag
    , TeslaH2 ( "Hlt2PIDB2KJPsiEENegTaggedTurboCalib:B+:M:CHILD(M,1)",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; m(#mu#mu) [MeV/c^{2}]",
                20*10, 4300, 5800, 20*10, 2300, 3400,
                cut, histname = "BJpsi_Pos").getAlgorithm()

      # positive tag
    , TeslaH2 ( "Hlt2PIDB2KJPsiEEPosTaggedTurboCalib:B+:M:CHILD(M,1)",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; m(#mu#mu) [MeV/c^{2}]",
                20*10, 4300, 5800, 20*10, 2300, 3400,
                cut, histname = "BJpsi_Neg").getAlgorithm()
    
    
    # KS0 -- Contact: j.devries@cern.ch                                   
      # positive tag                                                      
    , TeslaH1 ( "Hlt2PIDKs2PiPiLLTurboCalib:KS0:M",                       
                ";m(#pi^{+}#pi^{-}) [MeV/c^{2}]; Candidates",                               
                1000, 470, 525, cut, histname = "KS0").getAlgorithm() 

    # B -> J/psi (mumu) K -- Contact: Dianne
    , TeslaH2 ( "Hlt2PIDB2KJPsiMuMuPosTaggedTurboCalib:B+:M:CHILD(M,1)",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; m(#mu#mu) [MeV/c^{2}]",
                90*5, 5000, 5400, 90*5, 3000, 3200,
                cut, histname = "BJpsi_Neg").getAlgorithm()

    , TeslaH2 ( "Hlt2PIDB2KJPsiMuMuNegTaggedTurboCalib:B+:M:CHILD(M,1)",
                ";m(J/#psi K^{+}) [MeV/c^{2}]; m(#mu#mu) [MeV/c^{2}]",
                90*5, 5000, 5400, 90*5, 3000, 3200,
                cut, histname = "BJpsi_Pos").getAlgorithm()
    
#    # Lambda_b -> Lambda_c mu nu   with   Lambda_c -> ^p K pi
    , TeslaH1 ( "Hlt2PIDLb2LcMuNuTurboCalib:Lambda_c+:M",
                ";m(pK^{-}#pi^{+}) [MeV/c^{2}]; Candidates",
                870, 2215, 2360, cut, histname = "LbLc" ).getAlgorithm()
                





    ## Monitoring only
    , TeslaH1 ( "Hlt2PIDD02KPiTagTurboCalib:D*(2010)+:CHILD(M,1)",
              ";m(D^{0}#pi^{+}) [MeV/c^{2}]; Candidates",
              1000, 1865 - 60, 1865 + 60, cut).getAlgorithm()
    , TeslaH1 ( "Hlt2PIDD02KPiTagTurboCalib:D*(2010)+:M",
              ";m(K^{-}#pi^{+}) [MeV/c^{2}]; Candidates",
              1000, 1865 + 139 - 60, 1865 + 155 + 60, cut ).getAlgorithm()
    , TeslaH1 ( "Hlt2PIDD02KPiTagTurboCalib:D*(2010)+:M-CHILD(M,1)",
              ";m(D^{0}#pi^{+}) - m(K^{-}#pi^{+}) [MeV/c^{2}]; Candidates",
              1000, 139, 155, cut ).getAlgorithm()

#    , TeslaH1 ( "Hlt2PIDB2KJPsiEENegTaggedTurboCalib:B+:PP_INFO(LHCb.ProtoParticle.HasBremAdded,0)",
#                ";BremAdded",
#                2,-0.5,1.5,
#                cut, histname = "JpsiEEBrem").getAlgorithm()


    ]








