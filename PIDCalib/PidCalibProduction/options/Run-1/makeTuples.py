###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci, MicroDSTWriter
from PhysConf.MicroDST import uDstConf
from PhysSelPython.Wrappers import DataOnDemand
from PhysSelPython.Wrappers import Selection, SelectionSequence
# make a FilterDesktop
from Configurables import FilterDesktop

from DecayTreeTuple import *
from DecayTreeTuple.Configuration import *
from Configurables import LoKi__Hybrid__EvtTupleTool as LokiEventTool
from Configurables import LoKi__Hybrid__TupleTool as LokiTool
from Configurables import TupleToolTwoParticleMatching as MatcherTool

from Configurables import StoreExplorerAlg
from Configurables import ApplySWeights
from PhysConf.MicroDST import uDstConf
## from Configurables import DaVinci
from copy import copy


##################################################
###      C O N F I G U R A T I O N   1     #######
###. . . . . . . . . . . . . . . . . . . . #######
###        Variables by branch type        #######
##################################################
###  List here variables for each TYPE of branch.
### Branches are: "HEAD", the head of the decay chain
###               "INTERMEDIATE", produced and decayed
###               "TRACK", charged basic particle
###               "NEUTRAL", photons or pi0
##################################################

LokiVarsByType = {
  "HEAD" : {
    "ENDVERTEX_CHI2"   : "VFASPF(VCHI2)"
    , "ENDVERTEX_NDOF" : "VFASPF(VDOF)"
    , "IPCHI2" : "BPVIPCHI2()"
    , "IP"     : "BPVIP()"
    ,"BPVLTCHI2" : "BPVLTCHI2()"
    , "BPVDLS"   : "BPVDLS"
    ,"Mass" : "M"
  },
  
  "INTERMEDIATE" : {
    "ENDVERTEX_CHI2"   : "VFASPF(VCHI2)"
    , "ENDVERTEX_NDOF" : "VFASPF(VDOF)"
   , "IPCHI2" : "BPVIPCHI2()"
    , "IP"     : "BPVIP()"
    , "BPVDLS"   : "BPVDLS"
    ,"BPVLTCHI2" : "BPVLTCHI2()"
    ,"Mass" : "M"
  }, 

  "TRACK" : {
    "LoKi_PT"      : "PT"     ## These variables are also saved from the PIDCalib
    , "LoKi_P"     : "P"      ## tuple tool, so they are a waste of disk space
    , "LoKi_ETA"   : "ETA"    ## however the correct thing would be to keep them
    , "Loki_PIDK"  : "PIDK"   ## here and removing them from PIDCalib.
    , "Loki_PIDmu" : "PIDmu"  ##  ... to be done ...
    , "Loki_PIDp"  : "PIDp"   ## 
    , "sWeight"    : "WEIGHT"
    , "Loki_MINIPCHI2": "BPVIPCHI2()"
    , "RichDLLe"      : "PPFUN ( PP_RichDLLe )"
    , "RichDLLpi"     : "PPFUN ( PP_RichDLLpi )"
    , "RichDLLmu"     : "PPFUN ( PP_RichDLLmu )"
    , "RichDLLk"      : "PPFUN ( PP_RichDLLk )"
    , "RichDLLp"      : "PPFUN ( PP_RichDLLp )"
    , "RichDLLbt"     : "PPFUN ( PP_RichDLLbt )"
    , "MuonMuLL"      : "PPFUN ( PP_MuonMuLL )"
    , "MuonBgLL"      : "PPFUN ( PP_MuonBkgLL )"
    , "Charge"        : "switch ( ID > 0, +1, -1 )"
    , "MuonUnbiased"  : "switch ("\
                        "(TIS('L0.*Decision', 'L0TriggerTisTos')) & "\
                        "(TIS('Hlt1(?!ODIN)(?!L0)(?!Lumi)(?!Tell1)(?!MB)(?!NZS)(?!Velo)(?!BeamGas)(?!Incident).*Decision', 'Hlt1TriggerTisTos')) &"\
                        "(TIS('Hlt2(?!Forward)(?!DebugEvent)(?!Express)(?!Lumi)(?!Transparent)(?!PassThrough).*Decision', 'Hlt2TriggerTisTos')) , " \
                        "1,0)"
    , "ElectronUnbiased" : "switch ( "\
                        "(TIS('L0ElectronDecision', 'L0TriggerTisTos')) & "\
                        "(TIS('Hlt1PhysDecision', 'Hlt1TriggerTisTos')) &"\
                        "(TIS('Hlt2PhysDecision', 'Hlt2TriggerTisTos')) , " \
                        "1,0)"
    , "TRCHI2NDOF" :  "TRCHI2DOF"
    , "TRACK_GHOSTPROB" : "TRGHP"
  },
  
  "NEUTRAL" : {
  }
}


EventInfo = {
  "nPVs"              : "RECSUMMARY( LHCb.RecSummary.nPVs              , -9999)"
#  , "nLongTracks"       : "RECSUMMARY( LHCb.RecSummary.nLongTracks       , -9999)"
#  , "nDownstreamTracks" : "RECSUMMARY( LHCb.RecSummary.nDownstreamTracks , -9999)"
#  , "nUpstreamTracks"   : "RECSUMMARY( LHCb.RecSummary.nUpstreamTracks   , -9999)"
#  , "nVeloTracks"       : "RECSUMMARY( LHCb.RecSummary.nVeloTracks       , -9999)"
#  , "nTTracks"          : "RECSUMMARY( LHCb.RecSummary.nTTracks          , -9999)"
#  , "nBackTracks"       : "RECSUMMARY( LHCb.RecSummary.nBackTracks       , -9999)"
  , "nTracks"           : "RECSUMMARY( LHCb.RecSummary.nTracks           , -9999)"
  , "nRich1Hits"        : "RECSUMMARY( LHCb.RecSummary.nRich1Hits        , -9999)"
  , "nRich2Hits"        : "RECSUMMARY( LHCb.RecSummary.nRich2Hits        , -9999)"
#  , "nVeloClusters"     : "RECSUMMARY( LHCb.RecSummary.nVeloClusters     , -9999)"
#  , "nITClusters"       : "RECSUMMARY( LHCb.RecSummary.nITClusters       , -9999)"
#  , "nTTClusters"       : "RECSUMMARY( LHCb.RecSummary.nTTClusters       , -9999)"
#  , "nUTClusters"       : "RECSUMMARY( LHCb.RecSummary.nUTClusters       , -9999)"
#  , "nOTClusters"       : "RECSUMMARY( LHCb.RecSummary.nOTClusters       , -9999)"
#  , "nFTClusters"       : "RECSUMMARY( LHCb.RecSummary.nFTClusters       , -9999)"
  , "nSPDhits"          : "RECSUMMARY( LHCb.RecSummary.nSPDhits          , -9999)"
#  , "nMuonCoordsS0"     : "RECSUMMARY( LHCb.RecSummary.nMuonCoordsS0     , -9999)"
#  , "nMuonCoordsS1"     : "RECSUMMARY( LHCb.RecSummary.nMuonCoordsS1     , -9999)"
#  , "nMuonCoordsS2"     : "RECSUMMARY( LHCb.RecSummary.nMuonCoordsS2     , -9999)"
#  , "nMuonCoordsS3"     : "RECSUMMARY( LHCb.RecSummary.nMuonCoordsS3     , -9999)"
#  , "nMuonCoordsS4"     : "RECSUMMARY( LHCb.RecSummary.nMuonCoordsS4     , -9999)"
  , "nMuonTracks"       : "RECSUMMARY( LHCb.RecSummary.nMuonTracks       , -9999)"
}   


##################################################
###      C O N F I G U R A T I O N   2     #######
###. . . . . . . . . . . . . . . . . . . . #######
###        Variables by branch name        #######
##################################################
###  List here variables for each Branch name
###   The purpose of this configuration is to allow 
###   to set a variable for all "Lambda_c+" as long
###   as all Lambda_c+ will be named "Lc"
##################################################

LokiVarsByName = {

  "Lc" : {'WM_PiKPi': "WM('pi+', 'K-', 'pi+')" # D->Kpipi,
         ,'WM_KPiPi' : "WM('K-', 'pi+', 'pi+')" # D->Kpipi          
         ,'WM_KKPi'  : "WM('K-', 'K+', 'pi+')" # D->KKpi    
  },

  "Dz" : {'WM_PiPi'    : "WM('pi+','pi-')"                                     
           ,'WM_KK'      : "WM('K+','K-')"                                      
           ,'WM_DCS'     : "WM('pi+','K-')"  

  },

  "K"  : {
  },
  
  "mu" : {
     "MuonTOS"  :      "switch((TOS('L0MuonDecision', 'L0TriggerTisTos')) & "\
                        "(TOS('Hlt1(TrackAllL0|TrackMuon|SingleMuonHighPT)Decision', 'Hlt1TriggerTisTos')), "\
                        "1,0)"
 ,"MuonProbe" :     "switch((P>3000) & (PT>800) & (BPVIPCHI2()>10) & (TRCHI2DOF<3), 1,0)"
     ,"MuonTag" :     "switch((P>6000) & (PT>1500) & (BPVIPCHI2()>25) & (TRCHI2DOF<3) & (ISMUON), 1,0)"


  },
  
  "e" : {
    "ElectronTOS" : "switch ( "\
                        "(TOS('L0ElectronDecision', 'L0TriggerTisTos')) & "\
                        "(TOS('Hlt1PhysDecision', 'TriggerTisTos')) &"\
                        "(TOS('Hlt2PhysDecision', 'TriggerTisTos')) , " \
                        "1,0)"
    ,"ElectronProbe" :   "switch((P>3000) & (PT>500) & (BPVIPCHI2()>9) , 1,0)"
    ,"ElectronTag" :     "switch((P>6000) & (PT>1500) & (BPVIPCHI2()>9) & (PIDe>5), 1,0)"    

    }

}

##################################################
###      C O N F I G U R A T I O N   3     #######
###. . . . . . . . . . . . . . . . . . . . #######
###        Filter definition               #######
##################################################
### Filters used to define the various nTuples
##################################################


from Configurables import TupleToolPIDCalib as TTpid
_l0TisTagCrit   = TTpid.getDefaultProperty('MuonTisL0Criterion')
_hlt1TisTagCrit = TTpid.getDefaultProperty('MuonTisHlt1Criterion')
_hlt2TisTagCrit = TTpid.getDefaultProperty('MuonTisHlt2Criterion')

_l0TosTagCrit   = TTpid.getDefaultProperty('MuonTosL0Criterion')
_hlt1TosTagCrit = TTpid.getDefaultProperty('MuonTosHlt1Criterion')
_hlt2TosTagCrit = TTpid.getDefaultProperty('MuonTosHlt2Criterion')

### Service functions
def L0TIS(code):
    return "TIS('{0}', 'L0TriggerTisTos')".format(code)
def L0TOS(code):
    return "TOS('{0}', 'L0TriggerTisTos')".format(code)
def Hlt1TIS(code):
    return "TIS('{0}', 'Hlt1TriggerTisTos')".format(code)
def Hlt1TOS(code):
    return "TOS('{0}', 'Hlt1TriggerTisTos')".format(code)
def Hlt2TIS(code):
    return "TIS('{0}', 'Hlt2TriggerTisTos')".format(code)
def Hlt2TOS(code):
    return "TOS('{0}', 'Hlt2TriggerTisTos')".format(code)

class FilterCut:
  def __init__ (self, cut):
    self.cut = "(" + cut + ")"
  def __add__ (self, cut):
    ret = FilterCut(self.cut)
    if isinstance(cut, str):
      ret.cut += "& ( " + cut + ")"
    else:
      ret.cut += "& ( " + cut.cut + ")"
    return ret
  def printout (self):
    print "self.cut = %s" % self.cut


class filters:
  LoP = FilterCut("NINTREE( ('p+'==ABSID) & (P<40000) )==1")
  HiP = FilterCut("NINTREE( ('p+'==ABSID) & (P>40000) )==1")

  LoP.printout()

  # Require at least one 'MuonUnBiased' daughter
  MuonUnBiased = FilterCut("NINTREE( (ISBASIC) & ({l0}) "
                         "& ({hlt1}) & ({hlt2}) )>0".format(
       l0=L0TIS(_l0TisTagCrit), hlt1=Hlt1TIS(_hlt1TisTagCrit),
       hlt2=Hlt2TIS(_hlt2TisTagCrit)))
 # MuonUnBiased.printout()
  # Require at least one 'MuonUnBiased' granddaughter
  MuonUnBiased_2 = FilterCut(("NINGENERATION( (ISBASIC) & ({l0}) & "
              "({hlt1}) & ({hlt2}), 2 )>0").format(
       l0=L0TIS(_l0TisTagCrit), hlt1=Hlt1TIS(_hlt1TisTagCrit),
       hlt2=Hlt2TIS(_hlt2TisTagCrit)))

  # Require at least one 'Muon TOS-tagged' daughter
  MuonTosTagged = FilterCut(("NINTREE( (ISBASIC) & ({l0}) & "
                              "({hlt1}) & ({hlt2}) )>0").format(
      l0=L0TOS(_l0TosTagCrit), hlt1=Hlt1TOS(_hlt1TosTagCrit),
      hlt2=Hlt2TOS(_hlt2TosTagCrit)))

  # Require at least one 'Muon TOS-tagged' granddaughter
  MuonTosTagged = FilterCut(("NINGENERATION( (ISBASIC) & ({l0}) "
                         "& ({hlt1}) & ({hlt2}), 2 )>0").format(
       l0=L0TOS(_l0TosTagCrit), hlt1=Hlt1TOS(_hlt1TosTagCrit),
       hlt2=Hlt2TOS(_hlt2TosTagCrit)))

  # Require Lambda_b decay is unbiased with respect to proton PID
  Lb2LcMuNu = FilterCut(("( ({l0Tos}) | ({l0Tis}) ) & "
                                  "({hlt1Tos}) & ({hlt2Tos})").format(
                                  l0Tos=L0TOS("L0(Muon|Hadron)Decision"),
                                  l0Tis=L0TIS("L0.*Decision"),
                                  hlt1Tos=Hlt1TOS("Hlt1(TrackAllL0|TrackMuon|SingleMuonHighPT)Decision"),
                                  hlt2Tos=Hlt2TOS("Hlt2(SingleMuon|TopoMu).*Decision")))

 
 



  IncLc2PKPi = FilterCut(("(BPVIPCHI2()<4) & (VFASPF(VCHI2/VDOF)<5) & (BPVLTCHI2()>9) & (ADWM('D_s+',WM('K-','K+','pi+'))>25.*MeV) & (ADWM('D+',WM('K-','K+','pi+'))>25.*MeV) &(ADWM('D*(2010)+',WM('K-','pi+','pi+'))>20.*MeV) & ((WM('K-','pi+','pi+')>1.905*GeV) | (WM('K-','pi+','pi+')<1.80*GeV))  & (INTREE((ABSID=='p+') &(PT>100*MeV) &(TRGHOSTPROB<0.35) &(BPVIPCHI2()>9.)))  & (INTREE((ABSID=='K+') &(PT>400*MeV) &(TRGHOSTPROB<0.35) &(BPVIPCHI2()>9.) &(PROBNNk>0.3))) &(INTREE((ABSID=='pi+')&(PT>400*MeV) &(TRGHOSTPROB<0.35) &(BPVIPCHI2()>9.) &(PROBNNpi>0.2))) & (({LcHlt2Tos}) |(INTREE((ABSID=='p+') &({PHlt2Tis})) ))").format( LcHlt2Tos=Hlt2TOS("Hlt2(CharmHadD2HHH|CharmHadD2HHHDWideMass)Decision"),PHlt2Tis=Hlt2TIS("Hlt2.*Decision")))






  Jpsiee = FilterCut("(BPVIPCHI2()<9.0) & (VFASPF(VCHI2/VDOF)<9) & (abs(MM-CHILD(MM,1)-2182.3)<100) & (inRange(2250,CHILD(MM,1),3600)) & (NINTREE(('e-'==ABSID)&(BPVIPCHI2()>25) )==2)")



  Jpsiee.printout()
  
##################################################
###      C O N F I G U R A T I O N   4     #######
###. . . . . . . . . . . . . . . . . . . . #######
### Configuration of the decay structures  #######
##################################################
### List of all the decays, input stripping line,
###  decay structure, and its branch
### TupleConfig and Branch are tricks to ensure
### all the mandatory entries are set or to
### raise an exception here in the configuration
### in case they are not.
### The keywork "Type" allows to set the Type of 
### the branch in order to inherit the proper set
### of Loki variables as configured in 
### "Configuration1".
### The Name of the branch as defined in the 
### dictionary is used to inherit variables as 
### defined in configuration2.
### Finally, the keyword "isAlso" allows to set
### other inheritance from configuration2.
### For example, the two muons from the J/psi
### are named mup and mum, but you want both to
### inherit from "mu" since they are muons.
##################################################
class TupleConfig:
  def __init__ (self                 
                  , Decay
                  , InputLines
                  , Branches
                  , Calibration
                  , Filter = None
                ):
    self.Decay = Decay; self.Branches = Branches; 
    if isinstance ( InputLines, list ):
      self.InputLines = InputLines
    else:
      self.InputLines = [InputLines]


    self.Calibration = Calibration
    self.Filter = Filter

class Branch:
  def __init__ (self
                , Particle
                , Type
                , isAlso = None
                , LokiVariables = None
               ):
    if LokiVariables == None : LokiVariables = {}
    if isAlso == None        : isAlso        = []

    if   Type == "H" : Type = "HEAD"
    elif Type == "I" : Type = "INTERMEDIATE"
    elif Type == "T" : Type = "TRACK"
    elif Type == "N" : Type = "NEUTRAL"
    self.Particle = Particle; self.LokiVariables = LokiVariables
    self.Type = Type; self.isAlso = isAlso
    

tupleConfiguration = {
  "DSt_Pi" : TupleConfig (
    Decay = "[D*(2010)+ -> ^(D0 -> K- ^pi+) pi+]CC"
    , InputLines = "NoPIDDstarWithD02RSKPiLine"
    , Calibration   = "RSDStCalib"
    , Filter        = None
    , Branches = {
        "Dst"     : Branch("^([D*(2010)+ -> (D0 -> K- pi+) pi+]CC)", Type='H')
        , "Dz"    : Branch( "[D*(2010)+ -> ^(D0 -> K- pi+) pi+]CC",  Type='I')
        #, "K"     : Branch( "[D*(2010)+ -> (D0 -> ^K- pi+) pi+]CC",  Type='T')
        , "probe" : Branch( "[D*(2010)+ -> (D0 -> K- ^pi+) pi+]CC",  Type='T', isAlso = ['pi'])
        #, "pis"   : Branch( "[D*(2010)+ -> (D0 -> K- pi+) ^pi+]CC",  Type='T', isAlso=['pi'])
      }
    ),
  
  "DSt_K" : TupleConfig (
    Decay = "[D*(2010)+ -> ^(D0 -> ^K- pi+) pi+]CC"
    , InputLines = "NoPIDDstarWithD02RSKPiLine"
    , Calibration   = "RSDStCalib"
    , Filter        = None
    , Branches = {
        "Dst"     : Branch("^([D*(2010)+ -> (D0 -> K- pi+) pi+]CC)", Type='H')
        , "Dz"    : Branch( "[D*(2010)+ -> ^(D0 -> K- pi+) pi+]CC",  Type='I')
        , "probe" : Branch( "[D*(2010)+ -> (D0 -> ^K- pi+) pi+]CC",  Type='T', isAlso=['K'])
        #, "pi"    : Branch( "[D*(2010)+ -> (D0 -> K- ^pi+) pi+]CC",  Type='T')
        #, "pis"   : Branch( "[D*(2010)+ -> (D0 -> K- pi+) ^pi+]CC",  Type='T', isAlso=['pi'])
      }
    ),

    "Lam0_P" : TupleConfig (
      Decay =  "[Lambda0 -> ^p+ pi-]CC"
      , InputLines = [ "Lam0LLLine1V0ForPID", 
                           "Lam0LLLine2V0ForPID" ]
      , Calibration   = "Lam0Calib"
      , Filter        = None
      , Branches = {
          "L0"      : Branch("^([Lambda0 -> p+ pi-]CC)", Type='H')
          , "probe" : Branch("[Lambda0 -> ^p+ pi-]CC", Type='T', isAlso=['p'])
          #, "pi" : Branch("[Lambda0 -> p+ ^pi-]CC", Type='T')
        }
      ),

    "Jpsi_MuP" : TupleConfig (
      Decay = "J/psi(1S) -> ^mu+ ^mu-"
      , InputLines = ["MuIDCalib_JpsiFromBNoPIDNoMip"]
      , Calibration   = "JpsiCalib"
#      , Filter        = filters.MuonUnBiased
      , Filter        = None
      , Branches = {
          "Jpsi" : Branch("^(J/psi(1S) -> mu+ mu-)", Type='H')
          , "probe": Branch("J/psi(1S) -> ^mu+ mu-", Type='T', isAlso = ['mu'])
          , "tag"  : Branch("J/psi(1S) -> mu+ ^mu-", Type='T', isAlso = ['mu'])
        }
      ),

    "Jpsi_MuM" : TupleConfig (
      Decay = "J/psi(1S) -> ^mu+ ^mu-"
      , InputLines = ["MuIDCalib_JpsiFromBNoPIDNoMip"]
      , Calibration   = "JpsiCalib"
#      , Filter        = filters.MuonUnBiased
      , Filter        = None
      , Branches = {
          "Jpsi" : Branch("^(J/psi(1S) -> mu+ mu-)", Type='H')
          , "tag"  : Branch("J/psi(1S) -> ^mu+ mu-", Type='T', isAlso = ['mu'])
          , "probe": Branch("J/psi(1S) -> mu+ ^mu-", Type='T', isAlso = ['mu'])
        }
      ),
  
}


################################################################################
################################################################################
####                                                                        ####
####                                                                        ####
####              S T O P   H E R E                                         ####
####                                                                        ####
####  99% of use cases won't require you to modify what is below            ####
####                                                                        ####
################################################################################
################################################################################
                     
def parseConfiguration_Run1(tupleConfig, tesFormat, sTableFile):
  cfg = tupleConfig
  sequences = []

  for sample in cfg:
    ### Define the filter sequence 
    filterSequences = []
    for line in cfg[sample].InputLines:
      location = tesFormat.replace('<line>', line) 
      sequences += [ 
        ApplySWeights ("ApplySW"+line,
         InputTes   = location,
         sTableFile = sTableFile, 
         sTableDir  = cfg[sample].Calibration,
         sTableName = "sTableSignal", 
#         RootInTES  = "/Event/PID"
        )
       ]

      if cfg[sample].Filter == None:
        filterSequences += [SelectionSequence("fs_" + sample + line, 
                    TopSelection = DataOnDemand(location)
          )]
      else:
        filterSequences += [SelectionSequence("fs_" + sample + line,
          TopSelection = Selection("sel_" + sample + line,
            RequiredSelections = [DataOnDemand(location)],
            Algorithm = FilterDesktop("alg_" + sample + line,
              Code = cfg[sample].Filter.cut
              )
            )
          )]


    ### Define tuple sequence
    tuple = DecayTreeTuple(sample + "Tuple")
#    tuple.RootInTES = "/Event/PID"
    tuple.Inputs = [seq.outputLocation() for seq in filterSequences]
    tuple.Decay  = cfg[sample].Decay
    tuple.ToolList = [ "TupleToolEventInfo" ]
    eventTool = tuple.addTupleTool("LoKi::Hybrid::EvtTupleTool/LoKiEvent")
    eventTool.VOID_Variables = EventInfo
    eventTool.Preambulo = [
      "from LoKiTracks.decorators import *",
      "from LoKiCore.functions import *"
    ]
    tuple.addTool( eventTool )

    for branchName in cfg[sample].Branches:
      b = tuple.addBranches({branchName : cfg[sample].Branches[branchName].Particle})
      b = b[branchName]

      lokitool = b.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_"+branchName)
      vardict = copy(LokiVarsByType[ cfg[sample].Branches[branchName].Type ])
      pidcalibtool = b.addTupleTool ( "TupleToolPIDCalib/PIDCalibTool_"+branchName )
      for partName in [branchName] + cfg[sample].Branches[branchName].isAlso:
        if partName in LokiVarsByName:
          vardict.update ( LokiVarsByName[partName] )
        if partName == 'e':
          pidcalibtool.FillBremInfo = True

      vardict.update (cfg[sample].Branches[branchName].LokiVariables)
      lokitool.Variables        = vardict


    sequences += filterSequences + [tuple]

  return sequences

                    


## Entry point for ganga configuration
def configurePIDCalibTupleProduction(
      DataType
      , TupleFile
      , Simulation 
      , Lumi
      , Stream
      , InputType = 'MDST'
      , EvtMax    = -1
      , tesFormat = "/Event/<stream>/Phys/<line>/Particles"
      , sTableFile = None
      , protoRecalibrationSequences = None
   ):
#
  from Configurables import MessageSvc
  MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"
#  MessageSvc().OutputLevel = DEBUG

  if protoRecalibrationSequences == None: protoRecalibrationSequences = []
#
  dv = DaVinci()
  dv.DataType   = DataType
  dv.InputType  = InputType
  dv.EvtMax = EvtMax
  dv.TupleFile = TupleFile
#   
#  if InputType == 'MDST':
#    rootInTes = "/Event/"+Stream
#    uDstConf ( rootInTes )


  dv.Simulation = Simulation
  dv.Lumi       = Lumi

  tesFormat = tesFormat.replace('<stream>', Stream)
  

  configuredAlgorithms = parseConfiguration_Run1(tupleConfiguration, 
                                                 tesFormat, sTableFile) 

  dv.appendToMainSequence ( protoRecalibrationSequences + configuredAlgorithms )


#  if InputType == 'MDST':
#    for alg in  dv.UserAlgorithms:
#      alg.RootInTES = rootInTes

#below for local test

#TupleFile = 'PID_modesL.root'
#DataType = '2012'
#InputType = 'MDST'
#Simulation = False
#Lumi = True
#EvtMax = -1
#Stream = 'PID'
#tesFormat = "/Event/<stream>/Phys/<line>/Particles"
#dv = DaVinci()
#dv.DataType   = DataType
#dv.InputType  = InputType
#dv.EvtMax = EvtMax
#dv.TupleFile = TupleFile

#if InputType == 'MDST':
#    rootInTes = "/Event/Strip"
##    uDstConf ( rootInTes )


#dv.Simulation = Simulation
#dv.Lumi       = Lumi

#tesFormat = tesFormat.replace('<stream>', Stream)
#dv.UserAlgorithms = parseConfiguration(tupleConfiguration, tesFormat)


