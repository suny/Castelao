###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import DaVinci
from TeslaTools.TeslaMonitor import TeslaH1, TeslaH2, TeslaH3

def Brunellize ( TeslaHistogram ):
  inputs = []
  for input in TeslaHistogram.Inputs:
    inputs += [ input.replace("/Turbo/", "/PID/Phys/") ]
  TeslaHistogram.Inputs = inputs

dv = DaVinci()

cut = "MAXTREE ( TRGHP , ISBASIC ) < 0.3"

## Format:  TurboLine: Particle or decaydescriptor: LoKi variable[: other vars]

dv.MoniSequence =  [
    TeslaH1 ( "NoPIDDstarWithD02RSKPiLine:D*(2010)+:CHILD(M,1)",
              ";m(D^{0}#pi^{+}) [MeV/c^{2}]; Candidates",
              1000, 1865 - 60, 1865 + 60, cut).getAlgorithm()
    , TeslaH1 ( "NoPIDDstarWithD02RSKPiLine:D*(2010)+:M",
              ";m(K^{-}#pi^{+}) [MeV/c^{2}]; Candidates",
              1000, 1865 + 139 - 60, 1865 + 155 + 60, cut ).getAlgorithm()
    , TeslaH1 ( "NoPIDDstarWithD02RSKPiLine:D*(2010)+:M-CHILD(M,1)",
              ";m(D^{0}#pi^{+}) - m(K^{-}#pi^{+}) [MeV/c^{2}]; Candidates",
              1000, 139, 155, cut ).getAlgorithm()
    , TeslaH2 ( "NoPIDDstarWithD02RSKPiLine:D*(2010)+:CHILD(M,1):M-CHILD(M,1)",
              ";m(K^{-}#pi^{+}) [MeV/c^{2}]; m(D^{0}#pi^{+}) - m(K^{-}#pi^{+}) [MeV/c^{2}]",
              300, 1865-60, 1865+60, 300, 139., 155. , cut).getAlgorithm()
    , TeslaH1 ( "MuIDCalib_JpsiFromBNoPIDNoMip:J/psi(1S):M" ,
                ";m(#mu#mu) [MeV/c^{2}]; Candidates",
              1000, 3096 - 195, 3096 + 195,cut).getAlgorithm()
    , TeslaH1 ( "Lam0LLLine1V0ForPID:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
              1000, 1100, 1140,cut).getAlgorithm()
    , TeslaH1 ( "Lam0LLLine2V0ForPID:Lambda0:M" ,
                ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
              1000, 1100, 1140,cut).getAlgorithm()
    ]

for alg in dv.MoniSequence:
  Brunellize ( alg )








