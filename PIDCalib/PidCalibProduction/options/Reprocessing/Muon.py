###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import (GaudiSequencer, FakePidVariables,
                           ChargedProtoANNPIDConf ,
                           ChargedProtoParticleAddMuonInfo,
                           ChargedProtoCombineDLLsAlg,
                           MuonIDAlgLite
                          )

def MuonReprocessing (name = None):
  if name == None : name = "MuonReprocess"
  gaudiSequence = GaudiSequencer ( name + "Seq")

  gaudiSequence.Members += [
    FakePidVariables                (name+"fakePidVars", EraseCombInfo = True, EraseMuonInfo = True),
    MuonIDAlgLite                   (name+"AlgLite", DLL_flag = -1),
    ChargedProtoParticleAddMuonInfo (name+"ReMuon"),
    ChargedProtoCombineDLLsAlg      (name+"CProtoPCombDLLNewMuon"),
  ]

  return gaudiSequence



