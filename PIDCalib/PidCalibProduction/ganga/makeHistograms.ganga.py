###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
execfile ( "splash.ganga.include.py" )
import os



class HistoJobs ( object ):
  def __init__ (self):
    self._MagUp15     = None
    self._MagUp15em   = None
    self._MagUp12     = None
    self._MagUp11     = None
    self._MagDown15   = None
    self._MagDown15em = None
    self._MagDown12   = None
    self._MagDown11   = None
    #
    self.EvtMax       = -1
    self.versionDV    = 'v38r1p1'
#    self.OptionsDir   = "../options/"
    self.OptionsDir   = ( os.environ['User_release_area'] 
                          + "/DaVinci_" 
                          + self.versionDV
                          + "/WG/PIDCalib/"
                        )
    self.backend      = Dirac()

  def _createJob ( self , name , comment):
    j = Job()
    j.name    = name
    j.comment = comment
    j.application = DaVinci( version = self.versionDV )
#    j.merger = RootMerger ( files = ['histo.root'], ignorefailed = False )
    j.backend = self.backend
    j.splitter = SplitByFiles ( maxFiles = -1, filesPerJob = 100 )
    j.outputfiles = [ LocalFile ( "histo.root" ) ]

    return j


  def _getQuery (self
      , magPol       = None
      , collVersion  = None
    ):

    col      =  collVersion if collVersion else "12"
    pol      =  magPol if magPol else "MagUp"
    energy   =  {"12":"4000GeV", "11":"3500GeV", "15":"6500GeV"}[col]
    rec      =  {"12":"14", "11":"14", "15":"15"}[col]
    strp     =  {"12":"21", "11":"21r1", "15":"22"}[col]

    return ('/LHCb/Collision{col}/Beam{energy}-VeloClosed-{pol}/Real '
                       'Data/Reco{rec}/Stripping{strp}/90000000/PID.MDST'.format(
       col=col,pol=pol,energy=energy,rec=rec,strp=strp))

  
  def _compileExtraOpts ( self, dataType, inputType, evtMax=None ):
    evtMax = evtMax if evtMax else self.EvtMax
    return """if(1):
      DaVinci().DataType = "{dataType}"
      DaVinci().EvtMax   = {evtMax}
      DaVinci().InputType= "{inputType}"
      DaVinci().HistogramFile = "histo.root"
    """.format (
      dataType=dataType, evtMax=evtMax, inputType=inputType
    )
    
      
  @property  
  def MagUp15em (self):
    if self._MagUp15em:
      return self._MagUp15em

    j = self._createJob ( "PidH15emU", "15emMagUp" )
    j.application.optsfile = self.OptionsDir + "/scriptsR2/makeHistograms.py"
    j.inputdata = BKQuery (
        "/LHCb/Collision15em/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco15em/Turbo01em/95100000/FULLTURBO.DST",
                            dqflag = "All" ).getDataset()
    j.application.extraopts = self._compileExtraOpts ( "2015", "DST" )

    self._MagUp15em = j
    return self._MagUp15em

  
  @property  
  def MagDown15em (self):
    if self._MagDown15em:
      return self._MagDown15em

    j = self._createJob ( "PidH15emD" , "15emMagDown" )
    j.application.optsfile = self.OptionsDir + "/scriptsR2/makeHistograms.py"
    j.inputdata = BKQuery (
        "/LHCb/Collision15em/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15em/Turbo01em/95100000/FULLTURBO.DST",
                            dqflag = "All" ).getDataset()
    j.application.extraopts = self._compileExtraOpts ( "2015", "DST" )

    self._MagDown15em = j
    return self._MagDown15em

  
  @property  
  def MagUp15 (self):
    if self._MagUp15:
      return self._MagUp15

    j = self._createJob ( "PidH15U", "15MagUp" )
    j.application.optsfile = self.OptionsDir + "/scriptsR2/makeHistograms.py"
    j.inputdata = BKQuery (
        "/LHCb/Collision15/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco15a/Turbo01a/95100000/FULLTURBO.DST",
                            ).getDataset()
    j.application.extraopts = self._compileExtraOpts ( "2015", "DST" )

    self._MagUp15 = j
    return self._MagUp15

  
  @property  
  def MagDown15 (self):
    if self._MagDown15:
      return self._MagDown15

    print "Creating job object."
    j = self._createJob ( "PidH15D" , "15MagDown" )
    j.application.optsfile = self.OptionsDir + "/scriptsR2/makeHistograms.py"
    print "Querying Bookkeeping database."
    j.inputdata = BKQuery (
        "/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Turbo01a/95100000/FULLTURBO.DST",
                            ).getDataset()
    j.application.extraopts = self._compileExtraOpts ( "2015", "DST" )

    self._MagDown15 = j
    print "Job ready."
    return self._MagDown15


  @property
  def MagUp12 (self):
    if self._MagUp12:
      return self._MagUp12
    raise Exception("PIDCalib not backported to Run 1, yet. Use the old PIDCalib.")

    j = self._createJob ( "PidH12U", "12MagUp" )
    j.application.optsfile = self.OptionsDir + "/Run-1/makeHistograms.py"
    j.inputdata = BKQuery ( self._getQuery ("MagUp", "12" ) ).getDataset()
    j.application.extraopts = self._compileExtraOpts ( "2012", "DST" )

    self._MagUp12 = j
    return self._MagUp12

  
  @property  
  def MagDown12 (self):
    if self._MagDown12:
      return self._MagDown12
    raise Exception("PIDCalib not backported to Run 1, yet. Use the old PIDCalib.")

    j = self._createJob ( "PidH12D", "12MagDown" )
    j.application.optsfile = self.OptionsDir + "/Run-1/makeHistograms.py"
    j.inputdata = BKQuery ( self._getQuery ("MagDown", "12" ) ).getDataset()
    j.application.extraopts = self._compileExtraOpts ( "2012", "DST" )

    self._MagDown12 = j
    return self._MagDown12


  @property
  def MagUp11 (self):
    if self._MagUp12:
      return self._MagUp12
    raise Exception("PIDCalib not backported to Run 1, yet. Use the old PIDCalib.")

    j = self._createJob ( "PidH11U", "11MagUp")
    j.application.optsfile = self.OptionsDir + "/Run-1/makeHistograms.py"
    j.inputdata = BKQuery ( self._getQuery ("MagUp", "11" ) ).getDataset()
    j.application.extraopts = self._compileExtraOpts ( "2011", "DST" )

    self._MagUp11 = j
    return self._MagUp11

  
  @property  
  def MagDown11 (self):
    if self._MagDown11:
      return self._MagDown11
    raise Exception("PIDCalib not backported to Run 1, yet. Use the old PIDCalib.")

    j = self._createJob ( "PidH11D", "11MagDown" )
    j.application.optsfile = self.OptionsDir + "/Run-1/makeHistograms.py"
    j.inputdata = BKQuery ( self._getQuery ("MagDown", "11" ) ).getDataset()
    j.application.extraopts = self._compileExtraOpts ( "2011", "DST" )

    self._MagDown11 = j
    return self._MagDown11



histJob = HistoJobs ()


print "Reminder: to have all the pdf running, you need to configure ganga: "
print " SetupProject ganga --runtime-project Bender"

print "Preconfigured jobs you can just submit: "
for pidjob in histJob.__dict__:
  if "_Mag" in pidjob:
    print ". histJob." + pidjob[1:] + ".submit()"
print "---------------------"

