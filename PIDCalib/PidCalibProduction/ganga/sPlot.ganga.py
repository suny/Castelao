###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
execfile ( "splash.ganga.include.py" )



class sPlotClass:
  def fromSandbox ( self, inputjobId, key = None ):
    from Production.sPlot import sPlot as sPlotCoreCode
    from Production.sPlot import default_config
    from glob import glob as getFileMatching

    if not isinstance ( inputjobId , list ):
      inputjobId = [inputjobId]
      
    for job in [jobs(id) for id in inputjobId]:
      if not key: key = job.comment



      config = default_config()
      config.monitoring_key = key
      config.outputdir = "../sTables/"
      
      for j in job.subjobs:
        if j.status == 'completed':
          config.inputfiles += [ j.outputdir + "/histo.root" ]

      sPlotCoreCode ( config )

  def fromBookkeeping ( self, bkkquery, key = None, dqflag = None, maxFiles = -1):
    from Production.sPlot import sPlot as sPlotCoreCode
    from Production.sPlot import default_config
    from glob import glob as getFileMatching
    from re import search

    if dqflag == None:
      dqflag = "Ok"

    type = search("Collision\w+",bkkquery).group(0).replace("Collision","")
    pol  = search("Mag\w+",bkkquery).group(0)

    key = type + pol

      

    config = default_config()
    config.monitoring_key = key
    config.outputdir = "../sTables/"
    config.samples['JpsiCalib_Pos']['PREPROCESSING'] = lambda h: h.Rebin(10)
    config.samples['JpsiCalib_Neg']['PREPROCESSING'] = lambda h: h.Rebin(10)
    config.samples['RSDStCalib_Pos']['PREPROCESSING'] = lambda h: h.Rebin2D(10,10)
    config.samples['RSDStCalib_Neg']['PREPROCESSING'] = lambda h: h.Rebin2D(10,10)
    config.samples['Lam0Calib_Pos']['PREPROCESSING'] = lambda h: h.Rebin(10)
    config.samples['Lam0Calib_Neg']['PREPROCESSING'] = lambda h: h.Rebin(10)
    config.samples['Lam0Calib_HPT_Pos']['PREPROCESSING'] = lambda h: h.Rebin(10)
    config.samples['Lam0Calib_HPT_Neg']['PREPROCESSING'] = lambda h: h.Rebin(10)
    config.samples['Lam0Calib_VHPT_Pos']['PREPROCESSING'] = lambda h: h.Rebin(10)
    config.samples['Lam0Calib_VHPT_Neg']['PREPROCESSING'] = lambda h: h.Rebin(10)
    
    config.inputfiles = []

    d = BKQuery ( bkkquery, dqflag = dqflag ).getDataset()
    files = d.getReplicas()
    if len ( d.files ) == 0:
      raise Exception ( "No file found flagged '" + dqflag +"' at " + bkkquery   )

    counter = 0
    for f in files:
      if 'CERN-HIST-EOS' in files[f]:
        filename = files[f]['CERN-HIST-EOS'].replace ( 'srm://srm-eoslhcb.cern.ch:8443/srm/v2/server?SFN=', "root://eoslhcb.cern.ch//" )
        config.inputfiles += [ filename ]
        print "Input file: " + filename
        counter = counter + 1
        if maxFiles >= 0 and counter >= maxFiles:
          break
    if len ( config.inputfiles ) == 0 :
      raise Exception ( "No file found at CERN-HIST-EOS" )

    print "Selected " + str(len ( config.inputfiles ) ) + " files."

    sPlotCoreCode ( config )
    

    




sPlot = sPlotClass ()


print "To lauch the sPlot algorithm: "
print " >> sPlot.fromSandbox ( <job_id> [, <key>])"
print " "
print "Examples: "
print " .  sPlot.fromSandbox ( jobs[-1].id )"
print " .  sPlot.fromBookkeeping ( '/LHCb/Collision15em/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15em/Turbo01em/95100000/DAVINCIHIST', dqflag = 'All' )"

print " .  sPlot.fromBookkeeping ( '/validation/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15d/Turbo01a/95100000/DAVINCIHIST', dqflag = 'All' )"
