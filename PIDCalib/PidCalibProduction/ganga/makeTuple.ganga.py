###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
##                                                                            ##
## Contact: Lucio.Anderlini@cern.ch                                           ##
##                                                                            ##
## Simplified tool to produce PIDCalib nTuples                                ##
##                                                                            ##
## Usage: see the examples below                                              ##
##                                                                            ##
################################################################################

execfile ( "splash.ganga.include.py" )

import ntpath
import os.path

class defaultCalibrationFiles :
  MagUp2015em   = "../sTables/sPlotTables-15emMagUp.root"
  MagDown2015em = "../sTables/sPlotTables-15emMagDown.root"
  MagUp2015     = "../sTables/sPlotTables-15MagUp.root"
  MagDown2015   = "../sTables/sPlotTables-15MagDown.root"
  MagUp2012     = "../sTables/sPlotTables-12MagUp.root"
  MagDown2012   = "../sTables/sPlotTables-12MagDown.root"
  MagUp2011     = "../sTables/sPlotTables-11MagUp.root"
  MagDown2011   = "../sTables/sPlotTables-11MagDown.root"

class PIDCalibJob:
  def __init__(self
               ,  platform         = "x86_64-slc6-gcc48-opt"
               ,  davinciVersion   = "v37r2p3"
               ,  year             = "2012"
               ,  stripVersion     = "21"
               ,  suffix           = "" 
               ,  magPol           = "MagUp"
               ,  backend          = Dirac()
               ,  maxFiles         = -1
               ,  filesPerJob      = 8
               ,  autoresubmit     = False
               ,  simulation       = False
               ,  EvtMax           = -1
               ,  InputType        = None
               ,  bkkQuery         = None
               ,  bkkFlag          = "OK"
               ,  stream           = "PID"
               ,  sTableFile       = None
               ,  optsfile         = None
               ,  recalibSequences = None
              ):

    ## STORES THE VARIABLES IN AN ACCESSIBLE BUT HIDDEN WAY    
    self._platform       = platform      
    self._davinciVersion = davinciVersion
    self._year           = year          
    self._stripVersion   = stripVersion  
    self._suffix         = suffix        
    self._magPol         = magPol        
    self.backend         = backend
    self.splitter        = SplitByFiles(maxFiles = maxFiles, filesPerJob = filesPerJob)
    self._autoresubmit   = autoresubmit  
    self._gangaJob       = None
    self.simulation      = simulation
    self.luminosity      = not simulation
    self.EvtMax          = EvtMax
    self._outputfilename = "PIDCalib.root"
    self.stream          = stream
    if simulation:
      self.stream        = "AllStreams"

    self._inputType = InputType
    if self._inputType == None:
      if self.stream in ['PID', 'LEPTONIC', 'BHADRON', 'CHARM']:
        self._inputType = 'MDST'
      else:
        self._inputType = 'DST'

    self.bkkQuery         = bkkQuery
    self.bkkFlag          = bkkFlag
    self.sTableFile       = sTableFile
    self.optsfile         = optsfile 
    if optsfile == None: self.optsfile = []

    self.recalibSequences = recalibSequences 
    if recalibSequences == None:
      self.recalibSequences = []


  def _getDatasetFromBK(self, query = None):
    if query == None:
      if self.bkkQuery == None:
        query=('LHCb/Collision{col}/Beam{energy}-VeloClosed-{pol}/Real '
                           'Data/Reco{rec}/Stripping{strp}/90000000/PID.MDST'.format(
           col      =   self._collVersion, 
           energy   =   self._beamEnergy, 
           pol      =   self._magPol,
           rec      =   self._recoVersion, 
           strp     =   self._stripVersion
          ))
      else:
        query = self.bkkQuery

    bk = BKQuery(path = query, dqflag = self.bkkFlag)
    print "Getting input data according to BK path {0}".format(bk.path)
    dsAll = bk.getDataset()
    print "Selected " + str(len(dsAll.files) ) + " files."

    return dsAll;
 


  def instanceJob(self, 
                  jobName = None,
                  bkkQuery = None
                 ):

    ## PARSE YEAR OF DATATAKING
    if self._year == "2012":
      self._collVersion = "12"; self._beamEnergy = "4000GeV"   
    elif self._year == "2011":
      self._collVersion = "11"; self._beamEnergy = "3500GeV"   
    elif self._year == "2015":
      self._collVersion = "15"; self._beamEnergy = "6500GeV"   

    ## PARSE STRIPPING TO RECONSTRUCTION
    if self._stripVersion in ["20", "21"]:
      self._recoVersion = "14"
    if self._stripVersion in ["22", "23"]:
      self._recoVersion = "15"
    
    ## ADDS r1 FOR 2011 STRIPPING
    if self._year == "2011":
      self._stripVersion += "r1"
#      self._recoVersion += "a"

    if jobName == None:
      jobName="PID{coll}-{pol}-S{strp}{suf}".format(
         coll=self._collVersion, rec=self._recoVersion, 
         strp=self._stripVersion, suf=self._suffix, pol=self._magPol)



    self._gangaJob = Job(
      name = jobName
      , application = DaVinci(version = self._davinciVersion)
    )

    j = self._gangaJob

    dvSTablePath = ""
    if self.simulation == False:
      if self.sTableFile == None:
        if self._magPol + self._year in defaultCalibrationFiles.__dict__:
          self.sTableFile = defaultCalibrationFiles.__dict__[self._magPol + self._year + self._suffix]
        else:
          raise Exception("Sorry unexpected configuration: for " + 
                          self._magPol + " " + self._year )

      sTableFile = File ( self.sTableFile ) 
      dvSTablePath = sTableFile.name
      if not os.path.isfile(dvSTablePath):
        raise Exception ( "File " + dvSTablePath + " does not exist.")

      if type(self.backend) == type ( Dirac() ):
        j.inputfiles += [ LocalFile ( dvSTablePath ) ]
        dvSTablePath =   ntpath.basename ( sTableFile.name ) 


    j.application.optsfile = self.optsfile 
    if self._year in [ "2015", "2016", "2017", "2018" ]:
      run = 2
      tesFormat = "/Event/<stream>/<line>/Particles"
    else:
      run = 1
      tesFormat = "/Event/<stream>/Phys/<line>/Particles"

    if self.simulation:
      j.application.optsfile += ["../options/Run-{}/makeTuplesMC.py".format(run)]

    if (run == 2):
      j.application.optsfile += [ "../options/Run-2/ConfigureMDSTWriter.py" ]
    else:
      j.application.optsfile += ["../options/Run-{}/makeTuples.py".format(run)]

    j.inputdata = self._getDatasetFromBK(bkkQuery)
    if self.backend == Local():
      j.outputfiles = [LocalFile(self._outputfilename)]
    else:
      j.outputfiles = [DiracFile(self._outputfilename)]

    
    j.application.extraopts="""configurePIDCalibTupleProduction (
        DataType        = "{datatype}"
        , TupleFile     = "{tuplefile}"
        , Simulation    = {simulation}
        , Lumi          = {luminosity}
        , Stream        = "{stream}"
        , InputType     = "{inputtype}"
        , EvtMax        = {evtmax}
        , tesFormat     = "{tesformat}"
        , sTableFile    = "{fileBaseName}"
        , protoRecalibrationSequences = {recalibSequences}
      );
      """.format(
            datatype           =  self._year
            , tuplefile        =  self._outputfilename
            , simulation       =  self.simulation
            , luminosity       =  self.luminosity
            , stream           =  self.stream
            , inputtype        =  self._inputType 
            , evtmax           =  self.EvtMax
            , tesformat        =  tesFormat
            , fileBaseName     =  dvSTablePath
            , recalibSequences =  str(self.recalibSequences)
          )

    j.backend = self.backend
    j.splitter = self.splitter

    self._gangaJob = j
    return j


  def submit   (  self, 
                  jobName  = None,
                  bkkQuery = None
                 ):
    if self._gangaJob == None:
      self._gangaJob = self.instanceJob(jobName = jobName, bkkQuery = bkkQuery)
    
    self._gangaJob . submit()

  

  def unprepare ( self ):
    self._gangaJob = None





################################################################################
###                                                                          ###
### Pre-defined jobs (also useful as examples)                               ###
###                                                                          ###
################################################################################

class PIDCalib:
  up12 = PIDCalibJob(
                    year           = "2012"
                 ,  stripVersion   = "21"
                 ,  magPol         = "MagUp"
                 ,  maxFiles       = -1
                 ,  filesPerJob    = 20
                 ,  simulation     = False
                 ,  EvtMax         = -1
            )

  down12 = PIDCalibJob(
                    year           = "2012"
                 ,  stripVersion   = "21"
                 ,  magPol         = "MagDown"
                 ,  maxFiles       = -1
                 ,  filesPerJob    = 20
                 ,  simulation     = False
                 ,  EvtMax         = -1
            )

  up11 = PIDCalibJob(
                    year           = "2011"
                 ,  stripVersion   = "21"
                 ,  magPol         = "MagUp"
                 ,  maxFiles       = -1
                 ,  filesPerJob    = 20
                 ,  simulation     = False
                 ,  EvtMax         = -1
            )

  down11 = PIDCalibJob(
                    year           = "2011"
                 ,  stripVersion   = "21"
                 ,  magPol         = "MagDown"
                 ,  maxFiles       = -1
                 ,  filesPerJob    = 20
                 ,  simulation     = False
                 ,  EvtMax         = -1
            )


  test = PIDCalibJob(
                    year           = "2011"
                 ,  stripVersion   = "21"
                 ,  magPol         = "MagDown"
                 ,  maxFiles       = 1
                 ,  filesPerJob    = 20
                 ,  simulation     = False
                 ,  EvtMax         = 1000
            )


  validation2015 = PIDCalibJob(
                    year           = "2015"
                 ,  stripVersion   = "22"
                 ,  magPol         = "MagUp"
                 ,  maxFiles       = 1
                 ,  filesPerJob    = 5
                 ,  simulation     = False
                 ,  EvtMax         = 1000
#                 ,  bkkQuery       = "/validation/Collision15em/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco15/Turbo01/95100000/FULL.DST"
#                 ,  bkkQuery       = "/validation/Collision15em/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Turbo01/94000000/FULL.DST"
#                 , bkkQuery        = "/validation/Collision15em/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15b/Turbo01b/95100000/FULLTURBO.DST"
                 , bkkQuery        = 'LHCb/Collision15em/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15em/Turbo01em/95100000/FULLTURBO.DST'
                 ,  bkkFlag        = "All"
                 ,  stream         = "Turbo"
                 ,  backend        = Local()
      )

  down15em = PIDCalibJob(
                    year           = "2015"
                 ,  suffix         = "em"
                 ,  stripVersion   = "22"
                 ,  magPol         = "MagDown"
                 ,  maxFiles       = -1
                 ,  filesPerJob    = 20
                 ,  simulation     = False
                 ,  EvtMax         = -1
                 ,  bkkQuery       = '/LHCb/Collision15em/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15em/Turbo01em/95100000/FULLTURBO.DST'
                 ,  bkkFlag        = "All"
                 ,  stream         = "Turbo"
                 ,  backend        = Dirac()
      )

  up15em = PIDCalibJob(
                    year           = "2015"
                 ,  suffix         = "em"
                 ,  stripVersion   = "22"
                 ,  magPol         = "MagUp"
                 ,  maxFiles       = -1
                 ,  filesPerJob    = 20
                 ,  simulation     = False
                 ,  EvtMax         = -1
                 ,  bkkQuery       = '/LHCb/Collision15em/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco15em/Turbo01em/95100000/FULLTURBO.DST'
                 ,  bkkFlag        = "All"
                 ,  stream         = "Turbo"
                 ,  backend        = Dirac()
      )

  testMuonReprocessing = PIDCalibJob(
                    year          = "2015"
                 ,  stripVersion  = "22"
                 ,  magPol        = "MagDown"
                 ,  maxFiles      = 1
                 ,  filesPerJob   = 5
                 ,  simulation    = False
                 ,  EvtMax        = 1000
                 ,  bkkQuery   ='/validation/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15d/Turbo01a/95100000/FULLTURBO.DST'
                 ,  bkkFlag       = "All"
                 ,  stream        = "Turbo"
                 ,  backend       = Dirac()
                 ,  optsfile      = ["../options/Reprocessing/Muon.py"]
                 ,  recalibSequences = "[MuonReprocessing()]"
      )

  validate25ns = PIDCalibJob(
                    year          = "2015"
                 ,  stripVersion  = "22"
                 ,  magPol        = "MagDown"
                 ,  maxFiles      = 1
                 ,  filesPerJob   = 5
                 ,  simulation    = False
                 ,  EvtMax        = -1
                 ,  bkkQuery   ='/validation/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15d/Turbo01a/95100000/FULLTURBO.DST'
                 ,  bkkFlag       = "All"
                 ,  stream        = "Turbo"
                 ,  backend       = Dirac()
      )

  data2015_25ns_Down = PIDCalibJob(
                    year          = "2015"
                 ,  stripVersion  = "22"
                 ,  magPol        = "MagDown"
                 ,  maxFiles      = -1
                 ,  filesPerJob   = 5
                 ,  simulation    = False
                 ,  EvtMax        = -1
                 ,  bkkQuery      = '/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Turbo01a/95100000/FULLTURBO.DST'
                 ,  bkkFlag       = "All"
                 ,  stream        = "Turbo"
                 ,  backend       = Dirac()
      )

  test25ns = PIDCalibJob ( 
                    year          = "2015"
                 ,  stripVersion  = "22"
                 ,  magPol        = "MagDown"
                 ,  maxFiles      = 1
                 ,  filesPerJob   = 1
                 ,  simulation    = False
                 ,  EvtMax        = 1000
                 ,  bkkQuery      = '/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Turbo01a/95100000/FULLTURBO.DST'
                 ,  bkkFlag       = "All"
                 ,  stream        = "Turbo"
                 ,  backend       = Local()
  )

  data2015_25ns_Up = PIDCalibJob(
                    year          = "2015"
                 ,  stripVersion  = "22"
                 ,  magPol        = "MagUp"
                 ,  maxFiles      = -1
                 ,  filesPerJob   = 5
                 ,  simulation    = False
                 ,  EvtMax        = -1
                 ,  bkkQuery      = '/LHCb/Collision15/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco15a/Turbo01a/95100000/FULLTURBO.DST'
                 ,  bkkFlag       = "All"
                 ,  stream        = "Turbo"
                 ,  backend       = Dirac()
      )

  testMC = PIDCalibJob(
                    year          = "2015"
                 ,  stripVersion  = "22"
                 ,  magPol        = "MagDown"
                 ,  maxFiles      = 1
                 ,  filesPerJob   = 1
                 ,  simulation    = True
                 ,  EvtMax        = 1000
                 ,  bkkQuery   ='/MC/2015/Beam6500GeV-Jun2015-MagDown-Nu1.6-Pythia8/Sim08h/Trig0x40f9014e/Reco15em/Turbo01em/11442012/LDST'
                 ,  bkkFlag       = "All"
                 ,  stream        = "Turbo"
                 ,  backend       = Dirac()
      )
                  
  simulation15emDown = PIDCalibJob(
                    year          = "2015"
                 ,  stripVersion  = "22"
                 ,  magPol        = "MagDown"
                 ,  maxFiles      = -1
                 ,  filesPerJob   = 5
                 ,  simulation    = True
                 ,  EvtMax        = -1
                 ,  bkkQuery   ='/MC/2015/Beam6500GeV-Jun2015-MagDown-Nu1.6-Pythia8/Sim08h/Trig0x40f9014e/Reco15em/Turbo01em/11442012/LDST'
                 ,  bkkFlag       = "All"
                 ,  stream        = "Turbo"
                 ,  backend       = Dirac()
      )

  simulation12Down = PIDCalibJob(
                    year          = "2012"
                 ,  stripVersion  = "21"
                 ,  magPol        = "MagDown"
                 ,  maxFiles      = 1
                 ,  filesPerJob   = 5
                 ,  simulation    = True
                 ,  EvtMax        = 1000
                 ,  bkkQuery   ='/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/27163003/ALLSTREAMS.DST'
                 ,  bkkFlag       = "All"
                 ,  stream        = "AllStreams"
                 ,  backend       = Dirac()
      )
                  
                  



print "Preconfigured jobs you can just submit: "
for pidjob in PIDCalib.__dict__:
  if "__" not in pidjob:
    print ". PIDCalib." + pidjob + ".submit()"
print "---------------------"
