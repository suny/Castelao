###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import sys, os
sys.path.append ( "../python" )
print '\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/'
print '    __    __  __________       ____  ________  ______      ___ __   '
print '   / /   / / / / ____/ /_     / __ \/  _/ __ \/ ____/___ _/ (_) /_  '
print '  / /   / /_/ / /   / __ \   / /_/ // // / / / /   / __ `/ / / __ \ '
print ' / /___/ Integrated/ /_/ / Production// /_/ / /Environment/ / /_/ / '
print '/_____/_/ /_/\____/_.___/  /_/   /___/_____/\____/\__,_/_/_/_.___/  '
print '                                                                    '
#print '     Integrated            Production             Environment       '
#print '                                                                    '
print '\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/'
print '                                                                    '
                                                                   

