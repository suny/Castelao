###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
## Standard Cuts for PIDCalib
## + - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
##  This file contains a set of requirements used around PIDCalib to refine
##  the trigger strategy. They include only cuts to be applied BEFORE filling
##  histograms. These cuts are then written in the sTables to make sure they
##  are ensured again when applying sWeights to candidates.
##  Other selections (as trigger unbias) can be applied at later stage and, 
##  as long as they are uncorrelated with the mass, this is perfectly fine.
################################################################################

TrackGhostProb = "(MAXTREE ( TRGHP , ISBASIC ) < 0.4)"

DstCut = ( TrackGhostProb + 
      "& CHILDCUT ( (abs(WM('pi+','pi-') - PDGMASS) > 25) " + 
                 "& (abs(WM('K+','K-')   - PDGMASS) > 25)   " + 
                 "& (abs(WM('pi+','K-')  - PDGMASS) > 25) , 1 )"  )

OmegaCut = " in_range( 1113., CHILD(M, 1), 1118. ) & (CHILD(BPVIPCHI2(), 1)>6) & (CHILD(PIDp, 1, 1)>4. ) & (CHILD(PIDp, 1, 1)-CHILD(PIDK, 1, 1)>4. )"

PosId = "& (ID > 0)"
NegId = "& (ID < 0)"

def _WMr ( oldId, newId, minMass, maxMass ):
  "Wrong mass for the phi daughters, when coming from a Ds+"
  mass={'p+': 938.272046,'p~-':938.272046, 
        'K+': 493.667,    'K-':493.667, 
        'pi+':139.57018, 'pi-':139.57018}; 

  var = "sqrt( pow( ( E - sqrt(pow({mass},2) + pow(MINTREE(ID == '{oldId}',P),2)) + MINTREE(ID == '{oldId}', E) ),2) - pow(P,2) )"
  return " ( ( {var} < {min} ) | ( {var} > {max} ) )".format (
      min = minMass, max = maxMass,
      var = var.format (newId = newId, oldId = oldId, 
                        mass = mass[newId])
    )


DsPhiVetos =  (TrackGhostProb + 
      " & (  ( (ID > 0) &  {} & {} ) | ( (ID < 0) &  {} & {} ) )" 
        .format ( _WMr ("K+", "pi+", 1830, 1890), _WMr ("K+", "p+" , 2266, 2306),
                  _WMr ("K-", "pi-", 1830, 1890), _WMr ("K-", "p~-", 2266, 2306))
   )



MuonProbe = "(P>3000) & (PT>800) & (BPVIPCHI2()>10) & (TRCHI2DOF<3)"
MuonTag   = "(P>6000) & (PT>1500) & (BPVIPCHI2()>25) & (TRCHI2DOF<3) & (ISMUON)"

Jpsi_Pos  =  "&" + "( INTREE ( (Q > 0) & {probe} ) ) & ( INTREE ( ( Q < 0 ) & {tag} ) ) ".format ( probe = MuonProbe, tag = MuonTag ) 
Jpsi_Neg  =  "&" + "( INTREE ( (Q < 0) & {probe} ) ) & ( INTREE ( ( Q > 0 ) & {tag} ) ) ".format ( probe = MuonProbe, tag = MuonTag ) 
BJpsi_Pos =  "&" + "( INTREE ( (Q > 0) & {probe} ) ) & ( INTREE ( ( Q < 0 ) & {tag} ) ) ".format ( probe = MuonProbe, tag = MuonTag ) 
BJpsi_Neg =  "&" + "( INTREE ( (Q < 0) & {probe} ) ) & ( INTREE ( ( Q > 0 ) & {tag} ) ) ".format ( probe = MuonProbe, tag = MuonTag ) 

MuonProbe_lowpt = "(P>3000) & (BPVIPCHI2()>25) & (TRCHI2DOF<3)"
MuonTag_lowpt   = "(P>6000) & (PT>1500) & (BPVIPCHI2()>45) & (TRCHI2DOF<3) & (ISMUON)"
Jpsi_Pos_lowpt  =  "&" + "( INTREE ( (Q > 0) & {probe} ) ) & ( INTREE ( ( Q < 0 ) & {tag} ) ) ".format ( probe = MuonProbe_lowpt, tag = MuonTag_lowpt )
Jpsi_Neg_lowpt  =  "&" + "( INTREE ( (Q < 0) & {probe} ) ) & ( INTREE ( ( Q > 0 ) & {tag} ) ) ".format ( probe = MuonProbe_lowpt, tag = MuonTag_lowpt )

Jpsi_lowpt = "&" + " & ".join([
      "(BPVVDCHI2 > 225)",
      "(BPVDIRA > 0.9995)",
      "(VFASPF(VCHI2/VDOF)<5)",
        ])

################# Electrons ################################
Jpsiee = "&" + " & ".join([
    "(BPVIPCHI2()<9.0) ",
    "(VFASPF(VCHI2/VDOF)<9) ",
    "(NINTREE(('e-'==ABSID) & (BPVIPCHI2()>25)) == 2)"
  ])

#Dictionary with cuts for B2KJpsiEE in brem (and momentum) categories
B2KJpsiEE_mombremcats = {}
pbins = [0, 11500, 20000, 30000, 50000, 1e9] #roughly isopopulated for B2KJpsiEE
ChargeReq = {'Pos': '(Q > 0)', 'Neg': '(Q < 0)'}
OppCharge = {'Pos': 'Neg', 'Neg': 'Pos'}
eIDCut = "('e-'==ABSID)"
for ProbeCharge in ['Pos', 'Neg']:
    TagCharge = OppCharge[ProbeCharge]
    ChargeCut = ChargeReq[ProbeCharge]
    for Nbrem in [0, 1]:
        BremCut = "(PINFO(LHCb.Particle.HasBremAdded,  0.)=={})".format(Nbrem)
        ProbeCut = "&(INTREE({} & {} & {}))".format(ChargeCut, BremCut, eIDCut)
        Cut = TrackGhostProb + Jpsiee + ProbeCut
        CatName = 'BJpsiK_{}_DTF_PV_Probe{}Brem'.format(ProbeCharge, Nbrem)
        HistName = 'BJpsiEE_Calib{}Brem_{}'.format(Nbrem, ProbeCharge)
        B2KJpsiEE_mombremcats[CatName] = {'Cut': Cut, 'TagCharge': TagCharge, 'ProbeCharge': ProbeCharge, 'HistName': HistName}

        for ibin in range(len(pbins)-1):
            PCut = "((P>{pmin}) & (P<{pmax}))".format(pmin=pbins[ibin], pmax=pbins[ibin+1])
            ProbeCut = "&(INTREE({} & {} & {} & {} ))".format(ChargeCut, BremCut, eIDCut, PCut)
            Cut = TrackGhostProb + Jpsiee + ProbeCut
            #CatName = '{}Probe_{}Brem_pbin{}'.format(ProbeCharge, Nbrem, ibin)
            CatName = 'BJpsiK_{}_DTF_PV_Probe{}Brem_pbin{}'.format(ProbeCharge, Nbrem, ibin)
            HistName = 'BJpsiEE_Calib{}Brem_pbin{}_{}'.format(Nbrem, ibin, ProbeCharge)
            B2KJpsiEE_mombremcats[CatName] = {'Cut': Cut, 'TagCharge': TagCharge, 'ProbeCharge': ProbeCharge, 'HistName': HistName}


#################################################################


DsphiMuonTag   = "(PT>800) & (PROBNNmu>0.8)"

Dsphi_Pos =  "&" + "( INTREE ( ( Q < 0 ) & {tag} ) ) ".format ( tag = DsphiMuonTag )
Dsphi_Neg =  "&" + "( INTREE ( ( Q > 0 ) & {tag} ) ) ".format ( tag = DsphiMuonTag )

Dsphi =  "&" + " & ".join([
  "(VFASPF(VCHI2)<6)",
  "(BPVIPCHI2()<6)",
  "(BPVIP()<0.05)",
  "(inRange(1005, CHILD(MM, 1), 1035))",
  ])



# Cuts for neutral PID

Dst2D0PiCut      = "(M1>1800) & (M1<1930)"
Dst2D0PiRCut     = "((M-M1)>140) & ((M-M1)<154) & (M1>1800) & (M1<1930) & (CHILD(M3,1)>105) & (CHILD(M3,1)<165)"
Dst2D0PiMCut     = "((M-M1)>140) & ((M-M1)<154) & (M1>1800) & (M1<1930)"

D2EtapPiCut       = "(DTF_FUN(M,False, strings(['eta_prime']))>1800) & (DTF_FUN(M,False, strings(['eta_prime']))<2050) & (CHILD(PROBNNpi,2)*(1-CHILD(PROBNNk,2))>0.4) & (M1>930) & (M1<990) & (CHILD(M12,1)>600) & (CHILD(CHILD(Q,1),1)==1)"

Dsst2DsGammaCut   = "((M-M1)>100) & ((M-M1)<260) & (CHILDCUT(ADMASS('D_s+')<12.48,1)) & ((sqrt(pow2((CHILD(PHI,1)-CHILD(PHI,2))-2*3.141593*round(0.5*(CHILD(PHI,1)-CHILD(PHI,2))/3.1416))) + pow2(CHILD(ETA,1)-CHILD(ETA,2)))<0.25) & (CHILD(PIDK-PIDpi,1,3) < 3) & (min(CHILD(PROBNNk,1,1),CHILD(PROBNNk,1,2)) > 0.1) & (min(CHILD(PIDK-PIDpi,1,1),CHILD(PIDK-PIDpi,1,2)) >7) & ((CHILD(PT,1,1)+CHILD(PT,1,2)+CHILD(PT,1,3))>3200) & (CHILD(PT,1)>2000) & (CHILD(VFASPF(VCHI2/VDOF),1) < 6) & (CHILD(BPVDIRA,1) > 0.99995) & (CHILD(BPVIPCHI2(),1) < 1500) & (CHILD(DOCACHI2MAX,1) < 50.0) & (in_range ( 1920.0 , M1 , 2040.0 )) & (CHILD(BPVLTIME(),1)>0.2*picosecond)"

Eta2MuMuGammaCut = "(M>410) & (M<690) & (CHILD(CHILD(Q,1),1)==1)"

B2KstGammaCut    = "(M>4500) & (M<6500) & (BPVIPCHI2()<9) & (max(CHILD(PT,1,1),CHILD(PT,1,2))>1200) & (max(CHILD(PROBNNghost,1,1),CHILD(PROBNNghost,1,2))<0.4) & (CHILD(PIDK,1,1)>5) & ((CHILD(PIDK,1,1)-CHILD(PIDp,1,1))>2)"

Bs2PhiGammaCut   = "(M>4500) & (M<6500) & (BPVIPCHI2()<9) & (max(CHILD(PT,1,1),CHILD(PT,1,2))>1200) & (max(CHILD(PROBNNghost,1,1),CHILD(PROBNNghost,1,2))<0.4) & (min(CHILD(PIDK,1,1),CHILD(PIDK,1,2))>5) & (min(CHILD(PIDK,1,1)-CHILD(PIDp,1,1),CHILD(PIDK,1,2)-CHILD(PIDp,1,2))>2)"

