###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import (GaudiSequencer, ErasePidVariables,
                           ChargedProtoANNPIDConf ,
                           ChargedProtoParticleAddMuonInfo,
                           ChargedProtoCombineDLLsAlg
                          )

class ProbNNRecalibrator:
  def __init__(self, name, ProtoParticleLocation = None):
    self.name = name;
    self.ProtoParticleLocation = ProtoParticleLocation
    if not self.ProtoParticleLocation:
      self.ProtoParticleLocation = "/Event/Rec/ProtoP/Charged"
    self._sequence = None

  def _makeSequence(self):
    name = self.name
    gaudiSequence = GaudiSequencer ( name + "Seq" )

    gaudiSequence.Members += [
      ErasePidVariables               (name+"erasePidVars"),
      ChargedProtoCombineDLLsAlg      (name+"CProtoPCombDLLNewMuon"),
    ]

    for alg in gaudiSequence.Members:
      alg.ProtoParticleLocation = self.ProtoParticleLocation

    self._annpid = ChargedProtoANNPIDConf (name+"RedoProbNN", 
               RecoSequencer = gaudiSequence,
               ProtoParticlesLocation = self.ProtoParticleLocation
             ) 

    return gaudiSequence

  def sequence(self):
    if self._sequence:
      return self._sequence
    else:
      self._sequence = self._makeSequence()
      return self._sequence



from Gaudi.Configuration import ConfigurableUser, allConfigurables
class SWeightsTableFiles(ConfigurableUser):
    __slots__ = {'sTableMagDownFile': '',
                 'sTableMagUpFile': ''}
    _propertyDocDct = {'sTableMagUpFile':
                         'Path to ROOT file with weights for magnet up',
                       'sTableMagDownFile': 
                         'Path to ROOT file with weights for magnet down'}

    def __apply_configuration__(self):
        from Configurables import ApplySWeights
        if not (self.isPropertySet('sTableMagDownFile') 
                and self.isPropertySet('sTableMagUpFile')):
            raise RuntimeError('SWeightsTableFiles: missing settings for MagUp and/or MagDown')

        self.propagateProperties(['sTableMagUpFile', 'sTableMagDownFile'],
                                 [conf for conf in allConfigurables.itervalues()
                                  if isinstance(conf, ApplySWeights)])

#        for conf in allConfigurables.itervalues():
#            if isinstance(conf, ApplySWeights):
#                conf.sTableFile = self.MagUp
#                #conf.sTableFile = self.MagDown

