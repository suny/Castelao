###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
## class TupleConfig
##  used to define the configuration of the calibration sample (tuple+MicroDST)
################################################################################
class TupleConfig:
  def __init__ (self                 
                  , Decay
                  , InputLines
                  , Branches
                  , Calibration
                  , Filter = None
                  , Downstream = False
                ):
    self.Decay = Decay; self.Branches = Branches; 

    if isinstance ( InputLines, list ):
      self.InputLines = InputLines
    else:
      self.InputLines = [InputLines]

    self.Calibration = Calibration
    self.Filter = Filter
    self.Downstream = Downstream 



################################################################################
## class TupleConfig
##  used to define a branch of the decay descriptor with its properties
################################################################################
class Branch:
  def __init__ (self
                , Particle
                , Type
                , isAlso = None
                , LokiVariables = None
               ):
    if LokiVariables == None : LokiVariables = {}
    if isAlso == None        : isAlso        = []

    if   Type == "H" : Type = "HEAD"
    elif Type == "I" : Type = "INTERMEDIATE"
    elif Type == "T" : Type = "TRACK"
    elif Type == "N" : Type = "NEUTRAL"
    self.Particle = Particle; self.LokiVariables = LokiVariables
    self.Type = Type; self.isAlso = isAlso



