###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
##                                                                            ##
##  parseTupleConfig.py   -   Setup DaVinci to write nTuples and MicroDSTs    ##
##                                                                            ##
## Example:                                                                   ##
##                                                                            ##
##  configuredAlgorithms = parseConfiguration(                                ##
##      tupleConfiguration,  ## PIDCalib tuple configuration                  ##
##      tesFormat,           ## TES format "/Event/Turbo/<Line>/Particles"    ##
##      mdstOutputFile,      ## MicroDST filename also used as OUTPUT TES!!!  ##
##      mdstOutputPrefix,    ## MicroDST Prefix, to be changed in PRODUCTION  ##
##      varsByType,          ## dictionary of variables per branch type       ##
##      varsByName,          ## dictionary of variables per branch name       ##
##      eventVariables,      ## dictionary of event variables                 ##
##      writeNullWeightCandidates = True,  ## if False filters out sWeight=0  ## 
##    )                                                                       ##
##                                                                            ##
##                                                                            ##
## Example of minimal tupleConfiguration                                      ##
##                                                                            ##
##  tupleConfiguration = {                                                    ##
##    "DSt_PiP" : TupleConfig (                                               ##
##      Decay = "[D*(2010)+ -> ^(D0 -> K- ^pi+) pi+]CC"                       ##
##      , InputLines    = "Hlt2PIDD02KPiTagTurboCalib"                        ##
##      , Filter        = filters.PositiveID                                  ##
##      , Calibration   = "RSDStCalib_Pos"                                    ##
##      , Branches = {                                                        ##
##          "Dst"     : Branch("^([D*(2010)+ -> (D0 -> K- pi+) pi+]CC)", Type='H')
##          , "Dz"    : Branch( "[D*(2010)+ -> ^(D0 -> K- pi+) pi+]CC",  Type='I')
##          , "K"     : Branch( "[D*(2010)+ -> (D0 -> ^K- pi+) pi+]CC",  Type='T')
##          , "probe" : Branch( "[D*(2010)+ -> (D0 -> K- ^pi+) pi+]CC",  Type='T', isAlso=['pi'])
##          , "pis"   : Branch( "[D*(2010)+ -> (D0 -> K- pi+) ^pi+]CC",  Type='T', isAlso=['pi'])
##        }                                                                   ##
##      ),                                                                    ##
##    }                                                                       ##
##                                                                            ##
## Example of varsByType                                                      ##
##   LokiVarsByType = {                                                       ##
##        "HEAD" : {                                                          ##
##          "ENDVERTEX_CHI2"   : "VFASPF(VCHI2)"                              ##
##          ,"ENDVERTEX_NDOF" : "VFASPF(VDOF)"                                ##
##          ,"Mass" : "M"                                                     ##
##        },                                                                  ##
##                                                                            ##
##        "INTERMEDIATE" : {                                                  ##
##          ,"BPVLTCHI2" : "BPVLTCHI2()"                                      ##
##          ,"Mass" : "M"                                                     ##
##        },                                                                  ##
##                                                                            ##
##        "TRACK" : {                                                         ##
##          , "Loki_MINIPCHI2": "BPVIPCHI2()"                                 ##
##        },                                                                  ##
##                                                                            ##
##        "NEUTRAL" : {                                                       ##
##        }                                                                   ##
##     }                                                                      ##
##                                                                            ##
##                                                                            ##
##                                                                            ##
## Example of varsByName                                                      ##
##   varsByName = {                                                           ##
##                                                                            ##
##     "Lc" : {'WM_PiKPi': "WM('pi+', 'K-', 'pi+')" # D->Kpipi,               ##
##            ,'WM_KPiPi' : "WM('K-', 'pi+', 'pi+')" # D->Kpipi               ##
##            ,'WM_KKPi'  : "WM('K-', 'K+', 'pi+')" # D->KKpi                 ##
##     },                                                                     ##
##                                                                            ##
##     "Dz" : {'WM_PiPi'    : "WM('pi+','pi-')"                               ##
##              ,'WM_KK'      : "WM('K+','K-')"                               ##
##              ,'WM_DCS'     : "WM('pi+','K-')"                              ##
##                                                                            ##
##     },                                                                     ##
##                                                                            ##
##     "K"  : {                                                               ##
##     },                                                                     ##
##   }                                                                        ##
##                                                                            ##
##                                                                            ##
##                                                                            ##
################################################################################

from Gaudi.Configuration import *
from Configurables import DaVinci, MicroDSTWriter
from PhysConf.MicroDST import uDstConf
from PhysSelPython.Wrappers import DataOnDemand
from PhysSelPython.Wrappers import (Selection, 
                                    MergedSelection,
                                    SelectionSequence, 
                                    MultiSelectionSequence)
# make a FilterDesktop
from Configurables import FilterDesktop

from DecayTreeTuple import *
from DecayTreeTuple.Configuration import *
from Configurables import LoKi__Hybrid__EvtTupleTool as LokiEventTool
from Configurables import LoKi__Hybrid__TupleTool as LokiTool
from Configurables import TupleToolTwoParticleMatching as MatcherTool


from Configurables import StoreExplorerAlg
from PidCalibProduction.Configuration import ProbNNRecalibrator as ProbNNcalib
from Configurables import ApplySWeights
from PhysConf.MicroDST import uDstConf
from copy import copy

# for MicroDST writing
from PhysSelPython.Wrappers import SelectionSequence
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import ( SelDSTWriter,stripMicroDSTStreamConf,
                       stripMicroDSTElements, stripCalibMicroDSTStreamConf,
                       stripDSTElements, stripDSTStreamConf)

from Configurables import CopyAndMatchCombination

from TupleConfig import Branch
from TupleConfig import TupleConfig
from Configurables import TupleToolPIDCalib as TTpid




################################################################################
## Configuration of the MicroDST writer                                       ##
################################################################################
def configureMicroDSTwriter( name, prefix, sequences ):
  # Configure the dst writers for the output
  pack = False

  microDSTwriterInput = MultiSelectionSequence ( name , Sequences = sequences )

  # Configuration of MicroDST
  mdstStreamConf = stripMicroDSTStreamConf(pack=pack)
  mdstElements   = stripMicroDSTElements(pack=pack)

  # Configuration of SelDSTWriter
  SelDSTWriterElements = {'default': mdstElements }
  SelDSTWriterConf = {'default': mdstStreamConf}

  dstWriter = SelDSTWriter( "MyDSTWriter",
                            StreamConf = SelDSTWriterConf,
                            MicroDSTElements = SelDSTWriterElements,
                            OutputFileSuffix = prefix,
                            SelectionSequences = [microDSTwriterInput]
                          )

  from Configurables import StoreExplorerAlg
  return [dstWriter.sequence()]


################################################################################
## CONFIGURATION OF THE JOB                                                   ##
################################################################################
def parseConfigurationNeutral(tupleConfig,  # TupleConfig object describing sample
                              tesFormat,    # Input TES with "<line>" placeholder
                              mdstOutputFile,  # MicroDST output extension
                              mdstOutputPrefix,# MicroDST prefix for production
                              varsByType,   # requested variables by type
                              varsByName,   # requested variables by name
                              eventVariables, # event variables
                              writeNullWeightCandidates = True, 
                              reprocessing = False
                              ):
  cfg = tupleConfig
  reviveSequences = [] # mark sequences to unpack std particles
  swSequences     = [] # mark sequences to apply sWeights
  filterSequences = [] # mark sequences to be written in tuples
  matchSequences  = [] # mark sequences to be written in tuples
  tupleSequences  = [] # mark tuple sequences
  dstSequences    = [] # sequences writing (Micro)DST files

  for basicPart in ["Muons", "Pions", "Kaons", "Protons", "Electrons"]:
    location = "Phys/StdAllNoPIDs{s}/Particles".format ( s = basicPart )
    reviveSequences += [SelectionSequence("fs_std" + basicPart , 
                          TopSelection = DataOnDemand(location))]

  for basicPart in ["DownPions", "DownKaons", "DownProtons", "DownElectrons"]:
    location = "Phys/StdNoPIDs{s}/Particles".format ( s = basicPart )
    reviveSequences += [SelectionSequence("fs_std" + basicPart , 
                          TopSelection = DataOnDemand(location))]

  location = "Phys/StdLooseDownMuons/Particles"
  reviveSequences += [SelectionSequence("fs_std" + "DownMuons" , 
                          TopSelection = DataOnDemand(location))]

  location = "Phys/StdLooseAllPhotons/Particles"
  reviveSequences += [SelectionSequence("fs_std" + "Photons" , 
                          TopSelection = DataOnDemand(location))]

  location = "Phys/StdLooseMergedPi0/Particles"
  reviveSequences += [SelectionSequence("fs_std" + "Pi0" ,
                          TopSelection = DataOnDemand(location))]


  for sample in cfg:
################################################################################
## Configure sWeighting                                                       ##
################################################################################
    for line in cfg[sample].InputLines:
      location = tesFormat.replace('<line>', line) 
      protoLocation = location.replace('/Particles', '/Protos') 

      if cfg[sample].Calibration:
        swSequences += [ 
          ApplySWeights ("ApplySW"+sample,
                         InputTes   = location,
                         sTableDir  = cfg[sample].Calibration,
                         sTableName = "sTableSignal",
                         #OutputLevel = DEBUG
                         )
          ]


################################################################################
## Creates filter sequences to fill nTuples                                   ##
################################################################################

    selectionName = sample
    _cut = "DECTREE ('{}')".format ( cfg[sample].Decay.replace("^","") )

    if writeNullWeightCandidates == False:
      _cut += " & ( WEIGHT != 0 ) "

    if cfg[sample].Filter:
      _cut += " & ({}) ".format (  cfg[sample].Filter.cut )

    inputSelection = MergedSelection ( "input" + selectionName ,
          RequiredSelections = [
            DataOnDemand(tesFormat.replace('<line>', line))
              for line in  cfg[sample].InputLines ],
        )

    selection = Selection(selectionName,
        RequiredSelections = [ inputSelection ],
        Algorithm = FilterDesktop("alg_" + selectionName,
                                  Code = _cut),
      )

    filterSequence = SelectionSequence("Seq"+selectionName,
      TopSelection = selection)

    filterSequences += [filterSequence]

################################################################################
## Creates matching selections (used to create the proper locations in mdst)  ##
################################################################################
    matchingSel = Selection("Match" + selectionName,
        Algorithm = CopyAndMatchCombination ( "MatchAlg" + selectionName,
                                              Downstream = cfg[sample].Downstream,
                                              #OutputLevel = DEBUG
                                              ),
        RequiredSelections = [selection]
        )

    matchingSeq = SelectionSequence ( "SeqMatch"+ selectionName,
          TopSelection = matchingSel )

    matchSequences += [ matchingSeq ]



################################################################################
## Parses the configuration dictionaries and configure the tuples             ##
################################################################################
    tuple = DecayTreeTuple(sample + "Tuple")
    tuple.Inputs = [filterSequence.outputLocation()]
    tuple.Decay  = cfg[sample].Decay
    tuple.ToolList = [ "TupleToolEventInfo" ]

    eventTool = tuple.addTupleTool("LoKi::Hybrid::EvtTupleTool/LoKiEvent")

    if 'VOID' in eventVariables.keys():
      eventTool.VOID_Variables = eventVariables['VOID']

    eventTool.Preambulo = [
      "from LoKiTracks.decorators import *",
      "from LoKiCore.functions import *"]

    tuple.addTool( eventTool )
    
    tupleSequences += [tuple]

    for branchName in cfg[sample].Branches:
      b = tuple.addBranches({branchName : cfg[sample].Branches[branchName].Particle})
      b = b[branchName]

      lokitool = b.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_"+branchName)
      vardict = copy(varsByType[ cfg[sample].Branches[branchName].Type ])

      vardict.update (cfg[sample].Branches[branchName].LokiVariables)
      lokitool.Variables        = vardict

      
  print "Input TES: " 
  print "\n".join ( [f.outputLocation() for f in filterSequences] )
  if mdstOutputFile:
    dstSequences += configureMicroDSTwriter ( mdstOutputFile, mdstOutputPrefix, filterSequences)
    

  if reprocessing :
    return (swSequences + filterSequences + tupleSequences)
  else :
    return (filterSequences + dstSequences)
