###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from DecayTreeTuple.Configuration import *
from Configurables import GaudiSequencer
from Configurables import TupleToolDecay
from Configurables import LoKi__Hybrid__TupleTool as LoKiTupleTool
from Configurables import LoKi__Hybrid__EvtTupleTool as LoKiEvtTupleTool
    
class DecayConfigError(Exception):
  def __init__(self, name):
    self.name = name
    def __str__(self):
      s=("TupleBuilder - Bad configuration for {0}: expected 'top' "
         "in decay configuration dictionary").format(self.name)
      return repr(s)

class TriggerConfigError(Exception):
  def __init__(self, name, key, type="Trigger"):
    self.name=name
    self.key=key
    self.type=type
    def __str__(self):
      s=("TupleBuilder - Bad configuration of {0}: Got bad key {1} for {2} configuration "
         "dictionary. Valid keys are 'L0', 'HLT1', 'HLT2'").format(self.name, self.key, self.type)
      return repr(s)

class EvtCalibToolBadPropError(Exception):
  def __init__(self, name, key, tool):
    self.name=name
    self.key=key
    self.tool=tool
    def __str__(self):
      s=('TupleBuilder - Bad configuration of {0}: Got bad property '
         'name {1} for EvtTupleToolPIDCalib configuration dictionary. '
         'Valid properties are: {2}').format(self.name, self.key,
                                             str(self.tool.properties()))
      return repr(s)
    
class EvtCalibToolBadValueError(Exception):
  def __init__(self, name, key, value, msg):
    self.name=name
    self.key=key
    self.val=value
    self.msg=msg
    def __str__(self):
      s=("TupleBuilder - Bad configuration of {0}: Got bad value {1} of "
         "property {2} for EvtTupleToolPIDCalib configuration "
         "dictionary: {3}").format(self.name, self.val, self.key, self.msg)
      
      return repr(s)

class CalibToolBadPropError(Exception):
  def __init__(self, name, branch, key, tool):
    self.name=name
    self.key=key
    self.tool=tool
    self.branch=branch
    def __str__(self):
      s=('TupleBuilder - Bad configuration of {0}, branch {1}: Got bad property '
         'name {2} for TupleToolPIDCalib configuration dictionary. '
         'Valid properties are: {3}').format(self.name, self.branch, self.key,
                                             str(self.tool.properties()))
      return repr(s)
    
class CalibToolBadValueError(Exception):
  def __init__(self, name, branch, key, value, msg):
    self.name=name
    self.key=key
    self.val=value
    self.msg=msg
    self.branch=branch
    def __str__(self):
      s=("TupleBuilder - Bad configuration of {0}, branch {1}: Got bad value {2} of "
         "property {3} for TupleToolPIDCalib configuration "
         "dictionary: {4}").format(self.name, self.branch, self.val, self.key, self.msg)
      
      return repr(s)

class SequencerError(Exception):
  pass

  
class TupleBuilder(object):
    """Class to steer the configuration of DecayTreeTuple objects.
The class takes as arguments:
    name - the name of the DecayTreeTuple object;
    config_decay - a dictionary containing the decay descriptors for """
    """the branches, with the keys indicating the branch name. In addition, """
    """the decay descriptor should be specified with the key 'top'. If this """
    """is not specified, an exception will be raised.
    tupleName (optional) - the name of the tuple. By default, this is 'CalibPID'
    """
        
    def __init__(self, name, config_decay
                ,tupleName="CalibPID" ):

        self._stripLine = None
        self._stripLinePrefix = ""
        self._voidFilter = None
        self._candFilter = None
        self._name = name
        self._branches = None
        # copy the config dictionaries
        config_decay=config_decay.copy()

        # check that the configuration keys are valid
        if 'top' not in config_decay:
            raise DecayConfigError(name)
    
        # create the DecayTreeTuple object
        self._tuple = DecayTreeTuple(name)

        # clear the current ToolList
        self._tuple.ToolList=[]
  
        # set the decay descriptor and branches
        decayDesc = config_decay.pop("top")
        self._tuple.Decay=decayDesc
        if (len(config_decay)>0):
            self._branches = self._tuple.addBranches(config_decay)

        # set the tuple name
        self._tuple.TupleName=tupleName

    def setStrippingLine(self, lineName, tesPrefix="",
        stripReportsLocation='/Event/Strip/Phys/DecReports'):
        """Sets the name of the stripping line (lineName) to process. """
        """The optional arguments are the prefix of the TES location of the """
        """stripped particles (tesPrefix), and the TES location of the stripping """
        """DecReports (stripReportsLocation).
NB. For microDST, the tesPrefix should be set to an empty string (default)."""
        self._stripLine=lineName
        self._stripLinePrefix=tesPrefix
        self._stripReportsLocation=stripReportsLocation

    def setEventFilter(self, sel):
        """Sets the event-level filter selection. The filter selection should be """
        """a combination of LoKi functors with the following signature: void -> bool."""
        self._voidFilter=sel
 
    def setCandidateFilter(self, sel):
        """Sets the selection cuts used to filter the stripped candidates. """
        """The filter selection should be a list of LoKi functors that should be applied """
        """to the head (LHCb::Particle) of the decay.""" 
        self._candFilter=sel
        
    def addTools(self, config_toolLists):
        """config_toolLists should be a dictionary containing the list of """
        """tools to add.
The key indicates the branch name that the tools should be applied to, """
        """whilst 'top' indicates that the tools should be applied to all branches.
In addition 'MCTruth' indicates that the tools are MCTupleTool tools, """
        """such as MCTupleToolHeirarchy.
NB. Trigger/TISTOS tools, LoKi::Hybrid tuple tools and the PIDCalib """
        """tools are included and configured via separate methods."""
  
        # set top-level ToolList
        if 'top' in config_toolLists  and config_toolLists['top'] is not None:
            for tuptool in config_toolLists['top']:
                self._tuple.addTupleTool(tuptool)

        # set branch ToolLists
        for branch, dec in self._branches.items():
            if branch in config_toolLists and config_toolLists[branch] is not None:
                for tuptool in config_toolLists[branch]:
                    dec.addTupleTool(tuptool)

    def addMCTools(self, config_toolLists):
        """config_toolLists should be a dictionary containing the list of """
        """MCTupleTool tools to add (e.g. MCTupleToolHierarchy.
The key indicates the branch name that the tools should be applied to, """
        """whilst 'top' indicates that the tools should be applied to all branches."""
        # set top-level MCTruth ToolList
        if 'top' in config_toolLists:
            MCTruth=self._tuple.addTupleTool("TupleToolMCTruth")
            for tuptool in config_toolLists['top']:
                MCTruth.addTupleTool(tuptool)
        
        # set branch MCTruth ToolLists
        for branch, dec in self._branches.items():
            if branch in config_toolLists:
                MCTruth=dec.addTupleTool("TupleToolMCTruth")
                for tuptool in config_toolLists[branch]:
                    MCTruth.addTupleTool(tuptool)
                    
    def setVars(self, config_vars):
        """config_vars should be a dictionary containing dictionaries of """
        """LoKi::Hybrid::TupleTool variables.
The top-level key should be the branch name. The lower-level key """
        """should be the name of the variable, and the value should be the """
        """LoKi functor.
The lower-level key 'Preambulo' can be used to specify additions """
        """to the preambulo, if needed."""
        # set the LoKi::HybridTupleTool variables for the given branch
        for branch, dec in self._branches.items():
            if branch in config_vars and config_vars[branch] is not None:
                tool = dec.addTupleTool("LoKi::Hybrid::TupleTool/LoKiTool")
                preambulo = config_vars[branch].pop("Preambulo", None)
                tool.Variables=config_vars[branch]
                if preambulo is not None:
                    tool.Preambulo += preambulo

    def setEvtVars(self, config_evtVars):
        """config_evtVars should contain LoKi::Hybrid::EvtTupleTool variables.
The keys represent the variable name, whilst the values represent """
        """the relevant LoKi functor. The key 'Preambulo' can be used to specify """
        """additions to the preambulo, if needed.
NB. This method assumes all variables are VOID variables."""
    
        tool=self._tuple.addTupleTool("LoKi::Hybrid::EvtTupleTool/EvtLoKiTool")
        preambulo = config_evtVars.pop("Preambulo", None)
        tool.VOID_Variables=config_evtVars
  
    def setCalibTools(self, config_calibTupleTools):
        """config_calibTupleTools should be a dictionary containing the """
        """properties to set for the TupleToolPIDCalib instances of the """
        """branches. The key represents the branch name, whilst the values """
        """represent the dictionary of properties names (as defined in """
        """PIDCalibTupleTool::initialize) to values, e.g. {'Verbose' : True})."""
        for branch, dec in self._branches.items():
            if branch in config_calibTupleTools:
                tool = dec.addTupleTool("TupleToolPIDCalib")
                for propName, prop in (config_calibTupleTools[branch]).items():
                    try:
                        tool.setProp(propName, prop)
                    except AttributeError:
                        raise CalibToolBadPropError(self.name(), branch, propName, tool)
                    except ValueError as e:
                        raise CalibToolBadValueError(self.name(), branch, propName,
                                prop, e.message)
                
    def setEvtCalibTool(self, config_evtCalibTupleTool):
        """config_evtCalibTupleTool should be a dictionary containing the properties """
        """to set for the EvtTupleToolPIDCalib instance. The keys should be the """
        """properties names (as defined in EvtPIDCalibTupleTool::initialize), """
        """whilst the values should be the property values to set, e.g. """
        """{'Verbose' : True})."""
        tool = self._tuple.addTupleTool("EvtTupleToolPIDCalib")
        for propName, prop in config_evtCalibTupleTool.items():
            try:
                tool.setProp(propName, prop)
            except AttributeError:
                raise EvtCalibToolBadPropError(self.name(), propName, tool)
            except ValueError as e:
                raise EvtCalibToolBadValueError(self.name(), propName, prop, e.message)
    
    def setTriggerTool(self, config_triggerLines):
        """config_triggerLines should be a dictionary containing the list of """
        """trigger lines to calculate the decisions for. The key can be one """
        """of 'L0', 'HLT1' or 'HLT2'."""
        tool = self._tuple.addTupleTool("TupleToolTrigger/Trigger")
        triggerLines=[]
        for key, triggers in config_triggerLines.items():
            if key=='L0':
                tool.VerboseL0=True
                triggerLines+=triggers
            elif key=='HLT1':
                tool.VerboseHlt1=True
                triggerLines+=triggers
            elif key=='HLT2':
                tool.VerboseHlt2=True
                triggerLines+=triggers
            else:
                raise TriggerConfigError(self.name(), key, "Trigger")
        tool.TriggerList=triggerLines

    def setTISTOSTool(self, config_triggerLines):
        """config_triggerLines should be a dictionary containing the list of """
        """trigger lines to calculate the TISTOS decisions for. The key can be one """
        """of 'L0', 'HLT1' or 'HLT2'."""
        tool = self._tuple.addTupleTool("TupleToolTISTOS/TISTOS")
        triggerLines=[]
        for key, triggers in config_triggerLines.items():
            if key=='L0':
                tool.VerboseL0=True
                triggerLines+=triggers
            elif key=='HLT1':
                tool.VerboseHlt1=True
                triggerLines+=triggers
            elif key=='HLT2':
                tool.VerboseHlt2=True
                triggerLines+=triggers
            else:
                raise TriggerConfigError(self.name(), key, "TISTOS")
        tool.TriggerList=triggerLines

    def tuple(self):
        """Returns the underlying DecayTreeTuple object."""
        return self._tuple
    
    def sequence(self):
        """Returns a GaudiSequencer object containing the DecayTreeTuple """
        """object, and the various filter if defined."""
        if self._stripLine is None:
            raise SequencerError("Cannot create tuple sequence: "
                "input particle location has not been set")
                
        tupleSeq=GaudiSequencer("TupleSeq_{0}".format(self.name()))
        tupleSeq.IgnoreFilterPassed=False
        tupleSeq.ShortCircuit=True
        tupleSeq.ModeOR=False
        from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from Configurables import LoKi__HDRFilter as HDRFilter
        from Configurables import LoKi__VoidFilter as VOIDFilter
       
        tesLocation = "{pref}Phys/{line}/Particles".format(
            pref=self._stripLinePrefix,
            line=self._stripLine)
        inputData = AutomaticData(tesLocation)
        
        inputLocation = inputData.outputLocation()
        
        # set the candidate filter (if applicable)
        candFilterSeq = None
        if self._candFilter is not None:
            candFilter = Selection("{0}_{1}Filter".format(
                self.name(), self._stripLine),
                RequiredSelections=[inputData],
                Algorithm=FilterDesktop(Code=self._candFilter)
            )
            candFilterSeq = SelectionSequence('SelSeq{0}'.format(
                candFilter.name()),
                TopSelection=candFilter)
            inputLocation = candFilterSeq.outputLocation()
            
        # required for reverse compatibility with old DaVinci releases
        if hasattr(self._tuple,'Inputs'):
            self._tuple.Inputs=[inputLocation]
        else:
            self._tuple.InputLocations=[inputLocation]
        
        # make the stripping filter
        stripFilter = HDRFilter("{0}_StripFilter".format(self.name()),
            Code="HLT_PASS('Stripping{0}Decision')".format(self._stripLine),
            Location=self._stripReportsLocation)
        tupleSeq.Members+=[stripFilter]

        # make the event filter (if applicable)
        eventFilter = None
        if self._voidFilter is not None:
            eventFilter = VOIDFilter("{0}_EventFilter".format(self.name()),
                Code=self._voidFilter)
            tupleSeq.Members+=[eventFilter]
        
        if candFilterSeq is not None:
            tupleSeq.Members+=[candFilterSeq.sequence()]
            
        tupleSeq.Members+=[self._tuple]
        return tupleSeq

    def name(self):
        """Returns the name of the DecayTreeTuple object."""
        return self._name

    def tupleName(self):
        """Returns the TupleName of the DecayTreeTuple object."""
        return self._tuple.TupleName

    def decayDesc(self):
        """Returns the top-level decay descriptor of the """
        """DecayTreeTuple object."""  
        return self._tuple.Decay

    def branchDesc(self, branch):
        """Returns the decay descriptor of the specified branch. """
        """Returns None if the branch does not exist."""
        if branch in self._tuple.Branches:
            return self._tuple.Branches[branch]
        else:
            return None  

    def branches(self):
        """Returns a dictionary containing the branch names as keys, """
        """and the branch decay descriptors as the values.""" 
        return self._tuple.Branches

    def branchVars(self, branch):
        """Returns the dictionary of LoKi::Hybrid::TupleTool variables """
        """for the given branch. Returns None if the branch does not exist."""
        if branch in self._branches and hasattr(self._branches[branch], 'LoKiTool'):
            return self._branches[branch].LoKiTool.Variables
        else:
            return None

    def branchPreambulo(self, branch):
        """Returns the preambulo for the LoKi::Hybrid::TupleTool for the """
        """given branch. Returns None if the branch does not exist."""
        if branch in self._branches and hasattr(self._branches[branch], 'LoKiTool'):
            return self._branches[branch].LoKiTool.Preambulo
        else:
            return None
    
    def branchToolList(self, branch):
        """Returns the tool list for the given branch. Returns None if the """
        """branch does not exist."""
        if branch in self._branches and hasattr(self._branches[branch], 'LoKiTool'):
            return self._branches[branch].ToolList
        else:
            return None

    def branchToolLists(self):
        """Returns a dictionary containing the branch names as keys, """
        """and the branch tool lists as the values."""
        toolLists={}
        for branch in self._tuple.Branches:
            if self.branchToolList(branch) is not None:
                toolLists[branch]=self.branchToolList(branch)
        return toolLists  

    def vars(self):
        """Returns a dictionary containing the branch names as keys, """
        """and the LoKi::Hybrid::TuplTool variables as the values."""
        varDict={}
        for branch in self._tuple.Branches:
            if self.branchVars(branch) is not None:
                varDict[branch]=self.branchVars(branch)
        return varDict

    def preambulos(self):
        """Returns a dictionary containing the branch names as keys, """
        """and the preambulos of the LoKi::Hybrid::TuplTool objects """
        """as the values."""
        preambulos={}
        for branch in self._tuple.Branches:
            if self.branchPreambulo(branch) is not None:
                preambulos[branch]=self.branchPreambulo(branch)
        return preambulos

    def evtVars(self):
        """Returns a dictionary containing the LoKi::Hybrid::EvtTupleTool """
        """variables. Returns None if the EvtTupleTool does not exist."""    
        if hasattr(self._tuple, "EvtLoKiTool"):
            return self._tuple.EvtLoKiTool.VOID_Variables
        else: 
            return None  

    def evtPreambulo(self):
        """Returns the preambulo for the LoKi::Hybrid::EvtTupleTool. """
        """Returns None if the EvtTupleTool does not exist."""
        if hasattr(self._tuple, "EvtLoKiTool"):
            return self._tuple.EvtLoKiTool.Preambulo
        else: 
            return None  

    def toolList(self):
        """Returns the top-level tool list."""
        return self._tuple.ToolList

    def triggerList(self):
        """Returns the list of triggers for the TupleToolTrigger object. """
        """Returns None if the TupleToolTrigger object does not exist."""
        if hasattr(self._tuple, "Trigger"):
            return self._tuple.Trigger.TriggerList

    def triggerTISTOSList(self):
        """Returns the list of TISTOS triggers for the TupleToolTISTOS object. """
        """Returns None if the TupleToolTISTOS object does not exist."""
        if hasattr(self._tuple, "TISTOS"):
            return self._tuple.TISTOS.TriggerList
    
    # def __str__(self):
        # return self.name()
        
    def __str__(self):
        s="""name: '{name}'
    tuple name: '{tupName}'
    decay descriptor: '{decDesc}'
    branches: {branches}
    tool list: {tools}
    event variables: {vars}
    event preambulo: {preams}
    branch tool lists: {brTools}
    branch variables: {brVars}
    branch preambulos: {brPreams}
    trigger list: {trigList}
    TISTOS trigger list: {tistosList}
    """.format(
        name=self.name(),
        tupName=self.tupleName(),
        decDesc=self.decayDesc(),
        branches=str(self.branches()), 
        tools=str(self.toolList()),
        vars=str(self.evtVars()),
        preams=str(self.evtPreambulo()),
        brTools=str(self.branchToolLists()),
        brVars=str(self.vars()),
        brPreams=str(self.preambulos()),
        trigList=str(self.triggerList()),
        tistosList=str(self.triggerTISTOSList()))
        return s    
