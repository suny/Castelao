###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from CalibDataSel.DecayTreeTuple.TupleBuilder import TupleBuilder
from CalibDataSel.DecayTreeTuple.TupleVars_uDST import *

############### DecayTreeTuple objects ###############
TupleBuilders={}
for name, dec in DecayNames.items():
    builder = TupleBuilder(name+'Tuple', dec)
    builder.addTools(ToolLists)
    # set the LoKi EvtTupleTool variables
    if EvtTupleVars is not None:
        builder.setEvtVars(EvtTupleVars)
    # set theLoKi TupleTool variables
    if TupleVars is not None:
        builder.setVars(TupleVars)
    # add the EvtPIDCalibTupleTool and set its properties
    if EvtCalibToolConf is not None:
        if name in EvtCalibToolConf and EvtCalibToolConf[name] is not None:
            builder.setEvtCalibTool(EvtCalibToolConf[name])
    # add the PIDCalibTupleTools and set their properties
    if CalibToolConf is not None:
        if name in CalibToolConf and CalibToolConf[name] is not None:
            builder.setCalibTools(CalibToolConf[name])
    # add the TISTOS tuple tool and set the trigger list
    if TriggerLines is not None:
        if name in TriggerLines and TriggerLines[name] is not None:
            builder.setTISTOSTool(TriggerLines[name])
    TupleBuilders[name]=builder
__all__=('TupleBuilders',)
