###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
###############################################################
#                                                             #
##### configuration dictionary of LoKiTupleTool variables #####
#                                                             #
# The key must correspond to a branch in the DecayTreeTuple   #
#                                                             #
###############################################################

TupleVars={
 #    'K0S' : {
 #        'WM_Lam0'     : "WM('p+','pi-')"
 #        ,'WM_Lam0bar' : "WM('pi+','p~-')"
 #    }
##     ,'Lam0' : {
##         'WM_KS0' : "WM('pi+','pi-')"
##     }
##     ,'D0' : {
##         'WM_PiPi'    : "WM('pi+','pi-')"
##         ,'WM_KK'      : "WM('K+','K-')"
##         ,'WM_DCS'     : "WM('pi+','K-')"
##     }
    'Lamc' : {
        'WM_PiKPi': "WM('pi+', 'K-', 'pi+')" # D->Kpipi,
        ,'WM_KPiPi' : "WM('K-', 'pi+', 'pi+')" # D->Kpipi                       
        ,'WM_KKPi'  : "WM('K-', 'K+', 'pi+')" # D->KKpi          
    }
}

###############################################################
#                                                             #
##### Configuration dictionary of LoKi EvtTuple variables #####
#                                                             #
###############################################################
EvtTupleVars = None

###############################################################
#                                                             #
#####      Configuration dictionary of decay names        #####
#                                                             #
# 1) The key is the name of the DecayTreeTuple instance.      #
# 2) 'top' is the decay name, and MUST be specified.          #
# 3) All other values represent the branches.                 #
###############################################################
_DecayNames_RICH = {
    'K0S2PiPi' : {
        'top'  : 'KS0 -> ^pi+ ^pi-'
        ,'Pi'  : 'KS0 -> ^pi+ ^pi-'
        ,'K0S' : 'KS0 -> pi+ pi-'
    }
    ,'Phi2KK' : {
        'top'  : 'phi(1020) -> ^K+ ^K-'
        ,'K'   : 'phi(1020) -> ^K+ ^K-'
        ,'Phi' : 'phi(1020) -> K+ K-'
    }
    ,'Lam02PPi_LoP' : {
        'top'   : '[ Lambda0 -> ^p+ ^pi- ]CC'
        ,'P'    : '[ Lambda0 -> ^p+ pi- ]CC'
        ,'Pi'   : '[ Lambda0 -> p+ ^pi- ]CC'
        ,'Lam0' : '[ Lambda0 -> p+ pi- ]CC'
    }
    ,'Lam02PPi_HiP' : {
        'top'   : '[ Lambda0 -> ^p+ ^pi- ]CC'
        ,'P'    : '[ Lambda0 -> ^p+ pi- ]CC'
        ,'Pi'   : '[ Lambda0 -> p+ ^pi- ]CC'
        ,'Lam0' : '[ Lambda0 -> p+ pi- ]CC'
    }
    ,'DSt2D0Pi_D02RSKPi' : {
        'top'   : '[ D*(2010)- -> ^(D~0 -> ^K+ ^pi-) ^pi- ]CC'
         ,'D0'  : '[ D*(2010)- -> ^D~0 pi- ]CC'
         ,'K'   : '[ D*(2010)- -> (D~0 -> ^K+ pi-) pi- ]CC'
         ,'Pi'  : '[ D*(2010)- -> (D~0 -> K+ ^pi-) pi- ]CC'
         ,'sPi' : '[ D*(2010)- -> D~0 ^pi- ]CC'
         ,'DSt' : '[ D*(2010)- -> (D~0 -> K+ pi-) pi- ]CC'
    }

 ,'IncLc2PKPi' : {
        'top'   : '[ Lambda_c+ -> ^p+ ^K- ^pi+ ]CC'
        ,'P'    : '[ Lambda_c+ -> ^p+ K- pi+ ]CC'
        ,'K'    : '[ Lambda_c+ -> p+ ^K- pi+ ]CC'
        ,'Pi'   : '[ Lambda_c+ -> p+ K- ^pi+ ]CC'
        ,'Lamc' : '[ Lambda_c+ -> p+ K- pi+ ]CC'
    }

    ,'Lb2LcMuNu_Lc2PKPi' : {
        'top'   : '[ Lambda_b0 -> ^([ Lambda_c+ -> ^p+ ^K- ^pi+ ]cc) ^mu- ]CC'
        ,'Lamc' : '[ Lambda_b0 -> ^Lambda_c+ mu- ]CC'
        ,'P'    : '[ Lambda_b0 -> ([ Lambda_c+ -> ^p+ K- pi+ ]cc) mu- ]CC'
        ,'K'    : '[ Lambda_b0 -> ([ Lambda_c+ -> p+ ^K- pi+ ]cc) mu- ]CC'
        ,'Pi'   : '[ Lambda_b0 -> ([ Lambda_c+ -> p+ K- ^pi+ ]cc) mu- ]CC'
        ,'Mu'   : '[ Lambda_b0 -> Lambda_c+ ^mu- ]CC'
        ,'Lamb' : '[ Lambda_b0 -> ([ Lambda_c+ -> p+ K- pi+ ]cc) mu- ]CC'
    }
    ,'Sigc02LcPi_Lc2PKPi' : {
        'top'   : '[ Sigma_c0  -> ^([ Lambda_c+ -> ^p+ ^K- ^pi+ ]cc) ^pi- ]CC'
        ,'Lamc' : '[ Sigma_c0 -> ^Lambda_c+ pi- ]CC'
        ,'P'    : '[ Sigma_c0 -> ([ Lambda_c+ -> ^p+ K- pi+ ]cc) pi- ]CC'
        ,'K'    : '[ Sigma_c0 -> ([ Lambda_c+ -> p+ ^K- pi+ ]cc) pi- ]CC'
        ,'Pi'   : '[ Sigma_c0 -> ([ Lambda_c+ -> p+ K- ^pi+ ]cc) pi- ]CC'
        ,'sPi'  : '[ Sigma_c0 -> Lambda_c+ ^pi- ]CC'
        ,'Sigc0': '[ Sigma_c0  -> ([ Lambda_c+ -> p+ K- pi+ ]cc) pi- ]CC'
    }
    ,'Sigcpp2LcPi_Lc2PKPi' : {
        'top'   : '[ Sigma_c++  -> ^([ Lambda_c+ -> ^p+ ^K- ^pi+ ]cc) ^pi+ ]CC'
        ,'Lamc' : '[ Sigma_c++ -> ^Lambda_c+ pi+ ]CC'
        ,'P'    : '[ Sigma_c++ -> ([ Lambda_c+ -> ^p+ K- pi+ ]cc) pi+ ]CC'
        ,'K'    : '[ Sigma_c++ -> ([ Lambda_c+ -> p+ ^K- pi+ ]cc) pi+ ]CC'
        ,'Pi'   : '[ Sigma_c++ -> ([ Lambda_c+ -> p+ K- ^pi+ ]cc) pi+ ]CC'
        ,'sPi'  : '[ Sigma_c++ -> Lambda_c+ ^pi+ ]CC'
        ,'Sigcpp' : '[ Sigma_c++  -> ([ Lambda_c+ -> p+ K- pi+ ]cc) pi+ ]CC'
    }
    ,'JpsieeKFromB' : {
        'top'   : '( ( B+  -> ^( ( J/psi(1S)  -> ^( e+ ) ^( e- )) ) ^( K+ )) || ( B-  -> ^( ( J/psi(1S)  -> ^( e+ ) ^( e- )) ) ^( K- )) )'
        ,'e0'   : '( ( B+  -> ( J/psi(1S)  -> ^( e+ ) e- )  K+ ) || ( B-  -> ( J/psi(1S)  -> ^( e+ ) e-)  K- ) ) '
        ,'e1'   : '( ( B+  -> ( J/psi(1S)  -> e+ ^( e- ) )  K+ ) || ( B-  -> ( J/psi(1S)  -> e+ ^( e- ) )  K- ) )'
        ,'Jpsi' : '( ( B+  -> ^( ( J/psi(1S)  -> e+  e- ) ) K+ ) || ( B-  -> ^( ( J/psi(1S)  -> e+  e- ) ) K- ) )'
        ,'Bach' : '( ( B+  -> ( J/psi(1S)  -> e+  e- )  ^( K+ )) || ( B-  -> ( J/psi(1S)  -> e+  e- )  ^( K- )) )'
        ,'Bu'   : '( ( B+  -> ( J/psi(1S)  -> e+  e- )  K+ ) || ( B-  -> ( J/psi(1S)  -> e+  e- )  K- ) )'
        #'top'   : '[ B+ -> ^(J/psi(1S) -> ^e+ ^e-) ^K+ ]CC'
        #,'e0'    : '[ B+ -> (J/psi(1S) -> ^e+  e-) K+ ]CC'
        #,'e1'    : '[ B+ -> (J/psi(1S) -> e+  ^e-) K+ ]CC'
        #,'Jpsi' : '[ B+ -> ^(J/psi(1S) -> e+  e-) K+ ]CC'
        #,'Bach' : '[ B+ -> (J/psi(1S) -> e+  e-) ^K+ ]CC'
        #,'Bu'   : '[ B+ -> (J/psi(1S) -> e+  e-) K+ ]CC'
    }
}

_DecayNames_Muon = {
    'JpsiFromBNoPIDNoMip' : {
        'top'   : 'J/psi(1S) -> ^mu+ ^mu-'
        ,'Jpsi' : 'J/psi(1S) -> mu+ mu-'
        ,'Mu0'    : 'J/psi(1S) -> ^mu+ mu-'
        ,'Mu1'    : 'J/psi(1S) -> mu+ ^mu-'
    }
    ,'JpsiKFromBNoPIDNoMip' : {#TODO: fix this dd
        'top'   : '( ( B+  -> ^( ( J/psi(1S)  -> ^( mu+ ) ^( mu- )) ) ^( K+ )) || ( B-  -> ^( ( J/psi(1S)  -> ^( mu- ) ^( mu+ )) ) ^( K- )) )'
        ,'Mu0'   : '( ( B+  -> ( J/psi(1S)  -> mu-  ^( mu+ ))  K+ ) || ( B-  -> ( J/psi(1S)  -> mu-  ^( mu+ ))  K- ) ) '
        ,'Mu1'   : '( ( B+  -> ( J/psi(1S)  -> ^( mu- ) mu+ )  K+ ) || ( B-  -> ( J/psi(1S)  -> ^( mu- ) mu+ )  K- ) )'
        ,'Jpsi' : '( ( B+  -> ^( ( J/psi(1S)  -> mu+  mu- ) ) K+ ) || ( B-  -> ^( ( J/psi(1S)  -> mu-  mu+ ) ) K- ) )'
        ,'Bach' : '( ( B+  -> ( J/psi(1S)  -> mu+  mu- )  ^( K+ )) || ( B-  -> ( J/psi(1S)  -> mu-  mu+ )  ^( K- )) )'
        ,'Bu'   : '( ( B+  -> ( J/psi(1S)  -> mu+  mu- )  K+ ) || ( B-  -> ( J/psi(1S)  -> mu-  mu+ )  K- ) )'
          
        #'top'   : '[ B+ -> ^(J/psi(1S) -> ^mu+ ^mu-) ^K+ ]CC'
        #,'Mu'   : '[ B+ -> (J/psi(1S) -> ^mu+  ^mu-) K+ ]CC'
        #,'Jpsi' : '[ B+ -> ^(J/psi(1S) -> mu+  mu-) K+ ]CC'
        #,'Bach' : '[ B+ -> (J/psi(1S) -> mu+  mu-) ^K+ ]CC'
        #,'Bu'   : '[ B+ -> (J/psi(1S) -> mu+  mu-) K+ ]CC'
        ##'top'   : '[ B+ -> ^(J/psi(1S) -> ^mu+ ^mu-) ^K+ ]CC'
        ##,'Mu'   : '[ B+ -> (J/psi(1S) -> ^mu+  ^mu-) K+ ]CC'
        ##,'Jpsi' : '[ B+ -> ^(J/psi(1S) -> mu+  mu-) K+ ]CC'
        ##,'Bach' : '[ B+ -> (J/psi(1S) -> mu+  mu-) ^K+ ]CC'
        ##,'Bu'   : '[ B+ -> (J/psi(1S) -> mu+  mu-) K+ ]CC'
    }
}

_DecayNames_MuonUnBiased={}
for line, dec in _DecayNames_RICH.items():
    _DecayNames_MuonUnBiased['{0}_MuonUnBiased'.format(line)]=dec

_DecayNames_MuonTosTagged={}
for line, dec in _DecayNames_Muon.items():
    _DecayNames_MuonTosTagged['{0}_TOSTagged'.format(line)]=dec

DecayNames = _DecayNames_RICH.copy()
DecayNames.update(_DecayNames_Muon)
DecayNames.update(_DecayNames_MuonUnBiased)
DecayNames.update(_DecayNames_MuonTosTagged)

###############################################################
#                                                             #
#####  Configuration dictionary of EvtPIDCalibTupleTool   #####
#                                                             #
# 1) The key is the name of the DecayTreeTuple instance.      #
# 2) The values are a dictionary of EvtPIDCalibTupleTool      #
#    properties to values                                     #
###############################################################
_evtCalibToolConf_default={
    'Verbose' : False
    }

EvtCalibToolConf = {}
for k in DecayNames.keys():
    EvtCalibToolConf[k]=_evtCalibToolConf_default

###############################################################
#                                                             #
#####   Configuration dictionary of PIDCalibTupleTool     #####
#                                                             #
# 1) The key is the name of the DecayTreeTuple instance.      #
# 2) The values are dictionaries of branch names to a         #
#    dictionary of PIDCalibTupleTool properties to values     #
###############################################################

from Gaudi.Configuration import DEBUG, VERBOSE

################# Calib. tool properties ####################
# default branches
_calibToolConf_default = {
    'Verbose' : False,
    'FillPID' : False,
    'FillElectronTagAndProbe' : False,
    'FillElectronTisTagged' : False,
    'FillElectronTosTagged' : False,
    'FillMuonTagAndProbe' : False,
    'FillMuonTisTagged' : False,
    'FillMuonTosTagged' : False,
    'FillRichRadInfo' : False,
    'FillBremInfo' : False
}

# RICH PID branches
_calibToolConf_RICH = _calibToolConf_default.copy()
_calibToolConf_RICH['FillRichRadInfo'] = True
_calibToolConf_RICH['FillPID'] = True
_calibToolConf_RICH['FillBremInfo'] = True
_calibToolConf_RICH['FillMuonTisTagged']=True


# electron branches
_calibToolConf_electron = _calibToolConf_RICH.copy()
_calibToolConf_electron['FillBremInfo'] = True
_calibToolConf_electron['FillElectronTagAndProbe']=True
_calibToolConf_electron['FillElectronTisTagged']=True
#_calibToolConf_electron['OutputLevel']=DEBUG
#_calibToolConf_electron['FillElectronTosTagged']=True

# Jpsi from B muon PID branches
_calibToolConf_JpsiFromB = _calibToolConf_RICH.copy()
_calibToolConf_JpsiFromB['FillMuonTagAndProbe']=True
_calibToolConf_JpsiFromB['FillMuonTosTagged']=True

# Jpsi K from B muon PID branches
_calibToolConf_JpsiKFromB = _calibToolConf_RICH.copy()
_calibToolConf_JpsiKFromB['FillMuonTagAndProbe']=True
_calibToolConf_JpsiKFromB['FillMuonTosTagged']=True

#two below not need anymore
# MuonUnBiased (TISTagged) muon misID branches
_calibToolConf_MuonUnBiased = _calibToolConf_RICH.copy()
_calibToolConf_MuonUnBiased['FillMuonTisTagged']=True

# TOSTagged muon PID branches
_calibToolConf_TOSTagged = _calibToolConf_RICH.copy()
_calibToolConf_TOSTagged['FillMuonTagAndProbe']=True
_calibToolConf_TOSTagged['FillMuonTosTagged']=True

### Calib. tool properties
CalibToolConf = {
    # standard tuples
    'K0S2PiPi' : {
        'K0S' : _calibToolConf_default
        ,'Pi' : _calibToolConf_RICH
    }
    ,'Lam02PPi_LoP' : {
        'Lam0' : _calibToolConf_default
        ,'P' : _calibToolConf_RICH
        #,'Pi' : _calibToolConf_default
    }
    ,'Lam02PPi_HiP' : {
        'Lam0' : _calibToolConf_default
        ,'P' : _calibToolConf_RICH
        #,'Pi' : _calibToolConf_default
    }
    ,'DSt2D0Pi_D02RSKPi' : {
        'DSt' : _calibToolConf_default
        ,'D0' : _calibToolConf_default
        #,'sPi' : _calibToolConf_default
        ,'K' : _calibToolConf_RICH
        ,'Pi' : _calibToolConf_RICH
    }
    ,'Lb2LcMuNu_Lc2PKPi' : {
        'Lamc' : _calibToolConf_default
        ,'P'    : _calibToolConf_RICH
        #,'K'    : _calibToolConf_default
        #,'Pi'   : _calibToolConf_default
        #,'Mu'   : _calibToolConf_default
        #,'Lamb' : _calibToolConf_default
    }
    ,'Sigc02LcPi_Lc2PKPi' : {
        'Lamc' : _calibToolConf_default
        ,'P'    : _calibToolConf_RICH
        #,'K'    : _calibToolConf_default
        #,'Pi'   : _calibToolConf_default
        #,'sPi'  : _calibToolConf_default
        ,'Sigc0' : _calibToolConf_default
    }
    ,'Sigcpp2LcPi_Lc2PKPi' : {
        'Lamc' : _calibToolConf_default
        ,'P'    : _calibToolConf_RICH
        #,'K'    : _calibToolConf_default
        #,'Pi'   : _calibToolConf_default
        #,'sPi'  : _calibToolConf_default
        ,'Sigc0' : _calibToolConf_default
    }
     ,'IncLc2PKPi' : {
        'Lamc' : _calibToolConf_default
        ,'P'    : _calibToolConf_RICH
        ,'K'    : _calibToolConf_default
        ,'Pi'   : _calibToolConf_default
    }
    ,'JpsiFromBNoPIDNoMip' : {
        'Jpsi' : _calibToolConf_default
        ,'Mu0' : _calibToolConf_JpsiFromB
        ,'Mu1' : _calibToolConf_JpsiFromB
    }
    ,'JpsiKFromBNoPIDNoMip' : {
        'Jpsi' : _calibToolConf_default
        ,'Mu0' : _calibToolConf_JpsiKFromB
        ,'Mu1' : _calibToolConf_JpsiKFromB
        #,'Bach' : _calibToolConf_default
        #,'Bu' : _calibToolConf_default
    }
    ,'JpsieeKFromB' : {
        'Jpsi' : _calibToolConf_default
        ,'e0'   : _calibToolConf_electron
        ,'e1'   : _calibToolConf_electron
        #,'Bach' : _calibToolConf_default
        ,'Bu' : _calibToolConf_default
    }
    # MuonUnBiased tuples are not called anymore
    ,'K0S2PiPi_MuonUnBiased' : {
        'K0S' : _calibToolConf_RICH
        ,'Pi' : _calibToolConf_MuonUnBiased
    }
    ,'Lam02PPi_LoP_MuonUnBiased' : {
        'Lam0' : _calibToolConf_default
        ,'P' : _calibToolConf_MuonUnBiased
        #,'Pi' : _calibToolConf_default
    }
    ,'Lam02PPi_HiP_MuonUnBiased' : {
        'Lam0' : _calibToolConf_default
        ,'P' : _calibToolConf_MuonUnBiased
        #,'Pi' : _calibToolConf_default
    }
    ,'DSt2D0Pi_D02RSKPi_MuonUnBiased' : {
        'DSt' : _calibToolConf_default
        ,'D0' : _calibToolConf_default
        #,'sPi' : _calibToolConf_default
        ,'K' : _calibToolConf_MuonUnBiased
        ,'Pi' : _calibToolConf_MuonUnBiased
    }
    ,'Lb2LcMuNu_Lc2PKPi_MuonUnBiased' : {
        'Lamc' : _calibToolConf_default
        ,'P'    : _calibToolConf_MuonUnBiased
        #,'K'    : _calibToolConf_default
        #,'Pi'   : _calibToolConf_default
        #,'Mu'   : _calibToolConf_default
        #,'Lamb' : _calibToolConf_default
    }
    ,'Sigc02LcPi_Lc2PKPi_MuonUnBiased' : {
        'Lamc' : _calibToolConf_default
        ,'P'    : _calibToolConf_MuonUnBiased
        #,'K'    : _calibToolConf_default
        #,'Pi'   : _calibToolConf_default
        #,'sPi'  : _calibToolConf_default
        ,'Sigc0' : _calibToolConf_default
    }
    ,'Sigcpp2LcPi_Lc2PKPi_MuonUnBiased' : {
        'Lamc'    : _calibToolConf_default
        ,'P'      : _calibToolConf_MuonUnBiased
        #,'K'      : _calibToolConf_default
        #,'Pi'     : _calibToolConf_default
        #,'sPi'    : _calibToolConf_default
        ,'Sigcpp' : _calibToolConf_default
    }
    ,'JpsieeKFromB_MuonUnBiased' : {
        'Jpsi' : _calibToolConf_default
        ,'e0'   : _calibToolConf_MuonUnBiased
        ,'e1'   : _calibToolConf_MuonUnBiased
        #,'Bach' : _calibToolConf_default
        #,'Bu' : _calibToolConf_default
    }
    # TOSTagged tuples
    ,'JpsiFromBNoPIDNoMip_TOSTagged' : {
        'Jpsi' : _calibToolConf_default
        ,'Mu0' : _calibToolConf_TOSTagged
        ,'Mu1' : _calibToolConf_TOSTagged
    }
    ,'JpsiKFromBNoPIDNoMip_TOSTagged' : {
        'Jpsi' : _calibToolConf_default
        ,'Mu0' : _calibToolConf_TOSTagged
        ,'Mu1' : _calibToolConf_TOSTagged
        #,'Bach' : _calibToolConf_default
       
    }
}

###############################################################
#                                                             #
####   Configuration dictionary of trigger/TISTOS lines    ####
#                                                             #
# 1) The keys must be one of L0, HLT1 or HLT2                 #
#                                                             #
###############################################################

TriggerLines = {
    #standard lines
    "K0S2PiPi"                          : None
    ,"Lam02PPi_LoP"                     : None
    ,"Lam02PPi_HiP"                     : None
    ,"DSt2D0Pi_D02RSKPi"                : None
    ,'Lb2LcMuNu_Lc2PKPi'                : None
    ,'Sigc02LcPi_Lc2PKPi'               : None
    ,'Sigcpp2LcPi_Lc2PKPi'              : None
    ,'JpsiFromBNoPIDNoMip'              : None
    ,'JpsiKFromBNoPIDNoMip'             : None
    ,'JpsieeKFromB'                     : None
    # MuonUnBiased tuples
    ,'K0S2PiPi_MuonUnBiased'            : None
    ,'Lam02PPi_LoP_MuonUnBiased'        : None
    ,'Lam02PPi_HiP_MuonUnBiased'        : None
    ,'DSt2D0Pi_D02RSKPi_MuonUnBiased'   : None
    ,'Lb2LcMuNu_Lc2PKPi_MuonUnBiased'   : None
    ,'Sigc02LcPi_Lc2PKPi_MuonUnBiased'  : None
    ,'Sigcpp2LcPi_Lc2PKPi_MuonUnBiased' : None
    ,'JpsieeKFromB_MuonUnBiased'        : None
    # TOSTagged tuples
    ,'JpsiFromBNoPIDNoMip_TOSTagged'    : None
    ,'JpsiKFromBNoPIDNoMip_TOSTagged'   : None
  }


###############################################################
#                                                             #
####            Tool list for collision data               ####
#                                                             #
###############################################################

ToolLists = {
  'top'     : []
  ,'K0S'    : []
  ,'Lam0'   : []
  ,'DSt'    : []
  ,'D0'     : []
  ,'Jpsi'   : []
  ,'Lamc'   : []
  ,'Lamb'   : []
  ,'Sigcpp' : []
  ,'Sigc0'  : []
  ,'sPi'    : []
  ,'Bach'   : []
  ,'K'      : []
  ,'Pi'     : []
  ,'P'      : []
  ,'Mu'     : []
  }

###############################################################
#                                                             #
####             Tool list for Monte Carlo                 ####
#                                                             #
###############################################################

# ToolLists_MC = {
  # 'top'     : ['TupleToolEventInfo']
  # ,'K0S'    : ['TupleToolMCBackgroundInfo']
  # ,'Lam0'   : ['TupleToolMCBackgroundInfo']
  # ,'DSt'    : ['TupleToolMCBackgroundInfo']
  # ,'D0'     : ['TupleToolMCBackgroundInfo']
  # ,'Jpsi'   : ['TupleToolMCBackgroundInfo']
  # ,'Lamc'   : ['TupleToolMCBackgroundInfo']
  # ,'Lamb'   : ['TupleToolMCBackgroundInfo']
  # ,'Sigcpp' : ['TupleToolMCBackgroundInfo']
  # ,'Sigc0'  : ['TupleToolMCBackgroundInfo']
  # ,'sPi'    : []
  # ,'Bach'   : []
  # ,'K'      : []
  # ,'Pi'     : []
  # ,'P'      : []
  # ,'Mu'     : []
  # }
# MCToolLists = {
  # 'top'     : []
  # ,'K0S'    : ['MCTupleToolHierarchy', 'MCTupleToolKinematic']
  # ,'Lam0'   : ['MCTupleToolHierarchy', 'MCTupleToolKinematic']
  # ,'DSt'    : ['MCTupleToolHierarchy', 'MCTupleToolKinematic']
  # ,'D0'     : ['MCTupleToolHierarchy', 'MCTupleToolKinematic']
  # ,'Jpsi'   : ['MCTupleToolHierarchy', 'MCTupleToolKinematic']
  # ,'Lamc'   : ['MCTupleToolHierarchy', 'MCTupleToolKinematic']
  # ,'Lamb'   : ['MCTupleToolHierarchy', 'MCTupleToolKinematic']
  # ,'Sigcpp' : ['MCTupleToolHierarchy', 'MCTupleToolKinematic']
  # ,'Sigc0'  : ['MCTupleToolHierarchy', 'MCTupleToolKinematic']
  # ,'sPi'    : ['MCTupleToolHierarchy', 'MCTupleToolKinematic']
  # ,'Bach'   : ['MCTupleToolHierarchy', 'MCTupleToolKinematic']
  # ,'K'      : ['MCTupleToolHierarchy', 'MCTupleToolKinematic']
  # ,'Pi'     : ['MCTupleToolHierarchy', 'MCTupleToolKinematic']
  # ,'P'      : ['MCTupleToolHierarchy', 'MCTupleToolKinematic']
  # ,'Mu'     : ['MCTupleToolHierarchy', 'MCTupleToolKinematic']

__all__=("TupleVars", "EvtTupleVars",
         "CalibToolConf", "EvtCalibToolConf",
         "DecayNames", "ToolLists", "TriggerLines")#, "ToolLists_MC", "MCToolLists")
