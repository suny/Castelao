###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
## PIDCalib/sPlotNeutral.py                                                         ##
##                                                                           ##
##  Produces and stores the sWeights for the PIDCalib samples.               ##
############################################################################### 

import ROOT
ROOT.gROOT.SetBatch()

from multiprocessing import Pool

from ROOT import RooRealVar, RooDataHist, RooArgList
from PidCalibProduction import StandardOfflineRequirements as StdCut

from fitModels import D2EtapPi as D2EtapPi
from fitModels import Dst2D0PiM as Dst2D0PiM
from fitModels import Dst2D0PiR as Dst2D0PiR
from fitModels import Dsst2DsGamma as Dsst2DsGamma
from fitModels import Eta2MuMuGamma as Eta2MuMuGamma
from fitModels import Bs2PhiGamma as Bs2PhiGamma
from fitModels import B2KstGamma as B2KstGamma


################################################################################
## Configuration (default)                                                    ##
################################################################################
class default_config:
  def __init__ (s):
    s.monitoring_dir = "./www"
    s.monitoring_key = ""
    s.inputfiles = [ ]
    s.outputdir  = "."

    s.samples = {
      #============ Photons - Pi0s  ===============================#
      'D2EtapPi' :  {
         'CUT'             : StdCut.D2EtapPiCut,
         ### WARNING! This cut must be sync with histograms!
         'HEAD'            : "[D+]cc",
         'FITVARS'         : "DTF_FUN(M,False, strings(['eta_prime']))",
         'HISTOGRAM'       : ["Hlt2CaloPIDD2EtapPiTurboCalib/D2EtapPi"],
         'FITMODEL'        : D2EtapPi,
      },
      

      'D02KPiPi0Resolved' :  {
         'CUT'             : StdCut.Dst2D0PiRCut,
         ### WARNING! This cut must be sync with histograms!
         'HEAD'            : "[D*(2010)+]cc",
         'FITVARS'         : "M-M1",
         'HISTOGRAM'       : ["Hlt2CaloPIDDstD02KPiPi0_ResolvedPi0TurboCalib/D02KPiPi0R"],
         'FITMODEL'        : Dst2D0PiR,
      },

      'D02KPiPi0Merged' :  {
         'CUT'             : StdCut.Dst2D0PiMCut,
         ### WARNING! This cut must be sync with histograms!
         'HEAD'            : "[D*(2010)+]cc",
         'FITVARS'         : "M-M1",
         'HISTOGRAM'       : ["Hlt2CaloPIDDstD02KPiPi0_MergedPi0TurboCalib/D02KPiPi0M"],
         'FITMODEL'        : Dst2D0PiM,
      },

      'Dsst2DsGamma' :  {
         'CUT'             : StdCut.Dsst2DsGammaCut,
         ### WARNING! This cut must be sync with histograms!
         'HEAD'            : "[D*_s+]cc",
         'FITVARS'         : "M-CHILD(1,M)",
         'HISTOGRAM'       : ["Hlt2CaloPIDDsst2DsGammaTurboCalib/Dsst2DsGamma"],
         'FITMODEL'        : Dsst2DsGamma,
      },

      'Eta2MuMuGamma' :  {
         'CUT'             : StdCut.Eta2MuMuGammaCut,
         ### WARNING! This cut must be sync with histograms!
         'HEAD'            : "eta",
         'FITVARS'         : "M",
         'HISTOGRAM'       : ["Hlt2CaloPIDEta2MuMuGammaTurboCalib/Eta2MuMuGamma"],
         'FITMODEL'        : Eta2MuMuGamma,
       },

      'B2KstGamma' :  {
         'CUT'             : StdCut.B2KstGammaCut,
         ### WARNING! This cut must be sync with histograms!
         'HEAD'            : "[B0]cc",
         'FITVARS'         : "M",
         'HISTOGRAM'       : ["Hlt2CaloPIDBd2KstGammaTurboCalib/B2KstGamma"],
         'FITMODEL'        : B2KstGamma,
      },

      'Bs2PhiGamma' :  {
         'CUT'             : StdCut.Bs2PhiGammaCut,
         ### WARNING! This cut must be sync with histograms!
         'HEAD'            : "B_s0",
         'FITVARS'         : "M",
         'HISTOGRAM'       : ["Hlt2CaloPIDBs2PhiGammaTurboCalib/Bs2PhiGamma"],
         'FITMODEL'        : Bs2PhiGamma,
      },

      
    }
    print "Cross-check."
    for sample in s.samples.keys():
      print sample, type ( s.samples[sample]['FITMODEL'])


################################################################################
## Step 1: Loads histograms from file                                         ##
################################################################################
def loadHistograms ( config ):
  fBuf = ROOT.gDirectory.CurrentDirectory()
  for samplename in config.samples: 
    _cfg = config.samples [ samplename ]
    hist = None
    for fileName in config.inputfiles:
      file = ROOT.TFile.Open(fileName)
      if file:
        for histname in _cfg['HISTOGRAM']:
          loadedHisto = file . Get ( histname ) 
          if loadedHisto:
            if hist:
              hist.Add ( loadedHisto )
            else:
              fBuf.cd()
              hist = loadedHisto.Clone ( loadedHisto.GetName() + "Clone" )

#          else:
#            print ( "WARNING! Cannot load histogram " + histname +
#                              " from file " + fileName )
      else:
        raise Exception ( "Cannot access file " + fileName )
          
      file.Close()

    if not hist :
      print ( "Histogram for sample " + samplename  + " unavailable." )
      config.samples [ samplename ] = None
      
      #raise Exception ( "Histogram for sample " + samplename  +
      #                  " unavailable." )
 
    _cfg['HISTOGRAM'] = hist

  config.samples = {i:config.samples[i] for i in config.samples if config.samples[i] != None}
  print config.samples



################################################################################
## Step 2: fits the histograms, returns dict of pdfs                          ##
################################################################################
def fithistos ( config ):
  try:
    pidreport = file ( config.monitoring_dir + 
                       "/000-PIDREPORT-"+config.monitoring_key+".html" 
                     , "w")
  except:
    raise Exception ( "Could not create report file. Please make sure ./www directory exists" )

  pidreport . write  ( "<HTML><HEAD><TITLE>PID report</TITLE></HEAD><BODY><TABLE width = 100%>" )
  for samplename in sorted(config.samples) : 
    _cfg = config.samples [ samplename ]
  
    hist = _cfg [ 'HISTOGRAM' ]
    if 'PREPROCESSING' in _cfg:
      _cfg['PREPROCESSING'] ( hist )

    xmin = ymin = -1e10000
    xmax = ymax =  1e10000
    if 'BOUNDARIES' in _cfg and 'x' in _cfg['BOUNDARIES']:
      xmin = _cfg['BOUNDARIES']['x'][0]
      xmax = _cfg['BOUNDARIES']['x'][1]

    if 'BOUNDARIES' in _cfg and 'y' in _cfg['BOUNDARIES']:
      ymin = _cfg['BOUNDARIES']['y'][0]
      ymax = _cfg['BOUNDARIES']['y'][1]



    if type(hist) == type ( ROOT.TH1D() ):
      xmin = max(xmin,hist.GetXaxis().GetXmin())
      xmax = min(xmax,hist.GetXaxis().GetXmax())
    elif type(hist) == type ( ROOT.TH2D() ):
      xmin = max(xmin,hist.GetXaxis().GetXmin())
      xmax = min(xmax,hist.GetXaxis().GetXmax())
      ymin = max(ymin,hist.GetYaxis().GetXmin())
      ymax = min(ymax,hist.GetYaxis().GetXmax())
    else:
      raise Exception("Invalid histogram type. Only TH1D and TH2D are currently"
                      " implemented.")


#    print "Boundaries: ",xmin,xmax,ymin,ymax
#    exit(1)

    dataset = None
    vars = [ RooRealVar ( "x", hist.GetXaxis().GetTitle(), 
                               xmin, xmax )
           ]
    vars[0].setBins ( hist.GetNbinsX() )

    if type(hist) == type ( ROOT.TH1D() ):
      pass
    elif type(hist) == type ( ROOT.TH2D() ):
      vars += [RooRealVar ( "y", hist.GetYaxis().GetTitle(),
                                 ymin, ymax )
              ]
      vars[1].setBins ( hist.GetNbinsY() )



    ## dataset

    if type(hist) == type ( ROOT.TH1D() ):
      dataset = RooDataHist ( "roo" +  hist.GetName(), "", 
                              RooArgList(vars[0]), hist )
    else:
      dataset = RooDataHist ( "roo" +  hist.GetName(), "", 
                              RooArgList(vars[0], vars[1]), hist )



    fitter = _cfg['FITMODEL'] ( vars, dataset )
    fitter.fit()
    _cfg['FITRESULT'] = (
      fitter.report(config.monitoring_dir, config.monitoring_key+samplename, _cfg['CUT']))
    
    res = _cfg['FITRESULT']

    pidreport.write ( """
      <TR><TD><B>{sample}</B>
          <TD>Fit model: {model}
          <TD>Fit status: <B><FONT color={color}>{status}</FONT>
          <TD>&chi;<sup>2</sup>: {chi2}
          <TD>Candidates: {candidates:.3f} &times; 10<sup>6</sup>
          <TD><A href="{link}">More...</A>
      """.format(
      sample     = samplename,
      model      = str(fitter.__class__.__name__).replace("<", "").replace(">", ""),
      status     = res['STATUS'],
      color      = "#00CC00" if res['STATUS'] == 'SUCCESS' else "#FF0000",
      chi2       = ["{:.2f}".format ( x ) for x in res['CHI2'] ],
      candidates = res['CANDIDATES'] * 1.e-6,
      link       = res['HTMLREPORT']
      ))

  pidreport.write ( "</TABLE></HTML>" )
    
    

################################################################################
## Step 3: defines an sPlot relation table and stores it                      ##
################################################################################

def _computesWeight ( vars, categories, covmat, lambdaLoader ):
  denominator = 0.
  sumOfsWeights = 0.
  covmatLine = {}
  for specie1 in categories:
    denominator +=  ( categories[specie1].getYield() *  
                      categories[specie1].evalNormalizedPdf ( vars ) ) 




  for specie1 in categories:
    numerator   = 0.
    covmatLine [ specie1 ] = 0
    for specie2 in categories:
      numerator   += ( covmat[specie1][specie2] *
                        categories[specie2].evalNormalizedPdf ( vars ) ) 

      covmatLine [ specie1 ] += covmat[specie1][specie2]

    if numerator == 0 and denominator == 0:
      lambdaLoader ( specie1 , 0. )
    elif denominator == 0:
      return False
    else:
      lambdaLoader ( specie1 , numerator / denominator )
      sumOfsWeights += numerator / denominator


#  for specie1 in categories:
#    print ("CovmatLine for " +specie1+ " is: " + str(covmatLine[specie1]) +
#          ", should be: " + str(categories[specie1].getYield()) )
#   
#  print "SUM OF WEIGHTS SHOULD BE 1.0, IT IS: " + str(sumOfsWeights)

  return True;


def createsPlotTable (config):
  for samplename in config.samples: 
    _cfg = config.samples [ samplename ]
    _cfg['SPLOTTABLES'] = {}
    tables = _cfg['SPLOTTABLES']
    categories = _cfg['FITRESULT']['CATEGORIES'];
    for category in categories:
      comp = categories [ category ]
      tables [ category ] = _cfg [ 'HISTOGRAM' ].Clone ( "sTable" + category )
      table = tables [ category ]
      table . Reset()
      fitRes = _cfg['FITRESULT']

    if type(table) == type ( ROOT.TH1D() ):
      for iBin in range ( 1,  _cfg ['HISTOGRAM'].GetNbinsX() + 1):
        x = table.GetBinCenter ( iBin )
        _computesWeight ( [x] , categories, fitRes['COVMATRIX'],
            lambda specie, sWeight : tables [specie].SetBinContent( iBin , sWeight ))
        
    elif type(table) == type ( ROOT.TH2D() ):
      for iBin in range ( 1,  _cfg ['HISTOGRAM'].GetNbinsX() + 1):
        for jBin in range ( 1,  _cfg ['HISTOGRAM'].GetNbinsY() + 1):
          x = table.GetXaxis().GetBinCenter ( iBin )
          y = table.GetYaxis().GetBinCenter ( jBin )
          ok = _computesWeight ( [x,y] , categories, fitRes['COVMATRIX'],
              lambda specie, sWeight : tables [specie].SetBinContent( iBin, jBin, sWeight ))
          if ok == False:
            print "Undefined value for bin: ", [iBin, jBin], " corresponding to ", [x,y]
          


          
      

################################################################################
## Step 4: saves and archives the splot tables (can be used on the grid)      ##
################################################################################
def savesAndArchivesTheSplotTables ( config ):
  print 80*'#'
  print "Creating file {outdir}/sPlotTables-{key}.root".format(
      outdir = config.outputdir,
      key    = config.monitoring_key )

  archive = ROOT.TFile.Open ( 
    "{outdir}/sPlotTables-{key}.root".format(
      outdir = config.outputdir,
      key    = config.monitoring_key ), 
    "RECREATE")

  for samplename in config.samples: 
    _cfg = config.samples [ samplename ]
    _dir = archive.mkdir ( samplename , samplename );
    _dir.cd()

    _headSelection = ROOT.TNamed ( "HeadSelection" ,
        "(DECTREE ( ' {head} ' ))".format ( head = _cfg['HEAD'] ))
    _headSelection.Write()

    _filter = ROOT.TNamed ( "Filter" , _cfg ['CUT'] )
    _filter.Write()

    vars = _cfg['FITVARS'] . split( ':' )
    for tabName in _cfg['SPLOTTABLES']:
      table = _cfg['SPLOTTABLES'][tabName]
      if type(table) == type(ROOT.TH1D()):
        table.SetTitle(";" + vars[0])
      elif type(table) == type(ROOT.TH2D()):
        table.SetTitle(";" + vars[0] + ";" + vars[1])
      else:
        raise Exception("Only 1D and 2D maps are currently implemented")

      table.Write()
     
    archive.cd()
  
  archive.Close()
  



## Make the full job
def sPlot ( config ):
  ##############################################################################
  ## Step 1: load histograms                                                  ##
  loadHistograms ( config )
  ## Step 2: fits the histograms, returns dict of pdfs                        ##
  fithistos ( config )
  ## Step 3: defines an sPlot relation table and stores it                    ##
  createsPlotTable ( config )
  ## Step 4: saves and archives the splot tables (can be used on the grid)    ##
  savesAndArchivesTheSplotTables ( config )

  ##############################################################################

  for samplename in config.samples: 
    _cfg = config.samples [ samplename ]
    print samplename, _cfg['HISTOGRAM'].GetSumOfWeights() 


if __name__ == "__main__":
  sPlot ( default_config() )

