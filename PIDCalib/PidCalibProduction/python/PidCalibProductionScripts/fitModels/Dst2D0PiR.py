###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from interface import interface
from component import component

import ROOT
from ROOT import gROOT
fitmodel_name = "RooEGE"
fitmodel_name2 = "RooThrExp"
import os
fitmodels_dir = os.path.dirname(os.path.abspath(__file__))
fitmodel_filename = ".x " + fitmodels_dir + "/" + fitmodel_name + ".cxx+"
fitmodel_filename2 = ".x " + fitmodels_dir + "/" + fitmodel_name2 + ".cxx+"
gROOT.ProcessLine(fitmodel_filename)
gROOT.ProcessLine(fitmodel_filename2)

from ROOT import RooEGE, RooThrExp

from ROOT import RooRealVar, RooArgList, RooAddPdf, RooChebychev, RooAddPdf, RooGaussian, RooFormulaVar


class Dst2D0PiR(interface):
    def fit(self):
        x = self.vars[0]

        #Param init-------------

        _ntot = self.datahist.sum(False)

        _nsig = _ntot * 0.68
        _mean = 145
        _sigma = 0.77

        _aL = 1.12638e+00;
        _aR = -9.26905e-01;

        _nbkg = _ntot * 0.32
        _a = -9.65046e-02
        _w = 1.28820e+00

        #Signal-------------
        # is a double crystal ball with fixed tail parameters except right-hand slope

        mean = RooRealVar("mean", "mean", _mean, _mean-1, _mean+1)
        sigma = RooRealVar("sigma", "sigma",_sigma,_sigma*0.5,_sigma*2)

        aL = RooRealVar("aL", "aL", _aL, 0.5, 3.)
        aR = RooRealVar("aR", "aR", _aR, -3, -0.5)

        aL.setConstant();
        aR.setConstant();

        pdfSig = RooEGE("pdfSig", "pdfSig", x, mean, sigma, aL, aR)
        nSig = RooRealVar("nSig", "nSig", _nsig, _nsig * 0.5, _nsig * 2)

        #Bkg----------------

        w = RooRealVar("w", "w", _w, _w*0.5, _w*2);
        a = RooRealVar("a", "a", _a, _a*2, _a*0.5);

        w.setConstant();

        fm = RooFormulaVar("fm","fm","mean+w*log(-a*w/(1+a*w))",RooArgList(mean,w,a));

        pdfBkg = RooThrExp("pdfBkg","pdfBkg",x,fm,w,a);
        nBkg = RooRealVar("nBkg", "nBkg", _nbkg, _nbkg * 0.5, _nbkg * 2)

        #Model----------------
        
        self.model = RooAddPdf("model", "model", RooArgList(pdfBkg, pdfSig),RooArgList(nBkg, nSig))

        #Fit------------------

        self.fitResult = self.model.fitTo(self.datahist,ROOT.RooFit.Save(True))

        self.components = {
            'Signal': component(pdfSig, nSig),
            'Background': component(pdfBkg, nBkg)
        }

        self.saveFromGarbage = [x, pdfSig, pdfBkg, nSig, nBkg]
        self.saveFromGarbage += [a, fm, w]
        self.saveFromGarbage += [mean, sigma, aL, aR]

        return self.fitResult
