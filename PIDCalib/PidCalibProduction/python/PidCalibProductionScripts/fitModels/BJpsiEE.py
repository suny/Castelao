###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from interface import interface
from component import component
import fitUtils

import ROOT

from ROOT import RooGaussian, RooRealVar, RooPolynomial, RooArgList, RooAddPdf, RooChebychev
from ROOT import RooRealVar, RooGaussian, RooChebychev, RooArgList, RooCBShape
from ROOT import RooAddPdf, RooProdPdf, RooArgusBG, RooExponential

Minimizer = ROOT.RooFit.Minimizer
Save = ROOT.RooFit.Save

class BJpsiEE (interface):
  def fit(self):
    x = self.vars[0]
    y = self.vars[1]

    nTot = self.datahist.sum(False)


    # Define here the structure of the fit: variables and pdfs.
    meanX  = RooRealVar  ( "meanX", "meanX", 5280, 5200, 5300 )
    sigmaX = RooRealVar  ( "sigmaX", "sigmaX", 20, 10, 150 )
    gausX  = RooGaussian ( "gausX", "gausX", x, meanX, sigmaX )

    cbsigmaX = RooRealVar ( "cbsigmaX", "cbsigmaX" , 40, 30, 150.0)
    nX = new = RooRealVar ( "nX", "nX", 1.15304, 0., 10)
    alphaX   = RooRealVar ( "alphaX", "alphaX", 2.3, -3, 3)
    cballX   = RooCBShape ( "cballX", "cballX", x, meanX, cbsigmaX, alphaX, nX);

    sigFracX = RooRealVar ( "sigFracX", "sigFracX", 0.8, 0., 1.)
    sigX     = RooAddPdf ( "sigX", "Signal X",gausX, cballX, sigFracX)



#    bkg0X  = RooRealVar  ( "bkg0X", "bkg0X", -1.1, -2, 0)
    bkg0X  = RooRealVar  ( "bkg0X", "bkg0X", -2e-3, -5e-3, 5e-4)
    bkg1X  = RooRealVar  ( "bkg1X", "bkg1X", -0.3, -1, 0)
#    bkgX   = RooChebychev( "bkgX",  "bkgX",  x, ROOT.RooArgList ( bkg0X, bkg1X ))
    bkgX   = RooExponential ( "bkgX",  "bkgX",  x,  bkg0X)

    m0Prc  = RooRealVar  ( "prc0X", "prc0X", 5063, 5000, 5200)
    c_Prc  = RooRealVar  ( "c_Prc", "c_Prc", -5, -5, 0)
    prcX   = RooArgusBG  ( "prcX",  "prcX",  x, m0Prc, c_Prc )

    meanY  = RooRealVar  ( "meanY", "meanY", 3065, 3050, 3080 )
    sigmaY = RooRealVar  ( "sigmaY", "sigmaY", 40, 30, 80 )
    gausY  = RooGaussian ( "gausY", "gausY", y, meanY, sigmaY )
    gausPrc= RooGaussian ( "gausPrc", "gausPrc", y, meanY, sigmaY )

    bkg0Y  = RooRealVar  ( "bkg0Y", "bkg0Y", 0.35, 0, 1)
    bkg1Y  = RooRealVar  ( "bkg1Y", "bkg1Y", -0.1, -.5, .1)
    bkgY   = RooChebychev( "bkgY",  "bkgY",  y, ROOT.RooArgList ( bkg0Y, bkg1Y ))

    # Define one normalization parameter per component that you need.
    # Make sure that the sum of the initial values is nTot.
    nSig  = RooRealVar  ( "nSig", "nSig", 0.1*nTot, 0, nTot )
    nBkg  = RooRealVar  ( "nBkg", "nBkg", 0.8*nTot, 0, nTot )
    nPrc  = RooRealVar  ( "nPrc", "nPrc", 0.1*nTot, 0, nTot )

    # In C++ you usually "delete" objects you don't want any longer.
    # In python you have to mark those you don't want to loose.
    # List here all the objects useful to the fit
    self.saveFromGarbage += [ meanX, sigmaX, gausX, bkg0X, bkg1X, bkgX ]
    self.saveFromGarbage += [ cbsigmaX, nX, alphaX, cballX, sigX, sigFracX ]
    self.saveFromGarbage += [ meanY, sigmaY, gausY, bkg0Y, bkg1Y, bkgY ]
    self.saveFromGarbage += [ m0Prc, c_Prc, prcX, gausPrc, nPrc]

    sig = RooProdPdf ( "signal",  "signal", RooArgList ( sigX, gausY ) )
    bkg = RooProdPdf ( "bkg",     "bkg",    RooArgList ( bkgX,  bkgY  ) )
    pRc = RooProdPdf ( "pRc",     "pRc",    RooArgList ( prcX,  gausPrc  ) )

    simpleModelY = RooAddPdf ( "simpleModelY", "", RooArgList ( gausY, bkgY ), RooArgList( nSig, nBkg ) )

    # Don't forget to list again pdfs and their normalizations here.
    self.model = RooAddPdf ( "model", "model", RooArgList ( sig, bkg, pRc ) , RooArgList ( nSig, nBkg, nPrc ))
    yPars = [bkg0Y , bkg1Y , meanY , nBkg  , nSig  , sigmaY ]

    for par in yPars: par.setConstant(True)
    simpleModelY.fitTo ( self.datahist, ROOT.RooFit.Save(True) )
    for par in yPars: par.setConstant(False)

#    self.fitResult = self.model.fitTo ( self.datahist, Save(True), Minimizer("Minuit", "Simplex") )
#    self.fitResult = self.model.fitTo ( self.datahist, Save(True) )
    self.fitResult = fitUtils.fit ( self.datahist, self.model , "Minimize" ) 

    # Don't forget to list again pdfs and their normalizations here.
    self.components = {
      'Signal'       :  component ( sig , nSig, forecolor = ROOT.kRed   ),
      'Background'   :  component ( bkg , nBkg, forecolor = ROOT.kBlack ),
      'PartReco'     :  component ( pRc , nPrc, forecolor = ROOT.kGreen ),
    }

    return self.fitResult

    

