###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import ROOT
from interface import interface
from component import component
import fitUtils 

from ROOT import (RooGaussian, RooRealVar, RooPolynomial, RooArgList, 
                  RooAddPdf, RooChebychev, RooAddPdf, RooConstVar )

class Lambda0_adjusted (interface):
  def fit(self):
    x = self.vars[0]
    mean = {}; sigma = {}; gaus = {}

    s0 = [2.5, 4.9, 1.4]
    for iGaus in [str(i) for i in range(0,3)]:
      m = RooRealVar  ("mean"+iGaus,  "mean"+iGaus,  1116, 1114, 1118)
#      s = RooRealVar  ("sigma"+iGaus, "sigma"+iGaus, 4.5,  0.,  10.)
      s = RooRealVar  ("sigma"+iGaus, "sigma"+iGaus, s0[int(iGaus)],  0.,  10.)
      gaus[iGaus]  = RooGaussian ("gaus"+iGaus,  "gaus"+iGaus,  x, m, s)
      mean[iGaus] = m; sigma[iGaus] = s;


#    cf1 = RooRealVar ( "cf1", "cf1", 1.0, 0, 1.)
#    cf2 = RooRealVar ( "cf2", "cf2", 0.0, 0, 1.)
#    cf1 = RooRealVar ( "cf1", "cf1", 0.52, 0, 1.)
#    cf2 = RooRealVar ( "cf2", "cf2", 0.32, 0, 1.)
    cf1 = RooConstVar ( "cf1", "cf1", 0.52)
    cf2 = RooConstVar ( "cf2", "cf2", 0.32)

    signal = RooAddPdf ("L0signal", "", RooArgList ( gaus['0'], gaus['1'], gaus['2'] ),
                                      RooArgList ( cf1, cf2 ) )


#    bg0 = RooRealVar("bg0", "bg0", 0., -1, 1)
    bg0 = RooRealVar("bg0", "bg0", -0.0736, -0.2, 0.2)
#    bg0 = RooConstVar("bg0", "bg0", -0.08)

    bkg = RooChebychev ( "bkg", "bkg", x, RooArgList(bg0))

    nTot = self.datahist.sum(False)
#    nSig = RooRealVar ( "nSig", "nSig", 0.8 * nTot, 0, nTot)
#    nBkg = RooRealVar ( "nBkg", "nBkg", 0.2 * nTot, 0, nTot)
    nSig = RooRealVar ( "nSig", "nSig", 0.55 * nTot, 0, nTot)
    nBkg = RooRealVar ( "nBkg", "nBkg", 0.45 * nTot, 0, nTot)


    self.model = RooAddPdf ( "model", "model", RooArgList(signal, bkg) , RooArgList (nSig, nBkg) )

    self.fitResult = fitUtils.fit ( self.datahist, self.model , "Minimize" ) 

    tfSig = signal.asTF ( RooArgList ( x ) )
    tfBkg = bkg.asTF ( RooArgList ( x ) )

    self.components = {
      'Signal'      :  component ( signal , nSig, tfSig),
      'Background'  :  component ( bkg    , nBkg, tfBkg),
    }

    items = lambda x: [ x[i] for i in x ];
    self.saveFromGarbage =( [x, signal, cf1, cf2, bg0, bkg, nSig, nBkg] 
                            + items(gaus) + items(sigma) + items(mean) )

    return self.fitResult

