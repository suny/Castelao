###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from interface import interface
from component import component

import ROOT

from ROOT import RooGaussian, RooRealVar, RooPolynomial, RooArgList, RooAddPdf, RooChebychev
Minimizer = ROOT.RooFit.Minimizer

class Ks0 (interface):
  def fit(self):
    x = self.vars[0]
    # Define here the structure of the fit: variables and pdfs. 
    mean   = RooRealVar  ( "mean"  , "mean"  , 497.6, 495, 500.0 )
    sigma1 = RooRealVar  ( "sigma1", "sigma1", 5.5, 0.5, 7 )
    sigma2 = RooRealVar  ( "sigma2", "sigma2", 2.5, 0.5, 4 )
    sigma3 = RooRealVar  ( "sigma3", "sigma3", 1.0, 0.2, 3 )
    gausf1 = RooRealVar  ( "gausf1", "gausf1", 0.5, 0, 1 )
    gausf2 = RooRealVar  ( "gausf2", "gausf2", 0.0, 0, 1 )
    gaus1  = RooGaussian ( "gaus1" , "gaus1" , x, mean, sigma1 )
    gaus2  = RooGaussian ( "gaus2" , "gaus2" , x, mean, sigma2 )
    gaus3  = RooGaussian ( "gaus3" , "gaus3" , x, mean, sigma3 )
    sig    = RooAddPdf   ( "sig"   , "sig"   , RooArgList(gaus1, gaus2, gaus3), RooArgList(gausf1, gausf2) ) 

    bkg0  = RooRealVar  ( "bkg0", "bkg0", 0, -1, 1)
    bkg1  = RooRealVar  ( "bkg1", "bkg1", 0, -1, 1)
    bkg   = RooChebychev( "bkg",  "bkg",  x, ROOT.RooArgList ( bkg0, bkg1 ))

    nTot = self.datahist.sum(False)
    nSig  = RooRealVar  ( "nSig", "nSig", 0.75*nTot, 0, nTot )
    nBkg  = RooRealVar  ( "nBkg", "nBkg", 0.25*nTot, 0, nTot )
    
    # In C++ you usually "delete" objects you don't want any longer.
    # In python you have to mark those you don't want to loose. 
    # List here all the objects useful to the fit
    self.saveFromGarbage += [ mean, sigma1, sigma2, sigma3, 
                              gaus1, gaus2, gaus3, gausf1, gausf2, 
                              sig, bkg0, bkg1, bkg ]

    # List here the components through the "component" class. 
    # You are very welcome to add new components, the only requirement
    # is that there has to be one and only one signal component named
    # 'Signal'.

    # Don't forget to list again pdfs and their normalizations here.
    self.model = RooAddPdf ( "model", "model", RooArgList ( sig, bkg ) , RooArgList ( nSig, nBkg ))
  
    self.fitResult = self.model.fitTo ( self.datahist, ROOT.RooFit.Save(True), Minimizer("Minuit2", "Migrad"))
    self.fitResult = self.model.fitTo ( self.datahist, ROOT.RooFit.Save(True), Minimizer("Minuit", "Simplex"))

#    tfSig = gaus.asTF ( RooArgList ( x ) )
#    tfBkg = bkg.asTF  ( RooArgList ( x ) )

    self.components = {
      'Signal'       :  component ( sig  , nSig, forecolor = ROOT.kRed   ),
      'Background'   :  component ( bkg  , nBkg, forecolor = ROOT.kBlack ),
    }
    

    return self.fitResult
    

    

