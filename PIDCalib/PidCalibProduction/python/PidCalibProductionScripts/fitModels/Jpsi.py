###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from interface import interface
from component import component

import ROOT

from ROOT import RooCBShape, RooRealVar, RooPolynomial, RooArgList, RooAddPdf, RooChebychev, RooAddPdf

Minimizer = ROOT.RooFit.Minimizer
class Jpsi (interface):
  def fit(self):
    x = self.vars[0]
    mean1 = RooRealVar ("mean1", "mean1", 3096, 3085, 3110)
    sigma1 = RooRealVar ("sigma1", "sigma1",  15, 1.0, 30)
    alpha1 = RooRealVar ("alpha1", "alpha1", 0.5,  .0, 2.)
    n1 = RooRealVar ("n1", "n1", 2, 1.0, 5)
    cb1 = RooCBShape("cb1", "cb1", x, mean, sigma, alpha, n)

    mean2 = RooRealVar ("mean2", "mean2", 3096, 3085, 3110)
    sigma2 = RooRealVar ("sigma2", "sigma2", 15, 1.0, 30)
    alpha2 = RooRealVar ("alpha2", "alpha2", 0.5, .0, 2.)
    n2 = RooRealVar ("n2", "n2", 2, 1.0, 5)
    cb2 = RooCBShape("cb2", "cb2", x, mean, sigma, alpha, n)

    cf = RooRealVar ( "cf", "cf", 1, 0, 1)
    cb = RooAddPdf  ( "cb", "cb", RooArgList( cb1, cb2 ), RooArgList (cf))

    bg0 = RooRealVar("bg0", "bg0", 0, -100, 100)
    bg1 = RooRealVar("bg1", "bg1", 0, -100, 100)
    bg2 = RooRealVar("bg2", "bg2", 0, -1, 1)

    bkg = RooChebychev ( "bkg", "bkg", x, RooArgList(bg0, bg1) )
    
    
    nTot = self.datahist.sum(False)
    nSig = RooRealVar ( "nSig", "nSig", 0.8 * nTot, 0, nTot)
    nBkg = RooRealVar ( "nBkg", "nBkg", 0.2 * nTot, 0, nTot)


    self.model = RooAddPdf ( "model", "model", RooArgList(cb, bkg) , RooArgList (nSig, nBkg) )
    self.fitResult = self.model.fitTo ( self.datahist, ROOT.RooFit.Save(True) )
    self.fitResult = self.model.fitTo ( self.datahist, ROOT.RooFit.Save(True), Minimizer("Minuit", "Simplex" ))

    tfSig = cb.asTF ( RooArgList ( x ) )
    tfBkg = bkg.asTF ( RooArgList ( x ) )

    self.components = {
      'Signal'      :  component ( cb   , nSig, tfSig),
      'Background'  :  component ( bkg  , nBkg, tfBkg),
    }
    

    self.saveFromGarbage =  [x, cf, cb, bg0, bg1, bg2, bkg, nSig, nBkg]
    self.saveFromGarbage += [mean1, sigma1, alpha1, n1, cb1]
    self.saveFromGarbage += [mean2, sigma2, alpha2, n2, cb2]

    return self.fitResult
    
      

    
  
    
