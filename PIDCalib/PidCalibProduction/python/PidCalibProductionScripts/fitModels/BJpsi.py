###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from interface import interface
from component import component
import fitUtils

import ROOT

from ROOT import RooGaussian, RooRealVar, RooPolynomial, RooArgList, RooAddPdf, RooChebychev
from ROOT import RooRealVar, RooGaussian, RooChebychev, RooArgList, RooCBShape
from ROOT import RooAddPdf, RooProdPdf, RooArgusBG, RooConstVar

class BJpsi (interface):
  def fit(self):
    x = self.vars[0]
    y = self.vars[1]

    nTot = self.datahist.sum(False)

#   1  alpha 1X     3.98140e+00   1.60647e-05   3.48947e-03   1.47433e+00
#   2  alpha 2X    -2.30796e+00   7.99193e-05   1.57340e-03  -8.77774e-01
#   3  bkg0X        3.41367e-01   1.99898e-03   6.40257e-06   3.41368e-03
#   4  bkg0Y        7.25314e-02   2.34180e-03   7.66954e-06   7.25314e-04
#   5  bkg1X       -3.25839e-01   2.14401e-03   6.95634e-06  -3.25840e-03
#   6  bkg1Y        2.11528e-01   2.17155e-03   6.91289e-06   2.11528e-03
#   7  c_Prc       -5.00000e+00   constant
#   8  fX           2.82151e-01   1.87513e-05   2.75301e-04  -4.50814e-01
#   9  meanX        5.28000e+03   4.07186e-04   1.53417e-07   6.43476e-01
#  10  meanY        3.09614e+03   1.22590e-03   1.46438e-04  -5.69027e-01
#  11  n cb1X       1.53603e+00   6.22253e-04   5.00000e-02   1.02582e-01
#  12  n cb2X       1.30324e+01   3.09692e-04   2.67436e-02   1.05283e+00
#  13  nBkg         8.87854e+02   7.20707e-02   2.52131e-04  -6.22501e-01
#  14  nPrc         4.08755e+02   5.22364e-02   2.09910e-04  -9.40836e-01
#  15  nSig         3.09987e+03   7.90211e-02   4.62074e-04   4.73138e-01
#  16  prc0X        5.15351e+03   5.80238e-03   2.10596e-04   9.66418e-02
#  17  sigma 1X     8.22496e-03   3.14209e+01   1.17374e-05  -1.54514e+00
#  18  sigma 2X     2.48638e+01   1.04161e-03   2.77610e-04  -5.44763e-03
#  19  sigmaY       1.69452e+01   8.28448e-04   2.04089e-04  -4.88276e-01

    # Define here the structure of the fit: variables and pdfs. 
    meanX  = RooRealVar  ( "meanX", "meanX", 5280, 5250, 5300 )
#    meanX  = RooRealVar  ( "meanX", "meanX", 5280, 5200, 5300 )

    fX = RooRealVar("fX","fX",0.5, 0.1, 0.9)
    sigma_cb1X = RooRealVar("sigma 1X","width of cb1X", 17, 10.0, 30.0)
    sigma_cb2X = RooRealVar("sigma 2X","width of cb2X", 17, 10.0, 30.0)
    alpha_cb1X = RooRealVar("alpha 1X","alpha of cb1X",  1.0,  0, 5.0)
    alpha_cb2X = RooRealVar("alpha 2X","alpha of cb2X", -0.5, -5.0, 0)
    n_cb1X = RooRealVar("n cb1X", "n cb1X", 1.0, 0.1, 10.0)
    n_cb2X = RooRealVar("n cb2X", "n cb2X", 1.0, 0.1, 10.0)
#    n_cb1X = RooConstVar("n cb1X", "n cb1X", 2.)
#    n_cb2X = RooConstVar("n cb2X", "n cb2X", 2.)
    cb1X = RooCBShape("cb1X", "cb1X", x, meanX, sigma_cb1X, alpha_cb1X, n_cb1X)
    cb2X = RooCBShape("cb2X", "cb2X", x, meanX, sigma_cb1X, alpha_cb2X, n_cb2X)
    signalX = RooAddPdf("signal pdf","signal pdf", ROOT.RooArgList(cb1X, cb2X), ROOT.RooArgList(fX))

    meanY  = RooRealVar  ( "meanY", "meanY", 3096, 3092, 3100 )
    fY = RooRealVar("fY","fY",0.5, 0.1, 0.9)
    sigma_cb1Y = RooRealVar("sigma 1Y","width of cb1Y", 12, 5.0, 20.0)
    sigma_cb2Y = RooRealVar("sigma 2Y","width of cb2Y", 12, 5.0, 20.0)
    alpha_cb1Y = RooRealVar("alpha 1Y","alpha of cb1Y", 1.0, 0, 5.0)
    alpha_cb2Y = RooRealVar("alpha 2Y","alpha of cb2Y", -0.50, -5.0, 0)
    n_cb1Y = RooRealVar("n cb1Y", "n cb1Y", 1.0, 0.1, 10.0)
    n_cb2Y = RooRealVar("n cb2Y", "n cb2Y", 1.0, 0.1, 10.0)
#    n_cb1Y = RooConstVar("n cb1Y", "n cb1Y", 2.)
#    n_cb2Y = RooConstVar("n cb2Y", "n cb2Y", 2.)
    cb1Y = RooCBShape("cb1Y", "cb1Y", y, meanY, sigma_cb1Y, alpha_cb1Y, n_cb1Y)
    cb2Y = RooCBShape("cb2Y", "cb2Y", y, meanY, sigma_cb1Y, alpha_cb2Y, n_cb2Y)
    signalY = RooAddPdf("signalY pdf","signalY pdf", ROOT.RooArgList(cb1Y, cb2Y), ROOT.RooArgList(fY))

    bkg0X  = RooRealVar  ( "bkg0X", "bkg0X", -0.3, -.5, .5)
    bkg1X  = RooRealVar  ( "bkg1X", "bkg1X", -.3, -.5, .5)
    bkgX   = RooChebychev( "bkgX",  "bkgX",  x, ROOT.RooArgList ( bkg0X, bkg1X ))

#    m0Prc  = RooRealVar  ( "prc0X", "prc0X", 5150, 5100, 5200)
    m0Prc  = RooConstVar  ( "prc0X", "prc0X", 5150)
    c_Prc  = RooRealVar  ( "c_Prc", "c_Prc", -5, -10, 0)
    prcX   = RooArgusBG  ( "prcX",  "prcX",  x, m0Prc, c_Prc )

#    meanY  = RooRealVar  ( "meanY", "meanY", 3096, 3090, 3120 )
#    meanY  = RooRealVar  ( "meanY", "meanY", 3096, 3092, 3100 )
#    sigmaY = RooRealVar  ( "sigmaY", "sigmaY", 14, 10, 30 )
#    gausY  = RooGaussian ( "gausY", "gausY", y, meanY, sigmaY )
#    gausPrc= RooGaussian ( "gausPrc", "gausPrc", y, meanY, sigmaY )

    bkg0Y  = RooRealVar  ( "bkg0Y", "bkg0Y", 0.07, -.1, .1)
    bkg1Y  = RooRealVar  ( "bkg1Y", "bkg1Y", 0.02, -.1, .1)
    bkgY   = RooChebychev( "bkgY",  "bkgY",  y, ROOT.RooArgList ( bkg0Y, bkg1Y ))

    # Define one normalization parameter per component that you need.
    # Make sure that the sum of the initial values is nTot.
    nSig  = RooRealVar  ( "nSig", "nSig", 0.7*nTot, 0, nTot )
    nBkg  = RooRealVar  ( "nBkg", "nBkg", 0.2*nTot, 0, nTot )
    nPrc  = RooRealVar  ( "nPrc", "nPrc", 0.2*nTot, 0, nTot )
    
    # In C++ you usually "delete" objects you don't want any longer.
    # In python you have to mark those you don't want to loose. 
    # List here all the objects useful to the fit
    self.saveFromGarbage += [ meanX, sigma_cb1X,sigma_cb2X, alpha_cb1X, signalX,alpha_cb2X,n_cb1X, n_cb2X,cb1X, cb2X,fX,bkg0X, bkg1X, bkgX ]
    self.saveFromGarbage += [ meanY, sigma_cb1Y,sigma_cb2Y, alpha_cb1Y, signalY,alpha_cb2Y,n_cb1Y, n_cb2Y,cb1Y, cb2Y,fY,bkg0Y, bkg1Y, bkgY ]
#    self.saveFromGarbage += [ bkg0Y, bkg1Y, bkgY ]
#    self.saveFromGarbage += [ meanY, sigmaY, gausY, bkg0Y, bkg1Y, bkgY ]
    self.saveFromGarbage += [ m0Prc, c_Prc, prcX, nPrc, nSig, nBkg ]

    sig = RooProdPdf ( "signal",  "signal", RooArgList ( signalX, signalY ) )
    bkg = RooProdPdf ( "bkg",     "bkg",    RooArgList ( bkgX,  bkgY  ) )
    pRc = RooProdPdf ( "pRc",     "pRc",    RooArgList ( prcX,  signalY  ) )


    self.model = RooAddPdf ( "model", "model", RooArgList ( sig, bkg, pRc ) , RooArgList ( nSig, nBkg, nPrc ))
    self.fitResult = fitUtils.fit ( self.datahist, self.model ) 

    # Don't forget to list again pdfs and their normalizations here.
    self.components = {
      'Signal'       :  component ( sig , nSig, forecolor = ROOT.kRed   ),
      'Background'   :  component ( bkg , nBkg, forecolor = ROOT.kBlack ),
      'PartReco'     :  component ( pRc , nPrc, forecolor = ROOT.kGreen ),
    }

    return self.fitResult
