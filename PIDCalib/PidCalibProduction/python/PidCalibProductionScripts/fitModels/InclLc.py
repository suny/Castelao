###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from interface import interface
from component import component

import ROOT

from ROOT import RooGaussian, RooRealVar, RooPolynomial, RooArgList, RooAddPdf, RooExponential
from ROOT import RooCBShape, RooConstVar

class InclLc (interface):
  def fit(self):
    x = self.vars[0]

#  EXT PARAMETER                                INTERNAL      INTERNAL  
#  NO.   NAME      VALUE            ERROR       STEP SIZE       VALUE   
#   1  alpha1      -1.12158e+00   1.18446e-02   1.40582e-02   2.51914e+00
#   2  alpha2       1.24592e+00   9.26018e-03   1.41937e-02  -5.61290e-01
#   3  cf1          4.69661e-01   6.97914e-03   4.30097e-02  -6.07156e-02
#   4  mean1        2.28781e+03   5.67438e-03   3.63974e-03  -1.94455e-02
#   5  nBkg         6.74310e+06   5.73591e+03   4.35194e-03   3.20663e-01
#   6  nSig         3.51107e+06   5.46073e+03   3.60684e-03  -3.20657e-01
#   7  sigma1       5.55102e+00   7.25811e-03   7.50169e-03   1.13371e-02
#   8  slope       -2.33077e-04   1.86727e-05   3.25035e-03   1.55714e+00

    LcMassPDG = 2288.;

    mean1 = RooRealVar ("mean1", "mean1", LcMassPDG, LcMassPDG-10, LcMassPDG+10)
    sigma1 = RooRealVar ("sigma1", "sigma1",  5.5, 1., 10.)
    alpha1 = RooRealVar ("alpha1", "alpha1", -1.,  -5., -0.5)
#    n1 = RooRealVar ("n1", "n1", 5, 1, 6)
    n1 = RooConstVar ("n1", "n1", 3.)
    cb1 = RooCBShape("cb1", "cb1", x, mean1, sigma1, alpha1, n1)

    alpha2 = RooRealVar ("alpha2", "alpha2", 1.,  0.5, 5.)
#    n2 = RooRealVar ("n2", "n2", 5, 1, 6)
    n2 = RooConstVar ("n2", "n2", 3.)
    cb2 = RooCBShape("cb2", "cb2", x, mean1, sigma1, alpha2, n2)

    cf1 = RooRealVar ( "cf1", "cf1", .5, 0, 1.)

    signal = RooAddPdf ( "signal", "signal", cb1, cb2, cf1)

#    mean = RooRealVar ("mean", "mean", LcMassPDG, LcMassPDG-10, LcMassPDG+10)
#    sigma = RooRealVar ("sigma", "sigma", 6., 0.0, 10.)
#    gaus = RooGaussian("gaus", "gaus", x, mean, sigma)

    slope = RooRealVar("slope","slope", -0.0, -5., 0.)
    bkg = RooExponential ( "bkg", "bkg", x, slope )

    nTot = self.datahist.sum(False)
    nSig = RooRealVar ( "nSig", "nSig", 0.5 * nTot, 0, nTot)
    nBkg = RooRealVar ( "nBkg", "nBkg", 0.5 * nTot, 0, nTot)

    self.model = RooAddPdf ( "model", "model", RooArgList(signal, bkg) , RooArgList (nSig, nBkg) )
    self.fitResult = self.model.fitTo ( self.datahist, ROOT.RooFit.Save(True) )

    tfSig = signal.asTF ( RooArgList ( x ) )
    tfBkg = bkg.asTF  ( RooArgList ( x ) )

    self.components = {
      'Signal'      :  component ( signal , nSig, tfSig, forecolor = ROOT.kRed),
      'Background'  :  component ( bkg  , nBkg, tfBkg, forecolor = ROOT.kViolet),
    }

    self.saveFromGarbage = [x, mean1, sigma1, alpha1, n1, cb1, alpha2, n2, cb2, cf1, signal, slope, bkg, nSig, nBkg]

    return self.fitResult
