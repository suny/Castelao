###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import ROOT 

class component:
  def __init__ ( self, pdf, yld, tf = None,
                            forecolor = ROOT.kBlack, 
                            fillcolor = ROOT.kWhite, 
                            linestyle = ROOT.kSolid, 
                            fillstyle = 0 ):
    self._pdf = pdf.GetName()
    self._nVars = len(set(pdf.getVariables().contentsString().split(','))
                          & set(["x","y"])) #name of variables
#    self._yld = yld.getVal()
#    self._err = yld.getError()
    self.nFromFit = yld
    self.forecolor = forecolor
    self.fillcolor = fillcolor
    self.linestyle = linestyle
    self.fillstyle = fillstyle
    self._saveFromGarbage =  [ pdf ]

    if self._nVars == 1:
      self._tf  = pdf.createHistogram("x")
      lx = self._tf.GetXaxis().GetBinLowEdge ( 1 )
      nx = self._tf.GetNbinsX()
      ux = self._tf.GetXaxis().GetBinUpEdge  ( nx )
      self.integral = self._tf.Integral("weight")#GetSumOfWeights() * ( ux - lx ) / float(nx)
    elif self._nVars == 2:
      self._tf  = pdf.createHistogram("x,y")
      lx = self._tf.GetXaxis().GetBinCenter ( 1 )
      ly = self._tf.GetYaxis().GetBinCenter ( 1 )
      nx = self._tf.GetNbinsX()
      ny = self._tf.GetNbinsY()
      ux = self._tf.GetXaxis().GetBinCenter ( nx )
      uy = self._tf.GetYaxis().GetBinCenter ( ny )
      self.integral = self._tf.Integral("weight")#GetSumOfWeights() * (ux - lx) * (uy - ly) / float(nx * ny)
      self._tf.SaveAs("DEBUG" + self._pdf + ".root")

  def evalNormalizedPdf ( self, variables ):
    if self._nVars == 1:
      return self._tf.Interpolate ( variables [ 0 ] ) / self.integral;
    elif self._nVars == 2:
      return self._tf.Interpolate ( variables [ 0 ], variables [ 1 ] ) / self.integral;
    return 0;

  def getPdf(self):
    return self._pdf
  def getYield (self):
    return self.nFromFit.getVal()
  def getError (self):
    return self.nFromFit.getError()
    

