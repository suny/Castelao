###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from interface import interface
from component import component

import ROOT

from ROOT import gROOT

fitmodel_name = "RooThrExp"
import os
fitmodels_dir = os.path.dirname(os.path.abspath(__file__))
fitmodel_filename = ".x " + fitmodels_dir + "/" + fitmodel_name + ".cxx+"
gROOT.ProcessLine(fitmodel_filename)

from ROOT import RooThrExp

from ROOT import RooGaussian, RooRealVar, RooPolynomial, RooArgList, RooAddPdf, RooChebychev, RooProdPdf, RooAddPdf, RooGenericPdf, RooCBShape, RooFormulaVar


class Dsst2DsGamma(interface):
    def fit(self):
        x = self.vars[0]

        #Param init-------------

        _ntot = self.datahist.sum(False)

        _nsig = _ntot * 0.3
        _mean = 144
        _sigma = 8
        _aR = -7.84918e-01
        _nR = 100

        _nbkg = _ntot * 0.6
        _mb = 1.09507e+02
        _wb = 1.56311e+01
        _ab = -1.50242e-02

        _npr = _ntot * 0.3 * 0.1
        _meanpr = 8.05631e+01
        _sigmapr = 1.58855e+01
        _aRpr = -7.81226e-01
        _nRpr = 1.12650e+01

        #Signal-------------

        mean = RooRealVar("mean", "mean", _mean, _mean-2, _mean+2)
        sigma = RooRealVar("sigma", "sigma", _sigma, _sigma*0.75, _sigma*1.25)

        aR = RooRealVar("aR", "aR", _aR, -3, -0.5)
        nR = RooRealVar("nR", "nR", _nR, 1, 200)

        nR.setConstant()

        pdfSig = RooCBShape("pdfSig", "pdfSig", x, mean, sigma, aR, nR)
        nSig = RooRealVar("nSig", "nSig", _nsig , _nsig * 0.8 , _nsig * 1.2)

        #Bkg-------------
        # shape from fake 2012 with cut-aligned to turcal 2017

        mb = RooRealVar("mb", "mb", _mb)
        wb = RooRealVar("wb", "wb", _wb)
        ab = RooRealVar("ab", "ab", _ab, _ab*10, _ab*0.1)

        pdfBkg = RooThrExp("pdfBkg", "pdfBkg", x, mb, wb, ab)
        nBkg = RooRealVar("nBkg", "nBkg", _nbkg , _nbkg * 0.8 , _nbkg * 1.2)

        #Part-reco1 Ds*2DsPi0---------
        # shape from MC 2012 of D*0 -> [D0 pi0] reco'ed as [D0 gamma]

        mean_PR = RooRealVar("mean_PR", "mean PR", _meanpr)
        sigma_PR = RooRealVar("sigma_PR", "sigma PR",_sigmapr)
        aR_PR = RooRealVar("aR_PR", "aR_PR", _aRpr)
        nR_PR = RooRealVar("nR_PR", "nR_PR", _nRpr)

        pdfPR = RooCBShape("pdfPR", "pdfPR", x, mean_PR, sigma_PR, aR_PR, nR_PR)
        nPR = RooRealVar("nPR", "nPR", _npr , _npr * 0.8 , _npr * 1.2)

        #Model----------------

        self.model = RooAddPdf("model", "model",RooArgList(pdfBkg, pdfPR, pdfSig),RooArgList(nBkg, nPR, nSig))

        #Fit------------------

        self.fitResult = self.model.fitTo(self.datahist,ROOT.RooFit.Save(True))

        self.components = {
            'Signal': component(pdfSig, nSig),
            'PR': component(pdfPR, nPR),
            'Background': component(pdfBkg, nBkg)
        }

        self.saveFromGarbage = [x, pdfSig, pdfBkg, pdfPR, nSig, nBkg, nPR ]

        self.saveFromGarbage += [mb, wb, ab]
        self.saveFromGarbage += [mean_PR, sigma_PR, aR_PR, nR_PR]
        self.saveFromGarbage += [mean, sigma, aR, nR]

        return self.fitResult
