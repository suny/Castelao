###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from interface import interface
from component import component

import ROOT

from ROOT import RooGaussian, RooRealVar, RooPolynomial, RooArgList, RooAddPdf, RooChebychev

class Lc (interface):
  def fit(self):
    x = self.vars[0]
    mean = RooRealVar ("mean", "mean", 2285, 2280, 2290)
    sigma = RooRealVar ("sigma", "sigma", 10, 5, 30)
    gaus = RooGaussian("gaus", "gaus", x, mean, sigma)

    bg0 = RooRealVar("bg0", "bg0", 0, -2, 2)
    bg1 = RooRealVar("bg1", "bg1", 0, -2, 2)
    bg2 = RooRealVar("bg2", "bg2", 0, -1, 1)

    bkg = RooChebychev ( "bkg", "bkg", x, RooArgList(bg0) )
    
    
    nTot = self.datahist.sum(False)
    nSig = RooRealVar ( "nSig", "nSig", 0.8 * nTot, 0, nTot)
    nBkg = RooRealVar ( "nBkg", "nBkg", 0.2 * nTot, 0, nTot)

    self.model = RooAddPdf ( "model", "model", RooArgList(gaus, bkg) , RooArgList (nSig, nBkg) )
    self.fitResult = self.model.fitTo ( self.datahist, ROOT.RooFit.Save(True) )

    tfSig = gaus.asTF ( RooArgList ( x ) )
    tfBkg = bkg.asTF  ( RooArgList ( x ) )

    self.components = {
      'Signal'      :  component ( gaus , nSig, tfSig, forecolor = ROOT.kRed),
      'Background'  :  component ( bkg  , nBkg, tfBkg, forecolor = ROOT.kViolet),
    }
    
    self.saveFromGarbage = [x, mean, sigma, gaus, bg0, bg1, bg2, bkg, nSig, nBkg]

    return self.fitResult
    
      

    
  
    

