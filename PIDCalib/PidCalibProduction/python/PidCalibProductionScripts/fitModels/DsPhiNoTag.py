###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from interface import interface
from component import component
import fitUtils

import ROOT

from ROOT import RooGaussian, RooRealVar, RooPolynomial, RooArgList, RooAddPdf, RooChebychev
from ROOT import RooRealVar, RooGaussian, RooChebychev, RooArgList, RooFormulaVar
from ROOT import RooAddPdf, RooProdPdf, RooArgusBG, RooVoigtian, RooBifurGauss

import Ostap.Fixes
import Ostap.FitModels as Models
from   Ostap.PyRoUts  import cpp

class DsPhiNoTag (interface):
  def fit(self):
    x = self.vars[0]
    y = self.vars[1]

    nTot = self.datahist.sum(False)
################################################################################
## D E F I N E   A   P R E L I M I N A R Y   1 D   F I T   M O D E L          ##
################################################################################

    # Define here the structure of the fit: variables and pdfs. 
    meanX  = RooRealVar  ( "meanX", "mean_phi", 1019.46, 1015, 1025 )
    biasX = RooRealVar  ( "biasX", "biasX", 0. )
    resX = RooRealVar  ( "resX", "resX", 1.17 ) # , 0.5, 2. )

    resolutionX = ROOT.RooGaussModel("resolutionX","res X",x,biasX,resX)
    bw    = cpp.Gaudi.Math.BreitWigner( 1019.46 , 4.27 , 493.68 , 493.68 , 1 )
    relbw = Models.BreitWigner_pdf ( 'phi'	,
                                bw		,
                                mass  = x	,
                                mean  = meanX,
                                gamma = 4.27  ,
				convolution = resolutionX, #1.17,
				useFFT = True	,
                                )

    self.saveFromGarbage += [meanX, biasX, resX, resolutionX, bw, relbw]

    cheb0X  = RooRealVar  ( "cheb0X", "cheb0X", 0.18, -1, 1)
    cheb1X  = RooRealVar  ( "cheb1X", "cheb1X", -0.05, -1, 1)
    chebX   = RooChebychev( "chebX",  "chebX",  x, ROOT.RooArgList ( cheb0X, cheb1X ))

    cheb20X  = RooRealVar  ( "cheb20X", "bkg20X", -0.47, -1, 1)
    cheb21X  = RooRealVar  ( "cheb21X", "bkg21X", -0.07, -1, 1)
    cheb2X   = RooChebychev( "cheb2X",  "bkg2X",  x, ROOT.RooArgList ( cheb20X, cheb21X ))

    self.saveFromGarbage += [cheb0X,cheb1X, chebX, cheb20X, cheb21X, cheb2X]

    # Define one normalization parameter per component that you need.
    # Make sure that the sum of the initial values is nTot.
    nSig  = RooRealVar  ( "nSig", "nSig", 0.9*nTot, 0, nTot )
    nBkg  = RooRealVar  ( "nBkg", "nBkg", 0.1*nTot, 0, nTot )
    self.saveFromGarbage += [nSig, nBkg]
    
    # Don't forget to list again pdfs and their normalizations here.
    self.model = RooAddPdf ( "model", "model", RooArgList ( relbw.pdf , chebX ) , RooArgList ( nSig, nBkg ))
  
    r = self.model.fitTo ( self.datahist , ROOT.RooFit.Save() )
    self.saveFromGarbage += [r]

    meanX.setConstant()

################################################################################
## R E F I N E   M O D E L   A D D I N G   O N E   D I M E N S I O N          ##
################################################################################


    meanY  = RooRealVar  ( "meanY", "meanY",  1969.89, 1960, 1980 )
    sigmaY = RooRealVar  ( "sigmaY", "sigmaY", 5.224, 2, 15 )
    gausY  = RooGaussian ( "gausY", "gausY", y, meanY, sigmaY )
    wY = RooRealVar  ( "wY", "sigmaY", 1.905, 1., 3. )
    sigma2Y = RooFormulaVar("sigma2Y", "@0*@1", ROOT.RooArgList(sigmaY,wY) )
    gaus2Y  = RooGaussian ( "gaus2Y", "gaus2Y", y, meanY, sigma2Y )
    fracY = RooRealVar  ( "fracY", "fracY",  0.704, 0.1, 1. )  
    dgausY = RooAddPdf("dgausY","dgausY",ROOT.RooArgList(gausY,gaus2Y),RooArgList(fracY))  

    self.saveFromGarbage += [meanY,sigmaY,gausY,wY,sigma2Y,gaus2Y,fracY,dgausY]

    bkg0Y  = RooRealVar  ( "bkg0Y", "bkg0Y", 0.1, -1, 1)
    bkg1Y  = RooRealVar  ( "bkg1Y", "bkg1Y", 0.01, -1, 1)
#    bkgY   = RooChebychev( "bkgY",  "bkgY",  y, ROOT.RooArgList ( bkg0Y, bkg1Y ))
    bkgY   = RooChebychev( "bkgY",  "bkgY",  y, ROOT.RooArgList ( bkg0Y ))

    bkg20Y  = RooRealVar  ( "bkg20Y", "bkg20Y", -0.36, -1, 1)
    bkg2Y   = RooChebychev( "bkg2Y",  "bkg2Y",  y, ROOT.RooArgList ( bkg20Y ))

    self.saveFromGarbage += [bkg0Y,bkg1Y,bkgY,bkg20Y,bkg2Y]

    # Define one normalization parameter per component that you need.
    # Make sure that the sum of the initial values is nTot.
    nSig  = RooRealVar  ( "nSig", "nSig", 0.3*nTot, 0, nTot )
    nBkg  = RooRealVar  ( "nBkg", "nBkg", 0.7*nTot, 0, nTot )
    nBkgSigX  = RooRealVar  ( "nBkgSigX", "nBkg", 0.0*nTot, 0, nTot )
    nBkgSigY  = RooRealVar  ( "nBkgSigY", "nBkg", 0.0*nTot, 0, nTot )
    
    self.saveFromGarbage += [nSig,nBkg,nBkgSigX,nBkgSigY]
    
    sigX = relbw.pdf # voigX # relbw.pdf # gausX
    sigY = dgausY # gausY

    bkgX = chebX # 
    bkg2X = cheb2X # 

    self.saveFromGarbage += [sigX,sigY,bkgX,bkg2X]


    sig     = RooProdPdf ( "signal",  "signal", RooArgList ( sigX, sigY ) )
    bkg     = RooProdPdf ( "bkg",     "bkg",    RooArgList ( bkgX, bkgY  ) )
    bkgsigX = RooProdPdf ( "bkgsigX", "bkg",    RooArgList ( sigX,  bkg2Y  ) )
    bkgsigY = RooProdPdf ( "bkgsigY", "bkg",    RooArgList ( bkg2X,  sigY  ) )

    self.saveFromGarbage += [sig,bkg,bkgsigX,bkgsigY]

    # Don't forget to list again pdfs and their normalizations here.
    self.model = RooAddPdf ( "model", "model", RooArgList ( sig, bkg, bkgsigX, bkgsigY ) 
                                             , RooArgList ( nSig, nBkg, nBkgSigX, nBkgSigY ) )
#    self.fitResult = self.model.fitTo ( self.datahist , ROOT.RooFit.Save() )
    self.fitResult = fitUtils.fit ( self.datahist, self.model ) 

    # Don't forget to list again pdfs and their normalizations here.
    self.components = {
      'Signal'       :  component ( sig , nSig, forecolor = ROOT.kRed   ),
      'Background'   :  component ( bkg , nBkg, forecolor = ROOT.kBlack ), #, linestyle = ROOT.kDashed ),
      'Background_X' :  component ( bkgsigX , nBkgSigX, forecolor = ROOT.kGreen, linestyle = ROOT.kDashed ),
      'Background_Y' :  component ( bkgsigY , nBkgSigY, forecolor = ROOT.kMagenta ),
    }

    return self.fitResult

    

#####
