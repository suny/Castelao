###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from interface import interface
from component import component

import ROOT
from ROOT import gROOT

from ROOT import RooRealVar, RooAddPdf, RooArgList, RooGaussian, RooCBShape, RooArgusBG, RooFormulaVar, RooFFTConvPdf, RooExponential
from ROOT.RooFit import RooConst

fitmodel_name = "BifurcatedCB"
import os
fitmodels_dir = os.path.dirname(os.path.abspath(__file__))
fitmodel_filename = ".x " + fitmodels_dir + "/" + fitmodel_name + ".cxx+"
gROOT.ProcessLine(fitmodel_filename)

from ROOT import BifurcatedCB


class Bs2PhiGamma(interface):
    def fit(self):
        x = self.vars[0]

        #Param init-------------

        _ntot = self.datahist.sum(False)

        _nbkg = _ntot*0.73
        _a = -5e-4

        _nsig = _ntot*0.18
        _mean = 5366
        _sigma = 90
        _aL = 2.19
        _nL = 1.32
        _aR = -1.77
        _nR = 6.16                   

        _npr = _ntot*0.085
        _mshift = 280
        _argus_c = 1
        _argus_p = 1
        
        #Bkg-------------

        a = RooRealVar("a","a",_a, _a*10, _a*0.1);
        pdfBkg = RooExponential("pdfBkg","pdfBkg",x,a);
        nBkg = RooRealVar("nBkg","nBkg", _nbkg, _nbkg*0.1, _nbkg*10);

        #Signal-------------

        mean = RooRealVar("mean","mean",_mean,_mean-30,_mean+30);
        sigma = RooRealVar("sigma","sigma",_sigma,_sigma-15,_sigma+15);

        aL = RooRealVar("aL","aL",_aL);
        nL = RooRealVar("nL","nL",_nL);
        aR = RooRealVar("aR","aR",_aR);
        nR = RooRealVar("nR","nR",_aL);

        pdfSig = BifurcatedCB("pdfSig","pdfSig",x,mean,sigma,aL,nL,aR,nR);
        nSig = RooRealVar("nSig","nSig", _nsig, _nsig*0.1, _nsig*10);

        #Part-reco1 B2Xpi0---------

        x.setBins(10000,"cache") ;
        Normal = RooGaussian("Normal","Normal",x,RooConst(0.),sigma);

        mshift = RooRealVar("mshift","mshift",_mshift);

        argus_m0 = RooFormulaVar("argus_m0","argus_m0","mean-mshift",RooArgList(mean,mshift));
        argus_c = RooRealVar("argus_c","argus_c",_argus_c);
        argus_p = RooRealVar("argus_p","argus_p",_argus_p,-_argus_p,_argus_p+25);

        pdfPR0 = RooArgusBG("pdfPR0","pdfPR0",x,argus_m0,argus_c,argus_p) ;
        pdfPR = RooFFTConvPdf("pdfPR","pdfPR",x,pdfPR0,Normal);
        pdfPR.setBufferFraction(0.95);
        nPR = RooRealVar("nPR","nPR", _npr, _npr*0.2, _npr*5);                               

        #Model----------------
        
        self.model = RooAddPdf("model", "model",RooArgList(pdfBkg, pdfPR, pdfSig),RooArgList(nBkg, nPR, nSig))

        #Fit------------------

        self.fitResult = self.model.fitTo(self.datahist,ROOT.RooFit.Save(True))

        self.components = {
            'Signal': component(pdfSig, nSig),
            'PR': component(pdfPR, nPR),
            'Background': component(pdfBkg, nBkg),
        }

        self.saveFromGarbage = [x, pdfSig, pdfBkg, pdfPR, nSig, nBkg, nPR]
        self.saveFromGarbage = [Normal, pdfPR0]
        self.saveFromGarbage += [a, mean, sigma, aL, nL, aR, nR, mshift, argus_m0, argus_c, argus_p]

        return self.fitResult
