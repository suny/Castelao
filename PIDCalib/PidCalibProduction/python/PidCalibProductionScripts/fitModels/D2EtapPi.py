###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from interface import interface
from component import component

import ROOT
from ROOT import gROOT
import os
fitmodels_dir = os.path.dirname(os.path.abspath(__file__))

from ROOT import RooCBShape, RooRealVar, RooPolynomial, RooArgList, RooAddPdf, RooChebychev, RooAddPdf, RooNovosibirsk, RooGaussian, gROOT
from ROOT import RooExponential, RooFormulaVar

class D2EtapPi(interface):
    def fit(self):
        x = self.vars[0]

        #Param init-------------

        _ntot = self.datahist.sum(False)

        _nbkg = _ntot * 0.57
        _nds = _ntot * 0.41
        _npr = _ntot*0.015

        _meanDs = 1968.49
        _meanD = 1869.62

        _sigmaDs = 6
        _sscale = 2

        _frac12 = 0.5
        _frac = 0.7

        _meanprds = 1890
        _sigmaprds = 25
        
        _dmean = _meanDs - _meanD
        _smean = _meanDs / _meanD

        _c0 = -0.5

        #Bkg-------------

        c0 = RooRealVar("c0","c0",_c0,-1,1)
        pdfBkg = RooChebychev("pdfBkg","pdfBkg",x,RooArgList(c0))
        nBkg = RooRealVar("nBkg","nBkg",_nbkg,_nbkg*0.5,_nbkg*2)

        #Signal Ds-------------

        sscale = RooRealVar("sscale","sscale",_sscale,_sscale*0.5,_sscale*2)

        meanDs = RooRealVar("meanDs","meanDs", _meanDs, _meanDs-5, _meanDs+5)
        sigmaDs1 = RooRealVar("sigmaDs1","sigmaDs1",_sigmaDs,_sigmaDs-3,_sigmaDs+3)
        sigmaDs2 = RooFormulaVar("sigmaDs2","sigmaDs2","sigmaDs1*sscale",RooArgList(sigmaDs1,sscale))

        pdfDs1 = RooGaussian("pdfDs1","pdfDs1",x,meanDs,sigmaDs1)
        pdfDs2 = RooGaussian("pdfDs2","pdfDs2",x,meanDs,sigmaDs2)

        frac12 = RooRealVar("frac12","frac12",_frac12,0,1)

        pdfDs = RooAddPdf("pdfDs","pdfDs",RooArgList(pdfDs1,pdfDs2),RooArgList(frac12))

        #Signal D0-------------

        dmean = RooRealVar("dmean","dmean",_dmean)
        smean = RooRealVar("smean","smean",_smean)

        meanD0 = RooFormulaVar("meanD0","meanD0","meanDs-dmean",RooArgList(meanDs,dmean))
        sigmaD01 = RooFormulaVar("sigmaD01","sigmaD01","sigmaDs1/smean",RooArgList(sigmaDs1,smean))
        sigmaD02 = RooFormulaVar("sigmaD02","sigmaD02","sigmaDs2/smean",RooArgList(sigmaDs2,smean))

        pdfD01 = RooGaussian("pdfD01","pdfD01",x,meanD0,sigmaD01)
        pdfD02 = RooGaussian("pdfD02","pdfD02",x,meanD0,sigmaD02)

        pdfD0 = RooAddPdf("pdfD0","pdfD0",RooArgList(pdfD01,pdfD02),RooArgList(frac12))

        #Signal-------------

        frac = RooRealVar("frac","frac",_frac,0,1)
        pdfSig = RooAddPdf("pdfSig","pdfSig",RooArgList(pdfDs,pdfD0),RooArgList(frac))
        nSig = RooRealVar("nSig","nSig",_nds,_nds*0.5,_nds*2)

        #Part-reco1 D2PhiPi---------        

        meanPRDs = RooRealVar("meanPRDs","meanPRDs",_meanprds,_meanprds-10,_meanprds+10)
        sigmaPRDs = RooRealVar("sigmaPRDs","sigmaPRDs",_sigmaprds,_sigmaprds*0.2,_sigmaprds*2)

        meanPRD0 = RooFormulaVar("meanPRD0","meanPRD0","meanPRDs-dmean",RooArgList(meanPRDs,dmean))
        sigmaPRD0 = RooFormulaVar("sigmaPRD0","sigmaPRD0","sigmaPRDs/smean",RooArgList(sigmaPRDs,smean))

        pdfPRDs = RooGaussian("pdfPRDs","pdfPRDs",x,meanPRDs,sigmaPRDs)
        pdfPRD0 = RooGaussian("pdfPRD0","pdfPRD0",x,meanPRD0,sigmaPRD0)

        pdfPR = RooAddPdf("pdfPR","pdfPR",RooArgList(pdfPRDs,pdfPRD0),RooArgList(frac))
        nPR = RooRealVar("nPR","nPR", _npr, _npr*0.5, _npr*2)

        #Model----------------

        self.model = RooAddPdf("model","model",RooArgList(pdfBkg,pdfPR,pdfSig),RooArgList(nBkg,nPR,nSig))

        #Fit------------------
        
        self.fitResult = self.model.fitTo(self.datahist,ROOT.RooFit.Save(True))

        self.components = {
            'Signal': component(pdfSig, nSig),
            'PartReco': component(pdfPR, nPR),
            'Background': component(pdfBkg, nBkg)
        }

        self.saveFromGarbage = [x, pdfSig, pdfDs, pdfDs1, pdfDs2, pdfD0, pdfD01, pdfD02, pdfPR, pdfPRDs, pdfPRD0, pdfBkg, nSig, nPR, nBkg]        
        self.saveFromGarbage += [c0 , meanDs, sigmaDs1, sigmaDs2, sscale, frac12, meanD0, sigmaD01, sigmaD02, dmean, smean, frac, meanPRDs, sigmaPRDs, meanPRD0, sigmaPRD0]

        return self.fitResult
