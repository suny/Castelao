###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from interface import interface
from component import component

import ROOT
from ROOT import gROOT
fitmodel_name = "RooEGE"
fitmodel_name2 = "RooDoubleThr"
import os
fitmodels_dir = os.path.dirname(os.path.abspath(__file__))
fitmodel_filename = ".x " + fitmodels_dir + "/" + fitmodel_name + ".cxx+"
fitmodel_filename2 = ".x " + fitmodels_dir + "/" + fitmodel_name2 + ".cxx+"
gROOT.ProcessLine(fitmodel_filename)
gROOT.ProcessLine(fitmodel_filename2)

from ROOT import RooEGE, RooDoubleThr

from ROOT import RooRealVar, RooArgList, RooAddPdf, RooChebychev, RooAddPdf, RooFormulaVar


class Eta2MuMuGamma(interface):
    def fit(self):
        x = self.vars[0]

        #Param init-------------

        _ntot = self.datahist.sum(False)

        _nbkg = _ntot * 0.76
        _c0 = 5.22931e-01

        _nsig = _ntot * 0.24
        _mean = 5.48318e+02
        _sigma = 1.39426e+01
        _aL = 1.30355e+00
        _aR = -8.60724e-01

        _fpr = 0.013741
        _mR = 4.55103e+02
        _wR = 9.21490e+00
        _mL = 3.55680e+02
        _wL = 2.43174e+01

        _frac = 0.995683

        #Bkg----------------
        
        c0 = RooRealVar("c0","c0", _c0, -1, 1);
        pdfBkg = RooChebychev("pdfBkg","pdfBkg",x,RooArgList(c0));

        #Part-reco Eta2PiPiPi0-------

        mR = RooRealVar("mR","mR", _mR);
        wR = RooRealVar("wR","wR", _wR);
        mL = RooRealVar("mL","mL", _mL);
        wL = RooRealVar("wL","wL", _wL);
        pdfPR = RooDoubleThr("pdfPR","pdfPR",x,mL,wL,mR,wR);

        #Add pdf

        frac = RooRealVar("frac","frac",_frac);
        pdfBkgPR = RooAddPdf("pdfBkgPR","pdfBkgPR",RooArgList(pdfBkg,pdfPR),RooArgList(frac));

        nBkgPR = RooRealVar("nBkgPR","nBkgPR", _nbkg, _nbkg*0.8, _nbkg*1.2);

        #Signal-------------
        
        mean = RooRealVar("mean","mean", _mean, _mean-2, _mean+2);
        sigma = RooRealVar("sigma","sigma", _sigma, _sigma*0.75, _sigma*1.25);

        aL = RooRealVar("aL","aL", _aL, 0.5, 3);
        aR = RooRealVar("aR","aR", _aR, -3, -0.5);

        pdfSig = RooEGE("pdfSig","pdfSig",x,mean,sigma,aL,aR);
        nSig = RooRealVar("nSig","nSig", _nsig, _nsig*0.8, _nsig*1.2);

        #Model----------
        
        self.model = RooAddPdf("model", "model", RooArgList(pdfBkgPR, pdfSig),RooArgList(nBkgPR, nSig))
        
        #Fit------------------
        
        self.fitResult = self.model.fitTo(self.datahist,ROOT.RooFit.Save(True))

        self.components = {
            'Signal': component(pdfSig, nSig),
            'BackgroundPartReco': component(pdfBkgPR, nBkgPR)
        }

        self.saveFromGarbage = [x, pdfSig, pdfBkgPR, pdfBkg, pdfPR, nSig, nBkgPR]
        self.saveFromGarbage += [c0]
        self.saveFromGarbage += [mean, sigma, aL, aR]
        self.saveFromGarbage += [mL, wL, mR, wR, frac]

        return self.fitResult
