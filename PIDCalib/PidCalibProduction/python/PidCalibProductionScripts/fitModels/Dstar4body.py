###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from interface import interface
from component import component

import ROOT

from ROOT import (RooGaussian, RooRealVar, RooPolynomial, RooArgList, RooAddPdf, 
                  RooChebychev, RooProdPdf, RooAddPdf, RooGenericPdf, RooArgSet,
                  RooCBShape, RooExponential )

Minimizer = ROOT.RooFit.Minimizer
Range = ROOT.RooFit.Range

class Dstar4body (interface):
#   1  alphaDelta   1.39083e+02   9.54509e-02   1.09470e-02   4.01198e-01
#   2  bg0          6.78887e-02   3.57104e-04   3.86457e-05   6.78892e-03
#   3  bg0B        -1.90950e+00   5.63970e-03   2.03498e-04  -1.02152e+00
#   4  bg3          6.70856e-07   5.35094e-06   1.92745e-03  -1.57043e+00
#                                 WARNING -   - ABOVE PARAMETER IS AT LIMIT.
#   5  cf           3.62114e-01   3.25890e-04   8.27771e-04  -2.79393e-01
#   6  cf2Delta     9.53674e-07   5.22201e-07   3.37979e-03  -1.56884e+00
#   7  cfDelta      5.00000e-01   1.64048e-03   4.95479e-03   0.00000e+00
#   8  mean1        1.86546e+03   5.37185e-03   3.90412e-05   1.03252e-01
#   9  meanDelta    1.45432e+02   6.04489e-03   1.18700e-04   8.64387e-02
#  10  nCmb         1.73796e+05   4.17261e+01   4.10694e-04   5.51121e-01
#  11  nDelta      -1.30000e+00   8.88698e+00   1.11562e-03   8.59591e-01
#  12  nNR          6.06011e+03   2.76175e+01   7.63666e-04  -1.24337e+00
#  13  nRDs         2.92968e+01   3.69230e+00   4.35234e-01  -1.54813e+00
#  14  nSig         4.82618e+04   4.32140e+01   2.66648e-04  -6.14923e-01
#  15  sigma1       4.90906e+01   6.55810e-03   5.00000e-01   1.28551e+00
#  16  sigma2       7.76402e+00   4.83734e-03   3.02759e-04  -8.09464e-01
#  17  sigmaDelta   2.16632e+00   1.96557e-03   5.03381e-04  -6.21876e-01
#  18  sigmaDelta2   5.77064e-01   9.61483e-03   2.73189e-04  -1.12815e+00
#  19  sigmaDelta3   1.03628e-01   1.96606e-04   5.00000e-01  -1.53250e+00

  def fit(self):
    mD = self.vars[0]
    deltaM = self.vars[1]

    mean = RooRealVar ("mean1", "mean1", 1866, 1700, 2000)
    sigma1 = RooRealVar ("sigma1", "sigma1", 10, 5, 30)
    gaus1 = RooGaussian("gaus1", "gaus1", mD, mean, sigma1)
#    mean = RooRealVar ("mean", "mean", 1866, 1700, 2000)
    sigma2 = RooRealVar ("sigma2", "sigma2", 7, 4, 25)
    gaus2 = RooGaussian("gaus2", "gaus2", mD, mean, sigma2)

    cf = RooRealVar ("cf", "cf", .36, 0, 1)

    signalD = RooAddPdf ( "signalD", "signalD", RooArgList ( gaus1, gaus2), RooArgList(cf) ) 

    gaus1B = RooGaussian("gaus1B", "gausB", mD, mean, sigma1)
    gaus2B = RooGaussian("gaus2B", "gausB", mD, mean, sigma2)
    signalB = RooAddPdf ( "signalB", "signalB", RooArgList ( gaus1B, gaus2B), RooArgList(cf) ) 

    meanDelta   = RooRealVar ("meanDelta",    "meanDelta", 145.5, 140, 150)
    sigmaDelta  = RooRealVar ("sigmaDelta",   "sigmaDelta", 1.5, 1, 5)
    gausDelta   = RooGaussian("gausDelta",    "gausDelta", deltaM, meanDelta, sigmaDelta)
    sigmaDelta2 = RooRealVar ("sigmaDelta2",  "sigmaDelta2", .5, 0.1, 10)
    sigmaDelta3 = RooRealVar ("sigmaDelta3",  "sigmaDelta3", .1, 0.01, 0.5)
    gausDelta2  = RooGaussian("gausDelta2",   "gausDelta2", deltaM, meanDelta, sigmaDelta3)
    nDelta      = RooRealVar ("nDelta",       "nDelta",    -1.3, -3, -0.1)
    alphaDelta  = RooRealVar ("alphaDelta",   "alphaDelta", 140, 130, 150)
    cbDelta2    = RooCBShape ("cbDelta2",     "cbDelta2", deltaM, meanDelta, sigmaDelta2, nDelta, alphaDelta)
    cfDelta     = RooRealVar ("cfDelta",      "cfDelta", .50, 0, 1)
    cfDelta2    = RooRealVar ("cf2Delta",      "cf2Delta", .0, 0, 1e-5)
    signalDelta = RooAddPdf  ("signalDelta",  "signalDelta", RooArgList ( gausDelta, gausDelta2, cbDelta2),
                                                            RooArgList ( cfDelta, cfDelta2 ) )
    signalDelta2 = RooAddPdf  ("signalDelta2",  "signalDelta2", RooArgList ( gausDelta, gausDelta2, cbDelta2),
                                                            RooArgList ( cfDelta, cfDelta2 ) )
#    signalDelta = RooAddPdf  ("signalDelta",  "signalDelta", RooArgList ( gausDelta, gausDelta2 ),
#                                                            RooArgList ( cfDelta ) )


    bg0 = RooRealVar("bg0", "bg0", 0.07, 0, .1)
    bg1 = RooRealVar("bg1", "bg1", -0.8, -10, 10)
    bg2 = RooRealVar("bg2", "bg2", 0, -1, 1)
    bg3 = RooRealVar("bg3", "bg3", 0, 0, 1e-6)

    bkg = RooChebychev ( "bkg", "bkg", mD, RooArgList(bg0) )
    bkgrDs = RooExponential( "bkgrDs","bkgrDs",mD,bg3)


#    for par in [mean,sigma1,sigma2,cf]: 
#      par.setConstant()

    bg0B = RooRealVar("bg0B", "bg0", -3, -10, 0)
    bg1B = RooRealVar("bg1B", "bg1", -0.7, -10, 100)
    bg2B = RooRealVar("bg2B", "bg2", 0, -1, 1)

    bkgB = RooGenericPdf ( "bkgB", "bkg", "sqrt((@0)/139.57 -1)*exp(@1*(@0)/139.57)", RooArgList( deltaM, bg0B) )
#    bkgB = RooChebychev ( "bkgB", "bkgB", deltaM, RooArgList(bg0B, bg1B) )

    bg0Delta = RooRealVar("bg0Delta", "bg0", 0, -10, 10)
    bg1Delta = RooRealVar("bg1Delta", "bg1", -0.6, -10, 10)
    bg2Delta = RooRealVar("bg2Delta", "bg2", 0, -1, 1)

#    bkgDelta = RooChebychev ( "bkgDelta", "bkg", deltaM, RooArgList(bg0Delta, bg1Delta) )
#RooGenericPdf delmBkgModel(m_delmBkgModelName, " ", "sqrt((@0)/139.57 -1)*exp(@1*(@0)/139.57)",RooArgList(*delm,c));

    bkgDelta = RooGenericPdf ( "bkgDelta", "bkg", "sqrt((@0)/139.57 -1)*exp(@1*(@0)/139.57)", RooArgList( deltaM, bg0B) )

    signal = RooProdPdf ( "signal", "",        RooArgList ( signalD, signalDelta ) )
    nrD    = RooProdPdf ( "nrD", "",           RooArgList ( signalB, bkgB     ) )
    combo  = RooProdPdf ( "combinatorial", "", RooArgList ( bkg, bkgDelta   ) )
    mbDs   = RooProdPdf ( "MultibodyrealDstar", "",RooArgList (bkgrDs,signalDelta2) )
    
    nTot = self.datahist.sum(False)
    nSig = RooRealVar ( "nSig", "nSig", 0.09 * nTot, 0, nTot)
    nNR  = RooRealVar ( "nNR" , "nNR" , 0.1 * nTot, 0, nTot)
    nCmb = RooRealVar ( "nCmb", "nCmb", 0.71 * nTot, 0, nTot)
    nRDs = RooRealVar ( "nRDs", "nRDs", 0.1 * nTot, 0,nTot)

    self.model = RooAddPdf ( "model", "model", RooArgList(signal, nrD,  combo, mbDs) , RooArgList (nSig, nNR, nCmb, nRDs) )
    
    fixAtFirstIteration = [nDelta, alphaDelta, cfDelta, cfDelta2, bg0B]
    for var in fixAtFirstIteration:
      var.setConstant()

#    self.fitResult = self.model.fitTo ( self.datahist, ROOT.RooFit.Save(True), Minimizer("Minuit", "Simplex"))#,  Minimizer("Minuit2","Migrad") )

    for var in fixAtFirstIteration:
      var.setConstant(False)


    self.fitResult = self.model.fitTo ( self.datahist, ROOT.RooFit.Save(True), Minimizer("Minuit", "Simplex"))
                                        #Range ( 1865 - 50., 1865 + 50.) )


    tfSig = signal.asTF ( RooArgList ( mD, deltaM ) )
    tfNR  = nrD.asTF    ( RooArgList ( mD, deltaM ) )
    tfCmb = combo.asTF  ( RooArgList ( mD, deltaM ) )
    tfRDs = mbDs.asTF   ( RooArgList ( mD, deltaM ) )


    self.components = {
      'Signal'       :  component ( signal  , nSig, tfSig, forecolor = ROOT.kRed ),
      'UntaggedD'    :  component ( nrD     , nNR,  tfNR , forecolor = ROOT.kGreen),
      'Background'   :  component ( combo   , nCmb, tfCmb, forecolor = ROOT.kBlack ),
      'MultibodyD'   :  component ( mbDs   , nRDs, tfRDs, forecolor = ROOT.kMagenta ),
    }

    
    
    
    self.saveFromGarbage = [mean, sigma1, sigma2, gaus1, gaus2, signalD,
                            gaus1B, cf, gaus2B, signalB, sigmaDelta2,
                            meanDelta, sigmaDelta, gausDelta, sigmaDelta3,
                            nDelta, alphaDelta, cbDelta2,
                            bg0, bg1, bg2, bg3, bkg, bkgrDs, 
                            bg0B, bg1B, bg2B, bkgB, 
                            bg0Delta, bg1Delta, bg2Delta, bkgDelta, 
                            signal, nrD, combo,mbDs,
                            nSig, nNR, nCmb,nRDs,
                            signalDelta, signalDelta2, 
                            cfDelta, cfDelta2, gausDelta2, sigmaDelta2]

    return self.fitResult
    
      

    
  
    
