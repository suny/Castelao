###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from interface import interface
from component import component
import fitUtils

import ROOT

from ROOT import RooGaussian, RooRealVar, RooPolynomial, RooArgList, RooAddPdf, RooChebychev
from ROOT import RooRealVar, RooGaussian, RooChebychev, RooArgList, RooCBShape
from ROOT import RooAddPdf, RooProdPdf, RooArgusBG, RooExponential

Minimizer = ROOT.RooFit.Minimizer
Save = ROOT.RooFit.Save

class OmegaD (interface):
  def fit(self):
    y = self.vars[0]
#    y = self.vars[1]
    nTot = self.datahist.sum(False)

    # Define here the structure of the fit: variables and pdfs.
    ## X-pdf : for plotting only (defined as flat)
#    flat1 = RooRealVar("flat1", "flat1", 0, -10, 10)
#    sigX = RooChebychev ( "sigX", "sigX", x, RooArgList(flat1) )
#    flat2 = RooRealVar("flat2", "flat2", 0, -10, 10)
#    bkgX = RooChebychev ( "bkgX", "bkgX", x, RooArgList(flat2) )

    ## Y-pdf : for fitting (LambdaK projection) 
    meanY1 = RooRealVar ("meanY1", "meanY1", 1672,1660, 1675)
    sigmaY1 = RooRealVar ("sigmaY1", "sigmaY1", 2, 1, 5.)
    sigY = RooGaussian("sigY", "sigY", y, meanY1, sigmaY1)

    bkgY0  = RooRealVar  ( "bkgY0", "bkgY0", 0.,-0.5,0.5)
    bkgY1  = RooRealVar  ( "bkgY1", "bkgY1", 0.,-0.5,0.5)
    bkgY   = RooChebychev( "bkgY",  "bkgY",  y, ROOT.RooArgList ( bkgY0, bkgY1))

    # Define one normalization parameter per component that you need.
    # Make sure that the sum of the initial values is nTot.
    nSig = RooRealVar ( "nOmegaSig", "nOmegaSig", 0.7*nTot,0,nTot)
    nBkg = RooRealVar ( "nOmegaBkg", "nOmegaBkg", 0.3*nTot, 0,nTot)
    
    # In C++ you usually "delete" objects you don't want any longer.
    # In python you have to mark those you don't want to lose.
    # List here all the objects useful to the fit
#    self.saveFromGarbage += [ flat1, flat2, sigX, bkgX]
    self.saveFromGarbage += [ meanY1, sigmaY1, sigY ]
    self.saveFromGarbage += [ bkgY0, bkgY1, bkgY ]
    self.saveFromGarbage += [ nSig, nBkg ]

    self.model = RooAddPdf ( "model", "model", RooArgList ( sigY, bkgY ) , RooArgList ( nSig, nBkg ))
    self.fitResult = fitUtils.fit ( self.datahist, self.model )

    # Set self.model as 2D for plotting purposes 
#    sig = RooProdPdf ( "signal", "signal", RooArgList ( sigX, sigY ) )
#    bkg  = RooProdPdf ( "combo", "combo", RooArgList ( bkgX, bkgY ) )
#    self.model = RooAddPdf ( "model", "model", RooArgList(sig, bkg) , RooArgList (nSig, nBkg) )
            
    self.components = {
      'Signal'       :  component ( sigY , nSig, forecolor = ROOT.kRed   ),
      'Background'   :  component ( bkgY , nBkg, forecolor = ROOT.kBlack ),
    }

    return self.fitResult

    

