###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from interface import interface
from component import component
import fitUtils

import ROOT

from ROOT import (RooGaussian, RooRealVar, RooPolynomial, RooArgList, RooAddPdf, 
                  RooChebychev, RooProdPdf, RooAddPdf, RooGenericPdf, RooArgSet,
                  RooCBShape, RooExponential )

Minimizer = ROOT.RooFit.Minimizer
Range = ROOT.RooFit.Range

class Dstar_4comp (interface):
#   1  alphaDelta   2.10520e+01   6.69792e-01   5.00000e-01  -9.11422e-01   alphaDelta=21.4522
#   2  bg0         -2.73101e-01   5.62308e-03   1.42248e-04  -2.73135e-02    bg0=-0.272569
#   3  bg0B        -4.17912e+00   3.06652e-01   8.79162e-04  -1.10656e+00    bg0B=-4.18452
#   4  bg3          9.01778e-08   1.78067e-05   9.22309e-04  -1.57066e+00    bg3=1.32574e-06
#                                 WARNING -   - ABOVE PARAMETER IS AT LIMIT. cf=0.283379
#   5  cf           2.83104e-01   2.59648e-03   8.47171e-04  -4.48698e-01    cf2Delta=0.533894
#   6  cf2Delta     1.74494e-09   4.10163e-05   3.91759e-03  -1.57071e+00    cfDelta=0.4989
#                                 WARNING -   - ABOVE PARAMETER IS AT LIMIT. mean1=1865.47
#   7  cfDelta      5.00004e-01   2.11786e-02   8.95271e-04   8.32261e-06    meanDelta=145.461
#   8  mean1        1.86547e+03   1.07977e-02   9.16668e-05   1.03303e-01    nCmb=102716
#   9  meanDelta    1.45461e+02   2.46430e-03   5.54023e-05   9.23838e-02    nDelta=-1.30089
#  10  nCmb         1.02719e+05   4.97125e+02   3.88764e-04  -8.49139e-01    nNR=137916
#  11  nDelta      -1.29933e+00   1.03274e-01   1.72252e-01   8.59797e-01    nRDs=11785.6
#  12  nNR          1.37809e+05   1.89636e+03   2.06658e-03  -7.28231e-01    nSig=571679
#  13  nRDs         1.17643e+04   4.75616e+02   8.65459e-04  -1.33126e+00    sigma1=14.1183
#  14  nSig         5.71804e+05   1.91596e+03   4.97660e-04   3.98147e-01    sigma2=7.47498
#  15  sigma1       1.41254e+01   6.42670e-02   5.31285e-04  -6.36552e-01    sigmaDelta=0.554721
#  16  sigma2       7.47635e+00   1.40961e-02   1.61527e-04  -8.26639e-01    sigmaDelta2=1.08526
#  17  sigmaDelta   5.54826e-01   1.06749e-02   1.60906e-04  -1.13876e+00    sigmaDelta3=0.150101
#  18  sigmaDelta2   1.08524e+00   4.69351e-03   2.12331e-04  -9.28899e-01
#  19  sigmaDelta3   1.05368e-01   9.52793e+00   5.00000e-01  -1.52422e+00

  def fit(self):
    mD = self.vars[0]
    deltaM = self.vars[1]

    mean = RooRealVar ("mean1", "mean1", 1866, 1860, 1872)
    sigma1 = RooRealVar ("sigma1", "sigma1", 15, 8, 30)
    gaus1 = RooGaussian("gaus1", "gaus1", mD, mean, sigma1)
#    mean = RooRealVar ("mean", "mean", 1866, 1700, 2000)
    sigma2 = RooRealVar ("sigma2", "sigma2", 5, 4, 10)
    gaus2 = RooGaussian("gaus2", "gaus2", mD, mean, sigma2)

    cf = RooRealVar ("cf", "cf", .25, 0, 1.)

    signalD = RooAddPdf ( "signalD", "signalD", RooArgList ( gaus1, gaus2), RooArgList(cf) ) 

    gaus1B = RooGaussian("gaus1B", "gausB", mD, mean, sigma1)
    gaus2B = RooGaussian("gaus2B", "gausB", mD, mean, sigma2)
    signalB = RooAddPdf ( "signalB", "signalB", RooArgList ( gaus1B, gaus2B), RooArgList(cf) ) 

    meanDelta   = RooRealVar ("meanDelta",    "meanDelta", 145.5, 140, 150)
    meanDelta2  = RooRealVar ("meanDelta2",   "meanDelta2",145.5, 140, 150)
    sigmaDelta  = RooRealVar ("sigmaDelta",   "sigmaDelta", 0.5, 0.1, 10)
    gausDelta   = RooGaussian("gausDelta",    "gausDelta", deltaM, meanDelta, sigmaDelta)
    sigmaDelta2 = RooRealVar ("sigmaDelta2",  "sigmaDelta2", .8, 0.1, 10)
    sigmaDelta3 = RooRealVar ("sigmaDelta3",  "sigmaDelta3", .8, 0.1, 10)
    gausDelta2  = RooGaussian("gausDelta2",   "gausDelta2", deltaM, meanDelta, sigmaDelta3)
    sigmaDelta4 = RooRealVar ("sigmaDelta4",  "sigmaDelta3", .8, 0.1, 10)
    gausDelta4  = RooGaussian("gausDelta4",   "gausDelta2", deltaM, meanDelta, sigmaDelta4)
    nDelta      = RooRealVar ("nDelta",       "nDelta",    -1.3, -10, -0.1)
    alphaDelta  = RooRealVar ("alphaDelta",   "alphaDelta", 40, 30, 80)
    cbDelta2    = RooCBShape ("cbDelta2",     "cbDelta2", deltaM, meanDelta2, sigmaDelta2, nDelta, alphaDelta)
    cfDelta     = RooRealVar ("cfDelta",      "cfDelta", .46, 0, 1)
    cfDelta2    = RooRealVar ("cf2Delta",      "cf2Delta", .02, 0, 0.1)
    cfDelta3    = RooRealVar ("cf3Delta",      "cf3Delta", .02, 0, 0.1)
    signalDelta = RooAddPdf  ("signalDelta",  "signalDelta", RooArgList ( gausDelta, gausDelta2,  cbDelta2),
                                                              RooArgList ( cfDelta, cfDelta2 ) )


    signalDelta2 = RooAddPdf  ("signalDelta2",  "signalDelta2", RooArgList ( gausDelta, gausDelta2,  cbDelta2),
                                                              RooArgList ( cfDelta, cfDelta2 ) )
#    signalDelta = RooAddPdf  ("signalDelta",  "signalDelta", RooArgList ( gausDelta, gausDelta2 ),
#                                                            RooArgList ( cfDelta ) )


    bg0 = RooRealVar("bg0", "bg0", -0.27, -1, 1)
    bg1 = RooRealVar("bg1", "bg1", -0.27, -1, 1)
    bg2 = RooRealVar("bg2", "bg2", 1e-7, 1e-10,1e-5)
    bg3 = RooRealVar("bg3", "bg3", 1e-7, 1e-10,1e-5)

    bkg = RooChebychev ( "bkg", "bkg", mD, RooArgList(bg0) )
    bkgrDs = RooChebychev ( "bkg", "bkg", mD, RooArgList(bg1) )
#    bkg = RooExponential ( "bkg", "bkg", mD, bg2)
#    bkgrDs = RooExponential( "bkgrDs","bkgrDs",mD,bg3)


#    for par in [mean,sigma1,sigma2,cf]: 
#      par.setConstant()

    bg0B = RooRealVar("bg0B", "bg0", -4, -10, 10)
    bg1B = RooRealVar("bg1B", "bg1", -0.7, -10, 100)
    bg2B = RooRealVar("bg2B", "bg2", 0, -1, 1)

    bkgB = RooGenericPdf ( "bkgB", "bkg", "sqrt((@0)/139.57 -1)*exp(@1*(@0)/139.57)", RooArgList( deltaM, bg0B) )
#    bkgB = RooChebychev ( "bkgB", "bkgB", deltaM, RooArgList(bg0B, bg1B) )

    bg0Delta = RooRealVar("bg0Delta", "bg0", 0, -10, 10)
    bg1Delta = RooRealVar("bg1Delta", "bg1", -0.6, -10, 10)
    bg2Delta = RooRealVar("bg2Delta", "bg2", 0, -1, 1)

#    bkgDelta = RooChebychev ( "bkgDelta", "bkg", deltaM, RooArgList(bg0Delta, bg1Delta) )
#RooGenericPdf delmBkgModel(m_delmBkgModelName, " ", "sqrt((@0)/139.57 -1)*exp(@1*(@0)/139.57)",RooArgList(*delm,c));

    threshold = RooRealVar("threshold", "threshold", 139.57, 139, 142);
    bkgDelta = RooGenericPdf ( "bkgDelta", "bkg", "sqrt((@0)/@2 -1)*exp(@1*(@0)/@2)", RooArgList( deltaM, bg0B, threshold) )

    signal = RooProdPdf ( "signal", "",        RooArgList ( signalD, signalDelta ) )
    nrD    = RooProdPdf ( "nrD", "",           RooArgList ( signalB, bkgB     ) )
    combo  = RooProdPdf ( "combinatorial", "", RooArgList ( bkg, bkgDelta   ) )
    mbDs   = RooProdPdf ( "MultibodyrealDstar", "",RooArgList (bkgrDs,signalDelta2) )
    
    nTot = self.datahist.sum(False)
    nSig = RooRealVar ( "nSig", "nSig", 0.6 * nTot, 0, nTot)
    nNR  = RooRealVar ( "nNR" , "nNR" , 0.15 * nTot, 0, nTot)
    nCmb = RooRealVar ( "nCmb", "nCmb", 0.2 * nTot, 0, nTot)
    nRDs = RooRealVar ( "nRDs", "nRDs", 0.05 * nTot, 0,nTot)

    self.model = RooAddPdf ( "model", "model", RooArgList(signal, nrD,  combo, mbDs) , RooArgList (nSig, nNR, nCmb, nRDs) )
    
    fixAtFirstIteration = [nDelta, alphaDelta, cfDelta, cfDelta2]
    for var in fixAtFirstIteration:
      var.setConstant()

#    self.fitResult = self.model.fitTo ( self.datahist, ROOT.RooFit.Save(True))#,  Minimizer("Minuit2","Migrad") )
    self.fitResult = fitUtils.fit ( self.datahist, self.model, "Migrad" )


    for var in fixAtFirstIteration:
      var.setConstant(False)


#    self.fitResult = self.model.fitTo ( self.datahist, ROOT.RooFit.Save(True), Minimizer("Minuit", "Simplex"))
    self.fitResult = fitUtils.fit ( self.datahist, self.model ) 
                                        #Range ( 1865 - 50., 1865 + 50.) )


    tfSig = signal.asTF ( RooArgList ( mD, deltaM ) )
    tfNR  = nrD.asTF    ( RooArgList ( mD, deltaM ) )
    tfCmb = combo.asTF  ( RooArgList ( mD, deltaM ) )
    tfRDs = mbDs.asTF   ( RooArgList ( mD, deltaM ) )


    self.components = {
      'Signal'       :  component ( signal  , nSig, tfSig, forecolor = ROOT.kRed ),
      'UntaggedD'    :  component ( nrD     , nNR,  tfNR , forecolor = ROOT.kGreen),
      'Background'   :  component ( combo   , nCmb, tfCmb, forecolor = ROOT.kBlack ),
      'MultibodyD'   :  component ( mbDs   , nRDs, tfCmb, forecolor = ROOT.kMagenta ),
    }

    
    
    
    self.saveFromGarbage = [mean, sigma1, sigma2, gaus1, gaus2, signalD, threshold,
                            gaus1B, cf, gaus2B, signalB, sigmaDelta2,
                            meanDelta, meanDelta2, sigmaDelta, gausDelta, sigmaDelta3,
                            nDelta, alphaDelta, cbDelta2,
                            bg0, bg1, bg2, bg3, bkg, bkgrDs, 
                            bg0B, bg1B, bg2B, bkgB, 
                            bg0Delta, bg1Delta, bg2Delta, bkgDelta, 
                            signal, nrD, combo,mbDs,
                            nSig, nNR, nCmb,nRDs,
                            signalDelta, signalDelta2, 
                            cfDelta3, gausDelta4, sigmaDelta4,
                            cfDelta, cfDelta2, gausDelta2, sigmaDelta2]

    return self.fitResult
    
      

    
  
    
