###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from interface import interface
from component import component

import ROOT

from ROOT import (RooGaussian, RooRealVar, RooPolynomial, RooArgList, RooAddPdf, 
                  RooChebychev, RooProdPdf, RooAddPdf, RooGenericPdf,
                  RooCBShape )

Minimizer = ROOT.RooFit.Minimizer
Range = ROOT.RooFit.Range

class Dstar_2Gaus (interface):
  def fit(self):
    mD = self.vars[0]
    deltaM = self.vars[1]

    mean = RooRealVar ("mean1", "mean1", 1866, 1700, 2000)
    sigma1 = RooRealVar ("sigma1", "sigma1", 10, 5, 50)
    gaus1 = RooGaussian("gaus1", "gaus1", mD, mean, sigma1)
#    mean = RooRealVar ("mean", "mean", 1866, 1700, 2000)
    sigma2 = RooRealVar ("sigma2", "sigma2", 5, 1, 50)
    gaus2 = RooGaussian("gaus2", "gaus2", mD, mean, sigma2)

    cf = RooRealVar ("cf", "cf", 1.)#.95, 0, 1.)

    signalD = RooAddPdf ( "signalD", "signalD", RooArgList ( gaus1, gaus2), RooArgList(cf) ) 

    gaus1B = RooGaussian("gaus1B", "gausB", mD, mean, sigma1)
    gaus2B = RooGaussian("gaus2B", "gausB", mD, mean, sigma2)
    signalB = RooAddPdf ( "signalB", "signalB", RooArgList ( gaus1B, gaus2B), RooArgList(cf) ) 

    meanDelta   = RooRealVar ("meanDelta",    "meanDelta", 145.5, 140, 150)
    sigmaDelta  = RooRealVar ("sigmaDelta",   "sigmaDelta", 0.5, 0.1, 10)
    gausDelta   = RooGaussian("gausDelta",    "gausDelta", deltaM, meanDelta, sigmaDelta)
    gausDeltaC  = RooGaussian("gausDeltaC",   "gausDeltaC", deltaM, meanDelta, sigmaDelta)
    sigmaDelta2 = RooRealVar ("sigmaDelta2",  "sigmaDelta", .8, 0.1, 10)
    sigmaDelta3 = RooRealVar ("sigmaDelta3",  "sigmaDelta", .8, 0.1, 10)
    gausDelta2  = RooGaussian("gausDelta2",   "gausDelta", deltaM, meanDelta, sigmaDelta3)
    gausDelta2C = RooGaussian("gausDelta2C",  "gausDelta", deltaM, meanDelta, sigmaDelta3)
    nDelta      = RooRealVar ("nDelta",       "nDelta",    -1.3, -10, -0.1)
    alphaDelta  = RooRealVar ("alphaDelta",   "alphaDelta", 20, 0.1, 200)
    cbDelta2    = RooCBShape ("cbDelta2",     "cbDelta2", deltaM, meanDelta, sigmaDelta2, nDelta, alphaDelta)
    cbDelta2C   = RooCBShape ("cbDelta2C",    "cbDelta2C", deltaM, meanDelta, sigmaDelta2, nDelta, alphaDelta)
    cfDelta     = RooRealVar ("cfDelta",      "cfDelta", 0.)#50, 0, 1)
    cfDelta2    = RooRealVar ("cf2Delta",      "cf2Delta", .0)#, 0, 1)
    signalDelta = RooAddPdf  ("signalDelta",  "signalDelta", RooArgList ( gausDelta, gausDelta2, cbDelta2),
                                                            RooArgList ( cfDelta, cfDelta2 ) )
#    signalDelta = RooAddPdf  ("signalDelta",  "signalDelta", RooArgList ( gausDelta, gausDelta2 ),
#                                                            RooArgList ( cfDelta ) )
    signalDeltaC = RooAddPdf  ("signalDeltaC",  "signalDeltaC", RooArgList ( gausDeltaC, gausDelta2C, cbDelta2C),
                                                            RooArgList ( cfDelta, cfDelta2 ) )


    bg0 = RooRealVar("bg0", "bg0", +0.14, -10, 10)
    bg1 = RooRealVar("bg1", "bg1", -0.5, -10, 10)
    bg2 = RooRealVar("bg2", "bg2", 0, -1, 1)
    bgC0 = RooRealVar("bg0", "bg0", -0.07, -10, 10)
    bgC1 = RooRealVar("bg1", "bg1", -0.8, -10, 10)

    bkg = RooChebychev ( "bkg", "bkg", mD, RooArgList(bg0, bg1) )
    bkgC = RooChebychev ( "bkgC", "bkgC", mD, RooArgList(bgC0, bgC1) )

#    for par in [mean,sigma1,sigma2,cf]: 
#      par.setConstant()

    bg0B = RooRealVar("bg0B", "bg0", -0.2, -10, 100)
    bg1B = RooRealVar("bg1B", "bg1", -0.7, -10, 100)
    bg2B = RooRealVar("bg2B", "bg2", 0, -1, 1)

    bkgB = RooGenericPdf ( "bkgB", "bkg", "sqrt((@0)/139.57 -1)*exp(@1*(@0)/139.57)", RooArgList( deltaM, bg0B) )
#    bkgB = RooChebychev ( "bkgB", "bkgB", deltaM, RooArgList(bg0B, bg1B) )

    bg0Delta = RooRealVar("bg0Delta", "bg0", 0, -10, 10)
    bg1Delta = RooRealVar("bg1Delta", "bg1", -0.6, -10, 10)
    bg2Delta = RooRealVar("bg2Delta", "bg2", 0, -1, 1)

#    bkgDelta = RooChebychev ( "bkgDelta", "bkg", deltaM, RooArgList(bg0Delta, bg1Delta) )
#RooGenericPdf delmBkgModel(m_delmBkgModelName, " ", "sqrt((@0)/139.57 -1)*exp(@1*(@0)/139.57)",RooArgList(*delm,c));

    bkgDelta = RooGenericPdf ( "bkgDelta", "bkg", "sqrt((@0)/139.57 -1)*exp(@1*(@0)/139.57)", RooArgList( deltaM, bg0B) )

    signal = RooProdPdf ( "signal", "",        RooArgList ( signalD, signalDelta ) )
    nrD    = RooProdPdf ( "nrD", "",           RooArgList ( signalB, bkgB     ) )
    partD  = RooProdPdf ( "partD", "",         RooArgList ( bkgC,    signalDeltaC     ) )
    combo  = RooProdPdf ( "combinatorial", "", RooArgList ( bkg, bkgDelta   ) )
    
    
    nTot = self.datahist.sum(False)
    nSig = RooRealVar ( "nSig", "nSig", 0.6 * nTot, 0, nTot)
    nNR  = RooRealVar ( "nNR" , "nNR" , 0.05 * nTot, 0, nTot)
    nPrt = RooRealVar ( "nPrt", "nPrt", 0.05 * nTot, 0, nTot)
    nCmb = RooRealVar ( "nCmb", "nCmb", 0.3 * nTot, 0, nTot)


    self.model = RooAddPdf ( "model", "model", RooArgList(signal, nrD,  combo, partD) , RooArgList (nSig, nNR, nCmb, nPrt) )
    
    fixAtFirstIteration = [nDelta, alphaDelta, cfDelta, cfDelta2]
    for var in fixAtFirstIteration:
      var.setConstant()

    self.fitResult = self.model.fitTo ( self.datahist, ROOT.RooFit.Save(True))#,  Minimizer("Minuit2","Migrad") )

    for var in fixAtFirstIteration:
      var.setConstant(False)


    self.fitResult = self.model.fitTo ( self.datahist, ROOT.RooFit.Save(True), Minimizer("Minuit", "Simplex"))
                                        #Range ( 1865 - 50., 1865 + 50.) )

    tfSig = signal.asTF ( RooArgList ( mD, deltaM ) )
    tfNR  = nrD.asTF    ( RooArgList ( mD, deltaM ) )
    tfCmb = combo.asTF  ( RooArgList ( mD, deltaM ) )
    tfPrt = partD.asTF  ( RooArgList ( mD, deltaM ) )


    self.components = {
      'Signal'       :  component ( signal  , nSig, tfSig, forecolor = ROOT.kRed ),
      'UntaggedD'    :  component ( nrD     , nNR,  tfNR , forecolor = ROOT.kGreen),
      'Background'   :  component ( combo   , nCmb, tfCmb, forecolor = ROOT.kBlack ),
      'PartiallyD'   :  component ( partD , nPrt, tfPrt, forecolor = ROOT.kCyan ),
    }
    
    self.saveFromGarbage = [mean, sigma1, sigma2, gaus1, gaus2, signalD,
                            gaus1B, cf, gaus2B, signalB, sigmaDelta2,
                            meanDelta, sigmaDelta, gausDelta, sigmaDelta3,
                            nDelta, alphaDelta, cbDelta2,
                            bg0, bg1, bg2, bkg, 
                            bg0B, bg1B, bg2B, bkgB, 
                            bg0Delta, bg1Delta, bg2Delta, bkgDelta, 
                            signal, nrD, combo,
                            nSig, nNR, nCmb,
                            partD, nPrt, signalDeltaC, bkgC, bgC0, bgC1,
                            gausDeltaC, gausDelta2C, cbDelta2C,
                            signalDelta, cfDelta, cfDelta2, gausDelta2, sigmaDelta2]

    return self.fitResult
    
      

    
  
    
