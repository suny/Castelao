###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from interface import interface
from component import component

import ROOT

from ROOT import RooGaussian, RooRealVar, RooPolynomial, RooArgList, RooAddPdf, RooChebychev, RooProdPdf, RooAddPdf, RooGenericPdf, RooBukinPdf, RooCBShape

class Dstar_Bukin (interface):
  def fit(self):
    mD = self.vars[0]
    deltaM = self.vars[1]

    mean = RooRealVar ("mean", "mean", 1866, 1700, 2000)
    sigma = RooRealVar ("sigma", "sigma", 10, 5, 50)
    xp    = RooRealVar ("xp", "xp", 0, -100,100)
    xi    = RooRealVar ("xi", "xi", 0, -100, 100)
    rho1  = RooRealVar ("rho1", "rho1", 0, -100, 100)
    rho2  = RooRealVar ("rho2", "rho2", 0, -100, 100)

    signalD = RooBukinPdf ( "signalD", "signalD", mD, mean, sigma, xi, rho1, rho2)

#    gaus1B = RooGaussian("gaus1B", "gausB", mD, mean, sigma1)
#    gaus2B = RooGaussian("gaus2B", "gausB", mD, mean, sigma2)
#    signalB = RooAddPdf ( "signalB", "signalB", RooArgList ( gaus1B, gaus2B), RooArgList(cf) ) 
    signalB = RooBukinPdf ( "signalB", "signalB", mD, mean, sigma, xi, rho1, rho2)

    meanDelta   = RooRealVar ("meanDelta",    "meanDelta", 145.5, 140, 150)
    sigmaDelta  = RooRealVar ("sigmaDelta",   "sigmaDelta", 0.5, 0.1, 10)
    gausDelta   = RooGaussian("gausDelta",    "gausDelta", deltaM, meanDelta, sigmaDelta)
    sigmaDelta2 = RooRealVar ("sigmaDelta2",  "sigmaDelta", .8, 0.1, 10)
    sigmaDelta3 = RooRealVar ("sigmaDelta3",  "sigmaDelta", .8, 0.1, 10)
    gausDelta2  = RooGaussian("gausDelta2",   "gausDelta", deltaM, meanDelta, sigmaDelta3)
    nDelta      = RooRealVar ("nDelta",       "nDelta",    -1.3, -10, -0.1)
    alphaDelta  = RooRealVar ("alphaDelta",   "alphaDelta", 20, 0.1, 200)
    cbDelta2    = RooCBShape ("cbDelta2",     "cbDelta2", deltaM, meanDelta, sigmaDelta2, nDelta, alphaDelta)
    cfDelta     = RooRealVar ("cfDelta",      "cfDelta", .50, 0, 1)
    cfDelta2    = RooRealVar ("cf2Delta",     "cf2Delta", 0)#.10, 0, 1)
#    signalDelta = RooAddPdf  ("signalDelta",  "signalDelta", RooArgList ( gausDelta, gausDelta2, cbDelta2),
#                                                            RooArgList ( cfDelta, cfDelta2 ) )
#    signalDeltaC = RooAddPdf  ("signalDeltaC",  "signalDeltaC", RooArgList ( gausDelta, gausDelta2, cbDelta2),
#                                                            RooArgList ( cfDelta, cfDelta2 ) )
    signalDelta = gausDelta
    signalDeltaC = gausDelta2

#    for par in [meanDelta,sigmaDelta,sigmaDelta2,cfDelta]:
#      par.setConstant()

    bg0 = RooRealVar("bg0", "bg0", -0.07, -10, 10)
    bg1 = RooRealVar("bg1", "bg1", -0.8, -10, 10)
    bg2 = RooRealVar("bg2", "bg2", 0, -1, 1)
    bgC0 = RooRealVar("bg0", "bg0", -0.07, -10, 10)
    bgC1 = RooRealVar("bg1", "bg1", -0.8, -10, 10)

    bkg = RooChebychev ( "bkg", "bkg", mD, RooArgList(bg0, bg1) )
    bkgC = RooChebychev ( "bkgC", "bkgC", mD, RooArgList(bgC0, bgC1) )

#    for par in [mean,sigma1,sigma2,cf]: 
#      par.setConstant()

    bg0B = RooRealVar("bg0B", "bg0B", -0.2, -10, 100)
    bg1B = RooRealVar("bg1B", "bg1B", -0.7, -10, 100)
    bg2B = RooRealVar("bg2B", "bg2B", 0, -1, 1)

    bkgB = RooGenericPdf ( "bkgB", "bkg", "sqrt((@0)/139.57 -1)*exp(@1*(@0)/139.57)", RooArgList( deltaM, bg0B) )
#    bkgB = RooChebychev ( "bkgB", "bkgB", deltaM, RooArgList(bg0B, bg1B) )

    bg0Delta = RooRealVar("bg0Delta", "bg0", 0, -10, 10)
    bg1Delta = RooRealVar("bg1Delta", "bg1", -0.6, -10, 10)
    bg2Delta = RooRealVar("bg2Delta", "bg2", 0, -1, 1)

#    bkgDelta = RooChebychev ( "bkgDelta", "bkg", deltaM, RooArgList(bg0Delta, bg1Delta) )
#RooGenericPdf delmBkgModel(m_delmBkgModelName, " ", "sqrt((@0)/139.57 -1)*exp(@1*(@0)/139.57)",RooArgList(*delm,c));

    bkgDelta = RooGenericPdf ( "bkgDelta", "bkg", "sqrt((@0)/139.57 -1)*exp(@1*(@0)/139.57)", RooArgList( deltaM, bg0Delta) )

    signal = RooProdPdf ( "signal", "",        RooArgList ( signalD, signalDelta ) )
    nrD    = RooProdPdf ( "nrD", "",           RooArgList ( signalB, bkgB     ) )
    partD  = RooProdPdf ( "partD", "",         RooArgList ( bkgC,    signalDeltaC     ) )
    combo  = RooProdPdf ( "combinatorial", "", RooArgList ( bkg, bkgDelta   ) )
    
    
    nTot = self.datahist.sum(False)
    nSig = RooRealVar ( "nSig", "nSig", 0.6 * nTot, 0, nTot)
    nNR  = RooRealVar ( "nNR" , "nNR" , 0.05 * nTot, 0, nTot)
    nCmb = RooRealVar ( "nCmb", "nCmb", 0.3 * nTot, 0, nTot)
    nPrt = RooRealVar ( "nPrt", "nPrt", 0.05 * nTot, 0, nTot)


    self.model = RooAddPdf ( "model", "model", RooArgList(signal, nrD,  combo, partD) , RooArgList (nSig, nNR, nCmb, nPrt) )
    mD.setBins(30)
    self.fitResult = self.model.fitTo ( self.datahist, ROOT.RooFit.Save(True) )

    tfSig = signal.asTF ( RooArgList ( mD, deltaM ) )
    tfNR  = nrD.asTF    ( RooArgList ( mD, deltaM ) )
    tfCmb = combo.asTF  ( RooArgList ( mD, deltaM ) )
    tfPrt = partD.asTF  ( RooArgList ( mD, deltaM ) )


    self.components = {
      'SignalD'       :  component ( signal, nSig, tfSig, forecolor = ROOT.kRed ),
      'UntaggedD'    :  component ( nrD   , nNR,  tfNR , forecolor = ROOT.kGreen),
      'Background'   :  component ( combo , nCmb, tfCmb, forecolor = ROOT.kBlack ),
      'PartiallyD'   :  component ( partD , nPrt, tfPrt, forecolor = ROOT.kCyan ),
    }
    
    self.saveFromGarbage = [mean, signalD, signalB,
                            sigma, xp, xi, rho1, rho2,
                            meanDelta, sigmaDelta, gausDelta, 
                            bg0, bg1, bg2, bkg, 
                            bg0B, bg1B, bg2B, bkgB, 
                            bg0Delta, bg1Delta, bg2Delta, bkgDelta, 
                            signal, nrD, combo,
                            nSig, nNR, nCmb,
                            signalDelta, cfDelta, gausDelta2, sigmaDelta2,
                            signalB, sigmaDelta2,
                            meanDelta, sigmaDelta, gausDelta, sigmaDelta3,
                            nDelta, alphaDelta, cbDelta2,
                            bg0, bg1, bg2, bkg, 
                            bg0B, bg1B, bg2B, bkgB, 
                            bg0Delta, bg1Delta, bg2Delta, bkgDelta, 
                            signal, nrD, combo,
                            nSig, nNR, nCmb,
                            partD, nPrt, signalDeltaC, bkgC, bgC0, bgC1,
                            signalDelta, cfDelta, cfDelta2, gausDelta2, sigmaDelta2]


    return self.fitResult
    
      

    
  
    
