###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from interface import interface
from component import component

import ROOT

from ROOT import RooGaussian, RooRealVar, RooArgList, RooAddPdf, RooExponential

class DsPhiMuMuPi (interface):
  def fit(self):
    x = self.vars[0]
    mean = RooRealVar ("mean", "mean", 1970, 1900, 2030)
    sigma = RooRealVar ("sigma", "sigma", 10, 3, 30)
    gaus = RooGaussian("gaus", "gaus", x, mean, sigma)

    elambda = RooRealVar("elambda","elambda", 0, -10, 10)
    expo = RooExponential("expo","expo", x, elambda);

    nTot = self.datahist.sum(False)
    nSig = RooRealVar ( "nSig", "nSig", 0.5 * nTot, 0, nTot)
    nBkg = RooRealVar ( "nBkg", "nBkg", 0.5 * nTot, 0, nTot)

    self.model = RooAddPdf ( "model", "model", RooArgList(gaus, expo) , RooArgList (nSig, nBkg))
    self.fitResult = self.model.fitTo ( self.datahist, ROOT.RooFit.Save(True) )

    tfSig = gaus.asTF ( RooArgList ( x ) )
    tfBkg = expo.asTF  ( RooArgList ( x ) )

    self.components = {
      'Signal'      :  component ( gaus , nSig, tfSig, forecolor = ROOT.kRed),
      'Background'  :  component ( expo  , nBkg, tfBkg, forecolor = ROOT.kViolet),
    }
    
    self.saveFromGarbage = [x, mean, sigma, gaus, elambda, expo, nSig, nBkg]

    return self.fitResult
    
      

    
  
    

