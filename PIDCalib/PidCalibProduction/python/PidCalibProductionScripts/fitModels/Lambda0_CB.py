###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import ROOT
from interface import interface
from component import component
import fitUtils

from ROOT import (RooGaussian, RooRealVar, RooPolynomial, RooArgList, 
                  RooAddPdf, RooChebychev, RooAddPdf , RooCBShape)
                  
Minimizer = ROOT.RooFit.Minimizer
                

class Lambda0_CB (interface):
#   1  alpha1      -5.89297e-01   1.15193e-03   6.27516e-02   1.02109e+00
#   2  alpha2       1.44941e+00   2.86596e-02   4.95402e-02  -6.91441e-01
#   3  bg0          2.47201e-01   3.04419e-05   2.23548e-03   2.49790e-01
#   4  bg1          9.92279e-01   4.13845e-06   2.37433e-03   1.44645e+00
#   5  cf1          2.78570e-01   6.63641e-04   1.64828e-01  -4.58787e-01
#   6  mean1        1.11577e+03   8.17893e-04   9.50887e-02  -2.31838e-01
#   7  n1           4.63021e+00   2.00342e-02   5.00000e-01   2.15228e+00
#   8  n2           1.88989e+00   5.47067e-03   2.76642e-01  -5.88428e-01
#   9  nBkg         4.10105e+03   2.90329e+01   1.03521e-01  -1.29235e+00
#  10  nSig         2.06432e+05   9.96317e+01   3.48954e-01   1.21892e+00
#  11  sigma1       1.06897e+00   9.65521e-04   5.00000e-01  -4.82272e-02

  def fit(self):
    x = self.vars[0]
    mean = {}; sigma = {}; gaus = {}

    mean1 = RooRealVar ("mean1", "mean1", 1116.3, 1115, 1117)
    sigma1 = RooRealVar ("sigma1", "sigma1",  3.5, 0.1, 5.)
    alpha1 = RooRealVar ("alpha1", "alpha1", -3.58,  -6, -0.1)
    n1 = RooRealVar ("n1", "n1", 5, 1, 6)
    cb1 = RooCBShape("cb1", "cb1", x, mean1, sigma1, alpha1, n1)

    self.saveFromGarbage += [mean1, sigma1, alpha1, n1, cb1]

#    mean2 = RooRealVar ("mean2", "mean2", 1116, 1115, 1117)
#    sigma2 = RooRealVar ("sigma2", "sigma2",  1, .05, 2)
    alpha2 = RooRealVar ("alpha2", "alpha2", 1.45,  0.1, 2.)
    n2 = RooRealVar ("n2", "n2", 1.9, 1.0, 4)
    cb2 = RooCBShape("cb2", "cb2", x, mean1, sigma1, alpha2, n2)

    self.saveFromGarbage += [ alpha2, n2, cb2]
    for iGaus in [str(i) for i in range(0,3)]:
      m = RooRealVar  ("mean"+iGaus,  "mean"+iGaus,  1116.3, 1115, 1117)
      s = RooRealVar  ("sigma"+iGaus, "sigma"+iGaus, 1.5,  0.5,  30)
      gaus[iGaus]  = RooGaussian ("gaus"+iGaus,  "gaus"+iGaus,  x, m, s)
      mean[iGaus] = m; sigma[iGaus] = s;


    cf1 = RooRealVar ( "cf1", "cf1", 0.2, 0, 1.)
    cf2 = RooRealVar ( "cf2", "cf2", 1., 0, 1.)
  
#    signal = RooAddPdf ("L0signal", "", RooArgList ( gaus['0'], gaus['1'], gaus['2'] ),
#                                      RooArgList ( cf1, cf2 ) )


    signal = RooAddPdf ( "signalL0", "signal", cb1, gaus['0'], cf1 )
    signalWithGaussian = RooAddPdf("signalWithGaussian" ,"", signal, gaus['1'], cf2)
    self.saveFromGarbage += [signalWithGaussian]



    bg0 = RooRealVar("bg0", "bg0", 0.24, -1, 1)
    bg1 = RooRealVar("bg1", "bg1", 0, -1, 1)
    bg2 = RooRealVar("bg2", "bg2", 0, -1, 1)

    bkg = RooChebychev ( "bkg", "bkg", x, RooArgList(bg0))#, bg1) )
    
    
    nTot = self.datahist.sum(False)
    nSig = RooRealVar ( "nSig", "nSig", 0.85 * nTot, 0, nTot)
    nBkg = RooRealVar ( "nBkg", "nBkg", 0.15 * nTot, 0, nTot)


    self.model = RooAddPdf ( "model", "model", RooArgList(signalWithGaussian, bkg) , RooArgList (nSig, nBkg) )

    cf2.setConstant()
    self.fitResult = self.model.fitTo ( self.datahist, ROOT.RooFit.Save(True),
        Minimizer("Minuit", "Simplex") )
    cf2.setConstant(False)
#    self.fitResult = self.model.fitTo ( self.datahist, ROOT.RooFit.Save(True),
#        Minimizer("Minuit", "Simplex") )

    self.fitResult = fitUtils.fit ( self.datahist, self.model , "Minimize" ) 
    self.fitResult = fitUtils.fit ( self.datahist, self.model ) 

    tfSig = signal.asTF ( RooArgList ( x ) )
    tfBkg = bkg.asTF ( RooArgList ( x ) )

    self.components = {
      'Signal'      :  component ( signal , nSig, tfSig),
      'Background'  :  component ( bkg    , nBkg, tfBkg),
    }
    

    items = lambda x: [ x[i] for i in x ];
    self.saveFromGarbage +=( [x, signal, cf1, cf2, bg0, bg1, bg2, bkg, nSig, nBkg] 
                            + items(gaus) + items(sigma) + items(mean) )

    return self.fitResult
    
      

    
  
    

