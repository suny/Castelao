###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import ROOT
from interface import interface
from component import component
import fitUtils

from ROOT import (RooGaussian, RooRealVar, RooPolynomial, RooArgList, 
                  RooAddPdf, RooChebychev, RooAddPdf , RooCBShape)
                  
Minimizer = ROOT.RooFit.Minimizer
                

class Lambda0_2CB_adjusted (interface):
  def fit(self):
    x = self.vars[0]
    mean = {}; sigma = {}; gaus = {}

    mean1 = RooRealVar ("mean1", "mean1", 1115.77, 1115, 1117)
    sigma1 = RooRealVar ("sigma1", "sigma1",  1.07, 0.1, 12)
    alpha1 = RooRealVar ("alpha1", "alpha1", -.58,  -2, -0.1)
    n1 = RooRealVar ("n1", "n1", 5, 1, 20)
    cb1 = RooCBShape("cb1", "cb1", x, mean1, sigma1, alpha1, n1)

    self.saveFromGarbage += [mean1, sigma1, alpha1, n1, cb1]

    alpha2 = RooRealVar ("alpha2", "alpha2", 1.45,  0.1, 2.)
    n2 = RooRealVar ("n2", "n2", 1.9, 1.0, 4)
    cb2 = RooCBShape("cb2", "cb2", x, mean1, sigma1, alpha2, n2)

    self.saveFromGarbage += [ alpha2, n2, cb2]
    for iGaus in [str(i) for i in range(0,3)]:
      m = RooRealVar  ("mean"+iGaus,  "mean"+iGaus,  1116, 1070, 1130)
      s = RooRealVar  ("sigma"+iGaus, "sigma"+iGaus, 1.5,  0.5,  30)
      gaus[iGaus]  = RooGaussian ("gaus"+iGaus,  "gaus"+iGaus,  x, mean1, s)
      mean[iGaus] = m; sigma[iGaus] = s;


    cf1 = RooRealVar ( "cf1", "cf1", .28, 0, 1.)
    cf2 = RooRealVar ( "cf2", "cf2", 1., 0, 1.)
  
    signalCB = RooAddPdf ( "signalL0", "signalGaus", cb1, cb2, cf1 )
    signalWithGaussian = RooAddPdf("signalWithGaussian" ,"", signalCB, gaus['0'], cf2)
    self.saveFromGarbage += [signalCB, signalWithGaussian]

    signal = signalWithGaussian

    bg0 = RooRealVar("bg0", "bg0", 0.24, -1, 1)
    bg1 = RooRealVar("bg1", "bg1", 0, -1, 1)
    bg2 = RooRealVar("bg2", "bg2", 0, -1, 1)

    bkg = RooChebychev ( "bkg", "bkg", x, RooArgList(bg0))  
    
    nTot = self.datahist.sum(False)
    nSig = RooRealVar ( "nSig", "nSig", 0.8 * nTot, 0, nTot)
    nBkg = RooRealVar ( "nBkg", "nBkg", 0.2 * nTot, 0, nTot)


    self.model = RooAddPdf ( "model", "model", RooArgList(signal, bkg) , RooArgList (nSig, nBkg) )

    cf2.setConstant()
    self.fitResult = self.model.fitTo ( self.datahist, ROOT.RooFit.Save(True),
        Minimizer("Minuit", "Simplex") )
    cf2.setConstant(False)
#    self.fitResult = self.model.fitTo ( self.datahist, ROOT.RooFit.Save(True),
#        Minimizer("Minuit", "Simplex") )

    self.fitResult = fitUtils.fit ( self.datahist, self.model, "Minimize" ) 
    self.fitResult = fitUtils.fit ( self.datahist, self.model ) 

    tfSig = signal.asTF ( RooArgList ( x ) )
    tfBkg = bkg.asTF ( RooArgList ( x ) )


    self.components = {
      'Signal'      :  component ( signal , nSig, tfSig, forecolor = ROOT.kRed),
      'Background'  :  component ( bkg    , nBkg, tfBkg, forecolor = ROOT.kBlack),
    }
    

    items = lambda x: [ x[i] for i in x ];
    self.saveFromGarbage +=( [x, signal, cf1, cf2, bg0, bg1, bg2, bkg, nSig, nBkg] 
                            + items(gaus) + items(sigma) + items(mean) )

    return self.fitResult
    
      

    
  
    


