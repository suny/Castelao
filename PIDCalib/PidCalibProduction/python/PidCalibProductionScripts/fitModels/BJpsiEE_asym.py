###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from interface import interface
from component import component
import fitUtils

import ROOT

from ROOT import RooGaussian, RooRealVar, RooPolynomial, RooArgList, RooAddPdf, RooChebychev
from ROOT import RooRealVar, RooGaussian, RooChebychev, RooArgList, RooCBShape
from ROOT import RooAddPdf, RooProdPdf, RooArgusBG, RooExponential

Minimizer = ROOT.RooFit.Minimizer
Save = ROOT.RooFit.Save

class BJpsiEE_asym (interface):
  def fit(self):
    x = self.vars[0]
    y = self.vars[1]

    nTot = self.datahist.sum(False)


    # Define here the structure of the fit: variables and pdfs.
    meanX   = RooRealVar  ( "meanX", "meanX", 3150, 3100, 3200 )
    sigmaX1 = RooRealVar  ( "sigmaX1", "sigmaX1", 100, 5, 150 )
    sigmaX2 = RooRealVar  ( "sigmaX2", "sigmaX2", 100, 5, 150 )
    sigX01  = RooGaussian ( "gausX01", "gausX01", x, meanX, sigmaX1 )
    sigX02  = RooGaussian ( "gausX02", "gausX02", x, meanX, sigmaX2 )

    cfx     = RooRealVar ( "cfx", "cfx", 0.5, 0., 1.)

    sigX    = RooAddPdf ( "signalX", "signalX", sigX01, sigX02, cfx )

    self.saveFromGarbage += [ sigmaX1,sigmaX2,sigX01,sigX02,cfx ]

    bkg0X  = RooRealVar  ( "bkg0X", "bkg0X", -.5, -1, 1)
    bkg1X  = RooRealVar  ( "bkg1X", "bkg1X", -1, -2, 1)
    bkgX   = RooChebychev( "bkgX",  "bkgX",  x, ROOT.RooArgList ( bkg0X , bkg1X ))


    meanY  = RooRealVar   ( "meanY", "meanY", 3080, 3000, 3300 )
    cbsigmaY = RooRealVar ( "cbsigmaY", "cbsigmaY" , 120, 30, 150.0)
    nY = new = RooRealVar ( "nY", "nY", 0.5, 0.1, 3.)
    alphaY   = RooRealVar ( "alphaY", "alphaY", -3.5, -8, 0)
    cballY   = RooCBShape ( "cballY", "cballY", y, meanY, cbsigmaY, alphaY, nY);

    cbsigma1Y = RooRealVar ( "cbsigma1Y", "cbsigma1Y" , 60, 30, 150.0)
    n1Y = new = RooRealVar ( "n1Y", "n1Y", 1., 0.8, 5)
    alpha1Y   = RooRealVar ( "alpha1Y", "alpha1Y", 1.0, 0, 5)
    cball1Y   = RooCBShape ( "cball1Y", "cball1Y", y, meanY, cbsigma1Y, alpha1Y, n1Y);

    sigmaY    = RooRealVar  ( "sigmaY", "sigmaY", 50, 10, 150)
    gausy     = RooGaussian ( "gausy",  "gausy" , y, meanY, sigmaY )

    cfy1 = RooRealVar ( "cfy1", "sigFracY", 0.5, 0, 1)
    cfy2 = RooRealVar ( "cfy2", "sigFracY", 0.5, 0, 1)
    cfy3 = RooRealVar ( "cfy3", "sigFracY", 0.0, 0, 1)
    sigYcbs = RooAddPdf   ( "sigYcbs" , "sigYcbs", cballY, cball1Y, cfy1 )

    self.saveFromGarbage += [sigmaY, gausy, cfy1, cfy2, cfy3, sigYcbs]

    sigY   = RooAddPdf    ( "sigY",  "Signal Y", sigYcbs, gausy, cfy2 )
    bkg0Y  = RooRealVar   ( "bkg0Y", "bkg0Y", -.2, -1,1)
    bkg1Y  = RooRealVar   ( "bkg1Y", "bkg1Y", -1, -2, 1)
    bkgY   = RooChebychev ( "bkgY",  "bkgY",  y, ROOT.RooArgList ( bkg0Y , bkg1Y ))

    # Define one normalization parameter per component that you need.
    # Make sure that the sum of the initial values is nTot.
    nSig  = RooRealVar  ( "nSig", "nSig", 0.67*nTot, 0, nTot )
    nBkg  = RooRealVar  ( "nBkg", "nBkg", 0.33*nTot, 0, nTot )

    # In C++ you usually "delete" objects you don't want any longer.
    # In python you have to mark those you don't want to loose.
    # List here all the objects useful to the fit
    #self.__all__ += [ meanX, sigmaX, bkg0X, bkg1X, bkgX ]

    self.saveFromGarbage += [ meanX, sigX, bkg0X, bkg1X, bkgX ]
    self.saveFromGarbage += [ meanY, cbsigma1Y, n1Y, alpha1Y, cball1Y   , bkg0Y, bkg1Y, bkgY ]
    self.saveFromGarbage += [ cbsigmaY, nY, alphaY, cballY, sigY ]
    self.saveFromGarbage += [ bkg0Y, bkg1Y, bkgY ]

    sig = RooProdPdf ( "signal",  "signal", RooArgList ( sigX, sigY ) )
    bkg = RooProdPdf ( "bkg",     "bkg",    RooArgList ( bkgX,  bkgY  ) )

    self.model = RooAddPdf ( "model", "model", RooArgList ( sig, bkg ) , RooArgList ( nSig, nBkg ))

    self.fitResult = fitUtils.fit ( self.datahist, self.model , "Minimize" ) 



    # Don't forget to list again pdfs and their normalizations here.
    self.components = {
      'Signal'       :  component ( sig , nSig, forecolor = ROOT.kRed   ),
      'Background'   :  component ( bkg , nBkg, forecolor = ROOT.kBlack ),
    }

    return self.fitResult

