###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from interface import interface
from component import component

import ROOT
from ROOT import gROOT

from ROOT import RooRealVar, RooAddPdf, RooArgList, RooGaussian, RooCBShape, RooArgusBG, RooFormulaVar, RooFFTConvPdf, RooExponential
from ROOT.RooFit import RooConst

fitmodel_name = "BifurcatedCB"
import os
fitmodels_dir = os.path.dirname(os.path.abspath(__file__))
fitmodel_filename = ".x " + fitmodels_dir + "/" + fitmodel_name + ".cxx+"
gROOT.ProcessLine(fitmodel_filename)

from ROOT import BifurcatedCB


class B2KstGamma(interface):
    def fit(self):
        x = self.vars[0]

        #Param init-------------

        _ntot = self.datahist.sum(False)

        _nbkg = _ntot*0.59
        _a = -5e-4        

        _nsig = _ntot*0.21
        _mean = 5280
        _sigma = 90
        _aL = 2.19
        _nL = 1.32
        _aR = -1.77
        _nR = 6.16                   

        _npr1 = _ntot*0.14
        _mshift1 = 280
        _argus1_c = 1
        _argus1_p = 1

        _npr2 = _ntot*0.06
        _mshift2 = 140
        _argus2_c = -13.7        
        
        #Bkg-------------

        a = RooRealVar("a","a",_a, _a*10, _a*0.1);
        pdfBkg = RooExponential("pdfBkg","pdfBkg",x,a);
        nBkg = RooRealVar("nBkg","nBkg", _nbkg, _nbkg*0.1, _nbkg*10);

        #Signal-------------

        mean = RooRealVar("mean","mean",_mean,_mean-30,_mean+30);
        sigma = RooRealVar("sigma","sigma",_sigma,_sigma-15,_sigma+15);

        aL = RooRealVar("aL","aL",_aL);
        nL = RooRealVar("nL","nL",_nL);
        aR = RooRealVar("aR","aR",_aR);
        nR = RooRealVar("nR","nR",_aL);

        pdfSig = BifurcatedCB("pdfSig","pdfSig",x,mean,sigma,aL,nL,aR,nR);
        nSig = RooRealVar("nSig","nSig", _nsig, _nsig*0.1, _nsig*10);

        #Part-reco1 B2Xpi0---------

        x.setBins(10000,"cache") ;
        Normal = RooGaussian("Normal","Normal",x,RooConst(0.),sigma);

        mshift1 = RooRealVar("mshift1","mshift1",_mshift1);

        argus1_m0 = RooFormulaVar("argus1_m0","argus1_m0","mean-mshift1",RooArgList(mean,mshift1));
        argus1_c = RooRealVar("argus1_c","argus1_c",_argus1_c);
        argus1_p = RooRealVar("argus1_p","argus1_p",_argus1_p,-_argus1_p,_argus1_p+25);

        pdfPR10 = RooArgusBG("pdfPR10","pdfPR10",x,argus1_m0,argus1_c,argus1_p) ;
        pdfPR1 = RooFFTConvPdf("pdfPR1","pdfPR1",x,pdfPR10,Normal);
        pdfPR1.setBufferFraction(0.95);
        nPR1 = RooRealVar("nPR1","nPR1", _npr1, _npr1*0.2, _npr1*5);                               

        #Part-reco2 B2pipig+pi--------

        mshift2 = RooRealVar("mshift2","mshift2",_mshift2);

        argus2_m0 = RooFormulaVar("argus2_m0","argus2_m0","mean-mshift2",RooArgList(mean,mshift2));
        argus2_c = RooRealVar("argus2_c","argus2_c",_argus2_c);

        pdfPR20 = RooArgusBG("pdfPR20","pdfPR20",x,argus2_m0,argus2_c);
        pdfPR2 = RooFFTConvPdf("pdfPR2","pdfPR2",x,pdfPR20,Normal);
        pdfPR2.setBufferFraction(0.95);
        nPR2 = RooRealVar("nPR2","nPR2", _npr2, _npr2*0.2, _npr2*5);

        #Model----------------
        self.model = RooAddPdf("model", "model",RooArgList(pdfBkg, pdfPR1, pdfPR2, pdfSig),RooArgList(nBkg, nPR1, nPR2, nSig))

        #Fit------------------

        self.fitResult = self.model.fitTo(self.datahist,ROOT.RooFit.Save(True))

        self.components = {
            'Signal': component(pdfSig, nSig),
            'PR1': component(pdfPR1, nPR1),
            'PR2': component(pdfPR2, nPR2),
            'Background': component(pdfBkg, nBkg)
        }

        self.saveFromGarbage = [x, pdfSig, pdfBkg, pdfPR1, pdfPR2, nSig, nBkg, nPR1, nPR2 ]
        self.saveFromGarbage = [Normal, pdfPR10, pdfPR20]
        self.saveFromGarbage += [a, mean, sigma, aL, nL, aR, nR, mshift1, argus1_m0, argus1_c, argus1_p, mshift2, argus2_m0, argus2_c]

        return self.fitResult
