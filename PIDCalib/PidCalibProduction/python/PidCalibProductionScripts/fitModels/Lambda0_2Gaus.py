###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import ROOT
from interface import interface
from component import component
import fitUtils

from ROOT import (RooGaussian, RooRealVar, RooPolynomial, RooArgList, 
                  RooAddPdf, RooChebychev, RooAddPdf )
                  
                

class Lambda0_2Gaus (interface):
  def fit(self):
    x = self.vars[0]
    mean = {}; sigma = {}; gaus = {}
    for iGaus in [str(i) for i in range(0,2)]:
      m = RooRealVar  ("mean"+iGaus,  "mean"+iGaus,  1116, 1070, 1130)
      s = RooRealVar  ("sigma"+iGaus, "sigma"+iGaus, 1.5,  0.5,  30)
      gaus[iGaus]  = RooGaussian ("gaus"+iGaus,  "gaus"+iGaus,  x, m, s)
      mean[iGaus] = m; sigma[iGaus] = s;


    cf = RooRealVar ( "cf", "cf", 1., 0, 1.)
  
    signal = RooAddPdf ("L0signal", "", RooArgList ( gaus['0'], gaus['1']  ),
                                      RooArgList ( cf ) )


    bg0 = RooRealVar("bg0", "bg0", 0, -3, 3)
    bg1 = RooRealVar("bg1", "bg1", 0, -100, 100)
    bg2 = RooRealVar("bg2", "bg2", 0, -1, 1)

    bkg = RooChebychev ( "bkg", "bkg", x, RooArgList(bg0))#, bg1, bg2) )
    
    
    nTot = self.datahist.sum(False)
    nSig = RooRealVar ( "nSig", "nSig", 0.8 * nTot, 0, nTot)
    nBkg = RooRealVar ( "nBkg", "nBkg", 0.2 * nTot, 0, nTot)


    self.model = RooAddPdf ( "model", "model", RooArgList(signal, bkg) , RooArgList (nSig, nBkg) )
#    self.fitResult = self.model.fitTo ( self.datahist, ROOT.RooFit.Save(True) )
    self.fitResult = fitUtils.fit ( self.datahist, self.model ) 

    tfSig = signal.asTF ( RooArgList ( x ) )
    tfBkg = bkg.asTF ( RooArgList ( x ) )

    self.components = {
      'Signal'      :  component ( signal , nSig, tfSig),
      'Background'  :  component ( bkg    , nBkg, tfBkg),
    }
    

    items = lambda x: [ x[i] for i in x ];
    self.saveFromGarbage =( [x, signal, cf, bg0, bg1, bg2, bkg, nSig, nBkg] 
                            + items(gaus) + items(sigma) + items(mean) )

    return self.fitResult
    
      

    
  
    
