###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import ROOT


Components = ROOT.RooFit.Components
LineWidth = ROOT.RooFit.LineWidth
LineColor = ROOT.RooFit.LineColor
MarkerColor = ROOT.RooFit.MarkerColor
MarkerSize = ROOT.RooFit.MarkerSize
FillColor = ROOT.RooFit.FillColor
FillStyle = ROOT.RooFit.FillStyle
ROOT.gStyle.SetTitleFont ( 132 )
ROOT.gStyle.SetLabelFont ( 132 )

class interface:
  def __init__ ( self, vars, datahist ):
    self.vars     = vars
    self.datahist = datahist
    self.fitResult = None
    self.components = {}
    self.saveFromGarbage = []

  def getFitterStatus ( self, fitResult, checkHesse = False ):
    if self.fitResult == None: return "FIT DISABLED"

    migradstate = self.fitResult.statusCodeHistory(0)

    if migradstate != 0: return 'FAILED'

    if checkHesse:
      hessstate   = self.fitResult.statusCodeHistory(1)
      if hessstate   != 0: return 'WRONG ERRORS'

    return 'SUCCESS'

  def normOnlyFit(self):
    parameters = self.model.getParameters ( self.datahist )

    parIter = parameters.createIterator()
    parameter = parIter.Next()
    while parameter:
      parameter.setConstant()
      parameter = parIter.Next()
  
    for c in self.components :
      self.components [ c ].nFromFit.setConstant ( False )

    return  self.model.fitTo ( self.datahist, 
                                        ROOT.RooFit.Save(True), 
                                        ROOT.RooFit.Minos (True),
                                        ROOT.RooFit.Extended (True),
                                        ROOT.RooFit.Minimizer("Minuit", "Minimize"))
    

  def report(self, outputdir, outputfile, comments = None ):
    outputdir += '/'
    _counter = 0

    if comments == None: comments = ""

    if (self.fitResult == None):    
      self.fit()

    color = "#FFFFFF"
    if self.getFitterStatus ( self.fitResult ) != 'SUCCESS':
      color = "#FFEEEE"

    of = file ( outputdir + outputfile + '.html' , "w")
    of.write ("<HTML><HEAD><TITLE>" + outputfile + "</TITLE></HEAD>")
    of.write ("<BODY bgcolor = {}>".format ( color ) )

    chisquares = []

    for x in self.vars:
      frame = x.frame()
      self.datahist.plotOn ( frame , MarkerSize ( 0.1 ) )
      for category in self.components:
        comp = self.components [ category ]
        self.model.plotOn ( frame, Components  ( comp.getPdf()  ) 
                                 , LineWidth   (1)
                                 , LineColor   ( comp.forecolor )
#                                 , MarkerColor ( comp.forecolor )
                                 , FillColor   ( comp.fillcolor )
                                 , FillStyle   ( comp.fillstyle )
        )

      ROOT.gStyle.SetOptTitle ( 0 )

      self.datahist.plotOn ( frame , MarkerSize ( 0.1 ) )
      self.model.plotOn ( frame, LineWidth ( 2 ), LineColor ( ROOT.kBlue ) )

      frame.Draw()
      outputimg = "img/fig{file}-{counter}-{var}".format ( 
                                                      file    = outputfile,
                                                      counter = _counter,
                                                      var     = x.GetName(),
                                                    )
      def imgfile ( type, tag = None):
        if tag == None : tag = "" 
        else           : tag = "_" + tag

        return outputimg + tag + "." + type

      ROOT.gPad.SaveAs ( outputdir + imgfile ( 'pdf' ) ) 
      ROOT.gPad.SaveAs ( outputdir + imgfile ( 'png' ) ) 
      ROOT.gPad.SaveAs ( outputdir + imgfile ( 'root' ) ) 
      ROOT.gPad.SaveAs ( outputdir + imgfile ( 'C' ) ) 

      of.write ( "<A href='{pdf}'><IMG src='{png}' width = 48%></A>".format (
                  pdf = imgfile ( 'pdf' ),
                  png = imgfile ( 'png' )
               ))

      pullh = frame.pullHist()
      pullf = x.frame()
      pullf.addPlotable ( pullh, 'P' )
      pullf.Draw()

      of.write ( "<A href='{pdf}'><IMG src='{png}' width = 48%></A>".format (
                  pdf = imgfile ( 'pdf' , 'pull' ),
                  png = imgfile ( 'png' , 'pull' )
               ))
      ROOT.gPad.SaveAs ( outputdir + imgfile ( 'pdf', 'pull' ) ) 
      ROOT.gPad.SaveAs ( outputdir + imgfile ( 'png', 'pull' ) ) 
      ROOT.gPad.SaveAs ( outputdir + imgfile ( 'root', 'pull' ) ) 
      ROOT.gPad.SaveAs ( outputdir + imgfile ( 'C', 'pull' ) ) 


      
      of.write ( "\n<TABLE width = 50%>" )
      of.write ( "\n<TR><TD> Preliminary fit status: <TD>" + self.getFitterStatus(self.fitResult) )

      yld = {}; errors = {}
      normOnlyFitResult = self.normOnlyFit()
      for category in self.components:
        yld    [ category ] = self.components [ category ].getYield()
        errors [ category ] = self.components [ category ].getError()
        
        
      errorMessage = ""
      if yld [ category ] > errors [ category ]**5 :
        errorMessage = " <b>Uncertainty smaller than Poisson expectations</b>"


      if self.fitResult:
        for status in range(0, self.fitResult.numStatusHistory()):
          of.write ( "\n<TR><TD> <B>Preliminary fit </B> " + self.fitResult.statusLabelHistory(status)
                     + ": <TD>" + str( self.fitResult.statusCodeHistory(status) )  )

      for status in range(0, normOnlyFitResult.numStatusHistory()):
        of.write ( "\n<TR><TD> <B>Norm-Only fit </B> " + normOnlyFitResult.statusLabelHistory(status)
                   + ": <TD>" + str( normOnlyFitResult.statusCodeHistory(status) )  )

      of.write ( "\n<TR><TD> Number of candidates: <TD> {:.0f}".format(self.datahist.sum( False ) ))
      of.write ( "\n<TR><TD> Number of bins: <TD>"       + str ( x.getBins() ) )
      of.write ( "\n<TR><TD> Fit &chi;^{2}/ndf: <TD>"+"{:.2f}".format( frame.chiSquare()) )
      for category in self.components:
        comp = self.components [ category ]
        of.write ( "\n<TR><TD> Yield {} <TD> {:.2f} &pm; {:.2f} {}".format( 
                                    category, yld [ category ], errors [ category ] ,
                                    errorMessage))
      of.write ( "\n</TABLE>" )

      chisquares += [ frame.chiSquare() ]
    
    of.write ("<P><B>Comments</B>")
    of.write ("<PRE id=cut>" + comments + "</PRE>")

    of.write ("</BODY></HTML>")

    species = ROOT.RooArgList()
    index = {}
    for category in self.components:
      parname = self.components [ category ].nFromFit.GetName()
      index[category] = normOnlyFitResult.floatParsFinal().index ( parname )

    
    covMat = normOnlyFitResult.covarianceMatrix ( )
    covDict = {}
    for c1 in self.components:
      covDict[c1] = {}
      covmatLine = 0
      for c2 in self.components:
        covDict[c1][c2] = covMat[index[c1]][index[c2]]
        covmatLine += covMat[index[c1]][index[c2]]

#      print self.components [ c1 ].nFromFit.GetName()
#      print str(c1) + " => "+ str(covmatLine)
        
      
      

    return {
      'STATUS'     :  self.getFitterStatus(self.fitResult) ,
      'CANDIDATES' :  self.datahist.sum( False )           ,
      'CHI2'       :  chisquares                           ,
      'CATEGORIES' :  self.components                      ,
      'HTMLREPORT' :  outputfile + ".html"                 ,
      'COVMATRIX'  :  covDict                              ,
    }


