###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from interface import interface
from component import component

import ROOT

from ROOT import RooCBShape, RooRealVar, RooPolynomial, RooArgList, RooAddPdf, RooChebychev
"""
   1  alpha        2.21562e+00   2.51685e-02   4.96881e-03  -8.92174e-01
   2  bg0         -1.01918e-01   2.45389e-03   3.34984e-03   3.24369e+00
   3  mean         3.09978e+03   7.68416e-02   1.08551e-02   2.95836e+00
   4  n            7.91365e-01   9.81996e-02   1.26737e-02  -1.31851e+00
   5  nBkg         8.56581e+05   1.55685e+03   5.53456e-03   8.80445e-01
   6  nSig         1.10747e+05   1.30365e+03   2.84359e-03  -7.16364e+00
   7  sigma        1.35801e+01   8.26916e-02   7.73430e-03   3.27439e+00
"""

Minimizer = ROOT.RooFit.Minimizer

class Jpsi_1CB (interface):
  def fit(self):
    x = self.vars[0]
    mean = RooRealVar ("mean", "mean", 3096, 3085, 3110)
    sigma = RooRealVar ("sigma", "sigma", 15, 1.0, 30)
    alpha = RooRealVar ("alpha", "alpha", 2., .0, 10.)
    n = RooRealVar ("n", "n", 1.0, 0.1, 10)
    cb = RooCBShape("cb", "cb", x, mean, sigma, alpha, n)

    bg0 = RooRealVar("bg0", "bg0", -0.1, -1, 1)
    bg1 = RooRealVar("bg1", "bg1", 0, -100, 100)
    bg2 = RooRealVar("bg2", "bg2", 0, -1, 1)

    bkg = RooChebychev ( "bkg", "bkg", x, RooArgList(bg0) )
    
    
    nTot = self.datahist.sum(False)
    nSig = RooRealVar ( "nSig", "nSig", 0.8 * nTot, 0, nTot)
    nBkg = RooRealVar ( "nBkg", "nBkg", 0.2 * nTot, 0, nTot)


    self.model = RooAddPdf ( "model", "model", RooArgList(cb, bkg) , RooArgList (nSig, nBkg) )
    self.fitResult = self.model.fitTo ( self.datahist, ROOT.RooFit.Save(True) )
    self.fitResult = self.model.fitTo ( self.datahist, ROOT.RooFit.Save(True), Minimizer("Minuit", "Simplex" ))

    tfSig = cb.asTF ( RooArgList ( x ) )
    tfBkg = bkg.asTF ( RooArgList ( x ) )

    self.components = {
      'Signal'      :  component ( cb , nSig, tfSig),
      'Background'  :  component ( bkg  , nBkg, tfBkg),
    }
    

    self.saveFromGarbage = [x, mean, sigma, alpha, n, cb, bg0, bg1, bg2, bkg, nSig, nBkg]

    return self.fitResult
    
      

    
  
    
