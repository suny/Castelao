###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from interface import interface
from component import component
import fitUtils

import ROOT
from component import component
from ROOT import RooRealVar, RooGaussian, RooChebychev, RooArgList
from ROOT import RooAddPdf

import Ostap.Fixes
import Ostap.FitModels as Models
from   Ostap.PyRoUts  import cpp


# Contact: Ivan.Polyakov@cern.ch

class Phi_RBW (interface):
  def fit(self):
    x = self.vars[0]
    nTot = self.datahist.sum(False);
    # Define here the structure of the fit: variables and pdfs. 
    mean_phi  = RooRealVar  ( "mean_phi", "mean_phi", 1019.46, 1015, 1025 )
    sigma_phi = RooRealVar  ( "sigma_phi", "sigma_phi", 4., 0.5, 10 )
    gaus  = RooGaussian ( "gaus", "gaus", x, mean_phi, sigma_phi )

    bw    = cpp.Gaudi.Math.BreitWigner( 1019.46 , 4.27 , 493.68 , 493.68 , 1 )
    relbw = Models.BreitWigner_pdf ( 'Phi'	,
                                bw		,
                                mass  = x	,
                                mean  = mean_phi,
                                gamma = 4.27  ,
				convolution = 1.2,
				useFFT = True	,
                                )


    bkg0  = RooRealVar  ( "bkg0", "bkg0", 0, -1, 1)
    bkg1  = RooRealVar  ( "bkg1", "bkg1", 0, -1, 1)
    bkg   = RooChebychev( "bkg",  "bkg",  x, ROOT.RooArgList ( bkg0, bkg1 ))

    # Define one normalization parameter per component that you need.
    # Make sure that the sum of the initial values is nTot.
    nSig  = RooRealVar  ( "nSig", "nSig", 0.2*nTot, 0, nTot )
    nBkg  = RooRealVar  ( "nBkg", "nBkg", 0.8*nTot, 0, nTot )
    
    self.saveFromGarbage = [ mean_phi , relbw , bkg0, bkg1, bkg ]

    # List here the components through the "component" class. 
    # You are very welcome to add new components, the only requirement
    # is that there has to be one and only one signal component named
    # 'Signal'.
    # Don't forget to list again pdfs and their normalizations here.

#    model = RooAddPdf ( "model", "model", RooArgList ( gaus , bkg ) , RooArgList ( nSig, nBkg ))
    self.model = RooAddPdf ( "model", "model", RooArgList ( relbw.pdf , bkg ) , RooArgList ( nSig, nBkg ))
#    self.fitResult = self.model.fitTo ( self.datahist, ROOT.RooFit.Save(True) )
    self.fitResult = fitUtils.fit ( self.datahist, self.model , "Migrad" ) 


    self.components = {
#      'Signal'       :  component ( gaus  , nSig, forecolor = ROOT.kRed   ),
      'Signal'       :  component ( relbw.pdf  , nSig, forecolor = ROOT.kRed   ),
      'Background'   :  component ( bkg   , nBkg, forecolor = ROOT.kBlack ),
    }


    return self.fitResult
    
      

    
  
    

