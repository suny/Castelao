###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from interface import interface
from component import component
import fitUtils

import ROOT

from ROOT import RooGaussian, RooRealVar, RooPolynomial, RooArgList, RooAddPdf, RooChebychev
from ROOT import RooRealVar, RooGaussian, RooChebychev, RooArgList, RooCBShape, RooKeysPdf
from ROOT import RooAddPdf, RooProdPdf, RooArgusBG, RooExponential

Minimizer = ROOT.RooFit.Minimizer
Save = ROOT.RooFit.Save

class BJpsiEE_1D (interface):
  def fit(self):
    x = self.vars[0]

    # Define here the structure of the fit: variables and pdfs.
    #====
    mean    = RooRealVar ("mean" , "mean" , 5280, 5000,5500,"MeV");
    sigma   = RooRealVar ("sigma", "sigma", 9, 3, 17, "MeV");
    sigma2  = RooRealVar ("sigma2", "sigma2", 13, 7, 15, "MeV");
    al      = RooRealVar ("alpha_left", "alpha_left",0.6,0.01,2);
    ar      = RooRealVar ("alpha_right", "alpha_right", -0.7,-2,-0.01);
    nl      = RooRealVar ("n_left", "n_left",2,1,15);
    nr      = RooRealVar ("n_right", "n_right", 2,1,15);
    fracCB2 = RooRealVar ("fracCB2", "fracCB2", 0.5, 0.01,0.99);

    #constPars = [nl, nr]
    constPars = [al, ar]
    for par in constPars: par.setConstant(True)

    cb1 = RooCBShape ("cb1", "cb1", x, mean, sigma, al, nl);
    cb2 = RooCBShape ("cb2", "cb2", x, mean, sigma, ar, nr);
    sig = RooAddPdf ("sig", "sig", RooArgList(cb1, cb2 ), RooArgList(fracCB2));

    lam  = RooRealVar ("lam","lam",-5e-04,-0.01, 0.00);
    bkg = RooExponential ("bkg", "bkg", x,  lam);

    # Define one normalization parameter per component that 0.2 need.
    # Make sure that the sum of the initial values is nTot.                    
    nTot = self.datahist.sum(False)
    nSig  = RooRealVar  ( "nSig", "nSig", 0.67*nTot, 0, 1.1*nTot )
    nBkg  = RooRealVar  ( "nBkg", "nBkg", 0.33*nTot, 0, 1.1*nTot )

    # In C++ you usually "delete" objects you don't want any longer.
    # In python you have to mark those you don't want to loose.
    # List here all the objects useful to the fit
    self.saveFromGarbage += [ mean, sigma, sigma2, al, ar,
                              nl, nr, fracCB2, cb1, cb2, sig,
                              lam, bkg]
               
    # Don't forget to list again pdfs and their normalizations here.
    self.model = RooAddPdf ( "model", "model", RooArgList ( sig, bkg ) , RooArgList ( nSig, nBkg ))
                                 
    
    self.fitResult = self.model.fitTo ( self.datahist, ROOT.RooFit.Save(True), Minimizer("Minuit2", "Migrad"))
    self.fitResult = self.model.fitTo ( self.datahist, ROOT.RooFit.Save(True), Minimizer("Minuit", "Simplex"))
    
    
    self.components = {
      'Signal'       :  component ( sig  , nSig, forecolor = ROOT.kRed   ),
      'Background'   :  component ( bkg  , nBkg, forecolor = ROOT.kBlack ),
      }

    # Print the fraction of signal out of the fit window
    xmin = 5170.
    x.setRange('out', 4000., xmin)
    x.setRange('tot', 4000., 7000.)
    outfrac = sig.createIntegral(ROOT.RooArgSet(x), ROOT.RooFit.Range('out'))
    totfrac = sig.createIntegral(ROOT.RooArgSet(x), ROOT.RooFit.Range('tot'))
    print 'The fraction of signal below {} is '.format(xmin), outfrac.getValV()/totfrac.getValV()
                                                   
    return self.fitResult
