###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from interface import interface
from component import component

import ROOT

from ROOT import RooGaussian, RooRealVar, RooPolynomial, RooArgList, RooAddPdf, RooChebychev, RooProdPdf, RooAddPdf, RooGenericPdf, RooCBShape

class OmegaL(interface):
  def fit(self):
#    M_PPi = self.vars[0]
    M_LK = self.vars[0]
 
    ###########################################################################
    # P Pi - No longer fitting, model left in for plotting
    ###########################################################################
#    flat1 = RooRealVar("flat1", "flat1", 0, -10, 10)
#    signalPPi = RooChebychev ( "signalPPi", "signalPPi", M_PPi, RooArgList(flat1) )
#    flat2 = RooRealVar("flat2", "flat2", 0, -10, 10)
#    bkgPPi = RooChebychev ( "bkgPPi", "bkgPPi", M_PPi, RooArgList(flat2) )

    ###########################################################################
    # Lambda K - fit model
    ###########################################################################
    # Signal DCB 
    meanLK   = RooRealVar ("meanLK", "meanLK", 1672.5, 100, 3000)
    sigmaLK  = RooRealVar ("sigmaLK", "sigmaLK", 1.658, 0, 4)
    alphaLK = RooRealVar ("alphaLK", "alphaLK", 5.068, -10000, 1000)
    n = RooRealVar ("n", "n", 1)#, 0, 100)#,0, 100)
    cbLK = RooCBShape("cbLK", "cbLK", M_LK, meanLK, sigmaLK, alphaLK, n)
    
    meanLK2   = RooRealVar ("meanLK2", "meanLK2", 1673.5, 100, 3000)
    sigmaLK2 = RooRealVar ("sigmaLK2", "sigmaLK", 1.2, 0, 3)
    alphaLK2 = RooRealVar ("alphaLK2", "alphaLK2", -5.068, -1000, 1000)
    cbLK2 = RooCBShape("cbLK2", "cbLK2", M_LK, meanLK, sigmaLK2, alphaLK2, n)
    
    cfLK   = RooRealVar ("cfLK", "cfLK", .50, 0, 1) 
    signalLK = RooAddPdf  ("signalLK", "signalLK", RooArgList ( cbLK, cbLK2), RooArgList ( cfLK ) )

    # Background 2nd Order Chebychev
    bg0LK = RooRealVar("bg0LK", "bg0LK", 0.102, -10, 10)
    bg1LK = RooRealVar("bg1LK", "bg1LK", 0.0, -10, 10)
    #
    bkgLK = RooChebychev ( "bkgLK", "bkgLK", M_LK, RooArgList(bg0LK, bg1LK) )

    # Make Yields  
    nTot = self.datahist.sum(False)
    nSig = RooRealVar ( "nSig", "nSig", 0.80 * nTot, 0, nTot)
    nCmb = RooRealVar ( "nCmb", "nCmb", 0.20 * nTot, 0, nTot)

    # Set model as fitting in LK dimension only  
    self.model = RooAddPdf ( "model", "model",RooArgList(signalLK, bkgLK) , RooArgList (nSig, nCmb) )
    self.fitResult = self.model.fitTo ( self.datahist, ROOT.RooFit.Save(True) )
    
   
    # Set self.model as 2D for plotting purposes (hacky solution)
#    signal = RooProdPdf ( "signal", "signal", RooArgList ( signalPPi, signalLK ) )
#    combo  = RooProdPdf ( "combo", "combo", RooArgList ( bkgPPi, bkgLK ) ) 
#    self.model = RooAddPdf ( "model", "model", RooArgList(signal, combo) , RooArgList (nSig, nCmb) )
    
    ###########################################################################
    # Plot result
    ###########################################################################
#    tfSig = signal.asTF ( RooArgList ( M_PPi,  M_LK ) )
#    tfCmb = combo.asTF  ( RooArgList ( M_PPi, M_LK )  )
 
    self.components = {
      'Signal'       :  component ( signalLK , nSig , forecolor = ROOT.kRed ),
      'Background'   :  component ( bkgLK  , nCmb , forecolor = ROOT.kBlack ),
    }
    
    self.saveFromGarbage = [
			    n,alphaLK,alphaLK2,
			    meanLK2,
			    signalLK,
                            meanLK, sigmaLK, cbLK2, 
                            bg0LK, bg1LK, bkgLK, 
                            nSig, nCmb,
                            signalLK, cfLK, cbLK, sigmaLK2]

    return self.fitResult
