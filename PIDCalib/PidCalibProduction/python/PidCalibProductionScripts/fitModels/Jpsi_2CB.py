###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from interface import interface
from component import component

import ROOT

from ROOT import RooCBShape, RooRealVar, RooPolynomial, RooArgList, RooAddPdf, RooChebychev, RooAddPdf
from ROOT import RooExponential
Minimizer = ROOT.RooFit.Minimizer
"""
   1  alpha1       1.00001e+00   2.05902e-02   2.61456e-01  -1.57623e+00
   2  alpha2      -1.23399e+00   2.86385e-02   6.12400e-02   3.37777e+00
   3  bg0         -6.61822e-04   6.26478e-06   1.89964e-07  -6.61822e-06
   4  cf           5.09802e-01   1.61561e-02   6.03191e-02   3.12199e+00
   5  mean1        3.09960e+03   4.36932e-02   9.29091e-03   1.68929e-01
   6  n1           3.43162e+00   2.69148e-01   9.36975e-02  -8.94267e-01
   7  n2           1.49367e+01   1.96702e+00   5.00000e-01   1.43622e+00
   8  nBkg         2.51317e+06   2.32718e+03   5.15643e-03   7.54428e-01
   9  nSig         4.70058e+05   1.84500e+03   2.93757e-03  -7.54419e-01
  10  sigma1       1.24561e+01   2.20874e-01   1.48561e-02  -2.11499e-01
  11  sigma2       1.27059e+01   2.03838e-01   1.36433e-02  -2.94768e+00
"""

class Jpsi_2CB (interface):
  def fit(self):
    x = self.vars[0]
    mean1 = RooRealVar ("mean1", "mean1", 3100, 3085, 3110)
    sigma1 = RooRealVar ("sigma1", "sigma1", 8, 1.0, 30)
    alpha1 = RooRealVar ("alpha1", "alpha1", 1.4, 0.01, 2.)
    n1 = RooRealVar ("n1", "n1", 5, 2, 30)
    cb1 = RooCBShape("cb1", "cb1", x, mean1, sigma1, alpha1, n1)

#    mean2 = RooRealVar ("mean2", "mean2", 3100, 3085, 3110)
    sigma2 = RooRealVar ("sigma2", "sigma2", 15,1,30)#-15, -30, -1.)
    alpha2 = RooRealVar ("alpha2", "alpha2", -1.5, -2., 0.)
    n2 = RooRealVar ("n2", "n2", 2, 1.0, 30)
    cb2 = RooCBShape("cb2", "cb2", x, mean1, sigma2, alpha2, n2)

#    for par in [alpha2, n2]:
#      par.setConstant()

    cf = RooRealVar ( "cf", "cf", 1., 0, 1)
    cf.setConstant() ; 
    cb = RooAddPdf  ( "cb", "cb", RooArgList( cb1, cb2 ), RooArgList (cf))

    bg0 = RooRealVar("bg0", "bg0", 0, -100, 100)
    bg1 = RooRealVar("bg1", "bg1", 0, -100, 100)
    bg2 = RooRealVar("bg2", "bg2", 0, -1, 1)

#    bkg = RooChebychev ( "bkg", "bkg", x, RooArgList(bg0, bg1, bg2) )
    bkg = RooExponential( "bkg", "bkg", x, bg0 )
    
    
    nTot = self.datahist.sum(False)
    nSig = RooRealVar ( "nSig", "nSig", 0.6 * nTot, 0, nTot)
    nBkg = RooRealVar ( "nBkg", "nBkg", 0.4 * nTot, 0, nTot)


    self.model = RooAddPdf ( "model", "model", RooArgList(cb, bkg) , RooArgList (nSig, nBkg) )

    self.model.fitTo ( self.datahist )#, Minimizer("Minuit2","Migrad"))
    cf.setConstant(False);
    for par in [alpha1, n1, sigma1, mean1, sigma2, cf]:
      par.setConstant()

    self.fitResult = self.model.fitTo ( self.datahist )

    for par in [n1, sigma1, mean1, sigma2, cf]:
      par.setConstant(False)
    fitResult = self.fitResult = self.model.fitTo ( self.datahist, ROOT.RooFit.Save(True), Minimizer("Minuit","Simplex") )

    tfSig = cb.asTF ( RooArgList ( x ) )
    tfBkg = bkg.asTF ( RooArgList ( x ) )

    self.components = {
      'Signal'      :  component ( cb   , nSig, tfSig),
      'Background'  :  component ( bkg  , nBkg, tfBkg),
    }
    

    self.saveFromGarbage =  [x, cf, cb, bg0, bg1, bg2, bkg, nSig, nBkg]
    self.saveFromGarbage += [mean1, sigma1, alpha1, n1, cb1]
    self.saveFromGarbage += [       sigma2, alpha2, n2, cb2]

    return self.fitResult
    
      

    
  
    
