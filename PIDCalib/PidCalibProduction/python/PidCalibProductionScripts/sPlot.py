###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
## PIDCalib/sPlot.py                                                         ##
##                                                                           ##
##  Produces and stores the sWeights for the PIDCalib samples.               ##
############################################################################### 

import ROOT
ROOT.gROOT.SetBatch()

from multiprocessing import Pool

from ROOT import RooRealVar, RooDataHist, RooArgList
from PidCalibProduction import StandardOfflineRequirements as StdCut

from fitModels import Dstar_4comp as Dstar
#from fitModels import Lambda0_2CB as Lambda0
from fitModels import Lambda0_2CB_adjusted as Lambda0
from fitModels import Lambda0_2CB_adjusted as Lambda0_DD
from fitModels import Lambda0_CB as Lambda0_CB
from fitModels import Lambda0_adjusted as Lambda0_VHPT_DD
#from fitModels import Jpsi_1CB as Jpsi
from fitModels import Jpsi_2CB as Jpsi
from fitModels import Phi_RBW as Phi
from fitModels import Dstar4body as Dstar4body
from fitModels import DsPhiNoTag as DsPhiNoTag
from fitModels import DsPhi as DsPhi
from fitModels import BJpsi as BJpsi
from fitModels import BJpsi_NoPR as BJpsi_NoPR
#from fitModels import BJpsiEE_delta as BJpsiEE
from fitModels import BJpsiEE_1D as BJpsiEE
from fitModels import Ks0 as Ks0
from fitModels import Ks0DD as Ks0DD
from fitModels import Lc as Lc
from fitModels import InclLc as InclLc
from fitModels import DsPhiMuMuPi as DsPhiMuMuPi
from fitModels import OmegaL as OmegaL
from fitModels import OmegaD as OmegaD

################################################################################
## Configuration (default)                                                    ##
################################################################################
class default_config:
  def __init__ (s):
    s.monitoring_dir = "./www"
    s.monitoring_key = ""
    s.inputfiles = [ ]
    s.outputdir  = "."

    s.samples = {


      #============ Kaons - Pions ===============================#
      
      'RSDStCalib_Pos' :  {
        'CUT'             : StdCut.DstCut,
                          ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "D*(2010)+",
        'FITVARS'         : "CHILD(M,1):M-CHILD(M,1)",
        'HISTOGRAM'       : ["Hlt2PIDD02KPiTagTurboCalib/DstDz_Pos"],
        'FITMODEL'        : Dstar,
      },

      'RSDStCalib_Neg' :  {
        'CUT'             : StdCut.DstCut,
                          ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "D*(2010)-",
        'FITVARS'         : "CHILD(M,1):M-CHILD(M,1)",
        'HISTOGRAM'       : ["Hlt2PIDD02KPiTagTurboCalib/DstDz_Neg"],
        'FITMODEL'        : Dstar,
      },


      'KS0LLCalib' : {
        'CUT'             : StdCut.TrackGhostProb,
                          ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "KS0",
        "FITVARS"         : "M",
        "HISTOGRAM"       : ["Hlt2PIDKs2PiPiLLTurboCalib/KS0LL"],
        'FITMODEL'        : Ks0,
      },

      'KS0DDCalib' : {
        'CUT'             : StdCut.TrackGhostProb,
                          ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "KS0",
        "FITVARS"         : "M",
        "HISTOGRAM"       : ["Hlt2PIDKs2PiPiDDTurboCalib/KS0DD"],
        'FITMODEL'        : Ks0DD,
      },

      'DsPhiKKCalib_Pos' : {
        'CUT'             : StdCut.TrackGhostProb,
                          ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "[D_s+]cc",
        'FITVARS'         : "CHILD(M,1):M",
        'HISTOGRAM'       : [ "Hlt2PIDDs2PiPhiKKNegTaggedTurboCalib/DsPhiKK_Pos" ],
        'FITMODEL'        : DsPhi,
      },

      'DsPhiKKCalib_Neg' : {
        'CUT'             : StdCut.TrackGhostProb,
                          ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "[D_s+]cc",
        'FITVARS'         : "CHILD(M,1):M",
        'HISTOGRAM'       : [ "Hlt2PIDDs2PiPhiKKPosTaggedTurboCalib/DsPhiKK_Neg" ],
        'FITMODEL'        : DsPhi,
      },



      #============ Muons ===============================#      

      'JpsiCalib_Pos' :  {
        'CUT'             : StdCut.TrackGhostProb + StdCut.Jpsi_Pos,
                          ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "J/psi(1S)",
        'FITVARS'         : "M",
        'HISTOGRAM'       : ["Hlt2PIDDetJPsiMuMuNegTaggedTurboCalib/Jpsi_Pos"],
        'FITMODEL'        : Jpsi,
      },

      'JpsiCalib_Neg' :  {
        'CUT'             : StdCut.TrackGhostProb + StdCut.Jpsi_Neg,
                          ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "J/psi(1S)",
        'FITVARS'         : "M",
        'HISTOGRAM'       : ["Hlt2PIDDetJPsiMuMuPosTaggedTurboCalib/Jpsi_Neg"],
        'FITMODEL'        : Jpsi,
      },
      
      'JpsinoptCalib_Pos' :  {
        'CUT'             : StdCut.TrackGhostProb + StdCut.Jpsi_Pos_lowpt,
                          ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "J/psi(1S)",
        'FITVARS'         : "M",
        'HISTOGRAM'       : ["Hlt2PIDDetJPsiMuMuNegTaggedTurboCalib/Jpsi_noptcut_Pos"],
        'FITMODEL'        : Jpsi,
      },

      'JpsinoptCalib_Neg' :  {
        'CUT'             : StdCut.TrackGhostProb + StdCut.Jpsi_Neg_lowpt,
                          ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "J/psi(1S)",
        'FITVARS'         : "M",
        'HISTOGRAM'       : ["Hlt2PIDDetJPsiMuMuPosTaggedTurboCalib/Jpsi_noptcut_Neg"],
        'FITMODEL'        : Jpsi,
      },


      'BJpsiMMCalib_Pos' : {
        'CUT'             : StdCut.TrackGhostProb + StdCut.BJpsi_Pos,
                          ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "[B+]cc",
        'FITVARS'         : "M:CHILD(M,1)",
        'HISTOGRAM'       : ["Hlt2PIDB2KJPsiMuMuNegTaggedTurboCalib/BJpsi_Pos"],
        'FITMODEL'        : BJpsi,
#        'BOUNDARIES'      : {'x' : [5050,5400]},
      },

      'BJpsiMMCalib_Neg' : {
        'CUT'             : StdCut.TrackGhostProb + StdCut.BJpsi_Neg,
                          ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "[B+]cc",
        'FITVARS'         : "M:CHILD(M,1)",
        'HISTOGRAM'       : ["Hlt2PIDB2KJPsiMuMuPosTaggedTurboCalib/BJpsi_Neg"],
        'FITMODEL'        : BJpsi,
#        'BOUNDARIES'      : {'x' : [5050,5400]},
      },

      'BJpsiMMDTFCalib_Pos' : {
        'CUT'             : StdCut.TrackGhostProb + StdCut.BJpsi_Pos,
                          ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "[B+]cc",
        'FITVARS'         : "DTF_FUN(M,True,'J/psi(1S)'):CHILD(M,1)",
        'HISTOGRAM'       : ["Hlt2PIDB2KJPsiMuMuNegTaggedTurboCalib/BJpsi_NoPR_DTF_Pos"],
        'FITMODEL'        : BJpsi_NoPR,
#        'BOUNDARIES'      : {'x' : [5050,5400]},
      },

      'BJpsiMMDTFCalib_Neg' : {
        'CUT'             : StdCut.TrackGhostProb + StdCut.BJpsi_Neg,
                          ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "[B+]cc",
        'FITVARS'         : "DTF_FUN(M,True,'J/psi(1S)'):CHILD(M,1)",
        'HISTOGRAM'       : ["Hlt2PIDB2KJPsiMuMuPosTaggedTurboCalib/BJpsi_NoPR_DTF_Neg"],
        'FITMODEL'        : BJpsi_NoPR,
#        'BOUNDARIES'      : {'x' : [5050,5400]},
      },

      'DsPhiMMCalib_Pos' : {
        'CUT'             : StdCut.TrackGhostProb + StdCut.Dsphi_Pos + StdCut.Dsphi,
        ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "[D_s+]cc",
        'FITVARS'         : "M",
        'HISTOGRAM'       : ["Hlt2PIDDs2PiPhiMuMuNegTaggedTurboCalib/DsPhiMuMu_Pos"],
        'FITMODEL'        : DsPhiMuMuPi,
        },
      

     'DsPhiMMCalib_Neg' : {
       'CUT'             : StdCut.TrackGhostProb + StdCut.Dsphi_Neg + StdCut.Dsphi,
       ### WARNING! This cut must be sync with histograms!
       'HEAD'            : "[D_s+]cc",
       'FITVARS'         : "M",
       'HISTOGRAM'       : ["Hlt2PIDDs2PiPhiMuMuPosTaggedTurboCalib/DsPhiMuMu_Neg"],
       'FITMODEL'        : DsPhiMuMuPi,
       },
    
      #============ Protons ===============================#  
  
      'Lam0LLCalib_Pos' : {
        'CUT'             : StdCut.TrackGhostProb,
                          ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "Lambda0",
        'FITVARS'         : "M",
        'HISTOGRAM'       : ["Hlt2PIDLambda2PPiLLTurboCalib/L0LL_Pos"],
        'FITMODEL'        : Lambda0,
      },

      'Lam0LLCalib_Neg' : {
        'CUT'             : StdCut.TrackGhostProb,
                          ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "Lambda~0",
        'FITVARS'         : "M",
        'HISTOGRAM'       : ["Hlt2PIDLambda2PPiLLTurboCalib/L0LL_Neg"],
        'FITMODEL'        : Lambda0,
      },


      'Lam0LLCalib_HPT_Pos' : {
        'CUT'             : StdCut.TrackGhostProb,
                          ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "Lambda0",
        'FITVARS'         : "M",
        'HISTOGRAM'       : ["Hlt2PIDLambda2PPiLLhighPTTurboCalib/L0HPTLL_Pos"],
        'FITMODEL'        : Lambda0,
      },

      'Lam0LLCalib_HPT_Neg' : {
        'CUT'             : StdCut.TrackGhostProb,
                          ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "Lambda~0",
        'FITVARS'         : "M",
        'HISTOGRAM'       : ["Hlt2PIDLambda2PPiLLhighPTTurboCalib/L0HPTLL_Neg"],
        'FITMODEL'        : Lambda0,
      },


      'Lam0LLCalib_VHPT_Pos' : {
        'CUT'             : StdCut.TrackGhostProb,
                          ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "Lambda0",
        'FITVARS'         : "M",
        'HISTOGRAM'       : ["Hlt2PIDLambda2PPiLLveryhighPTTurboCalib/L0VHPTLL_Pos"],
        'FITMODEL'        : Lambda0_CB,
      },
  


      'Lam0LLCalib_VHPT_Neg' : {
        'CUT'             : StdCut.TrackGhostProb,
                          ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "Lambda~0",
        'FITVARS'         : "M",
        'HISTOGRAM'       : ["Hlt2PIDLambda2PPiLLveryhighPTTurboCalib/L0VHPTLL_Neg"],
        'FITMODEL'        : Lambda0_CB,
      },


      'Lam0LLisMuonCalib_Pos' : {
        'CUT'             : StdCut.TrackGhostProb,
                          ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "Lambda0",
        'FITVARS'         : "M",
        'HISTOGRAM'       : ["Hlt2PIDLambda2PPiLLisMuonTurboCalib/L0LLisMuon_Pos"],
        'FITMODEL'        : Lambda0,
      },

      'Lam0LLisMuonCalib_Neg' : {
        'CUT'             : StdCut.TrackGhostProb,
                          ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "Lambda~0",
        'FITVARS'         : "M",
        'HISTOGRAM'       : ["Hlt2PIDLambda2PPiLLisMuonTurboCalib/L0LLisMuon_Neg"],
        'FITMODEL'        : Lambda0,
      },

      'Lam0DDCalib_Pos' : {
        'CUT'             : StdCut.TrackGhostProb,
                          ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "Lambda0",
        'FITVARS'         : "M",
        'HISTOGRAM'       : ["Hlt2PIDLambda2PPiDDTurboCalib/L0DD_Pos"],
        'FITMODEL'        : Lambda0_CB,
      },

      'Lam0DDCalib_Neg' : {
        'CUT'             : StdCut.TrackGhostProb,
                          ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "Lambda~0",
        'FITVARS'         : "M",
        'HISTOGRAM'       : ["Hlt2PIDLambda2PPiDDTurboCalib/L0DD_Neg"],
        'FITMODEL'        : Lambda0_CB,
      },

      'Lam0DDCalib_HPT_Pos' : {
        'CUT'             : StdCut.TrackGhostProb,
                          ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "Lambda0",
        'FITVARS'         : "M",
        'HISTOGRAM'       : ["Hlt2PIDLambda2PPiDDhighPTTurboCalib/L0HPTDD_Pos"],
        'FITMODEL'        : Lambda0_DD,
      },

      'Lam0DDCalib_HPT_Neg' : {
        'CUT'             : StdCut.TrackGhostProb,
                          ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "Lambda~0",
        'FITVARS'         : "M",
        'HISTOGRAM'       : ["Hlt2PIDLambda2PPiDDhighPTTurboCalib/L0HPTDD_Neg"],
        'FITMODEL'        : Lambda0_DD,
      },

      'Lam0DDCalib_VHPT_Pos' : {
        'CUT'             : StdCut.TrackGhostProb,
                          ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "Lambda0",
        'FITVARS'         : "M",
        'HISTOGRAM'       : ["Hlt2PIDLambda2PPiDDveryhighPTTurboCalib/L0VHPTDD_Pos"],
        'FITMODEL'        : Lambda0_VHPT_DD,
      },

      'Lam0DDCalib_VHPT_Neg' : {
        'CUT'             : StdCut.TrackGhostProb,
                          ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "Lambda~0",
        'FITVARS'         : "M",
        'HISTOGRAM'       : ["Hlt2PIDLambda2PPiDDveryhighPTTurboCalib/L0VHPTDD_Neg"],
        'FITMODEL'        : Lambda0_VHPT_DD,
      },

      'LbLcMuCalib_Pos'  : {
        'CUT'             : StdCut.TrackGhostProb,
                          ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "[Lambda_b0]cc",
        'FITVARS'         : "CHILD(M,1)",
#        'HEAD'            : "[Lambda_c+]cc",
#        'FITVARS'         : "M",
        'HISTOGRAM'       : ["Hlt2PIDLb2LcMuNuTurboCalib/LbLc_Pos"],
        'FITMODEL'        : Lc,
      },

      'LbLcMuCalib_Neg'  : {
        'CUT'             : StdCut.TrackGhostProb,
                                ### WARNING! This cut must be sync with histograms!
#        'HEAD'            : "[Lambda_c+]cc",
#        'FITVARS'         : "M",
        'HEAD'            : "[Lambda_b0]cc",
        'FITVARS'         : "CHILD(M,1)",
        'HISTOGRAM'       : ["Hlt2PIDLb2LcMuNuTurboCalib/LbLc_Neg"],
        'FITMODEL'        : Lc,
      },

      'LcCalib_Pos'  : {
        'CUT'               : StdCut.TrackGhostProb, #Hlt2PIDLc2KPPiTurboCalib:Lambda_c+:M
                          ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "[Lambda_c+]cc",
        'FITVARS'         : "M",
        'BOUNDARIES'      : {'x' : [2240, 2330]},
        'HISTOGRAM'       : ["Hlt2PIDLc2KPPiTurboCalib/Lc_Pos"],
        'FITMODEL'        : InclLc,
      },

      'LcCalib_Neg'  : {
        'CUT'               : StdCut.TrackGhostProb, #Hlt2PIDLc2KPPiTurboCalib:Lambda_c+:M
                          ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "[Lambda_c+]cc",
        'FITVARS'         : "M",
        'BOUNDARIES'      : {'x' : [2240, 2330]},
        'HISTOGRAM'       : ["Hlt2PIDLc2KPPiTurboCalib/Lc_Neg"],
        'FITMODEL'        : InclLc,
      },

      'OmegaL_Neg'  : {
        'CUT'               : StdCut.TrackGhostProb + " & " + StdCut.OmegaCut, 
                          ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "[Omega-]cc",
        'FITVARS'         : "M",
        'BOUNDARIES'      : {'x' : [1655,1695]},
        'HISTOGRAM'       : ["Hlt2PIDOmega2LambdaKLLLTurboCalib/OmegaLLL_Neg"],
        'FITMODEL'        : OmegaL,
      },

      'OmegaL_Pos'  : {
        'CUT'               : StdCut.TrackGhostProb + " & " + StdCut.OmegaCut, 
                          ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "[Omega-]cc",
        'FITVARS'         : "M",
        'BOUNDARIES'      : {'x' : [1655,1695]},
        'HISTOGRAM'       : ["Hlt2PIDOmega2LambdaKLLLTurboCalib/OmegaLLL_Pos"],
        'FITMODEL'        : OmegaL,
      },

      'OmegaDDDCalib_Neg'  : {
        'CUT'               : StdCut.TrackGhostProb + " & " + StdCut.OmegaCut, #Hlt2PIDLc2KPPiTurboCalib:Lambda_c+:M
                                ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "[Omega-]cc",
        'FITVARS'         : "M",
        'BOUNDARIES'      : {'x' : [1655,1695]},
        'HISTOGRAM'       : ["Hlt2PIDOmega2LambdaKDDDTurboCalib/OmegaDDD_Neg"],
        'FITMODEL'        : OmegaD,
      },

      'OmegaDDDCalib_Pos'  : {
        'CUT'               : StdCut.TrackGhostProb + " & " + StdCut.OmegaCut, #Hlt2PIDLc2KPPiTurboCalib:Lambda_c+:M
                               ### WARNING! This cut must be sync with histograms!
        'HEAD'            : "[Omega-]cc",
        'FITVARS'         : "M",
        'BOUNDARIES'      : {'x' : [1655,1695]},
        'HISTOGRAM'       : ["Hlt2PIDOmega2LambdaKDDDTurboCalib/OmegaDDD_Pos"],
        'FITMODEL'        : OmegaD,
      },

    }

    #============ All Electrons categories - 1D fit ===============================#
    for ecattag, ecat in StdCut.B2KJpsiEE_mombremcats.iteritems():
        s.samples[ecat['HistName']] = {
          'CUT'             : StdCut.TrackGhostProb, #this is only a string in the sTable, cuts for p-brem categories are done elsewhere
          'HEAD'            : "[B+]cc",
          'FITVARS'         : "DTF_FUN(M,True,'J/psi(1S)')",
          'BOUNDARIES'      : {'x' : [5170, 5600]},
          'HISTOGRAM'       : ["Hlt2PIDB2KJPsiEE{}TaggedTurboCalib/{}".format(ecat['TagCharge'], ecattag)],
          'FITMODEL'        : BJpsiEE,
          }

    print "Cross-check."
    for sample in s.samples.keys():
      print sample, type ( s.samples[sample]['FITMODEL'])


################################################################################
## Step 1: Loads histograms from file                                         ##
################################################################################
def loadHistograms ( config ):
  fBuf = ROOT.gDirectory.CurrentDirectory()
  for samplename in config.samples: 
    _cfg = config.samples [ samplename ]
    hist = None
    for fileName in config.inputfiles:
      file = ROOT.TFile.Open(fileName)
      if file:
        for histname in _cfg['HISTOGRAM']:
          loadedHisto = file . Get ( histname ) 
          if loadedHisto:
            if hist:
              hist.Add ( loadedHisto )
            else:
              fBuf.cd()
              hist = loadedHisto.Clone ( loadedHisto.GetName() + "Clone" )

#          else:
#            print ( "WARNING! Cannot load histogram " + histname +
#                              " from file " + fileName )
      else:
        raise Exception ( "Cannot access file " + fileName )
          
      file.Close()

    if not hist :
      print ( "Histogram for sample " + samplename  + " unavailable." )
      config.samples [ samplename ] = None
      
      #raise Exception ( "Histogram for sample " + samplename  +
      #                  " unavailable." )
 
    _cfg['HISTOGRAM'] = hist

  config.samples = {i:config.samples[i] for i in config.samples if config.samples[i] != None}
  print config.samples



################################################################################
## Step 2: fits the histograms, returns dict of pdfs                          ##
################################################################################
def fithistos ( config ):
  try:
    pidreport = file ( config.monitoring_dir + 
                       "/000-PIDREPORT-"+config.monitoring_key+".html" 
                     , "w")
  except:
    raise Exception ( "Could not create report file. Please make sure ./www directory exists" )

  pidreport . write  ( "<HTML><HEAD><TITLE>PID report</TITLE></HEAD><BODY><TABLE width = 100%>" )
  for samplename in sorted(config.samples) : 
    _cfg = config.samples [ samplename ]
  
    hist = _cfg [ 'HISTOGRAM' ]
    if 'PREPROCESSING' in _cfg:
      _cfg['PREPROCESSING'] ( hist )

    xmin = ymin = -1e10000
    xmax = ymax =  1e10000
    if 'BOUNDARIES' in _cfg and 'x' in _cfg['BOUNDARIES']:
      xmin = _cfg['BOUNDARIES']['x'][0]
      xmax = _cfg['BOUNDARIES']['x'][1]

    if 'BOUNDARIES' in _cfg and 'y' in _cfg['BOUNDARIES']:
      ymin = _cfg['BOUNDARIES']['y'][0]
      ymax = _cfg['BOUNDARIES']['y'][1]



    if type(hist) == type ( ROOT.TH1D() ):
      xmin = max(xmin,hist.GetXaxis().GetXmin())
      xmax = min(xmax,hist.GetXaxis().GetXmax())
    elif type(hist) == type ( ROOT.TH2D() ):
      xmin = max(xmin,hist.GetXaxis().GetXmin())
      xmax = min(xmax,hist.GetXaxis().GetXmax())
      ymin = max(ymin,hist.GetYaxis().GetXmin())
      ymax = min(ymax,hist.GetYaxis().GetXmax())
    else:
      raise Exception("Invalid histogram type. Only TH1D and TH2D are currently"
                      " implemented.")


#    print "Boundaries: ",xmin,xmax,ymin,ymax
#    exit(1)

    dataset = None
    vars = [ RooRealVar ( "x", hist.GetXaxis().GetTitle(), 
                               xmin, xmax )
           ]
    vars[0].setBins ( hist.GetNbinsX() )

    if type(hist) == type ( ROOT.TH1D() ):
      pass
    elif type(hist) == type ( ROOT.TH2D() ):
      vars += [RooRealVar ( "y", hist.GetYaxis().GetTitle(),
                                 ymin, ymax )
              ]
      vars[1].setBins ( hist.GetNbinsY() )



    ## dataset

    if type(hist) == type ( ROOT.TH1D() ):
      dataset = RooDataHist ( "roo" +  hist.GetName(), "", 
                              RooArgList(vars[0]), hist )
    else:
      dataset = RooDataHist ( "roo" +  hist.GetName(), "", 
                              RooArgList(vars[0], vars[1]), hist )



    fitter = _cfg['FITMODEL'] ( vars, dataset )
    fitter.fit()
    _cfg['FITRESULT'] = (
      fitter.report(config.monitoring_dir, config.monitoring_key+samplename, _cfg['CUT']))
    
    res = _cfg['FITRESULT']

    pidreport.write ( """
      <TR><TD><B>{sample}</B>
          <TD>Fit model: {model}
          <TD>Fit status: <B><FONT color={color}>{status}</FONT>
          <TD>&chi;<sup>2</sup>: {chi2}
          <TD>Candidates: {candidates:.3f} &times; 10<sup>6</sup>
          <TD><A href="{link}">More...</A>
      """.format(
      sample     = samplename,
      model      = str(fitter.__class__.__name__).replace("<", "").replace(">", ""),
      status     = res['STATUS'],
      color      = "#00CC00" if res['STATUS'] == 'SUCCESS' else "#FF0000",
      chi2       = ["{:.2f}".format ( x ) for x in res['CHI2'] ],
      candidates = res['CANDIDATES'] * 1.e-6,
      link       = res['HTMLREPORT']
      ))

  pidreport.write ( "</TABLE></HTML>" )
    
    

################################################################################
## Step 3: defines an sPlot relation table and stores it                      ##
################################################################################

def _computesWeight ( vars, categories, covmat, lambdaLoader ):
  denominator = 0.
  sumOfsWeights = 0.
  covmatLine = {}
  for specie1 in categories:
    denominator +=  ( categories[specie1].getYield() *  
                      categories[specie1].evalNormalizedPdf ( vars ) ) 




  for specie1 in categories:
    numerator   = 0.
    covmatLine [ specie1 ] = 0
    for specie2 in categories:
      numerator   += ( covmat[specie1][specie2] *
                        categories[specie2].evalNormalizedPdf ( vars ) ) 

      covmatLine [ specie1 ] += covmat[specie1][specie2]

    if numerator == 0 and denominator == 0:
      lambdaLoader ( specie1 , 0. )
    elif denominator == 0:
      return False
    else:
      lambdaLoader ( specie1 , numerator / denominator )
      sumOfsWeights += numerator / denominator


#  for specie1 in categories:
#    print ("CovmatLine for " +specie1+ " is: " + str(covmatLine[specie1]) +
#          ", should be: " + str(categories[specie1].getYield()) )
#   
#  print "SUM OF WEIGHTS SHOULD BE 1.0, IT IS: " + str(sumOfsWeights)

  return True;


def createsPlotTable (config):
  for samplename in config.samples: 
    _cfg = config.samples [ samplename ]
    _cfg['SPLOTTABLES'] = {}
    tables = _cfg['SPLOTTABLES']
    categories = _cfg['FITRESULT']['CATEGORIES'];
    for category in categories:
      comp = categories [ category ]
      tables [ category ] = _cfg [ 'HISTOGRAM' ].Clone ( "sTable" + category )
      table = tables [ category ]
      table . Reset()
      fitRes = _cfg['FITRESULT']

    if type(table) == type ( ROOT.TH1D() ):
      for iBin in range ( 1,  _cfg ['HISTOGRAM'].GetNbinsX() + 1):
        x = table.GetBinCenter ( iBin )
        _computesWeight ( [x] , categories, fitRes['COVMATRIX'],
            lambda specie, sWeight : tables [specie].SetBinContent( iBin , sWeight ))
        
    elif type(table) == type ( ROOT.TH2D() ):
      for iBin in range ( 1,  _cfg ['HISTOGRAM'].GetNbinsX() + 1):
        for jBin in range ( 1,  _cfg ['HISTOGRAM'].GetNbinsY() + 1):
          x = table.GetXaxis().GetBinCenter ( iBin )
          y = table.GetYaxis().GetBinCenter ( jBin )
          ok = _computesWeight ( [x,y] , categories, fitRes['COVMATRIX'],
              lambda specie, sWeight : tables [specie].SetBinContent( iBin, jBin, sWeight ))
          if ok == False:
            print "Undefined value for bin: ", [iBin, jBin], " corresponding to ", [x,y]
          


          
      

################################################################################
## Step 4: saves and archives the splot tables (can be used on the grid)      ##
################################################################################
def savesAndArchivesTheSplotTables ( config ):
  print 80*'#'
  print "Creating file {outdir}/sPlotTables-{key}.root".format(
      outdir = config.outputdir,
      key    = config.monitoring_key )

  archive = ROOT.TFile.Open ( 
    "{outdir}/sPlotTables-{key}.root".format(
      outdir = config.outputdir,
      key    = config.monitoring_key ), 
    "RECREATE")

  for samplename in config.samples: 
    _cfg = config.samples [ samplename ]
    _dir = archive.mkdir ( samplename , samplename );
    _dir.cd()

    _headSelection = ROOT.TNamed ( "HeadSelection" ,
        "(DECTREE ( ' {head} ' ))".format ( head = _cfg['HEAD'] ))
    _headSelection.Write()

    _filter = ROOT.TNamed ( "Filter" , _cfg ['CUT'] )
    _filter.Write()

    vars = _cfg['FITVARS'] . split( ':' )
    for tabName in _cfg['SPLOTTABLES']:
      table = _cfg['SPLOTTABLES'][tabName]
      if type(table) == type(ROOT.TH1D()):
        table.SetTitle(";" + vars[0])
      elif type(table) == type(ROOT.TH2D()):
        table.SetTitle(";" + vars[0] + ";" + vars[1])
      else:
        raise Exception("Only 1D and 2D maps are currently implemented")

      table.Write()
     
    archive.cd()
  
  archive.Close()
  



## Make the full job
def sPlot ( config ):
  ##############################################################################
  ## Step 1: load histograms                                                  ##
  loadHistograms ( config )
  ## Step 2: fits the histograms, returns dict of pdfs                        ##
  fithistos ( config )
  ## Step 3: defines an sPlot relation table and stores it                    ##
  createsPlotTable ( config )
  ## Step 4: saves and archives the splot tables (can be used on the grid)    ##
  savesAndArchivesTheSplotTables ( config )

  ##############################################################################

  for samplename in config.samples: 
    _cfg = config.samples [ samplename ]
    print samplename, _cfg['HISTOGRAM'].GetSumOfWeights() 


if __name__ == "__main__":
  sPlot ( default_config() )

