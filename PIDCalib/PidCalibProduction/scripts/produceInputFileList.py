###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# dirac job created by ganga

import sys
path = ""; dqflag = "OK"
if len(sys.argv) > 1:
  path  = sys.argv[1]
else:
  exit(1)



if len(sys.argv) > 2:
  dqflag = sys.argv[2]

  
from DIRAC.Core.Base.Script import parseCommandLine
parseCommandLine()
from LHCbDIRAC.Interfaces.API.DiracLHCb import DiracLHCb
dirac = DiracLHCb()
#dirac.lhcbProxyInit()

queryOutput = dirac.bkQueryPath ( path, dqflag )

#lfns = list(queryOutput['Value']['LFNs'])
#replicas = dirac.getAllReplicas ( lfns )
#replicaList = replicas['Value']['Successful']
#fileList =  [replicaList[l]["CERN-HIST-EOS"] for l in replicaList]
#newFileList = []
#for fileName in fileList:
#  newFileList += [fileName.replace ( 'srm://srm-eoslhcb.cern.ch:8443/srm/v2/server?SFN=', "root://eoslhcb.cern.ch//" )]
#print " ".join(newFileList)

##dirac-dms-list-directory /lhcb/LHCb/Collision16/HIST.ROOT/00064665/0000/ | tee files_new.tmp # do first this to get the list

newFileList = []
with open ("/eos/lhcb/wg/PID/WGProd_histos/2015/MagUp/files_new.tmp", "r") as myfile:
#with open ("/eos/lhcb/wg/PID/WGProd_histos/2015/MagDown/files_new.tmp", "r") as myfile:
#with open ("/eos/lhcb/wg/PID/WGProd_histos/2016/MagUp/files_new.tmp", "r") as myfile:
#with open ("/eos/lhcb/wg/PID/WGProd_histos/2016/MagDown/files_new.tmp", "r") as myfile:
  for line in myfile:
    newFileList += [line.replace ( "/lhcb/LHCb", "root://eoslhcb.cern.ch///eos/lhcb/grid/prod/lhcb/LHCb" )]
print " ".join(newFileList)
                    

#for i in range(10000, 20163):
#  newFileList += [fileList[i].replace ( 'srm://srm-eoslhcb.cern.ch:8443/srm/v2/server?SFN=', "root://eoslhcb.cern.ch//" )]
#print " ".join(newFileList)
