###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import sys, os

from PidCalibProductionScripts.sPlotNeutral import sPlot as sPlotCoreCode
from PidCalibProductionScripts.sPlotNeutral import default_config


if len(sys.argv) != 3:
  print "Expected <key> and <input_file> as argument"
  exit(1)

key   = sys.argv[1];
ifile = sys.argv[2];


config = default_config()

config.inputfiles = [ ifile ]
config.monitoring_key = key

rebin1 = 1
rebin2 = 1

print config.samples

config.samples['D02KPiPi0Resolved']  ['PREPROCESSING'] = lambda h: h.Rebin(rebin1)
config.samples['D02KPiPi0Merged']    ['PREPROCESSING'] = lambda h: h.Rebin(rebin1)
config.samples['Eta2MuMuGamma']      ['PREPROCESSING'] = lambda h: h.Rebin(rebin1)
config.samples['Dsst2DsGamma']       ['PREPROCESSING'] = lambda h: h.Rebin(rebin1)
config.samples['D2EtapPi']           ['PREPROCESSING'] = lambda h: h.Rebin(rebin1)
config.samples['B2KstGamma']         ['PREPROCESSING'] = lambda h: h.Rebin(rebin1)
config.samples['Bs2PhiGamma']        ['PREPROCESSING'] = lambda h: h.Rebin(rebin1)

sPlotCoreCode ( config )
