###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import sys, os

from PidCalibProductionScripts.sPlot import sPlot as sPlotCoreCode
from PidCalibProductionScripts.sPlot import default_config


if len(sys.argv) != 3:
  print "Expected <key> and <input_file> as argument"
  exit(1)

key   = sys.argv[1];
ifile = sys.argv[2];


config = default_config()

config.inputfiles = [ ifile ]
config.monitoring_key = key

rebin1 = 1
rebin2 = 1

print config.samples

config.samples['RSDStCalib_Pos']      ['PREPROCESSING'] = lambda h: h.Rebin2D(rebin2,rebin2)
config.samples['RSDStCalib_Neg']      ['PREPROCESSING'] = lambda h: h.Rebin2D(rebin2,rebin2)
config.samples['KS0LLCalib']            ['PREPROCESSING'] = lambda h: h.Rebin  (rebin1)
config.samples['KS0DDCalib']            ['PREPROCESSING'] = lambda h: h.Rebin  (rebin1)
config.samples['DsPhiKKCalib_Pos']      ['PREPROCESSING'] = lambda h: h.Rebin2D(rebin2,rebin2)
config.samples['DsPhiKKCalib_Neg']      ['PREPROCESSING'] = lambda h: h.Rebin2D(rebin2,rebin2)

config.samples['JpsiCalib_Pos']       ['PREPROCESSING'] = lambda h: h.Rebin(rebin1)
config.samples['JpsiCalib_Neg']       ['PREPROCESSING'] = lambda h: h.Rebin(rebin1)
config.samples['JpsinoptCalib_Pos']       ['PREPROCESSING'] = lambda h: h.Rebin(rebin1)
config.samples['JpsinoptCalib_Neg']       ['PREPROCESSING'] = lambda h: h.Rebin(rebin1)
#config.samples['BJpsiMMCalib_Pos']      ['PREPROCESSING'] = lambda h: h.Rebin2D(rebin2,rebin2)
#config.samples['BJpsiMMCalib_Neg']      ['PREPROCESSING'] = lambda h: h.Rebin2D(rebin2,rebin2)
config.samples['DsPhiMMCalib_Pos']      ['PREPROCESSING'] = lambda h: h.Rebin(rebin1)
config.samples['DsPhiMMCalib_Neg']      ['PREPROCESSING'] = lambda h: h.Rebin(rebin1)
config.samples['BJpsiMMDTFCalib_Pos']      ['PREPROCESSING'] = lambda h: h.Rebin2D(rebin2,rebin2)
config.samples['BJpsiMMDTFCalib_Neg']      ['PREPROCESSING'] = lambda h: h.Rebin2D(rebin2,rebin2)

config.samples['Lam0LLCalib_Pos']       ['PREPROCESSING'] = lambda h: h.Rebin  (rebin1)
config.samples['Lam0LLCalib_Neg']       ['PREPROCESSING'] = lambda h: h.Rebin  (rebin1)
config.samples['Lam0LLCalib_HPT_Pos']   ['PREPROCESSING'] = lambda h: h.Rebin  (rebin1)
config.samples['Lam0LLCalib_HPT_Neg']   ['PREPROCESSING'] = lambda h: h.Rebin  (rebin1)
config.samples['Lam0LLCalib_VHPT_Pos']  ['PREPROCESSING'] = lambda h: h.Rebin  (rebin1)
config.samples['Lam0LLCalib_VHPT_Neg']  ['PREPROCESSING'] = lambda h: h.Rebin  (rebin1)
config.samples['Lam0LLisMuonCalib_Pos']       ['PREPROCESSING'] = lambda h: h.Rebin  (rebin1)
config.samples['Lam0LLisMuonCalib_Neg']       ['PREPROCESSING'] = lambda h: h.Rebin  (rebin1)
config.samples['Lam0DDCalib_Pos']       ['PREPROCESSING'] = lambda h: h.Rebin  (rebin1)
config.samples['Lam0DDCalib_Neg']       ['PREPROCESSING'] = lambda h: h.Rebin  (rebin1)
config.samples['Lam0DDCalib_HPT_Pos']   ['PREPROCESSING'] = lambda h: h.Rebin  (rebin1)
config.samples['Lam0DDCalib_HPT_Neg']   ['PREPROCESSING'] = lambda h: h.Rebin  (rebin1)
config.samples['Lam0DDCalib_VHPT_Pos']  ['PREPROCESSING'] = lambda h: h.Rebin  (rebin1)
config.samples['Lam0DDCalib_VHPT_Neg']  ['PREPROCESSING'] = lambda h: h.Rebin  (rebin1)
config.samples['LbLcMuCalib_Pos']            ['PREPROCESSING'] = lambda h: h.Rebin  (rebin1)
config.samples['LbLcMuCalib_Neg']            ['PREPROCESSING'] = lambda h: h.Rebin  (rebin1)
config.samples['LcCalib_Pos']            ['PREPROCESSING'] = lambda h: h.Rebin  (rebin1)
config.samples['LcCalib_Neg']            ['PREPROCESSING'] = lambda h: h.Rebin  (rebin1)

config.samples['OmegaL_Pos']          ['PREPROCESSING'] = lambda h: h.Rebin(rebin1)
config.samples['OmegaL_Neg']          ['PREPROCESSING'] = lambda h: h.Rebin(rebin1)
config.samples['OmegaDDDCalib_Pos']   ['PREPROCESSING'] = lambda h: h.Rebin(rebin1)
config.samples['OmegaDDDCalib_Neg']   ['PREPROCESSING'] = lambda h: h.Rebin(rebin1)

for sign in ['Pos', 'Neg']:
    for nbrem in ['0Brem', '1Brem']:
        config.samples['BJpsiEE_Calib{}_{}'.format(nbrem, sign)]['PREPROCESSING'] = lambda h: h.Rebin(rebin1)
        for ibin in range(5):
            config.samples['BJpsiEE_Calib{}_pbin{}_{}'.format(nbrem, ibin, sign)]['PREPROCESSING'] = lambda h: h.Rebin(rebin1)


# use the following commented snippet to test just a type of fit (e.g. JpsiEE fits)
#for k in config.samples.keys():
#    if 'BJpsiMMDTF' not in k:
#        config.samples.pop(k)

sPlotCoreCode ( config )
