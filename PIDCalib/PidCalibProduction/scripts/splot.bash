#!/bin/bash
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
export KEY=$1
export TMP_FILELIST='file.tmp'

#case $KEY in
#  "15MagUp"   ) export  BKKPATH="/LHCb/Collision15/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco15a/Turbo02/PIDCalibHist4 2015/95100000/DAVINCIHIST";;
#  "15MagDown" ) export  BKKPATH="/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Turbo02/PIDCalibHist4 2015/95100000/DAVINCIHIST";;
#  "16MagUp"   ) export  BKKPATH="/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco16/Turbo02a/PIDCalibHist4 2016/95100000/DAVINCIHIST";;
#  "16MagDown" ) export  BKKPATH="/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Turbo02a/PIDCalibHist4 2016/95100000/DAVINCIHIST";;
#  "15MagUp"   ) export  BKKPATH="/LHCb/Collision15/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco15a/Turbo02/PIDCalibHist5-2015/95100000/DAVINCIHIST";;
#  "15MagDown" ) export  BKKPATH="/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Turbo02/PIDCalibHist5-2015/95100000/DAVINCIHIST";;
#  "16MagUp"   ) export  BKKPATH="/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco16/Turbo02a/PIDCalibHist5-2016/95100000/DAVINCIHIST";;
#  "16MagDown" ) export  BKKPATH="/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Turbo02a/PIDCalibHist5-2016/95100000/DAVINCIHIST";;
#  *) echo "'"$KEY "' not known as key";;
#esac

#LHCb/Collision15/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco15a/Turbo02//PIDCalibHist5-2015/95100000/

case $KEY in
#  "15MagUp"   ) export  TMP_HISTO="/eos/user/m/mfontana/PIDCalib/histograms_15MagUp.root";;
#  "15MagDown" ) export  TMP_HISTO="/eos/user/m/mfontana/PIDCalib/histograms_15MagDown.root";;
#  "16MagUp"   ) export  TMP_HISTO="/eos/user/m/mfontana/PIDCalib/histograms_16MagUp.root";;
#  "16MagDown" ) export  TMP_HISTO="/eos/user/m/mfontana/PIDCalib/histograms_16MagDown.root";;
  "15MagUp"   ) export  TMP_HISTO="/eos/lhcb/wg/PID/WGProd_histos/2015/MagUp/histograms_15MagUp.root";;
  "15MagDown" ) export  TMP_HISTO="/eos/lhcb/wg/PID/WGProd_histos/2015/MagDown/histograms_15MagDown.root";;
  "16MagUp"   ) export  TMP_HISTO="/eos/lhcb/wg/PID/WGProd_histos/2016/MagUp/histograms_16MagUp.root";;
  "16MagDown" ) export  TMP_HISTO="/eos/lhcb/wg/PID/WGProd_histos/2016/MagDown/histograms_16MagDown.root";;
#  "17MagUp"   ) export  TMP_HISTO="/eos/lhcb/wg/PID/WGProd_histos/2017/MagUp/histograms_17MagUp_test.root";;
#  "17MagDown" ) export  TMP_HISTO="/eos/lhcb/wg/PID/WGProd_histos/2017/MagDown/histograms_17MagDown_test.root";;
  "17MagDownTest" ) export TMP_HISTO="/eos/lhcb/wg/PID/WGProd_histos/2017/Test_MagDown/histo.root";;
  "17MagUp" ) export TMP_HISTO="~/CastelaoDev_v1r0p1/WG/PIDCalib/scriptsR2/histos_pp_2017_magup.root";;
  "17MagDown" ) export TMP_HISTO="~/CastelaoDev_v1r0p1/WG/PIDCalib/scriptsR2/histos_pp_2017_magdown.root";;
  "17MagDownTest2" ) export TMP_HISTO="~/CastelaoDev_v1r0p1/WG/PIDCalib/scriptsR2/histos_pp_2017_bjpsik_magdown.root";;
  "17MagUpTest2" ) export TMP_HISTO="~/CastelaoDev_v1r0p1/WG/PIDCalib/scriptsR2/histos_pp_2017_bjpsik_magup.root";;
  "18MagUp" ) export TMP_HISTO="/eos/lhcb/wg/PID/WGProd_histos/2018/MagUp//histograms_18MagUp.root";;
  "18MagDown" ) export TMP_HISTO="/eos/lhcb/wg/PID/WGProd_histos/2018/MagDown//histograms_18MagDown.root";;
  "16MagUpRepro"   ) export  TMP_HISTO="/eos/lhcb/wg/PID/WGProd_histos/2016/MagUp/histograms_16MagUp_repro.root";;
  "16MagDownRepro" ) export  TMP_HISTO="/eos/lhcb/wg/PID/WGProd_histos/2016/MagDown/histograms_16MagDown_repro.root";;
  "17MagUpRepro"   ) export  TMP_HISTO="/eos/lhcb/wg/PID/WGProd_histos/2017/MagUp/histograms_17MagUp_repro.root";;
  "17MagDownRepro" ) export  TMP_HISTO="/eos/lhcb/wg/PID/WGProd_histos/2017/MagDown/histograms_17MagDown_repro.root";;

  *) echo "'"$KEY "' not known as key";;
esac

##lb-run LHCbDirac bash
#python produceInputFileList.py "$BKKPATH" > $TMP_FILELIST
#hadd -f $TMP_HISTO `cat $TMP_FILELIST`

export PYTHONPATH=../python:$PYTHONPATH
#. SetupProject.sh Bender v29r4 #--use PIDCalib/Production 
#lb-run Bender/latest python sPlot.py $KEY $TMP_HISTO
python sPlot.py $KEY $TMP_HISTO

