###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# Import needed files ############################
from Gaudi.Configuration import *
from Configurables import DaVinci, TupleToolTagging, CondDB

from Configurables import DecayTreeTuple, LoKi__Hybrid__TupleTool, LoKi__Hybrid__EvtTupleTool
from Configurables import TupleToolTrigger, TupleToolTISTOS, TupleToolDecay,Hlt2TriggerTisTos
from Configurables import TupleToolMCTruth,MCDecayTreeTuple,TupleToolTrackInfo, TupleToolGeometry,TupleToolKinematic
from Configurables import FilterDesktop#, LokiTool

from PhysSelPython.Wrappers import DataOnDemand, Selection, MergedSelection, SelectionSequence # FOR CopyAndMatchCombination
#from Configurables import CopyAndMatchCombination
from Configurables import CopyAndMatchCombinationTC

from TeslaTools import TeslaTruthUtils
from PhysConf.Filters import LoKi_Filters

from DecayTreeTuple.Configuration import *

################################################################################
## Configuration of the MicroDST writer                                       ##
################################################################################

from PhysSelPython.Wrappers import MultiSelectionSequence
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import ( SelDSTWriter, MicroDSTWriter ,stripMicroDSTStreamConf,
                       stripMicroDSTElements, stripCalibMicroDSTStreamConf,
                       stripDSTElements, stripDSTStreamConf)
from DSTWriters.streamconf import OutputStreamConf
from Configurables import OutputStream

def stripMicroDSTStreamConf( pack = True,
                             isMC = False,
                             selectiveRawEvent = False,
                             killTESAddressHistory = True ) :
    eItems = [ '/Event/Rec/Header#1',
               '/Event/Rec/Status#1',
               '/Event/Rec/Summary#1',
               '/Event/Trigger/RawEvent#1',
               '/Event/Turbo#99',
               ]
    if isMC :
        eItems += ["/Event/MC/Header#1",
                   "/Event/MC/DigiHeader#1",
                   "/Event/Gen/Header#1"]
    return OutputStreamConf( streamType    = OutputStream,
                             filePrefix    = '.',
                             fileExtension = '.mdst',
                             extraItems    = eItems,#['/Event/Rec/Summary#1'],#eItems,
                             vetoItems     = [],
                             selectiveRawEvent = selectiveRawEvent,
                             killTESAddressHistory = killTESAddressHistory )

def configureMicroDSTwriter( name, prefix, sequences):
  pack = True
  microDSTwriterInput = MultiSelectionSequence ( name , Sequences = sequences )
  mdstStreamConf = {'default':stripMicroDSTStreamConf(pack=pack, selectiveRawEvent = False)}
  mdstElements   = {'default':stripMicroDSTElements(pack=pack)}
  dstWriter = SelDSTWriter( "MyDSTWriter",
                            StreamConf = mdstStreamConf,
                            MicroDSTElements = mdstElements,
                            OutputFileSuffix = prefix,
                            SelectionSequences = [microDSTwriterInput]
                          )
  from Configurables import StoreExplorerAlg
  return [microDSTwriterInput.sequence(), dstWriter.sequence()]


def set_Tuples(datatype, sample, methods, MDST=False, Test=False, isTurbo=False, filtered = False):
    #######################
    ### CONFIG          ###
    #######################
    charge_list = ["Plus","Minus"]
    turbo_dict = { "VeloMuon"  : "T",
                  "MuonTT"    : "Long",
                  "Downstream": "Velo", }
    strip_dict = { "VeloMuon" : "TrackEffVeloMuon",
                   "MuonTT"   : "TrackEffMuonTT_Jpsi",
                   "Downstream" : "TrackEffDownMuon", }
    mode_dict = { "Plus" : "Line1",
                  "Minus" : "Line2", }# Tag(-)-and-Probe(+) (1) or  Tag(+)-and-Probe(-) (2)
    trig_year = {   "2011":[""],
                    "2012":[""],
                    "2015":[""],
                    "2016":["LowStat","HighStat"],
                    "2017":["LowStat","HighStat"],
                    "2018":["LowStat","HighStat"]}
    # Interesting trigger lines
    tlist = [
        "L0HadronDecision",
        "L0MuonDecision",
        "L0ElectronDecision"
        ]
    tlist+= [
        "Hlt1TrackMuonDecision",
        "Hlt1TrackAllL0Decision",
        "Hlt1TrackMVADecision",
        "Hlt1TrackMuonMVADecision",
        "Hlt1SingleMuonNoIPDecision",
        "Hlt1SingleMuonHighPTDecision",
        "Hlt1SingleElectronNoIPDecision",
        "Hlt1TrackMuonNoSPDDecision",
        "Hlt1CalibMuonAlignJpsiDecision",
        "Hlt1DiMuonHighMassDecision",
        "Hlt1DiMuonLowMassDecision",
        "Hlt1MultiMuonNoL0Decision",
        "Hlt1DiMuonNoL0Decision",
        "Hlt1TwoTrackMVADecision",
        "Hlt1GlobalDecision"     
        ]
    
    Hlt2_lines = []
    for method in methods:
        for charge in charge_list:
            for trig in trig_year[datatype]:
                Hlt2_lines += [ 'Hlt2TrackEffDiMuon'+method+charge+trig+'TaggedTurboCalib',
                                'Hlt2TrackEffDiMuon'+method+charge+trig+'MatchedTurboCalib']
    for Hlt2_line in Hlt2_lines: tlist+=[Hlt2_line+"Decision"]
    tlist+= [
        "Hlt2SingleMuonDecision",
        "Hlt2SingleMuonHighPTDecision",
        "Hlt2SingleMuonLowPTDecision",
        "Hlt2SingleMuonNoSPDDecision",
        "Hlt2SingleMuonRareDecision",
        "Hlt2SingleMuonVHighPTDecision",
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
        "Hlt2Topo4BodyDecision",
        "Hlt2TopoE3BodyDecision",
        "Hlt2TopoE4BodyDecision",
        "Hlt2TopoEE2BodyDecision",
        "Hlt2TopoMu2BodyDecision",
        "Hlt2TopoMu3BodyDecision",
        "Hlt2TopoMu4BodyDecision",
        "Hlt2TopoMuE2BodyDecision",
        "Hlt2TopoMuE3BodyDecision",
        "Hlt2TopoMuE4BodyDecision",
        "Hlt2TopoMuMu2BodyDecision",
        "Hlt2TopoMuMu3BodyDecision",
        "Hlt2TopoMuMu4BodyDecision",
        "Hlt2TopoMuMuDDDecision",
        "Hlt2GlobalDecision",
        "Hlt2LowMultMuonDecision",
        "Hlt2DiMuonBDecision",
        "Hlt2DiMuonDetachedDecision",
        "Hlt2DiMuonDetachedHeavyDecision",
        "Hlt2DiMuonDetachedJPsiDecision",
        "Hlt2DiMuonDetachedPsi2SDecision",
        "Hlt2DiMuonJPsiHighPTDecision",
        "Hlt2DiMuonPsi2SHighPTDecision",
        "Hlt2DiMuonSoftDecision",
        "Hlt2DiMuonZDecision",             
        "Hlt2Topo2BodyDecision", 
        "Hlt2Topo3BodyDecision", 
        "Hlt2Topo4BodyDecision", 
        "Hlt2TopoMu2BodyDecision", 
        "Hlt2TopoMu3BodyDecision", 
        "Hlt2TopoMu4BodyDecision" 
        ]

    #######################
    ### TUPLE CONFIG    ###
    #######################
    tuple_list = []
    filterSequences, matchingSequences,reviveSequences = [],[],[]
    inputSequences = [] # FOR WRITING OUT THE INFO FOR TupleToolTrackEffMatch TO MDST

    if MDST:
        matchingLocation = {"mu+"          :  "/Event/TRKCALIB/Phys/StdAllNoPIDsMuons/Particles",}
    else:
        matchingLocation = {
            "mu+"          :  "/Event/Phys/StdAllNoPIDsMuons/Particles",
            "pi+"          :  "/Event/Phys/StdAllNoPIDsPions/Particles",
            "K+"           :  "/Event/Phys/StdAllNoPIDsKaons/Particles",
            "p+"           :  "/Event/Phys/StdAllNoPIDsProtons/Particles",
            "e+"           :  "/Event/Phys/StdAllNoPIDsElectrons/Particles", }
    if "MC" in sample:
        if not filtered:
            stream = "AllStreams"
        else:
            stream = 'MuonTrackEff.Strip'
    elif int(datatype) <2015: stream = 'Calibration'
    else: stream = 'Dimuon'

    for method in methods:
        for charge in charge_list:
            if MDST:
                input_list = ["/Phys/filter_selection"+method+charge+"/Particles"]
            else:
                if isTurbo:
                    input_list = ["/Event/Turbo/Rec/Summary"]
                    for trig in trig_year[datatype]: 
                      input_list += ["/Event/Turbo/Hlt2TrackEffDiMuon"+method+charge+trig+"TaggedTurboCalib/Particles",
                                     "/Event/Turbo/Hlt2TrackEffDiMuon"+method+charge+trig+"MatchedTurboCalib/Particles"]
                    inputSelection_list =  [ DataOnDemand(line) for line in input_list if "Matched" in line]
                    for i in range(len(inputSelection_list)):
                        inputSequence = SelectionSequence("Seq_input_selection"+method+charge+str(i),TopSelection = inputSelection_list[i])
                        inputSequences.append(inputSequence)
                        tuple_list.append(inputSequence)
                else:
                    input_list = ["/Event/"+stream+"/Phys/"+strip_dict[method]+mode_dict[charge]+"/Particles" ]

            inputSelection = MergedSelection ( "input_selection"+method+charge ,
                  RequiredSelections = [ DataOnDemand(line) for line in input_list],)
            selection = Selection("filter_selection"+method+charge,
                RequiredSelections = [ inputSelection ],
                Algorithm = FilterDesktop("alg_selection"+method+charge, Code = 'HASVERTEX' ))
            filterSequence = SelectionSequence("Seq_selection"+method+charge,
              TopSelection = selection)
            filterSequences.append(filterSequence)

            matchingSel = Selection("Match_selection"+method+charge,
                Algorithm = CopyAndMatchCombinationTC ( "MatchAlg_selection"+method+charge ),
                RequiredSelections = [selection]
                )
            matchingSequence = SelectionSequence ( "SeqMatch_selection"+method+charge,
                  TopSelection = matchingSel )
            matchingSequences.append(matchingSequence)

            tuple = DecayTreeTuple("Tuple"+turbo_dict[method]+charge)
            tuple.Inputs = [filterSequence.outputLocation()]
            if MDST: tuple.RootInTES = "/Event/TRKCALIB/"
            else: tuple.RootInTES = '/Event'

            tuple.ToolList =  [ "TupleToolGeometry",
                                "TupleToolKinematic",
                                "TupleToolTISTOS",
                                "TupleToolTrigger",
                                "TupleToolPid",
                                "TupleToolRecoStats",
                                "TupleToolEventInfo",
                                "TupleToolBeamSpot",
                                "TupleToolL0Data" ]
#            if isTurbo: tuple.ToolList += ["TupleToolTrackInfo"]
            if "MC" in sample:
                tuple.ToolList+= [ "TupleToolMCTruth", "TupleToolMCBackgroundInfo" ]
            tuple.Decay = "J/psi(1S) -> ^mu+ ^mu-"

            eventTool = tuple.addTupleTool("LoKi::Hybrid::EvtTupleTool/LoKiEvent")
            eventVariables = {
            "nPVs_Brunel"              : "RECSUMMARY( LHCb.RecSummary.nPVs              , -9999)",
            "nLongTracks_Brunel"       : "RECSUMMARY( LHCb.RecSummary.nLongTracks       , -9999)",
            "nDownstreamTracks_Brunel" : "RECSUMMARY( LHCb.RecSummary.nDownstreamTracks , -9999)",
            "nVeloTracks_Brunel"       : "RECSUMMARY( LHCb.RecSummary.nVeloTracks       , -9999)",
            "nTracks_Brunel"           : "RECSUMMARY( LHCb.RecSummary.nTracks           , -9999)",
            "nRich1Hits_Brunel"        : "RECSUMMARY( LHCb.RecSummary.nRich1Hits        , -9999)",
            "nRich2Hits_Brunel"        : "RECSUMMARY( LHCb.RecSummary.nRich2Hits        , -9999)",
            "nVeloClusters_Brunel"     : "RECSUMMARY( LHCb.RecSummary.nVeloClusters     , -9999)",
            "nSPDhits_Brunel"          : "RECSUMMARY( LHCb.RecSummary.nSPDhits          , -9999)",
            "nMuonTracks_Brunel"       : "RECSUMMARY( LHCb.RecSummary.nMuonTracks       , -9999)",
          }
            if isTurbo:
                eventVariables["nPVs_Turbo"] = "RECSUMMARY( LHCb.RecSummary.nPVs              , -9999, '/Event/Turbo/Rec/Summary')"
                eventVariables["nLongTracks_Turbo"] = "RECSUMMARY( LHCb.RecSummary.nLongTracks       , -9999, '/Event/Turbo/Rec/Summary')"
                eventVariables["nDownstreamTracks_Turbo"] = "RECSUMMARY( LHCb.RecSummary.nDownstreamTracks , -9999, '/Event/Turbo/Rec/Summary')"
                eventVariables["nVeloTracks_Turbo"] = "RECSUMMARY( LHCb.RecSummary.nVeloTracks       , -9999, '/Event/Turbo/Rec/Summary')"
                eventVariables["nRich1Hits_Turbo"] = "RECSUMMARY( LHCb.RecSummary.nRich1Hits        , -9999, '/Event/Turbo/Rec/Summary')"
                eventVariables["nRich2Hits_Turbo"] = "RECSUMMARY( LHCb.RecSummary.nRich2Hits        , -9999, '/Event/Turbo/Rec/Summary')"
                eventVariables["nVeloClusters_Turbo"] = "RECSUMMARY( LHCb.RecSummary.nVeloClusters     , -9999, '/Event/Turbo/Rec/Summary')"
                eventVariables["nSPDhits_Turbo"] = "RECSUMMARY( LHCb.RecSummary.nSPDhits          , -9999, '/Event/Turbo/Rec/Summary')"
                eventVariables["nMuonTracks_Turbo"] = "RECSUMMARY( LHCb.RecSummary.nMuonTracks       , -9999, '/Event/Turbo/Rec/Summary')"

            eventTool.VOID_Variables = eventVariables
            eventTool.Preambulo = ["from LoKiTracks.decorators import *", "from LoKiCore.functions import *" ]
            tuple.addTool( eventTool )

            LoKiTool = tuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKiTool")
            LoKiTool.Variables = { "ETA" : "ETA","PHI" : "PHI" }

            tuple.addTool(TupleToolTrigger, name="TupleToolTrigger")
            tuple.addTool(TupleToolTISTOS,  name="TupleToolTISTOS" )
            tuple.TupleToolTrigger.Verbose = True
            tuple.TupleToolTrigger.TriggerList = tlist
            tuple.TupleToolTISTOS.Verbose = True
            tuple.TupleToolTISTOS.TriggerList = tlist
            tuple.TupleToolTISTOS.TUS = True

            trackeffTool = tuple.addTupleTool('TupleToolTrackEffMatch')
            trackeffTool.MC = ("MC" in sample)
            trackeffTool.Turbo = isTurbo
            trackeffTool.Method = method
            trackeffTool.OutputLevel = 5
            if isTurbo:
                if MDST:
                      if int(datatype) < 2016 :
                            trackeffTool.MatchedLocation1 = "/Turbo/Hlt2TrackEffDiMuon"+method+charge+"MatchedTurboCalib/Particles"
                      else :
                            trackeffTool.MatchedLocation1 = "/Turbo/Hlt2TrackEffDiMuon"+method+charge+"LowStatMatchedTurboCalib/Particles"
                            trackeffTool.MatchedLocation2 = "/Turbo/Hlt2TrackEffDiMuon"+method+charge+"HighStatMatchedTurboCalib/Particles"
                else:
                      if int(datatype) < 2016 :
                            trackeffTool.MatchedLocation1 = "/Event/Turbo/Hlt2TrackEffDiMuon"+method+charge+"MatchedTurboCalib/Particles"
                      else :
                            trackeffTool.MatchedLocation1 = "/Event/Turbo/Hlt2TrackEffDiMuon"+method+charge+"LowStatMatchedTurboCalib/Particles"
                            trackeffTool.MatchedLocation2 = "/Event/Turbo/Hlt2TrackEffDiMuon"+method+charge+"HighStatMatchedTurboCalib/Particles"
            if "MC" in sample:
                relations = [TeslaTruthUtils.getRelLoc("")]
                TeslaTruthUtils.makeTruth(tuple, relations, [ "MCTupleToolKinematic", "MCTupleToolReconstructed" ])

            tuple.WriteP2PVRelations = False
            if isTurbo:
                if MDST:    tuple.InputPrimaryVertices = "/Turbo/Primary"
                else:       tuple.InputPrimaryVertices = "/Event/Turbo/Primary"

            ttt_conf = Hlt2TriggerTisTos()
            if method=='MuonTT':
                ttt_conf.TOSFracOTIT = 0.
                ttt_conf.TISFracOTIT = 0.
                ttt_conf.TOSFracVelo = 0.
                ttt_conf.TISFracVelo = 0.
            if method=='VeloMuon':
                ttt_conf.TOSFracOTIT = 0.
                ttt_conf.TISFracOTIT = 0.
                ttt_conf.TOSFracTT = 0.
                ttt_conf.TISFracTT = 0.
            if method=='Downstream':
                ttt_conf.TOSFracVelo = 0.
                ttt_conf.TISFracVelo = 0.
            ttt_conf.TOSFracEcal = 0.
            ttt_conf.TOSFracHcal = 0.
            ttt_conf.TISFracEcal = 0.
            ttt_conf.TISFracHcal = 0.
            tuple.TupleToolTISTOS.addTool(ttt_conf)

            tuple_list.append(matchingSequence)

            tuple_list.append(tuple)

            matcher = tuple.addTupleTool ( "TupleToolTwoParticleMatching/Matcher_" + method+charge )
            matcher.ToolList = []
            matcher.Prefix = ""; matcher.Suffix = "_Brunel"
            matcher.MatchLocations = matchingLocation
            track_tool_match = TupleToolTrackInfo('track_match_'+method+charge)
            track_tool_match.Verbose = True
            matcher.addTool(track_tool_match)
            if isTurbo:
                matcher.ToolList += ["TupleToolTrackInfo/track_match_"+method+charge]
                geome_tool_match = TupleToolGeometry('geome_match_'+method+charge)
                matcher.addTool(geome_tool_match)
            matcher.ToolList += ["TupleToolGeometry/geome_match_"+method+charge]
            kinem_tool_match = TupleToolKinematic('kinem_match_'+method+charge)
            matcher.addTool(kinem_tool_match)
            matcher.ToolList += ["TupleToolKinematic/kinem_match_"+method+charge]
            lokimatchedtool = LoKi__Hybrid__TupleTool("Loki_match_"+method+charge)
            lokimatchedtool.Variables = { "ETA" : "ETA",   "PHI" : "PHI" }
            matcher.addTool(lokimatchedtool)
            matcher.ToolList += ["LoKi::Hybrid::TupleTool/Loki_match_"+method+charge]
            tistos_match    = TupleToolTISTOS("tistos_match_"+method+charge )
            tistos_match.Verbose = True
            tistos_match.TriggerList = tlist
            tistos_match.TUS = True

            ttt_conf = Hlt2TriggerTisTos("hlt2_match_"+method+charge)
            if method=='MuonTT':
                ttt_conf.TOSFracOTIT = 0.
                ttt_conf.TISFracOTIT = 0.
                ttt_conf.TOSFracVelo = 0.
                ttt_conf.TISFracVelo = 0.
            if method=='VeloMuon':
                ttt_conf.TOSFracOTIT = 0.
                ttt_conf.TISFracOTIT = 0.
                ttt_conf.TOSFracTT = 0.
                ttt_conf.TISFracTT = 0.
            if method=='Downstream':
                ttt_conf.TOSFracVelo = 0.
                ttt_conf.TISFracVelo = 0.
            ttt_conf.TOSFracEcal = 0.
            ttt_conf.TOSFracHcal = 0.
            ttt_conf.TISFracEcal = 0.
            ttt_conf.TISFracHcal = 0.
            tistos_match.addTool(ttt_conf)
            matcher.addTool(tistos_match)
            matcher.ToolList+= ["TupleToolTISTOS/tistos_match_"+method+charge]

    if "MC" in sample:
        mctuple = MCDecayTreeTuple("MCTuple")
        mctuple.Decay = "[Beauty =>  ^(J/psi(1S) => ^mu+ ^mu-) X ]CC "
        mctuple.ToolList =  ["MCTupleToolKinematic"
                             ,"MCTupleToolEventType"
                             ,"MCTupleToolReconstructed"
                             ,"MCTupleToolHierarchy"
                             ,"TupleToolEventInfo"]
        tuple_list.append(mctuple)

#    if not MDST and not "MC" in sample: tuple_list += configureMicroDSTwriter ( "TRKCALIB","", inputSequences+ filterSequences+matchingSequences)

    # DaVinci config
    # The tags are set in the WGP request.
    # DDDB_dict = {"2011" : "dddb-20130929",
    #              "2012" : "dddb-20130929-1",
    #              "2015" : "dddb-20150724",
    #              "2016" : "dddb-20150724",
    #              "2017" : "dddb-20170721-3",
    #              "2018" : "dddb-20170721-3"}
    # CondDB_dict = { "2011" : "sim-20130522-vc-m{0}100",
    #                 "2012" : "sim-20130522-1-vc-m{0}100",
    #                 "2015" : "sim-20161124-vc-m{0}100",
    #                 "2016" : "sim-20161124-2-vc-m{0}100",
    #                 "2017" : "sim-20190430-1-vc-m{0}100",
    #                 "2018" : "sim-20190430-vc-m{0}100"}
    if MDST:
        DaVinci().InputType = 'MDST'
    else:
        DaVinci().InputType =  'DST'

    # if "MC" in sample:
    #     DaVinci().InputType = 'MDST'
    #     DaVinci().DDDBtag = DDDB_dict[datatype]
    #     DaVinci().CondDBtag = CondDB_dict[datatype].format(sample.split('_')[1].lower()[0])
    if not 'MC' in sample:
        CondDB().LatestGlobalTagByDataType = datatype

    DaVinci().DataType    = datatype
    DaVinci().TupleFile = 'Tuple_{0}_{1}.root'.format(datatype,sample)
    DaVinci().Lumi = not ("MC" in sample)
    DaVinci().Simulation = ("MC" in sample)
    DaVinci().UserAlgorithms = tuple_list
    if isTurbo: 
        DaVinci().Turbo = True
        DaVinci().RootInTES = '/Event/Turbo'
    if Test:  DaVinci().EvtMax = 1000

    if isTurbo and int(datatype) > 2015 :
        filter = LoKi_Filters ( HLT2_Code  = "HLT_PASS_RE('Hlt2TrackEffDiMuon.*Decision')" )
        filterseq = filter.sequence('PreFilter')
        DaVinci().EventPreFilters = [filterseq]
    elif not isTurbo:
        filter = LoKi_Filters ( STRIP_Code  = "HLT_PASS_RE('StrippingTrackEff.*Decision')" )
        filterseq = filter.sequence('PreFilter')
        DaVinci().EventPreFilters = [filterseq]
        
    ###########################################################################
    # Turbo will set the RootInTES of all the things, I set it back to /Event #
    ###########################################################################
    def _set_root_in_tes_recursive_ ( s , root ) :
        #
        if isinstance ( s , str ) : return
        prps = s.getProperties().keys()
        if 'RootInTES' in prps :
            # print "Set RootInTES=%s for %s" % ( root , s.getName() )
            s.RootInTES = root
        #
        if not 'Members' in prps : return
        #
        for a in s.Members :
            _set_root_in_tes_recursive_ ( a , root )

    def _set_root_in_tes_ ( s , root, to_modify ) :
        #
        if isinstance ( s , str ) : return
        prps = s.getProperties().keys()
        if 'RootInTES' in prps :
            if s.getName() in to_modify:
                _set_root_in_tes_recursive_ ( s , root )
        #
        if not 'Members' in prps : return
        #
        for a in s.Members :
            _set_root_in_tes_ ( a , root, to_modify )


    def doMyChanges():

        to_modify = ['MyDSTWriterMainSeq']
        for method in methods:
            for charge in charge_list:
                for i in range(len(inputSelection_list)):
                    to_modify.append('Seq_input_selection'+method+charge+str(i))
                to_modify.append('SeqMatch_selection'+method+charge)
                to_modify.append('filter_selection'+method+charge)
                to_modify.append('Tuple'+method+charge)

        _set_root_in_tes_(DaVinci().mainSeq, '/Event', to_modify)
        #_set_root_in_tes_recursive_(DaVinci().mainSeq, '/Event')

        from Configurables import (
              Gaudi__DataLink as DataLink,
              DataOnDemandSvc
        )
        link_hlt1_sel = DataLink('Hlt1SelReportsLink', What='/Event/Turbo/Hlt1/SelReports', Target='/Event/Hlt1/SelReports')
        DataOnDemandSvc().AlgMap[link_hlt1_sel.Target] = link_hlt1_sel
        link_hlt1_dec = DataLink('Hlt1DecReportsLink', What='/Event/Turbo/Hlt1/DecReports', Target='/Event/Hlt1/DecReports')
        DataOnDemandSvc().AlgMap[link_hlt1_dec.Target] = link_hlt1_dec
        link_hlt2_sel = DataLink('Hlt2SelReportsLink', What='/Event/Turbo/Hlt2/SelReports', Target='/Event/Hlt2/SelReports')
        DataOnDemandSvc().AlgMap[link_hlt2_sel.Target] = link_hlt2_sel
        link_hlt2_dec = DataLink('Hlt2DecReportsLink', What='/Event/Turbo/Hlt2/DecReports', Target='/Event/Hlt2/DecReports')
        DataOnDemandSvc().AlgMap[link_hlt2_dec.Target] = link_hlt2_dec

    if isTurbo and not MDST : appendPostConfigAction(doMyChanges)
