###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# TO LOAD Tuple_Turbo.py, NEED TO ADD THIS LOCATION TO PYTHON PATH
import os,sys
sys.path.append(os.environ['TRACKCALIBPRODUCTIONROOT']+'/python/TrackCalibProduction')

from TrackCalibProduction.Tuple import set_Tuples

#######################
### INPUT           ###
#######################
datatype = "2015"        # FOR ALL CATEGORIES, SEE cond_db_dict IN Tuple_Turbo_test.py
sample = "Data_Turbo"          # DATA, MAG UP OR MAG DOWN
methods = ["VeloMuon","MuonTT","Downstream"]

# CALL FUNCTION THAT SETS DAVINCI OPTIONS WHICH DEPEND ON DATATYPE AND SAMPLE
set_Tuples(datatype,sample,methods,MDST=False,Test=False,isTurbo=True)
