'''Produce TrackCalib ntuples on 2011 MC, stripping output.'''

###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from TrackCalibProduction.Tuple import set_Tuples

#######################
### INPUT           ###
#######################
datatype = "2011"        # FOR ALL CATEGORIES, SEE cond_db_dict IN Tuple_Turbo_test.py
sample = "MC_UP_Sim09h" 
methods = ["VeloMuon","MuonTT","Downstream"]

# CALL FUNCTION THAT SETS DAVINCI OPTIONS WHICH DEPEND ON DATATYPE AND SAMPLE
set_Tuples(datatype, sample, methods, MDST=False, Test=False, isTurbo=False, filtered = True)
