/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "gsl/gsl_sys.h"
#include <boost/foreach.hpp>

// from Gaudi
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/Vector3DTypes.h"

// local
#include "TupleToolTrackEffMatch.h"

#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"

#include "Event/Particle.h"

//-----------------------------------------------------------------------------
// Implementation file for class : TupleToolTrackEffMatch
//
// 2018-10-12 : Renata Kopecna
// 2016-08-xx : Michael Kolpin
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( TupleToolTrackEffMatch )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TupleToolTrackEffMatch::TupleToolTrackEffMatch( const std::string& type,
                                        const std::string& name,
                                        const IInterface* parent )
  : TupleToolBase ( type, name , parent )
  //, m_lhcbid2mcparticles(NULL)
{
  declareInterface<IParticleTupleTool>(this);
  declareProperty("MatchedLocation1", m_matchedLocation1 = "/Event/Turbo/Hlt2TrackEffDiMuonMuonTTPlusMatchedTurboCalib/Particles"); //Two Turbo lines
  declareProperty("MatchedLocation2", m_matchedLocation2 = "/Event/Turbo/Hlt2TrackEffDiMuonDownstreamMinusHighStatMatchedTurboCalib/Particles");
  declareProperty("MC", m_MC = true);
  declareProperty("Turbo", m_Turbo = true);
  declareProperty("Method", m_method = "MuonTT");
  declareProperty("VeloAssocFrac", m_minVeloFracMatch = 0.5);
  declareProperty("MuonAssocFrac",m_minMuonFracMatch = 0.4);
  declareProperty("TTAssocFrac",  m_minTTFracMatch = 0.4);
  declareProperty("ITAssocFrac",  m_minITFracMatch = 0.4);
  declareProperty("OTAssocFrac",  m_minOTFracMatch = 0.4);
}
//=============================================================================
// Destructor
//=============================================================================
TupleToolTrackEffMatch::~TupleToolTrackEffMatch() {}

//=============================================================================
// initialize
//=============================================================================
StatusCode TupleToolTrackEffMatch::initialize(){
  if( ! TupleToolBase::initialize() ) return StatusCode::FAILURE;

  std::string p2mcAssocType("DaVinciSmartAssociator");
  m_p2mcAssoc = tool<IParticle2MCAssociator>(p2mcAssocType,this);

  return StatusCode::SUCCESS ;
}
//=============================================================================
// Fill
//=============================================================================
StatusCode TupleToolTrackEffMatch::fill( const LHCb::Particle* top
                                     , const LHCb::Particle* part
                                     , const std::string& head
                                     , Tuples::Tuple& tuple )
{
  const std::string prefix=fullName(head);
  if( m_method == "MuonTT" || m_method == "VeloMuon" || m_method == "Downstream"  || m_method == "DownMuon" ){ //for run I Downstream = DownMuon
    if ( msgLevel(MSG::VERBOSE) ) verbose() << "Using method "<< m_method << "." << endmsg;
  }
  else{
      if ( msgLevel(MSG::ERROR) ) error() << "Wrong method "<< m_method << " used! Fix your optionfile to set the method to MuonTT, VeloMuon, or Downstream!" << endmsg;
      return StatusCode::FAILURE;
  }

  // -- Skip the mother part.
  if( part == top ){
    if ( msgLevel(MSG::DEBUG) ) debug() << "Resonance does not have track: skipping" << endmsg;
    return StatusCode::SUCCESS;
  }
  else if ( msgLevel(MSG::DEBUG) ) debug() << "Resonance has a track: FILL" << endmsg;

  // -- Navigate back from the probe particle to the track.
  const LHCb::ProtoParticle* proto = part->proto();
  if(!proto){
    if ( msgLevel(MSG::VERBOSE) ) verbose() << "Could not find protoparticle corresponding to particle" << endmsg;
    return StatusCode::SUCCESS ;
  }
  else if ( msgLevel(MSG::VERBOSE) ) verbose() << "Found protoparticle corresponding to particle" << endmsg;

  const LHCb::Track* probeTrack = proto->track();
  if(!probeTrack){
    if ( msgLevel(MSG::WARNING) ) warning() <<   "Could not find track corresponding to protoparticle" << endmsg;
    return StatusCode::SUCCESS ;
  }
  else if ( msgLevel(MSG::VERBOSE) ) verbose() <<   "Found track corresponding to protoparticle" << endmsg;


  fillTagTrackInfo(probeTrack, head, tuple);

  // -- Get all long tracks in the event in the proper Tesla container/stripping
  if(m_Turbo ? (!exist<LHCb::Particles>(m_matchedLocation1) && !exist<LHCb::Particles>(m_matchedLocation2)) : (!exist<LHCb::Tracks>(LHCb::TrackLocation::Default))) {
    if (m_Turbo && msgLevel(MSG::VERBOSE) ) verbose() << "Containers " << m_matchedLocation1 << " and " << m_matchedLocation2 << " do not exist" << endmsg;
    if (!m_Turbo && msgLevel(MSG::ERROR) ) if ( msgLevel(MSG::ERROR) ) error() << "Container " << LHCb::TrackLocation::Default << " does not exist" << endmsg;
    tuple->column(head+"_Assoc", false).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_hasTTHits", false).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_Overlap_TTFraction", -1.).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_Overlap_TFraction", -1.).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_Overlap_ITFraction", -1.).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_Overlap_OTFraction", -1.).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_Overlap_VeloFraction", -1.).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_Overlap_MuonFraction", -1.).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_maxOverlap", -1.).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_Matched_StateCTB_tx",-1.e6 ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_Matched_StateCTB_ty",-1.e6 ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_Matched_StateCTB_x", -1.e6).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_Matched_StateCTB_y", -1.e6).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_Matched_StateCTB_z", -1.e6).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_Matched_Px", -1.e6).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_Matched_Py", -1.e6).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_Matched_Pz", -1.e6).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_Matched_P",  -1.e6).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_Matched_dP", -1.e6).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_Matched_probChi2", -1.e6 ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_Matched_Chi2NDoF", -1.e6).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_Matched_VeloChi2", -1.e6).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_Matched_VeloNDoF", -1.e6).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_Matched_GhostProb", -1.e6).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_DeltaR", -1.).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_DeltaP", -1.).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_DeltaPhi", -1.).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column(head+"_DeltaEta", -1.).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
     if (m_MC){
          tuple->column(head+"_Matched_SameMCPart", 0).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
          tuple->column(head+"_MCPart", false).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
          tuple->column(head+"_Matched_MCPart", false).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
          tuple->column(head+"_MatchedMC_Px",  -1.e6 ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
          tuple->column(head+"_MatchedMC_Py",  -1.e6 ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
          tuple->column(head+"_MatchedMC_Pz",  -1.e6 ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
          tuple->column(head+"_MatchedMC_P",   -1.e6 ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
          tuple->column(head+"_MatchedMC_PID", -1.e6 ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    }
    if ( msgLevel(MSG::VERBOSE) ) verbose() << "TupleToolTrackEffMatch done!" << endmsg;
    return StatusCode::SUCCESS;
  }

  if ( msgLevel(MSG::VERBOSE) ) verbose() << "Matched container found, reading in tracks!" << endmsg;
  // ---------------------------------------------------------------

  //define used tracks
  const LHCb::Track* assocTrack = NULL;
  const LHCb::MuonPID* assocMuPID = NULL; //new
  const LHCb::Track* longTrack = NULL;
  const LHCb::Particle::Range parts1 = getIfExists<LHCb::Particle::Range>(m_matchedLocation1);
  const LHCb::Particle::Range parts2 = getIfExists<LHCb::Particle::Range>(m_matchedLocation2);
  const LHCb::Particle *longPart = NULL;
  const LHCb::ProtoParticle *longProto = NULL;
  std::vector<LHCb::Particle> longParts;

  //loop over all formerly matched track and keep the one with highest overlap
  bool assoc = false;
  double maxOverlap = -1;
  double TTFracMatch    = -1.;
  double TFracMatch     = -1.;
  double ITFracMatch    = -1.;
  double OTFracMatch    = -1.;
  double VeloFracMatch  = -1.;
  double MuonFracMatch  = -1.;

  double TTFracMatchTmp = -1.;
  double TFracMatchTmp = -1.;
  double ITFracMatchTmp = -1.;
  double OTFracMatchTmp = -1.;
  double VeloFracMatchTmp = -1.;
  double MuonFracMatchTmp = -1.;

  bool hasTTHits   = false;
  bool hasTTHitsTmp   = false;

  bool minOverlap = false;  //Not necessary for Turbo output, miunOverlap required in trigger
  bool biggerOverlap = false;

  //Minimal overlap fraction for associated tracks (only for stripping, in Turbo done at the trigger level)
  const double minVeloFracMatch = m_minVeloFracMatch;
  const double minMuonFracMatch = m_minMuonFracMatch;
  const double minTTFracMatch = m_minTTFracMatch;
  const double minITFracMatch = m_minITFracMatch;
  const double minOTFracMatch = m_minOTFracMatch;

  int nCandidates = 0; //check if there are any associated track candidates

  if ( msgLevel(MSG::VERBOSE) ) verbose() << "Searching for track with highest overlap" << endmsg;

  // -- Loop over all particles from stripping --//
  if (!m_Turbo){
      LHCb::Tracks* tracks = get<LHCb::Tracks>(LHCb::TrackLocation::Default);
      for( LHCb::Tracks::const_iterator it = tracks->begin() ; it != tracks->end() ; ++it){
          longTrack = *it;
          if(longTrack->type() != 3) continue; // -- track has to be a long track
          if ( msgLevel(MSG::VERBOSE) ) verbose() << "Checking long track charge: " << longTrack->charge() << endmsg;
          if ((longTrack->charge()/(fabs(longTrack->charge()))) != -(part->particleID().pid()/(fabs(part->particleID().pid())))) continue; //has to have the right charge
          nCandidates++;

          TTFracMatchTmp = -1.;
          TFracMatchTmp = -1.;
          ITFracMatchTmp = -1.;
          OTFracMatchTmp = -1.;
          VeloFracMatchTmp = -1.;
          MuonFracMatchTmp = -1.;
          hasTTHitsTmp = false;

          commonLHCbIDs(longTrack, probeTrack, NULL, MuonFracMatchTmp, TTFracMatchTmp, TFracMatchTmp, ITFracMatchTmp, OTFracMatchTmp, VeloFracMatchTmp, hasTTHitsTmp);

          //Check for minimal overlaps + biggest overlaps
          if (m_method == "VeloMuon"){
              minOverlap    = bool ( VeloFracMatchTmp > minVeloFracMatch && MuonFracMatchTmp > minMuonFracMatch);
              biggerOverlap = bool ( VeloFracMatchTmp > VeloFracMatch    && MuonFracMatchTmp > MuonFracMatch);
          }
          else if (m_method == "MuonTT"){
              minOverlap    = bool ( (MuonFracMatchTmp > minMuonFracMatch)  && (hasTTHitsTmp ? (TTFracMatchTmp > minTTFracMatch) : true) ); // TT doesn't have to have hits
              biggerOverlap = bool ( (MuonFracMatchTmp > MuonFracMatch)     && (hasTTHitsTmp ? (TTFracMatchTmp > TTFracMatch) : true) ); // TT doesn't have to have hits
          }
          else{//Downstream
              minOverlap    = bool ( (TTFracMatchTmp > minTTFracMatch)  && (ITFracMatchTmp > minITFracMatch || OTFracMatchTmp > minOTFracMatch  ) );
              biggerOverlap = bool ( (TTFracMatchTmp > TTFracMatch)     && (ITFracMatchTmp > ITFracMatch    || OTFracMatchTmp > OTFracMatch     ) );
          }
          if (minOverlap){
              assoc = true; //associate doesn't mean matched
              if (biggerOverlap){
                  assocTrack = longTrack;
                  TTFracMatch = TTFracMatchTmp;
                  TFracMatch = TFracMatchTmp;
                  ITFracMatch = ITFracMatchTmp;
                  OTFracMatch = OTFracMatchTmp;
                  VeloFracMatch = VeloFracMatchTmp;
                  MuonFracMatch = MuonFracMatchTmp;
                  hasTTHits   = hasTTHitsTmp;
              }
          }
          if ( msgLevel(MSG::DEBUG) ){
              debug() << "Overlap of track " << it-tracks->begin() << ": ";
              if (m_method == "VeloMuon")   debug() << "Velo "  << VeloFracMatch  << ", Muon "  << MuonFracMatch << endmsg;
              else if (m_method == "MuonTT")debug() << "TT "    << TTFracMatch    << ", Muon "  << MuonFracMatch << endmsg;
              else                          debug() << "TT "    << TTFracMatch    << ", IT "    << ITFracMatchTmp << ", 0T " << OTFracMatchTmp <<endmsg;
          }

      }
  }
  // -- Loop over all particles from Turbo --//
  else{
      //check HighStat
      if ( exist<LHCb::Particles>(m_matchedLocation1)){
          if ( msgLevel(MSG::VERBOSE) ) verbose() << "Reading from TURBO location " <<  m_matchedLocation1 << endmsg;
          BOOST_FOREACH( const LHCb::Particle* tmpJpsi, parts1) { //new
              const LHCb::Particle::ConstVector jpsi_daughters = tmpJpsi->daughtersVector(); //new
              BOOST_FOREACH( const LHCb::Particle* tmpPart, jpsi_daughters) { //new
                  //only successfully matched tracks are saved, where pion ID is assigned to the particle
                  //multiple tracks with sufficient overlap can be found, fill into vector and check for overlap of each with probe
                  if ((fabs(tmpPart->particleID().pid()) != 211 ) ) continue;
                  longPart = tmpPart;
                  if ((longPart->particleID().pid()/(fabs(longPart->particleID().pid()))) != -(part->particleID().pid()/(fabs(part->particleID().pid())))) continue; //has to have the right charge
                  longParts.push_back(*longPart);
                  longProto = tmpPart->proto();
                  longTrack = longProto->track();
                  if (longProto->muonPID()) {           //new
                      assocMuPID = longProto->muonPID();     //new
                  } //new
                  else assocMuPID = NULL; //new
                  nCandidates++;
                  assoc = true;

                  TTFracMatchTmp = -1.;
                  TFracMatchTmp = -1.;
                  ITFracMatchTmp = -1.;
                  OTFracMatchTmp = -1.;
                  VeloFracMatchTmp = -1.;
                  MuonFracMatchTmp = -1.;
                  hasTTHitsTmp = false;

                  commonLHCbIDs(longTrack, probeTrack, assocMuPID, MuonFracMatchTmp, TTFracMatchTmp, TFracMatchTmp, ITFracMatchTmp, OTFracMatchTmp, VeloFracMatchTmp, hasTTHitsTmp); //new
                  if (MuonFracMatchTmp+TTFracMatchTmp+TFracMatchTmp+VeloFracMatchTmp > maxOverlap){
                      maxOverlap = (MuonFracMatchTmp+TTFracMatchTmp+TFracMatchTmp+VeloFracMatchTmp);
                      assocTrack = longTrack;
                      TTFracMatch = TTFracMatchTmp;
                      TFracMatch = TFracMatchTmp;
                      ITFracMatch = ITFracMatchTmp;
                      OTFracMatch = OTFracMatchTmp;
                      VeloFracMatch = VeloFracMatchTmp;
                      MuonFracMatch = MuonFracMatchTmp;
                      hasTTHits   = hasTTHitsTmp;
                  }
                  if ( msgLevel(MSG::DEBUG) ) debug() << "Overlap of track : " << (MuonFracMatchTmp+TTFracMatchTmp+TFracMatchTmp+VeloFracMatchTmp) << endmsg;
                  if ( msgLevel(MSG::DEBUG) ) debug() << "Maximum overlap in this event: " << maxOverlap <<  endmsg;

              }
          }
      }
      //check LowStat
      if (exist<LHCb::Particles>(m_matchedLocation2)) {
          if ( msgLevel(MSG::VERBOSE) ) verbose() << "Reading from TURBO location " <<  m_matchedLocation2 << endmsg;
          BOOST_FOREACH( const LHCb::Particle* tmpJpsi, parts2) { //new
              const LHCb::Particle::ConstVector jpsi_daughters = tmpJpsi->daughtersVector(); //new
              BOOST_FOREACH( const LHCb::Particle* tmpPart, jpsi_daughters) { //new
                  //only successfully matched tracks are saved, where pion ID is assigned to the particle
                  //multiple tracks with sufficient overlap can be found, fill into vector and check for overlap of each with probe
                  if ((fabs(tmpPart->particleID().pid()) != 211 ) ) continue;
                  longPart = tmpPart;
                  if ((longPart->particleID().pid()/(fabs(longPart->particleID().pid()))) != -(part->particleID().pid()/(fabs(part->particleID().pid())))) continue; //has to have the right charge
                  longParts.push_back(*longPart);
                  longProto = tmpPart->proto();
                  longTrack = longProto->track();
                  if (longProto->muonPID()) {           //new
                      assocMuPID = longProto->muonPID();     //new
                  } //new
                  else assocMuPID = NULL; //new
                  nCandidates++;
                  assoc = true;

                  TTFracMatchTmp = -1.;
                  TFracMatchTmp = -1.;
                  ITFracMatchTmp = -1.;
                  OTFracMatchTmp = -1.;
                  VeloFracMatchTmp = -1.;
                  MuonFracMatchTmp = -1.;
                  hasTTHitsTmp = false;

                  commonLHCbIDs(longTrack, probeTrack, assocMuPID, MuonFracMatchTmp, TTFracMatchTmp, TFracMatchTmp, ITFracMatchTmp, OTFracMatchTmp, VeloFracMatchTmp, hasTTHitsTmp); //new
                  if (MuonFracMatchTmp+TTFracMatchTmp+TFracMatchTmp+VeloFracMatchTmp > maxOverlap){
                      maxOverlap = (MuonFracMatchTmp+TTFracMatchTmp+TFracMatchTmp+VeloFracMatchTmp);
                      assocTrack = longTrack;
                      TTFracMatch = TTFracMatchTmp;
                      TFracMatch = TFracMatchTmp;
                      ITFracMatch = ITFracMatchTmp;
                      OTFracMatch = OTFracMatchTmp;
                      VeloFracMatch = VeloFracMatchTmp;
                      MuonFracMatch = MuonFracMatchTmp;
                      hasTTHits   = hasTTHitsTmp;
                  }
                  if ( msgLevel(MSG::DEBUG) ) debug() << "Overlap of track : " << (MuonFracMatchTmp+TTFracMatchTmp+TFracMatchTmp+VeloFracMatchTmp) << endmsg;
                  if ( msgLevel(MSG::DEBUG) ) debug() << "Maximum overlap in this event: " << maxOverlap <<  endmsg;
              }
          }
      }
  }

  if ( msgLevel(MSG::VERBOSE) ) verbose() << "Long tracks candidates " << nCandidates << endmsg;
  if (nCandidates == 0){
      tuple->column(head+"_Assoc", false).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_hasTTHits", false).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_Overlap_TTFraction", -1.).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_Overlap_TFraction", -1.).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_Overlap_ITFraction", -1.).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_Overlap_OTFraction", -1.).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_Overlap_VeloFraction", -1.).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_Overlap_MuonFraction", -1.).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_maxOverlap", -1.).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_Matched_StateCTB_tx",-1.e6 ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_Matched_StateCTB_ty",-1.e6 ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_Matched_StateCTB_x", -1.e6).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_Matched_StateCTB_y", -1.e6).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_Matched_StateCTB_z", -1.e6).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_Matched_Px", -1.e6).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_Matched_Py", -1.e6).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_Matched_Pz", -1.e6).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_Matched_P",  -1.e6).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_Matched_dP", -1.e6).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_Matched_probChi2", -1.e6 ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_Matched_Chi2NDoF", -1.e6).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_Matched_VeloChi2", -1.e6).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_Matched_VeloNDoF", -1.e6).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_Matched_GhostProb", -1.e6).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_DeltaR", -1.).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_DeltaP", -1.).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_DeltaPhi", -1.).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_DeltaEta", -1.).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      if (m_MC){
            tuple->column(head+"_Matched_SameMCPart", 0).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
            tuple->column(head+"_MCPart", false).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
            tuple->column(head+"_Matched_MCPart", false).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
            tuple->column(head+"_MatchedMC_Px",  -1.e6 ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
            tuple->column(head+"_MatchedMC_Py",  -1.e6 ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
            tuple->column(head+"_MatchedMC_Pz",  -1.e6 ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
            tuple->column(head+"_MatchedMC_P",   -1.e6 ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
            tuple->column(head+"_MatchedMC_PID", -1.e6 ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      }
      if ( msgLevel(MSG::VERBOSE) ) verbose() << "TupleToolTrackEffMatch done!" << endmsg;
      return StatusCode::SUCCESS;
  }

  if ( msgLevel(MSG::DEBUG) ) debug() << "Is there an associated track? " << assoc <<  endmsg;

  tuple->column(head+"_Assoc", assoc).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  tuple->column(head+"_hasTTHits", hasTTHits).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  tuple->column(head+"_maxOverlap", maxOverlap).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  tuple->column(head+"_Overlap_TTFraction", TTFracMatch).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  tuple->column(head+"_Overlap_TFraction",  TFracMatch).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  tuple->column(head+"_Overlap_ITFraction", ITFracMatch).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  tuple->column(head+"_Overlap_OTFraction", OTFracMatch).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  tuple->column(head+"_Overlap_VeloFraction", VeloFracMatch).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  tuple->column(head+"_Overlap_MuonFraction", MuonFracMatch).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);

  // -- This ends the basic functionality of this tool. The rest is for more special cases and MC association

  const LHCb::State *stateBeam = NULL;

  if ( assocTrack && assocTrack->hasStateAt(LHCb::State::ClosestToBeam) ){
      if ( msgLevel(MSG::VERBOSE) ) verbose()<< "Getting state closest to beam infos!" << endmsg;
      stateBeam = assocTrack->stateAt(LHCb::State::ClosestToBeam);
      tuple->column( head+"_Matched_StateCTB_tx",stateBeam->tx() ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column( head+"_Matched_StateCTB_ty",stateBeam->ty() ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column( head+"_Matched_StateCTB_x",stateBeam->x() ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column( head+"_Matched_StateCTB_y",stateBeam->y() ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column( head+"_Matched_StateCTB_z",stateBeam->z() ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  }
  else{
      tuple->column( head+"_Matched_StateCTB_tx",-1.e6 ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column( head+"_Matched_StateCTB_ty",-1.e6 ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column( head+"_Matched_StateCTB_x", -1.e6).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column( head+"_Matched_StateCTB_y", -1.e6).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column( head+"_Matched_StateCTB_z", -1.e6).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  }

  if ( msgLevel(MSG::VERBOSE) ) verbose() << "Checking kinematics of associated track!" << endmsg;

  // -- Associate MC particle to the long track that was associated to the probe track.
  const LHCb::MCParticle* assocPartMC = NULL;
  const LHCb::MCParticle* probePartMC = NULL;
  if( m_MC && assocTrack ) assocPartMC = assocMCPart( assocTrack );
  if( m_MC ) probePartMC = assocMCPart( probeTrack );

  double sigma_pLong = -1000.0;
  double pChi2Long =  -1000.0;
  double chi2NDoFLong = -1000.0;

  double deltaR = -1;
  double deltaP = -1;

  double deltaPhi = -1000.0;
  double deltaEta = -1000.0;

  double assocTrackPX = -1e10;
  double assocTrackPY = -1e10;
  double assocTrackPZ = -1e10;
  double assocTrackP = -1e10;
  double assocVeloChi2 = -1e10;
  double assocVeloNDoF = -1e10;
  double assocGhostProb = -1e10;

  //MC variables
  int sameTrack = -1;
  bool probeMC = (probePartMC != NULL);
  bool assocMC = (assocPartMC != NULL);

  double assocPartMCPX = -1e10;
  double assocPartMCPY = -1e10;
  double assocPartMCPZ = -1e10;
  double assocPartMCP  = -1e10;
  double assocPartMCPID=  0;

  if ( assocPartMC && probePartMC){
      if (assocPartMC == probePartMC) sameTrack = 1;
      else sameTrack = 0;
  }

  //get additional information of the associated track

  if( assocTrack != NULL ){

      assocTrackPX = assocTrack->momentum().X();
      assocTrackPY = assocTrack->momentum().Y();
      assocTrackPZ = assocTrack->momentum().Z();
      assocTrackP = assocTrack->p();

      assocVeloChi2 = assocTrack->info(LHCb::Track::FitVeloChi2,-1);
      assocVeloNDoF = assocTrack->info(LHCb::Track::FitVeloNDoF,-1);
      assocGhostProb = assocTrack->ghostProbability();
      sigma_pLong = sqrt(assocTrack->firstState().errP2());
      pChi2Long =  assocTrack->probChi2();
      chi2NDoFLong = assocTrack->chi2()/assocTrack->nDoF();

      // ----------------------------------------------------------

      // -- Calculate Delta R between the probe track and the associated track
      deltaPhi = fabs( assocTrack->phi() - probeTrack->phi() );
      if(deltaPhi > M_PI) deltaPhi  = 2*M_PI-deltaPhi;
      deltaEta = assocTrack->pseudoRapidity() - probeTrack->pseudoRapidity();
      deltaR = sqrt( deltaPhi*deltaPhi + deltaEta*deltaEta );
      deltaP = assocTrack->p() - probeTrack->p();
      // -----------------------------------------------------------------

      if ( msgLevel(MSG::VERBOSE) ){
            verbose() << "assocTrackPX " <<  assocTrackPX  << "  " << assocTrack->momentum().X() << endmsg;
            verbose() << "assocTrackPY " <<  assocTrackPY  << "  " << assocTrack->momentum().Y() << endmsg;
            verbose() << "assocTrackPZ " << assocTrackPZ   << "  " << assocTrack->momentum().Z() << endmsg;
            verbose() << "assocTrackP "  <<  assocTrackP   << "  " << assocTrack->p() << endmsg;

            verbose() <<  "assocVeloChi2 "   << assocVeloChi2  << "  " << assocTrack->info(LHCb::Track::FitVeloChi2,-1)<< endmsg;
            verbose() <<  "assocVeloNDoF "   << assocVeloNDoF  << "  " << assocTrack->info(LHCb::Track::FitVeloNDoF,-1)<< endmsg;
            verbose() <<  "assocGhostProb "  << assocGhostProb << "  " << assocTrack->ghostProbability()<< endmsg;
            verbose() <<  "sigma_pLong "     << sigma_pLong    << "  " << sqrt(assocTrack->firstState().errP2())   << endmsg;
            verbose() <<  "pChi2Long "       << pChi2Long      << "  " << assocTrack->probChi2()<< endmsg;
            verbose() <<  "chi2NDoFLong "    << chi2NDoFLong   << "  " << assocTrack->chi2()/assocTrack->nDoF() << endmsg;

            verbose() <<  "deltaPhi "    << deltaPhi   << "  " <<  fabs( assocTrack->phi() - probeTrack->phi() ) << "  " <<  2*M_PI-deltaPhi << endmsg;
            verbose() <<  "deltaEta "    << deltaEta   << "  " <<  assocTrack->pseudoRapidity() - probeTrack->pseudoRapidity() << endmsg;
            verbose() <<  "deltaR "      << deltaR     << "  " <<  sqrt( deltaPhi*deltaPhi + deltaEta*deltaEta ) << endmsg;
            verbose() <<  "deltaP "      << deltaP     << "  " <<  assocTrack->p() - probeTrack->p() << endmsg;
      }
  }

  // -- And now fill the tuple...

  tuple->column(head+"_Matched_Px", assocTrackPX).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  tuple->column(head+"_Matched_Py", assocTrackPY).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  tuple->column(head+"_Matched_Pz", assocTrackPZ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  tuple->column(head+"_Matched_P",  assocTrackP).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);

  tuple->column(head+"_Matched_dP", sigma_pLong).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  tuple->column(head+"_Matched_probChi2", pChi2Long ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  tuple->column(head+"_Matched_Chi2NDoF", chi2NDoFLong).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  tuple->column(head+"_Matched_VeloChi2", assocVeloChi2).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  tuple->column(head+"_Matched_VeloNDoF", assocVeloNDoF).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  tuple->column(head+"_Matched_GhostProb", assocGhostProb).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);

  tuple->column(head+"_DeltaR", deltaR).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  tuple->column(head+"_DeltaP", deltaP).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  tuple->column(head+"_DeltaPhi", deltaPhi).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  tuple->column(head+"_DeltaEta", deltaEta).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);

  if(m_MC){
      tuple->column(head+"_Matched_SameMCPart", sameTrack).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_MCPart", probeMC).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_Matched_MCPart", assocMC).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      if (assocMC){
            assocPartMCPX  = assocPartMC->momentum().x();
            assocPartMCPY  = assocPartMC->momentum().y();
            assocPartMCPZ  = assocPartMC->momentum().z();
            assocPartMCP   = assocPartMC->p();
            assocPartMCPID = assocPartMC->particleID().pid();
      }

      tuple->column(head+"_MatchedMC_Px",  assocPartMCPX).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_MatchedMC_Py",  assocPartMCPY).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_MatchedMC_Pz",  assocPartMCPZ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_MatchedMC_P",   assocPartMCP).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column(head+"_MatchedMC_PID", assocPartMCPID).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  }

  if ( msgLevel(MSG::VERBOSE) ) verbose() << "TupleToolTrackEffMatch done!" << endmsg;
  return StatusCode::SUCCESS ;

}
//=============================================================================
// Count what fractions of LHCbIDs are the same for two tracks in numerous subdetectors
//=============================================================================
void TupleToolTrackEffMatch::commonLHCbIDs(const LHCb::Track* longTrack, const LHCb::Track* probeTrack, const LHCb::MuonPID* longMuPID, double& muFraction, double& TTFraction, double& TFraction,
                                           double& ITFraction, double& OTFraction, double& VeloFraction, bool& hasTTHits){

    //check correct history of the tag track
  if (longTrack->history() != LHCb::Track::PatForward && longTrack->history() != LHCb::Track::PatMatch ){
       muFraction = -2;
       TTFraction = -2;
       TFraction = -2;
       ITFraction = -2;
       OTFraction = -2;
       VeloFraction = -2;
       if ( msgLevel(MSG::WARNING) ) warning() << "LongTracks history is " << longTrack->history()<< "! Should be PatForward or PatMatch. "  << endmsg;
       return;
  }

  // -- Get the LHCbIDs of the Probe-track and fill them into containers for each subdetector
  std::vector<LHCb::LHCbID> probeTrackIDs = probeTrack->lhcbIDs();
  std::vector<LHCb::LHCbID> probeTrackTTIDs;
  std::vector<LHCb::LHCbID> probeTrackTIDs;
  std::vector<LHCb::LHCbID> probeTrackITIDs;
  std::vector<LHCb::LHCbID> probeTrackOTIDs;
  std::vector<LHCb::LHCbID> probeTrackMuonIDs;
  std::vector<LHCb::LHCbID> probeTrackVeloIDs;

  std::vector<LHCb::LHCbID> longTrackIDs = longTrack->lhcbIDs();
  std::vector<LHCb::LHCbID> longTrackTTIDs;
  std::vector<LHCb::LHCbID> longTrackTIDs;
  std::vector<LHCb::LHCbID> longTrackITIDs;
  std::vector<LHCb::LHCbID> longTrackOTIDs;
  std::vector<LHCb::LHCbID> longTrackMuonIDs;
  std::vector<LHCb::LHCbID> longTrackVeloIDs;

  int muonMatch = 0;
  hasTTHits = false;

  // -- Get the LHCbIDs of the Probe-track and fill them into a Muon container (T-station & long methods from stripping)
  if(!m_Turbo) commonMuonLHCbIDs(longTrackIDs, longTrackMuonIDs, probeTrack, muonMatch); //new
  else if (longMuPID && longMuPID->muonTrack()) longTrackMuonIDs = longMuPID->muonTrack()->lhcbIDs(); //new

  // -- Check if the long track has TT/Velo/Muon hits
  for( std::vector<LHCb::LHCbID>::iterator it = longTrackIDs.begin() ; it != longTrackIDs.end() ; ++it){
    LHCb::LHCbID id = *it;
    if (id.isTT()){
        hasTTHits = true;
        longTrackTTIDs.push_back(id);
    }
    if(id.isIT() || id.isOT()) longTrackTIDs.push_back(id);
    if(id.isIT()) longTrackITIDs.push_back(id);
    if(id.isOT()) longTrackOTIDs.push_back(id);
    if(id.isVelo()) longTrackVeloIDs.push_back(id);
    //if(m_Turbo && id.isMuon()) longTrackMuonIDs.push_back(id);
  }
  // -- Sort LHCbIDs of probe track in subdetector IDs
  for( std::vector<LHCb::LHCbID>::iterator it = probeTrackIDs.begin() ; it != probeTrackIDs.end() ; ++it){
    LHCb::LHCbID id = (*it);
    if(id.isTT()) probeTrackTTIDs.push_back(id);
    if(id.isIT() || id.isOT()) probeTrackTIDs.push_back(id);
    if(id.isIT()) probeTrackITIDs.push_back(id);
    if(id.isOT()) probeTrackOTIDs.push_back(id);
    if(id.isVelo()) probeTrackVeloIDs.push_back(id);
    if(id.isMuon()) probeTrackMuonIDs.push_back(id);
  }

  bool verb = false;
  if ( msgLevel(MSG::DEBUG) && verb )  {
      verbose() << "probeTrack velo IDs: " << endmsg;
      for(auto const& id:probeTrackVeloIDs) {
          verbose() << "\t" << id << endmsg;
      }
      verbose() << "longTrack velo IDs: " << endmsg;
      for(auto const& id:longTrackVeloIDs) {
          verbose() << "\t" << id << endmsg;
      }

      verbose() << "probeTrack TT IDs: " << endmsg;
      for(auto const& id:probeTrackTTIDs) {
          verbose() << "\t" << id << endmsg;
      }
      verbose() << "longTrack II IDs: " << endmsg;
      for(auto const& id:probeTrackTTIDs) {
          verbose() << "\t" << id << endmsg;
      }

      verbose() << "probeTrack muon IDs: " << endmsg;
      for(auto const& id:probeTrackMuonIDs) {
          verbose() << "\t" << id << endmsg;
      }
      verbose() << "longTrack muon IDs: " << endmsg;
      for(auto const& id:longTrackMuonIDs) {
          verbose() << "\t" << id << endmsg;
      }
  }

  // -- Count common hist
  double OverlapTT  = nOverlap(probeTrackTTIDs, longTrackTTIDs, verb);
  double OverlapT   = nOverlap(probeTrackTIDs,  longTrackTIDs,  verb);
  double OverlapIT  = nOverlap(probeTrackITIDs, longTrackITIDs, verb);
  double OverlapOT  = nOverlap(probeTrackOTIDs, longTrackOTIDs, verb);
  double OverlapVelo = nOverlap(probeTrackVeloIDs, longTrackVeloIDs, verb);
  double OverlapMuon = m_Turbo ? (nOverlap(probeTrackMuonIDs, longTrackMuonIDs, verb)) : muonMatch;

  TTFraction =  (probeTrackTTIDs.size()!=0   && longTrackTTIDs.size()!=0) ?     std::max(double(OverlapTT)/probeTrackTTIDs.size(),double(OverlapTT)/longTrackTTIDs.size()) : 0.;
  TFraction =   (probeTrackTIDs.size()!=0    && longTrackTIDs.size()!=0)  ?     std::max(double(OverlapT)/probeTrackTIDs.size(),double(OverlapT)/longTrackTIDs.size())     : 0.;
  ITFraction =  (probeTrackITIDs.size()!=0   && longTrackITIDs.size()!=0) ?     std::max(double(OverlapIT)/probeTrackITIDs.size(),double(OverlapIT)/longTrackITIDs.size()) : 0.;
  OTFraction =  (probeTrackOTIDs.size()!=0   && longTrackOTIDs.size()!=0) ?     std::max(double(OverlapOT)/probeTrackOTIDs.size(),double(OverlapOT)/longTrackOTIDs.size()) : 0.;
  VeloFraction =(probeTrackVeloIDs.size()!=0 && longTrackVeloIDs.size()!=0) ?   std::max(double(OverlapVelo)/probeTrackVeloIDs.size(),double(OverlapVelo)/longTrackVeloIDs.size()) : 0.;
  muFraction =  (probeTrackMuonIDs.size()!=0 && longTrackMuonIDs.size()!=0) ?   std::max(double(OverlapMuon)/probeTrackMuonIDs.size(),double(OverlapMuon)/longTrackMuonIDs.size()) : 0.;

  if ( msgLevel(MSG::VERBOSE) ){
      verbose() << "TTFraction: "   << TTFraction   << endmsg;
      verbose() << "TFraction: "    << TFraction    << endmsg;
      verbose() << "ITFraction: "   << ITFraction   << endmsg;
      verbose() << "OTFraction: "   << OTFraction   << endmsg;
      verbose() << "VeloFraction: " << VeloFraction << endmsg;
      verbose() << "muFraction: "   << muFraction   << endmsg;
      verbose() << "hasTTHits: "    << hasTTHits    << endmsg;
  }

}

void TupleToolTrackEffMatch::commonMuonLHCbIDs(std::vector<LHCb::LHCbID> &longTrackIDs, std::vector<LHCb::LHCbID> &longTrackMuonIDs, const LHCb::Track* probeTrack, int&muonMatch){

    if (!exist<LHCb::MuonPIDs>( LHCb::MuonPIDLocation::Offline )){
        if ( msgLevel(MSG::WARNING) ) warning() << "Muon container not found!" << endmsg; //TODO
        muonMatch = 0;
        return;
    }

    LHCb::MuonPIDs* muPIDs   = get<LHCb::MuonPIDs>( LHCb::MuonPIDLocation::Offline );
    const LHCb::Track* muonTrack  = NULL;
    const LHCb::Track* idTrack = NULL;

    for(LHCb::MuonPIDs::iterator iMu = muPIDs->begin() ; iMu != muPIDs->end() ; ++iMu){
        LHCb::MuonPID* muPID = (*iMu);
        muonTrack = muPID->muonTrack(); // The track segment in the muon station
        idTrack = muPID->idTrack(); // The track which was id'ed
        if(muonTrack == NULL || idTrack == NULL) continue;
        if (longTrackIDs != idTrack->lhcbIDs()) continue; //check if long track is the id'ed muon track

        longTrackMuonIDs = muonTrack->lhcbIDs();
        longTrackIDs = idTrack->lhcbIDs();
        muonMatch =(int)muonTrack->nCommonLhcbIDs( *probeTrack );

        if ( msgLevel(MSG::VERBOSE) ){
            verbose() << "commonMuonLHCbIDs output:" << endmsg << endmsg;
            verbose() << "muonTrack nCommonLhcbIDs: " << muonMatch << endmsg;
            verbose() << "muonTrack muon IDs size: " << longTrackMuonIDs.size() << endmsg;
        }
    }
}

//=============================================================================
// Count overlapping LHCb IDs for two ID vectors
//=============================================================================
size_t TupleToolTrackEffMatch::nOverlap(const std::vector<LHCb::LHCbID> list1, const std::vector<LHCb::LHCbID> list2, bool verb){

  if ( msgLevel(MSG::DEBUG)  && verb){
      debug()<< "probe hits: " << list1.size() <<endmsg;
      debug()<< "long hits: " << list2.size() << endmsg;
  }
  std::vector<LHCb::LHCbID>::const_iterator first1 = list2.begin() ;
  std::vector<LHCb::LHCbID>::const_iterator last1  = list2.end() ;
  std::vector<LHCb::LHCbID>::const_iterator first2 = list1.begin() ;
  std::vector<LHCb::LHCbID>::const_iterator last2  = list1.end() ;

  size_t rc(0) ;

  while (first1 != last1 && first2 != last2)
  {
    if ( *first1 < *first2 )
    {
      ++first1;
    }
    else if ( *first2 < *first1 )
    {
      ++first2;
    }
    else
    {
      ++first1;
      ++first2;
      ++rc ;
    }
  }
   if ( msgLevel(MSG::DEBUG) && verb )  {
      debug()<< "# common hits: " << rc << endmsg;
      if (rc == 0)  debug()<< "NO COMMON HITS!" << endmsg;
  }
  return rc;
}

//=============================================================================
// Check if the associated track really belongs to a muon from the corresponding mother in MC
//=============================================================================
const LHCb::MCParticle* TupleToolTrackEffMatch::assocMCPart( const LHCb::Track* assocTrack ){

  LHCb::Particle::Range longTracks;

  // -- Warning: This is hardcoded!
  if(exist<LHCb::Particle::Range>("Phys/StdAllLooseMuons/Particles")){
    longTracks = get<LHCb::Particle::Range>("Phys/StdAllLooseMuons/Particles");

  }else{
    if ( msgLevel(MSG::DEBUG) ) debug() << "Could not find container: " << "Phys/StdAllLooseMuons/Particles" << endmsg;
    return NULL;
  }

  const LHCb::Particle* assocPart = NULL;

  for(LHCb::Particle::Range::const_iterator it = longTracks.begin() ; it != longTracks.end() ; ++it){

    const LHCb::Particle* part = (*it);
    if( !part->proto() ) continue;
    if( !part->proto()->track() ) continue;

    const LHCb::Track* track = part->proto()->track();

    if( track == assocTrack){
      assocPart = part;
      break;
    }

  }

  if( assocPart == NULL) return NULL;

  const LHCb::MCParticle* mcp = NULL;
  if( assocPart->particleID().pid() != 0){
    mcp = m_p2mcAssoc->relatedMCP( assocPart );
  }

  return mcp;

}


//=============================================================================
// Add tag info as in TupleToolTrackInfo (in Stripping probe causes trouble)
//=============================================================================
void TupleToolTrackEffMatch::fillTagTrackInfo(const LHCb::Track* tagTrack, const std::string& head, Tuples::Tuple& tuple){

    if (msgLevel(MSG::DEBUG))
        debug() << head << " " << tagTrack->type() << " "+head+"_TRACK_CHI2 " << tagTrack->chi2() << endmsg ;
    if (msgLevel(MSG::VERBOSE)) verbose() << *tagTrack << endmsg ;

    tuple->column( head+"_TRACK_Type",  tagTrack->type() ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column( head+"_TRACK_Key",   tagTrack->key() ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column( head+"_TRACK_CHI2",  tagTrack->chi2() ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    int nDoF = tagTrack->nDoF();
    tuple->column( head+"_TRACK_NDOF",  nDoF ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    if (nDoF) {
        tuple->column( head+"_TRACK_CHI2NDOF", tagTrack->chi2()/nDoF ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
        //tuple->column( head+"_TRACK_PCHI2", tagTrack->probChi2() );
        if ( tagTrack->info(LHCb::Track::AdditionalInfo::FitVeloNDoF,0) > 0 )  {
            tuple->column( head+"_TRACK_VeloCHI2NDOF",
                                   tagTrack->info(LHCb::Track::AdditionalInfo::FitVeloChi2, -1.)/
                                   tagTrack->info(LHCb::Track::AdditionalInfo::FitVeloNDoF, 0) ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
        }
        else tuple->column( head+"_TRACK_VeloCHI2NDOF",-1.).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
        if ( tagTrack->info(LHCb::Track::AdditionalInfo::FitTNDoF,0) > 0 )  {
            tuple->column( head+"_TRACK_TCHI2NDOF",
                                   tagTrack->info(LHCb::Track::AdditionalInfo::FitTChi2, -1.)/
                                   tagTrack->info(LHCb::Track::AdditionalInfo::FitTNDoF, 0) ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
        }
        else tuple->column( head+"_TRACK_TCHI2NDOF",-1.).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    }
    else
    {
      if (msgLevel(MSG::VERBOSE)) verbose() << "No NDOF" << endmsg ;
      tuple->column( head+"_TRACK_CHI2NDOF", -1 ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column( head+"_TRACK_PCHI2",-1.).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      if(isVerbose()) tuple->column( head+"_TRACK_VeloCHI2NDOF",-1.).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      if(isVerbose()) tuple->column( head+"_TRACK_TCHI2NDOF",-1.).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    }
    if ( isVerbose() )
    {
      // -- Run I+II
      int nVeloR      = 0;
      int nVeloPhi    = 0;
      int nVelo       = 0;
      int nVeloPileUp = 0;
      int nTT         = 0;
      int nIT         = 0;
      int nOT         = 0;

      //-- hopefully unique double constructed from multiplying all hit IDs
      double veloUTID = 1.;
      double ttUTID   = 1.;
      double itUTID   = 1.;
      double otUTID   = 1.;

      for ( LHCb::LHCbID id : tagTrack->lhcbIDs() ){
        if ( id.isVelo() ){
          veloUTID *= (double(id.veloID().channelID())/1000000.);
          ++nVelo;
          if( id.isVeloR() )      ++nVeloR;
          if( id.isVeloPhi() )    ++nVeloPhi;
          if( id.isVeloPileUp() ) ++nVeloPileUp;
        }else if( id.isTT() ){
          ttUTID *= (double(id.stID().channelID())/10000000.);
          ++nTT;
        }else if( id.isIT() ){
          itUTID *= (double(id.stID().channelID())/20000000.);
          ++nIT;
        }else if( id.isOT() ){
          otUTID *=  (double(id.otID().channelID())/50000000.);
          ++nOT;
        }
      }
      // -- unique ids
      tuple->column( head+"_TRACK_VELO_UTID", veloUTID ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column( head+"_TRACK_TT_UTID",   ttUTID   ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column( head+"_TRACK_IT_UTID",   itUTID   ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column( head+"_TRACK_OT_UTID",   otUTID   ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);

      // -- nHits
      tuple->column( head+"_TRACK_nVeloHits",       nVelo       ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column( head+"_TRACK_nVeloRHits",      nVeloR      ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column( head+"_TRACK_nVeloPhiHits",    nVeloPhi    ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column( head+"_TRACK_nVeloPileUpHits", nVeloPileUp ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column( head+"_TRACK_nTTHits",         nTT         ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column( head+"_TRACK_nITHits",         nIT         ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      tuple->column( head+"_TRACK_nOTHits",         nOT         ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);

      // -- history of the track (= which algorithm the track was made with)
      tuple->column( head+"_TRACK_History", tagTrack->history() ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);

    }

    tuple->column( head+"_TRACK_MatchCHI2",  tagTrack->info(LHCb::Track::AdditionalInfo::FitMatchChi2,-1) ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column( head+"_TRACK_GhostProb",  tagTrack->ghostProbability() ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column( head+"_TRACK_CloneDist",  tagTrack->info(LHCb::Track::AdditionalInfo::CloneDist,-1) ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    tuple->column( head+"_TRACK_Likelihood", tagTrack->likelihood() ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);

    //return StatusCode(test);

    return;
}
