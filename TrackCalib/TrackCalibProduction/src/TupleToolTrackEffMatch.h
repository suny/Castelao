/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TUPLETOOLTRACKEFF_H 
#define TUPLETOOLTRACKEFF_H 1

// Include files
// from Gaudi
#include "Kernel/IParticleTupleTool.h"
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Math/Boost.h"
#include "GaudiKernel/Vector4DTypes.h"
#include "Event/State.h"
#include "Event/Track.h"
#include "Event/CaloCluster.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackInterfaces/IHitExpectation.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
#include "Kernel/IParticle2MCAssociator.h"
#include "Kernel/LHCbID.h"


/** @class TupleToolTrackEffMatch TupleToolTrackEffMatch.h
 *  
 *  Fill tracking efficiency information: Check if probe track is matched to a long track
 *
 *  m_Turbo: use Turbo or stripping output? (needed since different location structure)
 *  m_matchedLocation1: location of muons in turbo data (default LowStat trigger line)
 *  m_matchedLocation2: location of muons in turbo data (default HighStat trigger line)
 *  m_matchedLocation: location of J/psi or muon in stripped data
 *  m_motherParticle: Use J/psi as input for matching in stripped data
 *  m_muonTrack: Use muon tracks as input for matching  in stripped data (if false, use particles)
 *  m_probeCharge; Charge of the probe; to check for wrong sign of the probe/tag at an early stage
 * 
 * if Verbose is true:
 *
 * - head_upgradedMass: Invariant mass of tag track and track associated to probe track
 * - head_nTTHits: Number of TT hits of MuonTT track.
 * - head_Matched_nTTHits: Number of TT hits on track matched to MuonTT track.
 * - head_Matched_Px: x component of momentum of track matched to MuonTT track.
 * - head_Matched_Py: y component of momentum of track matched to MuonTT track.
 * - head_Matched_Pz: z component of momentum of track matched to MuonTT track.
 * - head_Matched_P: absolut value of momentum of track matched to MuonTT track.
 * - head_Matched_dP: error on first track state of track matched to MuonTT track.
 * - head_Matched_probChi2: ProbChi2 of track matched to MuonTT track.
 * - head_Matched_Chi2NDoF: Chi2 / ndof of track matched to MuonTT track.
 * - head_DeltaR: Delta R between MuonTT track and track matched to MuonTT track. 
 * - head_DeltaP: Momentum difference between MuonTT track and track matched to MuonTT track. 
 * - head_wrongDeltaR: Delta R between MuonTT track and track closest to MuonTT track, not being the matched one. A track was matched in this event.  
 * - head_wrongDeltaP: Momentum difference between MuonTT track and track closest to MuonTT track, not being the matched one. A track was matched in this event.
 * - head_wrongP: Momentum of track closest to MuonTT track, not being the matched one. A track was matched in this event.
 * - head_wrongDeltaR2: Delta R between MuonTT track and track closest to MuonTT track. No track was matched in this event.  
 * - head_wrongDeltaP2: Momentum difference between MuonTT track and track closest to MuonTT track. No track was matched in this event. 
 * - head_wrongP2: Momentum of track closest to MuonTT track. No track was matched in this event.
 * - head_MatchedMC_Px: x component of momentum of MC particle mached to the track matched to MuonTT track.
 * - head_MatchedMC_Py: y component of momentum of MC particle mached to the track matched to MuonTT track.
 * - head_MatchedMC_Pz: z component of momentum of MC particle mached to the track matched to MuonTT track.
 * - head_MatchedMC_P: Absolute value of momentum of MC particle mached to the track matched to MuonTT track.
 * - head_MatchedMC_PID: PID of MC particle mached to the track matched to MuonTT track.
 *
 * \sa TupleToolTrackEffMatch, DecayTreeTuple, MCDecayTreeTuple
 * 
 *  @author Renata Kopecna
 *  @date   2018-06-28
 */

class TupleToolTrackEffMatch : public TupleToolBase, virtual public IParticleTupleTool {


public: 
  /// Standard constructor
  TupleToolTrackEffMatch( const std::string& type, 
                      const std::string& name,
                      const IInterface* parent);

  virtual ~TupleToolTrackEffMatch( ); ///< Destructor
  virtual StatusCode initialize() override;
  virtual StatusCode fill( const LHCb::Particle*
               , const LHCb::Particle*
               , const std::string&
               , Tuples::Tuple& ) override; 

protected:

private:
  
  const LHCb::MCParticle* assocMCPart( const LHCb::Track* assocTrack );
  void commonLHCbIDs(const LHCb::Track* longTrack, const LHCb::Track* probeTrack,  const LHCb::MuonPID* longMuPID,  double& muFraction, double& TTFraction, double& TFraction, double& ITFraction, double& OTFraction, double& VeloFraction, bool& hasTTHits);
  void commonMuonLHCbIDs(std::vector<LHCb::LHCbID>& longTrackIDs, std::vector<LHCb::LHCbID>& longTrackMuonIDs, const LHCb::Track* probeTrack, int&muonMatch);
  size_t nOverlap(const std::vector<LHCb::LHCbID> list1, const std::vector<LHCb::LHCbID> list2, bool verb);  
  void fillTagTrackInfo(const LHCb::Track* tagTrack, const std::string &head, Tuples::Tuple& tuple);

  std::string m_matchedLocation1;
  std::string m_matchedLocation2;
  std::string m_matchedLocation;
  bool m_MC;  
  bool m_Turbo;
  std::string m_method; 
  double m_minVeloFracMatch;
  double m_minMuonFracMatch;
  double m_minTTFracMatch;
  double m_minITFracMatch;
  double m_minOTFracMatch;

  //bool m_motherParticle;
  //bool m_muonTrack;
  //std::string m_probeCharge;

  IParticle2MCAssociator* m_p2mcAssoc;

  //ILHCbIDsToMCParticles* m_lhcbid2mcparticles;
  
  

};
#endif // TUPLETOOLTRACKEFF_H

