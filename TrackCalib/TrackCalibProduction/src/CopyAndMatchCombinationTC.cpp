/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CopyAndMatchCombinationTC.h"


DECLARE_COMPONENT( CopyAndMatchCombinationTC )
//
//==============================================================================
// Constructor
//==============================================================================
CopyAndMatchCombinationTC::CopyAndMatchCombinationTC ( 
    const std::string& name,  
    ISvcLocator*       pSvc)
  :  base_class           ( name , pSvc )
  ,  m_motherName         (      ""     )
  ,  m_motherId           (      0      )
  ,  m_charConj           (    true     )
  ,  m_inputParticles     (             )
  ,  m_useInputParticles  (    false    )
  ,  m_matcher            (    NULL     )
  ,  m_matching_locations (             )
  ,  m_parsed_locations   (             )
{
  declareProperty ( "MotherName", m_motherName,
          "Particle name of the top of the decay chain.");

  declareProperty ( "AllowChargeConjugation", m_charConj,
          "If true, does not distinguish between particle and anti-particle");

  declareProperty("TeslaMatcher", m_teslaMatcherName = "TeslaMatcher:PUBLIC",
                  "Tool name for ITeslaMatcher interface" );

  declareProperty ( "MatchLocations" , m_matching_locations );                   

}

//==============================================================================
// Initialize
//==============================================================================
StatusCode CopyAndMatchCombinationTC::initialize ()
{
  StatusCode sc = DaVinciAlgorithm::initialize () ;
  if (sc == StatusCode::FAILURE) 
    return sc;

  LHCb::IParticlePropertySvc*  ppSvc = 
    this -> template svc<LHCb::IParticlePropertySvc> 
                            ( "LHCb::ParticlePropertySvc" , true) ;

  const LHCb::ParticleProperty* property = ppSvc->find(m_motherName);
  if (property)
    m_motherId = property->pdgID().pid(); 

  m_matcher = tool<ITeslaMatcher>(m_teslaMatcherName, this);
  if (m_matcher == NULL)
    return Error("Cannot load TeslaMatcher tool", StatusCode::FAILURE);

  initTESlocations().ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);

  return StatusCode::SUCCESS;
}


//==============================================================================
// Exectue
//==============================================================================
StatusCode CopyAndMatchCombinationTC::execute ()
{
  const LHCb::Particle::Range pRange = i_particles();
  for (const LHCb::Particle *p : pRange )
  {
    if (p == NULL) continue;

    LHCb::Particle *part = new LHCb::Particle(*p);  // copy the particle
    StatusCode sc = matchDaughters ( part );
    if (sc) 
      this->markParticle(part);
    else
      return Warning ( "Match failed" , StatusCode::SUCCESS );
  }

  setFilterPassed( true ); // Mandatory. Set to true if event is accepted
  return StatusCode::SUCCESS;
}


//==============================================================================
// Finalize
//==============================================================================
StatusCode CopyAndMatchCombinationTC::finalize ()
{
  StatusCode sc = DaVinciAlgorithm::finalize () ;
  if (sc == StatusCode::FAILURE) 
    return sc;

  return StatusCode::SUCCESS;
}

//==============================================================================
// match daughters if basic, or recall 
//==============================================================================
StatusCode CopyAndMatchCombinationTC::matchDaughters(LHCb::Particle* p)
{
  // copy the list of references to daughters
  const SmartRefVector <LHCb::Particle> daughters ( p->daughters() );

  // loop over the copied list and remove particles from the original one
  for (auto daughter : daughters)
  {
    if (daughter == NULL) 
      return StatusCode::FAILURE;

    p->removeFromDaughters ( daughter );

    const LHCb::Particle* matched; 
    if (daughter->isBasicParticle()) //if basic, match
    {
      const int apid = daughter->particleID().abspid(); 
      StatusCode sc = m_matcher->findBestMatch( daughter,
                                                matched,
                                                m_parsed_locations[apid]);
      if (! (sc && matched) )
        return Warning ( "Match failure. TES: " + m_parsed_locations[apid],
                         StatusCode::FAILURE);


      LHCb::Particle* m = new LHCb::Particle ( *matched );
      m->setWeight ( p->weight() );
      this->markParticle ( m );
      p->addToDaughters  ( m );

    }
    else // if not basic, copy and match daughters
    {
      LHCb::Particle* m = new LHCb::Particle( *daughter );
      matchDaughters     ( m ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
      this->markParticle ( m );
      p->addToDaughters  ( m );
    }

  }

  return StatusCode::SUCCESS;

}


//==============================================================================
// Event listener
//==============================================================================
void CopyAndMatchCombinationTC::handle(const Incident&)
{
}

//==============================================================================
// Set Input
//==============================================================================
StatusCode CopyAndMatchCombinationTC::setInput ( 
    const LHCb::Particle::ConstVector& input )
{
  m_inputParticles    = input ;
  m_useInputParticles = true  ;
  info() << "The external set of " << input.size()
    << " particles will be used as input instead of InputLocations "
    << endmsg ;
  return StatusCode ( StatusCode::SUCCESS , true ) ;
}

//==============================================================================
// Set Input
//==============================================================================
StatusCode CopyAndMatchCombinationTC::initTESlocations ()
{
#define SET_TES(part,tes) \
  if (m_matching_locations.count(part) == 0)\
    m_matching_locations [part] = tes

  SET_TES("pi+", "Phys/StdAllNoPIDsPions/Particles");
  SET_TES("K+",  "Phys/StdAllNoPIDsKaons/Particles");
  SET_TES("p+",  "Phys/StdAllNoPIDsProtons/Particles");
  SET_TES("e+",  "Phys/StdAllNoPIDsElectrons/Particles");
  SET_TES("mu+", "Phys/StdAllNoPIDsMuons/Particles");
#undef SET_TES

  // Gets the ParticleProperty service
  LHCb::IParticlePropertySvc*  ppSvc = 
    this -> template svc<LHCb::IParticlePropertySvc> 
                            ( "LHCb::ParticlePropertySvc" , true) ;

  std::map<std::string, std::string>::const_iterator matchLocation;

  for (matchLocation = m_matching_locations.begin();
       matchLocation != m_matching_locations.end();
       matchLocation++)
  {
    const LHCb::ParticleProperty* property = ppSvc->find(matchLocation->first);
    if (property)
      m_parsed_locations [abs(property->pdgID().pid())] = matchLocation->second;
  }

  return StatusCode::SUCCESS;
}


