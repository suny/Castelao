###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: TrackingScripts
################################################################################
gaudi_subdir(TrackingScripts v1r0)

gaudi_depends_on_subdirs(Phys/DaVinciMCKernel                          
                         Phys/DecayTreeTupleBase                          
                         Phys/LoKiPhysMC
                         Tr/TrackFitEvent)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${ROOT_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS})

gaudi_add_module(TrackingScripts
                 src/*.cpp
                 INCLUDE_DIRS ROOT Phys/LoKi
                 LINK_LIBRARIES DaVinciMCKernelLib DecayTreeTupleBaseLib LoKiPhysMCLib TrackFitEvent)
