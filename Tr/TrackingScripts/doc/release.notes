!-----------------------------------------------------------------------------
! Package     : TrackingScripts
! Responsible : Michel De Cian
! Purpose     : Scripts for all tracking related things
!-----------------------------------------------------------------------------

! 2017-01-20 - Carlos Vazquez Sierra
  - Integrating Tr/TrackingScripts in Erasmus

! 2016-11-09 - Paul Seyfert
  - Added TupleTool for GhostClassification

! 2014-11-14 - Michel De Cian
 - Put TrackMatchChecker in 'Tr/TrackingScripts/TrackMatching' to not lose the code, as 'Tf/TrackMatching' is removed and the checker is removed from Tr/TrackCheckers. 

! 2014-10-30 - Michel De Cian
 - Add ghost probability tools that were used for the upgrade as they might be useful for future studies.

! 2014-01-16 - Paul Seyfert
 - Added scripts for benchmarking TrackEventCloneKiller vs.
   TrackBestTrackCreator in 2012. And tracking performance on MC for 2015.

! 2013-05-03 - Michel De Cian
 - Updated prepareTrackEfffData.C and trackEffFit.C

! 2013-03-22 - Paul Seyfert
 - Added VeloMuonTrackAnalyzer, consider these as work in progress. They can
   and are used, but documentation and features might not be fully documented
   or implemented.

! 2013-01-23 - Michel De Cian
 - Added prepareTrackEffData.C, trackEffFit.C and readme.txt, which are scripts used to calculate the tracking 
efficiency with the long method on data and MC.

! 2013-01-21 - Michel De Cian
 - Added TupleToolTrackEff.h/cpp, TupleToolTrackEff_readme.txt, TrackEffJpsiMC.py to the TrackingEfficiency folder. These are scripts to run the Long Method (MuonTT tracks) and put the information in a nTuple (from the script or the stripping line).

! 2013-01-09 - Paul Seyfert
 - script to write ntuples to study impact of GhostProbability on B decay
   analyses

! 2012-12-10 - Paul Seyfert
 - added wrapper for standalone ghost probability computer

! 2012-09-21 - Paul Seyfert
 - TMVA training script for ghost classifiers as developed for Reco13e

! 2012-09-01 - Paul Seyfert
 - adding directories for GhostProbability developement.
 - at the moment only scripts for ntuple production, which relies heavily on 
   the Phys/Tau23Mu package

!============================== v1r0 19-04-2012 ==============================
- Creation of package
