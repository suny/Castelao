/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"

#include "Event/Particle.h"
#include "Event/Track.h"
#include "Event/GhostTrackInfo.h"

// local
#include "TupleToolGhostTrackInfo.h"

//-----------------------------------------------------------------------------
// Implementation file for class : TupleToolGhostTrackInfo
//
// 2016-09-12 : Paul Seyfert
//-----------------------------------------------------------------------------

DECLARE_COMPONENT( TupleToolGhostTrackInfo )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
  TupleToolGhostTrackInfo::TupleToolGhostTrackInfo( const std::string& type,
                                          const std::string& name,
                                          const IInterface* parent )
    : TupleToolBase ( type, name , parent )
{
  declareInterface<IParticleTupleTool>(this);
}

//=============================================================================
StatusCode TupleToolGhostTrackInfo::initialize()
{
  if( !TupleToolBase::initialize() ) return StatusCode::FAILURE;

  m_ghostClassification = tool<ITrackGhostClassification>("AllTrackGhostClassification");

  return StatusCode::SUCCESS;
}

//=============================================================================
StatusCode TupleToolGhostTrackInfo::fill( const LHCb::Particle*
                                     , const LHCb::Particle* P
                                     , const std::string& head
                                     , Tuples::Tuple& tuple )
{
  const std::string prefix = fullName(head);

  bool test = true;
  if ( !P ) return StatusCode::FAILURE;

  //first just return if the particle isn't supposed to have a track
  if ( !P->isBasicParticle() ) {
    test &= tuple->column( prefix+"_ghostCat", -996 );
    return StatusCode::SUCCESS;
  }

  const LHCb::ProtoParticle* protop = P->proto();
  if (!protop) {
    test &= tuple->column( prefix+"_ghostCat", -997 );
    return StatusCode::SUCCESS;
  }

  const LHCb::Track* track = protop->track();
  if (!track) {
    test &= tuple->column( prefix+"_ghostCat", -998 );
    return StatusCode::SUCCESS;
  }

  LHCb::GhostTrackInfo gInfo;
  if (m_ghostClassification->info(*track,gInfo).isSuccess()) {
    test &= tuple->column( prefix+"_ghostCat",gInfo.classification() );
  } else {
    test &= tuple->column( prefix+"_ghostCat", -999 );
  }

  return StatusCode(test);
}
