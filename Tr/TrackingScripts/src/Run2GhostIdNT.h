/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef Run2GhostIdNT_H
#define Run2GhostIdNT_H 1

// Include files
#include "Linker/LinkerTool.h"

#include "TrackInterfaces/IHitExpectation.h"
//#include "TrackInterfaces/IVPExpectation.h"
#include "OTDAQ/IOTRawBankDecoder.h"
#include "TrackInterfaces/IGhostProbability.h"
#include "GaudiAlg/GaudiTupleTool.h"
#include "GaudiKernel/IIncidentListener.h"
#include "MCInterfaces/ITrackGhostClassification.h"



class IClassifierReader;


/** @class Run2GhostIdNT Run2GhostIdNT.h
 *
 *  @author Paul Seyfert
 *  @date   06-02-2015
 */
class Run2GhostIdNT : public GaudiTupleTool,
                       virtual public IGhostProbability {


public:
  /// Standard constructor
  Run2GhostIdNT( const std::string& type,
                  const std::string& name,
                  const IInterface* parent);



  virtual ~Run2GhostIdNT(){}; ///< Destructor

  virtual StatusCode finalize() override; ///< Algorithm initialization
  virtual StatusCode initialize() override; ///< Algorithm initialization
  virtual StatusCode execute(LHCb::Track& aTrack) const override; 
  virtual StatusCode beginEvent() override {return m_ghostTool->beginEvent();};
  virtual std::vector<std::string> variableNames(LHCb::Track::Types type) const override {return m_ghostTool->variableNames(type);};
  virtual std::vector<float> netInputs(LHCb::Track& aTrack) const override {return m_ghostTool->netInputs(aTrack);};


protected:

private:
  ITrackGhostClassification* m_classification;
  IGhostProbability* m_ghostTool;

};

#endif // Run2GhostIdNT_H
