/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TUPLETOOLGhostInfo_H
#define TUPLETOOLGhostInfo_H 1

// Include files
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Kernel/IParticleTupleTool.h"
#include "TrackInterfaces/IHitExpectation.h"
#include "TrackInterfaces/IVPExpectation.h"
#include "TrackInterfaces/ITrackFitter.h"

class IClassifierReader;
class FlattenLong;

/** @class TupleToolGhostInfo TupleToolGhostInfo.h jborel/TupleToolGhostInfo.h
 *
 *  @author Angelo Di Canto
 *  @date   2013-12-15
 */
class TupleToolGhostInfo : public TupleToolBase, virtual public IParticleTupleTool {
public:
  /// Standard constructor
  TupleToolGhostInfo( const std::string& type,
		    const std::string& name,
		    const IInterface* parent);

  virtual ~TupleToolGhostInfo(){}; ///< Destructor

  virtual StatusCode initialize(); ///< Algorithm initialization

  virtual StatusCode fill( const LHCb::Particle*
			   , const LHCb::Particle*
			   , const std::string&
			   , Tuples::Tuple& );

protected:
  void initNamesLong();

private:
  ITrackFitter    *m_trackFitter;
  IVPExpectation  *m_vpExpectation;
  IHitExpectation *m_utExpectation;
  IHitExpectation *m_ftExpectation;

  std::vector<std::string> m_inNames;
  std::vector<double>* m_inputVec;
  IClassifierReader* m_readerLong;
  FlattenLong* m_FlattenTableLong;
};

#endif // TUPLETOOLGhostInfo_H
