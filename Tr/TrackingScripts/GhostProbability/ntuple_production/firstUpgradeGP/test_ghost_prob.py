###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *

from STTools import STOfflineConf
STOfflineConf.DefaultConf().configureTools()

from Configurables import CombineParticles
D2pipi = CombineParticles("D2pipi")
D2pipi.DecayDescriptor = "D0 -> pi+ pi-" 
D2pipi.MotherCut = "(VFASPF(VCHI2/VDOF)<10)"

from PhysSelPython.Wrappers import Selection, SelectionSequence
from StandardParticles import StdLoosePions
selD2pipi = Selection("selD2pipi", Algorithm = D2pipi , RequiredSelections = [StdLoosePions])
seq = SelectionSequence("seq", TopSelection = selD2pipi)

from Configurables import DecayTreeTuple, TupleToolGhostInfo, TrackMasterFitter
alg = DecayTreeTuple("TestGhostNtuple")
alg.Decay = "D0 -> ^pi+ ^pi-"
alg.TupleName = "Tuple"
alg.Inputs = [ seq.outputLocation() ]
alg.ToolList = [ "TupleToolMCTruth", "TupleToolKinematic", "TupleToolGhostInfo" ]
alg.addTool(TupleToolGhostInfo("TupleToolGhostInfo"))
alg.TupleToolGhostInfo.addTool(TrackMasterFitter("TrackMasterFitter"))
alg.TupleToolGhostInfo.TrackMasterFitter.MeasProvider.IgnoreVelo = True
alg.TupleToolGhostInfo.TrackMasterFitter.MeasProvider.IgnoreTT = True
alg.TupleToolGhostInfo.TrackMasterFitter.MeasProvider.IgnoreOT = True
alg.TupleToolGhostInfo.TrackMasterFitter.MeasProvider.IgnoreIT = True
alg.TupleToolGhostInfo.TrackMasterFitter.MeasProvider.IgnoreVP = False
alg.TupleToolGhostInfo.TrackMasterFitter.MeasProvider.IgnoreUT = False
alg.TupleToolGhostInfo.TrackMasterFitter.MeasProvider.IgnoreFT = False

from Configurables import DaVinci
DaVinci().appendToMainSequence( [ seq.sequence() ] )
DaVinci().UserAlgorithms = [ alg ]

DaVinci().Simulation = True
DaVinci().TupleFile = "TestGhostNtuple.root"
DaVinci().EvtMax = -1
DaVinci().Lumi = False
