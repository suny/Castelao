#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

print "sorry I don't want to find out how to use code from a different package in code I place here."
print "It boils down to: "
print "  SetupErasmus"
print "a) first time:"
print "  getpack Phys/Tau23Mu head"
print "  cd Phys/Tau23Mu"
print "b) from then on:  "
print "  cd $TAU23MUROOT"
print "  svn up"
print ""
print "c) in common:"
print "  source cmt/setup.sh"
print "  cd ZooThings"
print "  source init.sh"
print "  make JohannImpl"
print "  ./JohannImpl <zoontuple(s)>"
print "  mv output.root <desired name for ntuple>"
print ""
print "sorry, JohannImpl at the moment cannot change the name of the output file. This is a bug I didn't have the time to fix yet."
