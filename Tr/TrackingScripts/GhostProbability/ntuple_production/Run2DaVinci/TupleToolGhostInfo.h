/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TUPLETOOLGhostInfo_H
#define TUPLETOOLGhostInfo_H 1

// Include files
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Kernel/IParticleTupleTool.h"
#include "TrackInterfaces/IHitExpectation.h"
#include "TrackInterfaces/IVeloExpectation.h"
#include "TrackInterfaces/IVPExpectation.h"
#include "TrackInterfaces/ITrackFitter.h"
#include "GaudiKernel/IIncidentListener.h"
#include "OTDAQ/IOTRawBankDecoder.h"
#include "TrackInterfaces/IGhostProbability.h"
#include "MCInterfaces/ITrackGhostClassification.h"



/** @class TupleToolGhostInfo TupleToolGhostInfo.h jborel/TupleToolGhostInfo.h
 *
 *  @author Angelo Di Canto
 *  @date   2013-12-15
 */
class TupleToolGhostInfo : public TupleToolBase,
                           virtual public IParticleTupleTool,
                           virtual public IIncidentListener {
public:
  /// Standard constructor
  TupleToolGhostInfo( const std::string& type,
		    const std::string& name,
		    const IInterface* parent);

  virtual ~TupleToolGhostInfo(){}; ///< Destructor

  virtual StatusCode initialize(); ///< Algorithm initialization

  virtual StatusCode fill( const LHCb::Particle*
			   , const LHCb::Particle*
			   , const std::string&
			   , Tuples::Tuple& );
  
  virtual void handle( const Incident& incident);

protected:
  StatusCode countHits();

private:
  bool              m_needrefresh;
  ITrackFitter     *m_trackFitter;
  IVPExpectation   *m_vpExpectation;
  IVeloExpectation *m_veloExpectation;
  IOTRawBankDecoder* m_otdecoder;


  std::vector<IHitExpectation*> m_Expectations;
  IGhostProbability* m_ghostTool;
  IGhostProbability* m_ghostToolNew;
  ITrackGhostClassification*    m_ghostClassification;


  std::string m_oldtool;
  std::string m_newtool;
  std::vector<std::string> m_inNames;
  std::vector<std::string> m_expectorNames;
  std::vector<int>* m_expectedHits;


  int m_veloHits;
  int m_ttHits;
  int m_utHits;
  int m_ftHits;
  int m_itHits;
  int m_otHits;
};

#endif // TUPLETOOLGhostInfo_H
