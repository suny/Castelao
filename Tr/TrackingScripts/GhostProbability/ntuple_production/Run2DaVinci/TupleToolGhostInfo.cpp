/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "GaudiKernel/IIncidentSvc.h"

#include "Event/RecSummary.h"


#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"

#include "Kernel/HitPattern.h"

//#include "Event/FTRawCluster.h"
//#include "Event/FTCluster.h"
#include "Event/Particle.h"
#include "Event/STCluster.h"
#include "Event/Track.h"
#include "Event/KalmanFitResult.h"
#include "Event/VPCluster.h"
#include "Event/VeloCluster.h"
#include "Event/GhostTrackInfo.h" 


#include "TMath.h"

// local
#include "TupleToolGhostInfo.h"

//-----------------------------------------------------------------------------
// Implementation file for class : TupleToolGhostInfo
//
// 2013-12-15 : Angelo Di Canto
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
// actually acts as a using namespace TupleTool
DECLARE_COMPONENT( TupleToolGhostInfo )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
  TupleToolGhostInfo::TupleToolGhostInfo( const std::string& type,
                                          const std::string& name,
                                          const IInterface* parent )
    : TupleToolBase ( type, name , parent ),
      m_needrefresh(true),
      m_otdecoder(0)
{
  declareInterface<IParticleTupleTool>(this);
  //declareProperty( "Expectors" , m_expectorNames=std::vector<std::string>{"UTHitExpectation","FTHitExpectation"} );
  //declareProperty( "Expectors" , m_expectorNames=std::vector<std::string>({"ITHitExpectation","TTHitExpectation","OTHitExpectation"}) );
  declareProperty( "OldTool", m_oldtool = "TrackNNGhostId");
  
}

//=============================================================================
StatusCode TupleToolGhostInfo::initialize()
{
  if( !TupleToolBase::initialize() ) return StatusCode::FAILURE;
  incSvc()->addListener(this, IncidentType::BeginEvent);



  if (LHCb::LHCbID::Velo >= 15) return Error("HARD CODED NUMBER IS WRONG!",StatusCode::FAILURE, 1);
  if (LHCb::LHCbID::IT >= 15) return Error("HARD CODED NUMBER IS WRONG!",StatusCode::FAILURE, 1);
  if (LHCb::LHCbID::OT >= 15) return Error("HARD CODED NUMBER IS WRONG!",StatusCode::FAILURE, 1);
  if (LHCb::LHCbID::TT >= 15) return Error("HARD CODED NUMBER IS WRONG!",StatusCode::FAILURE, 1);
  if (LHCb::LHCbID::UT >= 15) return Error("HARD CODED NUMBER IS WRONG!",StatusCode::FAILURE, 1);
  if (LHCb::LHCbID::FT >= 15) return Error("HARD CODED NUMBER IS WRONG!",StatusCode::FAILURE, 1);
  if (LHCb::LHCbID::VP >= 15) return Error("HARD CODED NUMBER IS WRONG!",StatusCode::FAILURE, 1);


  m_ghostTool = tool<IGhostProbability>(m_oldtool,this);
  m_ghostClassification = tool<ITrackGhostClassification>("AllTrackGhostClassification");


  // track fitter
  m_trackFitter = tool<ITrackFitter>("TrackInitFit",this);

//  // expect hit patterns
//  if (false) {
//    m_vpExpectation = tool<IVPExpectation>("VPExpectation");
//  } else {
//    m_vpExpectation = NULL;
//  }
//  m_veloExpectation = tool<IVeloExpectation>("VeloExpectation");
//  for (auto entry : m_expectorNames) {
//    m_Expectations.push_back(tool<IHitExpectation>(entry));
//  }
//  m_expectedHits = new std::vector<int>(m_expectorNames.size(),0);
//  //m_otdecoder = tool<IOTRawBankDecoder>("OTRawBankDecoder");


  return StatusCode::SUCCESS;
}

void TupleToolGhostInfo::handle (const Incident& incident) {
  if (IncidentType::BeginEvent == incident.type() ) {
    m_needrefresh = true;
  }
}

StatusCode TupleToolGhostInfo::countHits() {
  const LHCb::RecSummary * rS =
    getIfExists<LHCb::RecSummary>(evtSvc(),LHCb::RecSummaryLocation::Default);
  if ( !rS )
  {
    rS = getIfExists<LHCb::RecSummary>(evtSvc(),LHCb::RecSummaryLocation::Default,false);
  }
  if ( !rS )
  {
    rS = getIfExists<LHCb::RecSummary>(evtSvc(),"/Event/Turbo/Rec/Summary",false);
  }
  if ( !rS )
  {
    return StatusCode::FAILURE;
  }

  m_otHits = rS->info(LHCb::RecSummary::nOTClusters,-1);
  m_veloHits =  rS->info(LHCb::RecSummary::nVeloClusters,-1);
  m_itHits = rS->info(LHCb::RecSummary::nITClusters,-1);
  m_ttHits = rS->info(LHCb::RecSummary::nTTClusters,-1);
  
  return StatusCode::SUCCESS;

  m_needrefresh = false;

}

namespace {
  inline float* subdetectorhits(const LHCb::Track *aTrack) {
    static float returnvalue[15] ; /// FIXME hard code is very bad
    returnvalue[LHCb::LHCbID::Velo] = 0.f;
    returnvalue[LHCb::LHCbID::TT] = 0.f;
    returnvalue[LHCb::LHCbID::IT] = 0.f;
    returnvalue[LHCb::LHCbID::OT] = 0.f;
    returnvalue[LHCb::LHCbID::UT] = 0.f;
    returnvalue[LHCb::LHCbID::FT] = 0.f;
    returnvalue[LHCb::LHCbID::VP] = 0.f;
    for (std::vector<LHCb::LHCbID>::const_iterator idIter = aTrack->lhcbIDs().begin() ; aTrack->lhcbIDs().end() != idIter ; ++idIter) {
      (returnvalue[idIter->detectorType()]) += 1.f;
    }
    return returnvalue;
  }
}


//=============================================================================
StatusCode TupleToolGhostInfo::fill( const LHCb::Particle*
                                     , const LHCb::Particle* P
                                     , const std::string& head
                                     , Tuples::Tuple& tuple )
{
  const std::string prefix = fullName(head);
  
  verbose() << " ====================== new track =========================" << endmsg;

  bool test = StatusCode::SUCCESS;
  if ( !P ) return StatusCode::FAILURE;

  //first just return if the particle isn't supposed to have a track
  if ( !P->isBasicParticle() ) return StatusCode::SUCCESS;

  const LHCb::ProtoParticle* protop = P->proto();
  if (!protop) return StatusCode::SUCCESS;

  const LHCb::Track* track = protop->track();
  if (!track) return StatusCode::SUCCESS;

  if (m_needrefresh) {
    m_ghostTool->beginEvent();
    test &= countHits();
    if (!track) return StatusCode::SUCCESS;
  }

  float* obsarray = subdetectorhits(track);

  test &= tuple->column( prefix+"_obsVELO",obsarray[LHCb::LHCbID::Velo] );
  test &= tuple->column( prefix+"_obsVP",obsarray[LHCb::LHCbID::VP] );
  test &= tuple->column( prefix+"_obsIT",obsarray[LHCb::LHCbID::IT] );
  test &= tuple->column( prefix+"_obsOT",obsarray[LHCb::LHCbID::OT] );
  test &= tuple->column( prefix+"_obsTT",obsarray[LHCb::LHCbID::TT] );
  test &= tuple->column( prefix+"_obsFT",obsarray[LHCb::LHCbID::FT] );
  test &= tuple->column( prefix+"_obsUT",obsarray[LHCb::LHCbID::UT] );


//  std::vector<int> expected;
//  expected.reserve(m_Expectations.size());
//  for (auto entry : m_Expectations) {
//    expected.push_back(entry->nExpected(*track));
//  }
//  test &= tuple->column( prefix+"_expVP",(m_vpExpectation ? m_vpExpectation->nExpected(*track) : 0));
//  test &= tuple->column( prefix+"_expVelo",m_veloExpectation->nExpected(*track));
//  for (unsigned i = 0 ; i < m_expectorNames.size() ; ++i) {
//    test &= tuple->column( prefix+"_exp"+m_expectorNames[i],expected[i]);
//  }

  test &= tuple->column( prefix+"_veloHits",m_veloHits);
  test &= tuple->column( prefix+"_ttHits"  ,m_ttHits  );
  test &= tuple->column( prefix+"_utHits"  ,m_utHits  );
  test &= tuple->column( prefix+"_ftHits"  ,m_ftHits  );
  test &= tuple->column( prefix+"_itHits"  ,m_itHits  );
  test &= tuple->column( prefix+"_otHits"  ,m_otHits  );
  LHCb::GhostTrackInfo gInfo;
  //if (m_ghostClassification->info(*track,gInfo).isSuccess()) {
  //  test &= tuple->column( prefix+"_ghostCat",gInfo.classification() );
  //} else {
  //  test &= tuple->column( prefix+"_ghostCat", -999 );
  //}



  //  FIXME
  LHCb::Track *refitted = track->clone();
  LHCb::KalmanFitResult* fit = static_cast<LHCb::KalmanFitResult*>( refitted->fitResult() );
  if (!fit) {
    m_trackFitter->fit(*refitted).ignore();
    fit = static_cast<LHCb::KalmanFitResult*>( refitted->fitResult() );
  }
  test &= tuple->column( prefix+"_originalGhostProb", track->ghostProbability() );

  /////FIXME
  m_ghostTool->execute(*refitted);
  std::vector<float> recomp_inputs = m_ghostTool->netInputs( *refitted);
  std::vector<std::string> recomp_vars = m_ghostTool->variableNames(LHCb::Track::Long);
  if (recomp_inputs.size()<recomp_vars.size() && track->checkType(LHCb::Track::Long)) always() << "NANANANANANANA" << endmsg;
  for (unsigned k = 0 ; k < recomp_vars.size() ; ++k) {
    if (track->checkType(LHCb::Track::Long)) {
      test &= tuple->column( prefix+"_toolvar_"+std::to_string(k)+"_"+recomp_vars[k], recomp_inputs[k]);
    } else {
      test &= tuple->column( prefix+"_toolvar_"+std::to_string(k)+"_"+recomp_vars[k], -9.f);
    }
  }
  test &= tuple->column( prefix+"_recomputedGhostProb", refitted->ghostProbability() );
  /////FIXME
  //////if (refitted->ghostProbability() > 100) {
  //////  always() << "something wrong here" << endmsg;
  //////  always() << *fit << endmsg;
  //////}
  test &= tuple->column( prefix+"_NewGhostProb", refitted->ghostProbability() );
  
  float nOTBad = ((int) (fit->nMeasurements(LHCb::Measurement::OT) - track->info(LHCb::Track::FitFracUsedOTTimes , 0) * fit->nMeasurements(LHCb::Measurement::OT)));
  int nUTOutliers = ((int) (fit->nMeasurements(LHCb::Measurement::UT) - fit->nActiveMeasurements(LHCb::Measurement::UT)));
  int nFTOutliers = ((int) (fit->nMeasurements(LHCb::Measurement::FT) - fit->nActiveMeasurements(LHCb::Measurement::FT)));
  int nTTOutliers = ((int) (fit->nMeasurements(LHCb::Measurement::TT) - fit->nActiveMeasurements(LHCb::Measurement::TT)));
  int nITOutliers = ((int) (fit->nMeasurements(LHCb::Measurement::IT) - fit->nActiveMeasurements(LHCb::Measurement::IT)));
  test &= tuple->column( prefix+"_IToutlier",nITOutliers );
  test &= tuple->column( prefix+"_UToutlier",nUTOutliers );
  test &= tuple->column( prefix+"_TToutlier",nTTOutliers );
  test &= tuple->column( prefix+"_FToutlier",nFTOutliers );
  test &= tuple->column( prefix+"_OTunused",nOTBad );

  //  if (!fit) 
//  if (!fit) {
////    test &= tuple->column( prefix+"_GhostbusterMLP", -999. );
////    test &= tuple->column( prefix+"_GhostProb", -999. );
////    return StatusCode(test);
//  }
  delete refitted;

  return StatusCode(test);
}
