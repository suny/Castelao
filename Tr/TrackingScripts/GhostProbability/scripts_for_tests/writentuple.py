###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *

from STTools import STOfflineConf
STOfflineConf.DefaultConf().configureTools()
from Configurables import L0Conf
L0Conf().EnsureKnownTCK=False
from Configurables import TrackMasterFitter, TupleToolProtoPData, TupleToolTrackInfo
from Configurables import Run2GhostId, TrackNNGhostId, DaVinci
from DecayTreeTuple.Configuration import *

DaVinci().InputType = "DST"

alg = DecayTreeTuple("TestGhostNtuple")

#alg.Decay = "Z0 -> ^mu- ^mu+"
#alg.Inputs = ['ALL/Phys/Z02MuMuLine/Particles']
#alg.Decay = "[^(D0 -> ^K- ^pi+)]CC"
#alg.Inputs = ['Phys/StdLooseD02KPi/Particles']

alg.Decay = "^(KS0 -> ^pi- ^pi+)"
alg.Inputs = ['Phys/StdLooseKsLL/Particles','Phys/StdLooseKsDD/Particles']

alg.TupleName = "Tuple"
alg.ToolList = []

#alg.addBranches({
##   "K" : "[(D0 -> ^K- pi+)]CC",
#   "pi" : "[(D0 -> K- ^pi+)]CC",
#   "D" : "[^(D0 -> K- pi+)]CC"}
#)

alg.addTupleTool("TupleToolRecoStats")


alg.addTupleTool("TupleToolKinematic")
from Configurables import FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence

desktop = FilterDesktop("vertices")
desktop.Code = "HASVERTEX"
from PhysSelPython.Wrappers import DataOnDemand
MyStream = "Dimuon"
#mydod = DataOnDemand(Location = "Turbo/Hlt2CharmHadD02KPi_XSecTurbo/Particles")
#mydod = DataOnDemand(Location = "Phys/D02HHForXSecD02HHLine/Particles")
#mydod = DataOnDemand(Location = "Turbo/Hlt2CharmHadD02KPi_XSecTurbo/Particles")
from CommonParticles.StdLooseD02HH import StdLooseD02KPi
#sel = Selection("filtervertices", Algorithm = desktop, RequiredSelections = [mydod])
#selseq = SelectionSequence("filterverticesseq", TopSelection = sel)
#alg.Inputs = ['Turbo/Hlt2CharmHadD02KPi_XSecTurbo/Particles']#selseq.outputLocation()]






from Configurables import TupleToolGhostInfo
oldghostinfo = alg.addTupleTool('TupleToolGhostInfo/oldghostinfo')
oldghostinfo.OldTool="Run2GhostId"
oldghostinfo.ExtraName = "OldImplementation"
oldghostinfo.addTool(Run2GhostId("Run2GhostId"),name="Run2GhostId")
#oldghostinfo.addTool(TrackNNGhostId("TrackNNGhostId"),name="TrackNNGhostId")

from Configurables import TrackInitFit, TrackMasterFitter
from TrackFitter.ConfiguredFitters import ConfiguredMasterFitter
from Configurables import TrackBestTrackCreator, TrackMasterFitter, TrackStateInitTool, FastVeloFitLHCbIDs

def giveitafit(thething):

   thething.addTool(TrackInitFit,"TrackInitFit")
   thething.TrackInitFit.addTool( TrackMasterFitter, name = "Fit" )
   thething.TrackInitFit.addTool( TrackStateInitTool, name = "Init")
   thething.TrackInitFit.Init.UseFastMomentumEstimate = True
   ConfiguredMasterFitter( getattr(thething.TrackInitFit, "Fit"), SimplifiedGeometry = True, LiteClusters = True, MSRossiAndGreisen = True )
   #ConfiguredMasterFitter( getattr(thething.TrackInitFit, "Fit"), SimplifiedGeometry = True, LiteClusters = False, MSRossiAndGreisen = True )
   thething.TrackInitFit.Init.VeloFitterName = "FastVeloFitLHCbIDs"
   thething.TrackInitFit.Init.addTool(FastVeloFitLHCbIDs, name = "FastVeloFitLHCbIDs")
   if DaVinci().InputType == "MDST" or True:
      thething.TrackInitFit.RootInTES = MyStream
      thething.TrackInitFit.Fit.RootInTES = MyStream
      thething.TrackInitFit.Init.RootInTES = MyStream
      thething.TrackInitFit.Init.FastVeloFitLHCbIDs.RootInTES = MyStream


giveitafit(oldghostinfo)
giveitafit(refitter)


##from Configurables import RefitParticleTracks
##refitter = RefitParticleTracks()
##refitter.Inputs = alg.Inputs


infotoolpi = alg.addTupleTool("TupleToolTrackInfo")
infotoolpi.Verbose=True
infotoolpi.DataList = [ "FitVeloChi2", "FitVeloNDoF", "FitTChi2", "FitTNDoF", "FitMatchChi2", "NCandCommonHits", "Cand1stQPat", "Cand2ndQPat", "Cand1stChi2Mat", "Cand2ndChi2Mat" ]
#infotoolK = alg.K.addTupleTool("TupleToolTrackInfo/infotoolK")
#infotoolK.Verbose=True
#infotoolK.DataList = [ "FitVeloChi2", "FitVeloNDoF", "FitTChi2", "FitTNDoF", "FitMatchChi2", "NCandCommonHits", "Cand1stQPat", "Cand2ndQPat", "Cand1stChi2Mat", "Cand2ndChi2Mat" ]
#
#historytoolK = alg.K.addTupleTool("TupleToolProtoPData/historytoolK")
#historytoolK.DataList = [ "TrackHistory", "CaloTrMatch","CaloElectronMatch",
#  "CaloBremMatch", "CaloChargedSpd", "CaloChargedPrs", "CaloChargedEcal",
#  "CaloChargedID", "CaloDepositID", "ShowerShape", "ClusterMass",
#  "CaloNeutralSpd", "CaloNeutralPrs", "CaloNeutralEcal", "CaloNeutralHcal2Ecal",
#  "CaloNeutralE49", "CaloNeutralID", "CaloSpdE", "CaloPrsE",
#  "CaloEcalE", "CaloHcalE", "CaloEcalChi2", "CaloBremChi2",
#  "CaloClusChi2", "CaloTrajectoryL", "CaloEoverP", "EcalPIDe",
#  "PrsPIDe", "BremPIDe", "HcalPIDe", "HcalPIDmu",
#  "EcalPIDmu"]




historytoolpi = alg.addTupleTool("TupleToolProtoPData")
historytoolpi.DataList = [ "TrackHistory", "CaloTrMatch","CaloElectronMatch",
  "CaloBremMatch", "CaloChargedSpd", "CaloChargedPrs", "CaloChargedEcal",
  "CaloChargedID", "CaloDepositID", "ShowerShape", "ClusterMass",
  "CaloNeutralSpd", "CaloNeutralPrs", "CaloNeutralEcal", "CaloNeutralHcal2Ecal",
  "CaloNeutralE49", "CaloNeutralID", "CaloSpdE", "CaloPrsE",
  "CaloEcalE", "CaloHcalE", "CaloEcalChi2", "CaloBremChi2",
  "CaloClusChi2", "CaloTrajectoryL", "CaloEoverP", "EcalPIDe",
  "PrsPIDe", "BremPIDe", "HcalPIDe", "HcalPIDmu",
  "EcalPIDmu"]






from Configurables import DaVinci

DaVinci().Simulation = False
DaVinci().TupleFile = "flup.root"
DaVinci().Lumi = False

MessageSvc().Format = "% F%90W%S%7W%R%T %0W%M"


#DaVinci().RootInTES = "Turbo"

from Configurables import L0Conf
L0Conf().EnsureKnownTCK=False

from Configurables import LHCbApp

#LHCbApp().CondDBtag="cond-20150602"
#LHCbApp().DDDBtag="dddb-20150526"

LHCbApp().DDDBtag="dddb-20150724"
LHCbApp().CondDBtag="cond-20150805"
# BenderTools.GetDBtags     INFO       CALIBOFF : head-2015604 
# BenderTools.GetDBtags     INFO        DQFLAGS : dq-20150717 
# BenderTools.GetDBtags     INFO         ONLINE : HEAD 


DaVinci().DataType  = "2015"
from Gaudi.Configuration import *
DaVinci().EvtMax = -1

juggleSeq = GaudiSequencer("JuggleSeq")
from Configurables import RawEventJuggler
RawEventJuggler().Input = 4.1
RawEventJuggler().Output = 0.3
RawEventJuggler().Sequencer = juggleSeq


#DaVinci().UserAlgorithms = [ selseq.sequence(), alg ]
#DaVinci().UserAlgorithms = [GaudiSequencer("JuggleSeq"), alg ]
DaVinci().UserAlgorithms = [ alg ]

from Configurables import CondDB
CondDB().IgnoreHeartBeat = True


from Configurables import OTRawBankDecoder, PatSeedFit, Tf__OTHitCreator, Tf__DefaultVeloPhiHitManager, Tf__DefaultVeloRHitManager, FastVeloHitManager

ToolSvc().addTool(OTRawBankDecoder())
ToolSvc().addTool(PatSeedFit())
ToolSvc().addTool(Tf__OTHitCreator("OTHitCreator") )
ToolSvc().addTool(FastVeloHitManager("FastVeloHitManager"))
ToolSvc().addTool(Tf__DefaultVeloPhiHitManager("DefaultVeloPhiHitManager"))
ToolSvc().addTool(Tf__DefaultVeloRHitManager("DefaultVeloRHitManager"))
ToolSvc().OTRawBankDecoder.RootInTES = '/Event/' + MyStream
ToolSvc().PatSeedFit.RootInTES = '/Event/' + MyStream
ToolSvc().OTHitCreator.RootInTES = '/Event/' + MyStream
ToolSvc().FastVeloHitManager.RootInTES = '/Event/' + MyStream
ToolSvc().DefaultVeloPhiHitManager.RootInTES = '/Event/' + MyStream
ToolSvc().DefaultVeloRHitManager.RootInTES = '/Event/' + MyStream


