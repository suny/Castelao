/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <TString.h>
#include <fstream>
#include <TNtuple.h>
#include <TH1D.h>
#include <TFile.h>
#include <vector>
#include <TMath.h>
#include <TEntryList.h>

float transform(float MLP) {
  return 0.5*(1.+TMath::Log(1./MLP-1.)/TMath::Sqrt(1.+TMath::Log(1./MLP-1.)*TMath::Log(1./MLP-1.)));
}

void MakeFlatteningFunction(
	//Input file and tree to be used ("TrainTree" or "TestTree"):
	TString InputFile = "TMVA_output.root", TString Tree = "TrainTree",
	// Leaf in the tree that represents the classifier, maximum and minimum value:
	TString Classifier = "MLP",
	// Invert order?
	bool Invert = true,
	// No of points in the lookup table created:
	const int noBins = 200, 
	// Output file name:
	TString OutputFile = "Flattening.C",
  TString FunctionName = "LongTable" ,
  TString Criterion = "classID==1" )
{
	// Input stuff
	TFile* File = new TFile(InputFile);
	TTree* Data = (TNtuple*)File->Get(Tree);
  float retval;
  Data->Draw(">>theghosts",Criterion.Data(),"entrylist");
  TEntryList* entries = (TEntryList*)gDirectory->Get("theghosts");
  Data->SetBranchAddress(Classifier.Data(), &retval);
  std::vector<float> values;
  for (Long64_t ievt = 0 ; ievt<entries->GetN() ; ++ievt) {
    Data->GetEntry(entries->GetEntry(ievt));
    values.push_back(transform(retval));
  }
  std::sort(values.begin(),values.end());
  
	// Calculate output stuff
	double output[noBins+1];
	double input[noBins+1];
	if (Invert) {
    output[0] = 1.f;
		for (int i = 1; i < noBins; i++) {
			output[i] = 1.f - (float) i / noBins;
		}
    output[noBins] = 0.f;
	} else {
    output[0] = 0.f;
		for (int i = 1; i < noBins; i++) {
			output[i] = (float) i / noBins;
		}
    output[noBins] = 1.f;
	}
  input[0] = 0.f;
  for (int i = 1; i < noBins; i++) {
		input[i] = values[(values.size()*i)/noBins];
	}
	input[noBins] = 1.f;
	
	// Save as pseudo script
	std::ofstream FileStream;
	FileStream.open(OutputFile);
	
	FileStream << "// Need to add use RichDet v* Det to requirements file" << std::endl;
	FileStream << std::endl;
	
	FileStream << "// Include" << std::endl;
	FileStream << "#include \"RichDet/Rich1DTabFunc.h\"" << std::endl;
	FileStream << std::endl;
	
	FileStream << "// Create object" << std::endl;
  FileStream << "/**" << std::endl;
  FileStream << " * @brief flattening function of the GhostProbability" << std::endl;
  FileStream << " *" << std::endl;
  FileStream << " * @return instance of function. Gives ownership! User has to delete!" << std::endl;
  FileStream << " */" << std::endl;
	FileStream << "Rich::TabulatedFunction1D* " << FunctionName << "() {" << std::endl;
	FileStream << std::endl;
	
	FileStream << "// Initialisation (should do this just once, slow)" << std::endl;
	FileStream << "  double input[" << noBins+1 <<"] = {";
	for (int i = 0; i < noBins ; i++)
	{
		FileStream << input[i] << ", ";
	}
	FileStream << input[noBins] << "};" << std::endl;
	FileStream << "double output[" << noBins+1 <<"] = {";
	for (int i = 0; i < noBins ; i++)
	{
		FileStream << output[i] << ", ";
	}
	FileStream << output[noBins] << "};" << std::endl;
	FileStream << "return new Rich::TabulatedFunction1D(input, output, " << noBins+1 << ", gsl_interp_linear);" << std::endl;
	FileStream << "}" << std::endl;
	FileStream << std::endl;
	
	FileStream.close();
	
	// delete objects
	delete File;
}


