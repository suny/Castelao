/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <TString.h>
#include <fstream>
#include <TNtuple.h>
#include <TH1D.h>
#include <TFile.h>

void MakeFlatteningFunction(
	//Input file and tree to be used ("TrainTree" or "TestTree"):
	TString InputFile = "TMVA_output.root", TString Tree = "TrainTree",
	// Leaf in the tree that represents the classifier, maximum and minimum value:
	TString Classifier = "MLP", double Left = -0.2, double Right = 1.2,
	// Invert order?
	bool Invert = true,
	// No of points in the lookup table created:
	const int noBins = 200, 
	// Output file name:
	TString OutputFile = "Flattening.C")
{
	// Input stuff
	TFile* File = new TFile(InputFile);
	TNtuple* Data = (TNtuple*)File->Get(Tree);
	TH1D* OriginalHisto = new TH1D("OriginalHisto","",noBins,Left,Right);
	
	Data->Project("OriginalHisto",Classifier,"classID==1");
	
	// Calculate accumulated histogram
	int AccumulatedHisto[noBins];
	int sum = 0;
	for (int i = 0; i<noBins; i++) {
		sum += OriginalHisto->GetBinContent(i);
		AccumulatedHisto[i] = sum;
	} 
	sum += OriginalHisto->GetBinContent(noBins+1);
	
	// Calculate output stuff
	double output[noBins];
	double input[noBins];
	if (Invert) {
		for (int i = 0; i < noBins; i++) {
			output[i] = 1 - (double) AccumulatedHisto[i] / sum;
		}
	} else {
		for (int i = 0; i < noBins; i++) {
			output[i] = (double) AccumulatedHisto[i] / sum;
		}
	}
	for (int i = 0; i < noBins; i++) {
		input[i] = (double) Left + i * (Right - Left) / noBins;
	}
	
	// Save as pseudo script
	std::ofstream FileStream;
	FileStream.open(OutputFile);
	
	FileStream << "// Need to add use RichDet v* Det to requirements file" << std::endl;
	FileStream << std::endl;
	
	FileStream << "// Include" << std::endl;
	FileStream << "#include \"RichDet/Rich1DTabFunc.h\"" << std::endl;
	FileStream << std::endl;
	
	FileStream << "// Create object" << std::endl;
	FileStream << "Rich::TabulatedFunction1D* FlattenLookupTable;" << std::endl;
	FileStream << std::endl;
	
	FileStream << "// Initialisation (should do this just once, slow)" << std::endl;
	FileStream << "double input[" << noBins <<"] = {";
	for (int i = 0; i < noBins - 1; i++)
	{
		FileStream << input[i] << ", ";
	}
	FileStream << input[noBins-1] << "};" << std::endl;
	FileStream << "double output[" << noBins <<"] = {";
	for (int i = 0; i < noBins - 1; i++)
	{
		FileStream << output[i] << ", ";
	}
	FileStream << output[noBins-1] << "};" << std::endl;
	FileStream << "FlattenLookupTable = new Rich::TabulatedFunction1D(input, output, " << noBins << ", gsl_interp_linear);" << std::endl;
	FileStream << std::endl;
	
	FileStream << "// Usage (maps double to double)" << std::endl;
	FileStream << "// Should also check that input value is in required range!" << std::endl;
	FileStream << "OUTPUT = FlattenLookupTable->value(INPUT);" << std::endl;
	FileStream << std::endl;
	
	FileStream << "// Double-check result" << std::endl;
	FileStream << "if (OUTPUT < 0) OUTPUT = 0;" << std::endl;
	FileStream << "if (OUTPUT > 1) OUTPUT = 1;" << std::endl;
	FileStream << std::endl;
	
	FileStream << "// Delete in the end" << std::endl;
	FileStream << "delete FlattenLookupTable;" << std::endl;
	FileStream << std::endl;
	
	FileStream.close();
	
	// delete objects
	delete File;
}


