###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
## #####################################################################
# A stripping selection for VeloCalo K_S->pi+pi- decays
# To be used for tracking studies
#
# @authors G. Krocker, P. Seyfert, S. Wandernoth
# @date 2010-Aug-17
#
# @authors P. Seyfert, A. Jaeger
# @date 2011-May-12
# 
#######################################################################


from Gaudi.Configuration import *
from LHCbKernel.Configuration import *
from Configurables import OfflineVertexFitter,ProtoParticleMUONFilter, CombineParticles    
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from Configurables import ChargedProtoParticleMaker, NoPIDsParticleMaker, DataOnDemandSvc, UnpackTrack, DelegatingTrackSelector, TrackSelector, CombinedParticleMaker, BestPIDParticleMaker

from Configurables import FastVeloTracking
  
from StrippingConf.StrippingLine import StrippingLine
from Configurables import TrackStateInitAlg, TrackEventFitter, TrackPrepareVelo,TrackContainerCopy,TrackMatchVeloSeed, Tf__PatVeloSpaceTool
from Configurables import TrackCloneFinder
from StrippingUtils.Utils import LineBuilder

from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, TisTosParticleTagger

from Configurables import LoKi__VoidFilter
from Configurables import GaudiSequencer
from Configurables import TrackToDST
from Configurables import TrackSys
from PhysSelPython.Wrappers import AutomaticData
# Get the fitters
from TrackFitter.ConfiguredFitters import ConfiguredFit, ConfiguredFitSeed, ConfiguredFitDownstream

from Configurables import TrackEventCloneKiller,VeloCaloBuilder
from Configurables import TrackEventFitter, TrackMasterFitter
from Configurables import TrackKalmanFilter, TrackMasterExtrapolator
#from Configurables import TrackCloneFinder
from Configurables import DecodeVeloRawBuffer
from Configurables import CaloGetterTool
from Configurables import CaloGetterInit
from StandardParticles import StdLoosePions

from SelPy.utils import ( UniquelyNamedObject,
                          ClonableObject,
                          SelectionBase )
confdict={
                        "TrChi2Long":           5.      # adimensional
                ,       "TrChi2VeloCalo":       1.      # adimensional
                ,       "Doca":                 1.      # mm
                ,       "Dira":                 0.99995 # mm
                ,       "VertChi2":             5.      # mm
                ,       "MinVertChi2":          0.035   # adimensional
                ,       "MinZVert":             15.     # mm
                ,       "MassPreComb":          150.    # MeV
                ,       "MassPostComb":         60.     # MeV
                ,       "Prescale":             1
                ,       "Postscale":            1
         }

class StrippingTrackEffVeloCaloConf(LineBuilder):
    """
    Definition of tag and probe JPsi stripping.
    """
    __configuration_keys__ = (
                        "TrChi2Long"
                ,       "TrChi2VeloCalo"
                ,       "Doca"
                ,       "Dira"
                ,       "VertChi2"
                ,       "MinVertChi2"
                ,       "MinZVert"
                ,       "MassPreComb"
                ,       "MassPostComb","Prescale","Postscale"
                              )

    def __init__(self, name, config) :

        LineBuilder.__init__(self, name, config)

    
# copy & paste. CHANGE THESE!    
#    self.TisTosPreFilter1Jpsi = selHlt1Jpsi('TisTosFilter1Jpsifor'+name, HLT1TisTosSpecs = config['HLT1TisTosSpecs'], HLT1PassOnAll = config['HLT1PassOnAll'])
#    self.TisTosPreFilter2Jpsi = selHlt2Jpsi('TisTosFilter2Jpsifor'+name, hlt1Filter = self.TisTosPreFilter1Jpsi, HLT2TisTosSpecs = config['HLT2TisTosSpecs'], HLT2PassOnAll = config['HLT2PassOnAll'])


    
    self.TrackingPreFilter = trackingPreFilter('TrackingPreFilter'+name )
    self.VeloCaProtoPFilter = selCaloPParts('VeloCalo'+name, self.TrackingPreFilter)
    self.VeloCaPFilter = makeMyPions('VeloCalo'+name, self.VeloCaProtoPFilter)
    
    self.veloCaloMinusKS = chargeFilter(name+'VeloCaloKSMinus', trackAlgo =  'VeloCalo',   partSource =  self.VeloCaPFilter , charge = -1)
        self.veloCaloPlusKS = chargeFilter(name+'VeloCaloKSPlus', trackAlgo = 'VeloCalo',  partSource = self.VeloCaPFilter,  charge = 1)
    self.longMinusKS = chargeFilter( name+'LongKSMinus', trackAlgo = 'LongPi',   partSource = StdLoosePions, charge = -1)
    self.longPlusKS = chargeFilter( name+'LongKSPlus', trackAlgo =  'LongPi',   partSource = StdLoosePions  , charge = 1)
    
    # ##########################################
    self.KSPiPiTrackEff1 = makeResonanceVeloCaTrackEff(name + "VeloCaKSSel1", 
                               resonanceName = 'KS0', 
                               decayDescriptor = 'KS0 -> pi+ pi-',
                               plusCharge = self.veloCaloPlusKS, 
                               minusCharge = self.longMinusKS,
                               mode = 1,
                               TrChi2Long = config['TrChi2Long'],
                               TrChi2VeloCalo = config['TrChi2VeloCalo'],
                               Doca = config['Doca'],
                               Dira = config['Dira'],
                               VertChi2 = config['VertChi2'],
                               MinZVert = config['MinZVert'],
                               MassPreComb = config['MassPreComb'],
                               MassPostComb = config['MassPostComb'])

    self.KSPiPiTrackEff2 = makeResonanceVeloCaTrackEff(name + "VeloCaKSSel2", 
                               resonanceName = 'KS0', 
                               decayDescriptor = 'KS0 -> pi+ pi-',
                               plusCharge = self.longPlusKS,
                               minusCharge = self.veloCaloMinusKS,
                               mode = 2,
                               TrChi2Long = config['TrChi2Long'],
                               TrChi2VeloCalo = config['TrChi2VeloCalo'],
                               Doca = config['Doca'],
                               Dira = config['Dira'],
                               VertChi2 = config['VertChi2'],
                               MinZVert = config['MinZVert'],
                               MassPreComb = config['MassPreComb'],
                               MassPostComb = config['MassPostComb'])
    
    self.nominal_line1 =  StrippingLine(name + 'Line1',  prescale = config['Prescale'], postscale = config['Postscale'], algos=[self.KSPiPiTrackEff1])
    self.nominal_line2 =  StrippingLine(name + 'Line2',  prescale = config['Prescale'], postscale = config['Postscale'], algos=[self.KSPiPiTrackEff2])

    self.registerLine(self.nominal_line1)
    self.registerLine(self.nominal_line2)
    
# ########################################################################################
# Make the protoparticles
# ########################################################################################
def selCaloPParts(name, trackingSeq):
   """
       Make ProtoParticles out of VeloMuon tracks
   """
   veloprotos = ChargedProtoParticleMaker(name+"ProtoPMaker")
   veloprotos.Inputs = ["Rec/VeloCalo/Tracks"]
   veloprotos.Output = "Rec/ProtoP/"+name+"ProtoPMaker/ProtoParticles"
   veloprotos.addTool( DelegatingTrackSelector, name="TrackSelector" )
   tracktypes = [ "Long" ]
   #veloprotos.OutputLevel =0
   #if (trackcont == "Best") :
   #    tracktypes = [ "Long" ]
   veloprotos.TrackSelector.TrackTypes = tracktypes
   selector = veloprotos.TrackSelector
   for tsname in tracktypes:
       selector.addTool(TrackSelector,name=tsname)
       ts = getattr(selector,tsname)
       # Set Cuts
       ts.TrackTypes = [tsname]

#        
   veloprotoseq = GaudiSequencer(name+"ProtoPSeq")
   veloprotoseq.Members += [ veloprotos ]

   return GSWrapper(name="WrappedVeloCaloProtoPSeqFor" + name,
                    sequencer=veloprotoseq,
                    output='Rec/ProtoP/' + name +'ProtoPMaker/ProtoParticles',
                    requiredSelections = [ trackingSeq])
#   return Selection(name+"_SelPParts", Algorithm = veloprotos, OutputBranch="Rec/ProtoP", Extension="ProtoParticles",RequiredSelections=[trackingSeq], InputDataSetter=None)

def makeMyPions(name, protoParticlesMaker):
   """
     Make Particles out of the muon ProtoParticles
   """
   particleMaker =  NoPIDsParticleMaker(name+"ParticleMaker" , Particle = "Pion")
   particleMaker.Input = "Rec/ProtoP/"+name+"ProtoPMaker/ProtoParticles"
   #particleMaker.OutputLevel = 0

   DataOnDemandSvc().AlgMap.update( {
           "/Event/Phys/" + particleMaker.name() + '/Particles' : particleMaker.getFullName(),
           "/Event/Phys/" + particleMaker.name() + '/Vertices'  : particleMaker.getFullName() 
   } )


   return Selection(name+"SelVeloMuonParts", Algorithm = particleMaker, RequiredSelections = [protoParticlesMaker], InputDataSetter=None)

def makeResonanceVeloCaTrackEff(name, resonanceName, decayDescriptor, plusCharge, minusCharge, 
                              mode, Doca, Dira, TrChi2Long, TrChi2VeloCalo, VertChi2, MinZVert, MassPreComb, MassPostComb):    
   """
   THIS COMMENT IS OBSOLETE COPY AND PASTE
   Create and return a Resonance -> mu mu Selection object, with one track a long track
   and the other a MuonVelo track.
   """
    
    
   VeloCaloResonance = CombineParticles('_'+name)
   VeloCaloResonance.DecayDescriptor = decayDescriptor
   VeloCaloResonance.OutputLevel = 4 
   
   lCut = "((TRCHI2DOF < %(TrChi2Long)s)) & (MIPDV(PRIMARY)>1.5)" % locals()
   vcCut = "((TRCHI2DOF < %(TrChi2VeloCalo)s)) & (MIPDV(PRIMARY)>1.5)" % locals()

   if(mode == 1):
       VeloCaloResonance.DaughtersCuts = {"pi+": lCut,
                                          "pi-": vcCut}

       VeloCaloResonance.CombinationCut = "(ADAMASS('%(resonanceName)s')<%(MassPreComb)s*MeV) & (ADOCA(1,2) < %(Doca)s * mm)"% locals()
       VeloCaloResonance.MotherCut = "(VFASPF(VCHI2/VDOF)< %(VertChi2)s) & (ADMASS('%(resonanceName)s')<%(MassPostComb)s*MeV) & (VFASPF(VZ) > %(MinZVert)s * cm)& (BPVDIRA > %(Dira)s)"% locals()
        
       return Selection( name, Algorithm = VeloCaloResonance, RequiredSelections = [minusCharge, plusCharge] )
     
   if(mode == 2):
       VeloCaloResonance.DaughtersCuts = {"pi-": lCut  % locals(),
                                          "pi+": vcCut  % locals() }

       VeloCaloResonance.CombinationCut = "(ADAMASS('%(resonanceName)s')<%(MassPreComb)s*MeV) & (ADOCA(1,2) < %(Doca)s * mm)"% locals()
       VeloCaloResonance.MotherCut = "(VFASPF(VCHI2/VDOF)< %(VertChi2)s) & (ADMASS('%(resonanceName)s')<%(MassPostComb)s*MeV) & (VFASPF(VZ) > %(MinZVert)s * cm)& (BPVDIRA > %(Dira)s)"% locals()
            
       return Selection( name, Algorithm = VeloCaloResonance, RequiredSelections = [plusCharge, minusCharge] )
# ########################################################################################
# Charge filter, that filters, well, the charge and takes the particles from the right source (long or Velomuon)
# ########################################################################################
def chargeFilter(name, trackAlgo,  partSource, charge):
    """
        Select plus or minus charge for Velomuon or long track
    """
    Filter = FilterDesktop() #there is maybe a change needed
    myFilter1 = Filter.configurable("myFilter1")
            
    if(charge == -1):
        myFilter1.Code = "(Q < 0)"
    if(charge == 1):
        myFilter1.Code = "(Q > 0)"    
            
    if(trackAlgo == 'VeloCalo'):
        return Selection( name+'_chargeFilter'+'VeloCalo', Algorithm = myFilter1, RequiredSelections = [  partSource ] )
    if(trackAlgo == 'LongPi'):
        return Selection( name+'_chargeFilter'+'LongPi', Algorithm = myFilter1, RequiredSelections = [  partSource ] )
# ################################################################
"""
Define TisTos Prefilters

"""
#getMuonParticles = DataOnDemand(Location = 'Phys/StdLooseMuons')


#def selHlt1Jpsi(name, longPartsFilter):
def selHlt1Jpsi(name, HLT1TisTosSpecs, HLT1PassOnAll):
   """
   Filter the long track muon to be TOS on a HLT1 single muon trigger,
   for J/psi selection
   """
   #Hlt1Jpsi = TisTosParticleTagger(name+"Hlt1Jpsi")
   Hlt1Jpsi = TisTosParticleTagger(
   TisTosSpecs = HLT1TisTosSpecs #{ "Hlt1TrackMuonDecision%TOS" : 0, "Hlt1SingleMuonNoIPL0Decision%TOS" : 0}
   ,ProjectTracksToCalo = False
   ,CaloClustForCharged = False
   ,CaloClustForNeutral = False
   ,TOSFrac = { 4:0.0, 5:0.0 }
   ,NoRegex = True
   )
   Hlt1Jpsi.PassOnAll = HLT1PassOnAll
   #Hlt1Jpsi.PassOnAll = True # TESTING!
   #
   return Selection(name+"_SelHlt1Jpsi", Algorithm = Hlt1Jpsi, RequiredSelections = [ StdLooseMuons ])

#########################################################
def selHlt2Jpsi(name, hlt1Filter, HLT2TisTosSpecs, HLT2PassOnAll):
   """
   Filter the long track muon to be TOS on a HLT2 single muon trigger,
   for J/psi selection
   """
   #Hlt2Jpsi = TisTosParticleTagger("Hlt2Jpsi")
   Hlt2Jpsi = TisTosParticleTagger(
   TisTosSpecs =HLT2TisTosSpecs #{ "Hlt2SingleMuon.*Decision%TOS" : 0}
   ,ProjectTracksToCalo = False
   ,CaloClustForCharged = False
   ,CaloClustForNeutral = False
   ,TOSFrac = { 4:0.0, 5:0.0 }
   ,NoRegex = False
   )
   Hlt2Jpsi.PassOnAll = HLT2PassOnAll
   #Hlt2Jpsi.PassOnAll = True # TESTING!
   #
   return Selection(name+"_SelHlt2Jpsi", Algorithm = Hlt2Jpsi, RequiredSelections = [ hlt1Filter ])
##########################################################
        

def trackingPreFilter(name):

   calo = CaloGetterTool("CaloGetterTool")
   calo.GetClusters = True

   MyVeloCaloBuilder = VeloCaloBuilder("VeloCaloBuilder")
   #VeloCaloBuilder.OutputLevel = 1 
   MyVeloCaloBuilder.addTool(calo)
   MyVeloCaloBuilder.quali = 3    #max. matching chi-square for tracks to be stored
   MyVeloCaloBuilder.IPcut = -999 #impact parameter with respect to z-axis of velotracks to be considered
   MyVeloCaloBuilder.zcut = 50    #z position of first state of velotracks to be considered
   MyVeloCaloBuilder.VeloLoc = "Rec/Track/UnFittedVelo"
   MyVeloCaloBuilder.OutputTracks = "Rec/VeloCalo/Tracks"

   preve = TrackPrepareVelo("preve")
   preve.inputLocation = "Rec/Track/Velo"
   preve.outputLocation = "Rec/Track/UnFittedVelo"
   preve.bestLocation = ""

   #TODO: apparently FastVelo is now (april 2012) run with fixes in the production which don't neccessarily apply to the stripping...
   alg = GaudiSequencer("VeloCaloTrackingFor"+name,
                         Members = [ DecodeVeloRawBuffer(name+"VeloDecoding",DecodeToVeloLiteClusters=True,DecodeToVeloClusters=True),
                     FastVeloTracking(name+"FastVelo",OutputTracksName="Rec/Track/Velo"),
                 preve, CaloGetterInit(), 
                 MyVeloCaloBuilder])

   return GSWrapper(name="WrappedVeloMuonTracking",
                     sequencer=alg,
                     output='Rec/VeloCalo/Tracks',requiredSelections = [StdLoosePions])
#,
#                     requiredSelections = [ StdLoosePions])



class GSWrapper(UniquelyNamedObject,
                ClonableObject,
                SelectionBase) :
    
    def __init__(self, name, sequencer, output, requiredSelections) :
        UniquelyNamedObject.__init__(self, name)
        ClonableObject.__init__(self, locals())
        SelectionBase.__init__(self,
                               algorithm = sequencer,
                               outputLocation = output,
                               requiredSelections = requiredSelections )




