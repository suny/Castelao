###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *

from Configurables import Brunel, LHCbApp
Brunel()


from Configurables import LHCbApp
LHCbApp().DDDBtag   = "dddb-20130929-1"
LHCbApp().CondDBtag = "sim-20131023-vc-md100"



Brunel().DataType = '2012'  # sets the 2011 configuration of Brunel
Brunel().InputType = "DST" # input has the format digi
Brunel().WithMC    = True   # use the MC truth information in the digi file
Brunel().Simulation = True
Brunel().OutputType = "NONE"
Brunel().EvtMax = 10

from Configurables import TrackSys
from Configurables import RecSysConf, RecMoniConf

Brunel().RecoSequence = ["Decoding","VELO","TT","IT","OT","Tr","Vertex"]#,"RICH","CALO","MUON","PROTO","SUMMARY"]
RecMoniConf().MoniSequence = ["Tr"]#,"OT"]
Brunel().MCLinksSequence = ['Unpack',"Tr"]
Brunel().MCCheckSequence = ["Pat",
                           "Tr"]



#from Configurables import TrackEffChecker
#from Configurables import TypeStat
#TrackEffChecker("BestTracks").ChiCut = 3.
#typestat = TypeStat("TypeStat")
#typestat.ChiCut = 3.
#typestat.KLCut = False
#typestat.SelectionCriteria = "ChargedLong"
#typestat.GhostClassification = "LongGhostClassification"
#typestat.Selector = "MCReconstructible/Selector"
#typestat.TracksInContainer = "Rec/Track/Best"

#from Configurables import TrackBestTrackCreator
##TrackBestTrackCreator().MaxChi2DoF = 1.5
#
#
#
#
#
#
from Configurables import TrackEffChecker, TrackAssociator,TrackContainerCopy
from Configurables import ExchangeAnalyzer, TrackEffNT, UnpackTrack, EventNodeKiller
#
copyBest = TrackContainerCopy( "CopyForward" )
copyBest.inputLocation = "Rec/Track/Best"
mylocation = "RecoTwelve/Track/Best"
copyBest.outputLocation = mylocation
#copyBest.OutputLevel = 0
GaudiSequencer("InitReprocSeq").Members+=[UnpackTrack(),copyBest]
EventNodeKiller().Nodes += [ "Rec/Track" ]
def stuff() :
    assoCopy = TrackAssociator("AssocBest").clone("AssocCopy")
    
    checkCopy = TrackEffChecker("BestTracks").clone("CheckReco12")
    checkOrg = TrackEffChecker("BestTracks").clone("CheckReco14")
    checkCopy.HistoDir = "Copy"
    GaudiSequencer("CheckPatSeq").Members += [assoCopy,checkCopy,checkOrg]
    checkOrg.ChiCut = 5.
    checkCopy.ChiCut = 3.
    for checker in [checkOrg, checkCopy] :
       checker.KLCut = True
       checker.SelectionCriteria = "ChargedLong"
       checker.RequireLongTrack = True
    for alg in [checkCopy,assoCopy] :
      alg.TracksInContainer = mylocation
      #alg.OutputLevel = 0
    GaudiSequencer("CheckPatSeq").Members += [TrackEffNT("oldReconewReco",TracksInContainerA=mylocation,TracksInContainerB="Rec/Track/Best")]

appendPostConfigAction(stuff)

TrackEffNT("oldReconewReco").NTupleLUN = "FILE1"

from Configurables import  NTupleSvc

NTupleSvc().Output += ["FILE1 DATAFILE='myalg.root' TYP='ROOT' OPT='NEW'"]
from Configurables import TrackInitFit

from Configurables import MCReconstructible, MCParticleSelector, TrackProjectorSelector, TrackMasterFitter, TrajOTProjector
TrackEffNT("oldReconewReco").addTool(MCReconstructible, name="Selector")
TrackEffNT("oldReconewReco").Selector.addTool(MCParticleSelector, name="Selector")
TrackEffNT("oldReconewReco").Selector.Selector.rejectElectrons = True
TrackEffNT("oldReconewReco").Selector.Selector.rejectInteractions = True
TrackEffNT("oldReconewReco").Selector.Selector.zInteraction = 9400.
TrackEffNT("oldReconewReco").addTool(TrackInitFit().clone("myTrackInitFit"))
TrackEffNT("oldReconewReco").myTrackInitFit.addTool(TrackMasterFitter().clone("Fit"))
TrackEffNT("oldReconewReco").myTrackInitFit.Fit.addTool(TrackProjectorSelector())
#TrackEffNT("forwardmatch").myTrackInitFit.Fit.TrackProjectorSelector.OT = "TrajOTProjector/myOTprojector"
#TrackEffNT("forwardmatch").myTrackInitFit.Fit.TrackProjectorSelector.addTool(TrajOTProjector().clone("myOTprojector"))
#TrackEffNT("forwardmatch").myTrackInitFit.Fit.TrackProjectorSelector.myOTprojector.MaxDriftTimePull = -1
#




from Configurables import L0Conf
#L0Conf().EnsureKnownTCK=False

