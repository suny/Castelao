###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from Configurables import Brunel, LHCbApp


#Brunel().DatasetName = datasetName # sets output and histogram file names
Brunel().DataType = '2012'  # sets the 2011 configuration of Brunel
Brunel().InputType = "DIGI" # input has the format digi
Brunel().WithMC    = True   # use the MC truth information in the digi file
Brunel().Simulation = True
Brunel().OutputType = "NONE"

from Configurables import Brunel, TrackSys

Brunel().RecoSequence = ["Decoding","VELO","TT","IT","OT","Tr","Vertex"]#,"RICH","CALO","MUON","PROTO","SUMMARY"]
from Configurables import RecSysConf, RecMoniConf
RecMoniConf().MoniSequence = ["Tr","OT"]
Brunel().MCCheckSequence = ["Pat","Tr"]







from GaudiKernel.ProcessJobOptions import importOptions
from Gaudi.Configuration import *
#importOptions("$APPCONFIGOPTS/Brunel/MC09-WithTruth.py")
from Configurables import Brunel, TrackSys, LHCbApp
from Configurables import RecSysConf, RecMoniConf
from Configurables import PatForward
from Configurables import PatForwardTool

#Brunel().Monitors = ["SC"]
#Brunel().MoniSequence = []
#from Configurables import Brunel



from Configurables import TrackEffChecker
from Configurables import TypeStat
TrackEffChecker("BestTracks").ChiCut = 3.
typestat = TypeStat("TypeStat")
typestat.ChiCut = 3.
typestat.KLCut = False
typestat.SelectionCriteria = "ChargedLong"
typestat.GhostClassification = "LongGhostClassification"
typestat.Selector = "MCReconstructible/Selector"
typestat.TracksInContainer = "Rec/Track/Best"
cdstat = TypeStat("TypeStat2")
cdstat.ChiCut = 3.
cdstat.KLCut = True
cdstat.SelectionCriteria = "ChargedDownstream"
cdstat.GhostClassification = "LongGhostClassification"
cdstat.Selector = "MCReconstructible/Selector"
cdstat.TracksInContainer = "Rec/Track/Best"
def what_I_want():
        TrackEffChecker("BestTracks").RequireLongTrack = False
        allbest = TrackEffChecker("BestTracks").clone("AllBest")
        allbest.HistoDir = "AllBest"
        allbest.SelectionCriteria = "ChargedDownstream"
        longbest = TrackEffChecker("BestTracks").clone("LongBest")
        longbest.HistoDir = "LongBest"
        longbest.SelectionCriteria = "ChargedLong"
        longbest.RequireLongTrack = True
        alldown = TrackEffChecker("Downstream").clone("LongDown")
        alldown.HistoDir = "LongDown"
        alldown.SelectionCriteria = "ChargedLong"
        downchecker = TrackEffChecker("BestTracks").clone("DownDown")
        downchecker.HistoDir = "DownDown"
        downchecker.SelectionCriteria = "ChargedDownstream"
        downchecker.RequireDownTrack = True
        downchecker.RequireLongTrack = False
        downnotlongchecker = TrackEffChecker("BestTracks").clone("DownNotLong")
        downnotlongchecker.HistoDir = "DownNotLong"
        downnotlongchecker.SelectionCriteria = "ChargedLong"
        downnotlongchecker.RequireDownTrack = True
        downnotlongchecker.RequireLongTrack = False
        downnotlongchecker.DownNotLong = True
        

        downlchecker = TrackEffChecker("BestTracks").clone("DownOfLong")
        downlchecker.HistoDir = "DownOfLong"
        downlchecker.SelectionCriteria = "ChargedLong"
        downlchecker.RequireDownTrack = True
        downlchecker.RequireLongTrack = False
        GaudiSequencer("CheckPatSeq").Members += [ allbest,longbest,alldown,typestat,cdstat,downchecker,downlchecker , downnotlongchecker]
appendPostConfigAction(what_I_want)
#TrackEffChecker("BestTracks").OutputLevel = 0
#TrackEffChecker("AllBest").OutputLevel = 0

from Configurables import TrackBestTrackCreator
#TrackBestTrackCreator().MaxChi2DoF = 1.5

#TrackBestTrackCreator().OutputLevel = 0





from GaudiKernel.ProcessJobOptions import importOptions
from Gaudi.Configuration import *
#importOptions("$APPCONFIGOPTS/Brunel/MC09-WithTruth.py")
from Configurables import Brunel, TrackSys, LHCbApp
from Configurables import RecSysConf, RecMoniConf


from Configurables import TrackEffChecker, TrackAssociator

from Configurables import TrackEventCloneKiller, TrackOverlapCreator,TrackContainerCopy
GaudiSequencer("TrackFitSeq").Members+=[TrackEventCloneKiller()]
def overlapcheck():
     GaudiSequencer("TrackFitSeq").Members+=[TrackOverlapCreator()]
appendPostConfigAction(overlapcheck)
TrackEventCloneKiller().CopyTracks=True
TrackEventCloneKiller().CompareInSameContainerForwardUpstream=False
TrackEventCloneKiller().SkipSameContainerTracks=True
TrackEventCloneKiller().StoreCloneTracks=False
TrackEventCloneKiller().IgnoredTrackTypes=[  ]
TrackEventCloneKiller().TracksOutContainer= "Rec/Track/AllBest"
TrackEventCloneKiller().TracksInContainers=[ 'Rec/Track/Forward' , 'Rec/Track/Seed' , 'Rec/Track/Match' , 'Rec/Track/Downstream' , 'Rec/Track/VeloTT' ]

from TrackFitter.ConfiguredFitters import ConfiguredFit
from Configurables import TrackStateInitAlg
stateInitAlg = TrackStateInitAlg("InitBestFit")
stateInitAlg.TrackLocation = "Rec/Track/AllBest"
stateInitAlg.StateInitTool.VeloFitterName = "FastVeloFitLHCbIDs"
GaudiSequencer("TrackFitSeq").Members += [stateInitAlg]
GaudiSequencer("TrackFitSeq").Members += [ConfiguredFit("FitBest_","Rec/Track/AllBest")]      

copyBest = TrackContainerCopy( "CopyForward" )
copyBest.inputLocation = "Rec/Track/Forward";
copyBest.outputLocation = "Rec/Track/FittedForward"
GaudiSequencer("TrackFitSeq").Members += [ copyBest ]
stateInitAlg2 = TrackStateInitAlg("InitForwardFit")
stateInitAlg2.TrackLocation = "Rec/Track/FittedForward"
stateInitAlg2.StateInitTool.VeloFitterName = "FastVeloFitLHCbIDs"
GaudiSequencer("TrackFitSeq").Members += [stateInitAlg]
GaudiSequencer("TrackFitSeq").Members += [ConfiguredFit("FitForward_","Rec/Track/FittedForward")]      

copyBest = TrackContainerCopy( "CopyMatch" )
copyBest.inputLocation = "Rec/Track/Match";
copyBest.outputLocation = "Rec/Track/FittedMatch"
GaudiSequencer("TrackFitSeq").Members += [ copyBest ]
stateInitAlg2 = TrackStateInitAlg("InitMatchFit")
stateInitAlg2.TrackLocation = "Rec/Track/FittedMatch"
stateInitAlg2.StateInitTool.VeloFitterName = "FastVeloFitLHCbIDs"
GaudiSequencer("TrackFitSeq").Members += [stateInitAlg]
GaudiSequencer("TrackFitSeq").Members += [ConfiguredFit("FitMatch_","Rec/Track/FittedMatch")]      

from Configurables import TrackBuildCloneTable, TrackCloneCleaner

def what_I_want_as_well():
   for l in ["Match","Forward"]:
      asso = TrackAssociator("AssocBest").clone("AssocFitted"+l)
      asso.TracksInContainer = "Rec/Track/Fitted" + l
      GaudiSequencer("CheckPatSeq").Members += [asso]


def what_I_want(choice):
        if (1 == choice):
          asso = TrackAssociator("AssocBest").clone("AssocOverlap")
        if (2 == choice):
          asso = TrackAssociator("AssocBest").clone("AssocOldOnly")
        if (3 == choice):
          asso = TrackAssociator("AssocBest").clone("AssocNewOnly")
        if (4 == choice):
          asso = TrackAssociator("AssocBest").clone("AssocOld")
        if (5 == choice):
          asso = TrackAssociator("AssocBest").clone("AssocMerge")
        TrackEffChecker("BestTracks").RequireLongTrack = False
        if (1 == choice):
          allbest = TrackEffChecker("BestTracks").clone("AllBestOverlap")
        if (2 == choice):
          allbest = TrackEffChecker("BestTracks").clone("AllBestOldOnly")
        if (3 == choice):
          allbest = TrackEffChecker("BestTracks").clone("AllBestNewOnly")
        if (4 == choice):
          allbest = TrackEffChecker("BestTracks").clone("AllBestOld")
        if (5 == choice):
          allbest = TrackEffChecker("BestTracks").clone("AllBestMerge")
        allbest.HistoDir = "AllBest"
        allbest.SelectionCriteria = "ChargedDownstream"
        if (1 == choice):
          longbest = TrackEffChecker("BestTracks").clone("LongBestOverlap")
        if (2 == choice):
          longbest = TrackEffChecker("BestTracks").clone("LongBestOldOnly")
        if (3 == choice):
          longbest = TrackEffChecker("BestTracks").clone("LongBestNewOnly")
        if (4 == choice):
          longbest = TrackEffChecker("BestTracks").clone("LongBestOld")
        if (5 == choice):
          longbest = TrackEffChecker("BestTracks").clone("LongBestMerge")
        longbest.HistoDir = "LongBest"
        longbest.SelectionCriteria = "ChargedLong"
        longbest.RequireLongTrack = True
        if (1 == choice):
          downchecker = TrackEffChecker("BestTracks").clone("DownDownOverlap")
        if (2 == choice):
          downchecker = TrackEffChecker("BestTracks").clone("DownDownOldOnly")
        if (3 == choice):
          downchecker = TrackEffChecker("BestTracks").clone("DownDownNewOnly")
        if (4 == choice):
          downchecker = TrackEffChecker("BestTracks").clone("DownDownOld")
        if (5 == choice):
          downchecker = TrackEffChecker("BestTracks").clone("DownDownMerge")
        downchecker.HistoDir = "DownDown"
        downchecker.SelectionCriteria = "ChargedDownstream"
        downchecker.RequireDownTrack = True
        downchecker.RequireLongTrack = False
        if (1 == choice):
          downnotlongchecker = TrackEffChecker("BestTracks").clone("DownNotLongOverlap")
        if (2 == choice):
          downnotlongchecker = TrackEffChecker("BestTracks").clone("DownNotLongOldOnly")
        if (3 == choice):
          downnotlongchecker = TrackEffChecker("BestTracks").clone("DownNotLongNewOnly")
        if (4 == choice):
          downnotlongchecker = TrackEffChecker("BestTracks").clone("DownNotLongOld")
        if (5 == choice):
          downnotlongchecker = TrackEffChecker("BestTracks").clone("DownNotLongMerge")
        downnotlongchecker.HistoDir = "DownNotLong"
        downnotlongchecker.SelectionCriteria = "ChargedLong"
        downnotlongchecker.RequireDownTrack = True
        downnotlongchecker.RequireLongTrack = False
        downnotlongchecker.DownNotLong = True

        if (1 == choice):
          downlchecker = TrackEffChecker("BestTracks").clone("DownOfLongOverlap")
        if (2 == choice):
          downlchecker = TrackEffChecker("BestTracks").clone("DownOfLongOldOnly")
        if (3 == choice):
          downlchecker = TrackEffChecker("BestTracks").clone("DownOfLongNewOnly")
        if (4 == choice):
          downlchecker = TrackEffChecker("BestTracks").clone("DownOfLongOld")
        if (5 == choice):
          downlchecker = TrackEffChecker("BestTracks").clone("DownOfLongMerge")
        downlchecker.HistoDir = "DownOfLong"
        downlchecker.SelectionCriteria = "ChargedLong"
        downlchecker.RequireDownTrack = True
        downlchecker.RequireLongTrack = False
        checkerliste = [ allbest,longbest,downchecker,downlchecker , downnotlongchecker]
        liste = [ asso] 
        for alg in checkerliste:
           liste += [alg]
        GaudiSequencer("CheckPatSeq").Members += liste
        for alg in liste:
          if (1 == choice):
           alg.TracksInContainer = "Rec/Track/Overlap"
          if (2 == choice):
           alg.TracksInContainer = "Rec/Track/OldOnly"
          if (3 == choice):
           alg.TracksInContainer = "Rec/Track/NewOnly"
          if (4 == choice):
           alg.TracksInContainer = "Rec/Track/AllBest"
          if (5 == choice):
           alg.TracksInContainer = "Rec/Track/Merge"
        for alg in checkerliste:
          if (1 == choice):
           alg.HistoDir = alg.HistoDir + "Overlap"
          if (2 == choice):
           alg.HistoDir = alg.HistoDir + "OldOnly"
          if (3 == choice):
           alg.HistoDir = alg.HistoDir + "NewOnly"
          if (4 == choice):
           alg.HistoDir = alg.HistoDir + "Old"
          if (5 == choice):
           alg.HistoDir = alg.HistoDir + "Merge"


from Configurables import ExchangeAnalyzer, TrackEffNT
def what_I_do():
   for i in [1,2,3,4,5]:
     what_I_want(i)
   GaudiSequencer("CheckPatSeq").Members += [ExchangeAnalyzer(TracksInContainerA="Rec/Track/OldOnly",TracksInContainerB="Rec/Track/NewOnly"),TrackEffNT("newclonekilleroldclonekiller",TracksInContainerA="Rec/Track/Best",TracksInContainerB="Rec/Track/AllBest"),TrackEffNT("forwardmatch",TracksInContainerA="Rec/Track/FittedForward",TracksInContainerB="Rec/Track/FittedMatch")]
appendPostConfigAction(what_I_want_as_well)
appendPostConfigAction(what_I_do)

def cloneflagging():
  for c in ["AllBest","FittedForward","FittedMatch"] :
    build = TrackBuildCloneTable("FindTrackClones").clone("FindClonesIn"+c)
    flag = TrackCloneCleaner("FlagTrackClones").clone("FlagClonesIn"+c)
    build.inputLocation = "Rec/Track/"+c
    build.outputLocation = "Rec/Track/"+c+"Clones"
    flag.inputLocation = "Rec/Track/"+c
    flag.linkerLocation = "Rec/Track/"+c+"Clones"
    GaudiSequencer("TrackFitSeq").Members += [build,flag]
    
appendPostConfigAction(cloneflagging)




TrackEffNT("newclonekilleroldclonekiller").NTupleLUN = "FILE1"
TrackEffNT("forwardmatch").NTupleLUN = "FILE1"

from Configurables import  NTupleSvc

NTupleSvc().Output += ["FILE1 DATAFILE='myalg.root' TYP='ROOT' OPT='NEW'"]

from Configurables import TrackInitFit

from Configurables import MCReconstructible, MCParticleSelector, TrackProjectorSelector, TrackMasterFitter, TrajOTProjector
TrackEffNT("forwardmatch").addTool(MCReconstructible, name="Selector")
TrackEffNT("forwardmatch").Selector.addTool(MCParticleSelector, name="Selector")
TrackEffNT("forwardmatch").Selector.Selector.rejectElectrons = True
TrackEffNT("forwardmatch").Selector.Selector.rejectInteractions = True
TrackEffNT("forwardmatch").Selector.Selector.zInteraction = 9400.
TrackEffNT("forwardmatch").addTool(TrackInitFit().clone("myTrackInitFit"))
TrackEffNT("forwardmatch").myTrackInitFit.addTool(TrackMasterFitter().clone("Fit"))
TrackEffNT("forwardmatch").myTrackInitFit.Fit.addTool(TrackProjectorSelector())
TrackEffNT("forwardmatch").myTrackInitFit.Fit.TrackProjectorSelector.OT = "TrajOTProjector/myOTprojector"
TrackEffNT("forwardmatch").myTrackInitFit.Fit.TrackProjectorSelector.addTool(TrajOTProjector().clone("myOTprojector"))
TrackEffNT("forwardmatch").myTrackInitFit.Fit.TrackProjectorSelector.myOTprojector.MaxDriftTimePull = -1

TrackEffNT("newclonekilleroldclonekiller").addTool(MCReconstructible, name="Selector")
TrackEffNT("newclonekilleroldclonekiller").addTool(TrackInitFit().clone("myTrackInitFit"),name="myTrackInitFit")
TrackEffNT("newclonekilleroldclonekiller").Selector.addTool(MCParticleSelector, name="Selector")
TrackEffNT("newclonekilleroldclonekiller").Selector.Selector.rejectElectrons = True
TrackEffNT("newclonekilleroldclonekiller").Selector.Selector.rejectInteractions = True
TrackEffNT("newclonekilleroldclonekiller").Selector.Selector.zInteraction = 9400.
TrackEffNT("newclonekilleroldclonekiller").addTool(TrackInitFit().clone("myTrackInitFit"))
TrackEffNT("newclonekilleroldclonekiller").myTrackInitFit.addTool(TrackMasterFitter().clone("Fit"))
TrackEffNT("newclonekilleroldclonekiller").myTrackInitFit.Fit.addTool(TrackProjectorSelector())
TrackEffNT("newclonekilleroldclonekiller").myTrackInitFit.Fit.TrackProjectorSelector.OT = "TrajOTProjector/myOTprojector"
TrackEffNT("newclonekilleroldclonekiller").myTrackInitFit.Fit.TrackProjectorSelector.addTool(TrajOTProjector().clone("myOTprojector"))
TrackEffNT("newclonekilleroldclonekiller").myTrackInitFit.Fit.TrackProjectorSelector.myOTprojector.MaxDriftTimePull = -1







from Configurables import L0Conf
L0Conf().EnsureKnownTCK=False

