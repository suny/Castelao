/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
#ifndef INCLUDE_TRACKEFFNT_H
#define INCLUDE_TRACKEFFNT_H 1

#include "GaudiAlg/GaudiTupleAlg.h"
#include "TrackCheckerTwoBase.h"

#include "Event/Track.h"
#include "Linker/AllLinks.h"
//#include "TrackStateInitTool.h"
#include "GaudiKernel/ToolHandle.h"
#include "TrackInterfaces/ITrackFitter.h"
#include "TrackInterfaces/ITrackStateInit.h"
#include "MCInterfaces/ITrackGhostClassification.h"

class ITrackManipulator;

/** @class TrackEffNT TrackEffNT.h
 *  
 *
 * @author Paul Seyfert <pseyfert@cern.ch>
 * @date   2012-06-19
 */
class TrackEffNT : public TrackCheckerTwoBase {

public:

  /// Standard Constructor
  TrackEffNT(const std::string& name, ISvcLocator* pSvcLocator);

  virtual ~TrackEffNT(); ///< Destructor

  virtual StatusCode initialize(); ///< Algorithm initialization
  virtual StatusCode    execute(); ///< Algorithm event execution
  virtual StatusCode   finalize(); ///< Algorithm finalize
  void fillTrack(LHCb::Track* tr, bool priv);
  bool samehits(LHCb::Track* a , LHCb::Track* b);
  void filltrackparticleindex() ;
  void cuttracks();

protected:

  IMCReconstructible::RecCategory m_long; ///<  Pointer to selector 
  IMCReconstructible::RecCategory m_down; ///<  Pointer to selector 


private:

  ToolHandle<ITrackStateInit> m_initTool;
  ITrackFitter* m_trackFitter;
  ITrackFitter* m_trackFitter_priv;
  ITrackGhostClassification* m_ghostClassification;
  ITrackManipulator* m_ghostTool;
  std::vector<float> v_trackchia;
  std::vector<float> v_tchia;
  std::vector<float> v_velochia;
  std::vector<float> v_dofa;
  std::vector<float> v_tdofa;
  std::vector<float> v_vdofa;
  std::vector<float> v_trackchib;
  std::vector<float> v_tchib;
  std::vector<float> v_velochib;
  std::vector<float> v_dofb;
  std::vector<float> v_tdofb;
  std::vector<float> v_vdofb;
  std::vector<float> v_tap;
  std::vector<float> v_tapt;
  std::vector<float> v_taphi;
  std::vector<float> v_taeta;
  std::vector<float> v_tbp;
  std::vector<float> v_tbpt;
  std::vector<float> v_tbphi;
  std::vector<float> v_tbeta;
  std::vector<float> v_talastx;
  std::vector<float> v_talasty;
  std::vector<float> v_tblastx;
  std::vector<float> v_tblasty;
  std::vector<float> v_p;
  std::vector<float> v_pt;
  std::vector<float> v_phi;
  std::vector<float> v_eta;
  std::vector<float> v_otexp;
  std::vector<float> v_otobsa;
  std::vector<float> v_otobsb;
  std::vector<float> v_itexp;
  std::vector<float> v_itobsa;
  std::vector<float> v_itobsb;
  std::vector<float> v_ttexp;
  std::vector<float> v_ttobsa;
  std::vector<float> v_ttobsb;
  std::vector<float> v_veloexp;
  std::vector<float> v_veloobsa;
  std::vector<float> v_veloobsb;
  std::vector<float> v_pid;
  std::vector<float> v_fromks;
  std::vector<float> v_longrec;
  std::vector<float> v_downrec;
  std::vector<float> v_fromb;
  std::vector<float> v_fromgoodb;
  std::vector<float> v_tracktypea;
  std::vector<float> v_tracktypeb;
  std::vector<float> v_counta;
  std::vector<float> v_countb;
  std::vector<float> v_tracktypeoverlap;
  std::vector<float> v_countoverlap;
  std::vector<int>   v_tindexa;
  std::vector<int>   v_tindexb;
  std::vector<float> v_motherz;
  std::vector<LHCb::MCParticle*> v_pointer;


  std::vector<float> t_type;
  std::vector<float> t_chi;
  std::vector<float> t_gp;
  std::vector<float> t_vchi;
  std::vector<float> t_tchi;
  std::vector<float> t_mchi;
  std::vector<float> t_dof;
  std::vector<float> t_vdof;
  std::vector<float> t_tdof;
  std::vector<float> t_hist;
  std::vector<float> t_p;
  std::vector<float> t_pt;
  std::vector<float> t_phi;
  std::vector<float> t_eta;
  std::vector<float> t_otexp;
  std::vector<float> t_otobs;
  std::vector<float> t_otout;
  std::vector<float> t_otbad;
  std::vector<float> t_ottru;
  std::vector<float> t_otfal;
  std::vector<float> t_otgoodsup;
  std::vector<float> t_otbadsup;
  std::vector<float> t_itexp;
  std::vector<float> t_itobs;
  std::vector<float> t_ttexp;
  std::vector<float> t_ttobs;
  std::vector<float> t_veloexp;
  std::vector<float> t_veloobs;
  std::vector<float> t_tblastx;
  std::vector<float> t_tblasty;
  std::vector<float> t_ina;
  std::vector<float> t_inb;
  std::vector<int>   t_particleindex;
  std::vector<int>   t_refitindex;
  std::vector<LHCb::Track*> t_pointera;
  std::vector<LHCb::Track*> t_pointerb;
  std::vector<float> t_ttless_chi;
  std::vector<float> t_ttless_vchi;
  std::vector<float> t_ttless_tchi;
  std::vector<float> t_ttless_mchi;
  std::vector<float> t_ttless_dof;
  std::vector<float> t_ttless_vdof;
  std::vector<float> t_ttless_tdof;
  std::vector<float> t_ttless_p;
  std::vector<float> t_ttless_pt;
  std::vector<float> t_ttless_phi;
  std::vector<float> t_ttless_eta;
  bool fillingA;
  std::vector<float> t_wrongttless_chi;
  std::vector<float> t_wrongttless_vchi;
  std::vector<float> t_wrongttless_tchi;
  std::vector<float> t_wrongttless_mchi;
  std::vector<float> t_wrongttless_dof;
  std::vector<float> t_wrongttless_vdof;
  std::vector<float> t_wrongttless_tdof;
  std::vector<float> t_wrongttless_p;
  std::vector<float> t_wrongttless_pt;
  std::vector<float> t_wrongttless_phi;
  std::vector<float> t_wrongttless_eta;
  std::vector<float> t_ghostcat;
  

  std::vector<float> t_kl;



/// no idea what this is
  std::vector< std::vector<int> > m_linkedIds;


  std::vector<LHCb::Track*> privtracks;

};
#endif // INCLUDE_TRACKEFFNT_H

