/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
#include "Linker/AllLinks.h"

#include "ExchangeAnalyzer.h"


//-----------------------------------------------------------------------------
// Implementation file for class : ExchangeAnalyzer
//
// 2012-06-06 : Paul Seyfert <pseyfert@cern.ch>
//-----------------------------------------------------------------------------

DECLARE_COMPONENT( ExchangeAnalyzer );


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
ExchangeAnalyzer::ExchangeAnalyzer(const std::string& name, ISvcLocator* pSvcLocator)
  : TrackCheckerTwoBase(name, pSvcLocator)
{
  ;
}

//=============================================================================
// Destructor
//=============================================================================
ExchangeAnalyzer::~ExchangeAnalyzer() {;}

//=============================================================================
// Initialization
//=============================================================================
StatusCode ExchangeAnalyzer::initialize()
{
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if (sc.isFailure()) return sc;  // error printed already by GaudiAlgorithm

  debug() << "==> Initialize" << endmsg;


  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode ExchangeAnalyzer::execute()
{
  debug() << "==> Execute" << endmsg;

  AllLinks<LHCb::MCParticle,ContainedObject> allIds( evtSvc(), msgSvc(), "Pat/LHCbID" );
  m_linkedIds.clear();
  LHCb::MCParticle* part = allIds.first();
  while ( NULL != part ) {
    unsigned int minSize = part->key();
    while ( m_linkedIds.size() <= minSize ) {
      std::vector<int> dum;
      m_linkedIds.push_back( dum );
    }
    m_linkedIds[part->key()].push_back( allIds.key() );
    part = allIds.next();
  }



  if (initializeEvent().isFailure()){
    return Error("Failed to initialize event", StatusCode::FAILURE);
  }


  LHCb::Tracks* oldones = get<LHCb::Tracks>(tracksInContainerA());
  LHCb::Tracks* newones = get<LHCb::Tracks>(tracksInContainerB());
  //LHCb::Tracks* oldones = get<LHCb::Tracks>("Rec/Track/AllBest");
  //LHCb::Tracks* newones = get<LHCb::Tracks>("Rec/Track/Best");

  for (LHCb::Tracks::const_iterator told = oldones->begin() ; oldones->end() != told ; ++told) {
    const LHCb::MCParticle* pold = mcTruth(*told);
    if (NULL==pold) {counter("oldghost")+=1;continue;}
    const LHCb::MCParticle* foundnew = NULL;
    for (LHCb::Tracks::const_iterator tnew = newones->begin() ; newones->end() != tnew ; ++tnew) {
      const LHCb::MCParticle* pnew = mcTruth(*tnew);
      if (pnew==pold) {
        foundnew = pnew ; 
        break;
      }
    }
    if (foundnew)
      counter("weExchanged")+=1;
    else
      counter("weLost")+=1;
  }

  for (LHCb::Tracks::const_iterator tnew = newones->begin() ; newones->end() != tnew ; ++tnew) {
    const LHCb::MCParticle* pnew = mcTruth(*tnew);
    if (NULL==pnew) {counter("newghost")+=1;continue;}
    const LHCb::MCParticle* foundold = NULL;
    for (LHCb::Tracks::const_iterator told = oldones->begin() ; oldones->end() != told ; ++told) {
      const LHCb::MCParticle* pold = mcTruth(*told);
      if (pnew==pold) {
        foundold = pold ; 
        break;
      }
    }
    if (foundold)
      counter("weExchanged")+=0;
    else
      counter("weGained")+=1;
  }
  return StatusCode::SUCCESS;
}

//=============================================================================
// Finalize
//=============================================================================
StatusCode ExchangeAnalyzer::finalize()
{
  debug() << "==> Finalize" << endmsg;

  info() << "weExchanged " <<counter("weExchanged").flag() << endmsg;
  info() << "weLost " <<counter("weLost").flag() << endmsg;
  info() << "weGained " <<counter("weGained").flag() << endmsg;

  return TrackCheckerTwoBase::finalize();  // must be called after all other actions
}


