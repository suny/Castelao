/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
#include "TrackEffNT.h"
#include "Linker/AllLinks.h"
#include "Linker/LinkedTo.h"
#include "Event/STCluster.h"
#include "Kernel/STChannelID.h"
#include "Event/Node.h"
#include "Event/OTMeasurement.h"
#include "Event/TrackFitResult.h"



#include "Event/Track.h"
#include "Event/State.h"
#include "Event/GhostTrackInfo.h"
#include "Event/OTTime.h"

#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/ToStream.h"

#include "Map.h"

#include <boost/foreach.hpp>


#include "TrackInterfaces/ITrackManipulator.h"

//-----------------------------------------------------------------------------
// Implementation file for class : TrackEffNT
//
// 2012-06-19 : Paul Seyfert <pseyfert@cern.ch>
//-----------------------------------------------------------------------------

DECLARE_COMPONENT( TrackEffNT );


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TrackEffNT::TrackEffNT(const std::string& name, ISvcLocator* pSvcLocator)
  : TrackCheckerTwoBase(name, pSvcLocator)
{
  ;
}

//=============================================================================
// Destructor
//=============================================================================
TrackEffNT::~TrackEffNT() {;}

//=============================================================================
// Initialization
//=============================================================================
StatusCode TrackEffNT::initialize()
{
  StatusCode sc = TrackCheckerTwoBase::initialize(); // must be executed first
  if (sc.isFailure()) return sc;  // error printed already by GaudiTupleAlg

  debug() << "==> Initialize" << endmsg;
  m_trackFitter = tool<ITrackFitter>("TrackInitFit");
  m_trackFitter_priv = tool<ITrackFitter>("TrackInitFit/myTrackInitFit",this);

  //sc = m_initTool.retrieve();
  m_ghostTool = tool<ITrackManipulator>("TrackNNGhostId");

    const TrackMaps::RecMap& theMap = TrackMaps::recDescription();
    m_long = theMap.find(std::string("ChargedLong"))->second;
    m_down = theMap.find(std::string("ChargedDownstream"))->second;


//  static const std::string histoDir = "Track/" ;
//  if ( "" == histoTopDir() ) setHistoTopDir(histoDir);
//
//  // Set the path for the linker table Track - MCParticle
//  if ( m_linkerInTableA == "" ) m_linkerInTableA = m_tracksInContainerA;
//  if ( m_linkerInTableB == "" ) m_linkerInTableB = m_tracksInContainerB;
//
//
//  m_selector = tool<IMCReconstructible>(m_selectorName,
//                                        "Selector", this );
//
//  m_extrapolator = tool<ITrackExtrapolator>(m_extrapolatorName);
//
//  // Retrieve the magnetic field and the poca tool
//  m_poca = tool<ITrajPoca>("TrajPoca");
//  m_pIMF = svc<IMagneticFieldSvc>( "MagneticFieldSvc",true );
//  m_stateCreator = tool<IIdealStateCreator>("IdealStateCreator");
//  m_visPrimVertTool = tool<IVisPrimVertTool>("VisPrimVertTool");
  m_ghostClassification = tool<ITrackGhostClassification>("LongGhostClassification",this);
//
//  const TrackMaps::RecMap& theMap = TrackMaps::recDescription();
//  m_recCat = theMap.find(m_selectionCriteria)->second;



  return sc;
}

bool TrackEffNT::samehits(LHCb::Track* one, LHCb::Track* two) {

  if (one == two) return true;

  unsigned fromids = 0;
  unsigned toids = 0;

  BOOST_FOREACH(const LHCb::LHCbID id, one->lhcbIDs()) {
    if (id.isVelo() || id.isST() || id.isOT()) ++fromids;
  }
  BOOST_FOREACH(const LHCb::LHCbID id, two->lhcbIDs()) {
    if (id.isVelo() || id.isST() || id.isOT()) ++toids;
  }


  if (fromids > toids)
    std::swap(one, two);

  unsigned n_total = (fromids>toids)?fromids:toids;//one->nLHCbIDs();
  unsigned n_common = 0;

  BOOST_FOREACH(const LHCb::LHCbID id, one->lhcbIDs()) {
    // count only hits in "standard tracking detectors"
    if ((!id.isVelo()) && (!id.isST()) && (!id.isOT())) continue;
    if (std::binary_search(
          two->lhcbIDs().begin(), two->lhcbIDs().end(), id))
      ++n_common;
  }

  return n_common==n_total;
}



void TrackEffNT::fillTrack(LHCb::Track* track, bool priv = false) {

using namespace Gaudi;
using namespace Gaudi::Units;
using namespace LHCb;


  LinkedTo<LHCb::MCParticle> otLink(evtSvc(),msgSvc(),
    LHCb::OTTimeLocation::Default);
  bool notfound = true;
  if (!fillingA && !priv) {
    for (unsigned kk = 0 ; kk < t_pointera.size() ; ++kk) {
      if (t_pointera[kk] && samehits(t_pointera[kk],track)) {
        t_pointerb[kk]=track;
        t_inb[kk]=1.;
        notfound = false;
        break;
      }
    }
  }
  if (notfound) {
    t_type.push_back((float)track->type());
    m_ghostTool->execute(*track);
    t_gp.push_back((float)track->ghostProbability());
    t_chi.push_back(track->chi2());
    t_dof.push_back(track->nDoF());
    t_hist.push_back(track->history());
    t_p.push_back(track->p());
    t_pt.push_back(track->pt());
    t_phi.push_back(track->phi());
    t_eta.push_back(track->pseudoRapidity());
    t_vchi.push_back(track->info(17,-999.));
    t_vdof.push_back(track->info(18,-999.));
    t_tchi.push_back(track->info(19,-999.));
    t_tdof.push_back(track->info(20,-999.));
    t_mchi.push_back(track->info(21,-999.));
    t_kl.push_back(track->info(LHCb::Track::CloneDist,-999.));
    if (fillingA && !priv) {
      t_ina.push_back(1.);
      t_inb.push_back(0.);
      t_pointera.push_back(track);
      t_pointerb.push_back(NULL);
    } 
    if (!fillingA && !priv) {
      t_inb.push_back(1.);
      t_ina.push_back(0.);
      t_pointera.push_back(NULL);
      t_pointerb.push_back(track);
    }
    if (priv) {
      t_inb.push_back(0.);
      t_ina.push_back(0.);
      t_pointera.push_back(NULL);
      t_pointerb.push_back(track);
    }
    const std::vector< LHCb::LHCbID > ids = track->lhcbIDs();
    float ttobsa=0;
    float itobsa=0;
    float otobsa=0;
    float veloobsa=0;
    for (std::vector<LHCb::LHCbID>::const_iterator idit = ids.begin() ; ids.end() != idit ; ++idit) {
      if ((*idit).isVelo()) veloobsa++;
      if ((*idit).isTT()) ttobsa++;
      if ((*idit).isIT()) itobsa++;
      if ((*idit).isOT()) otobsa++;
    }
    size_t numOT(0),numOTOutliers(0), numOTBadDriftTime(0);
    float goodsupression(0.f), badsupression(0.f);
    if (track->fitResult()) {
      BOOST_FOREACH( const LHCb::Node* node, track->fitResult()->nodes() ) {
        if(node->hasMeasurement() && node->measurement().type()==LHCb::Measurement::OT ) {
          ++numOT ; 
          if(node->type() == LHCb::Node::Outlier )
            ++numOTOutliers ;
          else {
            const LHCb::OTMeasurement* otmeas =
              static_cast<const LHCb::OTMeasurement*>(&(node->measurement() )) ;
            LHCb::OTChannelID otID = otmeas->channel();
            LHCb::MCParticle* mcp = otLink.first(otID);
            if( otmeas->driftTimeStrategy() != LHCb::OTMeasurement::FitDistance ) {
              ++numOTBadDriftTime ;
              if ((NULL == mcp) || (mcp != mcTruth(track))) {
                goodsupression+=1.f;
              } else {
                badsupression+=1.f;
              }
            } 
          }
        }

      }
    }
    float trueothits = 0.f;
    float falseothits = 0.f;
    {
      for (unsigned a = 0 ; a < track->lhcbIDs().size() ; ++a) {
        LHCb::LHCbID theid = track->lhcbIDs()[a];
        if (theid.isOT()) {
          LHCb::OTChannelID otID = theid.otID();
          LHCb::MCParticle* mcp = otLink.first(otID);
          if ((NULL == mcp) || (mcp != mcTruth(track))) {
            falseothits+=1.f;
          } else {
            trueothits+=1.f;
          }
        }
      }
    }
    t_otexp.push_back(0);
    t_otobs.push_back(otobsa);
    t_otout.push_back(numOTOutliers);
    t_otbad.push_back(numOTBadDriftTime);
    t_ottru.push_back(trueothits);
    t_otfal.push_back(falseothits);
    t_otgoodsup.push_back(goodsupression);
    t_otbadsup.push_back(badsupression);
    t_itexp.push_back(0);
    t_itobs.push_back(itobsa);
    t_ttexp.push_back(0);
    t_ttobs.push_back(ttobsa);
    t_veloexp.push_back(0);
    t_veloobs.push_back(veloobsa);
    LHCb::GhostTrackInfo ginfo;
    m_ghostClassification->info(*track,ginfo);
    t_ghostcat.push_back(ginfo.classification());
    float talastx = -999999;
    float talasty = -999999;


      if (track->stateAt(LHCb::State::EndRich2)) {
        talastx=track->stateAt(LHCb::State::EndRich2)->x();
        talasty=track->stateAt(LHCb::State::EndRich2)->y();
      }

    t_tblastx.push_back(talastx);
    t_tblasty.push_back(talasty);

    t_particleindex.push_back(0);
    if (!priv) 
    { // do something
      LHCb::Track* newtrack = new LHCb::Track();
      newtrack->copy(*track);
      m_trackFitter_priv->fit(*newtrack);
      fillTrack(newtrack,true);
      privtracks.push_back(newtrack);
      //delete newtrack;
    }
    t_refitindex.push_back(t_particleindex.size()-1);
  }

}

void TrackEffNT::filltrackparticleindex() {
  for (unsigned int k = 0 ; k < t_pointera.size() ; ++k) {
    LHCb::Track* track = t_pointera[k];
    if (NULL==track) {
      track = t_pointerb[k];
   }
    if (t_ina[k] == t_inb[k] && t_inb[k] < 0.5) {
      continue;
    }
    const LHCb::MCParticle* part = mcTruth(track);
    t_particleindex[k]= -1;
    if (part) {
      for (unsigned int m = 0 ; m < v_pointer.size() ; ++m) {
        if (v_pointer[m] == part) {
          t_particleindex[k] = m;
          break;
        }
      }
    }
  }
}

void TrackEffNT::cuttracks() {
  LinkedTo<LHCb::MCParticle,LHCb::STCluster>
    ttLink( evtSvc(), msgSvc(), LHCb::STClusterLocation::TTClusters );

  for (unsigned t = 0 ; t < t_pointera.size() ; ++t) {
    const LHCb::Track* thetrack;
    LHCb::Track* fittedtrack = new LHCb::Track();
    if (t_pointera[t])
      thetrack = t_pointera[t];
    else
      thetrack = t_pointerb[t];
    fittedtrack->copy(*thetrack);

    // remove TT here
    //
    bool through = (0>t_particleindex[t]);
    while(!through) {
      through = true;
      for (unsigned a = 0 ; a < fittedtrack->lhcbIDs().size() ; ++a) {
        LHCb::LHCbID theid = fittedtrack->lhcbIDs()[a];
        if (theid.isTT()) {
          LHCb::STChannelID ttID = theid.stID();
          LHCb::MCParticle* mcp = ttLink.first(ttID);
          if ((NULL == mcp) || (mcp != v_pointer[t_particleindex[t]])) {
            fittedtrack->removeFromLhcbIDs(theid);
            through = false;
            break;
          }
        }
      }
    }   //
    
    //
    //
    //
    m_trackFitter->fit(*fittedtrack);
    // m_initTool->fit(*thetrack,true);
    // m_trackFitter->fit(*thetrack);
    t_wrongttless_chi.push_back(fittedtrack->chi2());
    t_wrongttless_dof.push_back(fittedtrack->nDoF());
    t_wrongttless_p.push_back(  fittedtrack->p());
    t_wrongttless_pt.push_back( fittedtrack->pt());
    t_wrongttless_phi.push_back(fittedtrack->phi());
    t_wrongttless_eta.push_back(fittedtrack->pseudoRapidity());
    
    t_wrongttless_vchi.push_back(fittedtrack->info(17,-999.));
    t_wrongttless_vdof.push_back(fittedtrack->info(18,-999.));
    t_wrongttless_tchi.push_back(fittedtrack->info(19,-999.));
    t_wrongttless_tdof.push_back(fittedtrack->info(20,-999.));
    t_wrongttless_mchi.push_back(fittedtrack->info(21,-999.));//
    //
    through = false;
    while(!through) {
      through = true;
      for (unsigned a = 0 ; a < fittedtrack->lhcbIDs().size() ; ++a) {
        LHCb::LHCbID theid = fittedtrack->lhcbIDs()[a];
        if (theid.isTT()) {
          fittedtrack->removeFromLhcbIDs(theid);
          through = false;
          break;
        }
      }
    }   //
    m_trackFitter->fit(*fittedtrack);
    // m_initTool->fit(*thetrack,true);
    // m_trackFitter->fit(*thetrack);
    t_ttless_chi.push_back(fittedtrack->chi2());
    t_ttless_dof.push_back(fittedtrack->nDoF());
    t_ttless_p.push_back(  fittedtrack->p());
    t_ttless_pt.push_back( fittedtrack->pt());
    t_ttless_phi.push_back(fittedtrack->phi());
    t_ttless_eta.push_back(fittedtrack->pseudoRapidity());

    t_ttless_vchi.push_back(fittedtrack->info(17,-999.));
    t_ttless_vdof.push_back(fittedtrack->info(18,-999.));
    t_ttless_tchi.push_back(fittedtrack->info(19,-999.));
    t_ttless_tdof.push_back(fittedtrack->info(20,-999.));
    t_ttless_mchi.push_back(fittedtrack->info(21,-999.));
    delete fittedtrack;
  }
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode TrackEffNT::execute()
{
  debug() << "==> Execute" << endmsg;

  v_motherz.clear();
  v_tap.clear();
  v_tapt.clear();
  v_taphi.clear();
  v_taeta.clear();
  v_tbp.clear();
  v_tbpt.clear();
  v_tbphi.clear();
  v_tbeta.clear();
  v_p.clear();
  v_pt.clear();
  v_phi.clear();
  v_eta.clear();
  v_otexp.clear();
  v_otobsa.clear();
  v_otobsb.clear();
  v_itexp.clear();
  v_itobsa.clear();
  v_itobsb.clear();
  v_ttexp.clear();
  v_ttobsa.clear();
  v_ttobsb.clear();
  v_veloexp.clear();
  v_veloobsa.clear();
  v_veloobsb.clear();
  v_pid.clear();
  v_fromks.clear();
  v_longrec.clear();
  v_downrec.clear();
  v_fromb.clear();
  v_fromgoodb.clear();
  v_tracktypea.clear();
  v_tracktypeb.clear();
  v_counta.clear();
  v_countb.clear();
  v_tracktypeoverlap.clear();
  v_countoverlap.clear();
  v_trackchia.clear();
  v_tchia.clear();
  v_velochia.clear();
  v_dofa.clear();
  v_tdofa.clear();
  v_vdofa.clear();
  v_trackchib.clear();
  v_tchib.clear();
  v_velochib.clear();
  v_dofb.clear();
  v_tdofb.clear();
  v_vdofb.clear();
  v_talastx.clear();
  v_talasty.clear();
  v_tblastx.clear();
  v_tblasty.clear();
  v_tindexb.clear();
  v_tindexa.clear();


  v_pointer.clear();
  t_type.clear();
  t_chi.clear();
  t_gp.clear();
  t_dof.clear();
  t_hist.clear();
  t_p.clear();
  t_pt.clear();
  t_phi.clear();
  t_eta.clear();
  t_otexp.clear();
  t_otobs.clear();
  t_otout.clear();
  t_otbad.clear();
  t_ottru.clear();
  t_otfal.clear();
  t_otgoodsup.clear();
  t_otbadsup.clear();
  t_itexp.clear();
  t_itobs.clear();
  t_ttexp.clear();
  t_ttobs.clear();
  t_veloexp.clear();
  t_veloobs.clear();
  t_tblastx.clear();
  t_tblasty.clear();
  t_ina.clear();
  t_inb.clear();
  t_particleindex.clear();
  t_refitindex.clear();
  t_pointera.clear();
  t_pointerb.clear();
  t_ttless_chi.clear();
  t_ttless_dof.clear();
  t_ttless_p.clear();
  t_ttless_pt.clear();
  t_ttless_phi.clear();
  t_ttless_eta.clear();
  t_wrongttless_chi.clear();
  t_wrongttless_dof.clear();
  t_wrongttless_p.clear();
  t_wrongttless_pt.clear();
  t_wrongttless_phi.clear();
  t_wrongttless_eta.clear();



                    
  t_vchi.clear();
  t_vdof.clear();
  t_tchi.clear();
  t_tdof.clear();
  t_mchi.clear();
  t_ttless_vchi.clear();
  t_ttless_vdof.clear();
  t_ttless_tchi.clear();
  t_ttless_tdof.clear();
  t_ttless_mchi.clear();
  t_wrongttless_vchi.clear();
  t_wrongttless_vdof.clear();
  t_wrongttless_tchi.clear();
  t_wrongttless_tdof.clear();
  t_wrongttless_mchi.clear();
  t_kl.clear();
  t_ghostcat.clear();



  AllLinks<LHCb::MCParticle,ContainedObject> allIds( evtSvc(), msgSvc(), "Pat/LHCbID" );
  m_linkedIds.clear();
  LHCb::MCParticle* part = allIds.first();
  while ( NULL != part ) {
    unsigned int minSize = part->key();
    while ( m_linkedIds.size() <= minSize ) {
      std::vector<int> dum;
      m_linkedIds.push_back( dum );
    }
    m_linkedIds[part->key()].push_back( allIds.key() );
    part = allIds.next();
  }



  if (initializeEvent().isFailure()){
    return Error("Failed to initialize event", StatusCode::FAILURE);
  }


  const LHCb::MCParticles* partCont = get<LHCb::MCParticles>(LHCb::MCParticleLocation::Default);
  LHCb::MCParticles::const_iterator iterP = partCont->begin();

  const LHCb::Tracks* tra = get<LHCb::Tracks>(m_tracksInContainerA);
  LHCb::Tracks::const_iterator iterT = tra->begin();
  fillingA=true;
  for (; iterT!=tra->end() ; ++iterT) {
    fillTrack(*iterT);
  }
  fillingA=false;
  tra = get<LHCb::Tracks>(m_tracksInContainerB);
  iterT = tra->begin();
  for (; iterT!=tra->end() ; ++iterT) {
    fillTrack(*iterT);
  }


  for (; iterP!=partCont->end();++iterP) {


    LHCb::MCParticle* ppointer;



    int   tindexa = -1;
    int   tindexb = -1;
    float talastx = -10000.;
    float talasty = -10000.;
    float tblastx = -10000.;
    float tblasty = -10000.;
    float p       = -100.; //
    float pt      = -100.; //
    float phi     = -100.;
    float eta     = -100.; //
    float otexp   = -100.;
    float otobsa   = -100.; //
    float otobsb   = -100.; //
    float itexp   = -100.;
    float itobsa   = -100.; //
    float itobsb   = -100.; //
    float ttexp   = -100.;
    float ttobsa   = -100.; //
    float ttobsb   = -100.; //
    float veloexp = -100.;
    float veloobsa = -100.; //
    float veloobsb = -100.; //
    float motherz = -1000.;
    float pid;     //
    float fromks;  //
    float longrec = -5.; //
    float downrec = -5.; //
    float fromb;   //
    float fromgoodb; //
    float tracktypea;//
    float tracktypeb;//
    float counta;//
    float countb;//
    float tracktypeoverlap = -100.;
    float countoverlap = -100.;

    float trackchia = -100.;//
    float tchia = -100.;//
    float velochia = -100.;//
    float dofa = -100.;//
    float tdofa = -100.;//
    float vdofa = -100.;//

    float trackchib = -100.;//
    float tchib = -100.;    //
    float velochib = -100.; //
    float dofb = -100.;     //
    float tdofb = -100.;    //
    float vdofb = -100.;    //


    float tap = -100.;
    float tapt = -100.;
    float taphi = -100.;
    float taeta = -100.;
    float tbp = -100.;
    float tbpt = -100.;
    float tbphi = -100.;
    float tbeta = -100.;   




    ppointer = (*iterP);
    if ((*iterP)->mother())
      motherz = (*iterP)->mother()->endVertices()[0]->position().z();
    p = (*iterP)->p();
    pt = (*iterP)->pt();
    eta = (*iterP)->pseudoRapidity();
    fromgoodb = bAncestorWithReconstructibleDaughters(*iterP);
    fromb = bAncestor(*iterP);
    fromks = ksLambdaAncestor(*iterP);
    longrec = selector()->isReconstructibleAs(m_long,*iterP)?1.:0.;
    downrec = selector()->isReconstructibleAs(m_down,*iterP)?1.:0.;
    //always() << "hey, this guy(" << (*iterP)->key() << " " << *iterP << ") is " << longrec << "(longrec) " << downrec << "(downrec)" << "\t the longcat is " << m_long << endmsg;
    if (!longrec && !downrec) continue;
    pid = (*iterP)->particleID().pid();
    TrackCheckerTwoBase::LinkInfo infoA = reconstructedA(*iterP);
    TrackCheckerTwoBase::LinkInfo infoB = reconstructedB(*iterP);
    if (infoA.track) {
      for (unsigned int t = 0 ; t < t_pointera.size() ; ++t) {
        if (t_pointera[t] == infoA.track) {
          tindexa = t ;
          break;
        }
      }
      tracktypea=infoA.track->type();
      tap  =infoA.track->p();
      taphi=infoA.track->phi();
      tapt =infoA.track->pt();
      taeta=infoA.track->pseudoRapidity();
      counta=infoA.clone+1;
      const std::vector< LHCb::LHCbID > ids = infoA.track->lhcbIDs();
      ttobsa=0;
      itobsa=0;
      otobsa=0;
      veloobsa=0;
      if (infoA.track->stateAt(LHCb::State::EndRich2)) {
	talastx=infoA.track->stateAt(LHCb::State::EndRich2)->x();
	talasty=infoA.track->stateAt(LHCb::State::EndRich2)->y();
      }
      for (std::vector<LHCb::LHCbID>::const_iterator idit = ids.begin() ; ids.end() != idit ; ++idit) {
        if ((*idit).isVelo()) veloobsa++;
        if ((*idit).isTT()) ttobsa++;
        if ((*idit).isIT()) itobsa++;
        if ((*idit).isOT()) otobsa++;
      }
      trackchia = infoA.track->chi2();
      dofa = (float)infoA.track->nDoF();
      tchia = infoA.track->info(19, -100.);
      velochia = infoA.track->info(17, -100.);
      tdofa = infoA.track->info(20, -100.);
      vdofa = infoA.track->info(18, -100.);
    } else {
      tracktypea=0;
      counta=0;
    }
    if (infoB.track) {
      for (unsigned int t = 0 ; t < t_pointerb.size() ; ++t) {
        if (t_pointerb[t] == infoB.track) {
          tindexb = t ;
          break;
        }
      }
      tracktypeb=infoB.track->type();
      tbp  =infoB.track->p();
      tbphi=infoB.track->phi();
      tbpt =infoB.track->pt();
      tbeta=infoB.track->pseudoRapidity();
      countb=infoB.clone+1;
      const std::vector< LHCb::LHCbID > ids = infoB.track->lhcbIDs();
      ttobsb=0;
      itobsb=0;
      otobsb=0;
      veloobsb=0;
      if (infoB.track->stateAt(LHCb::State::EndRich2)) {
        tblastx=infoB.track->stateAt(LHCb::State::EndRich2)->x();
        tblasty=infoB.track->stateAt(LHCb::State::EndRich2)->y();
      }
      for (std::vector<LHCb::LHCbID>::const_iterator idit = ids.begin() ; ids.end() != idit ; ++idit) {
        if ((*idit).isVelo()) veloobsb++;
        if ((*idit).isTT()) ttobsb++;
        if ((*idit).isIT()) itobsb++;
        if ((*idit).isOT()) otobsb++;
      }
      trackchib = infoB.track->chi2();
      dofb = (float)infoB.track->nDoF();
      tchib = infoB.track->info(19, -100.);
      velochib = infoB.track->info(17, -100.);
      tdofb = infoB.track->info(20, -100.);
      vdofb = infoB.track->info(18, -100.);
    } else {
      tracktypeb=0;
      countb=0;
    }

    v_pointer.push_back(*iterP);
    v_motherz.push_back(motherz);
    v_tindexa.push_back(tindexa);
    v_tindexb.push_back(tindexb);
    v_talastx.push_back(talastx);
    v_talasty.push_back(talasty);
    v_tblastx.push_back(tblastx);
    v_tblasty.push_back(tblasty);
    v_tap.push_back(tap);
    v_tapt.push_back(tapt);
    v_taphi.push_back(taphi);
    v_taeta.push_back(taeta);
    v_tbp.push_back(tbp);
    v_tbpt.push_back(tbpt);
    v_tbphi.push_back(tbphi);
    v_tbeta.push_back(tbeta);    
    v_p.push_back(p);
    v_pt.push_back(pt);
    v_phi.push_back(phi);
    v_eta.push_back(eta);
    v_otexp.push_back(otexp);
    v_otobsa.push_back(otobsa);
    v_otobsb.push_back(otobsb);
    v_itexp.push_back(itexp);
    v_itobsa.push_back(itobsa);
    v_itobsb.push_back(itobsb);
    v_ttexp.push_back(ttexp);
    v_ttobsa.push_back(ttobsa);
    v_ttobsb.push_back(ttobsb);
    v_veloexp.push_back(veloexp);
    v_veloobsa.push_back(veloobsa);
    v_veloobsb.push_back(veloobsb);
    v_pid.push_back(pid);
    v_fromks.push_back(fromks);
    v_longrec.push_back(longrec);
    v_downrec.push_back(downrec);
    v_fromb.push_back(fromb);
    v_fromgoodb.push_back(fromgoodb);
    v_tracktypea.push_back(tracktypea);
    v_tracktypeb.push_back(tracktypeb);
    v_counta.push_back(counta);
    v_countb.push_back(countb);
    v_tracktypeoverlap.push_back(tracktypeoverlap);
    v_countoverlap.push_back(countoverlap);

    v_trackchia.push_back(trackchia);
    v_tchia.push_back(tchia);
    v_velochia.push_back(velochia);
    v_dofa.push_back(dofa);
    v_tdofa.push_back(tdofa);
    v_vdofa.push_back(vdofa);

    v_trackchib.push_back(trackchib);
    v_tchib.push_back(tchib);
    v_velochib.push_back(velochib);
    v_dofb.push_back(dofb);
    v_tdofb.push_back(tdofb);
    v_vdofb.push_back(vdofb);








  } // end particle loop


  filltrackparticleindex();
  cuttracks();

  Tuple theTuple = nTuple("TrackEffNT" , "", CLID_ColumnWiseTuple);
  int maxparts = 3000;
  int maxtracks = 1500;

  theTuple->farray("talastx",v_talastx,"Nparts",maxparts);
  theTuple->farray("talasty",v_talasty,"Nparts",maxparts);
  theTuple->farray("tblastx",v_tblastx,"Nparts",maxparts);
  theTuple->farray("tblasty",v_tblasty,"Nparts",maxparts);
  theTuple->farray("tap",v_tap,"Nparts",maxparts);
  theTuple->farray("tapt",v_tapt,"Nparts",maxparts);
  theTuple->farray("taphi",v_taphi,"Nparts",maxparts);
  theTuple->farray("taeta",v_taeta,"Nparts",maxparts);
  theTuple->farray("tbp",v_tbp,"Nparts",maxparts);
  theTuple->farray("tbpt",v_tbpt,"Nparts",maxparts);
  theTuple->farray("tbphi",v_tbphi,"Nparts",maxparts);
  theTuple->farray("tbeta",v_tbeta,"Nparts",maxparts);
  theTuple->farray("p",v_p,"Nparts",maxparts);
  theTuple->farray("pt",v_pt,"Nparts",maxparts);
  theTuple->farray("phi",v_phi,"Nparts",maxparts);
  theTuple->farray("eta",v_eta,"Nparts",maxparts);
  theTuple->farray("motherz",v_motherz,"Nparts",maxparts);
  theTuple->farray("otexp",v_otexp,"Nparts",maxparts);
  theTuple->farray("otobsa",v_otobsa,"Nparts",maxparts);
  theTuple->farray("otobsb",v_otobsb,"Nparts",maxparts);
  theTuple->farray("itexp",v_itexp,"Nparts",maxparts);
  theTuple->farray("itobsa",v_itobsa,"Nparts",maxparts);
  theTuple->farray("itobsb",v_itobsb,"Nparts",maxparts);
  theTuple->farray("ttexp",v_ttexp,"Nparts",maxparts);
  theTuple->farray("ttobsa",v_ttobsa,"Nparts",maxparts);
  theTuple->farray("ttobsb",v_ttobsb,"Nparts",maxparts);
  theTuple->farray("veloexp",v_veloexp,"Nparts",maxparts);
  theTuple->farray("veloobsa",v_veloobsa,"Nparts",maxparts);
  theTuple->farray("veloobsb",v_veloobsb,"Nparts",maxparts);
  theTuple->farray("pid",v_pid,"Nparts",maxparts);
  theTuple->farray("fromks",v_fromks,"Nparts",maxparts);
  theTuple->farray("longrec",v_longrec,"Nparts",maxparts);
  theTuple->farray("downrec",v_downrec,"Nparts",maxparts);
  theTuple->farray("fromb",v_fromb,"Nparts",maxparts);
  theTuple->farray("fromgoodb",v_fromgoodb,"Nparts",maxparts);
  theTuple->farray("tracktypea",v_tracktypea,"Nparts",maxparts);
  theTuple->farray("tracktypeb",v_tracktypeb,"Nparts",maxparts);
  theTuple->farray("counta",v_counta,"Nparts",maxparts);
  theTuple->farray("countb",v_countb,"Nparts",maxparts);
  theTuple->farray("tracktypeoverlap",v_tracktypeoverlap,"Nparts",maxparts);
  theTuple->farray("countoverlap",v_countoverlap,"Nparts",maxparts);
  theTuple->farray("trackchia",v_trackchia,"Nparts",maxparts);
  theTuple->farray("tchia",v_tchia,"Nparts",maxparts);
  theTuple->farray("velochia",v_velochia,"Nparts",maxparts);
  theTuple->farray("dofa",v_dofa,"Nparts",maxparts);
  theTuple->farray("tdofa",v_tdofa,"Nparts",maxparts);
  theTuple->farray("vdofa",v_vdofa,"Nparts",maxparts);
  theTuple->farray("trackchib",v_trackchib,"Nparts",maxparts);
  theTuple->farray("tchib",v_tchib,"Nparts",maxparts);
  theTuple->farray("velochib",v_velochib,"Nparts",maxparts);
  theTuple->farray("dofb",v_dofb,"Nparts",maxparts);
  theTuple->farray("tdofb",v_tdofb,"Nparts",maxparts);
  theTuple->farray("vdofb",v_vdofb,"Nparts",maxparts);
  theTuple->farray("tindexa",v_tindexa,"Nparts",maxparts);
  theTuple->farray("tindexb",v_tindexb,"Nparts",maxparts);

  theTuple->farray("type",t_type,"Ntracks",maxtracks);
  theTuple->farray("t_chi",t_chi,"Ntracks",maxtracks);
  theTuple->farray("t_gp",t_gp,"Ntracks",maxtracks);
  theTuple->farray("t_vchi",t_vchi,"Ntracks",maxtracks);
  theTuple->farray("t_tchi",t_tchi,"Ntracks",maxtracks);
  theTuple->farray("t_mchi",t_mchi,"Ntracks",maxtracks);
  theTuple->farray("t_dof",t_dof,"Ntracks",maxtracks);
  theTuple->farray("t_vdof",t_vdof,"Ntracks",maxtracks);
  theTuple->farray("t_tdof",t_tdof,"Ntracks",maxtracks);
  theTuple->farray("t_hist",t_hist,"Ntracks",maxtracks);
  theTuple->farray("t_p",t_p,"Ntracks",maxtracks);
  theTuple->farray("t_pt",t_pt,"Ntracks",maxtracks);
  theTuple->farray("t_phi",t_phi,"Ntracks",maxtracks);
  theTuple->farray("t_eta",t_eta,"Ntracks",maxtracks);
  theTuple->farray("t_otexp",t_otexp,"Ntracks",maxtracks);
  theTuple->farray("t_otobs",t_otobs,"Ntracks",maxtracks);
  theTuple->farray("t_otout",t_otout,"Ntracks",maxtracks);
  theTuple->farray("t_otbad",t_otbad,"Ntracks",maxtracks);
  theTuple->farray("t_ottru",t_ottru,"Ntracks",maxtracks);
  theTuple->farray("t_otfal",t_otfal,"Ntracks",maxtracks);
  theTuple->farray("t_otgoodsup",t_otgoodsup,"Ntracks",maxtracks);
  theTuple->farray("t_otbadsup",t_otbadsup,"Ntracks",maxtracks);
  theTuple->farray("t_itexp",t_itexp,"Ntracks",maxtracks);
  theTuple->farray("t_itobs",t_itobs,"Ntracks",maxtracks);
  theTuple->farray("t_ttexp",t_ttexp,"Ntracks",maxtracks);
  theTuple->farray("t_ttobs",t_ttobs,"Ntracks",maxtracks);
  theTuple->farray("t_veloexp",t_veloexp,"Ntracks",maxtracks);
  theTuple->farray("t_veloobs",t_veloobs,"Ntracks",maxtracks);
  theTuple->farray("t_tlastx",t_tblastx,"Ntracks",maxtracks);
  theTuple->farray("t_tlasty",t_tblasty,"Ntracks",maxtracks);
  theTuple->farray("t_ina",t_ina,"Ntracks",maxtracks);
  theTuple->farray("t_inb",t_inb,"Ntracks",maxtracks);
  theTuple->farray("t_particleindex",t_particleindex,"Ntracks",maxtracks);
  theTuple->farray("t_refitindex",t_refitindex,"Ntracks",maxtracks);
  theTuple->farray("t_kl",t_kl,"Ntracks",maxtracks);
  theTuple->farray("t_ttless_chi",t_ttless_chi,"Ntracks",maxtracks);
  theTuple->farray("t_ttless_vchi",t_ttless_vchi,"Ntracks",maxtracks);
  theTuple->farray("t_ttless_tchi",t_ttless_tchi,"Ntracks",maxtracks);
  theTuple->farray("t_ttless_mchi",t_ttless_mchi,"Ntracks",maxtracks);
  theTuple->farray("t_ttless_dof",t_ttless_dof,"Ntracks",maxtracks);
  theTuple->farray("t_ttless_vdof",t_ttless_vdof,"Ntracks",maxtracks);
  theTuple->farray("t_ttless_tdof",t_ttless_tdof,"Ntracks",maxtracks);
  theTuple->farray("t_ttless_p",  t_ttless_p,"Ntracks",maxtracks);
  theTuple->farray("t_ttless_pt", t_ttless_pt,"Ntracks",maxtracks);
  theTuple->farray("t_ttless_phi",t_ttless_phi,"Ntracks",maxtracks);
  theTuple->farray("t_ttless_eta",t_ttless_eta,"Ntracks",maxtracks);
  theTuple->farray("t_wrongttless_chi",t_wrongttless_chi,"Ntracks",maxtracks);
  theTuple->farray("t_wrongttless_vchi",t_wrongttless_vchi,"Ntracks",maxtracks);
  theTuple->farray("t_wrongttless_tchi",t_wrongttless_tchi,"Ntracks",maxtracks);
  theTuple->farray("t_wrongttless_mchi",t_wrongttless_mchi,"Ntracks",maxtracks);
  theTuple->farray("t_wrongttless_dof",t_wrongttless_dof,"Ntracks",maxtracks);
  theTuple->farray("t_wrongttless_vdof",t_wrongttless_vdof,"Ntracks",maxtracks);
  theTuple->farray("t_wrongttless_tdof",t_wrongttless_tdof,"Ntracks",maxtracks);
  theTuple->farray("t_wrongttless_p",  t_wrongttless_p,"Ntracks",maxtracks);
  theTuple->farray("t_wrongttless_pt", t_wrongttless_pt,"Ntracks",maxtracks);
  theTuple->farray("t_wrongttless_phi",t_wrongttless_phi,"Ntracks",maxtracks);
  theTuple->farray("t_wrongttless_eta",t_wrongttless_eta,"Ntracks",maxtracks);
  theTuple->farray("t_ghostcat",t_ghostcat,"Ntracks",maxtracks);
 

  theTuple->write();
  for (unsigned kkkk = 0 ; kkkk < privtracks.size() ; ++kkkk) {
    delete privtracks[kkkk];
  }
  privtracks.clear();
  return StatusCode::SUCCESS;
}

//=============================================================================
// Finalize
//=============================================================================
StatusCode TrackEffNT::finalize()
{
  debug() << "==> Finalize" << endmsg;


  return GaudiTupleAlg::finalize();  // must be called after all other actions
}


