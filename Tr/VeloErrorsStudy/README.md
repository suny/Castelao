# Study VELO error parametrisation with track refitting on MC

This package provides functions to refit Run 2 MC using the VELO error parametrisation applied to 2017 real data, in order to study the effect on IP chi2, FD chi2, etc.

For further info, please see the [summary from A&S week](https://indico.cern.ch/event/958290/#8-on-the-velo-hit-error-parame). The plots presented there use these options.

Example ntupling options are in [options/tuple.py](options/tuple.py) for D0->Kpi LTUNB Turbo MC, and [options/tuple_bhadron.py](options/tuple_bhadron.py) for Lb->Lcpi Stripping filtered MC. These have been tested with Castelao/v3r6 (vased on DaVinci/v45r5.

The main functionality is in [python/VeloErrorsStudy/refit.py](python/VeloErrorsStudy/refit.py). Note that you can pass a list of selections as the first argument to `refit_tracks_and_particles`.

With this, you get two TTrees in your output file - one before refitting, and one after, but otherwise identical. So you can add one as a friend with an alias to the other and then do comparisons of, eg, IP chi2:

```
import ROOT

f = ROOT.TFile('Tuples.root')
toriginal = f.Get('D0KpiTuple/DecayTree')
trefit = f.Get('D0KpiTuple_refit/DecayTree')
trefit.AddFriend(toriginal, 'original')

trefit.Draw('K_IPCHI2_OWNPV/original.K_IPCHI2_OWNPV')
```

You could also apply cuts to some variables without the refit (eg, Track chi2 and vertex chi2) and some variables with it (eg, IP chi2 and FD chi2).
