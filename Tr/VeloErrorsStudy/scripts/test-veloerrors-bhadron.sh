#!/usr/bin/bash
###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

data=$1
if [ -z "$data" ] ; then
    data=MC
    #data=Data
fi
opts="$(ls ${VELOERRORSSTUDYOPTS}/Bhadron_${data}_2017_MagDown*.py)"
opts+=" ${VELOERRORSSTUDYOPTS}/tuple_bhadron.py"

if [ ! -d $data ] ; then
    mkdir $data
fi
cd $data
if [ -z "$2" ] ; then
    gaudirun.py --option "from Configurables import DaVinci;DaVinci().EvtMax = 1000" $opts | tee stdout
else
    gaudipython $opts
fi
cd -
