###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

'''Functions to configure track & particle refitting, and add the VELO error
parametrisation conditions from 2017 real data.'''

from Configurables import GaudiSequencer, CopyProtoParticle2MCRelations, TESCheck, \
    TrackContainerCopy, ParticleRefitter, MCMatchObjP2MCRelator
from TrackFitter.ConfiguredFitters import ConfiguredEventFitter
from PhysSelPython.Wrappers import AutomaticData, SelectionSequence, \
    MultiSelectionSequence
import os
from Gaudi.Configuration import importOptions
from DSTWriters.microdstelements import CloneParticleTrees
from TeslaTools import TeslaTruthUtils


def add_simcond_overlay():
    '''Add the SimCond overlay to use the VELO error parametrisation from
    2017 real data.'''
    importOptions('$APPCONFIGOPTS/Conditions/Velo-MC-ErrorParam-2017.py')
    

def default_relations_locations():
    '''Get the default relations locations.'''
    return MCMatchObjP2MCRelator().getDefaultProperty('RelTableLocations')


def _originals_loc(loc):
    '''Get the location of the MC truth relations tables for the copied original
    (pre-refit) particles given the location of the default tables.'''
    if loc.startswith('/Event'):
        return loc.replace('/Event', '/Event/Originals')
    return 'Originals/' + loc


def originals_relations_locations(locations = default_relations_locations()):
    '''Get the locations of the MC truth relations tables for the copied
    original (pre-refit) particles given the locations of the default
    tables.'''
    return [_originals_loc(loc) for loc in locations]


def relations_locations(locations = default_relations_locations()):
    '''Get the locations of the MC truth relations tables, including
    the original (pre-refit) and refitted particles.'''
    return locations + originals_relations_locations(locations)


def refit_tracks_and_particles(inputselections, RootInTES, simulation,
                               tracklocations = ['Rec/Track/Best', 'Track/Best/Long'],
                               relationslocations = relations_locations()):
    '''Configure refitting of tracks and particles from the given Selections. 'inputselections' can either be a 
    single Selection or a list/tuple of Selections. The original particles & tracks are copied before refitting. 
    Returns the refitting sequence and a list of Selections that point to the originals (non-refitted).
    The Selection(s) given in 'inputselections' will then use the refitted tracks & particles.'''

    if not isinstance(inputselections, (tuple, list)):
        inputselections = [inputselections]
    if not isinstance(tracklocations, (tuple, list)):
        tracklocations = [tracklocations]

    # Copy the original tracks & particles and make a selection that uses them.
    # This is necessary due to weird caching issues if you ntuple the same particles before
    # and after refitting.
    refitseq = GaudiSequencer('RefitSeq', IgnoreFilterPassed = True)
    copyseq = GaudiSequencer('CopySeq', IgnoreFilterPassed = True)
    refitseq.Members.append(copyseq)
    originals = []
    partrefitseq = GaudiSequencer('ParticleRefitSeq', IgnoreFilterPassed = True)

    # Use CloneParticleTrees to copy particles with MC links.
    cloneparts = CloneParticleTrees('Originals', isMC = simulation)
    originalsseq = MultiSelectionSequence('Originals')
    for inputsel in inputselections:
        originalsseq._sequences.append(SelectionSequence('Originals:' + inputsel.name(),
                                                         TopSelection = inputsel))
    copyparts = cloneparts(originalsseq)[0]
    copyseq.Members.append(copyparts)

    if simulation:
        for inputsel in inputselections:
            copymc = CopyProtoParticle2MCRelations('CopyMCRelations:' + inputsel.name(),
                                                   InputLocations = relationslocations,
                                                   OutputPrefix = copyparts.OutputPrefix)
            copyseq.Members.append(copymc)
            
    # Selections for the copies of the non-refit originals.
    for inputsel in inputselections:
        original = AutomaticData(copyparts.OutputPrefix + '/' + inputsel.outputLocation())
        originals.append(original)
        
        # Refit the particles after the track refit.
        partrefitter = ParticleRefitter('RefitParticles:' + inputsel.name(), Inputs = [inputsel.outputLocation()])
        partrefitseq.Members.append(partrefitter)
        
    # Refit the tracks.
    for loc in tracklocations:
        trackrefitseq = GaudiSequencer('TrackRefit:' + loc)
        refitseq.Members.append(trackrefitseq)
        
        # Check that the tracks location exists.
        check = TESCheck('Check:' + loc, Inputs = [os.path.join(RootInTES, loc)], Stop = False)
        # Keep a copy of the original tracks.
        cp = TrackContainerCopy('Copy:' + loc, inputLocations = [loc], outputLocation = loc + 'Original')
        # Refit the original tracks in place.
        evtfitter = ConfiguredEventFitter('RefitTracks:' + loc, loc, SimplifiedGeometry = True,
                                          LiteClusters = True, MSRossiAndGreisen = True)

        trackrefitseq.Members += [check, cp, evtfitter]

    # Add the particle refitters to the sequence.
    refitseq.Members.append(partrefitseq)

    # Return the refitting sequence and Selections for the original, non-refitted particles.
    return refitseq, originals


def configure_originals_mc_truth(dtt, mctoollist,
                                 relations = default_relations_locations()):
    '''Configure MC truth for the copied non-refitted particles. dtt is the 
    DecayTreeTuple instance, mctoollist is the tool list given to 
    TupleToolMCTruth. Optionally, non-default relations locations can be 
    given (ie, for Turbo).'''
    if 'TupleToolMCTruth' not in dtt.ToolList:
        dtt.ToolList += ['TupleToolMCTruth']
    TeslaTruthUtils.makeTruth(dtt, relations_locations(relations), mctoollist)
    for tool in (dtt.TupleToolMCTruth.DaVinciSmartAssociator,
                 dtt.TupleToolMCTruth.MCMatchObjP2MCRelator,
                 dtt.TupleToolMCTruth.DaVinciSmartAssociator.P2MCPFromProtoP,
                 dtt.TupleToolMCTruth.DaVinciSmartAssociator.BackgroundCategory.P2MCPFromProtoP,
                 dtt.TupleToolMCBackgroundInfo.BackgroundCategory.P2MCPFromProtoP,):
        tool.MCParticleDefaultLocation = 'Originals/MC/Particles'
