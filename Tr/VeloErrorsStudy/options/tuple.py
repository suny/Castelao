###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

'''Options to make ntuples for D*->(D0->Kpi)pi from Turbo output. On MC, 
ntuples are made for the original candidates plus an identical ntuple for 
candidates refitted with the 2017 VELO error parametrisation from real data.

Tested on data from:
/LHCb/Collision17/Beam6500GeV-VeloClosed-MagDown/Real Data/Turbo04/94000000/CHARMTWOBODY.MDST

and MC from:
/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-Pythia8/Sim09f/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/27163003/ALLSTREAMS.MDST
'''

from Configurables import DaVinci
from PhysSelPython.Wrappers import TurboData, SelectionSequence, TupleSelection
from TeslaTools import TeslaTruthUtils
from VeloErrorsStudy.refit import add_simcond_overlay, \
    refit_tracks_and_particles, configure_originals_mc_truth

data = TurboData('Hlt2CharmHadDstp2D0Pip_D02KmPip_LTUNBTurbo')
decay = "[D*(2010)+ -> ^(D0 -> ^K- ^pi+) ^pi+]CC"

turborelations = TeslaTruthUtils.getRelLocs() + [
    TeslaTruthUtils.getRelLoc(''),
    # Location of the truth tables for PersistReco objects
    'Relations/Hlt2/Protos/Charged'
]


def make_tuple(name, inputdata, mc, isoriginals = False):
    '''Make the ntuple with the given name from the input selection.
    If mc = True, add MC truth tools. If isoriginals = True,
    configure MC truth for the copied, non-refitted particles.'''
    dtt = TupleSelection(name, [inputdata], Decay = decay,
                         Branches = {"Dst"   : "[D*(2010)+ -> D0 pi+]CC",
                                     "D0"    : "[D*(2010)+ -> ^D0 pi+]CC",
                                     "PiBach": "[D*(2010)+ -> D0 ^pi+]CC",
                                     "K"     : "[D*(2010)+ -> (D0->^K- pi+) pi+]CC",
                                     "Pi"    : "[D*(2010)+ -> (D0->K- ^pi+) pi+]CC",
                                     })
    dtf = dtt.Dst.addTupleTool('TupleToolDecayTreeFitter/DTF')
    dtf.Verbose = True
    tttr = dtt.addTupleTool('TupleToolTrackInfo')
    tttr.Verbose = True
    hybrid = dtt.addTupleTool('LoKi::Hybrid::TupleTool')
    hybrid.Variables = {'BPVLTIME' : 'BPVLTIME()'}
    if mc:
        dtt.ToolList += ['TupleToolMCTruth', 'TupleToolMCBackgroundInfo']
        mctools = ['MCTupleToolKinematic', 'MCTupleToolPrompt']
        if not isoriginals:
            TeslaTruthUtils.makeTruth(dtt.algorithm(), turborelations, mctools)
        else:
            configure_originals_mc_truth(dtt.algorithm(), mctools, turborelations)
    seq = SelectionSequence(name + '_Seq', TopSelection = dtt)
    return seq.sequence()


mc = DaVinci().getProp('Simulation')
if mc:
    DaVinci().TurboStream = 'AllStreams'
    # Add the overlay for the VELO error parametrisation from real data.
    add_simcond_overlay()
    # Get the sequence to refit the tracks & particles with the updated
    # VELO error parametrisation, and selections for the original,
    # non-refitted particles.
    refitseq, originals = refit_tracks_and_particles(data, DaVinci().getRootInTES(), mc,
                                                     relationslocations = turborelations)
    originals = originals[0]
    DaVinci().UserAlgorithms += [refitseq, make_tuple('D0KpiTuple_refit', data, mc)]
else:
    DaVinci().TurboStream = 'CharmTwoBody'
    originals = data

DaVinci().UserAlgorithms += [make_tuple('D0KpiTuple', originals, mc, True)]
DaVinci().TupleFile = 'Tuples.root'
