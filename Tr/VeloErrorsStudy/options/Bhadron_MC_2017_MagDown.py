###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
lb-run LHCbDirac/prod dirac-bookkeeping-get-files -B /MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-Pythia8/Sim09h/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2Filtered/15364010/LB2LCH.STRIP.DST --DQFlags OK

For BK query: {'Visible': 'Yes', 'ConfigName': 'MC', 'ConditionDescription': 'Beam6500GeV-2017-MagDown-Nu1.6-25ns-Pythia8', 'DataQuality': ['OK'], 'EventType': '15364010', 'FileType': 'LB2LCH.STRIP.DST', 'ConfigVersion': '2017', 'ProcessingPass': '/Sim09h/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2Filtered', 'SimulationConditions': 'Beam6500GeV-2017-MagDown-Nu1.6-25ns-Pythia8'}
Nb of Files      : 7
Nb of Events     : 93'754
Total size       : 25.179 GB (268.6 kB per evt)
Luminosity       : 0.000 
Size  per /pb    : 0.0 GB


'''

from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles(
['LFN:/lhcb/MC/2017/LB2LCH.STRIP.DST/00096787/0000/00096787_00000001_1.lb2lch.strip.dst',
 'LFN:/lhcb/MC/2017/LB2LCH.STRIP.DST/00096787/0000/00096787_00000002_1.lb2lch.strip.dst',
 'LFN:/lhcb/MC/2017/LB2LCH.STRIP.DST/00096787/0000/00096787_00000003_1.lb2lch.strip.dst',
 'LFN:/lhcb/MC/2017/LB2LCH.STRIP.DST/00096787/0000/00096787_00000004_1.lb2lch.strip.dst',
 'LFN:/lhcb/MC/2017/LB2LCH.STRIP.DST/00096787/0000/00096787_00000005_1.lb2lch.strip.dst',
 'LFN:/lhcb/MC/2017/LB2LCH.STRIP.DST/00096787/0000/00096787_00000006_1.lb2lch.strip.dst',
 'LFN:/lhcb/MC/2017/LB2LCH.STRIP.DST/00096787/0000/00096787_00000007_1.lb2lch.strip.dst'],
clear=True)
