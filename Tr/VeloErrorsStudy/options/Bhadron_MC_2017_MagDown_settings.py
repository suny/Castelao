###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Production Info: 
    Configuration Name: MC
    Configuration Version: 2017
    Event type: 15364010
-----------------------
 StepName: Sim09h - 2017 - MD - Nu1.6 (Lumi 4 at 25ns) - 25ns spillover - Pythia8 
    StepId             : 139087
    ApplicationName    : Gauss
    ApplicationVersion : v49r14
    OptionFiles        : $APPCONFIGOPTS/Gauss/Beam6500GeV-md100-2017-nu1.6.py;$APPCONFIGOPTS/Gauss/EnableSpillover-25ns.py;$APPCONFIGOPTS/Gauss/DataType-2016.py;$APPCONFIGOPTS/Gauss/RICHRandomHits.py;$DECFILESROOT/options/@{eventType}.py;$LBPYTHIA8ROOT/options/Pythia8.py;$APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmNoCuts.py
    DDB                : dddb-20170721-3
    CONDDB             : sim-20190430-1-vc-md100
    ExtraPackages      : AppConfig.v3r383;Gen/DecFiles.v30r32
    Visible            : Y
-----------------------
Number of Steps   9780
Total number of files: 19659
         GAUSSHIST:99
         SIM:9780
         LOG:9780
Number of events 0
Path:  /MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-Pythia8/Sim09h

'''

from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().DataType = '2017'
DaVinci().Simulation = True
DaVinci().CondDBtag = 'sim-20190430-1-vc-md100'
DaVinci().DDDBtag = 'dddb-20170721-3'
