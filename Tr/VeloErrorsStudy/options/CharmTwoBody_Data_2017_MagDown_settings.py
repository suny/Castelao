###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Production Info: 
    Configuration Name: LHCb
    Configuration Version: Collision17
    Event type: 94000000
-----------------------
 StepName: Turbo Physics lines, 2017, post TS2 
    StepId             : 132190
    ApplicationName    : DaVinci
    ApplicationVersion : v42r6p1
    OptionFiles        : $APPCONFIGOPTS/Turbo/Streams_pp_2017.py;$APPCONFIGOPTS/Turbo/Tesla_Data_2017.py;$APPCONFIGOPTS/Turbo/Tesla_KillInputHlt2Reps.py;$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py
    DDB                : dddb-20150724
    CONDDB             : cond-20170724
    ExtraPackages      : AppConfig.v3r329;TurboStreamProd.v4r2p5
    Visible            : Y
-----------------------
Number of Steps   9305
Total number of files: 55830
         CHARMTWOBODY.MDST:9305
         LEPTONS.MDST:9305
         CHARMCHARGED.MDST:9305
         CHARMMULTIBODY.MDST:9305
         LOG:9305
         CHARMSPEC.MDST:9305
Number of events 0
Path:  /LHCb/Collision17/Beam6500GeV-VeloClosed-MagDown/Real Data/Turbo04

'''

from Configurables import DaVinci
DaVinci().InputType = 'MDST'
DaVinci().DataType = '2017'

from Configurables import CondDB
CondDB().LatestGlobalTagByDataType = DaVinci().getProp('DataType')
