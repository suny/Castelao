2018-11-29 - Castelao v1r4
========================================
## Prepare version v1r4, based on DaVinci v44r6.
   - Modifications for TrackCalib WG production (!27 & !26):
     + Add MC DDDB&CondDB dict in Tuple.py.
     + Update files for WG Production.
     + Update TupleToolTrackEffMatch.
     + Added TrackInfo as in TupleToolTrackCalib.
     + Add scripts for T&A WGProduction.
   - Added copyright statements (!23).
