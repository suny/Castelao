# When preparing a release of Castelao:
  - ```git clone --recurse-submodules ssh://git@gitlab.cern.ch:7999/lhcb/Castelao.git```
# Math/SomeUtils is submoduled from lhcb/Math in this way:
  - ```git submodule add ssh://git@gitlab.cern.ch:7999/lhcb/Math.git```

